-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/24/2017 16:36:17


DELETE FROM `areatrigger_template` WHERE `Id`=12515;
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `VerifiedBuild`) VALUES
(12515, 0, 2, 0.5, 2, 0, 0, 0, 0, 23420);


INSERT IGNORE INTO `weather_update` (`map_id`, `zone_id`, `weather_state`, `grade`, `unk`) VALUES
(0, 0, 102, 0.4, 0); -- 0 - 102 - 0.4


DELETE FROM `quest_poi` WHERE (`QuestID`=24602 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=24602 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24602 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24678 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24920 AND `BlobIndex`=7 AND `Idx1`=7) OR (`QuestID`=24920 AND `BlobIndex`=6 AND `Idx1`=6) OR (`QuestID`=24920 AND `BlobIndex`=5 AND `Idx1`=5) OR (`QuestID`=24920 AND `BlobIndex`=4 AND `Idx1`=4) OR (`QuestID`=24920 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=24920 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=24920 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24920 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24903 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24902 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24904 AND `BlobIndex`=0 AND `Idx1`=1) OR (`QuestID`=24904 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24676 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=24676 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=24676 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24676 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24674 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24674 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24575 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=24575 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24575 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24675 AND `BlobIndex`=4 AND `Idx1`=2) OR (`QuestID`=24675 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24675 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24677 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24592 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=24592 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24592 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24672 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24673 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24593 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=24593 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=24593 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24593 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24646 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24646 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24628 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24628 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24627 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24627 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24617 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24616 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24616 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24578 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24501 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24501 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24495 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=24495 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=24495 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24495 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24484 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24484 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24483 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24472 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=24472 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24472 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24468 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24468 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24438 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14466 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14465 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14402 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14401 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14401 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14400 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14400 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14399 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14399 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14412 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14412 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14404 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=14404 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=14404 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14404 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14416 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14416 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14406 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14403 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14398 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14397 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14395 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14395 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14396 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14386 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14386 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14368 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=14368 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=14368 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14368 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14382 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=14382 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14382 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14369 AND `BlobIndex`=9 AND `Idx1`=1) OR (`QuestID`=14369 AND `BlobIndex`=5 AND `Idx1`=0) OR (`QuestID`=14367 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14366 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14348 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14348 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14347 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14347 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14336 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14321 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14320 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14313 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14222 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14222 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14221 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14218 AND `BlobIndex`=7 AND `Idx1`=7) OR (`QuestID`=14218 AND `BlobIndex`=6 AND `Idx1`=6) OR (`QuestID`=14218 AND `BlobIndex`=5 AND `Idx1`=5) OR (`QuestID`=14218 AND `BlobIndex`=4 AND `Idx1`=4) OR (`QuestID`=14218 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=14218 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=14218 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14218 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14212 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14294 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14293 AND `BlobIndex`=0 AND `Idx1`=1) OR (`QuestID`=14293 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14214 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14204 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14204 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14159 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=26129 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14154 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14154 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=28850 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24930 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=24930 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24930 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14157 AND `BlobIndex`=2 AND `Idx1`=0) OR (`QuestID`=14288 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14277 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14099 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14094 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=14094 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=14094 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14094 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14098 AND `BlobIndex`=3 AND `Idx1`=2) OR (`QuestID`=14098 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14098 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14093 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=14093 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=14093 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=14093 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14091 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=14078 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13518 AND `BlobIndex`=4 AND `Idx1`=4) OR (`QuestID`=13518 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=13518 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13518 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13518 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=26385 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=28517 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=26706 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=26706 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24681 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=24681 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24681 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24680 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=24679 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=24679 AND `BlobIndex`=0 AND `Idx1`=0);
INSERT INTO `quest_poi` (`QuestID`, `BlobIndex`, `Idx1`, `ObjectiveIndex`, `QuestObjectiveID`, `QuestObjectID`, `MapID`, `WorldMapAreaId`, `Floor`, `Priority`, `Flags`, `WorldEffectID`, `PlayerConditionID`, `WoDUnk1`, `VerifiedBuild`) VALUES
(24602, 2, 2, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24602
(24602, 1, 1, 0, 265438, 49921, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24602
(24602, 0, 0, 0, 265438, 49921, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24602
(24678, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24678
(24920, 7, 7, -1, 0, 0, 638, 539, 0, 0, 1, 0, 0, 0, 23420), -- 24920
(24920, 6, 6, 0, 265635, 38363, 638, 539, 0, 0, 1, 0, 0, 0, 23420), -- 24920
(24920, 5, 5, 0, 265634, 38287, 638, 539, 0, 0, 1, 0, 0, 0, 23420), -- 24920
(24920, 4, 4, 0, 265634, 38287, 638, 539, 0, 0, 1, 0, 0, 0, 23420), -- 24920
(24920, 3, 3, 0, 265634, 38287, 638, 539, 0, 0, 1, 0, 0, 0, 23420), -- 24920
(24920, 2, 2, -1, 0, 0, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 24920
(24920, 1, 1, 0, 265635, 38363, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 24920
(24920, 0, 0, 0, 265635, 38363, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 24920
(24903, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24903
(24902, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24902
(24904, 0, 1, 0, 265467, 38854, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24904
(24904, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24904
(24676, 3, 3, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24676
(24676, 2, 2, 0, 265134, 37685, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24676
(24676, 1, 1, 0, 265133, 37686, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24676
(24676, 0, 0, 0, 265132, 37692, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24676
(24674, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24674
(24674, 0, 0, 0, 265172, 37802, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24674
(24575, 2, 2, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24575
(24575, 1, 1, 27, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24575
(24575, 0, 0, 0, 266403, 201775, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24575
(24675, 4, 2, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24675
(24675, 1, 1, 0, 266831, 50219, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24675
(24675, 0, 0, 0, 266831, 50219, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24675
(24677, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24677
(24592, 2, 2, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24592
(24592, 1, 1, 0, 265566, 37733, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24592
(24592, 0, 0, 0, 265565, 37735, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24592
(24672, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24672
(24673, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24673
(24593, 3, 3, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24593
(24593, 2, 2, 0, 266555, 201952, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24593
(24593, 1, 1, 0, 266554, 201951, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24593
(24593, 0, 0, 0, 266553, 201950, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24593
(24646, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24646
(24646, 0, 0, 0, 256045, 50086, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24646
(24628, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24628
(24628, 0, 0, 0, 265213, 50017, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24628
(24627, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24627
(24627, 0, 0, 0, 266711, 37757, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24627
(24617, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24617
(24616, 1, 1, 0, 266512, 37953, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24616
(24616, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24616
(24578, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24578
(24501, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24501
(24501, 0, 0, 0, 265584, 37045, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24501
(24495, 3, 3, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24495
(24495, 2, 2, 0, 256540, 49760, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24495
(24495, 1, 1, -1, 0, 0, 638, 539, 0, 0, 1, 0, 0, 0, 23420), -- 24495
(24495, 0, 0, 0, 256540, 49760, 638, 539, 0, 0, 1, 0, 0, 0, 23420), -- 24495
(24484, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24484
(24484, 0, 0, 0, 253933, 36813, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24484
(24483, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24483
(24472, 2, 2, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24472
(24472, 1, 1, 0, 265446, 49742, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24472
(24472, 0, 0, 0, 265445, 36293, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24472
(24468, 1, 1, 0, 255768, 37078, 654, 545, 0, 0, 7, 0, 0, 0, 23420), -- 24468
(24468, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24468
(24438, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24438
(14466, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14466
(14465, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14465
(14402, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14402
(14401, 1, 1, -1, 0, 0, 654, 545, 0, 0, 7, 0, 0, 0, 23420), -- 14401
(14401, 0, 0, 0, 264872, 49281, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14401
(14400, 1, 1, -1, 0, 0, 654, 545, 0, 0, 7, 0, 0, 0, 23420), -- 14400
(14400, 0, 0, 0, 265575, 49279, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14400
(14399, 1, 1, -1, 0, 0, 654, 545, 0, 0, 7, 0, 0, 0, 23420), -- 14399
(14399, 0, 0, 0, 265253, 49280, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14399
(14412, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14412
(14412, 0, 0, 0, 264483, 36488, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14412
(14404, 3, 3, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14404
(14404, 2, 2, 0, 265423, 49339, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14404
(14404, 1, 1, 0, 265422, 49338, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14404
(14404, 0, 0, 0, 265421, 49337, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14404
(14416, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14416
(14416, 0, 0, 0, 261452, 36560, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14416
(14406, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14406
(14403, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14403
(14398, 0, 0, -1, 0, 0, 654, 678, 0, 0, 7, 0, 0, 0, 23420), -- 14398
(14397, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14397
(14395, 1, 1, 0, 256573, 36450, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14395
(14395, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14395
(14396, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14396
(14386, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14386
(14386, 0, 0, 0, 267072, 36312, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14386
(14368, 3, 3, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14368
(14368, 2, 2, 0, 264865, 36289, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14368
(14368, 1, 1, 0, 264864, 36288, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14368
(14368, 0, 0, 0, 264863, 36287, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14368
(14382, 2, 2, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14382
(14382, 1, 1, 0, 264796, 36399, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14382
(14382, 0, 0, 0, 264795, 36397, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14382
(14369, 9, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14369
(14369, 5, 0, 0, 255603, 36634, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14369
(14367, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14367
(14366, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14366
(14348, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14348
(14348, 0, 0, 26, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14348
(14347, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14347
(14347, 0, 0, 0, 262806, 34511, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14347
(14336, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14336
(14321, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14321
(14320, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14320
(14313, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14313
(14222, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14222
(14222, 0, 0, 0, 264452, 35627, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14222
(14221, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14221
(14218, 7, 7, -1, 0, 0, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14218
(14218, 6, 6, 0, 264948, 35229, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14218
(14218, 5, 5, 0, 264948, 35229, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14218
(14218, 4, 4, 0, 264948, 35229, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14218
(14218, 3, 3, 0, 264948, 35229, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14218
(14218, 2, 2, 0, 264948, 35229, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14218
(14218, 1, 1, 0, 264948, 35229, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14218
(14218, 0, 0, 0, 264948, 35229, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14218
(14212, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14212
(14294, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14294
(14293, 0, 1, 0, 264587, 35753, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14293
(14293, 0, 0, -1, 0, 0, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14293
(14214, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14214
(14204, 1, 1, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14204
(14204, 0, 0, 0, 263905, 35463, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14204
(14159, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14159
(26129, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 26129
(14154, 1, 1, 30, 0, 0, 654, 545, 0, 0, 7, 0, 0, 0, 23420), -- 14154
(14154, 0, 0, -1, 0, 0, 654, 545, 0, 0, 3, 0, 0, 0, 23420), -- 14154
(28850, 0, 0, -1, 0, 0, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 28850
(24930, 2, 2, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24930
(24930, 1, 1, 0, 265434, 35118, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24930
(24930, 0, 0, 0, 265434, 35118, 638, 539, 0, 0, 1, 0, 0, 0, 23420), -- 24930
(14157, 2, 0, -1, 0, 0, 654, 611, 0, 0, 3, 0, 0, 0, 23420), -- 14157
(14288, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14288
(14277, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14277
(14099, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14099
(14094, 3, 3, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14094
(14094, 2, 2, 0, 265006, 46896, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14094
(14094, 1, 1, -1, 0, 0, 638, 539, 0, 0, 1, 0, 0, 0, 23420), -- 14094
(14094, 0, 0, 0, 265006, 46896, 638, 539, 0, 0, 1, 0, 0, 0, 23420), -- 14094
(14098, 3, 2, -1, 0, 0, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14098
(14098, 1, 1, 0, 267179, 35830, 654, 545, 0, 0, 3, 0, 0, 0, 23420), -- 14098
(14098, 0, 0, 0, 267179, 35830, 654, 545, 0, 0, 3, 0, 0, 0, 23420), -- 14098
(14093, 3, 3, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14093
(14093, 2, 2, 0, 264929, 34884, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14093
(14093, 1, 1, -1, 0, 0, 638, 539, 0, 0, 1, 0, 0, 0, 23420), -- 14093
(14093, 0, 0, 0, 264929, 34884, 638, 539, 0, 0, 3, 0, 0, 0, 23420), -- 14093
(14091, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 14091
(14078, 0, 0, -1, 0, 0, 654, 611, 0, 0, 1, 0, 0, 0, 23420), -- 14078
(13518, 4, 4, -1, 0, 0, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13518
(13518, 3, 3, 0, 267130, 33094, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13518
(13518, 2, 2, 0, 267129, 33095, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13518
(13518, 1, 1, 0, 267128, 32911, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13518
(13518, 0, 0, 0, 267127, 33093, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13518
(26385, 0, 0, -1, 0, 0, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 26385
(28517, 0, 0, -1, 0, 0, 1, 381, 0, 0, 1, 0, 0, 0, 23420), -- 28517
(26706, 1, 1, 0, 254200, 43729, 654, 545, 0, 0, 7, 0, 0, 0, 23420), -- 26706
(26706, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 26706
(24681, 2, 2, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24681
(24681, 1, 1, 0, 265366, 37921, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24681
(24681, 0, 0, 0, 265364, 37916, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24681
(24680, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420), -- 24680
(24679, 1, 1, 0, 265108, 38147, 654, 545, 0, 0, 7, 0, 0, 0, 23420), -- 24679
(24679, 0, 0, -1, 0, 0, 654, 545, 0, 0, 1, 0, 0, 0, 23420); -- 24679


DELETE FROM `quest_poi_points` WHERE (`QuestID`=24602 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=24602 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=24602 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=24602 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=24602 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=24602 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=24602 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24602 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=24602 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=24602 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=24602 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24602 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24602 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24678 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24920 AND `Idx1`=7 AND `Idx2`=0) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=11) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=10) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=9) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=8) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=7) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=6) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=5) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=4) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=3) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=2) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=1) OR (`QuestID`=24920 AND `Idx1`=6 AND `Idx2`=0) OR (`QuestID`=24920 AND `Idx1`=5 AND `Idx2`=7) OR (`QuestID`=24920 AND `Idx1`=5 AND `Idx2`=6) OR (`QuestID`=24920 AND `Idx1`=5 AND `Idx2`=5) OR (`QuestID`=24920 AND `Idx1`=5 AND `Idx2`=4) OR (`QuestID`=24920 AND `Idx1`=5 AND `Idx2`=3) OR (`QuestID`=24920 AND `Idx1`=5 AND `Idx2`=2) OR (`QuestID`=24920 AND `Idx1`=5 AND `Idx2`=1) OR (`QuestID`=24920 AND `Idx1`=5 AND `Idx2`=0) OR (`QuestID`=24920 AND `Idx1`=4 AND `Idx2`=2) OR (`QuestID`=24920 AND `Idx1`=4 AND `Idx2`=1) OR (`QuestID`=24920 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=24920 AND `Idx1`=3 AND `Idx2`=5) OR (`QuestID`=24920 AND `Idx1`=3 AND `Idx2`=4) OR (`QuestID`=24920 AND `Idx1`=3 AND `Idx2`=3) OR (`QuestID`=24920 AND `Idx1`=3 AND `Idx2`=2) OR (`QuestID`=24920 AND `Idx1`=3 AND `Idx2`=1) OR (`QuestID`=24920 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=24920 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=24920 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=24920 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=24920 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=24920 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=24920 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24920 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24920 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24920 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24903 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24902 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24904 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24904 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24676 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=24676 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=24676 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24676 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=24676 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=24676 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=24676 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=24676 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=24676 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=24676 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24676 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24676 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24674 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24674 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24575 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=24575 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=24575 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=24575 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=24575 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=24575 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24575 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=24575 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=24575 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=24575 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24575 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24575 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24675 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=24675 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=24675 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=24675 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=24675 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=24675 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=24675 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24675 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=24675 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=24675 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=24675 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=24675 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24675 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24675 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24677 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24592 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=24592 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24592 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24672 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24673 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24593 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=24593 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=24593 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24593 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24646 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24646 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24628 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24628 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=24628 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=24628 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=24628 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=24628 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=24628 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24628 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24628 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24627 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24627 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=24627 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=24627 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=24627 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=24627 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=24627 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=24627 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24627 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24627 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24617 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24616 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24616 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24578 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24501 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24501 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24495 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=24495 AND `Idx1`=2 AND `Idx2`=6) OR (`QuestID`=24495 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=24495 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=24495 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=24495 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=24495 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=24495 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=24495 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24495 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=24495 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=24495 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=24495 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=24495 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24495 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24495 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24484 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24484 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=24484 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=24484 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=24484 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=24484 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24484 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24484 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24483 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24472 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=24472 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24472 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=24472 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=24472 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=24472 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=24472 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24472 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24472 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=24468 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24468 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24438 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14466 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14465 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14402 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14401 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14401 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14400 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14400 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14399 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14399 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14412 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14412 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=14412 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=14412 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=14412 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=14412 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=14412 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=14412 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14404 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=14404 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=14404 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14404 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14416 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14416 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=14416 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=14416 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=14416 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=14416 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14406 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14403 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14398 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14397 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14395 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=14395 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=14395 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=14395 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=14395 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=14395 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=14395 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=14395 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=14395 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=14395 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14395 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14396 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14386 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14386 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14368 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=14368 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=14368 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14368 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14382 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=14382 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14382 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14369 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14369 AND `Idx1`=0 AND `Idx2`=9) OR (`QuestID`=14369 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=14369 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=14369 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=14369 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=14369 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=14369 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=14369 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=14369 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=14369 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14367 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14366 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14348 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14348 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=14348 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=14348 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=14348 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=14348 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=14348 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=14348 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=14348 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14347 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14347 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=14347 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=14347 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=14347 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=14347 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=14347 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=14347 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=14347 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=14347 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14336 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14321 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14320 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14313 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14222 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14222 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=14222 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=14222 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=14222 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=14222 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=14222 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=14222 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=14222 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=14222 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14221 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14218 AND `Idx1`=7 AND `Idx2`=0) OR (`QuestID`=14218 AND `Idx1`=6 AND `Idx2`=2) OR (`QuestID`=14218 AND `Idx1`=6 AND `Idx2`=1) OR (`QuestID`=14218 AND `Idx1`=6 AND `Idx2`=0) OR (`QuestID`=14218 AND `Idx1`=5 AND `Idx2`=3) OR (`QuestID`=14218 AND `Idx1`=5 AND `Idx2`=2) OR (`QuestID`=14218 AND `Idx1`=5 AND `Idx2`=1) OR (`QuestID`=14218 AND `Idx1`=5 AND `Idx2`=0) OR (`QuestID`=14218 AND `Idx1`=4 AND `Idx2`=3) OR (`QuestID`=14218 AND `Idx1`=4 AND `Idx2`=2) OR (`QuestID`=14218 AND `Idx1`=4 AND `Idx2`=1) OR (`QuestID`=14218 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=14218 AND `Idx1`=3 AND `Idx2`=6) OR (`QuestID`=14218 AND `Idx1`=3 AND `Idx2`=5) OR (`QuestID`=14218 AND `Idx1`=3 AND `Idx2`=4) OR (`QuestID`=14218 AND `Idx1`=3 AND `Idx2`=3) OR (`QuestID`=14218 AND `Idx1`=3 AND `Idx2`=2) OR (`QuestID`=14218 AND `Idx1`=3 AND `Idx2`=1) OR (`QuestID`=14218 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=14218 AND `Idx1`=2 AND `Idx2`=10) OR (`QuestID`=14218 AND `Idx1`=2 AND `Idx2`=9) OR (`QuestID`=14218 AND `Idx1`=2 AND `Idx2`=8) OR (`QuestID`=14218 AND `Idx1`=2 AND `Idx2`=7) OR (`QuestID`=14218 AND `Idx1`=2 AND `Idx2`=6) OR (`QuestID`=14218 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=14218 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=14218 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=14218 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=14218 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=14218 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=14218 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=14218 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=14218 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=14218 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=14218 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=14218 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=14218 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=14218 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=14218 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14218 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=14218 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=14218 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=14218 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=14218 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=14218 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=14218 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14212 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14294 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14293 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14293 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14214 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14204 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14204 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=14204 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=14204 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=14204 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=14204 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=14204 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=14204 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14159 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=26129 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14154 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14154 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=28850 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24930 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=24930 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=24930 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=24930 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=24930 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=24930 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=24930 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=24930 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=24930 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=24930 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24930 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=24930 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=24930 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=24930 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=24930 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=24930 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=24930 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24930 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24930 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14157 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14288 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14277 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14099 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14094 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=14094 AND `Idx1`=2 AND `Idx2`=8) OR (`QuestID`=14094 AND `Idx1`=2 AND `Idx2`=7) OR (`QuestID`=14094 AND `Idx1`=2 AND `Idx2`=6) OR (`QuestID`=14094 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=14094 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=14094 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=14094 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=14094 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=14094 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=14094 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14094 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=14094 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=14094 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=14094 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=14094 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=14094 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=14094 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=14094 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=14094 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14098 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=14098 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=14098 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=14098 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=14098 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=14098 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=14098 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=14098 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14098 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=14098 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=14098 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=14098 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14093 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=14093 AND `Idx1`=2 AND `Idx2`=6) OR (`QuestID`=14093 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=14093 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=14093 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=14093 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=14093 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=14093 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=14093 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=14093 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=14093 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=14093 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=14093 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=14093 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=14093 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=14093 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14091 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=14078 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13518 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=13518 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=13518 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13518 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13518 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=26385 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=28517 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=26706 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=26706 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24681 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=24681 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=24681 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=24681 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=11) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=10) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=9) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=24681 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24680 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=24679 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=24679 AND `Idx1`=0 AND `Idx2`=0);
INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(24602, 2, 0, -1728, 1872, 23420), -- 24602
(24602, 1, 5, -1583, 1977, 23420), -- 24602
(24602, 1, 4, -1542, 1963, 23420), -- 24602
(24602, 1, 3, -1552, 1902, 23420), -- 24602
(24602, 1, 2, -1568, 1858, 23420), -- 24602
(24602, 1, 1, -1616, 1832, 23420), -- 24602
(24602, 1, 0, -1671, 1830, 23420), -- 24602
(24602, 0, 5, -1715, 1928, 23420), -- 24602
(24602, 0, 4, -1709, 1970, 23420), -- 24602
(24602, 0, 3, -1656, 1999, 23420), -- 24602
(24602, 0, 2, -1652, 1967, 23420), -- 24602
(24602, 0, 1, -1657, 1942, 23420), -- 24602
(24602, 0, 0, -1672, 1911, 23420), -- 24602
(24678, 0, 0, -1728, 1872, 23420), -- 24678
(24920, 7, 0, -1645, 1645, 23420), -- 24920
(24920, 6, 11, -1225, 1453, 23420), -- 24920
(24920, 6, 10, -1278, 1506, 23420), -- 24920
(24920, 6, 9, -1299, 1531, 23420), -- 24920
(24920, 6, 8, -1349, 1773, 23420), -- 24920
(24920, 6, 7, -1291, 1777, 23420), -- 24920
(24920, 6, 6, -1060, 1722, 23420), -- 24920
(24920, 6, 5, -908, 1653, 23420), -- 24920
(24920, 6, 4, -835, 1561, 23420), -- 24920
(24920, 6, 3, -835, 1544, 23420), -- 24920
(24920, 6, 2, -886, 1465, 23420), -- 24920
(24920, 6, 1, -1051, 1402, 23420), -- 24920
(24920, 6, 0, -1053, 1402, 23420), -- 24920
(24920, 5, 7, -1303, 1748, 23420), -- 24920
(24920, 5, 6, -1194, 1728, 23420), -- 24920
(24920, 5, 5, -1181, 1719, 23420), -- 24920
(24920, 5, 4, -1166, 1707, 23420), -- 24920
(24920, 5, 3, -1142, 1686, 23420), -- 24920
(24920, 5, 2, -1112, 1624, 23420), -- 24920
(24920, 5, 1, -1120, 1550, 23420), -- 24920
(24920, 5, 0, -1273, 1482, 23420), -- 24920
(24920, 4, 2, -824, 1562, 23420), -- 24920
(24920, 4, 1, -816, 1552, 23420), -- 24920
(24920, 4, 0, -818, 1540, 23420), -- 24920
(24920, 3, 5, -1111, 1472, 23420), -- 24920
(24920, 3, 4, -1115, 1479, 23420), -- 24920
(24920, 3, 3, -1078, 1486, 23420), -- 24920
(24920, 3, 2, -1003, 1489, 23420), -- 24920
(24920, 3, 1, -939, 1449, 23420), -- 24920
(24920, 3, 0, -1060, 1443, 23420), -- 24920
(24920, 2, 0, -1645, 1645, 23420), -- 24920
(24920, 1, 4, -1320, 1734, 23420), -- 24920
(24920, 1, 3, -1326, 1749, 23420), -- 24920
(24920, 1, 2, -1316, 1743, 23420), -- 24920
(24920, 1, 1, -1311, 1729, 23420), -- 24920
(24920, 1, 0, -1313, 1718, 23420), -- 24920
(24920, 0, 2, -1349, 1773, 23420), -- 24920
(24920, 0, 1, -1339, 1773, 23420), -- 24920
(24920, 0, 0, -1341, 1764, 23420), -- 24920
(24903, 0, 0, -1645, 1645, 23420), -- 24903
(24902, 0, 0, -1664, 1590, 23420), -- 24902
(24904, 1, 0, -1678, 1612, 23420), -- 24904
(24904, 0, 0, -1664, 1590, 23420), -- 24904
(24676, 3, 0, -1369, 1210, 23420), -- 24676
(24676, 2, 0, -1108, 1107, 23420), -- 24676
(24676, 1, 0, -1129, 1148, 23420), -- 24676
(24676, 0, 8, -1120, 1083, 23420), -- 24676
(24676, 0, 7, -1183, 1127, 23420), -- 24676
(24676, 0, 6, -1187, 1180, 23420), -- 24676
(24676, 0, 5, -1176, 1181, 23420), -- 24676
(24676, 0, 4, -1153, 1177, 23420), -- 24676
(24676, 0, 3, -1136, 1167, 23420), -- 24676
(24676, 0, 2, -1115, 1139, 23420), -- 24676
(24676, 0, 1, -1104, 1089, 23420), -- 24676
(24676, 0, 0, -1102, 1075, 23420), -- 24676
(24674, 1, 0, -1373, 1230, 23420), -- 24674
(24674, 0, 0, -1207, 912, 23420), -- 24674
(24575, 2, 0, -1369, 1210, 23420), -- 24575
(24575, 1, 4, -1136, 855, 23420), -- 24575
(24575, 1, 3, -1202, 890, 23420), -- 24575
(24575, 1, 2, -1204, 1089, 23420), -- 24575
(24575, 1, 1, -1024, 988, 23420), -- 24575
(24575, 1, 0, -1072, 841, 23420), -- 24575
(24575, 0, 5, -1184, 952, 23420), -- 24575
(24575, 0, 4, -1187, 1058, 23420), -- 24575
(24575, 0, 3, -1019, 997, 23420), -- 24575
(24575, 0, 2, -1016, 981, 23420), -- 24575
(24575, 0, 1, -1064, 850, 23420), -- 24575
(24575, 0, 0, -1097, 836, 23420), -- 24575
(24675, 2, 0, -1366, 1217, 23420), -- 24675
(24675, 1, 5, -1582, 1016, 23420), -- 24675
(24675, 1, 4, -1603, 1080, 23420), -- 24675
(24675, 1, 3, -1366, 1055, 23420), -- 24675
(24675, 1, 2, -1328, 1044, 23420), -- 24675
(24675, 1, 1, -1285, 989, 23420), -- 24675
(24675, 1, 0, -1551, 950, 23420), -- 24675
(24675, 0, 6, -1553, 1145, 23420), -- 24675
(24675, 0, 5, -1579, 1186, 23420), -- 24675
(24675, 0, 4, -1540, 1191, 23420), -- 24675
(24675, 0, 3, -1446, 1177, 23420), -- 24675
(24675, 0, 2, -1431, 1143, 23420), -- 24675
(24675, 0, 1, -1474, 1132, 23420), -- 24675
(24675, 0, 0, -1520, 1132, 23420), -- 24675
(24677, 0, 0, -1369, 1210, 23420), -- 24677
(24592, 2, 0, -2045, 977, 23420), -- 24592
(24592, 1, 0, -1922, 985, 23420), -- 24592
(24592, 0, 0, -2103, 791, 23420), -- 24592
(24672, 0, 0, -2213, 1152, 23420), -- 24672
(24673, 0, 0, -2457, 1556, 23420), -- 24673
(24593, 3, 0, -2070, 1278, 23420), -- 24593
(24593, 2, 0, -2076, 1265, 23420), -- 24593
(24593, 1, 0, -2067, 1261, 23420), -- 24593
(24593, 0, 0, -2061, 1270, 23420), -- 24593
(24646, 1, 0, -2070, 1278, 23420), -- 24646
(24646, 0, 0, -2119, 1630, 23420), -- 24646
(24628, 1, 0, -2064, 1259, 23420), -- 24628
(24628, 0, 7, -2302, 1586, 23420), -- 24628
(24628, 0, 6, -2297, 1631, 23420), -- 24628
(24628, 0, 5, -2209, 1693, 23420), -- 24628
(24628, 0, 4, -1999, 1571, 23420), -- 24628
(24628, 0, 3, -2004, 1474, 23420), -- 24628
(24628, 0, 2, -2012, 1382, 23420), -- 24628
(24628, 0, 1, -2042, 1330, 23420), -- 24628
(24628, 0, 0, -2127, 1326, 23420), -- 24628
(24627, 1, 0, -2070, 1278, 23420), -- 24627
(24627, 0, 8, -2314, 1622, 23420), -- 24627
(24627, 0, 7, -2314, 1661, 23420), -- 24627
(24627, 0, 6, -2313, 1688, 23420), -- 24627
(24627, 0, 5, -2195, 1682, 23420), -- 24627
(24627, 0, 4, -2075, 1583, 23420), -- 24627
(24627, 0, 3, -2018, 1514, 23420), -- 24627
(24627, 0, 2, -2019, 1449, 23420), -- 24627
(24627, 0, 1, -2056, 1417, 23420), -- 24627
(24627, 0, 0, -2118, 1416, 23420), -- 24627
(24617, 0, 0, -2070, 1278, 23420), -- 24617
(24616, 1, 0, -2238, 1429, 23420), -- 24616
(24616, 0, 0, -2273, 1447, 23420), -- 24616
(24578, 0, 0, -2273, 1447, 23420), -- 24578
(24501, 1, 0, -2457, 1556, 23420), -- 24501
(24501, 0, 0, -2246, 1289, 23420), -- 24501
(24495, 3, 0, -2460, 1544, 23420), -- 24495
(24495, 2, 6, -2469, 1320, 23420), -- 24495
(24495, 2, 5, -2481, 1479, 23420), -- 24495
(24495, 2, 4, -2436, 1512, 23420), -- 24495
(24495, 2, 3, -2418, 1475, 23420), -- 24495
(24495, 2, 2, -2405, 1380, 23420), -- 24495
(24495, 2, 1, -2406, 1330, 23420), -- 24495
(24495, 2, 0, -2451, 1319, 23420), -- 24495
(24495, 1, 0, -2460, 1544, 23420), -- 24495
(24495, 0, 6, -2469, 1320, 23420), -- 24495
(24495, 0, 5, -2481, 1479, 23420), -- 24495
(24495, 0, 4, -2436, 1512, 23420), -- 24495
(24495, 0, 3, -2418, 1475, 23420), -- 24495
(24495, 0, 2, -2405, 1380, 23420), -- 24495
(24495, 0, 1, -2406, 1330, 23420), -- 24495
(24495, 0, 0, -2451, 1319, 23420), -- 24495
(24484, 1, 0, -2457, 1556, 23420), -- 24484
(24484, 0, 6, -2447, 1309, 23420), -- 24484
(24484, 0, 5, -2466, 1310, 23420), -- 24484
(24484, 0, 4, -2474, 1346, 23420), -- 24484
(24484, 0, 3, -2481, 1387, 23420), -- 24484
(24484, 0, 2, -2440, 1464, 23420), -- 24484
(24484, 0, 1, -2404, 1444, 23420), -- 24484
(24484, 0, 0, -2294, 1302, 23420), -- 24484
(24483, 0, 0, -2457, 1556, 23420), -- 24483
(24472, 2, 0, -2222, 1810, 23420), -- 24472
(24472, 1, 0, -2278, 1969, 23420), -- 24472
(24472, 0, 6, -2394, 1791, 23420), -- 24472
(24472, 0, 5, -2394, 1992, 23420), -- 24472
(24472, 0, 4, -2359, 1993, 23420), -- 24472
(24472, 0, 3, -2310, 1970, 23420), -- 24472
(24472, 0, 2, -2266, 1829, 23420), -- 24472
(24472, 0, 1, -2318, 1789, 23420), -- 24472
(24472, 0, 0, -2351, 1784, 23420), -- 24472
(24468, 1, 11, -2091, 1700, 23420), -- 24468
(24468, 1, 10, -2114, 1752, 23420), -- 24468
(24468, 1, 9, -2114, 1780, 23420), -- 24468
(24468, 1, 8, -2101, 1817, 23420), -- 24468
(24468, 1, 7, -2070, 1861, 23420), -- 24468
(24468, 1, 6, -2015, 1877, 23420), -- 24468
(24468, 1, 5, -1966, 1856, 23420), -- 24468
(24468, 1, 4, -1921, 1801, 23420), -- 24468
(24468, 1, 3, -1913, 1759, 23420), -- 24468
(24468, 1, 2, -1958, 1715, 23420), -- 24468
(24468, 1, 1, -1992, 1694, 23420), -- 24468
(24468, 1, 0, -2057, 1692, 23420), -- 24468
(24468, 0, 0, -2222, 1810, 23420), -- 24468
(24438, 0, 0, -2222, 1810, 23420), -- 24438
(14466, 0, 0, -1518, 2608, 23420), -- 14466
(14465, 0, 0, -1583, 2555, 23420), -- 14465
(14402, 0, 0, -1902, 2256, 23420), -- 14402
(14401, 1, 0, -2143, 2427, 23420), -- 14401
(14401, 0, 0, -2112, 2330, 23420), -- 14401
(14400, 1, 0, -2115, 2434, 23420), -- 14400
(14400, 0, 0, -2116, 2432, 23420), -- 14400
(14399, 1, 0, -2156, 2372, 23420), -- 14399
(14399, 0, 0, -2157, 2371, 23420), -- 14399
(14412, 1, 0, -2300, 2279, 23420), -- 14412
(14412, 0, 6, -2359, 2251, 23420), -- 14412
(14412, 0, 5, -2376, 2284, 23420), -- 14412
(14412, 0, 4, -2367, 2297, 23420), -- 14412
(14412, 0, 3, -2326, 2333, 23420), -- 14412
(14412, 0, 2, -2318, 2320, 23420), -- 14412
(14412, 0, 1, -2318, 2254, 23420), -- 14412
(14412, 0, 0, -2324, 2247, 23420), -- 14412
(14404, 3, 0, -2300, 2279, 23420), -- 14404
(14404, 2, 0, -2337, 2257, 23420), -- 14404
(14404, 1, 0, -2347, 2304, 23420), -- 14404
(14404, 0, 0, -2362, 2261, 23420), -- 14404
(14416, 1, 0, -2060, 2254, 23420), -- 14416
(14416, 0, 4, -2270, 2167, 23420), -- 14416
(14416, 0, 3, -2253, 2238, 23420), -- 14416
(14416, 0, 2, -2114, 2186, 23420), -- 14416
(14416, 0, 1, -2084, 2170, 23420), -- 14416
(14416, 0, 0, -2180, 2161, 23420), -- 14416
(14406, 0, 0, -2060, 2254, 23420), -- 14406
(14403, 0, 0, -2300, 2279, 23420), -- 14403
(14398, 0, 0, -2143, 2432, 23420), -- 14398
(14397, 0, 0, -1902, 2256, 23420), -- 14397
(14395, 1, 9, -2114, 2506, 23420), -- 14395
(14395, 1, 8, -2126, 2509, 23420), -- 14395
(14395, 1, 7, -2090, 2613, 23420), -- 14395
(14395, 1, 6, -2022, 2706, 23420), -- 14395
(14395, 1, 5, -1991, 2708, 23420), -- 14395
(14395, 1, 4, -1902, 2673, 23420), -- 14395
(14395, 1, 3, -1905, 2588, 23420), -- 14395
(14395, 1, 2, -1918, 2561, 23420), -- 14395
(14395, 1, 1, -1932, 2542, 23420), -- 14395
(14395, 1, 0, -1966, 2497, 23420), -- 14395
(14395, 0, 0, -1898, 2526, 23420), -- 14395
(14396, 0, 0, -1898, 2526, 23420), -- 14396
(14386, 1, 0, -1844, 2567, 23420), -- 14386
(14386, 0, 0, -1950, 2701, 23420), -- 14386
(14368, 3, 0, -1843, 2564, 23420), -- 14368
(14368, 2, 0, -1935, 2541, 23420), -- 14368
(14368, 1, 0, -1932, 2562, 23420), -- 14368
(14368, 0, 0, -1987, 2509, 23420), -- 14368
(14382, 2, 0, -1844, 2567, 23420), -- 14382
(14382, 1, 0, -2197, 2595, 23420), -- 14382
(14382, 0, 0, -2101, 2692, 23420), -- 14382
(14369, 1, 0, -1844, 2567, 23420), -- 14369
(14369, 0, 9, -2211, 2475, 23420), -- 14369
(14369, 0, 8, -2243, 2554, 23420), -- 14369
(14369, 0, 7, -2115, 2683, 23420), -- 14369
(14369, 0, 6, -2103, 2686, 23420), -- 14369
(14369, 0, 5, -1958, 2711, 23420), -- 14369
(14369, 0, 4, -1943, 2694, 23420), -- 14369
(14369, 0, 3, -1933, 2563, 23420), -- 14369
(14369, 0, 2, -1978, 2528, 23420), -- 14369
(14369, 0, 1, -2058, 2470, 23420), -- 14369
(14369, 0, 0, -2179, 2462, 23420), -- 14369
(14367, 0, 0, -1844, 2567, 23420), -- 14367
(14366, 0, 0, -1865, 2267, 23420), -- 14366
(14348, 1, 0, -1921, 2309, 23420), -- 14348
(14348, 0, 7, -2008, 2288, 23420), -- 14348
(14348, 0, 6, -2002, 2319, 23420); -- 14348


INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(14348, 0, 5, -1979, 2383, 23420), -- 14348
(14348, 0, 4, -1942, 2404, 23420), -- 14348
(14348, 0, 3, -1932, 2407, 23420), -- 14348
(14348, 0, 2, -1898, 2384, 23420), -- 14348
(14348, 0, 1, -1904, 2313, 23420), -- 14348
(14348, 0, 0, -1932, 2246, 23420), -- 14348
(14347, 1, 0, -1921, 2309, 23420), -- 14347
(14347, 0, 8, -2065, 2291, 23420), -- 14347
(14347, 0, 7, -2068, 2298, 23420), -- 14347
(14347, 0, 6, -2078, 2340, 23420), -- 14347
(14347, 0, 5, -2050, 2414, 23420), -- 14347
(14347, 0, 4, -2034, 2422, 23420), -- 14347
(14347, 0, 3, -1917, 2406, 23420), -- 14347
(14347, 0, 2, -1906, 2356, 23420), -- 14347
(14347, 0, 1, -1981, 2210, 23420), -- 14347
(14347, 0, 0, -2019, 2208, 23420), -- 14347
(14336, 0, 0, -1921, 2309, 23420), -- 14336
(14321, 0, 0, -1865, 2267, 23420), -- 14321
(14320, 0, 0, -1926, 2409, 23420), -- 14320
(14313, 0, 0, -1863, 2266, 23420), -- 14313
(14222, 1, 0, -1620, 1498, 23420), -- 14222
(14222, 0, 8, -1614, 1515, 23420), -- 14222
(14222, 0, 7, -1610, 1527, 23420), -- 14222
(14222, 0, 6, -1603, 1534, 23420), -- 14222
(14222, 0, 5, -1559, 1568, 23420), -- 14222
(14222, 0, 4, -1555, 1569, 23420), -- 14222
(14222, 0, 3, -1550, 1564, 23420), -- 14222
(14222, 0, 2, -1546, 1560, 23420), -- 14222
(14222, 0, 1, -1586, 1508, 23420), -- 14222
(14222, 0, 0, -1602, 1505, 23420), -- 14222
(14221, 0, 0, -1620, 1498, 23420), -- 14221
(14218, 7, 0, -1540, 1571, 23420), -- 14218
(14218, 6, 2, -1712, 1684, 23420), -- 14218
(14218, 6, 1, -1697, 1692, 23420), -- 14218
(14218, 6, 0, -1710, 1671, 23420), -- 14218
(14218, 5, 3, -1745, 1590, 23420), -- 14218
(14218, 5, 2, -1733, 1596, 23420), -- 14218
(14218, 5, 1, -1716, 1591, 23420), -- 14218
(14218, 5, 0, -1748, 1570, 23420), -- 14218
(14218, 4, 3, -1674, 1672, 23420), -- 14218
(14218, 4, 2, -1675, 1680, 23420), -- 14218
(14218, 4, 1, -1657, 1671, 23420), -- 14218
(14218, 4, 0, -1665, 1671, 23420), -- 14218
(14218, 3, 6, -1697, 1598, 23420), -- 14218
(14218, 3, 5, -1702, 1611, 23420), -- 14218
(14218, 3, 4, -1703, 1625, 23420), -- 14218
(14218, 3, 3, -1673, 1637, 23420), -- 14218
(14218, 3, 2, -1663, 1641, 23420), -- 14218
(14218, 3, 1, -1666, 1626, 23420), -- 14218
(14218, 3, 0, -1680, 1580, 23420), -- 14218
(14218, 2, 10, -1465, 1515, 23420), -- 14218
(14218, 2, 9, -1480, 1522, 23420), -- 14218
(14218, 2, 8, -1492, 1552, 23420), -- 14218
(14218, 2, 7, -1478, 1571, 23420), -- 14218
(14218, 2, 6, -1470, 1581, 23420), -- 14218
(14218, 2, 5, -1453, 1594, 23420), -- 14218
(14218, 2, 4, -1446, 1597, 23420), -- 14218
(14218, 2, 3, -1419, 1585, 23420), -- 14218
(14218, 2, 2, -1412, 1573, 23420), -- 14218
(14218, 2, 1, -1407, 1526, 23420), -- 14218
(14218, 2, 0, -1432, 1514, 23420), -- 14218
(14218, 1, 8, -1581, 1623, 23420), -- 14218
(14218, 1, 7, -1592, 1639, 23420), -- 14218
(14218, 1, 6, -1580, 1675, 23420), -- 14218
(14218, 1, 5, -1576, 1682, 23420), -- 14218
(14218, 1, 4, -1554, 1708, 23420), -- 14218
(14218, 1, 3, -1542, 1697, 23420), -- 14218
(14218, 1, 2, -1515, 1665, 23420), -- 14218
(14218, 1, 1, -1555, 1626, 23420), -- 14218
(14218, 1, 0, -1573, 1618, 23420), -- 14218
(14218, 0, 6, -1515, 1621, 23420), -- 14218
(14218, 0, 5, -1507, 1690, 23420), -- 14218
(14218, 0, 4, -1445, 1682, 23420), -- 14218
(14218, 0, 3, -1436, 1675, 23420), -- 14218
(14218, 0, 2, -1432, 1670, 23420), -- 14218
(14218, 0, 1, -1425, 1610, 23420), -- 14218
(14218, 0, 0, -1485, 1593, 23420), -- 14218
(14212, 0, 0, -1540, 1571, 23420), -- 14212
(14294, 0, 0, -1741, 1663, 23420), -- 14294
(14293, 1, 0, -1673, 1345, 23420), -- 14293
(14293, 0, 0, -1786, 1438, 23420), -- 14293
(14214, 0, 0, -1804, 1407, 23420), -- 14214
(14204, 1, 0, -1790, 1427, 23420), -- 14204
(14204, 0, 6, -1807, 1446, 23420), -- 14204
(14204, 0, 5, -1808, 1529, 23420), -- 14204
(14204, 0, 4, -1742, 1513, 23420), -- 14204
(14204, 0, 3, -1730, 1506, 23420), -- 14204
(14204, 0, 2, -1716, 1482, 23420), -- 14204
(14204, 0, 1, -1729, 1462, 23420), -- 14204
(14204, 0, 0, -1756, 1438, 23420), -- 14204
(14159, 0, 0, -1814, 1428, 23420), -- 14159
(26129, 0, 0, -1768, 1348, 23420), -- 26129
(14154, 1, 0, -1680, 1437, 23420), -- 14154
(14154, 0, 0, -1680, 1442, 23420), -- 14154
(28850, 0, 0, -1680, 1442, 23420), -- 28850
(24930, 2, 0, -1767, 1353, 23420), -- 24930
(24930, 1, 8, -1723, 1354, 23420), -- 24930
(24930, 1, 7, -1764, 1398, 23420), -- 24930
(24930, 1, 6, -1795, 1466, 23420), -- 24930
(24930, 1, 5, -1747, 1490, 23420), -- 24930
(24930, 1, 4, -1682, 1415, 23420), -- 24930
(24930, 1, 3, -1660, 1388, 23420), -- 24930
(24930, 1, 2, -1654, 1379, 23420), -- 24930
(24930, 1, 1, -1662, 1344, 23420), -- 24930
(24930, 1, 0, -1680, 1337, 23420), -- 24930
(24930, 0, 8, -1723, 1354, 23420), -- 24930
(24930, 0, 7, -1764, 1398, 23420), -- 24930
(24930, 0, 6, -1795, 1466, 23420), -- 24930
(24930, 0, 5, -1747, 1490, 23420), -- 24930
(24930, 0, 4, -1682, 1415, 23420), -- 24930
(24930, 0, 3, -1660, 1388, 23420), -- 24930
(24930, 0, 2, -1654, 1379, 23420), -- 24930
(24930, 0, 1, -1662, 1344, 23420), -- 24930
(24930, 0, 0, -1680, 1337, 23420), -- 24930
(14157, 0, 0, -1749, 1426, 23420), -- 14157
(14288, 0, 0, -1768, 1348, 23420), -- 14288
(14277, 0, 0, -1690, 1328, 23420), -- 14277
(14099, 0, 0, -1633, 1304, 23420), -- 14099
(14094, 3, 0, -1465, 1404, 23420), -- 14094
(14094, 2, 8, -1574, 1401, 23420), -- 14094
(14094, 2, 7, -1561, 1433, 23420), -- 14094
(14094, 2, 6, -1524, 1442, 23420), -- 14094
(14094, 2, 5, -1441, 1441, 23420), -- 14094
(14094, 2, 4, -1419, 1429, 23420), -- 14094
(14094, 2, 3, -1430, 1399, 23420), -- 14094
(14094, 2, 2, -1466, 1362, 23420), -- 14094
(14094, 2, 1, -1510, 1328, 23420), -- 14094
(14094, 2, 0, -1549, 1305, 23420), -- 14094
(14094, 1, 0, -1465, 1404, 23420), -- 14094
(14094, 0, 8, -1574, 1401, 23420), -- 14094
(14094, 0, 7, -1561, 1433, 23420), -- 14094
(14094, 0, 6, -1524, 1442, 23420), -- 14094
(14094, 0, 5, -1441, 1441, 23420), -- 14094
(14094, 0, 4, -1419, 1429, 23420), -- 14094
(14094, 0, 3, -1430, 1399, 23420), -- 14094
(14094, 0, 2, -1466, 1362, 23420), -- 14094
(14094, 0, 1, -1510, 1328, 23420), -- 14094
(14094, 0, 0, -1549, 1305, 23420), -- 14094
(14098, 2, 0, -1438, 1401, 23420), -- 14098
(14098, 1, 6, -1572, 1336, 23420), -- 14098
(14098, 1, 5, -1583, 1363, 23420), -- 14098
(14098, 1, 4, -1566, 1431, 23420), -- 14098
(14098, 1, 3, -1538, 1439, 23420), -- 14098
(14098, 1, 2, -1499, 1371, 23420), -- 14098
(14098, 1, 1, -1487, 1338, 23420), -- 14098
(14098, 1, 0, -1552, 1309, 23420), -- 14098
(14098, 0, 3, -1460, 1416, 23420), -- 14098
(14098, 0, 2, -1464, 1440, 23420), -- 14098
(14098, 0, 1, -1425, 1437, 23420), -- 14098
(14098, 0, 0, -1399, 1404, 23420), -- 14098
(14093, 3, 0, -1438, 1401, 23420), -- 14093
(14093, 2, 6, -1595, 1336, 23420), -- 14093
(14093, 2, 5, -1572, 1441, 23420), -- 14093
(14093, 2, 4, -1523, 1459, 23420), -- 14093
(14093, 2, 3, -1442, 1451, 23420), -- 14093
(14093, 2, 2, -1376, 1427, 23420), -- 14093
(14093, 2, 1, -1371, 1347, 23420), -- 14093
(14093, 2, 0, -1477, 1323, 23420), -- 14093
(14093, 1, 0, -1438, 1401, 23420), -- 14093
(14093, 0, 6, -1575, 1345, 23420), -- 14093
(14093, 0, 5, -1572, 1441, 23420), -- 14093
(14093, 0, 4, -1523, 1459, 23420), -- 14093
(14093, 0, 3, -1442, 1451, 23420), -- 14093
(14093, 0, 2, -1376, 1427, 23420), -- 14093
(14093, 0, 1, -1371, 1347, 23420), -- 14093
(14093, 0, 0, -1477, 1323, 23420), -- 14093
(14091, 0, 0, -1438, 1401, 23420), -- 14091
(14078, 0, 0, -1405, 1447, 23420), -- 14078
(13518, 4, 0, 7420, -283, 23420), -- 13518
(13518, 3, 0, 7439, 106, 23420), -- 13518
(13518, 2, 0, 7296, 243, 23420), -- 13518
(13518, 1, 0, 7456, 165, 23420), -- 13518
(13518, 0, 0, 7366, 135, 23420), -- 13518
(26385, 0, 0, 7414, -261, 23420), -- 26385
(28517, 0, 0, 10316, 2447, 23420), -- 28517
(26706, 1, 0, -1274, 2125, 23420), -- 26706
(26706, 0, 0, -1299, 2129, 23420), -- 26706
(24681, 2, 0, -1322, 2120, 23420), -- 24681
(24681, 1, 2, -1222, 2610, 23420), -- 24681
(24681, 1, 1, -1193, 2604, 23420), -- 24681
(24681, 1, 0, -1270, 2588, 23420), -- 24681
(24681, 0, 11, -1418, 2315, 23420), -- 24681
(24681, 0, 10, -1422, 2408, 23420), -- 24681
(24681, 0, 9, -1399, 2515, 23420), -- 24681
(24681, 0, 8, -1369, 2565, 23420), -- 24681
(24681, 0, 7, -1294, 2629, 23420), -- 24681
(24681, 0, 6, -1146, 2615, 23420), -- 24681
(24681, 0, 5, -1065, 2596, 23420), -- 24681
(24681, 0, 4, -983, 2534, 23420), -- 24681
(24681, 0, 3, -944, 2461, 23420), -- 24681
(24681, 0, 2, -963, 2440, 23420), -- 24681
(24681, 0, 1, -1070, 2382, 23420), -- 24681
(24681, 0, 0, -1259, 2308, 23420), -- 24681
(24680, 0, 0, -1322, 2120, 23420), -- 24680
(24679, 1, 0, -1627, 1914, 23420), -- 24679
(24679, 0, 0, -1728, 1872, 23420); -- 24679


DELETE FROM `quest_details` WHERE `ID` IN (24602 /*24602*/, 24678 /*24678*/, 24920 /*24920*/, 24903 /*24903*/, 24902 /*24902*/, 24904 /*24904*/, 24676 /*24676*/, 24674 /*24674*/, 24575 /*24575*/, 24675 /*24675*/, 24677 /*24677*/, 24592 /*24592*/, 24672 /*24672*/, 24673 /*24673*/, 24593 /*24593*/, 24646 /*24646*/, 24628 /*24628*/, 24627 /*24627*/, 24617 /*24617*/, 24616 /*24616*/, 24578 /*24578*/, 24501 /*24501*/, 24495 /*24495*/, 24484 /*24484*/, 24483 /*24483*/, 24472 /*24472*/, 24468 /*24468*/, 24438 /*24438*/, 14466 /*14466*/, 14465 /*14465*/, 14402 /*14402*/, 14401 /*14401*/, 14400 /*14400*/, 14399 /*14399*/, 14412 /*14412*/, 14404 /*14404*/, 14416 /*14416*/, 14406 /*14406*/, 14403 /*14403*/, 14398 /*14398*/, 14397 /*14397*/, 14395 /*14395*/, 14396 /*14396*/, 14386 /*14386*/, 14368 /*14368*/, 14382 /*14382*/, 14369 /*14369*/, 14367 /*14367*/, 14366 /*14366*/, 14348 /*14348*/, 14347 /*14347*/, 14336 /*14336*/, 14321 /*14321*/, 14320 /*14320*/, 14313 /*14313*/, 14222 /*14222*/, 14221 /*14221*/, 14218 /*14218*/, 14212 /*14212*/, 14294 /*14294*/, 14293 /*14293*/, 14214 /*14214*/, 14204 /*14204*/, 14159 /*14159*/, 26129 /*26129*/, 14154 /*14154*/, 28850 /*28850*/, 24930 /*24930*/, 14157 /*14157*/, 14288 /*14288*/, 14277 /*14277*/, 14099 /*14099*/, 14094 /*14094*/, 14098 /*14098*/, 14093 /*14093*/, 14091 /*14091*/, 14078 /*14078*/, 13522 /*13522*/, 13518 /*13518*/, 26385 /*26385*/, 28517 /*28517*/, 26706 /*26706*/, 24681 /*24681*/, 24680 /*24680*/, 24679 /*24679*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(24602, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24602
(24678, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24678
(24920, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24920
(24903, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24903
(24902, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24902
(24904, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24904
(24676, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24676
(24674, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24674
(24575, 1, 274, 0, 0, 0, 0, 0, 0, 23420), -- 24575
(24675, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24675
(24677, 274, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24677
(24592, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24592
(24672, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24672
(24673, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24673
(24593, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24593
(24646, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24646
(24628, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24628
(24627, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24627
(24617, 25, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24617
(24616, 1, 25, 0, 0, 0, 0, 0, 0, 23420), -- 24616
(24578, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24578
(24501, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24501
(24495, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24495
(24484, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24484
(24483, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24483
(24472, 1, 6, 5, 0, 0, 0, 0, 0, 23420), -- 24472
(24468, 1, 25, 0, 0, 0, 0, 0, 0, 23420), -- 24468
(24438, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24438
(14466, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14466
(14465, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14465
(14402, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14402
(14401, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14401
(14400, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14400
(14399, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14399
(14412, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14412
(14404, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14404
(14416, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14416
(14406, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14406
(14403, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14403
(14398, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14398
(14397, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14397
(14395, 25, 6, 1, 0, 0, 0, 0, 0, 23420), -- 14395
(14396, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14396
(14386, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14386
(14368, 18, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14368
(14382, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14382
(14369, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14369
(14367, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14367
(14366, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14366
(14348, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14348
(14347, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14347
(14336, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14336
(14321, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14321
(14320, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14320
(14313, 1, 5, 274, 0, 0, 0, 0, 0, 23420), -- 14313
(14222, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14222
(14221, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14221
(14218, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14218
(14212, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14212
(14294, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14294
(14293, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14293
(14214, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14214
(14204, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14204
(14159, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14159
(26129, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 26129
(14154, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14154
(28850, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 28850
(24930, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24930
(14157, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14157
(14288, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14288
(14277, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14277
(14099, 25, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14099
(14094, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14094
(14098, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14098
(14093, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14093
(14091, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14091
(14078, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14078
(13522, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13522
(13518, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13518
(26385, 5, 2, 0, 0, 0, 60, 0, 0, 23420), -- 26385
(28517, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 28517
(26706, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 26706
(24681, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24681
(24680, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 24680
(24679, 0, 0, 0, 0, 0, 0, 0, 0, 23420); -- 24679


DELETE FROM `npc_vendor` WHERE (`entry`=42853 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42853 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42953 AND `item`=8952 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42953 AND `item`=4599 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42953 AND `item`=3771 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42953 AND `item`=3770 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42953 AND `item`=2287 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=42953 AND `item`=117 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43558 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38853 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36779 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36717 AND `item`=2493 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36717 AND `item`=2490 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36717 AND `item`=2488 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36717 AND `item`=2495 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36717 AND `item`=2494 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36717 AND `item`=2491 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36717 AND `item`=2492 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36717 AND `item`=2489 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=36695 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=68993 AND `item`=37460 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=30817 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=38783 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `ExtendedCost`, `type`, `PlayerConditionID`, `IgnoreFiltering`, `VerifiedBuild`) VALUES
(42853, 25, 39505, 0, 0, 1, 0, 0, 23420), -- 39505
(42853, 24, 20815, 0, 0, 1, 0, 0, 23420), -- 20815
(42853, 23, 39354, 0, 0, 1, 0, 0, 23420), -- 39354
(42853, 22, 6260, 0, 0, 1, 0, 0, 23420), -- 6260
(42853, 21, 2324, 0, 0, 1, 0, 0, 23420), -- 2324
(42853, 20, 2604, 0, 0, 1, 0, 0, 23420), -- 2604
(42853, 19, 6529, 0, 0, 1, 0, 0, 23420), -- 6529
(42853, 18, 4289, 0, 0, 1, 0, 0, 23420), -- 4289
(42853, 17, 3371, 0, 0, 1, 0, 0, 23420), -- 3371
(42853, 16, 2880, 0, 0, 1, 0, 0, 23420), -- 2880
(42853, 15, 2320, 0, 0, 1, 0, 0, 23420), -- 2320
(42853, 14, 30817, 0, 0, 1, 0, 0, 23420), -- 30817
(42853, 13, 2678, 0, 0, 1, 0, 0, 23420), -- 2678
(42853, 12, 6217, 0, 0, 1, 0, 0, 23420), -- 6217
(42853, 11, 6256, 0, 0, 1, 0, 0, 23420), -- 6256
(42853, 10, 5956, 0, 0, 1, 0, 0, 23420), -- 5956
(42853, 9, 85663, 0, 0, 1, 0, 0, 23420), -- 85663
(42853, 8, 2901, 0, 0, 1, 0, 0, 23420), -- 2901
(42853, 7, 7005, 0, 0, 1, 0, 0, 23420), -- 7005
(42853, 6, 5042, 0, 0, 1, 0, 0, 23420), -- 5042
(42853, 5, 4470, 0, 0, 1, 0, 0, 23420), -- 4470
(42853, 4, 4498, 0, 0, 1, 0, 0, 23420), -- 4498
(42853, 3, 4496, 0, 0, 1, 0, 0, 23420), -- 4496
(42853, 2, 4540, 0, 0, 1, 0, 0, 23420), -- 4540
(42853, 1, 159, 0, 0, 1, 0, 0, 23420), -- 159
(42953, 6, 8952, 0, 0, 1, 0, 0, 23420), -- 8952
(42953, 5, 4599, 0, 0, 1, 0, 0, 23420), -- 4599
(42953, 4, 3771, 0, 0, 1, 0, 0, 23420), -- 3771
(42953, 3, 3770, 0, 0, 1, 0, 0, 23420), -- 3770
(42953, 2, 2287, 0, 0, 1, 0, 0, 23420), -- 2287
(42953, 1, 117, 0, 0, 1, 0, 0, 23420), -- 117
(43558, 25, 39505, 0, 0, 1, 0, 0, 23420), -- 39505
(43558, 24, 20815, 0, 0, 1, 0, 0, 23420), -- 20815
(43558, 23, 39354, 0, 0, 1, 0, 0, 23420), -- 39354
(43558, 22, 6260, 0, 0, 1, 0, 0, 23420), -- 6260
(43558, 21, 2324, 0, 0, 1, 0, 0, 23420), -- 2324
(43558, 20, 2604, 0, 0, 1, 0, 0, 23420), -- 2604
(43558, 19, 6529, 0, 0, 1, 0, 0, 23420), -- 6529
(43558, 18, 4289, 0, 0, 1, 0, 0, 23420), -- 4289
(43558, 17, 3371, 0, 0, 1, 0, 0, 23420), -- 3371
(43558, 16, 2880, 0, 0, 1, 0, 0, 23420), -- 2880
(43558, 15, 2320, 0, 0, 1, 0, 0, 23420), -- 2320
(43558, 14, 30817, 0, 0, 1, 0, 0, 23420), -- 30817
(43558, 13, 2678, 0, 0, 1, 0, 0, 23420), -- 2678
(43558, 12, 6217, 0, 0, 1, 0, 0, 23420), -- 6217
(43558, 11, 6256, 0, 0, 1, 0, 0, 23420), -- 6256
(43558, 10, 5956, 0, 0, 1, 0, 0, 23420), -- 5956
(43558, 9, 85663, 0, 0, 1, 0, 0, 23420), -- 85663
(43558, 8, 2901, 0, 0, 1, 0, 0, 23420), -- 2901
(43558, 7, 7005, 0, 0, 1, 0, 0, 23420), -- 7005
(43558, 6, 5042, 0, 0, 1, 0, 0, 23420), -- 5042
(43558, 5, 4470, 0, 0, 1, 0, 0, 23420), -- 4470
(43558, 4, 4498, 0, 0, 1, 0, 0, 23420), -- 4498
(43558, 3, 4496, 0, 0, 1, 0, 0, 23420), -- 4496
(43558, 2, 4540, 0, 0, 1, 0, 0, 23420), -- 4540
(43558, 1, 159, 0, 0, 1, 0, 0, 23420), -- 159
(38853, 25, 39505, 0, 0, 1, 0, 0, 23420), -- 39505
(38853, 24, 20815, 0, 0, 1, 0, 0, 23420), -- 20815
(38853, 23, 39354, 0, 0, 1, 0, 0, 23420), -- 39354
(38853, 22, 6260, 0, 0, 1, 0, 0, 23420), -- 6260
(38853, 21, 2324, 0, 0, 1, 0, 0, 23420), -- 2324
(38853, 20, 2604, 0, 0, 1, 0, 0, 23420), -- 2604
(38853, 19, 6529, 0, 0, 1, 0, 0, 23420), -- 6529
(38853, 18, 4289, 0, 0, 1, 0, 0, 23420), -- 4289
(38853, 17, 3371, 0, 0, 1, 0, 0, 23420), -- 3371
(38853, 16, 2880, 0, 0, 1, 0, 0, 23420), -- 2880
(38853, 15, 2320, 0, 0, 1, 0, 0, 23420), -- 2320
(38853, 14, 30817, 0, 0, 1, 0, 0, 23420), -- 30817
(38853, 13, 2678, 0, 0, 1, 0, 0, 23420), -- 2678
(38853, 12, 6217, 0, 0, 1, 0, 0, 23420), -- 6217
(38853, 11, 6256, 0, 0, 1, 0, 0, 23420), -- 6256
(38853, 10, 5956, 0, 0, 1, 0, 0, 23420), -- 5956
(38853, 9, 85663, 0, 0, 1, 0, 0, 23420), -- 85663
(38853, 8, 2901, 0, 0, 1, 0, 0, 23420), -- 2901
(38853, 7, 7005, 0, 0, 1, 0, 0, 23420), -- 7005
(38853, 6, 5042, 0, 0, 1, 0, 0, 23420), -- 5042
(38853, 5, 4470, 0, 0, 1, 0, 0, 23420), -- 4470
(38853, 4, 4498, 0, 0, 1, 0, 0, 23420), -- 4498
(38853, 3, 4496, 0, 0, 1, 0, 0, 23420), -- 4496
(38853, 2, 4540, 0, 0, 1, 0, 0, 23420), -- 4540
(38853, 1, 159, 0, 0, 1, 0, 0, 23420), -- 159
(36779, 25, 39505, 0, 0, 1, 0, 0, 23420), -- 39505
(36779, 24, 20815, 0, 0, 1, 0, 0, 23420), -- 20815
(36779, 23, 39354, 0, 0, 1, 0, 0, 23420), -- 39354
(36779, 22, 6260, 0, 0, 1, 0, 0, 23420), -- 6260
(36779, 21, 2324, 0, 0, 1, 0, 0, 23420), -- 2324
(36779, 20, 2604, 0, 0, 1, 0, 0, 23420), -- 2604
(36779, 19, 6529, 0, 0, 1, 0, 0, 23420), -- 6529
(36779, 18, 4289, 0, 0, 1, 0, 0, 23420), -- 4289
(36779, 17, 3371, 0, 0, 1, 0, 0, 23420), -- 3371
(36779, 16, 2880, 0, 0, 1, 0, 0, 23420), -- 2880
(36779, 15, 2320, 0, 0, 1, 0, 0, 23420), -- 2320
(36779, 14, 30817, 0, 0, 1, 0, 0, 23420), -- 30817
(36779, 13, 2678, 0, 0, 1, 0, 0, 23420), -- 2678
(36779, 12, 6217, 0, 0, 1, 0, 0, 23420), -- 6217
(36779, 11, 6256, 0, 0, 1, 0, 0, 23420), -- 6256
(36779, 10, 5956, 0, 0, 1, 0, 0, 23420), -- 5956
(36779, 9, 85663, 0, 0, 1, 0, 0, 23420), -- 85663
(36779, 8, 2901, 0, 0, 1, 0, 0, 23420), -- 2901
(36779, 7, 7005, 0, 0, 1, 0, 0, 23420), -- 7005
(36779, 6, 5042, 0, 0, 1, 0, 0, 23420), -- 5042
(36779, 5, 4470, 0, 0, 1, 0, 0, 23420), -- 4470
(36779, 4, 4498, 0, 0, 1, 0, 0, 23420), -- 4498
(36779, 3, 4496, 0, 0, 1, 0, 0, 23420), -- 4496
(36779, 2, 4540, 0, 0, 1, 0, 0, 23420), -- 4540
(36779, 1, 159, 0, 0, 1, 0, 0, 23420), -- 159
(36717, 8, 2493, 0, 0, 1, 0, 1, 23420), -- 2493
(36717, 7, 2490, 0, 0, 1, 0, 1, 23420), -- 2490
(36717, 6, 2488, 0, 0, 1, 0, 1, 23420), -- 2488
(36717, 5, 2495, 0, 0, 1, 0, 1, 23420), -- 2495
(36717, 4, 2494, 0, 0, 1, 0, 1, 23420), -- 2494
(36717, 3, 2491, 0, 0, 1, 0, 1, 23420), -- 2491
(36717, 2, 2492, 0, 0, 1, 0, 1, 23420), -- 2492
(36717, 1, 2489, 0, 0, 1, 0, 1, 23420), -- 2489
(36695, 25, 39505, 0, 0, 1, 0, 0, 23420), -- 39505
(36695, 24, 20815, 0, 0, 1, 0, 0, 23420), -- 20815
(36695, 23, 39354, 0, 0, 1, 0, 0, 23420), -- 39354
(36695, 22, 6260, 0, 0, 1, 0, 0, 23420), -- 6260
(36695, 21, 2324, 0, 0, 1, 0, 0, 23420), -- 2324
(36695, 20, 2604, 0, 0, 1, 0, 0, 23420), -- 2604
(36695, 19, 6529, 0, 0, 1, 0, 0, 23420), -- 6529
(36695, 18, 4289, 0, 0, 1, 0, 0, 23420), -- 4289
(36695, 17, 3371, 0, 0, 1, 0, 0, 23420), -- 3371
(36695, 16, 2880, 0, 0, 1, 0, 0, 23420), -- 2880
(36695, 15, 2320, 0, 0, 1, 0, 0, 23420), -- 2320
(36695, 14, 30817, 0, 0, 1, 0, 0, 23420), -- 30817
(36695, 13, 2678, 0, 0, 1, 0, 0, 23420), -- 2678
(36695, 12, 6217, 0, 0, 1, 0, 0, 23420), -- 6217
(36695, 11, 6256, 0, 0, 1, 0, 0, 23420), -- 6256
(36695, 10, 5956, 0, 0, 1, 0, 0, 23420), -- 5956
(36695, 9, 85663, 0, 0, 1, 0, 0, 23420), -- 85663
(36695, 8, 2901, 0, 0, 1, 0, 0, 23420), -- 2901
(36695, 7, 7005, 0, 0, 1, 0, 0, 23420), -- 7005
(36695, 6, 5042, 0, 0, 1, 0, 0, 23420), -- 5042
(36695, 5, 4470, 0, 0, 1, 0, 0, 23420), -- 4470
(36695, 4, 4498, 0, 0, 1, 0, 0, 23420), -- 4498
(36695, 3, 4496, 0, 0, 1, 0, 0, 23420), -- 4496
(36695, 2, 4540, 0, 0, 1, 0, 0, 23420), -- 4540
(36695, 1, 159, 0, 0, 1, 0, 0, 23420), -- 159
(68993, 1, 37460, 0, 0, 1, 0, 0, 23420), -- 37460
(38783, 25, 39505, 0, 0, 1, 0, 0, 23420), -- 39505
(38783, 24, 20815, 0, 0, 1, 0, 0, 23420), -- 20815
(38783, 23, 39354, 0, 0, 1, 0, 0, 23420), -- 39354
(38783, 22, 6260, 0, 0, 1, 0, 0, 23420), -- 6260
(38783, 21, 2324, 0, 0, 1, 0, 0, 23420), -- 2324
(38783, 20, 2604, 0, 0, 1, 0, 0, 23420), -- 2604
(38783, 19, 6529, 0, 0, 1, 0, 0, 23420), -- 6529
(38783, 18, 4289, 0, 0, 1, 0, 0, 23420), -- 4289
(38783, 17, 3371, 0, 0, 1, 0, 0, 23420), -- 3371
(38783, 16, 2880, 0, 0, 1, 0, 0, 23420), -- 2880
(38783, 15, 2320, 0, 0, 1, 0, 0, 23420), -- 2320
(38783, 14, 30817, 0, 0, 1, 0, 0, 23420), -- 30817
(38783, 13, 2678, 0, 0, 1, 0, 0, 23420), -- 2678
(38783, 12, 6217, 0, 0, 1, 0, 0, 23420), -- 6217
(38783, 11, 6256, 0, 0, 1, 0, 0, 23420), -- 6256
(38783, 10, 5956, 0, 0, 1, 0, 0, 23420), -- 5956
(38783, 9, 85663, 0, 0, 1, 0, 0, 23420), -- 85663
(38783, 8, 2901, 0, 0, 1, 0, 0, 23420), -- 2901
(38783, 7, 7005, 0, 0, 1, 0, 0, 23420), -- 7005
(38783, 6, 5042, 0, 0, 1, 0, 0, 23420), -- 5042
(38783, 5, 4470, 0, 0, 1, 0, 0, 23420), -- 4470
(38783, 4, 4498, 0, 0, 1, 0, 0, 23420), -- 4498
(38783, 3, 4496, 0, 0, 1, 0, 0, 23420), -- 4496
(38783, 2, 4540, 0, 0, 1, 0, 0, 23420), -- 4540
(38783, 1, 159, 0, 0, 1, 0, 0, 23420); -- 159


DELETE FROM `gossip_menu` WHERE (`entry`=83 AND `text_id`=580) OR (`entry`=12693 AND `text_id`=17830) OR (`entry`=11061 AND `text_id`=15377) OR (`entry`=10837 AND `text_id`=14840) OR (`entry`=10838 AND `text_id`=14845) OR (`entry`=14176 AND `text_id`=14832) OR (`entry`=10843 AND `text_id`=14843) OR (`entry`=10841 AND `text_id`=15034) OR (`entry`=14175 AND `text_id`=15029) OR (`entry`=9821 AND `text_id`=13584) OR (`entry`=14290 AND `text_id`=14835) OR (`entry`=11079 AND `text_id`=15408) OR (`entry`=11672 AND `text_id`=16330) OR (`entry`=5665 AND `text_id`=6961) OR (`entry`=12180 AND `text_id`=17125) OR (`entry`=10773 AND `text_id`=14938) OR (`entry`=10809 AND `text_id`=14986) OR (`entry`=10833 AND `text_id`=15025) OR (`entry`=10827 AND `text_id`=15013) OR (`entry`=12188 AND `text_id`=17128) OR (`entry`=12185 AND `text_id`=17126) OR (`entry`=12189 AND `text_id`=17129) OR (`entry`=12190 AND `text_id`=17130) OR (`entry`=12186 AND `text_id`=17127) OR (`entry`=12199 AND `text_id`=17138) OR (`entry`=12198 AND `text_id`=17137) OR (`entry`=12197 AND `text_id`=17136) OR (`entry`=12196 AND `text_id`=17135) OR (`entry`=12195 AND `text_id`=17134) OR (`entry`=12193 AND `text_id`=17133) OR (`entry`=12192 AND `text_id`=17132) OR (`entry`=12191 AND `text_id`=17131) OR (`entry`=10842 AND `text_id`=15035) OR (`entry`=5855 AND `text_id`=7028) OR (`entry`=5853 AND `text_id`=7021) OR (`entry`=14177 AND `text_id`=14848) OR (`entry`=10700 AND `text_id`=14845) OR (`entry`=14204 AND `text_id`=14835) OR (`entry`=10699 AND `text_id`=14843) OR (`entry`=14172 AND `text_id`=14832) OR (`entry`=10698 AND `text_id`=14840) OR (`entry`=14174 AND `text_id`=14839) OR (`entry`=14173 AND `text_id`=14848) OR (`entry`=10676 AND `text_id`=14798) OR (`entry`=4302 AND `text_id`=5474) OR (`entry`=9072 AND `text_id`=12270) OR (`entry`=12677 AND `text_id`=17820) OR (`entry`=14258 AND `text_id`=17616) OR (`entry`=12539 AND `text_id`=17616) OR (`entry`=12609 AND `text_id`=17749);
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES
(83, 580), -- 6491
(12693, 17830), -- 38611
(11061, 15377), -- 38553
(10837, 14840), -- 38465
(10838, 14845), -- 38466
(14176, 14832), -- 38793
(10843, 14843), -- 38796
(10841, 15034), -- 42853
(14175, 15029), -- 38798
(9821, 13584), -- 68993
(14290, 14835), -- 38799
(11079, 15408), -- 38764
(11672, 16330), -- 42953
(5665, 6961), -- 50570
(12180, 17125), -- 50247
(10773, 14938), -- 37065
(10809, 14986), -- 36456
(10833, 15025), -- 36452
(10827, 15013), -- 36290
(12188, 17128), -- 50247
(12185, 17126), -- 50247
(12189, 17129), -- 50247
(12190, 17130), -- 50247
(12186, 17127), -- 50247
(12199, 17138), -- 50247
(12198, 17137), -- 50247
(12197, 17136), -- 50247
(12196, 17135), -- 50247
(12195, 17134), -- 50247
(12193, 17133), -- 50247
(12192, 17132), -- 50247
(12191, 17131), -- 50247
(10842, 15035), -- 36717
(5855, 7028), -- 50574
(5853, 7021), -- 50567
(14177, 14848), -- 36652
(10700, 14845), -- 44468
(14204, 14835), -- 44459
(10699, 14843), -- 44464
(14172, 14832), -- 44455
(10698, 14840), -- 44465
(14174, 14839), -- 44461
(14173, 14848), -- 44469
(10676, 14798), -- 34913
(4302, 5474), -- 3838
(9072, 12270), -- 25013
(12677, 17820), -- 48735
(14258, 17616), -- 50507
(12539, 17616), -- 50505
(12609, 17749); -- 36616


DELETE FROM `quest_template` WHERE `ID` IN (24602 /*24602*/, 24678 /*24678*/, 24920 /*24920*/, 24903 /*24903*/, 24902 /*24902*/, 24904 /*24904*/, 24676 /*24676*/, 24674 /*24674*/, 24575 /*24575*/, 24675 /*24675*/, 24677 /*24677*/, 24592 /*24592*/, 24672 /*24672*/, 24673 /*24673*/, 24593 /*24593*/, 24646 /*24646*/, 24628 /*24628*/, 24627 /*24627*/, 24617 /*24617*/, 24616 /*24616*/, 24578 /*24578*/, 24501 /*24501*/, 24495 /*24495*/, 24484 /*24484*/, 24483 /*24483*/, 24472 /*24472*/, 24468 /*24468*/, 24438 /*24438*/, 14467 /*14467*/, 14466 /*14466*/, 14465 /*14465*/, 14402 /*14402*/, 14401 /*14401*/, 14400 /*14400*/, 14399 /*14399*/, 14412 /*14412*/, 14404 /*14404*/, 14416 /*14416*/, 14406 /*14406*/, 14403 /*14403*/, 14398 /*14398*/, 14397 /*14397*/, 14395 /*14395*/, 14396 /*14396*/, 14386 /*14386*/, 14368 /*14368*/, 14382 /*14382*/, 14369 /*14369*/, 14367 /*14367*/, 14366 /*14366*/, 14348 /*14348*/, 14347 /*14347*/, 14336 /*14336*/, 14321 /*14321*/, 14320 /*14320*/, 14313 /*14313*/, 14375 /*14375*/, 14222 /*14222*/, 14221 /*14221*/, 14218 /*14218*/, 14212 /*14212*/, 14294 /*14294*/, 14293 /*14293*/, 14214 /*14214*/, 14204 /*14204*/, 31889 /*31889*/, 14159 /*14159*/, 26129 /*26129*/, 14154 /*14154*/, 28850 /*28850*/, 24930 /*24930*/, 14157 /*14157*/, 14288 /*14288*/, 14277 /*14277*/, 14094 /*14094*/, 14098 /*14098*/, 14093 /*14093*/, 43301 /*43301*/, 43283 /*43283*/, 43300 /*43300*/, 43297 /*43297*/, 43285 /*43285*/, 43291 /*43291*/, 43298 /*43298*/, 43286 /*43286*/, 43296 /*43296*/, 43292 /*43292*/, 43284 /*43284*/, 43299 /*43299*/, 44911 /*44911*/, 44910 /*44910*/, 44909 /*44909*/, 44908 /*44908*/, 44891 /*44891*/, 43303 /*43303*/, 43179 /*43179*/, 42422 /*42422*/, 42421 /*42421*/, 42420 /*42420*/, 42234 /*42234*/, 42233 /*42233*/, 42170 /*42170*/, 41761 /*41761*/, 14091 /*14091*/, 14078 /*14078*/, 13518 /*13518*/, 26385 /*26385*/, 28517 /*28517*/, 14434 /*14434*/, 26706 /*26706*/, 24681 /*24681*/, 24680 /*24680*/, 24679 /*24679*/, 43242 /*43242*/, 43245 /*43245*/, 43287 /*43287*/, 43288 /*43288*/);
INSERT INTO `quest_template` (`ID`, `QuestType`, `QuestLevel`, `QuestPackageID`, `MinLevel`, `QuestSortID`, `QuestInfoID`, `SuggestedGroupNum`, `RewardNextQuest`, `RewardXPDifficulty`, `RewardXPMultiplier`, `RewardMoney`, `RewardMoneyDifficulty`, `RewardMoneyMultiplier`, `RewardBonusMoney`, `RewardDisplaySpell1`, `RewardDisplaySpell2`, `RewardDisplaySpell3`, `RewardSpell`, `RewardHonor`, `RewardKillHonor`, `StartItem`, `RewardArtifactXPDifficulty`, `RewardArtifactXPMultiplier`, `RewardArtifactCategoryID`, `Flags`, `FlagsEx`, `RewardSkillLineID`, `RewardNumSkillUps`, `PortraitGiver`, `PortraitTurnIn`, `RewardItem1`, `RewardItem2`, `RewardItem3`, `RewardItem4`, `RewardAmount1`, `RewardAmount2`, `RewardAmount3`, `RewardAmount4`, `ItemDrop1`, `ItemDrop2`, `ItemDrop3`, `ItemDrop4`, `ItemDropQuantity1`, `ItemDropQuantity2`, `ItemDropQuantity3`, `ItemDropQuantity4`, `RewardChoiceItemID1`, `RewardChoiceItemID2`, `RewardChoiceItemID3`, `RewardChoiceItemID4`, `RewardChoiceItemID5`, `RewardChoiceItemID6`, `RewardChoiceItemQuantity1`, `RewardChoiceItemQuantity2`, `RewardChoiceItemQuantity3`, `RewardChoiceItemQuantity4`, `RewardChoiceItemQuantity5`, `RewardChoiceItemQuantity6`, `RewardChoiceItemDisplayID1`, `RewardChoiceItemDisplayID2`, `RewardChoiceItemDisplayID3`, `RewardChoiceItemDisplayID4`, `RewardChoiceItemDisplayID5`, `RewardChoiceItemDisplayID6`, `POIContinent`, `POIx`, `POIy`, `POIPriority`, `RewardTitle`, `RewardArenaPoints`, `RewardFactionID1`, `RewardFactionID2`, `RewardFactionID3`, `RewardFactionID4`, `RewardFactionID5`, `RewardFactionValue1`, `RewardFactionValue2`, `RewardFactionValue3`, `RewardFactionValue4`, `RewardFactionValue5`, `RewardFactionCapIn1`, `RewardFactionCapIn2`, `RewardFactionCapIn3`, `RewardFactionCapIn4`, `RewardFactionCapIn5`, `RewardFactionOverride1`, `RewardFactionOverride2`, `RewardFactionOverride3`, `RewardFactionOverride4`, `RewardFactionOverride5`, `RewardFactionFlags`, `AreaGroupID`, `TimeAllowed`, `AllowableRaces`, `QuestRewardID`, `RewardCurrencyID1`, `RewardCurrencyID2`, `RewardCurrencyID3`, `RewardCurrencyID4`, `RewardCurrencyQty1`, `RewardCurrencyQty2`, `RewardCurrencyQty3`, `RewardCurrencyQty4`, `AcceptedSoundKitID`, `CompleteSoundKitID`, `VerifiedBuild`) VALUES
(24602, 2, 11, 0, 5, 4714, 0, 0, 24679, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24602
(24678, 2, 11, 0, 5, 4714, 0, 0, 24602, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 50220, 0, 1, 0, 268566536, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50220, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24678
(24920, 2, 11, 0, 5, 4755, 0, 0, 24678, 6, 1, 800, 6, 1, 2760, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24920
(24903, 2, 11, 0, 5, 4755, 0, 0, 24920, 4, 1, 300, 4, 1, 1680, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24903
(24902, 2, 11, 0, 5, 4755, 0, 0, 24903, 6, 1, 800, 6, 1, 2760, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24902
(24904, 2, 11, 2100, 5, 4714, 0, 0, 24902, 7, 1, 1300, 7, 1, 3300, 0, 0, 0, 0, 0, 0, 50334, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50334, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 7, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24904
(24676, 2, 11, 0, 5, 4714, 0, 0, 24904, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24676
(24674, 2, 11, 2076, 5, 4714, 0, 0, 0, 4, 1, 300, 4, 1, 1680, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 134217736, 0, 0, 0, 7864, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24674
(24575, 2, 10, 0, 5, 4714, 0, 0, 0, 5, 1, 350, 5, 1, 960, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49881, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24575
(24675, 2, 10, 0, 5, 4714, 0, 0, 0, 6, 1, 350, 5, 1, 1200, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33554440, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24675
(24677, 2, 10, 0, 5, 4714, 0, 0, 24575, 3, 1, 175, 3, 1, 480, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24677
(24592, 2, 10, 2049, 5, 4714, 0, 0, 0, 6, 1, 700, 6, 1, 1200, 0, 0, 0, 0, 0, 0, 50218, 0, 1, 0, 8, 0, 0, 0, 0, 0, 58255, 0, 0, 0, 1, 0, 0, 0, 0, 0, 50218, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24592
(24672, 2, 9, 0, 5, 4714, 0, 0, 24592, 4, 1, 200, 4, 1, 570, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24672
(24673, 2, 9, 0, 5, 4714, 0, 0, 24672, 3, 1, 150, 3, 1, 390, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24673
(24593, 2, 9, 2050, 5, 4714, 0, 0, 24673, 5, 1, 300, 5, 1, 780, 0, 0, 0, 72829, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24593
(24646, 2, 9, 0, 5, 4714, 0, 0, 24593, 4, 1, 200, 4, 1, 570, 0, 0, 0, 0, 0, 0, 50134, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50134, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24646
(24628, 2, 11, 0, 5, 4714, 0, 0, 0, 4, 1, 300, 4, 1, 1680, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24628
(24627, 2, 9, 0, 5, 4714, 0, 0, 0, 5, 1, 300, 5, 1, 780, 0, 0, 0, 71042, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24627
(24617, 2, 9, 0, 5, 4706, 0, 0, 24627, 5, 1, 300, 5, 1, 780, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24617
(24616, 2, 9, 2057, 5, 4714, 0, 0, 24617, 5, 1, 300, 5, 1, 780, 0, 0, 0, 0, 0, 0, 49944, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49944, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24616
(24578, 2, 8, 0, 5, 4714, 0, 0, 24616, 3, 1, 110, 3, 1, 330, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24578
(24501, 2, 9, 2006, 5, 4714, 0, 0, 0, 5, 1, 300, 5, 1, 780, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 134217736, 0, 0, 0, 22185, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24501
(24495, 2, 8, 0, 5, 4714, 0, 0, 0, 5, 1, 225, 5, 1, 660, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24495
(24484, 2, 8, 0, 5, 4714, 0, 0, 24501, 5, 1, 225, 5, 1, 660, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24484
(24483, 2, 8, 0, 5, 4714, 0, 0, 24484, 3, 1, 0, 0, 1, 330, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24483
(24472, 2, 8, 0, 5, 4714, 0, 0, 0, 5, 1, 225, 5, 1, 660, 0, 0, 0, 95840, 0, 0, 0, 0, 1, 0, 2097160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24472
(24468, 2, 8, 1996, 5, 4714, 0, 0, 24472, 5, 1, 225, 5, 1, 660, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24468
(24438, 2, 8, 0, 4, 4714, 0, 0, 24468, 5, 1, 225, 5, 1, 660, 0, 0, 0, 95679, 0, 0, 0, 0, 1, 0, 2097160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24438
(14467, 0, 7, 0, 4, 0, 0, 0, 0, 2, 1, 0, 0, 1, 120, 0, 0, 0, 69257, 0, 0, 0, 0, 1, 0, 2097160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14467
(14466, 2, 7, 0, 4, 4714, 0, 0, 0, 2, 1, 45, 2, 1, 120, 0, 0, 0, 68954, 0, 0, 0, 0, 1, 0, 1048584, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14466
(14465, 2, 7, 0, 4, 4714, 0, 0, 14466, 2, 1, 45, 2, 1, 120, 0, 0, 0, 82892, 0, 0, 0, 0, 1, 0, 2097160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14465
(14402, 2, 7, 0, 4, 4714, 0, 0, 14465, 3, 1, 85, 3, 1, 240, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14402
(14401, 2, 7, 1920, 4, 4714, 0, 0, 14402, 4, 1, 125, 4, 1, 360, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14401
(14400, 2, 7, 0, 4, 4714, 0, 0, 14401, 3, 1, 85, 3, 1, 240, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14400
(14399, 2, 7, 0, 4, 4714, 0, 0, 14400, 4, 1, 125, 4, 1, 360, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14399
(14412, 2, 7, 1930, 4, 4714, 0, 0, 0, 4, 1, 125, 4, 1, 360, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14412
(14404, 2, 7, 0, 4, 4714, 0, 0, 14405, 4, 1, 125, 4, 1, 360, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 52039, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14404
(14416, 2, 7, 1933, 4, 4714, 0, 0, 14463, 4, 1, 125, 4, 1, 360, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14416
(14406, 2, 7, 0, 4, 4714, 0, 0, 14416, 1, 1, 17, 1, 1, 60, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14406
(14403, 2, 7, 0, 4, 4714, 0, 0, 0, 3, 1, 85, 3, 1, 240, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 16777224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14403
(14398, 2, 7, 0, 4, 4714, 0, 0, 14399, 3, 1, 0, 0, 1, 240, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14398
(14397, 2, 7, 0, 4, 4714, 0, 0, 0, 2, 1, 45, 2, 1, 120, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 16777224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14397
(14395, 2, 6, 1914, 4, 4714, 0, 0, 14397, 5, 1, 125, 5, 1, 390, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14395
(14396, 2, 6, 0, 4, 4714, 0, 0, 14395, 3, 1, 65, 3, 1, 180, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14396
(14386, 2, 6, 1905, 4, 4714, 0, 0, 0, 4, 1, 100, 4, 1, 270, 0, 0, 0, 69027, 0, 0, 49240, 0, 1, 0, 138412040, 0, 0, 0, 21746, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49240, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14386
(14368, 2, 6, 1888, 5, 4714, 0, 0, 0, 5, 1, 125, 5, 1, 390, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14368
(14382, 2, 6, 1901, 4, 4714, 0, 0, 0, 5, 1, 125, 5, 1, 390, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14382
(14369, 2, 6, 1889, 4, 4714, 0, 0, 0, 5, 1, 125, 5, 1, 390, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14369
(14367, 2, 6, 0, 5, 4714, 0, 0, 0, 3, 1, 65, 3, 1, 180, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 16777224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14367
(14366, 2, 5, 0, 5, 4714, 0, 0, 14367, 4, 1, 75, 4, 1, 210, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14366
(14348, 2, 5, 1875, 5, 4714, 0, 0, 14366, 5, 1, 100, 5, 1, 270, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 131080, 0, 0, 0, 0, 0, 2723, 0, 0, 0, 5, 0, 0, 0, 49202, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14348
(14347, 2, 5, 1874, 5, 4714, 0, 0, 14366, 5, 1, 100, 5, 1, 270, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14347
(14336, 2, 5, 0, 5, 4714, 0, 0, 0, 3, 1, 50, 3, 1, 120, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 16777224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14336
(14321, 2, 5, 0, 5, 4714, 0, 0, 14336, 5, 1, 100, 5, 1, 270, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14321
(14320, 2, 5, 0, 5, 4714, 0, 0, 0, 5, 1, 100, 5, 1, 270, 0, 0, 0, 69296, 0, 0, 0, 0, 1, 0, 2097160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14320
(14313, 2, 5, 0, 5, 4714, 0, 0, 14320, 3, 1, 50, 3, 1, 120, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14313
(14375, 0, 5, 0, 1, 4714, 0, 0, 0, 5, 1, 0, 0, 1, 270, 0, 0, 0, 68639, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14375
(14222, 2, 5, 0, 1, 4755, 0, 0, 0, 7, 1, 300, 7, 1, 390, 0, 0, 0, 93477, 0, 0, 0, 0, 1, 0, 2621440, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14222
(14221, 2, 5, 0, 1, 4755, 0, 0, 14222, 3, 1, 50, 3, 1, 120, 0, 0, 0, 81040, 0, 0, 0, 0, 1, 0, 2621440, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14221
(14218, 2, 5, 9647, 1, 4755, 0, 0, 14221, 5, 1, 100, 5, 1, 270, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14218
(14212, 2, 5, 0, 1, 4755, 0, 0, 14218, 4, 1, 75, 4, 1, 210, 0, 0, 0, 101644, 0, 0, 0, 0, 1, 0, 2621440, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14212
(14294, 2, 4, 0, 1, 4755, 0, 0, 0, 1, 1, 7, 1, 1, 30, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14294
(14293, 2, 4, 0, 1, 4755, 0, 0, 0, 3, 1, 35, 3, 1, 120, 0, 0, 0, 93555, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14293
(14214, 2, 4, 0, 1, 4755, 0, 0, 14293, 2, 1, 0, 0, 1, 60, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14214
(14204, 2, 4, 575, 1, 4714, 0, 0, 14214, 5, 1, 75, 5, 1, 210, 0, 0, 0, 43511, 0, 0, 48707, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 66247, 0, 0, 0, 1, 0, 0, 0, 48707, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14204
(31889, 2, -1, 0, 1, -394, 102, 0, 31897, 5, 1, 75, 5, 1, 23700, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 65536, 2176, 0, 0, 0, 4558, 89125, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 31889
(14159, 2, 4, 0, 1, 4714, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 67352, 0, 0, 0, 0, 1, 0, 1074266112, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14159
(26129, 2, 3, 0, 1, 4714, 0, 0, 0, 1, 1, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 26129
(14154, 2, 4, 574, 1, 4755, 0, 0, 26129, 5, 1, 75, 5, 1, 210, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14154
(28850, 2, 3, 0, 1, 4755, 0, 0, 14154, 1, 1, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524416, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 52430847, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 28850
(24930, 2, 3, 9646, 1, 4755, 0, 0, 0, 4, 1, 40, 4, 1, 120, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24930
(14157, 2, 4, 0, 1, 4755, 0, 0, 28850, 1, 1, 7, 1, 1, 30, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14157
(14288, 2, 3, 0, 1, 4714, 0, 0, 14157, 2, 1, 13, 2, 1, 30, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14288
(14277, 2, 2, 0, 1, 4714, 21, 0, 14288, 5, 1, 35, 5, 1, 90, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14277
(14094, 2, 2, 0, 1, 4755, 0, 0, 0, 6, 1, 65, 6, 1, 120, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 52040, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14094
(14098, 2, 2, 0, 1, 4755, 0, 0, 14099, 6, 1, 65, 6, 1, 120, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14098
(14093, 2, 2, 0, 1, 4755, 0, 0, 14099, 6, 1, 65, 6, 1, 120, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 0, 0, 0, 0, 0, 55004, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14093
(43301, 2, -1, 0, 1, 16, 0, 0, 0, 8, 1.5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43301
(43283, 2, -1, 0, 1, 1, 0, 0, 0, 8, 1, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43283
(43300, 2, -1, 0, 1, 16, 0, 0, 0, 8, 0.75, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 60, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43300
(43297, 2, -1, 0, 1, 267, 0, 0, 0, 8, 0.75, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 60, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43297
(43285, 2, -1, 0, 1, 267, 0, 0, 0, 8, 1.5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43285
(43291, 2, -1, 0, 1, 1, 0, 0, 0, 8, 0.5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43291
(43298, 2, -1, 0, 1, 16, 0, 0, 0, 8, 0.5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43298
(43286, 2, -1, 0, 1, 267, 0, 0, 0, 8, 1, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43286
(43296, 2, -1, 0, 1, 267, 0, 0, 0, 8, 0.5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43296
(43292, 2, -1, 0, 1, 1, 0, 0, 0, 8, 0.75, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 60, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43292
(43284, 2, -1, 0, 1, 1, 0, 0, 0, 8, 1.5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43284
(43299, 2, -1, 0, 1, 16, 0, 0, 0, 8, 1, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43299
(44911, 2, 110, 0, 110, 0, 140, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34177088, 64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 44911
(44910, 2, 110, 0, 110, 0, 140, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34177024, 64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 44910
(44909, 2, 110, 0, 110, 0, 140, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34177088, 67108928, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 303, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 44909
(44908, 2, 110, 0, 110, 0, 140, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34177088, 67108928, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 302, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 44908
(44891, 2, 110, 0, 110, 0, 140, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34177088, 67108928, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 301, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 44891
(43303, 2, 110, 0, 110, 7558, 113, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 300, 0, 0, 0, 1, 0, 33619976, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1883, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43303
(43179, 2, 110, 0, 110, 7502, 128, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34078720, 67108864, 0, 0, 0, 0, 146753, 142007, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 141987, 141992, 141990, 141989, 141988, 141991, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43179
(42422, 2, 110, 0, 110, 8147, 128, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34078976, 67108864, 0, 0, 0, 0, 146752, 142006, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1894, 0, 0, 0, 0, 8, 0, 0, 0, 0, 7, 7, 7, 7, 7, 150000, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 42422
(42421, 2, 110, 0, 110, 7637, 128, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34078976, 67108864, 0, 0, 0, 0, 146751, 142005, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1859, 0, 0, 0, 0, 8, 0, 0, 0, 0, 7, 7, 7, 7, 7, 150000, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 42421
(42420, 2, 110, 0, 110, 7334, 128, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34078976, 67108864, 0, 0, 0, 0, 146750, 142004, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1900, 0, 0, 0, 0, 8, 0, 0, 0, 0, 7, 7, 7, 7, 7, 150000, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 42420
(42234, 2, 110, 0, 110, 7541, 128, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34078976, 67108864, 0, 0, 0, 0, 146749, 142003, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1948, 0, 0, 0, 0, 8, 0, 0, 0, 0, 7, 7, 7, 7, 7, 150000, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 42234
(42233, 2, 110, 0, 110, 7503, 128, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34078976, 67108864, 0, 0, 0, 0, 146748, 142002, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1828, 0, 0, 0, 0, 8, 0, 0, 0, 0, 7, 7, 7, 7, 7, 150000, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 42233
(42170, 2, 110, 0, 110, 7558, 128, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 34078976, 67108864, 0, 0, 0, 0, 146747, 142001, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1883, 0, 0, 0, 0, 8, 0, 0, 0, 0, 7, 7, 7, 7, 7, 150000, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 42170
(41761, 2, -1, 0, 98, 7503, 110, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 150096, 0, 0, 0, 0, 0, 1, 36372736, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1828, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 48, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 41761
(14091, 2, 2, 0, 1, 4755, 0, 0, 0, 5, 1, 35, 5, 1, 90, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 17301504, 512, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14091
(14078, 2, 1, 0, 1, 4755, 0, 0, 14091, 5, 1, 15, 5, 1, 60, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 524288, 512, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14078
(13518, 2, 10, 0, 8, 148, 0, 0, 0, 5, 1, 350, 5, 1, 960, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13518
(26385, 2, 10, 0, 8, 148, 0, 0, 13518, 2, 1, 85, 2, 1, 240, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 2097152, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 26385
(28517, 2, 10, 0, 8, 1657, 0, 0, 0, 3, 1, 175, 3, 1, 480, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 2097152, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 28517
(14434, 0, 12, 0, 5, 4706, 0, 0, 0, 1, 1, 50, 1, 1, 510, 0, 0, 0, 78107, 0, 0, 0, 0, 1, 0, 2097152, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 14434
(26706, 2, 12, 0, 12, 4714, 0, 0, 0, 7, 1, 1500, 7, 1, 7800, 0, 0, 0, 95840, 0, 0, 0, 0, 1, 0, 2097288, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 7, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 26706
(24681, 2, 12, 2083, 5, 4714, 0, 0, 0, 6, 1, 1000, 6, 1, 6600, 0, 0, 0, 81040, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24681
(24680, 2, 12, 0, 5, 4714, 0, 0, 24681, 1, 1, 50, 1, 1, 510, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24680
(24679, 2, 11, 0, 5, 4714, 0, 0, 24680, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 51956, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51956, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 24679
(43242, 2, -1, 0, 1, 40, 0, 0, 0, 8, 1, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43242
(43245, 2, -1, 0, 1, 40, 0, 0, 0, 8, 1.5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43245
(43287, 2, -1, 0, 1, 40, 0, 0, 0, 8, 0.5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43287
(43288, 2, -1, 0, 1, 40, 0, 0, 0, 8, 0.75, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 60, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420); -- 43288


DELETE FROM `quest_objectives` WHERE `ID` IN (265438 /*265438*/, 265635 /*265635*/, 265634 /*265634*/, 265467 /*265467*/, 265134 /*265134*/, 265133 /*265133*/, 265132 /*265132*/, 265172 /*265172*/, 266403 /*266403*/, 266831 /*266831*/, 265566 /*265566*/, 265565 /*265565*/, 266555 /*266555*/, 266554 /*266554*/, 266553 /*266553*/, 256045 /*256045*/, 265213 /*265213*/, 266711 /*266711*/, 266512 /*266512*/, 265584 /*265584*/, 256540 /*256540*/, 253933 /*253933*/, 265446 /*265446*/, 265445 /*265445*/, 255768 /*255768*/, 264872 /*264872*/, 265575 /*265575*/, 265253 /*265253*/, 264483 /*264483*/, 265423 /*265423*/, 265422 /*265422*/, 265421 /*265421*/, 261452 /*261452*/, 256573 /*256573*/, 267072 /*267072*/, 264865 /*264865*/, 264864 /*264864*/, 264863 /*264863*/, 264796 /*264796*/, 264795 /*264795*/, 255603 /*255603*/, 265475 /*265475*/, 262806 /*262806*/, 264452 /*264452*/, 264948 /*264948*/, 267253 /*267253*/, 264587 /*264587*/, 263905 /*263905*/, 269150 /*269150*/, 269149 /*269149*/, 269148 /*269148*/, 269147 /*269147*/, 269145 /*269145*/, 265434 /*265434*/, 265006 /*265006*/, 267179 /*267179*/, 264929 /*264929*/, 287277 /*287277*/, 287278 /*287278*/, 287242 /*287242*/, 285159 /*285159*/, 285073 /*285073*/, 284172 /*284172*/, 284171 /*284171*/, 284170 /*284170*/, 283946 /*283946*/, 283945 /*283945*/, 283830 /*283830*/, 282762 /*282762*/, 267130 /*267130*/, 267129 /*267129*/, 267128 /*267128*/, 267127 /*267127*/, 254200 /*254200*/, 265366 /*265366*/, 265365 /*265365*/, 265364 /*265364*/, 265108 /*265108*/);
INSERT INTO `quest_objectives` (`ID`, `QuestID`, `Type`, `StorageIndex`, `ObjectID`, `Amount`, `Flags`, `Flags2`, `ProgressBarWeight`, `VerifiedBuild`) VALUES
(265438, 24602, 1, 0, 49921, 5, 0, 1, 0, 23420), -- 265438
(265635, 24920, 0, 1, 38363, 40, 0, 0, 0, 23420), -- 265635
(265634, 24920, 0, 0, 38287, 6, 0, 0, 0, 23420), -- 265634
(265467, 24904, 0, 0, 38854, 1, 0, 0, 0, 23420), -- 265467
(265134, 24676, 0, 2, 37685, 1, 1, 0, 0, 23420), -- 265134
(265133, 24676, 0, 1, 37686, 1, 1, 0, 0, 23420), -- 265133
(265132, 24676, 0, 0, 37692, 4, 0, 0, 0, 23420), -- 265132
(265172, 24674, 0, 0, 37802, 1, 1, 0, 0, 23420), -- 265172
(266403, 24575, 2, 0, 201775, 5, 0, 0, 0, 23420), -- 266403
(266831, 24675, 1, 0, 50219, 10, 0, 1, 0, 23420), -- 266831
(265566, 24592, 0, 1, 37733, 1, 1, 0, 0, 23420), -- 265566
(265565, 24592, 0, 0, 37735, 1, 1, 0, 0, 23420), -- 265565
(266555, 24593, 2, 2, 201952, 1, 0, 0, 0, 23420), -- 266555
(266554, 24593, 2, 1, 201951, 1, 0, 0, 0, 23420), -- 266554
(266553, 24593, 2, 0, 201950, 1, 0, 0, 0, 23420), -- 266553
(256045, 24646, 1, 0, 50086, 1, 0, 1, 0, 23420), -- 256045
(265213, 24628, 1, 0, 50017, 6, 0, 1, 0, 23420), -- 265213
(266711, 24627, 0, 0, 37757, 6, 0, 0, 0, 23420), -- 266711
(266512, 24616, 0, 0, 37953, 1, 1, 0, 0, 23420), -- 266512
(265584, 24501, 0, 0, 37045, 1, 1, 0, 0, 23420), -- 265584
(256540, 24495, 1, 0, 49760, 6, 0, 1, 0, 23420), -- 256540
(253933, 24484, 0, 0, 36813, 6, 0, 0, 0, 23420), -- 253933
(265446, 24472, 1, 1, 49742, 1, 0, 1, 0, 23420), -- 265446
(265445, 24472, 0, 0, 36293, 4, 0, 0, 0, 23420), -- 265445
(255768, 24468, 0, 0, 37078, 5, 0, 0, 0, 23420), -- 255768
(264872, 14401, 1, 0, 49281, 1, 0, 1, 0, 23420), -- 264872
(265575, 14400, 1, 0, 49279, 1, 0, 1, 0, 23420), -- 265575
(265253, 14399, 1, 0, 49280, 1, 0, 1, 0, 23420), -- 265253
(264483, 14412, 0, 0, 36488, 6, 0, 0, 0, 23420), -- 264483
(265423, 14404, 1, 2, 49339, 1, 0, 1, 0, 23420), -- 265423
(265422, 14404, 1, 1, 49338, 1, 0, 1, 0, 23420), -- 265422
(265421, 14404, 1, 0, 49337, 1, 0, 1, 0, 23420), -- 265421
(261452, 14416, 0, 0, 36560, 5, 0, 0, 0, 23420), -- 261452
(256573, 14395, 0, 0, 36450, 4, 0, 0, 0, 23420), -- 256573
(267072, 14386, 0, 0, 36312, 1, 1, 0, 0, 23420), -- 267072
(264865, 14368, 0, 2, 36289, 1, 0, 0, 0, 23420), -- 264865
(264864, 14368, 0, 1, 36288, 1, 0, 0, 0, 23420), -- 264864
(264863, 14368, 0, 0, 36287, 1, 0, 0, 0, 23420), -- 264863
(264796, 14382, 0, 1, 36399, 1, 1, 0, 0, 23420), -- 264796
(264795, 14382, 0, 0, 36397, 1, 1, 0, 0, 23420), -- 264795
(255603, 14369, 0, 0, 36634, 8, 0, 0, 0, 23420), -- 255603
(265475, 14348, 0, 0, 36233, 4, 0, 0, 0, 23420), -- 265475
(262806, 14347, 0, 0, 34511, 10, 0, 0, 0, 23420), -- 262806
(264452, 14222, 0, 0, 35627, 8, 0, 0, 0, 23420), -- 264452
(264948, 14218, 0, 0, 35229, 80, 0, 0, 0, 23420), -- 264948
(267253, 14212, 0, 0, 35582, 30, 0, 0, 0, 23420), -- 267253
(264587, 14293, 0, 0, 35753, 1, 0, 0, 0, 23420), -- 264587
(263905, 14204, 0, 0, 35463, 6, 0, 0, 0, 23420), -- 263905
(269150, 31889, 11, 0, 66412, 1, 1, 0, 0, 23420), -- 269150
(269149, 31889, 11, 3, 66442, 1, 0, 0, 0, 23420), -- 269149
(269148, 31889, 11, 4, 66452, 1, 0, 0, 0, 23420), -- 269148
(269147, 31889, 11, 2, 66436, 1, 0, 0, 0, 23420), -- 269147
(269145, 31889, 11, 5, 66352, 1, 0, 0, 0, 23420), -- 269145
(265434, 24930, 0, 0, 35118, 5, 0, 0, 0, 23420), -- 265434
(265006, 14094, 1, 0, 46896, 4, 0, 1, 0, 23420), -- 265006
(267179, 14098, 0, 0, 35830, 3, 0, 0, 0, 23420), -- 267179
(264929, 14093, 0, 0, 34884, 6, 0, 0, 0, 23420), -- 264929
(287277, 44909, 14, 0, 55504, 1, 0, 0, 0, 23420), -- 287277
(287278, 44908, 14, 0, 55506, 1, 0, 0, 0, 23420), -- 287278
(287242, 44891, 14, 0, 55498, 1, 0, 0, 0, 23420), -- 287242
(285159, 43303, 3, 0, 106776, 1, 0, 0, 0, 23420), -- 285159
(285073, 43179, 14, 0, 52074, 1, 0, 0, 0, 23420), -- 285073
(284172, 42422, 14, 0, 48274, 1, 0, 0, 0, 23420), -- 284172
(284171, 42421, 14, 0, 50522, 1, 0, 0, 0, 23420), -- 284171
(284170, 42420, 14, 0, 50514, 1, 0, 0, 0, 23420), -- 284170
(283946, 42234, 14, 0, 50518, 1, 0, 0, 0, 23420), -- 283946
(283945, 42233, 14, 0, 50524, 1, 0, 0, 0, 23420), -- 283945
(283830, 42170, 14, 0, 50520, 1, 0, 0, 0, 23420), -- 283830
(282762, 41761, 0, 0, 104757, 4, 0, 0, 0, 23420), -- 282762
(267130, 13518, 0, 3, 33094, 1, 0, 0, 0, 23420), -- 267130
(267129, 13518, 0, 2, 33095, 1, 0, 0, 0, 23420), -- 267129
(267128, 13518, 0, 1, 32911, 1, 0, 0, 0, 23420), -- 267128
(267127, 13518, 0, 0, 33093, 1, 0, 0, 0, 23420), -- 267127
(254200, 26706, 0, 0, 43729, 1, 0, 0, 0, 23420), -- 254200
(265366, 24681, 0, 2, 37921, 4, 0, 0, 0, 23420), -- 265366
(265365, 24681, 0, 1, 37938, 8, 0, 0, 0, 23420), -- 265365
(265364, 24681, 0, 0, 37916, 40, 0, 0, 0, 23420), -- 265364
(265108, 24679, 0, 0, 38147, 1, 0, 0, 0, 23420); -- 265108


DELETE FROM `quest_visual_effect` WHERE (`ID`=265467 AND `Index`=0) OR (`ID`=264587 AND `Index`=0) OR (`ID`=269150 AND `Index`=0) OR (`ID`=269149 AND `Index`=0) OR (`ID`=269148 AND `Index`=0) OR (`ID`=269147 AND `Index`=0) OR (`ID`=269145 AND `Index`=0) OR (`ID`=285159 AND `Index`=0);
INSERT INTO `quest_visual_effect` (`ID`, `Index`, `VisualEffect`, `VerifiedBuild`) VALUES
(265467, 0, 1976, 23420),
(264587, 0, 2914, 23420),
(269150, 0, 1069, 23420),
(269149, 0, 1072, 23420),
(269148, 0, 1073, 23420),
(269147, 0, 1071, 23420),
(269145, 0, 1053, 23420),
(285159, 0, 505, 23420);


DELETE FROM `creature_questitem` WHERE (`CreatureEntry`=37701 AND `Idx`=0) OR (`CreatureEntry`=37786 AND `Idx`=0) OR (`CreatureEntry`=36461 AND `Idx`=0) OR (`CreatureEntry`=32935 AND `Idx`=0);
INSERT INTO `creature_questitem` (`CreatureEntry`, `Idx`, `ItemId`, `VerifiedBuild`) VALUES
(37701, 0, 49881, 23420), -- 37701
(37786, 0, 50219, 23420), -- 37786
(36461, 0, 49281, 23420), -- 36461
(32935, 0, 44863, 23420); -- 32935


DELETE FROM `gameobject_template` WHERE `entry` IN (201964 /*201964*/, 201871 /*201871*/, 195578 /*195578*/, 201775 /*201775*/, 201939 /*201939*/, 201951 /*201951*/, 201950 /*201950*/, 201952 /*201952*/, 201914 /*201914*/, 1619 /*1619*/, 201607 /*201607*/, 205005 /*205005*/, 205009 /*205009*/, 201891 /*201891*/, 201594 /*201594*/, 197338 /*197338*/, 202625 /*202625*/, 202624 /*202624*/, 202634 /*202634*/, 202628 /*202628*/, 202627 /*202627*/, 202626 /*202626*/, 202646 /*202646*/, 202645 /*202645*/, 202644 /*202644*/, 202643 /*202643*/, 202633 /*202633*/, 202632 /*202632*/, 202631 /*202631*/, 202630 /*202630*/, 202629 /*202629*/, 202642 /*202642*/, 202636 /*202636*/, 202635 /*202635*/, 202641 /*202641*/, 202640 /*202640*/, 202639 /*202639*/, 202638 /*202638*/, 202637 /*202637*/, 196864 /*196864*/, 196863 /*196863*/, 196473 /*196473*/, 196810 /*196810*/, 196809 /*196809*/, 196808 /*196808*/, 197333 /*197333*/, 196465 /*196465*/, 196466 /*196466*/, 196411 /*196411*/, 196472 /*196472*/, 1618 /*1618*/, 1731 /*1731*/, 1617 /*1617*/, 196403 /*196403*/, 196849 /*196849*/, 197337 /*197337*/, 196394 /*196394*/, 195581 /*195581*/, 195579 /*195579*/, 195580 /*195580*/, 196846 /*196846*/, 196879 /*196879*/, 196880 /*196880*/, 196854 /*196854*/, 204423 /*204423*/, 195454 /*195454*/, 195453 /*195453*/, 195465 /*195465*/, 195466 /*195466*/, 204132 /*204132*/, 204131 /*204131*/, 204774 /*204774*/, 177278 /*177278*/, 194107 /*194107*/, 180682 /*180682*/, 181646 /*181646*/, 164767 /*164767*/, 164764 /*164764*/, 164763 /*164763*/, 164762 /*164762*/, 164761 /*164761*/, 164760 /*164760*/, 164759 /*164759*/, 164766 /*164766*/, 164765 /*164765*/, 92535 /*92535*/, 175725 /*175725*/, 91672 /*91672*/, 193981 /*193981*/, 92529 /*92529*/, 92530 /*92530*/, 175730 /*175730*/, 177279 /*177279*/, 21581 /*21581*/, 92537 /*92537*/, 92526 /*92526*/, 92525 /*92525*/, 193583 /*193583*/, 193584 /*193584*/, 175760 /*175760*/, 175727 /*175727*/, 92538 /*92538*/, 91673 /*91673*/, 208830 /*208830*/, 208829 /*208829*/, 175731 /*175731*/, 207321 /*207321*/, 142110 /*142110*/, 208815 /*208815*/, 208814 /*208814*/, 187337 /*187337*/, 187296 /*187296*/, 176310 /*176310*/, 195112 /*195112*/, 208831 /*208831*/, 204458 /*204458*/, 204428 /*204428*/, 203428 /*203428*/, 207999 /*207999*/, 207626 /*207626*/, 202319 /*202319*/);
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `IconName`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(201964, 8, 9247, '', '', 1, 1643, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 201964
(201871, 3, 49, '', '', 1, 43, 27777, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23645, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 201871
(195578, 0, 9062, '', '', 0.89, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195578
(201775, 10, 181, '', '', 1, 1851, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70354, 0, 0, 0, 24815, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 201775
(201939, 3, 5743, '', '', 1, 1691, 27788, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 201939
(201951, 10, 439, '', '', 0.7, 93, 24593, 0, 3000, 0, 0, 0, 0, 0, 0, 71200, 0, 0, 0, 21295, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 201951
(201950, 10, 439, '', '', 0.7, 93, 24593, 0, 3000, 0, 0, 0, 0, 0, 0, 71200, 0, 0, 0, 21295, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 201950
(201952, 10, 439, '', '', 0.7, 93, 24593, 0, 3000, 0, 0, 0, 0, 0, 0, 71200, 0, 0, 0, 21295, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 201952
(201914, 3, 270, '', '', 0.35, 259, 27783, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19676, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 201914
(1619, 50, 414, '', '', 0.4, 29, 1416, 0, 0, 65, 115, 30, 0, 0, 0, 0, 0, 20, 1, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 1619
(201607, 3, 9130, '', '', 1, 1852, 27739, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19676, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 201607
(205005, 8, 9142, '', '', 1.5531, 1, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 205005
(205009, 8, 9141, '', '', 0.919897, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 205009
(201891, 5, 6957, '', '', 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 201891
(201594, 3, 6957, '', '', 2, 1691, 27732, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 201594
(197338, 1, 9146, '', '', 0.76, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 197338
(202625, 7, 9374, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202625
(202624, 7, 9374, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202624
(202634, 7, 9376, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202634
(202628, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202628
(202627, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202627
(202626, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202626
(202646, 7, 9376, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202646
(202645, 7, 9376, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202645
(202644, 7, 9376, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202644
(202643, 7, 9376, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202643
(202633, 7, 9376, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202633
(202632, 7, 9376, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202632
(202631, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202631
(202630, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202630
(202629, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202629
(202642, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202642
(202636, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202636
(202635, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202635
(202641, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202641
(202640, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202640
(202639, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202639
(202638, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202638
(202637, 7, 9375, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 202637
(196864, 1, 9063, '', '', 0.35, 0, 0, 5000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196864
(196863, 1, 9063, '', '', 0.32, 0, 0, 5000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196863
(196473, 3, 255, '', '', 1, 1691, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 23645, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 27592, 1, 0, 0, 23420), -- 196473
(196810, 3, 317, '', '', 0.85, 1691, 27605, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196810
(196809, 3, 6736, '', '', 1, 1691, 27604, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196809
(196808, 3, 9095, '', '', 1, 1691, 27603, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196808
(197333, 10, 9510, '', '', 2, 0, 14395, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 197333
(196465, 3, 9119, '', '', 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196465
(196466, 3, 9120, '', '', 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196466
(196411, 0, 9028, '', '', 0.9199, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196411
(196472, 3, 9122, '', '', 1.2, 1691, 27591, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23645, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196472
(1618, 50, 269, '', '', 0.5, 29, 1415, 0, 0, 50, 100, 30, 0, 0, 0, 0, 0, 15, 1, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 1618
(1731, 50, 310, '', '', 0.5, 38, 1502, 0, 0, 50, 100, 30, 0, 0, 0, 0, 0, 15, 1, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 1731
(1617, 50, 270, '', '', 0.6, 29, 1414, 0, 0, 50, 100, 30, 0, 0, 0, 0, 0, 15, 1, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 1617
(196403, 3, 9095, '', '', 1, 1691, 27574, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23645, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196403
(196849, 8, 9138, '', '', 1, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196849
(197337, 13, 9147, '', '', 1, 0, 168, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 197337
(196394, 2, 9093, '', '', 1, 0, 9884, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196394
(195581, 0, 9063, '', '', 0.6, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195581
(195579, 0, 9063, '', '', 0.9, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195579
(195580, 0, 9062, '', '', 0.72, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195580
(196846, 19, 9139, '', '', 1.23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196846
(196879, 8, 9141, '', '', 0.85, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196879
(196880, 8, 9142, '', '', 1.56, 1, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196880
(196854, 8, 9140, '', '', 1.07, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 196854
(204423, 15, 8253, '', '', 1, 2337, 1, 10, 0, 0, 0, -1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 204423
(195454, 1, 9033, '', '', 0.87, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195454
(195453, 1, 9032, '', '', 0.97, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195453
(195465, 1, 9039, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195465
(195466, 1, 9039, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195466
(204132, 8, 305, '', '', 1, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 204132
(204131, 8, 273, '', '', 1, 1, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 204131
(204774, 19, 1948, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 204774
(177278, 8, 1407, '', '', 1, 883, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 177278
(194107, 3, 261, '', '', 0.3, 1634, 26827, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 21400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194107
(180682, 25, 6291, '', '', 1, 4, 17520, 2, 6, 1628, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180682
(181646, 15, 7087, '', '', 1, 503, 30, 1, 0, 0, 1, 586, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 181646
(164767, 7, 39, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 164767
(164764, 7, 39, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 164764
(164763, 7, 39, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 164763
(164762, 7, 39, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 164762
(164761, 7, 39, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 164761
(164760, 7, 39, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 164760
(164759, 7, 39, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 164759
(164766, 7, 39, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 164766
(164765, 7, 39, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 164765
(92535, 5, 948, '', '', 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 92535
(175725, 9, 558, '', '', 1, 1787, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 175725
(91672, 7, 91, '', '', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 91672
(193981, 8, 8520, '', '', 1.5, 1589, 10, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 193981
(92529, 5, 949, '', '', 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 92529
(92530, 5, 949, '', '', 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 92530
(175730, 9, 559, '', '', 1, 1836, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 175730
(177279, 8, 1407, '', '', 1, 883, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 177279
(21581, 9, 558, '', '', 1, 627, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 21581
(92537, 5, 950, '', '', 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 92537
(92526, 5, 952, '', '', 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 92526
(92525, 5, 953, '', '', 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 92525
(193583, 5, 1208, '', '', 0.75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 193583
(193584, 5, 1209, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 193584
(175760, 9, 3871, '', '', 1, 2093, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 175760
(175727, 9, 559, '', '', 1, 1803, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 175727
(92538, 5, 951, '', '', 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 92538
(91673, 8, 348, '', '', 1, 4, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 91673
(208830, 6, 10618, '', '', 1.8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 208830
(208829, 10, 10097, '', '', 0.75, 1960, 29316, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 208830, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 208829
(175731, 9, 558, '', '', 1, 1844, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 175731
(207321, 48, 10016, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 207321
(142110, 19, 1948, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 142110
(208815, 6, 49, '', '', 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 208815
(208814, 3, 6358, '', '', 1.25, 1691, 39754, 0, 1, 0, 0, 0, 208815, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 208814
(187337, 34, 7615, '', '', 0.33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 187337
(187296, 34, 7615, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 187296
(176310, 15, 3015, '', '', 1, 967, 30, 1, 0, 0, 1, 588, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 176310
(195112, 0, 4673, '', '', 0.35, 0, 1837, 196608, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195112
(208831, 3, 261, '', '', 0.5, 1691, 39774, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 208831
(204458, 8, 9539, '', '', 2, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 204458
(204428, 10, 7538, '', '', 1, 0, 0, 0, 3000, 0, 0, 0, 0, 0, 0, 81425, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 204428
(203428, 15, 8253, '', '', 1, 2338, 15, 10, 0, 0, 83, 749, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 203428
(207999, 10, 352, '', '', 1, 0, 0, 0, 3000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 207999
(207626, 10, 7225, '', '', 1, 0, 0, 0, 3000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 207626
(202319, 5, 6660, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420); -- 202319


DELETE FROM `gameobject_questitem` WHERE (`GameObjectEntry`=201871 AND `Idx`=0) OR (`GameObjectEntry`=201939 AND `Idx`=0) OR (`GameObjectEntry`=201914 AND `Idx`=0) OR (`GameObjectEntry`=201607 AND `Idx`=0) OR (`GameObjectEntry`=201594 AND `Idx`=0) OR (`GameObjectEntry`=196473 AND `Idx`=0) OR (`GameObjectEntry`=196810 AND `Idx`=0) OR (`GameObjectEntry`=196809 AND `Idx`=0) OR (`GameObjectEntry`=196808 AND `Idx`=0) OR (`GameObjectEntry`=196472 AND `Idx`=0) OR (`GameObjectEntry`=196403 AND `Idx`=0) OR (`GameObjectEntry`=194107 AND `Idx`=0) OR (`GameObjectEntry`=208814 AND `Idx`=0) OR (`GameObjectEntry`=208831 AND `Idx`=0);
INSERT INTO `gameobject_questitem` (`GameObjectEntry`, `Idx`, `ItemId`, `VerifiedBuild`) VALUES
(201871, 0, 49921, 23420), -- 201871
(201939, 0, 50086, 23420), -- 201939
(201914, 0, 50017, 23420), -- 201914
(201607, 0, 49760, 23420), -- 201607
(201594, 0, 49742, 23420), -- 201594
(196473, 0, 49280, 23420), -- 196473
(196810, 0, 49337, 23420), -- 196810
(196809, 0, 49338, 23420), -- 196809
(196808, 0, 49339, 23420), -- 196808
(196472, 0, 49279, 23420), -- 196472
(196403, 0, 49202, 23420), -- 196403
(194107, 0, 44864, 23420), -- 194107
(208814, 0, 69898, 23420), -- 208814
(208831, 0, 69910, 23420); -- 208831


DELETE FROM `npc_text` WHERE `ID` IN (17830 /*17830*/, 580 /*580*/, 15377 /*15377*/, 15408 /*15408*/, 16330 /*16330*/, 6961 /*6961*/, 14986 /*14986*/, 15025 /*15025*/, 15013 /*15013*/, 14938 /*14938*/, 17129 /*17129*/, 17130 /*17130*/, 17137 /*17137*/, 17136 /*17136*/, 17134 /*17134*/, 17138 /*17138*/, 17133 /*17133*/, 17132 /*17132*/, 17127 /*17127*/, 17135 /*17135*/, 17131 /*17131*/, 17128 /*17128*/, 17126 /*17126*/, 17125 /*17125*/, 15035 /*15035*/, 15029 /*15029*/, 7028 /*7028*/, 7021 /*7021*/, 14832 /*14832*/, 14840 /*14840*/, 14843 /*14843*/, 14845 /*14845*/, 14835 /*14835*/, 14848 /*14848*/, 13584 /*13584*/, 14839 /*14839*/, 15034 /*15034*/, 14798 /*14798*/, 5474 /*5474*/, 12270 /*12270*/, 17820 /*17820*/, 17616 /*17616*/, 17749 /*17749*/);
INSERT INTO `npc_text` (`ID`, `Probability0`, `Probability1`, `Probability2`, `Probability3`, `Probability4`, `Probability5`, `Probability6`, `Probability7`, `BroadcastTextId0`, `BroadcastTextId1`, `BroadcastTextId2`, `BroadcastTextId3`, `BroadcastTextId4`, `BroadcastTextId5`, `BroadcastTextId6`, `BroadcastTextId7`, `VerifiedBuild`) VALUES
(17830, 1, 0, 0, 0, 0, 0, 0, 0, 50737, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17830
(580, 100, 0, 0, 0, 0, 0, 0, 0, 2545, 0, 0, 0, 0, 0, 0, 0, 23420), -- 580
(15377, 1, 0, 0, 0, 0, 0, 0, 0, 38483, 0, 0, 0, 0, 0, 0, 0, 23420), -- 15377
(15408, 1, 0, 0, 0, 0, 0, 0, 0, 38634, 0, 0, 0, 0, 0, 0, 0, 23420), -- 15408
(16330, 1, 0, 0, 0, 0, 0, 0, 0, 42896, 0, 0, 0, 0, 0, 0, 0, 23420), -- 16330
(6961, 1, 0, 0, 0, 0, 0, 0, 0, 9557, 0, 0, 0, 0, 0, 0, 0, 23420), -- 6961
(14986, 1, 0, 0, 0, 0, 0, 0, 0, 36459, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14986
(15025, 1, 0, 0, 0, 0, 0, 0, 0, 36593, 0, 0, 0, 0, 0, 0, 0, 23420), -- 15025
(15013, 1, 0, 0, 0, 0, 0, 0, 0, 36556, 0, 0, 0, 0, 0, 0, 0, 23420), -- 15013
(14938, 1, 0, 0, 0, 0, 0, 0, 0, 36319, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14938
(17129, 1, 0, 0, 0, 0, 0, 0, 0, 47131, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17129
(17130, 1, 0, 0, 0, 0, 0, 0, 0, 47132, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17130
(17137, 1, 0, 0, 0, 0, 0, 0, 0, 47140, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17137
(17136, 1, 0, 0, 0, 0, 0, 0, 0, 47139, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17136
(17134, 1, 0, 0, 0, 0, 0, 0, 0, 47137, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17134
(17138, 1, 0, 0, 0, 0, 0, 0, 0, 47141, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17138
(17133, 1, 0, 0, 0, 0, 0, 0, 0, 47136, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17133
(17132, 1, 0, 0, 0, 0, 0, 0, 0, 47134, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17132
(17127, 1, 0, 0, 0, 0, 0, 0, 0, 47129, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17127
(17135, 1, 0, 0, 0, 0, 0, 0, 0, 47138, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17135
(17131, 1, 0, 0, 0, 0, 0, 0, 0, 47133, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17131
(17128, 1, 0, 0, 0, 0, 0, 0, 0, 47130, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17128
(17126, 1, 0, 0, 0, 0, 0, 0, 0, 47108, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17126
(17125, 1, 0, 0, 0, 0, 0, 0, 0, 47105, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17125
(15035, 1, 0, 0, 0, 0, 0, 0, 0, 36636, 0, 0, 0, 0, 0, 0, 0, 23420), -- 15035
(15029, 1, 0, 0, 0, 0, 0, 0, 0, 36606, 0, 0, 0, 0, 0, 0, 0, 23420), -- 15029
(7028, 1, 0, 0, 0, 0, 0, 0, 0, 9701, 0, 0, 0, 0, 0, 0, 0, 23420), -- 7028
(7021, 1, 0, 0, 0, 0, 0, 0, 0, 9688, 0, 0, 0, 0, 0, 0, 0, 23420), -- 7021
(14832, 1, 0, 0, 0, 0, 0, 0, 0, 36055, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14832
(14840, 1, 0, 0, 0, 0, 0, 0, 0, 36068, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14840
(14843, 1, 0, 0, 0, 0, 0, 0, 0, 36071, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14843
(14845, 1, 0, 0, 0, 0, 0, 0, 0, 36073, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14845
(14835, 1, 0, 0, 0, 0, 0, 0, 0, 36058, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14835
(14848, 1, 0, 0, 0, 0, 0, 0, 0, 36076, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14848
(13584, 1, 0, 0, 0, 0, 0, 0, 0, 30361, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13584
(14839, 1, 0, 0, 0, 0, 0, 0, 0, 36066, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14839
(15034, 1, 0, 0, 0, 0, 0, 0, 0, 36635, 0, 0, 0, 0, 0, 0, 0, 23420), -- 15034
(14798, 1, 0, 0, 0, 0, 0, 0, 0, 35965, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14798
(5474, 1, 0, 0, 0, 0, 0, 0, 0, 8099, 0, 0, 0, 0, 0, 0, 0, 23420), -- 5474
(12270, 1, 0, 0, 0, 0, 0, 0, 0, 24118, 0, 0, 0, 0, 0, 0, 0, 23420), -- 12270
(17820, 1, 0, 0, 0, 0, 0, 0, 0, 50711, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17820
(17616, 1, 0, 0, 0, 0, 0, 0, 0, 49789, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17616
(17749, 1, 0, 0, 0, 0, 0, 0, 0, 50429, 0, 0, 0, 0, 0, 0, 0, 23420); -- 17749