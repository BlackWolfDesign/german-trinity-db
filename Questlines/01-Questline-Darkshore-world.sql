-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 03/16/2017 22:46:21


DELETE FROM `areatrigger_template` WHERE `Id`=9397;
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `VerifiedBuild`) VALUES
(9397, 4, 0, 8, 8, 4, 4, 0.3, 0.3, 23420);


INSERT IGNORE INTO `weather_update` (`map_id`, `zone_id`, `weather_state`, `grade`, `unk`) VALUES
(1, 0, 1, 0, 0); -- 1 - Fog - 0


DELETE FROM `quest_poi` WHERE (`QuestID`=13591 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13515 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13515 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13514 AND `BlobIndex`=4 AND `Idx1`=2) OR (`QuestID`=13514 AND `BlobIndex`=3 AND `Idx1`=1) OR (`QuestID`=13514 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13590 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13844 AND `BlobIndex`=0 AND `Idx1`=3) OR (`QuestID`=13844 AND `BlobIndex`=0 AND `Idx1`=2) OR (`QuestID`=13844 AND `BlobIndex`=0 AND `Idx1`=1) OR (`QuestID`=13844 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13513 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13513 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13513 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13512 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13512 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13512 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13510 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13510 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13511 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13511 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13507 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13507 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13507 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13509 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13509 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13508 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13506 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13504 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13504 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13505 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13505 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13560 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13560 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13589 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13599 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13569 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13569 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13598 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13598 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13566 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=13566 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13566 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13566 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13565 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13565 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13565 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13564 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13564 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13563 AND `BlobIndex`=0 AND `Idx1`=2) OR (`QuestID`=13563 AND `BlobIndex`=0 AND `Idx1`=1) OR (`QuestID`=13563 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13562 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13562 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13831 AND `BlobIndex`=1 AND `Idx1`=0) OR (`QuestID`=13557 AND `BlobIndex`=1 AND `Idx1`=2) OR (`QuestID`=13557 AND `BlobIndex`=2 AND `Idx1`=1) OR (`QuestID`=13557 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13529 AND `BlobIndex`=3 AND `Idx1`=2) OR (`QuestID`=13529 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13529 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13554 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13554 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13528 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13528 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13527 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13527 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13521 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13521 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13537 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13537 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13520 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13520 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13561 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13561 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13561 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13522 AND `BlobIndex`=2 AND `Idx1`=1) OR (`QuestID`=13522 AND `BlobIndex`=1 AND `Idx1`=0) OR (`QuestID`=13518 AND `BlobIndex`=4 AND `Idx1`=4) OR (`QuestID`=13518 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=13518 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13518 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13518 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13900 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13891 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13891 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13546 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13546 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13885 AND `BlobIndex`=2 AND `Idx1`=5) OR (`QuestID`=13885 AND `BlobIndex`=4 AND `Idx1`=4) OR (`QuestID`=13885 AND `BlobIndex`=5 AND `Idx1`=3) OR (`QuestID`=13885 AND `BlobIndex`=6 AND `Idx1`=2) OR (`QuestID`=13885 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13885 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=31902 AND `BlobIndex`=0 AND `Idx1`=5) OR (`QuestID`=31902 AND `BlobIndex`=0 AND `Idx1`=4) OR (`QuestID`=31902 AND `BlobIndex`=0 AND `Idx1`=3) OR (`QuestID`=31902 AND `BlobIndex`=0 AND `Idx1`=2) OR (`QuestID`=31902 AND `BlobIndex`=0 AND `Idx1`=1) OR (`QuestID`=31902 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13925 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13925 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13925 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13899 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13898 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13898 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13572 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13572 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13545 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13545 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13953 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13895 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13893 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13882 AND `BlobIndex`=7 AND `Idx1`=7) OR (`QuestID`=13882 AND `BlobIndex`=6 AND `Idx1`=6) OR (`QuestID`=13882 AND `BlobIndex`=8 AND `Idx1`=5) OR (`QuestID`=13882 AND `BlobIndex`=4 AND `Idx1`=4) OR (`QuestID`=13882 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=13882 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13882 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13882 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13896 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13544 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13544 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13544 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13948 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13948 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13910 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13910 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13918 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13918 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13918 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13909 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13909 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13907 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13907 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13911 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13911 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13912 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13912 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13526 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13526 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13525 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13892 AND `BlobIndex`=2 AND `Idx1`=1) OR (`QuestID`=13892 AND `BlobIndex`=1 AND `Idx1`=0) OR (`QuestID`=13881 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13881 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13881 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13902 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13588 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13588 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13588 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13940 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13587 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13587 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13586 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13581 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13583 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13582 AND `BlobIndex`=2 AND `Idx1`=1) OR (`QuestID`=13582 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13578 AND `BlobIndex`=2 AND `Idx1`=1) OR (`QuestID`=13578 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13580 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13580 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13576 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13576 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13585 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13584 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13584 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13579 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13577 AND `BlobIndex`=1 AND `Idx1`=0) OR (`QuestID`=13575 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13558 AND `BlobIndex`=0 AND `Idx1`=1) OR (`QuestID`=13558 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13605 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13605 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13605 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13547 AND `BlobIndex`=5 AND `Idx1`=5) OR (`QuestID`=13547 AND `BlobIndex`=4 AND `Idx1`=4) OR (`QuestID`=13547 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=13547 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13547 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13547 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13542 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13542 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13542 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13573 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13543 AND `BlobIndex`=3 AND `Idx1`=3) OR (`QuestID`=13543 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13543 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13543 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13601 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13523 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13523 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13596 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13596 AND `BlobIndex`=0 AND `Idx1`=0) OR (`QuestID`=13519 AND `BlobIndex`=2 AND `Idx1`=2) OR (`QuestID`=13519 AND `BlobIndex`=1 AND `Idx1`=1) OR (`QuestID`=13519 AND `BlobIndex`=0 AND `Idx1`=0);
INSERT INTO `quest_poi` (`QuestID`, `BlobIndex`, `Idx1`, `ObjectiveIndex`, `QuestObjectiveID`, `QuestObjectID`, `MapID`, `WorldMapAreaId`, `Floor`, `Priority`, `Flags`, `WorldEffectID`, `PlayerConditionID`, `WoDUnk1`, `VerifiedBuild`) VALUES
(13591, 0, 0, -1, 0, 0, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13591
(13515, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13515
(13515, 0, 0, 0, 253530, 32862, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13515
(13514, 4, 2, 0, 266598, 33913, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13514
(13514, 3, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13514
(13514, 0, 0, 0, 266597, 32858, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13514
(13590, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13590
(13844, 0, 3, 32, 0, 0, 1, 42, 0, 0, 0, 0, 0, 292030, 23420), -- 13844
(13844, 0, 2, 0, 264536, 45944, 1, 42, 0, 0, 0, 0, 0, 291999, 23420), -- 13844
(13844, 0, 1, 0, 264535, 34033, 1, 42, 0, 0, 0, 0, 0, 292000, 23420), -- 13844
(13844, 0, 0, -1, 0, 0, 1, 42, 0, 0, 0, 0, 0, 292030, 23420), -- 13844
(13513, 2, 2, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13513
(13513, 1, 1, 0, 251687, 44942, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13513
(13513, 0, 0, 0, 251687, 44942, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13513
(13512, 2, 2, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13512
(13512, 1, 1, 0, 262505, 32869, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13512
(13512, 0, 0, 0, 262504, 32868, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13512
(13510, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13510
(13510, 0, 0, 30, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13510
(13511, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13511
(13511, 0, 0, 0, 267491, 32970, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13511
(13507, 2, 2, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13507
(13507, 1, 1, 0, 253228, 34248, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13507
(13507, 0, 0, 0, 253227, 32859, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13507
(13509, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13509
(13509, 0, 0, 0, 267385, 33056, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13509
(13508, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13508
(13506, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13506
(13504, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13504
(13504, 0, 0, 0, 265676, 32861, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13504
(13505, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13505
(13505, 0, 0, 0, 267309, 44830, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13505
(13560, 1, 1, 0, 256718, 32852, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13560
(13560, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13560
(13589, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13589
(13599, 0, 0, -1, 0, 0, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13599
(13569, 1, 1, 30, 0, 0, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13569
(13569, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13569
(13598, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13598
(13598, 0, 0, 0, 252950, 44976, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13598
(13566, 3, 3, 0, 266473, 45885, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13566
(13566, 2, 2, 0, 266472, 45027, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13566
(13566, 1, 1, 0, 266471, 44969, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13566
(13566, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13566
(13565, 2, 2, 0, 266154, 34010, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13565
(13565, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13565
(13565, 0, 0, 0, 266153, 33207, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13565
(13564, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13564
(13564, 0, 0, 0, 267649, -1, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13564
(13563, 0, 2, 1, 265958, 5382, 1, 42, 0, 1884546736, 0, 0, 0, 0, 23420), -- 13563
(13563, 0, 1, 0, 265957, 33181, 1, 42, 0, 0, 0, 0, 0, 0, 23420), -- 13563
(13563, 0, 0, -1, 0, 0, 1, 42, 0, 0, 0, 0, 0, 0, 23420), -- 13563
(13562, 1, 1, 30, 0, 0, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13562
(13562, 0, 0, -1, 0, 0, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13562
(13831, 1, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13831
(13557, 1, 2, 0, 256239, 44925, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13557
(13557, 2, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13557
(13557, 0, 0, 0, 256238, 33024, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13557
(13529, 3, 2, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13529
(13529, 1, 1, 0, 266621, 33021, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13529
(13529, 0, 0, 0, 266620, 33020, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13529
(13554, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13554
(13554, 0, 0, 0, 257477, 44966, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13554
(13528, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13528
(13528, 0, 0, 0, 266022, 44913, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13528
(13527, 1, 1, 0, 256073, 44911, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13527
(13527, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13527
(13521, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13521
(13521, 0, 0, 0, 266042, 44863, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13521
(13537, 1, 1, 0, 255170, 12238, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13537
(13537, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13537
(13520, 1, 1, -1, 0, 0, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13520
(13520, 0, 0, 0, 253095, 44864, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13520
(13561, 2, 2, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13561
(13561, 1, 1, 0, 266016, 33180, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13561
(13561, 0, 0, 0, 266015, 33179, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13561
(13522, 2, 1, 0, 254184, 32928, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13522
(13522, 1, 0, -1, 0, 0, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13522
(13518, 4, 4, -1, 0, 0, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13518
(13518, 3, 3, 0, 267130, 33094, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13518
(13518, 2, 2, 0, 267129, 33095, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13518
(13518, 1, 1, 0, 267128, 32911, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13518
(13518, 0, 0, 0, 267127, 33093, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13518
(13900, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13900
(13891, 1, 1, 0, 266614, 34331, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13891
(13891, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13891
(13546, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13546
(13546, 0, 0, 0, 267419, 32996, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13546
(13885, 2, 5, 0, 267644, 34324, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13885
(13885, 4, 4, 0, 267643, 34325, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13885
(13885, 5, 3, 0, 267643, 34325, 1, 42, 0, 0, 3, 0, 0, 0, 23420), -- 13885
(13885, 6, 2, 0, 267643, 34325, 1, 42, 0, 0, 3, 0, 0, 0, 23420), -- 13885
(13885, 1, 1, 0, 267642, 34323, 1, 42, 0, 0, 3, 0, 0, 0, 23420), -- 13885
(13885, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13885
(31902, 0, 5, 32, 0, 0, 0, 301, 0, 0, 0, 0, 0, 672538, 23420), -- 31902
(31902, 0, 4, 4, 269180, 66520, 0, 29, 0, 0, 0, 0, 0, 719361, 23420), -- 31902
(31902, 0, 3, 3, 269179, 66518, 0, 38, 0, 0, 0, 0, 0, 719298, 23420), -- 31902
(31902, 0, 2, 2, 269178, 66515, 0, 28, 0, 0, 0, 0, 0, 718547, 23420), -- 31902
(31902, 0, 1, 1, 269177, 66512, 0, 23, 0, 0, 0, 0, 0, 718414, 23420), -- 31902
(31902, 0, 0, 0, 269176, 66478, 0, 26, 0, 0, 0, 0, 0, 718217, 23420), -- 31902
(13925, 2, 2, 0, 266616, 34373, 1, 42, 0, 0, 3, 0, 0, 0, 23420), -- 13925
(13925, 1, 1, 0, 266616, 34373, 1, 42, 0, 0, 3, 0, 0, 0, 23420), -- 13925
(13925, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13925
(13899, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13899
(13898, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13898
(13898, 0, 0, 0, 264976, 33079, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13898
(13572, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13572
(13572, 0, 0, 0, 267505, 194150, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13572
(13545, 1, 1, 0, 264908, 33000, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13545
(13545, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13545
(13953, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13953
(13895, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13895
(13893, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13893
(13882, 7, 7, 0, 267514, 46355, 1, 42, 0, 0, 3, 0, 0, 0, 23420), -- 13882
(13882, 6, 6, 0, 267514, 46355, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13882
(13882, 8, 5, 0, 267514, 46355, 1, 42, 0, 0, 3, 0, 0, 0, 23420), -- 13882
(13882, 4, 4, 0, 267512, 46354, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13882
(13882, 3, 3, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13882
(13882, 2, 2, 0, 267513, 46356, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13882
(13882, 1, 1, 0, 267513, 46356, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13882
(13882, 0, 0, 0, 267513, 46356, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13882
(13896, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13896
(13544, 2, 2, 0, 267315, 44887, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13544
(13544, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13544
(13544, 0, 0, 27, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13544
(13948, 1, 1, 0, 264781, 34411, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13948
(13948, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13948
(13910, 1, 1, 0, 263712, 34349, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13910
(13910, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13910
(13918, 2, 2, 0, 267080, 46387, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13918
(13918, 1, 1, 28, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13918
(13918, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13918
(13909, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13909
(13909, 0, 0, 0, 265849, 46384, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13909
(13907, 1, 1, 0, 267359, 34344, 1, 42, 0, 0, 3, 0, 0, 0, 23420), -- 13907
(13907, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13907
(13911, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13911
(13911, 0, 0, 30, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13911
(13912, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13912
(13912, 0, 0, 0, 267171, 46386, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13912
(13526, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13526
(13526, 0, 0, 0, 252342, 44850, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13526
(13525, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13525
(13892, 2, 1, 0, 266852, 34410, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13892
(13892, 1, 0, 0, 266852, 34410, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13892
(13881, 2, 2, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13881
(13881, 1, 1, 0, 266217, -1, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13881
(13881, 0, 0, 0, 266216, 34302, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13881
(13902, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13902
(13588, 2, 2, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13588
(13588, 1, 1, 0, 267269, 34282, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13588
(13588, 0, 0, 0, 267268, 34316, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13588
(13940, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13940
(13587, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13587
(13587, 0, 0, 0, 253558, 46695, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13587
(13586, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13586
(13581, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13581
(13583, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13583
(13582, 2, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13582
(13582, 0, 0, 0, 266121, 46692, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13582
(13578, 2, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13578
(13578, 0, 0, 0, 257798, 44960, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13578
(13580, 1, 1, 0, 263989, 34371, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13580
(13580, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13580
(13576, 1, 1, 0, 263993, 33165, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13576
(13576, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13576
(13585, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13585
(13584, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13584
(13584, 0, 0, 0, 263710, 33083, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13584
(13579, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13579
(13577, 1, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13577
(13575, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13575
(13558, 0, 1, 0, 267388, 44929, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13558
(13558, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13558
(13605, 2, 2, 30, 0, 0, 1, 42, 0, 0, 7, 0, 0, 0, 23420), -- 13605
(13605, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13605
(13605, 0, 0, 30, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13605
(13547, 5, 5, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13547
(13547, 4, 4, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13547
(13547, 3, 3, 0, 266057, 33037, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13547
(13547, 2, 2, 0, 266056, 33035, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13547
(13547, 1, 1, 0, 266055, 33033, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13547
(13547, 0, 0, 0, 266054, 33001, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13547
(13542, 2, 2, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13542
(13542, 1, 1, 27, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13542
(13542, 0, 0, 0, 252588, 32986, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13542
(13573, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13573
(13543, 3, 3, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13543
(13543, 2, 2, 0, 254169, 32990, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13543
(13543, 1, 1, 0, 254168, 32989, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13543
(13543, 0, 0, 0, 254167, 32988, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13543
(13601, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13601
(13523, 1, 1, 0, 267522, 32937, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13523
(13523, 0, 0, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13523
(13596, 1, 1, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13596
(13596, 0, 0, 0, 253259, 44968, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13596
(13519, 2, 2, -1, 0, 0, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13519
(13519, 1, 1, 0, 264751, 32888, 1, 42, 0, 0, 1, 0, 0, 0, 23420), -- 13519
(13519, 0, 0, 0, 264750, 32899, 1, 42, 0, 0, 1, 0, 0, 0, 23420); -- 13519

DELETE FROM `quest_poi_points` WHERE (`QuestID`=13591 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13515 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13515 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=11) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=10) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=9) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=8) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=7) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=6) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=13514 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13514 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13514 AND `Idx1`=0 AND `Idx2`=9) OR (`QuestID`=13514 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=13514 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13514 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13514 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13514 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13514 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13514 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13514 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13514 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13590 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13844 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=13844 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13844 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13844 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13513 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13513 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13513 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13513 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13513 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13513 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13513 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13513 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13513 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13513 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13513 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13513 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13513 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13512 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13512 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13512 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13510 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13510 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13510 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13510 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13510 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13511 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13511 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13507 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13507 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13507 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13507 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13507 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13507 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13507 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13507 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13507 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13507 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13507 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13507 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13507 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13507 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13509 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13509 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13509 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13509 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13509 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13509 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13509 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13508 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13506 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13504 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13504 AND `Idx1`=0 AND `Idx2`=10) OR (`QuestID`=13504 AND `Idx1`=0 AND `Idx2`=9) OR (`QuestID`=13504 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=13504 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13504 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13504 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13504 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13504 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13504 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13504 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13504 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13505 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=11) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=10) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=9) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13505 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13560 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13560 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13589 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13599 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13569 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13569 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13598 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13598 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13598 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13598 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13598 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13598 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13598 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13598 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13598 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=11) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=10) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=9) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=8) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=7) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=6) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=5) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=4) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=3) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=2) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=1) OR (`QuestID`=13566 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=11) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=10) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=9) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=8) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=7) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=6) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=13566 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13566 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13566 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=11) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=10) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=9) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=8) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=7) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=6) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=13565 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13565 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13565 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13564 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13564 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13564 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13564 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13564 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13563 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13563 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13563 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13562 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13562 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13831 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13557 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=13557 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=13557 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=13557 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13557 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13557 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13557 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13557 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13557 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13557 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13557 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13529 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13529 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=13529 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13529 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13529 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13529 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13529 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13529 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13529 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13529 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13554 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13554 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=13554 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13554 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13554 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13554 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13554 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13554 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13554 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13554 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13528 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13528 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13528 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13528 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13528 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13528 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13528 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13528 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13528 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13527 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13527 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13521 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13521 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13521 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13521 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13521 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13521 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13521 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13521 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13537 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13537 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13520 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=11) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=10) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=9) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13520 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13561 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13561 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=13561 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13561 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13561 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13561 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13561 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13561 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13561 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13561 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13561 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13561 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13561 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13561 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13561 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13522 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13522 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13518 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=13518 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=13518 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13518 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13518 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13900 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13891 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13891 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13546 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13546 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13885 AND `Idx1`=5 AND `Idx2`=7) OR (`QuestID`=13885 AND `Idx1`=5 AND `Idx2`=6) OR (`QuestID`=13885 AND `Idx1`=5 AND `Idx2`=5) OR (`QuestID`=13885 AND `Idx1`=5 AND `Idx2`=4) OR (`QuestID`=13885 AND `Idx1`=5 AND `Idx2`=3) OR (`QuestID`=13885 AND `Idx1`=5 AND `Idx2`=2) OR (`QuestID`=13885 AND `Idx1`=5 AND `Idx2`=1) OR (`QuestID`=13885 AND `Idx1`=5 AND `Idx2`=0) OR (`QuestID`=13885 AND `Idx1`=4 AND `Idx2`=5) OR (`QuestID`=13885 AND `Idx1`=4 AND `Idx2`=4) OR (`QuestID`=13885 AND `Idx1`=4 AND `Idx2`=3) OR (`QuestID`=13885 AND `Idx1`=4 AND `Idx2`=2) OR (`QuestID`=13885 AND `Idx1`=4 AND `Idx2`=1) OR (`QuestID`=13885 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=13885 AND `Idx1`=3 AND `Idx2`=3) OR (`QuestID`=13885 AND `Idx1`=3 AND `Idx2`=2) OR (`QuestID`=13885 AND `Idx1`=3 AND `Idx2`=1) OR (`QuestID`=13885 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=13885 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=13885 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=13885 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=13885 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13885 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=13885 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13885 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13885 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13885 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13885 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13885 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13885 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13885 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=31902 AND `Idx1`=5 AND `Idx2`=0) OR (`QuestID`=31902 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=31902 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=31902 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=31902 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=31902 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=11) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=10) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=9) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=8) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=7) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=6) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=13925 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13925 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13925 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13925 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13925 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13925 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13925 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13899 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13898 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13898 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=13898 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13898 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13898 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13898 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13898 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13898 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13898 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13898 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13572 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13572 AND `Idx1`=0 AND `Idx2`=10) OR (`QuestID`=13572 AND `Idx1`=0 AND `Idx2`=9) OR (`QuestID`=13572 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=13572 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13572 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13572 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13572 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13572 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13572 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13572 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13572 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13545 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13545 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13953 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13895 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13893 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13882 AND `Idx1`=7 AND `Idx2`=2) OR (`QuestID`=13882 AND `Idx1`=7 AND `Idx2`=1) OR (`QuestID`=13882 AND `Idx1`=7 AND `Idx2`=0) OR (`QuestID`=13882 AND `Idx1`=6 AND `Idx2`=6) OR (`QuestID`=13882 AND `Idx1`=6 AND `Idx2`=5) OR (`QuestID`=13882 AND `Idx1`=6 AND `Idx2`=4) OR (`QuestID`=13882 AND `Idx1`=6 AND `Idx2`=3) OR (`QuestID`=13882 AND `Idx1`=6 AND `Idx2`=2) OR (`QuestID`=13882 AND `Idx1`=6 AND `Idx2`=1) OR (`QuestID`=13882 AND `Idx1`=6 AND `Idx2`=0) OR (`QuestID`=13882 AND `Idx1`=5 AND `Idx2`=3) OR (`QuestID`=13882 AND `Idx1`=5 AND `Idx2`=2) OR (`QuestID`=13882 AND `Idx1`=5 AND `Idx2`=1) OR (`QuestID`=13882 AND `Idx1`=5 AND `Idx2`=0) OR (`QuestID`=13882 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=13882 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=13882 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=13882 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=13882 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=13882 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=13882 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=13882 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13882 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13882 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13882 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13882 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13882 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13882 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13882 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13882 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13882 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13896 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13544 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13544 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13544 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13948 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13948 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13910 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13910 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=11) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=10) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=9) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=8) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=7) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=6) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=5) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=4) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=3) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=2) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=1) OR (`QuestID`=13918 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13918 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13918 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13909 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13909 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13909 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13909 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13909 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13909 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13909 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13907 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13907 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13907 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13907 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13907 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13907 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13907 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13907 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13911 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13911 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13911 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13911 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13911 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13912 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13912 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13526 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13526 AND `Idx1`=0 AND `Idx2`=10) OR (`QuestID`=13526 AND `Idx1`=0 AND `Idx2`=9) OR (`QuestID`=13526 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=13526 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13526 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13526 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13526 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13526 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13526 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13526 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13526 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13525 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13892 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13881 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13881 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13881 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13881 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13881 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13881 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13881 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13881 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13881 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13881 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13881 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13881 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13902 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13588 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13588 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13588 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13588 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13588 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13588 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13588 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13588 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13940 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13587 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13587 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13586 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13581 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13583 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13582 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13582 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13578 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13578 AND `Idx1`=0 AND `Idx2`=9) OR (`QuestID`=13578 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=13578 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13578 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13578 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13578 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13578 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13578 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13578 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13578 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13580 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13580 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13576 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=13576 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=13576 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=13576 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=13576 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13576 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13576 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13576 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13576 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13576 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13576 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13576 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13585 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13584 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13584 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13584 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13584 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13584 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13584 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13584 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13584 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13579 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13577 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13575 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13558 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13558 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13605 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13605 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13605 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13605 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13605 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13605 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13547 AND `Idx1`=5 AND `Idx2`=0) OR (`QuestID`=13547 AND `Idx1`=4 AND `Idx2`=0) OR (`QuestID`=13547 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=13547 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13547 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13547 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13542 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13542 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13542 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13573 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13543 AND `Idx1`=3 AND `Idx2`=0) OR (`QuestID`=13543 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13543 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13543 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13601 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13523 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13523 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13523 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13523 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13523 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13523 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13523 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13523 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13596 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13596 AND `Idx1`=0 AND `Idx2`=9) OR (`QuestID`=13596 AND `Idx1`=0 AND `Idx2`=8) OR (`QuestID`=13596 AND `Idx1`=0 AND `Idx2`=7) OR (`QuestID`=13596 AND `Idx1`=0 AND `Idx2`=6) OR (`QuestID`=13596 AND `Idx1`=0 AND `Idx2`=5) OR (`QuestID`=13596 AND `Idx1`=0 AND `Idx2`=4) OR (`QuestID`=13596 AND `Idx1`=0 AND `Idx2`=3) OR (`QuestID`=13596 AND `Idx1`=0 AND `Idx2`=2) OR (`QuestID`=13596 AND `Idx1`=0 AND `Idx2`=1) OR (`QuestID`=13596 AND `Idx1`=0 AND `Idx2`=0) OR (`QuestID`=13519 AND `Idx1`=2 AND `Idx2`=0) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=11) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=10) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=9) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=8) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=7) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=6) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=5) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=4) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=3) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=2) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=1) OR (`QuestID`=13519 AND `Idx1`=1 AND `Idx2`=0) OR (`QuestID`=13519 AND `Idx1`=0 AND `Idx2`=0);
INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(13591, 0, 0, 7392, -250, 23420), -- 13591
(13515, 1, 0, 7400, -1656, 23420), -- 13515
(13515, 0, 0, 7444, -1692, 23420), -- 13515
(13514, 2, 11, 7318, -1662, 23420), -- 13514
(13514, 2, 10, 7307, -1605, 23420), -- 13514
(13514, 2, 9, 7341, -1532, 23420), -- 13514
(13514, 2, 8, 7392, -1481, 23420), -- 13514
(13514, 2, 7, 7437, -1470, 23420), -- 13514
(13514, 2, 6, 7505, -1470, 23420), -- 13514
(13514, 2, 5, 7544, -1515, 23420), -- 13514
(13514, 2, 4, 7550, -1571, 23420), -- 13514
(13514, 2, 3, 7527, -1617, 23420), -- 13514
(13514, 2, 2, 7471, -1679, 23420), -- 13514
(13514, 2, 1, 7414, -1718, 23420), -- 13514
(13514, 2, 0, 7363, -1730, 23420), -- 13514
(13514, 1, 0, 7393, -1450, 23420), -- 13514
(13514, 0, 9, 7288, -1580, 23420), -- 13514
(13514, 0, 8, 7293, -1540, 23420), -- 13514
(13514, 0, 7, 7295, -1529, 23420), -- 13514
(13514, 0, 6, 7314, -1496, 23420), -- 13514
(13514, 0, 5, 7317, -1491, 23420), -- 13514
(13514, 0, 4, 7393, -1452, 23420), -- 13514
(13514, 0, 3, 7574, -1533, 23420), -- 13514
(13514, 0, 2, 7585, -1557, 23420), -- 13514
(13514, 0, 1, 7444, -1690, 23420), -- 13514
(13514, 0, 0, 7441, -1692, 23420), -- 13514
(13590, 0, 0, 7400, -1656, 23420), -- 13590
(13844, 3, 0, 7377, -806, 23420), -- 13844
(13844, 2, 0, 7190, -751, 23420), -- 13844
(13844, 1, 0, 7185, -733, 23420), -- 13844
(13844, 0, 0, 7377, -806, 23420), -- 13844
(13513, 2, 0, 7381, -790, 23420), -- 13513
(13513, 1, 6, 7129, -802, 23420), -- 13513
(13513, 1, 5, 7108, -670, 23420), -- 13513
(13513, 1, 4, 7121, -653, 23420), -- 13513
(13513, 1, 3, 7157, -658, 23420), -- 13513
(13513, 1, 2, 7196, -784, 23420), -- 13513
(13513, 1, 1, 7186, -811, 23420), -- 13513
(13513, 1, 0, 7167, -825, 23420), -- 13513
(13513, 0, 4, 7328, -941, 23420), -- 13513
(13513, 0, 3, 7320, -933, 23420), -- 13513
(13513, 0, 2, 7350, -917, 23420), -- 13513
(13513, 0, 1, 7363, -942, 23420), -- 13513
(13513, 0, 0, 7346, -959, 23420), -- 13513
(13512, 2, 0, 7385, -792, 23420), -- 13512
(13512, 1, 0, 7346, -942, 23420), -- 13512
(13512, 0, 0, 7103, -649, 23420), -- 13512
(13510, 1, 0, 7385, -792, 23420), -- 13510
(13510, 0, 3, 7921, -1153, 23420), -- 13510
(13510, 0, 2, 7988, -1086, 23420), -- 13510
(13510, 0, 1, 8055, -1153, 23420), -- 13510
(13510, 0, 0, 7988, -1220, 23420), -- 13510
(13511, 1, 0, 7385, -792, 23420), -- 13511
(13511, 0, 0, 7995, -1128, 23420), -- 13511
(13507, 2, 0, 7386, -791, 23420), -- 13507
(13507, 1, 5, 7727, -1028, 23420), -- 13507
(13507, 1, 4, 7827, -936, 23420), -- 13507
(13507, 1, 3, 7875, -950, 23420), -- 13507
(13507, 1, 2, 7954, -1050, 23420), -- 13507
(13507, 1, 1, 7918, -1116, 23420), -- 13507
(13507, 1, 0, 7869, -1124, 23420), -- 13507
(13507, 0, 6, 7715, -1046, 23420), -- 13507
(13507, 0, 5, 7703, -957, 23420), -- 13507
(13507, 0, 4, 7743, -894, 23420), -- 13507
(13507, 0, 3, 7915, -901, 23420), -- 13507
(13507, 0, 2, 7910, -1033, 23420), -- 13507
(13507, 0, 1, 7884, -1073, 23420), -- 13507
(13507, 0, 0, 7760, -1087, 23420), -- 13507
(13509, 1, 0, 7385, -792, 23420), -- 13509
(13509, 0, 5, 7771, -1074, 23420), -- 13509
(13509, 0, 4, 7733, -1040, 23420), -- 13509
(13509, 0, 3, 7730, -995, 23420), -- 13509
(13509, 0, 2, 7766, -943, 23420), -- 13509
(13509, 0, 1, 7936, -880, 23420), -- 13509
(13509, 0, 0, 7992, -1151, 23420), -- 13509
(13508, 0, 0, 7965, -1104, 23420), -- 13508
(13506, 0, 0, 7385, -792, 23420), -- 13506
(13504, 1, 0, 7386, -791, 23420), -- 13504
(13504, 0, 10, 7314, -1046, 23420), -- 13504
(13504, 0, 9, 7252, -957, 23420), -- 13504
(13504, 0, 8, 7239, -929, 23420), -- 13504
(13504, 0, 7, 7234, -889, 23420), -- 13504
(13504, 0, 6, 7259, -832, 23420), -- 13504
(13504, 0, 5, 7365, -771, 23420), -- 13504
(13504, 0, 4, 7538, -820, 23420), -- 13504
(13504, 0, 3, 7581, -835, 23420), -- 13504
(13504, 0, 2, 7732, -914, 23420), -- 13504
(13504, 0, 1, 7782, -1083, 23420), -- 13504
(13504, 0, 0, 7391, -1123, 23420), -- 13504
(13505, 1, 0, 7381, -790, 23420), -- 13505
(13505, 0, 11, 7389, -1117, 23420), -- 13505
(13505, 0, 10, 7315, -1048, 23420), -- 13505
(13505, 0, 9, 7239, -952, 23420), -- 13505
(13505, 0, 8, 7194, -861, 23420), -- 13505
(13505, 0, 7, 7304, -820, 23420), -- 13505
(13505, 0, 6, 7456, -801, 23420), -- 13505
(13505, 0, 5, 7534, -819, 23420), -- 13505
(13505, 0, 4, 7576, -841, 23420), -- 13505
(13505, 0, 3, 7697, -912, 23420), -- 13505
(13505, 0, 2, 7673, -995, 23420), -- 13505
(13505, 0, 1, 7623, -1064, 23420), -- 13505
(13505, 0, 0, 7452, -1119, 23420), -- 13505
(13560, 1, 0, 7745, -408, 23420), -- 13560
(13560, 0, 0, 7396, -280, 23420), -- 13560
(13589, 0, 0, 7385, -792, 23420), -- 13589
(13599, 0, 0, 7420, -261, 23420), -- 13599
(13569, 1, 0, 6573, 253, 23420), -- 13569
(13569, 0, 0, 6542, 239, 23420), -- 13569
(13598, 1, 0, 6542, 239, 23420), -- 13598
(13598, 0, 7, 6474, -165, 23420), -- 13598
(13598, 0, 6, 6315, -108, 23420), -- 13598
(13598, 0, 5, 6318, -63, 23420), -- 13598
(13598, 0, 4, 6422, -25, 23420), -- 13598
(13598, 0, 3, 6515, -36, 23420), -- 13598
(13598, 0, 2, 6543, -68, 23420), -- 13598
(13598, 0, 1, 6567, -149, 23420), -- 13598
(13598, 0, 0, 6571, -176, 23420), -- 13598
(13566, 3, 11, 6499, -119, 23420), -- 13566
(13566, 3, 10, 6482, -63, 23420), -- 13566
(13566, 3, 9, 6482, -6, 23420), -- 13566
(13566, 3, 8, 6482, 21, 23420), -- 13566
(13566, 3, 7, 6516, 72, 23420), -- 13566
(13566, 3, 6, 6583, 83, 23420), -- 13566
(13566, 3, 5, 6600, 44, 23420), -- 13566
(13566, 3, 4, 6606, 0, 23420), -- 13566
(13566, 3, 3, 6612, -63, 23420), -- 13566
(13566, 3, 2, 6612, -119, 23420), -- 13566
(13566, 3, 1, 6572, -176, 23420), -- 13566
(13566, 3, 0, 6533, -176, 23420), -- 13566
(13566, 2, 11, 6431, 83, 23420), -- 13566
(13566, 2, 10, 6431, 140, 23420), -- 13566
(13566, 2, 9, 6453, 208, 23420), -- 13566
(13566, 2, 8, 6487, 259, 23420), -- 13566
(13566, 2, 7, 6533, 259, 23420), -- 13566
(13566, 2, 6, 6561, 230, 23420), -- 13566
(13566, 2, 5, 6561, 168, 23420), -- 13566
(13566, 2, 4, 6544, 134, 23420), -- 13566
(13566, 2, 3, 6533, 112, 23420), -- 13566
(13566, 2, 2, 6516, 78, 23420), -- 13566
(13566, 2, 1, 6476, 49, 23420), -- 13566
(13566, 2, 0, 6436, 49, 23420), -- 13566
(13566, 1, 11, 6578, 338, 23420), -- 13566
(13566, 1, 10, 6583, 411, 23420), -- 13566
(13566, 1, 9, 6634, 490, 23420), -- 13566
(13566, 1, 8, 6679, 524, 23420), -- 13566
(13566, 1, 7, 6719, 524, 23420), -- 13566
(13566, 1, 6, 6787, 524, 23420), -- 13566
(13566, 1, 5, 6815, 502, 23420), -- 13566
(13566, 1, 4, 6821, 439, 23420), -- 13566
(13566, 1, 3, 6787, 389, 23420), -- 13566
(13566, 1, 2, 6747, 332, 23420), -- 13566
(13566, 1, 1, 6691, 281, 23420), -- 13566
(13566, 1, 0, 6634, 264, 23420), -- 13566
(13566, 0, 0, 6542, 239, 23420), -- 13566
(13565, 2, 11, 6380, -164, 23420), -- 13565
(13565, 2, 10, 6374, -68, 23420), -- 13565
(13565, 2, 9, 6374, 4, 23420), -- 13565
(13565, 2, 8, 6448, 49, 23420), -- 13565
(13565, 2, 7, 6516, 49, 23420), -- 13565
(13565, 2, 6, 6544, 27, 23420), -- 13565
(13565, 2, 5, 6589, -23, 23420), -- 13565
(13565, 2, 4, 6600, -74, 23420), -- 13565
(13565, 2, 3, 6595, -136, 23420), -- 13565
(13565, 2, 2, 6572, -176, 23420), -- 13565
(13565, 2, 1, 6499, -209, 23420), -- 13565
(13565, 2, 0, 6448, -209, 23420), -- 13565
(13565, 1, 0, 6544, 241, 23420), -- 13565
(13565, 0, 0, 6482, -123, 23420), -- 13565
(13564, 1, 0, 6542, 239, 23420), -- 13564
(13564, 0, 3, 6471, 242, 23420), -- 13564
(13564, 0, 2, 6538, 309, 23420), -- 13564
(13564, 0, 1, 6605, 242, 23420), -- 13564
(13564, 0, 0, 6538, 175, 23420), -- 13564
(13563, 2, 0, 6629, -58, 23420), -- 13563
(13563, 1, 0, 6629, -58, 23420), -- 13563
(13563, 0, 0, 7384, -224, 23420), -- 13563
(13562, 1, 0, 6753, 33, 23420), -- 13562
(13562, 0, 0, 7403, -255, 23420), -- 13562
(13831, 0, 0, 7373, -289, 23420), -- 13831
(13557, 2, 3, 6764, -706, 23420), -- 13557
(13557, 2, 2, 6772, -649, 23420), -- 13557
(13557, 2, 1, 6828, -691, 23420), -- 13557
(13557, 2, 0, 6818, -782, 23420), -- 13557
(13557, 1, 0, 7373, -289, 23420), -- 13557
(13557, 0, 5, 6746, -705, 23420), -- 13557
(13557, 0, 4, 6716, -641, 23420), -- 13557
(13557, 0, 3, 6839, -680, 23420), -- 13557
(13557, 0, 2, 6857, -693, 23420), -- 13557
(13557, 0, 1, 6876, -771, 23420), -- 13557
(13557, 0, 0, 6802, -776, 23420), -- 13557
(13529, 2, 0, 7373, -289, 23420), -- 13529
(13529, 1, 7, 6806, -776, 23420), -- 13529
(13529, 1, 6, 6719, -693, 23420), -- 13529
(13529, 1, 5, 6717, -646, 23420), -- 13529
(13529, 1, 4, 6739, -640, 23420), -- 13529
(13529, 1, 3, 6874, -634, 23420), -- 13529
(13529, 1, 2, 6898, -636, 23420), -- 13529
(13529, 1, 1, 6876, -767, 23420), -- 13529
(13529, 1, 0, 6858, -796, 23420), -- 13529
(13529, 0, 0, 6798, -747, 23420), -- 13529
(13554, 1, 0, 7375, -289, 23420), -- 13554
(13554, 0, 8, 6818, -782, 23420), -- 13554
(13554, 0, 7, 6806, -776, 23420), -- 13554
(13554, 0, 6, 6719, -693, 23420), -- 13554
(13554, 0, 5, 6717, -646, 23420), -- 13554
(13554, 0, 4, 6739, -640, 23420), -- 13554
(13554, 0, 3, 6874, -634, 23420), -- 13554
(13554, 0, 2, 6898, -636, 23420), -- 13554
(13554, 0, 1, 6876, -767, 23420), -- 13554
(13554, 0, 0, 6858, -796, 23420), -- 13554
(13528, 1, 0, 6963, -485, 23420), -- 13528
(13528, 0, 7, 7155, -609, 23420), -- 13528
(13528, 0, 6, 6952, -555, 23420), -- 13528
(13528, 0, 5, 6897, -526, 23420), -- 13528
(13528, 0, 4, 6889, -505, 23420), -- 13528
(13528, 0, 3, 6957, -360, 23420), -- 13528
(13528, 0, 2, 7154, -387, 23420), -- 13528
(13528, 0, 1, 7371, -586, 23420), -- 13528
(13528, 0, 0, 7331, -645, 23420), -- 13528
(13527, 1, 0, 7318, -545, 23420), -- 13527
(13527, 0, 0, 7375, -289, 23420), -- 13527
(13521, 1, 0, 7376, -425, 23420), -- 13521
(13521, 0, 6, 7294, -338, 23420), -- 13521
(13521, 0, 5, 7401, -15, 23420), -- 13521
(13521, 0, 4, 7507, -30, 23420), -- 13521
(13521, 0, 3, 7533, -112, 23420), -- 13521
(13521, 0, 2, 7582, -278, 23420), -- 13521
(13521, 0, 1, 7580, -541, 23420), -- 13521
(13521, 0, 0, 7317, -544, 23420), -- 13521
(13537, 1, 11, 7341, -407, 23420), -- 13537
(13537, 1, 10, 7329, -362, 23420), -- 13537
(13537, 1, 9, 7358, -328, 23420), -- 13537
(13537, 1, 8, 7442, -311, 23420), -- 13537
(13537, 1, 7, 7488, -311, 23420), -- 13537
(13537, 1, 6, 7516, -322, 23420), -- 13537
(13537, 1, 5, 7522, -339, 23420), -- 13537
(13537, 1, 4, 7522, -379, 23420), -- 13537
(13537, 1, 3, 7493, -407, 23420), -- 13537
(13537, 1, 2, 7442, -430, 23420), -- 13537
(13537, 1, 1, 7403, -430, 23420), -- 13537
(13537, 1, 0, 7369, -430, 23420), -- 13537
(13537, 0, 0, 7449, -370, 23420), -- 13537
(13520, 1, 0, 7415, -250, 23420), -- 13520
(13520, 0, 11, 7422, -424, 23420), -- 13520
(13520, 0, 10, 7383, -355, 23420), -- 13520
(13520, 0, 9, 7408, -192, 23420), -- 13520
(13520, 0, 8, 7421, -184, 23420), -- 13520
(13520, 0, 7, 7446, -175, 23420), -- 13520
(13520, 0, 6, 7459, -171, 23420), -- 13520
(13520, 0, 5, 7490, -164, 23420); -- 13520

INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(13520, 0, 4, 7555, -179, 23420), -- 13520
(13520, 0, 3, 7587, -311, 23420), -- 13520
(13520, 0, 2, 7569, -377, 23420), -- 13520
(13520, 0, 1, 7535, -445, 23420), -- 13520
(13520, 0, 0, 7471, -484, 23420), -- 13520
(13561, 2, 0, 6788, -9, 23420), -- 13561
(13561, 1, 7, 6639, -130, 23420), -- 13561
(13561, 1, 6, 6617, -67, 23420), -- 13561
(13561, 1, 5, 6631, -35, 23420), -- 13561
(13561, 1, 4, 6663, 14, 23420), -- 13561
(13561, 1, 3, 6740, 24, 23420), -- 13561
(13561, 1, 2, 6768, -101, 23420), -- 13561
(13561, 1, 1, 6750, -174, 23420), -- 13561
(13561, 1, 0, 6701, -174, 23420), -- 13561
(13561, 0, 5, 6617, -88, 23420), -- 13561
(13561, 0, 4, 6597, -30, 23420), -- 13561
(13561, 0, 3, 6685, 23, 23420), -- 13561
(13561, 0, 2, 6734, 27, 23420), -- 13561
(13561, 0, 1, 6753, -135, 23420), -- 13561
(13561, 0, 0, 6732, -206, 23420), -- 13561
(13522, 1, 11, 7329, 89, 23420), -- 13522
(13522, 1, 10, 7318, 129, 23420), -- 13522
(13522, 1, 9, 7318, 185, 23420), -- 13522
(13522, 1, 8, 7318, 230, 23420), -- 13522
(13522, 1, 7, 7369, 281, 23420), -- 13522
(13522, 1, 6, 7420, 275, 23420), -- 13522
(13522, 1, 5, 7465, 225, 23420), -- 13522
(13522, 1, 4, 7488, 157, 23420), -- 13522
(13522, 1, 3, 7499, 100, 23420), -- 13522
(13522, 1, 2, 7471, 16, 23420), -- 13522
(13522, 1, 1, 7437, -23, 23420), -- 13522
(13522, 1, 0, 7369, -46, 23420), -- 13522
(13522, 0, 0, 7420, -261, 23420), -- 13522
(13518, 4, 0, 7420, -283, 23420), -- 13518
(13518, 3, 0, 7439, 106, 23420), -- 13518
(13518, 2, 0, 7296, 243, 23420), -- 13518
(13518, 1, 0, 7456, 165, 23420), -- 13518
(13518, 0, 0, 7366, 135, 23420), -- 13518
(13900, 0, 0, 4571, 871, 23420), -- 13900
(13891, 1, 0, 4811, 103, 23420), -- 13891
(13891, 0, 0, 5007, 95, 23420), -- 13891
(13546, 1, 0, 4731, 204, 23420), -- 13546
(13546, 0, 0, 4601, -8, 23420), -- 13546
(13885, 5, 7, 5273, 50, 23420), -- 13885
(13885, 5, 6, 5131, 100, 23420), -- 13885
(13885, 5, 5, 5045, 178, 23420), -- 13885
(13885, 5, 4, 4983, 310, 23420), -- 13885
(13885, 5, 3, 5021, 411, 23420), -- 13885
(13885, 5, 2, 5104, 442, 23420), -- 13885
(13885, 5, 1, 5502, 184, 23420), -- 13885
(13885, 5, 0, 5327, 36, 23420), -- 13885
(13885, 4, 5, 5534, 139, 23420), -- 13885
(13885, 4, 4, 5186, 338, 23420), -- 13885
(13885, 4, 3, 5201, 457, 23420), -- 13885
(13885, 4, 2, 5422, 481, 23420), -- 13885
(13885, 4, 1, 5566, 254, 23420), -- 13885
(13885, 4, 0, 5577, 131, 23420), -- 13885
(13885, 3, 3, 4226, 254, 23420), -- 13885
(13885, 3, 2, 4161, 448, 23420), -- 13885
(13885, 3, 1, 4370, 335, 23420), -- 13885
(13885, 3, 0, 4326, 232, 23420), -- 13885
(13885, 2, 3, 4276, 11, 23420), -- 13885
(13885, 2, 2, 4277, 104, 23420), -- 13885
(13885, 2, 1, 4398, 110, 23420), -- 13885
(13885, 2, 0, 4337, 1, 23420), -- 13885
(13885, 1, 7, 4688, 355, 23420), -- 13885
(13885, 1, 6, 4685, 399, 23420), -- 13885
(13885, 1, 5, 4736, 466, 23420), -- 13885
(13885, 1, 4, 4827, 475, 23420), -- 13885
(13885, 1, 3, 5043, 499, 23420), -- 13885
(13885, 1, 2, 5043, 344, 23420), -- 13885
(13885, 1, 1, 4874, 183, 23420), -- 13885
(13885, 1, 0, 4762, 170, 23420), -- 13885
(13885, 0, 0, 5007, 95, 23420), -- 13885
(31902, 5, 0, -8287, 515, 23420), -- 31902
(31902, 4, 0, -7983, -1270, 23420), -- 31902
(31902, 3, 0, -10230, -4008, 23420), -- 31902
(31902, 2, 0, -6513, -1111, 23420), -- 31902
(31902, 1, 0, 2295, -4987, 23420), -- 31902
(31902, 0, 0, 66, -4000, 23420), -- 31902
(13925, 2, 11, 4762, 170, 23420), -- 13925
(13925, 2, 10, 4688, 355, 23420), -- 13925
(13925, 2, 9, 4736, 466, 23420), -- 13925
(13925, 2, 8, 4822, 539, 23420), -- 13925
(13925, 2, 7, 5032, 563, 23420), -- 13925
(13925, 2, 6, 5147, 487, 23420), -- 13925
(13925, 2, 5, 5340, 487, 23420), -- 13925
(13925, 2, 4, 5398, 466, 23420), -- 13925
(13925, 2, 3, 5472, 346, 23420), -- 13925
(13925, 2, 2, 5513, 269, 23420), -- 13925
(13925, 2, 1, 5464, 136, 23420), -- 13925
(13925, 2, 0, 5327, 36, 23420), -- 13925
(13925, 1, 4, 4358, 432, 23420), -- 13925
(13925, 1, 3, 4445, 382, 23420), -- 13925
(13925, 1, 2, 4497, 202, 23420), -- 13925
(13925, 1, 1, 4436, 108, 23420), -- 13925
(13925, 1, 0, 4318, 124, 23420), -- 13925
(13925, 0, 0, 5007, 95, 23420), -- 13925
(13899, 0, 0, 4540, 931, 23420), -- 13899
(13898, 1, 0, 4985, 85, 23420), -- 13898
(13898, 0, 8, 4595, 796, 23420), -- 13898
(13898, 0, 7, 4567, 814, 23420), -- 13898
(13898, 0, 6, 4489, 893, 23420), -- 13898
(13898, 0, 5, 4517, 939, 23420), -- 13898
(13898, 0, 4, 4595, 1045, 23420), -- 13898
(13898, 0, 3, 4647, 976, 23420), -- 13898
(13898, 0, 2, 4691, 915, 23420), -- 13898
(13898, 0, 1, 4670, 806, 23420), -- 13898
(13898, 0, 0, 4657, 793, 23420), -- 13898
(13572, 1, 0, 4730, 199, 23420), -- 13572
(13572, 0, 10, 4462, 85, 23420), -- 13572
(13572, 0, 9, 4465, 134, 23420), -- 13572
(13572, 0, 8, 4469, 150, 23420), -- 13572
(13572, 0, 7, 4549, 184, 23420), -- 13572
(13572, 0, 6, 4573, 181, 23420), -- 13572
(13572, 0, 5, 4652, 170, 23420), -- 13572
(13572, 0, 4, 4696, 141, 23420), -- 13572
(13572, 0, 3, 4684, 72, 23420), -- 13572
(13572, 0, 2, 4673, 35, 23420), -- 13572
(13572, 0, 1, 4636, 19, 23420), -- 13572
(13572, 0, 0, 4471, -5, 23420), -- 13572
(13545, 1, 11, 4643, -23, 23420), -- 13545
(13545, 1, 10, 4603, 29, 23420), -- 13545
(13545, 1, 9, 4576, 83, 23420), -- 13545
(13545, 1, 8, 4562, 130, 23420), -- 13545
(13545, 1, 7, 4582, 184, 23420), -- 13545
(13545, 1, 6, 4629, 204, 23420), -- 13545
(13545, 1, 5, 4690, 198, 23420), -- 13545
(13545, 1, 4, 4764, 171, 23420), -- 13545
(13545, 1, 3, 4791, 111, 23420), -- 13545
(13545, 1, 2, 4778, 42, 23420), -- 13545
(13545, 1, 1, 4717, -24, 23420), -- 13545
(13545, 1, 0, 4684, -23, 23420), -- 13545
(13545, 0, 0, 4731, 204, 23420), -- 13545
(13953, 0, 0, 4984, 88, 23420), -- 13953
(13895, 0, 0, 5134, 64, 23420), -- 13895
(13893, 0, 0, 4988, 87, 23420), -- 13893
(13882, 7, 2, 4217, 452, 23420), -- 13882
(13882, 7, 1, 4312, 516, 23420), -- 13882
(13882, 7, 0, 4347, 360, 23420), -- 13882
(13882, 6, 6, 4667, 171, 23420), -- 13882
(13882, 6, 5, 4620, 233, 23420), -- 13882
(13882, 6, 4, 4732, 357, 23420), -- 13882
(13882, 6, 3, 4811, 420, 23420), -- 13882
(13882, 6, 2, 5196, 322, 23420), -- 13882
(13882, 6, 1, 5167, 225, 23420), -- 13882
(13882, 6, 0, 4871, 71, 23420), -- 13882
(13882, 5, 3, 4327, 170, 23420), -- 13882
(13882, 5, 2, 4411, 299, 23420), -- 13882
(13882, 5, 1, 4520, 214, 23420), -- 13882
(13882, 5, 0, 4387, 85, 23420), -- 13882
(13882, 4, 0, 4996, 81, 23420), -- 13882
(13882, 3, 0, 5007, 95, 23420), -- 13882
(13882, 2, 5, 4758, 574, 23420), -- 13882
(13882, 2, 4, 4816, 636, 23420), -- 13882
(13882, 2, 3, 4948, 679, 23420), -- 13882
(13882, 2, 2, 5016, 561, 23420), -- 13882
(13882, 2, 1, 4851, 520, 23420), -- 13882
(13882, 2, 0, 4779, 520, 23420), -- 13882
(13882, 1, 3, 4584, 734, 23420), -- 13882
(13882, 1, 2, 4632, 734, 23420), -- 13882
(13882, 1, 1, 4644, 696, 23420), -- 13882
(13882, 1, 0, 4543, 674, 23420), -- 13882
(13882, 0, 4, 5174, 647, 23420), -- 13882
(13882, 0, 3, 5104, 726, 23420), -- 13882
(13882, 0, 2, 5445, 668, 23420), -- 13882
(13882, 0, 1, 5414, 635, 23420), -- 13882
(13882, 0, 0, 5240, 596, 23420), -- 13882
(13896, 0, 0, 4652, 581, 23420), -- 13896
(13544, 2, 0, 4939, 87, 23420), -- 13544
(13544, 1, 0, 4731, 204, 23420), -- 13544
(13544, 0, 0, 4805, 67, 23420), -- 13544
(13948, 1, 0, 4534, 428, 23420), -- 13948
(13948, 0, 0, 4984, 88, 23420), -- 13948
(13910, 1, 0, 4691, 696, 23420), -- 13910
(13910, 0, 0, 4652, 581, 23420), -- 13910
(13918, 2, 11, 4490, 626, 23420), -- 13918
(13918, 2, 10, 4456, 641, 23420), -- 13918
(13918, 2, 9, 4445, 689, 23420), -- 13918
(13918, 2, 8, 4479, 724, 23420), -- 13918
(13918, 2, 7, 4599, 734, 23420), -- 13918
(13918, 2, 6, 4622, 735, 23420), -- 13918
(13918, 2, 5, 4717, 733, 23420), -- 13918
(13918, 2, 4, 4733, 719, 23420), -- 13918
(13918, 2, 3, 4769, 678, 23420), -- 13918
(13918, 2, 2, 4835, 590, 23420), -- 13918
(13918, 2, 1, 4831, 543, 23420), -- 13918
(13918, 2, 0, 4764, 534, 23420), -- 13918
(13918, 1, 11, 4490, 626, 23420), -- 13918
(13918, 1, 10, 4456, 641, 23420), -- 13918
(13918, 1, 9, 4445, 689, 23420), -- 13918
(13918, 1, 8, 4479, 724, 23420), -- 13918
(13918, 1, 7, 4599, 734, 23420), -- 13918
(13918, 1, 6, 4622, 735, 23420), -- 13918
(13918, 1, 5, 4717, 733, 23420), -- 13918
(13918, 1, 4, 4733, 719, 23420), -- 13918
(13918, 1, 3, 4769, 678, 23420), -- 13918
(13918, 1, 2, 4835, 590, 23420), -- 13918
(13918, 1, 1, 4831, 543, 23420), -- 13918
(13918, 1, 0, 4764, 534, 23420), -- 13918
(13918, 0, 0, 4648, 577, 23420), -- 13918
(13909, 1, 0, 4652, 581, 23420), -- 13909
(13909, 0, 5, 4790, 499, 23420), -- 13909
(13909, 0, 4, 4476, 611, 23420), -- 13909
(13909, 0, 3, 4511, 750, 23420), -- 13909
(13909, 0, 2, 4767, 833, 23420), -- 13909
(13909, 0, 1, 5075, 594, 23420), -- 13909
(13909, 0, 0, 5001, 453, 23420), -- 13909
(13907, 1, 6, 4530, 606, 23420), -- 13907
(13907, 1, 5, 4584, 661, 23420), -- 13907
(13907, 1, 4, 4637, 654, 23420), -- 13907
(13907, 1, 3, 4675, 627, 23420), -- 13907
(13907, 1, 2, 4630, 533, 23420), -- 13907
(13907, 1, 1, 4591, 525, 23420), -- 13907
(13907, 1, 0, 4542, 536, 23420), -- 13907
(13907, 0, 0, 4652, 581, 23420), -- 13907
(13911, 1, 0, 4648, 577, 23420), -- 13911
(13911, 0, 3, 4581, 580, 23420), -- 13911
(13911, 0, 2, 4648, 647, 23420), -- 13911
(13911, 0, 1, 4715, 580, 23420), -- 13911
(13911, 0, 0, 4648, 513, 23420), -- 13911
(13912, 1, 0, 4648, 577, 23420), -- 13912
(13912, 0, 0, 4626, 624, 23420), -- 13912
(13526, 1, 0, 4731, 204, 23420), -- 13526
(13526, 0, 10, 4245, 9, 23420), -- 13526
(13526, 0, 9, 4323, 117, 23420), -- 13526
(13526, 0, 8, 4363, 149, 23420), -- 13526
(13526, 0, 7, 4436, 202, 23420), -- 13526
(13526, 0, 6, 4465, 215, 23420), -- 13526
(13526, 0, 5, 4586, 264, 23420), -- 13526
(13526, 0, 4, 4719, 201, 23420), -- 13526
(13526, 0, 3, 4752, 175, 23420), -- 13526
(13526, 0, 2, 4742, 126, 23420), -- 13526
(13526, 0, 1, 4676, 52, 23420), -- 13526
(13526, 0, 0, 4362, -17, 23420), -- 13526
(13525, 0, 0, 4731, 204, 23420), -- 13525
(13892, 1, 0, 4567, 406, 23420), -- 13892
(13881, 2, 0, 5007, 95, 23420), -- 13881
(13881, 1, 3, 4739, 104, 23420), -- 13881
(13881, 1, 2, 4806, 171, 23420), -- 13881
(13881, 1, 1, 4873, 104, 23420), -- 13881
(13881, 1, 0, 4806, 37, 23420), -- 13881
(13881, 0, 6, 4709, 37, 23420), -- 13881
(13881, 0, 5, 4763, 130, 23420), -- 13881
(13881, 0, 4, 4898, 342, 23420), -- 13881
(13881, 0, 3, 4940, 320, 23420), -- 13881
(13881, 0, 2, 4911, 170, 23420), -- 13881
(13881, 0, 1, 4891, 85, 23420), -- 13881
(13881, 0, 0, 4845, 21, 23420), -- 13881
(13902, 0, 0, 4984, 88, 23420); -- 13902

INSERT INTO `quest_poi_points` (`QuestID`, `Idx1`, `Idx2`, `X`, `Y`, `VerifiedBuild`) VALUES
(13588, 2, 0, 5919, 194, 23420), -- 13588
(13588, 1, 5, 5912, 183, 23420), -- 13588
(13588, 1, 4, 5912, 184, 23420), -- 13588
(13588, 1, 3, 5913, 184, 23420), -- 13588
(13588, 1, 2, 5916, 183, 23420), -- 13588
(13588, 1, 1, 5914, 181, 23420), -- 13588
(13588, 1, 0, 5912, 181, 23420), -- 13588
(13588, 0, 0, 5913, 181, 23420), -- 13588
(13940, 0, 0, 5919, 194, 23420), -- 13940
(13587, 1, 0, 5768, -164, 23420), -- 13587
(13587, 0, 0, 5839, -221, 23420), -- 13587
(13586, 0, 0, 5768, -164, 23420), -- 13586
(13581, 0, 0, 5919, 194, 23420), -- 13581
(13583, 0, 0, 5919, 194, 23420), -- 13583
(13582, 1, 0, 6134, 70, 23420), -- 13582
(13582, 0, 0, 6066, -12, 23420), -- 13582
(13578, 1, 0, 6134, 70, 23420), -- 13578
(13578, 0, 9, 6074, 55, 23420), -- 13578
(13578, 0, 8, 6045, 105, 23420), -- 13578
(13578, 0, 7, 6090, 212, 23420), -- 13578
(13578, 0, 6, 6117, 224, 23420), -- 13578
(13578, 0, 5, 6162, 211, 23420), -- 13578
(13578, 0, 4, 6209, 185, 23420), -- 13578
(13578, 0, 3, 6274, 127, 23420), -- 13578
(13578, 0, 2, 6222, -4, 23420), -- 13578
(13578, 0, 1, 6201, -19, 23420), -- 13578
(13578, 0, 0, 6166, -34, 23420), -- 13578
(13580, 1, 0, 5558, 492, 23420), -- 13580
(13580, 0, 0, 5788, 370, 23420), -- 13580
(13576, 1, 10, 5552, 366, 23420), -- 13576
(13576, 1, 9, 5518, 377, 23420), -- 13576
(13576, 1, 8, 5468, 417, 23420), -- 13576
(13576, 1, 7, 5456, 456, 23420), -- 13576
(13576, 1, 6, 5492, 505, 23420), -- 13576
(13576, 1, 5, 5572, 513, 23420), -- 13576
(13576, 1, 4, 5607, 513, 23420), -- 13576
(13576, 1, 3, 5658, 470, 23420), -- 13576
(13576, 1, 2, 5705, 407, 23420), -- 13576
(13576, 1, 1, 5710, 379, 23420), -- 13576
(13576, 1, 0, 5699, 353, 23420), -- 13576
(13576, 0, 0, 5788, 370, 23420), -- 13576
(13585, 0, 0, 5919, 194, 23420), -- 13585
(13584, 1, 0, 5776, 144, 23420), -- 13584
(13584, 0, 6, 5552, 152, 23420), -- 13584
(13584, 0, 5, 5583, 216, 23420), -- 13584
(13584, 0, 4, 5613, 277, 23420), -- 13584
(13584, 0, 3, 5811, 220, 23420), -- 13584
(13584, 0, 2, 5851, 111, 23420), -- 13584
(13584, 0, 1, 5858, 79, 23420), -- 13584
(13584, 0, 0, 5645, 73, 23420), -- 13584
(13579, 0, 0, 5776, 144, 23420), -- 13579
(13577, 0, 0, 6134, 70, 23420), -- 13577
(13575, 0, 0, 5788, 370, 23420), -- 13575
(13558, 1, 0, 6474, 724, 23420), -- 13558
(13558, 0, 0, 6277, 258, 23420), -- 13558
(13605, 2, 0, 6257, 350, 23420), -- 13605
(13605, 1, 0, 6271, 264, 23420), -- 13605
(13605, 0, 3, 6326, 523, 23420), -- 13605
(13605, 0, 2, 6393, 590, 23420), -- 13605
(13605, 0, 1, 6460, 523, 23420), -- 13605
(13605, 0, 0, 6393, 456, 23420), -- 13605
(13547, 5, 0, 6360, 491, 23420), -- 13547
(13547, 4, 0, 6360, 490, 23420), -- 13547
(13547, 3, 0, 6328, 553, 23420), -- 13547
(13547, 2, 0, 6418, 525, 23420), -- 13547
(13547, 1, 0, 6439, 368, 23420), -- 13547
(13547, 0, 0, 6545, 476, 23420), -- 13547
(13542, 2, 0, 6277, 268, 23420), -- 13542
(13542, 1, 11, 6409, 368, 23420), -- 13542
(13542, 1, 10, 6363, 401, 23420), -- 13542
(13542, 1, 9, 6326, 428, 23420), -- 13542
(13542, 1, 8, 6202, 603, 23420), -- 13542
(13542, 1, 7, 6458, 697, 23420), -- 13542
(13542, 1, 6, 6549, 719, 23420), -- 13542
(13542, 1, 5, 6565, 667, 23420), -- 13542
(13542, 1, 4, 6574, 565, 23420), -- 13542
(13542, 1, 3, 6559, 450, 23420), -- 13542
(13542, 1, 2, 6544, 433, 23420), -- 13542
(13542, 1, 1, 6507, 399, 23420), -- 13542
(13542, 1, 0, 6440, 364, 23420), -- 13542
(13542, 0, 0, 6409, 467, 23420), -- 13542
(13573, 0, 0, 5919, 194, 23420), -- 13573
(13543, 3, 0, 6277, 258, 23420), -- 13543
(13543, 2, 0, 6573, 486, 23420), -- 13543
(13543, 1, 0, 6380, 570, 23420), -- 13543
(13543, 0, 0, 6434, 379, 23420), -- 13543
(13601, 0, 0, 6285, 270, 23420), -- 13601
(13523, 1, 6, 6885, 173, 23420), -- 13523
(13523, 1, 5, 6884, 213, 23420), -- 13523
(13523, 1, 4, 6962, 372, 23420), -- 13523
(13523, 1, 3, 7037, 287, 23420), -- 13523
(13523, 1, 2, 7072, 162, 23420), -- 13523
(13523, 1, 1, 7008, 128, 23420), -- 13523
(13523, 1, 0, 6951, 121, 23420), -- 13523
(13523, 0, 0, 6896, 135, 23420), -- 13523
(13596, 1, 0, 7346, -236, 23420), -- 13596
(13596, 0, 9, 6864, 106, 23420), -- 13596
(13596, 0, 8, 6793, 159, 23420), -- 13596
(13596, 0, 7, 6788, 164, 23420), -- 13596
(13596, 0, 6, 6792, 211, 23420), -- 13596
(13596, 0, 5, 6854, 285, 23420), -- 13596
(13596, 0, 4, 6869, 301, 23420), -- 13596
(13596, 0, 3, 6890, 302, 23420), -- 13596
(13596, 0, 2, 6894, 297, 23420), -- 13596
(13596, 0, 1, 6922, 92, 23420), -- 13596
(13596, 0, 0, 6884, 92, 23420), -- 13596
(13519, 2, 0, 7346, -236, 23420), -- 13519
(13519, 1, 11, 6865, 108, 23420), -- 13519
(13519, 1, 10, 6802, 159, 23420), -- 13519
(13519, 1, 9, 6793, 176, 23420), -- 13519
(13519, 1, 8, 6795, 203, 23420), -- 13519
(13519, 1, 7, 6800, 215, 23420), -- 13519
(13519, 1, 6, 6856, 283, 23420), -- 13519
(13519, 1, 5, 6876, 298, 23420), -- 13519
(13519, 1, 4, 6890, 296, 23420), -- 13519
(13519, 1, 3, 6902, 287, 23420), -- 13519
(13519, 1, 2, 6920, 113, 23420), -- 13519
(13519, 1, 1, 6917, 100, 23420), -- 13519
(13519, 1, 0, 6885, 95, 23420), -- 13519
(13519, 0, 0, 6871, 195, 23420); -- 13519


DELETE FROM `quest_details` WHERE `ID` IN (13591 /*13591*/, 13515 /*13515*/, 13514 /*13514*/, 13590 /*13590*/, 13844 /*13844*/, 13513 /*13513*/, 13512 /*13512*/, 13510 /*13510*/, 13511 /*13511*/, 13507 /*13507*/, 13509 /*13509*/, 13508 /*13508*/, 13506 /*13506*/, 13504 /*13504*/, 13505 /*13505*/, 13560 /*13560*/, 13589 /*13589*/, 13599 /*13599*/, 13569 /*13569*/, 13598 /*13598*/, 13566 /*13566*/, 13565 /*13565*/, 13564 /*13564*/, 13563 /*13563*/, 13562 /*13562*/, 13831 /*13831*/, 13557 /*13557*/, 13529 /*13529*/, 13554 /*13554*/, 13528 /*13528*/, 13527 /*13527*/, 13521 /*13521*/, 13537 /*13537*/, 13520 /*13520*/, 13561 /*13561*/, 13522 /*13522*/, 13518 /*13518*/, 13900 /*13900*/, 13891 /*13891*/, 13546 /*13546*/, 13885 /*13885*/, 13572 /*13572*/, 13545 /*13545*/, 13898 /*13898*/, 13899 /*13899*/, 13953 /*13953*/, 13925 /*13925*/, 13895 /*13895*/, 13893 /*13893*/, 13882 /*13882*/, 13896 /*13896*/, 13544 /*13544*/, 13948 /*13948*/, 13910 /*13910*/, 13911 /*13911*/, 13918 /*13918*/, 13909 /*13909*/, 13907 /*13907*/, 13912 /*13912*/, 13526 /*13526*/, 13525 /*13525*/, 13892 /*13892*/, 13881 /*13881*/, 13902 /*13902*/, 13588 /*13588*/, 13940 /*13940*/, 13587 /*13587*/, 13586 /*13586*/, 13581 /*13581*/, 13583 /*13583*/, 13582 /*13582*/, 13578 /*13578*/, 13580 /*13580*/, 13576 /*13576*/, 13585 /*13585*/, 13584 /*13584*/, 13579 /*13579*/, 13577 /*13577*/, 13575 /*13575*/, 13558 /*13558*/, 13605 /*13605*/, 13547 /*13547*/, 13542 /*13542*/, 13573 /*13573*/, 13543 /*13543*/, 13601 /*13601*/, 13523 /*13523*/, 13596 /*13596*/, 13519 /*13519*/);
INSERT INTO `quest_details` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `VerifiedBuild`) VALUES
(13591, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13591
(13515, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13515
(13514, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13514
(13590, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13590
(13844, 3, 396, 273, 0, 100, 800, 500, 0, 23420), -- 13844
(13513, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13513
(13512, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13512
(13510, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13510
(13511, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13511
(13507, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13507
(13509, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13509
(13508, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13508
(13506, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13506
(13504, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13504
(13505, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13505
(13560, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13560
(13589, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13589
(13599, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13599
(13569, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13569
(13598, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13598
(13566, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13566
(13565, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13565
(13564, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13564
(13563, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13563
(13562, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13562
(13831, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13831
(13557, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13557
(13529, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13529
(13554, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13554
(13528, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13528
(13527, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13527
(13521, 5, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13521
(13537, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13537
(13520, 2, 1, 0, 0, 0, 60, 0, 0, 23420), -- 13520
(13561, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13561
(13522, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13522
(13518, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13518
(13900, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13900
(13891, 274, 5, 0, 0, 200, 800, 0, 0, 23420), -- 13891
(13546, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- 13546
(13885, 1, 397, 0, 0, 0, 1200, 0, 0, 23420), -- 13885
(13572, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- 13572
(13545, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- 13545
(13898, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13898
(13899, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13899
(13953, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13953
(13925, 1, 6, 0, 0, 0, 900, 0, 0, 23420), -- 13925
(13895, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13895
(13893, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13893
(13882, 1, 0, 0, 0, 50, 0, 0, 0, 23420), -- 13882
(13896, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13896
(13544, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- 13544
(13948, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13948
(13910, 25, 274, 0, 0, 0, 600, 0, 0, 23420), -- 13910
(13911, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13911
(13918, 1, 25, 273, 0, 0, 1000, 300, 0, 23420), -- 13918
(13909, 6, 1, 0, 0, 0, 600, 0, 0, 23420), -- 13909
(13907, 1, 5, 0, 0, 0, 500, 0, 0, 23420), -- 13907
(13912, 1, 273, 6, 0, 50, 500, 600, 0, 23420), -- 13912
(13526, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- 13526
(13525, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- 13525
(13892, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13892
(13881, 1, 6, 0, 0, 50, 800, 0, 0, 23420), -- 13881
(13902, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13902
(13588, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13588
(13940, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13940
(13587, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13587
(13586, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13586
(13581, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13581
(13583, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13583
(13582, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13582
(13578, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13578
(13580, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13580
(13576, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13576
(13585, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13585
(13584, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13584
(13579, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13579
(13577, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13577
(13575, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13575
(13558, 1, 1, 25, 0, 0, 0, 0, 0, 23420), -- 13558
(13605, 1, 5, 25, 0, 0, 0, 0, 0, 23420), -- 13605
(13547, 1, 1, 0, 0, 0, 0, 0, 0, 23420), -- 13547
(13542, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13542
(13573, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13573
(13543, 1, 25, 0, 0, 0, 0, 0, 0, 23420), -- 13543
(13601, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13601
(13523, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13523
(13596, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13596
(13519, 0, 0, 0, 0, 0, 0, 0, 0, 23420); -- 13519


DELETE FROM `npc_vendor` WHERE (`entry`=43419 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43419 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43419 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43419 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43419 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43419 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43419 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43419 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=4340 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=4342 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=4341 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=2325 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=14341 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=8343 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=6532 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=4400 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=4399 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=4291 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=2605 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=32979 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4187 AND `item`=1202 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4187 AND `item`=17187 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4187 AND `item`=850 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4187 AND `item`=1846 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4187 AND `item`=849 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4187 AND `item`=848 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4187 AND `item`=1845 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4187 AND `item`=847 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43424 AND `item`=5048 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43424 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43424 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43424 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43424 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43424 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43424 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43424 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43424 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43424 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43424 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43439 AND `item`=844 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43439 AND `item`=1844 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43439 AND `item`=843 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43439 AND `item`=845 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43439 AND `item`=1843 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43439 AND `item`=846 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43436 AND `item`=839 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43436 AND `item`=3590 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43436 AND `item`=840 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43436 AND `item`=838 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43436 AND `item`=3589 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43436 AND `item`=837 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43436 AND `item`=16060 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43436 AND `item`=3428 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43436 AND `item`=16059 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=63083 AND `item`=98715 AND `ExtendedCost`=4301 AND `type`=1) OR (`entry`=63083 AND `item`=98715 AND `ExtendedCost`=4300 AND `type`=1) OR (`entry`=63083 AND `item`=98715 AND `ExtendedCost`=4299 AND `type`=1) OR (`entry`=63083 AND `item`=98715 AND `ExtendedCost`=4298 AND `type`=1) OR (`entry`=63083 AND `item`=98715 AND `ExtendedCost`=4297 AND `type`=1) OR (`entry`=63083 AND `item`=98715 AND `ExtendedCost`=4296 AND `type`=1) OR (`entry`=63083 AND `item`=98715 AND `ExtendedCost`=4295 AND `type`=1) OR (`entry`=63083 AND `item`=98715 AND `ExtendedCost`=4294 AND `type`=1) OR (`entry`=63083 AND `item`=98715 AND `ExtendedCost`=4293 AND `type`=1) OR (`entry`=63083 AND `item`=98715 AND `ExtendedCost`=4292 AND `type`=1) OR (`entry`=10085 AND `item`=37460 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43425 AND `item`=5042 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43425 AND `item`=4470 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43425 AND `item`=4498 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43425 AND `item`=4496 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43425 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43425 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43425 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=43425 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=33231 AND `item`=3857 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=33231 AND `item`=18567 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=33231 AND `item`=3466 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=33231 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=33231 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=33231 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=20815 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=39354 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=2324 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=2604 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=6260 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=2605 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=6530 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=6529 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=4289 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=2880 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=2678 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=2321 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=2320 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=3371 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=6217 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=6256 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=39505 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=5956 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=2901 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=85663 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4194 AND `item`=7005 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=8766 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=1645 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=1708 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=1205 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=1179 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=159 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=8950 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=4601 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=4544 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=4542 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=4541 AND `ExtendedCost`=0 AND `type`=1) OR (`entry`=4190 AND `item`=4540 AND `ExtendedCost`=0 AND `type`=1);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `ExtendedCost`, `type`, `PlayerConditionID`, `IgnoreFiltering`, `VerifiedBuild`) VALUES
(43419, 8, 5042, 0, 0, 1, 0, 0, 23420), -- 5042
(43419, 7, 4470, 0, 0, 1, 0, 0, 23420), -- 4470
(43419, 6, 4498, 0, 0, 1, 0, 0, 23420), -- 4498
(43419, 5, 4496, 0, 0, 1, 0, 0, 23420), -- 4496
(43419, 4, 4541, 0, 0, 1, 0, 0, 23420), -- 4541
(43419, 3, 4540, 0, 0, 1, 0, 0, 23420), -- 4540
(43419, 2, 1179, 0, 0, 1, 0, 0, 23420), -- 1179
(43419, 1, 159, 0, 0, 1, 0, 0, 23420), -- 159
(32979, 33, 4340, 0, 0, 1, 0, 0, 23420), -- 4340
(32979, 32, 4342, 0, 0, 1, 0, 0, 23420), -- 4342
(32979, 31, 4341, 0, 0, 1, 0, 0, 23420), -- 4341
(32979, 30, 2325, 0, 0, 1, 0, 0, 23420), -- 2325
(32979, 29, 14341, 0, 0, 1, 0, 0, 23420), -- 14341
(32979, 28, 8343, 0, 0, 1, 0, 0, 23420), -- 8343
(32979, 27, 6532, 0, 0, 1, 0, 0, 23420), -- 6532
(32979, 26, 4400, 0, 0, 1, 0, 0, 23420), -- 4400
(32979, 25, 4399, 0, 0, 1, 0, 0, 23420), -- 4399
(32979, 24, 3857, 0, 0, 1, 0, 0, 23420), -- 3857
(32979, 23, 3466, 0, 0, 1, 0, 0, 23420), -- 3466
(32979, 22, 4291, 0, 0, 1, 0, 0, 23420), -- 4291
(32979, 21, 20815, 0, 0, 1, 0, 0, 23420), -- 20815
(32979, 20, 39354, 0, 0, 1, 0, 0, 23420), -- 39354
(32979, 19, 2324, 0, 0, 1, 0, 0, 23420), -- 2324
(32979, 18, 2604, 0, 0, 1, 0, 0, 23420), -- 2604
(32979, 17, 6260, 0, 0, 1, 0, 0, 23420), -- 6260
(32979, 16, 2605, 0, 0, 1, 0, 0, 23420), -- 2605
(32979, 15, 6530, 0, 0, 1, 0, 0, 23420), -- 6530
(32979, 14, 6529, 0, 0, 1, 0, 0, 23420), -- 6529
(32979, 13, 4289, 0, 0, 1, 0, 0, 23420), -- 4289
(32979, 12, 2880, 0, 0, 1, 0, 0, 23420), -- 2880
(32979, 11, 2678, 0, 0, 1, 0, 0, 23420), -- 2678
(32979, 10, 2321, 0, 0, 1, 0, 0, 23420), -- 2321
(32979, 9, 2320, 0, 0, 1, 0, 0, 23420), -- 2320
(32979, 8, 3371, 0, 0, 1, 0, 0, 23420), -- 3371
(32979, 7, 6217, 0, 0, 1, 0, 0, 23420), -- 6217
(32979, 6, 6256, 0, 0, 1, 0, 0, 23420), -- 6256
(32979, 5, 39505, 0, 0, 1, 0, 0, 23420), -- 39505
(32979, 4, 5956, 0, 0, 1, 0, 0, 23420), -- 5956
(32979, 3, 2901, 0, 0, 1, 0, 0, 23420), -- 2901
(32979, 2, 85663, 0, 0, 1, 0, 0, 23420), -- 85663
(32979, 1, 7005, 0, 0, 1, 0, 0, 23420), -- 7005
(4187, 8, 1202, 0, 0, 1, 0, 1, 23420), -- 1202
(4187, 7, 17187, 0, 0, 1, 0, 1, 23420), -- 17187
(4187, 6, 850, 0, 0, 1, 0, 0, 23420), -- 850
(4187, 5, 1846, 0, 0, 1, 0, 0, 23420), -- 1846
(4187, 4, 849, 0, 0, 1, 0, 0, 23420), -- 849
(4187, 3, 848, 0, 0, 1, 0, 0, 23420), -- 848
(4187, 2, 1845, 0, 0, 1, 0, 0, 23420), -- 1845
(4187, 1, 847, 0, 0, 1, 0, 0, 23420), -- 847
(43424, 11, 5048, 0, 0, 1, 0, 0, 23420), -- 5048
(43424, 10, 4542, 0, 0, 1, 0, 0, 23420), -- 4542
(43424, 9, 1205, 0, 0, 1, 0, 0, 23420), -- 1205
(43424, 8, 5042, 0, 0, 1, 0, 0, 23420), -- 5042
(43424, 7, 4470, 0, 0, 1, 0, 0, 23420), -- 4470
(43424, 6, 4498, 0, 0, 1, 0, 0, 23420), -- 4498
(43424, 5, 4496, 0, 0, 1, 0, 0, 23420), -- 4496
(43424, 4, 4541, 0, 0, 1, 0, 0, 23420), -- 4541
(43424, 3, 4540, 0, 0, 1, 0, 0, 23420), -- 4540
(43424, 2, 1179, 0, 0, 1, 0, 0, 23420), -- 1179
(43424, 1, 159, 0, 0, 1, 0, 0, 23420), -- 159
(43439, 6, 844, 0, 0, 1, 0, 0, 23420), -- 844
(43439, 5, 1844, 0, 0, 1, 0, 0, 23420), -- 1844
(43439, 4, 843, 0, 0, 1, 0, 0, 23420), -- 843
(43439, 3, 845, 0, 0, 1, 0, 0, 23420), -- 845
(43439, 2, 1843, 0, 0, 1, 0, 0, 23420), -- 1843
(43439, 1, 846, 0, 0, 1, 0, 0, 23420), -- 846
(43436, 9, 839, 0, 0, 1, 0, 0, 23420), -- 839
(43436, 8, 3590, 0, 0, 1, 0, 0, 23420), -- 3590
(43436, 7, 840, 0, 0, 1, 0, 0, 23420), -- 840
(43436, 6, 838, 0, 0, 1, 0, 0, 23420), -- 838
(43436, 5, 3589, 0, 0, 1, 0, 0, 23420), -- 3589
(43436, 4, 837, 0, 0, 1, 0, 0, 23420), -- 837
(43436, 3, 16060, 0, 0, 1, 0, 0, 23420), -- 16060
(43436, 2, 3428, 0, 0, 1, 0, 0, 23420), -- 3428
(43436, 1, 16059, 0, 0, 1, 0, 0, 23420), -- 16059
(63083, 10, 98715, 0, 4301, 1, 0, 0, 23420), -- 98715
(63083, 9, 98715, 0, 4300, 1, 0, 0, 23420), -- 98715
(63083, 8, 98715, 0, 4299, 1, 0, 0, 23420), -- 98715
(63083, 7, 98715, 0, 4298, 1, 0, 0, 23420), -- 98715
(63083, 6, 98715, 0, 4297, 1, 0, 0, 23420), -- 98715
(63083, 5, 98715, 0, 4296, 1, 0, 0, 23420), -- 98715
(63083, 4, 98715, 0, 4295, 1, 0, 0, 23420), -- 98715
(63083, 3, 98715, 0, 4294, 1, 0, 0, 23420), -- 98715
(63083, 2, 98715, 0, 4293, 1, 0, 0, 23420), -- 98715
(63083, 1, 98715, 0, 4292, 1, 0, 0, 23420), -- 98715
(10085, 1, 37460, 0, 0, 1, 0, 0, 23420), -- 37460
(43425, 8, 5042, 0, 0, 1, 0, 0, 23420), -- 5042
(43425, 7, 4470, 0, 0, 1, 0, 0, 23420), -- 4470
(43425, 6, 4498, 0, 0, 1, 0, 0, 23420), -- 4498
(43425, 5, 4496, 0, 0, 1, 0, 0, 23420), -- 4496
(43425, 4, 4541, 0, 0, 1, 0, 0, 23420), -- 4541
(43425, 3, 4540, 0, 0, 1, 0, 0, 23420), -- 4540
(43425, 2, 1179, 0, 0, 1, 0, 0, 23420), -- 1179
(43425, 1, 159, 0, 0, 1, 0, 0, 23420), -- 159
(33231, 6, 3857, 0, 0, 1, 0, 0, 23420), -- 3857
(33231, 5, 18567, 0, 0, 1, 0, 0, 23420), -- 18567
(33231, 4, 3466, 0, 0, 1, 0, 0, 23420), -- 3466
(33231, 3, 2880, 0, 0, 1, 0, 0, 23420), -- 2880
(33231, 2, 5956, 0, 0, 1, 0, 0, 23420), -- 5956
(33231, 1, 2901, 0, 0, 1, 0, 0, 23420), -- 2901
(4194, 21, 20815, 0, 0, 1, 0, 0, 23420), -- 20815
(4194, 20, 39354, 0, 0, 1, 0, 0, 23420), -- 39354
(4194, 19, 2324, 0, 0, 1, 0, 0, 23420), -- 2324
(4194, 18, 2604, 0, 0, 1, 0, 0, 23420), -- 2604
(4194, 17, 6260, 0, 0, 1, 0, 0, 23420), -- 6260
(4194, 16, 2605, 0, 0, 1, 0, 0, 23420), -- 2605
(4194, 15, 6530, 0, 0, 1, 0, 0, 23420), -- 6530
(4194, 14, 6529, 0, 0, 1, 0, 0, 23420), -- 6529
(4194, 13, 4289, 0, 0, 1, 0, 0, 23420), -- 4289
(4194, 12, 2880, 0, 0, 1, 0, 0, 23420), -- 2880
(4194, 11, 2678, 0, 0, 1, 0, 0, 23420), -- 2678
(4194, 10, 2321, 0, 0, 1, 0, 0, 23420), -- 2321
(4194, 9, 2320, 0, 0, 1, 0, 0, 23420), -- 2320
(4194, 8, 3371, 0, 0, 1, 0, 0, 23420), -- 3371
(4194, 7, 6217, 0, 0, 1, 0, 0, 23420), -- 6217
(4194, 6, 6256, 0, 0, 1, 0, 0, 23420), -- 6256
(4194, 5, 39505, 0, 0, 1, 0, 0, 23420), -- 39505
(4194, 4, 5956, 0, 0, 1, 0, 0, 23420), -- 5956
(4194, 3, 2901, 0, 0, 1, 0, 0, 23420), -- 2901
(4194, 2, 85663, 0, 0, 1, 0, 0, 23420), -- 85663
(4194, 1, 7005, 0, 0, 1, 0, 0, 23420), -- 7005
(4190, 12, 8766, 0, 0, 1, 0, 0, 23420), -- 8766
(4190, 11, 1645, 0, 0, 1, 0, 0, 23420), -- 1645
(4190, 10, 1708, 0, 0, 1, 0, 0, 23420), -- 1708
(4190, 9, 1205, 0, 0, 1, 0, 0, 23420), -- 1205
(4190, 8, 1179, 0, 0, 1, 0, 0, 23420), -- 1179
(4190, 7, 159, 0, 0, 1, 0, 0, 23420), -- 159
(4190, 6, 8950, 0, 0, 1, 0, 0, 23420), -- 8950
(4190, 5, 4601, 0, 0, 1, 0, 0, 23420), -- 4601
(4190, 4, 4544, 0, 0, 1, 0, 0, 23420), -- 4544
(4190, 3, 4542, 0, 0, 1, 0, 0, 23420), -- 4542
(4190, 2, 4541, 0, 0, 1, 0, 0, 23420), -- 4541
(4190, 1, 4540, 0, 0, 1, 0, 0, 23420); -- 4540


DELETE FROM `gossip_menu` WHERE (`entry`=10491 AND `text_id`=14454) OR (`entry`=10431 AND `text_id`=14469) OR (`entry`=10428 AND `text_id`=14454) OR (`entry`=10430 AND `text_id`=14468) OR (`entry`=10480 AND `text_id`=14506) OR (`entry`=10416 AND `text_id`=14454) OR (`entry`=12726 AND `text_id`=17861) OR (`entry`=10484 AND `text_id`=14510) OR (`entry`=10479 AND `text_id`=14505) OR (`entry`=10410 AND `text_id`=14447) OR (`entry`=10427 AND `text_id`=14454) OR (`entry`=10429 AND `text_id`=14454) OR (`entry`=10482 AND `text_id`=14508) OR (`entry`=8521 AND `text_id`=10654) OR (`entry`=14239 AND `text_id`=5722) OR (`entry`=347 AND `text_id`=824) OR (`entry`=14255 AND `text_id`=5725) OR (`entry`=10295 AND `text_id`=14299) OR (`entry`=10292 AND `text_id`=14296) OR (`entry`=10289 AND `text_id`=14293) OR (`entry`=10288 AND `text_id`=14292) OR (`entry`=10291 AND `text_id`=14295) OR (`entry`=10293 AND `text_id`=14297) OR (`entry`=10287 AND `text_id`=14291) OR (`entry`=10286 AND `text_id`=14290) OR (`entry`=10296 AND `text_id`=14300) OR (`entry`=10294 AND `text_id`=14298) OR (`entry`=7690 AND `text_id`=9384) OR (`entry`=4138 AND `text_id`=5121) OR (`entry`=12725 AND `text_id`=17859) OR (`entry`=8519 AND `text_id`=10652) OR (`entry`=10301 AND `text_id`=14304) OR (`entry`=14991 AND `text_id`=20326) OR (`entry`=14258 AND `text_id`=17616) OR (`entry`=14257 AND `text_id`=4437) OR (`entry`=20690 AND `text_id`=31032) OR (`entry`=14238 AND `text_id`=538) OR (`entry`=15145 AND `text_id`=21709) OR (`entry`=9821 AND `text_id`=13584) OR (`entry`=14164 AND `text_id`=4993) OR (`entry`=14256 AND `text_id`=4794) OR (`entry`=4301 AND `text_id`=5473) OR (`entry`=10486 AND `text_id`=14524) OR (`entry`=10496 AND `text_id`=14526) OR (`entry`=10509 AND `text_id`=14541) OR (`entry`=10490 AND `text_id`=14517) OR (`entry`=83 AND `text_id`=580) OR (`entry`=10486 AND `text_id`=14511) OR (`entry`=10492 AND `text_id`=14518) OR (`entry`=10515 AND `text_id`=14547) OR (`entry`=11853 AND `text_id`=16615) OR (`entry`=10516 AND `text_id`=14548) OR (`entry`=10483 AND `text_id`=14509) OR (`entry`=10272 AND `text_id`=14264) OR (`entry`=10269 AND `text_id`=14260) OR (`entry`=10268 AND `text_id`=14259) OR (`entry`=10278 AND `text_id`=14275) OR (`entry`=10279 AND `text_id`=14276) OR (`entry`=10277 AND `text_id`=14274);
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES
(10491, 14454), -- 33359
(10431, 14469), -- 33178
(10428, 14454), -- 33359
(10430, 14468), -- 34103
(10480, 14506), -- 32963
(10416, 14454), -- 33359
(12726, 17861), -- 32979
(10484, 14510), -- 34205
(10479, 14505), -- 33048
(10410, 14447), -- 194771
(10427, 14454), -- 33359
(10429, 14454), -- 33359
(10482, 14508), -- 32978
(8521, 10654), -- 43429
(14239, 5722), -- 49963
(347, 824), -- 43420
(14255, 5725), -- 49923
(10295, 14299), -- 33095
(10292, 14296), -- 33103
(10289, 14293), -- 33099
(10288, 14292), -- 33098
(10291, 14295), -- 33102
(10293, 14297), -- 33093
(10287, 14291), -- 33097
(10286, 14290), -- 33096
(10296, 14300), -- 32911
(10294, 14298), -- 33094
(7690, 9384), -- 43431
(4138, 5121), -- 11037
(12725, 17859), -- 51997
(8519, 10652), -- 43428
(10301, 14304), -- 32912
(14991, 20326), -- 63083
(14258, 17616), -- 49942
(14257, 4437), -- 49940
(20690, 31032), -- 49968
(14238, 538), -- 49968
(15145, 21709), -- 10085
(9821, 13584), -- 10085
(14164, 4993), -- 49927
(14256, 4794), -- 49939
(4301, 5473), -- 3841
(10486, 14524), -- 34301
(10496, 14526), -- 34301
(10509, 14541), -- 34392
(10490, 14517), -- 33072
(83, 580), -- 6491
(10486, 14511), -- 34301
(10492, 14518), -- 33072
(10515, 14547), -- 34403
(11853, 16615), -- 34342
(10516, 14548), -- 34402
(10483, 14509), -- 33091
(10272, 14264), -- 33001
(10269, 14260), -- 32987
(10268, 14259), -- 3694
(10278, 14275), -- 33035
(10279, 14276), -- 33037
(10277, 14274); -- 33033


DELETE FROM `quest_template` WHERE `ID` IN (13591 /*13591*/, 13515 /*13515*/, 13514 /*13514*/, 13590 /*13590*/, 13844 /*13844*/, 13513 /*13513*/, 13512 /*13512*/, 13510 /*13510*/, 13511 /*13511*/, 13507 /*13507*/, 13509 /*13509*/, 13508 /*13508*/, 13506 /*13506*/, 13504 /*13504*/, 13505 /*13505*/, 13560 /*13560*/, 13589 /*13589*/, 13599 /*13599*/, 13568 /*13568*/, 13569 /*13569*/, 13598 /*13598*/, 13566 /*13566*/, 13565 /*13565*/, 13564 /*13564*/, 13563 /*13563*/, 13562 /*13562*/, 13831 /*13831*/, 13557 /*13557*/, 13529 /*13529*/, 13554 /*13554*/, 13528 /*13528*/, 13527 /*13527*/, 13521 /*13521*/, 13537 /*13537*/, 13520 /*13520*/, 13561 /*13561*/, 13522 /*13522*/, 43290 /*43290*/, 43289 /*43289*/, 43282 /*43282*/, 13900 /*13900*/, 13891 /*13891*/, 13546 /*13546*/, 13885 /*13885*/, 13572 /*13572*/, 13545 /*13545*/, 13898 /*13898*/, 13899 /*13899*/, 13953 /*13953*/, 13925 /*13925*/, 13895 /*13895*/, 13893 /*13893*/, 13882 /*13882*/, 13896 /*13896*/, 13544 /*13544*/, 13948 /*13948*/, 13910 /*13910*/, 13918 /*13918*/, 13909 /*13909*/, 13907 /*13907*/, 13911 /*13911*/, 13912 /*13912*/, 13526 /*13526*/, 13525 /*13525*/, 13881 /*13881*/, 13892 /*13892*/, 13902 /*13902*/, 13588 /*13588*/, 13940 /*13940*/, 13587 /*13587*/, 13586 /*13586*/, 13581 /*13581*/, 13583 /*13583*/, 13582 /*13582*/, 13578 /*13578*/, 13580 /*13580*/, 13576 /*13576*/, 13585 /*13585*/, 13584 /*13584*/, 13579 /*13579*/, 13577 /*13577*/, 13575 /*13575*/, 13558 /*13558*/, 13605 /*13605*/, 13547 /*13547*/, 13542 /*13542*/, 13573 /*13573*/, 13543 /*13543*/, 13601 /*13601*/, 13523 /*13523*/, 13596 /*13596*/, 13519 /*13519*/, 13570 /*13570*/);
INSERT INTO `quest_template` (`ID`, `QuestType`, `QuestLevel`, `QuestPackageID`, `MinLevel`, `QuestSortID`, `QuestInfoID`, `SuggestedGroupNum`, `RewardNextQuest`, `RewardXPDifficulty`, `RewardXPMultiplier`, `RewardMoney`, `RewardMoneyDifficulty`, `RewardMoneyMultiplier`, `RewardBonusMoney`, `RewardDisplaySpell1`, `RewardDisplaySpell2`, `RewardDisplaySpell3`, `RewardSpell`, `RewardHonor`, `RewardKillHonor`, `StartItem`, `RewardArtifactXPDifficulty`, `RewardArtifactXPMultiplier`, `RewardArtifactCategoryID`, `Flags`, `FlagsEx`, `RewardSkillLineID`, `RewardNumSkillUps`, `PortraitGiver`, `PortraitTurnIn`, `RewardItem1`, `RewardItem2`, `RewardItem3`, `RewardItem4`, `RewardAmount1`, `RewardAmount2`, `RewardAmount3`, `RewardAmount4`, `ItemDrop1`, `ItemDrop2`, `ItemDrop3`, `ItemDrop4`, `ItemDropQuantity1`, `ItemDropQuantity2`, `ItemDropQuantity3`, `ItemDropQuantity4`, `RewardChoiceItemID1`, `RewardChoiceItemID2`, `RewardChoiceItemID3`, `RewardChoiceItemID4`, `RewardChoiceItemID5`, `RewardChoiceItemID6`, `RewardChoiceItemQuantity1`, `RewardChoiceItemQuantity2`, `RewardChoiceItemQuantity3`, `RewardChoiceItemQuantity4`, `RewardChoiceItemQuantity5`, `RewardChoiceItemQuantity6`, `RewardChoiceItemDisplayID1`, `RewardChoiceItemDisplayID2`, `RewardChoiceItemDisplayID3`, `RewardChoiceItemDisplayID4`, `RewardChoiceItemDisplayID5`, `RewardChoiceItemDisplayID6`, `POIContinent`, `POIx`, `POIy`, `POIPriority`, `RewardTitle`, `RewardArenaPoints`, `RewardFactionID1`, `RewardFactionID2`, `RewardFactionID3`, `RewardFactionID4`, `RewardFactionID5`, `RewardFactionValue1`, `RewardFactionValue2`, `RewardFactionValue3`, `RewardFactionValue4`, `RewardFactionValue5`, `RewardFactionCapIn1`, `RewardFactionCapIn2`, `RewardFactionCapIn3`, `RewardFactionCapIn4`, `RewardFactionCapIn5`, `RewardFactionOverride1`, `RewardFactionOverride2`, `RewardFactionOverride3`, `RewardFactionOverride4`, `RewardFactionOverride5`, `RewardFactionFlags`, `AreaGroupID`, `TimeAllowed`, `AllowableRaces`, `QuestRewardID`, `RewardCurrencyID1`, `RewardCurrencyID2`, `RewardCurrencyID3`, `RewardCurrencyID4`, `RewardCurrencyQty1`, `RewardCurrencyQty2`, `RewardCurrencyQty3`, `RewardCurrencyQty4`, `AcceptedSoundKitID`, `CompleteSoundKitID`, `VerifiedBuild`) VALUES
(13591, 2, 15, 1562, 10, 148, 0, 0, 0, 6, 1, 1400, 6, 1, 23700, 0, 0, 0, 0, 0, 0, 46318, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13591
(13515, 2, 14, 0, 10, 148, 0, 0, 0, 6, 1, 1300, 6, 1, 16800, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 134217736, 0, 0, 0, 28341, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13515
(13514, 2, 14, 0, 10, 148, 0, 0, 0, 6, 1, 1300, 6, 1, 16800, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13514
(13590, 2, 14, 0, 10, 148, 0, 0, 13515, 5, 1, 600, 5, 1, 13500, 0, 0, 0, 0, 0, 0, 44985, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13590
(13844, 2, 14, 1629, 10, 148, 0, 0, 0, 5, 1, 600, 5, 1, 13500, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 134217736, 0, 0, 0, 29061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13844
(13513, 2, 14, 0, 10, 148, 0, 0, 0, 5, 1, 600, 5, 1, 13500, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13513
(13512, 2, 14, 0, 10, 148, 0, 0, 0, 5, 1, 600, 5, 1, 13500, 0, 0, 0, 0, 0, 0, 44995, 0, 1, 0, 8, 0, 0, 0, 0, 0, 52650, 0, 0, 0, 1, 0, 0, 0, 0, 0, 44995, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13512
(13510, 2, 13, 1506, 10, 148, 0, 0, 0, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13510
(13511, 2, 13, 0, 10, 148, 0, 0, 0, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13511
(13507, 2, 13, 1503, 10, 148, 0, 0, 0, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13507
(13509, 2, 13, 0, 10, 148, 0, 0, 0, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 44999, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 44999, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13509
(13508, 2, 13, 0, 10, 148, 0, 0, 13511, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13508
(13506, 2, 13, 0, 10, 148, 0, 0, 13508, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 44979, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13506
(13504, 2, 13, 0, 10, 148, 0, 0, 0, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13504
(13505, 2, 13, 0, 10, 148, 0, 0, 0, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13505
(13560, 2, 13, 1533, 8, 148, 0, 0, 0, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13560
(13589, 2, 13, 0, 10, 148, 0, 0, 0, 4, 1, 400, 4, 1, 6600, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13589
(13599, 2, 12, 0, 10, 148, 0, 0, 0, 4, 1, 350, 4, 1, 3900, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 4, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13599
(13568, 0, 12, 0, 10, 148, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 62803, 0, 0, 0, 0, 1, 0, 8, 512, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13568
(13569, 2, 12, 1542, 10, 148, 0, 0, 0, 5, 1, 500, 5, 1, 5100, 0, 0, 0, 64359, 0, 0, 0, 0, 1, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13569
(13598, 2, 12, 0, 10, 148, 0, 0, 0, 5, 1, 500, 5, 1, 5100, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13598
(13566, 2, 12, 0, 10, 148, 0, 0, 13569, 5, 1, 500, 5, 1, 5100, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13566
(13565, 2, 12, 1538, 10, 148, 0, 0, 0, 5, 1, 500, 5, 1, 5100, 0, 0, 0, 0, 0, 0, 45911, 0, 1, 0, 134217736, 0, 0, 0, 4982, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45911, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13565
(13564, 2, 11, 0, 8, 148, 0, 0, 0, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13564
(13563, 2, 11, 1536, 8, 148, 0, 0, 0, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 134217736, 0, 0, 0, 1937, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13563
(13562, 2, 11, 0, 8, 148, 0, 0, 0, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13562
(13831, 2, 11, 0, 8, 148, 0, 0, 0, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 45898, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13831
(13557, 2, 11, 0, 8, 148, 0, 0, 0, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 44925, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13557
(13529, 2, 11, 1522, 8, 148, 0, 0, 0, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 134217736, 0, 0, 0, 2017, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13529
(13554, 2, 11, 1530, 8, 148, 0, 0, 0, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13554
(13528, 2, 11, 0, 8, 148, 0, 0, 13554, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13528
(13527, 2, 11, 0, 8, 148, 0, 0, 13528, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13527
(13521, 2, 10, 0, 8, 148, 0, 0, 13527, 5, 1, 350, 5, 1, 960, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13521
(13537, 2, 11, 0, 8, 148, 0, 0, 0, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 46337, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1134, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13537
(13520, 2, 10, 9617, 8, 148, 0, 0, 0, 5, 1, 350, 5, 1, 960, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13520
(13561, 2, 11, 0, 8, 148, 0, 0, 0, 5, 1, 400, 5, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13561
(13522, 2, 10, 0, 8, 148, 0, 0, 0, 5, 1, 350, 5, 1, 960, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13522
(43290, 2, -1, 0, 1, 17, 0, 0, 0, 8, 0.75, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 60, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43290
(43289, 2, -1, 0, 1, 17, 0, 0, 0, 8, 0.5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43289
(43282, 2, -1, 0, 1, 17, 0, 0, 0, 8, 1.5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 33555200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 43282
(13900, 2, 20, 1665, 17, 148, 0, 0, 0, 7, 1, 3500, 7, 1, 35700, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 7, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13900
(13891, 2, 18, 1657, 16, 148, 0, 0, 0, 6, 1, 2000, 6, 1, 27600, 0, 0, 0, 0, 0, 0, 46370, 0, 1, 0, 0, 0, 0, 0, 28628, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13891
(13546, 2, 19, 1528, 17, 148, 0, 0, 0, 5, 1, 1100, 5, 1, 22800, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13546
(13885, 2, 19, 0, 16, 148, 0, 0, 13891, 5, 1, 1100, 5, 1, 22800, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13885
(13572, 2, 19, 1544, 17, 148, 0, 0, 0, 5, 1, 1100, 5, 1, 22800, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13572
(13545, 2, 19, 0, 17, 148, 0, 0, 13546, 5, 1, 1100, 5, 1, 22800, 0, 0, 0, 0, 0, 0, 44889, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 44889, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13545
(13898, 2, 20, 1663, 17, 148, 0, 0, 0, 5, 1, 1200, 5, 1, 23700, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13898
(13899, 2, 20, 0, 17, 148, 0, 0, 13900, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13899
(13953, 2, 19, 0, 17, 148, 0, 0, 13899, 3, 1, 500, 3, 1, 11400, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13953
(13925, 2, 18, 0, 16, 148, 0, 0, 13885, 4, 1, 700, 4, 1, 16500, 0, 0, 0, 0, 0, 0, 46363, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13925
(13895, 2, 19, 0, 17, 148, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13895
(13893, 2, 19, 0, 17, 148, 0, 0, 0, 2, 1, 0, 0, 1, 5700, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13893
(13882, 2, 18, 1650, 16, 148, 0, 0, 13925, 5, 1, 1000, 5, 1, 22200, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13882
(13896, 2, 19, 0, 17, 148, 0, 0, 13893, 3, 1, 500, 3, 1, 11400, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13896
(13544, 2, 19, 0, 17, 148, 0, 0, 13545, 5, 1, 1100, 5, 1, 22800, 0, 0, 0, 0, 0, 0, 44888, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 44886, 44888, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13544
(13948, 2, 19, 0, 17, 148, 0, 0, 0, 4, 1, 0, 0, 1, 17100, 0, 0, 0, 0, 0, 0, 46696, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46696, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13948
(13910, 2, 18, 1670, 16, 148, 0, 0, 0, 5, 1, 1000, 5, 1, 22200, 0, 0, 0, 0, 0, 0, 46385, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13910
(13918, 2, 18, 1674, 16, 148, 0, 0, 0, 5, 1, 1000, 5, 1, 22200, 0, 0, 0, 65310, 0, 0, 46388, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46702, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13918
(13909, 2, 18, 0, 16, 148, 0, 0, 13910, 3, 1, 1000, 5, 1, 11100, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13909
(13907, 2, 18, 0, 16, 148, 0, 0, 13909, 5, 1, 1000, 5, 1, 22200, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13907
(13911, 2, 18, 1671, 16, 148, 0, 0, 0, 5, 1, 1000, 5, 1, 22200, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13911
(13912, 2, 18, 0, 16, 148, 0, 0, 13918, 5, 1, 1000, 5, 1, 22200, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13912
(13526, 2, 19, 0, 17, 148, 0, 0, 13544, 5, 1, 1100, 5, 1, 22800, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13526
(13525, 2, 19, 0, 17, 148, 0, 0, 0, 1, 1, 0, 0, 1, 2280, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13525
(13881, 2, 18, 0, 16, 148, 0, 0, 13882, 5, 1, 1000, 5, 1, 22200, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13881
(13892, 2, 19, 0, 17, 148, 0, 0, 13948, 5, 1, 1100, 5, 1, 22800, 0, 0, 0, 0, 0, 0, 46696, 0, 1, 0, 65544, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46696, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13892
(13902, 2, 18, 0, 17, 148, 0, 0, 0, 1, 1, 100, 1, 1, 2220, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13902
(13588, 2, 17, 1559, 13, 148, 0, 0, 0, 5, 1, 900, 5, 1, 21300, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13588
(13940, 2, 17, 0, 13, 148, 0, 0, 13588, 3, 1, 0, 0, 1, 10800, 0, 0, 0, 65425, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13940
(13587, 2, 17, 9618, 13, 148, 0, 0, 13940, 3, 1, 0, 0, 1, 10800, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13587
(13586, 2, 17, 0, 13, 148, 0, 0, 0, 5, 1, 0, 0, 1, 21300, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 136, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13586
(13581, 2, 15, 0, 13, 148, 0, 0, 0, 2, 1, 0, 0, 1, 4800, 0, 0, 0, 65397, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13581
(13583, 2, 16, 0, 13, 148, 0, 0, 0, 3, 1, 0, 0, 1, 10200, 0, 0, 0, 61899, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 3, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13583
(13582, 2, 15, 0, 13, 148, 0, 0, 0, 5, 1, 700, 5, 1, 18900, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13582
(13578, 2, 17, 1549, 13, 148, 0, 0, 13582, 5, 1, 900, 5, 1, 21300, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13578
(13580, 2, 17, 1551, 13, 148, 0, 0, 13581, 5, 1, 900, 5, 1, 21300, 0, 0, 0, 0, 0, 0, 46546, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 46546, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13580
(13576, 2, 15, 1547, 13, 148, 0, 0, 13580, 5, 1, 700, 5, 1, 18900, 0, 0, 0, 0, 0, 0, 44959, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 44959, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13576
(13585, 2, 17, 0, 13, 148, 0, 0, 0, 1, 1, 0, 0, 1, 2160, 0, 0, 0, 65399, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13585
(13584, 2, 16, 1555, 13, 148, 0, 0, 13585, 5, 1, 800, 5, 1, 20400, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13584
(13579, 2, 15, 0, 13, 148, 0, 0, 13584, 1, 1, 70, 1, 1, 1920, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13579
(13577, 2, 16, 0, 13, 148, 0, 0, 13578, 3, 1, 0, 0, 1, 10200, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13577
(13575, 2, 15, 0, 13, 148, 0, 0, 13576, 2, 1, 0, 0, 1, 4800, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13575
(13558, 2, 16, 1532, 13, 148, 0, 0, 0, 6, 1, 1600, 6, 1, 25500, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 134217736, 0, 0, 0, 31938, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13558
(13605, 2, 15, 1571, 13, 148, 0, 0, 0, 6, 1, 1400, 6, 1, 23700, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 69, 0, 0, 0, 6, 6, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13605
(13547, 2, 15, 0, 13, 148, 0, 0, 13558, 5, 1, 0, 0, 1, 18900, 0, 0, 0, 62095, 0, 0, 0, 0, 1, 0, 2097160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13547
(13542, 2, 15, 1524, 13, 148, 0, 0, 0, 5, 1, 700, 5, 1, 18900, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 44868, 0, 0, 0, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13542
(13573, 2, 15, 0, 13, 148, 0, 0, 0, 1, 1, 0, 0, 1, 1920, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13573
(13543, 2, 15, 0, 13, 148, 0, 0, 0, 5, 1, 0, 0, 1, 18900, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 18875469, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13543
(13601, 2, 15, 0, 13, 148, 0, 0, 0, 2, 1, 0, 0, 1, 4800, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 2, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13601
(13523, 2, 13, 1517, 13, 148, 0, 0, 0, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 44975, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 44975, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13523
(13596, 2, 13, 0, 13, 148, 0, 0, 0, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13596
(13519, 2, 13, 1513, 13, 148, 0, 0, 0, 5, 1, 600, 5, 1, 9000, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 134217736, 0, 0, 0, 28367, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 5, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420), -- 13519
(13570, 0, 15, 1543, 10, 148, 0, 0, 0, 5, 1, 700, 5, 1, 18900, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 8, 0, 0, 0, 0, 0, 46325, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 890, 878, 23420); -- 13570


DELETE FROM `quest_objectives` WHERE `ID` IN (266049 /*266049*/, 253530 /*253530*/, 266598 /*266598*/, 266597 /*266597*/, 267435 /*267435*/, 264536 /*264536*/, 264535 /*264535*/, 251687 /*251687*/, 262505 /*262505*/, 262504 /*262504*/, 267491 /*267491*/, 253228 /*253228*/, 253227 /*253227*/, 267385 /*267385*/, 253325 /*253325*/, 265676 /*265676*/, 267309 /*267309*/, 256718 /*256718*/, 252950 /*252950*/, 266473 /*266473*/, 266472 /*266472*/, 266471 /*266471*/, 266154 /*266154*/, 266153 /*266153*/, 267649 /*267649*/, 265958 /*265958*/, 265957 /*265957*/, 256013 /*256013*/, 256239 /*256239*/, 256238 /*256238*/, 266621 /*266621*/, 266620 /*266620*/, 257477 /*257477*/, 266022 /*266022*/, 256073 /*256073*/, 266042 /*266042*/, 255170 /*255170*/, 253095 /*253095*/, 266016 /*266016*/, 266015 /*266015*/, 254184 /*254184*/, 264115 /*264115*/, 266615 /*266615*/, 266614 /*266614*/, 267419 /*267419*/, 267644 /*267644*/, 267643 /*267643*/, 267642 /*267642*/, 267505 /*267505*/, 264908 /*264908*/, 264976 /*264976*/, 266617 /*266617*/, 266616 /*266616*/, 267514 /*267514*/, 267513 /*267513*/, 267512 /*267512*/, 267315 /*267315*/, 264781 /*264781*/, 263713 /*263713*/, 263712 /*263712*/, 267081 /*267081*/, 267080 /*267080*/, 265849 /*265849*/, 267359 /*267359*/, 267171 /*267171*/, 252342 /*252342*/, 266217 /*266217*/, 266216 /*266216*/, 266852 /*266852*/, 267269 /*267269*/, 267268 /*267268*/, 253558 /*253558*/, 266121 /*266121*/, 257798 /*257798*/, 263989 /*263989*/, 263993 /*263993*/, 263710 /*263710*/, 267388 /*267388*/, 266057 /*266057*/, 266056 /*266056*/, 266055 /*266055*/, 266054 /*266054*/, 252588 /*252588*/, 254169 /*254169*/, 254168 /*254168*/, 254167 /*254167*/, 267522 /*267522*/, 253259 /*253259*/, 264751 /*264751*/, 264750 /*264750*/);
INSERT INTO `quest_objectives` (`ID`, `QuestID`, `Type`, `StorageIndex`, `ObjectID`, `Amount`, `Flags`, `Flags2`, `ProgressBarWeight`, `VerifiedBuild`) VALUES
(266049, 13591, 1, 0, 46318, 1, 0, 0, 0, 23420), -- 266049
(253530, 13515, 0, 0, 32862, 1, 1, 0, 0, 23420), -- 253530
(266598, 13514, 0, 1, 33913, 6, 0, 0, 0, 23420), -- 266598
(266597, 13514, 0, 0, 32858, 30, 0, 0, 0, 23420), -- 266597
(267435, 13590, 1, 0, 44985, 1, 0, 1, 0, 23420), -- 267435
(264536, 13844, 1, 1, 45944, 1, 0, 1, 0, 23420), -- 264536
(264535, 13844, 0, 0, 34033, 1, 1, 0, 0, 23420), -- 264535
(251687, 13513, 1, 0, 44942, 6, 0, 1, 0, 23420), -- 251687
(262505, 13512, 0, 1, 32869, 1, 1, 0, 0, 23420), -- 262505
(262504, 13512, 0, 0, 32868, 1, 1, 0, 0, 23420), -- 262504
(267491, 13511, 0, 0, 32970, 1, 1, 0, 0, 23420), -- 267491
(253228, 13507, 0, 1, 34248, 6, 0, 0, 0, 23420), -- 253228
(253227, 13507, 0, 0, 32859, 6, 0, 0, 0, 23420), -- 253227
(267385, 13509, 0, 0, 33056, 12, 0, 0, 0, 23420), -- 267385
(253325, 13506, 1, 0, 44979, 1, 0, 0, 0, 23420), -- 253325
(265676, 13504, 0, 0, 32861, 10, 0, 0, 0, 23420), -- 265676
(267309, 13505, 1, 0, 44830, 8, 0, 1, 0, 23420), -- 267309
(256718, 13560, 0, 0, 32852, 50, 0, 0, 0, 23420), -- 256718
(252950, 13598, 1, 0, 44976, 6, 0, 1, 0, 23420), -- 252950
(266473, 13566, 1, 2, 45885, 3, 0, 1, 0, 23420), -- 266473
(266472, 13566, 1, 1, 45027, 3, 0, 1, 0, 23420), -- 266472
(266471, 13566, 1, 0, 44969, 3, 0, 1, 0, 23420), -- 266471
(266154, 13565, 0, 1, 34010, 6, 0, 0, 0, 23420), -- 266154
(266153, 13565, 0, 0, 33207, 1, 1, 0, 0, 23420), -- 266153
(267649, 13564, 10, 0, -1, 0, 0, 0, 0, 23420), -- 267649
(265958, 13563, 1, 1, 5382, 1, 0, 1, 0, 23420), -- 265958
(265957, 13563, 0, 0, 33181, 1, 1, 0, 0, 23420), -- 265957
(256013, 13831, 1, 0, 45898, 1, 0, 1, 0, 23420), -- 256013
(256239, 13557, 1, 1, 44925, 1, 0, 1, 0, 23420), -- 256239
(256238, 13557, 0, 0, 33024, 8, 0, 0, 0, 23420), -- 256238
(266621, 13529, 0, 1, 33021, 8, 0, 0, 0, 23420), -- 266621
(266620, 13529, 0, 0, 33020, 1, 1, 0, 0, 23420), -- 266620
(257477, 13554, 1, 0, 44966, 6, 0, 1, 0, 23420), -- 257477
(266022, 13528, 1, 0, 44913, 6, 0, 1, 0, 23420), -- 266022
(256073, 13527, 1, 0, 44911, 1, 0, 1, 0, 23420), -- 256073
(266042, 13521, 1, 0, 44863, 4, 0, 1, 0, 23420), -- 266042
(255170, 13537, 1, 0, 12238, 4, 0, 0, 0, 23420), -- 255170
(253095, 13520, 1, 0, 44864, 16, 0, 1, 0, 23420), -- 253095
(266016, 13561, 0, 1, 33180, 6, 0, 0, 0, 23420), -- 266016
(266015, 13561, 0, 0, 33179, 6, 0, 0, 0, 23420), -- 266015
(254184, 13522, 0, 0, 32928, 8, 0, 0, 0, 23420), -- 254184
(264115, 13900, 0, 0, 51314, 1, 0, 0, 0, 23420), -- 264115
(266615, 13891, 1, 1, 46370, 1, 0, 1, 0, 23420), -- 266615
(266614, 13891, 0, 0, 34331, 1, 0, 0, 0, 23420), -- 266614
(267419, 13546, 0, 0, 32996, 1, 0, 0, 0, 23420), -- 267419
(267644, 13885, 0, 2, 34324, 8, 0, 0, 0, 23420), -- 267644
(267643, 13885, 0, 1, 34325, 8, 0, 0, 0, 23420), -- 267643
(267642, 13885, 0, 0, 34323, 8, 0, 0, 0, 23420), -- 267642
(267505, 13572, 2, 0, 194150, 8, 0, 0, 0, 23420), -- 267505
(264908, 13545, 0, 0, 33000, 7, 0, 0, 0, 23420), -- 264908
(264976, 13898, 0, 0, 33079, 8, 0, 0, 0, 23420), -- 264976
(266617, 13925, 1, 1, 46363, 1, 0, 1, 0, 23420), -- 266617
(266616, 13925, 0, 0, 34373, 1, 0, 0, 0, 23420), -- 266616
(267514, 13882, 1, 2, 46355, 1, 0, 1, 0, 23420), -- 267514
(267513, 13882, 1, 1, 46356, 1, 0, 1, 0, 23420), -- 267513
(267512, 13882, 1, 0, 46354, 1, 0, 1, 0, 23420), -- 267512
(267315, 13544, 1, 0, 44887, 1, 0, 1, 0, 23420), -- 267315
(264781, 13948, 0, 0, 34411, 1, 0, 0, 0, 23420), -- 264781
(263713, 13910, 1, 1, 46385, 1, 0, 1, 0, 23420), -- 263713
(263712, 13910, 0, 0, 34349, 1, 0, 0, 0, 23420), -- 263712
(267081, 13918, 1, 1, 46388, 1, 0, 1, 0, 23420), -- 267081
(267080, 13918, 1, 0, 46387, 1, 0, 1, 0, 23420), -- 267080
(265849, 13909, 1, 0, 46384, 5, 0, 1, 0, 23420), -- 265849
(267359, 13907, 0, 0, 34344, 10, 0, 0, 0, 23420), -- 267359
(267171, 13912, 1, 0, 46386, 1, 0, 1, 0, 23420), -- 267171
(252342, 13526, 1, 0, 44850, 8, 0, 1, 0, 23420), -- 252342
(266217, 13881, 10, 1, -1, 0, 0, 0, 0, 23420), -- 266217
(266216, 13881, 0, 0, 34302, 6, 0, 0, 0, 23420), -- 266216
(266852, 13892, 0, 0, 34410, 1, 0, 0, 0, 23420), -- 266852
(267269, 13588, 0, 1, 34282, 12, 0, 0, 0, 23420), -- 267269
(267268, 13588, 0, 0, 34316, 1, 0, 0, 0, 23420), -- 267268
(253558, 13587, 1, 0, 46695, 1, 0, 1, 0, 23420), -- 253558
(266121, 13582, 1, 0, 46692, 1, 0, 1, 0, 23420), -- 266121
(257798, 13578, 1, 0, 44960, 8, 0, 1, 0, 23420), -- 257798
(263989, 13580, 0, 0, 34371, 1, 0, 0, 0, 23420), -- 263989
(263993, 13576, 0, 0, 33165, 8, 0, 0, 0, 23420), -- 263993
(263710, 13584, 0, 0, 33083, 8, 0, 0, 0, 23420), -- 263710
(267388, 13558, 1, 1, 44929, 1, 1, 1, 0, 23420), -- 267388
(266057, 13547, 0, 3, 33037, 1, 0, 0, 0, 23420), -- 266057
(266056, 13547, 0, 2, 33035, 1, 0, 0, 0, 23420), -- 266056
(266055, 13547, 0, 1, 33033, 1, 0, 0, 0, 23420), -- 266055
(266054, 13547, 0, 0, 33001, 1, 0, 0, 0, 23420), -- 266054
(252588, 13542, 0, 0, 32986, 8, 0, 0, 0, 23420), -- 252588
(254169, 13543, 0, 2, 32990, 1, 0, 0, 0, 23420), -- 254169
(254168, 13543, 0, 1, 32989, 1, 0, 0, 0, 23420), -- 254168
(254167, 13543, 0, 0, 32988, 1, 0, 0, 0, 23420), -- 254167
(267522, 13523, 0, 0, 32937, 6, 0, 0, 0, 23420), -- 267522
(253259, 13596, 1, 0, 44968, 6, 0, 1, 0, 23420), -- 253259
(264751, 13519, 0, 1, 32888, 10, 0, 0, 0, 23420), -- 264751
(264750, 13519, 0, 0, 32899, 1, 1, 0, 0, 23420); -- 264750


DELETE FROM `quest_visual_effect` WHERE (`ID`=264536 AND `Index`=0);
INSERT INTO `quest_visual_effect` (`ID`, `Index`, `VisualEffect`, `VerifiedBuild`) VALUES
(264536, 0, 2099, 23420);


DELETE FROM `creature_questitem` WHERE (`CreatureEntry`=33181 AND `Idx`=0) OR (`CreatureEntry`=33022 AND `Idx`=0) OR (`CreatureEntry`=33020 AND `Idx`=0) OR (`CreatureEntry`=33021 AND `Idx`=0) OR (`CreatureEntry`=32860 AND `Idx`=0) OR (`CreatureEntry`=33905 AND `Idx`=0) OR (`CreatureEntry`=33009 AND `Idx`=0) OR (`CreatureEntry`=11982 AND `Idx`=0) OR (`CreatureEntry`=2206 AND `Idx`=0) OR (`CreatureEntry`=2233 AND `Idx`=2) OR (`CreatureEntry`=2233 AND `Idx`=1) OR (`CreatureEntry`=2233 AND `Idx`=0) OR (`CreatureEntry`=2207 AND `Idx`=0) OR (`CreatureEntry`=32997 AND `Idx`=0) OR (`CreatureEntry`=2165 AND `Idx`=0) OR (`CreatureEntry`=2071 AND `Idx`=1) OR (`CreatureEntry`=2071 AND `Idx`=0) OR (`CreatureEntry`=2070 AND `Idx`=0) OR (`CreatureEntry`=2237 AND `Idx`=1) OR (`CreatureEntry`=2237 AND `Idx`=0) OR (`CreatureEntry`=34398 AND `Idx`=0) OR (`CreatureEntry`=34385 AND `Idx`=0) OR (`CreatureEntry`=33041 AND `Idx`=0) OR (`CreatureEntry`=32985 AND `Idx`=0) OR (`CreatureEntry`=113201 AND `Idx`=2) OR (`CreatureEntry`=113201 AND `Idx`=1) OR (`CreatureEntry`=113201 AND `Idx`=0);
INSERT INTO `creature_questitem` (`CreatureEntry`, `Idx`, `ItemId`, `VerifiedBuild`) VALUES
(33181, 0, 5382, 23420), -- 33181
(33022, 0, 44966, 23420), -- 33022
(33020, 0, 44966, 23420), -- 33020
(33021, 0, 44966, 23420), -- 33021
(32860, 0, 44942, 23420), -- 32860
(33905, 0, 44913, 23420), -- 33905
(33009, 0, 44913, 23420), -- 33009
(11982, 0, 20951, 23420), -- 11982
(2206, 0, 7499, 23420), -- 2206
(2233, 2, 12237, 23420), -- 2233
(2233, 1, 7500, 23420), -- 2233
(2233, 0, 5385, 23420), -- 2233
(2207, 0, 7499, 23420), -- 2207
(32997, 0, 44886, 23420), -- 32997
(2165, 0, 5414, 23420), -- 2165
(2071, 1, 5413, 23420), -- 2071
(2071, 0, 5386, 23420), -- 2071
(2070, 0, 5413, 23420), -- 2070
(2237, 1, 5413, 23420), -- 2237
(2237, 0, 5386, 23420), -- 2237
(34398, 0, 46695, 23420), -- 34398
(34385, 0, 46692, 23420), -- 34385
(33041, 0, 44929, 23420), -- 33041
(32985, 0, 44868, 23420), -- 32985
(113201, 2, 134808, 23420), -- 113201
(113201, 1, 129903, 23420), -- 113201
(113201, 0, 129888, 23420); -- 113201


DELETE FROM `gameobject_template` WHERE `entry` IN (195014 /*195014*/, 175984 /*175984*/, 176291 /*176291*/, 194787 /*194787*/, 195006 /*195006*/, 2045 /*2045*/, 194101 /*194101*/, 204130 /*204130*/, 194103 /*194103*/, 194102 /*194102*/, 194104 /*194104*/, 194088 /*194088*/, 176289 /*176289*/, 194090 /*194090*/, 13873 /*13873*/, 194089 /*194089*/, 13359 /*13359*/, 194145 /*194145*/, 194771 /*194771*/, 194204 /*194204*/, 194209 /*194209*/, 194208 /*194208*/, 180683 /*180683*/, 91737 /*91737*/, 194714 /*194714*/, 180663 /*180663*/, 194130 /*194130*/, 194124 /*194124*/, 194133 /*194133*/, 194131 /*194131*/, 194122 /*194122*/, 1621 /*1621*/, 266354 /*266354*/, 35591 /*35591*/, 180655 /*180655*/, 194179 /*194179*/, 1620 /*1620*/, 194105 /*194105*/, 204228 /*204228*/, 195078 /*195078*/, 1732 /*1732*/, 1926 /*1926*/, 1923 /*1923*/, 194144 /*194144*/, 194142 /*194142*/, 194141 /*194141*/, 195045 /*195045*/, 195044 /*195044*/, 195056 /*195056*/, 195055 /*195055*/, 175227 /*175227*/, 195080 /*195080*/, 195043 /*195043*/, 195021 /*195021*/, 195042 /*195042*/, 195054 /*195054*/, 195058 /*195058*/, 194143 /*194143*/, 61929 /*61929*/, 178147 /*178147*/, 17185 /*17185*/, 1921 /*1921*/, 194139 /*194139*/, 194150 /*194150*/, 194140 /*194140*/, 194100 /*194100*/, 195057 /*195057*/, 194114 /*194114*/, 194106 /*194106*/, 16393 /*16393*/, 180474 /*180474*/, 195071 /*195071*/, 180475 /*180475*/, 180480 /*180480*/, 180477 /*180477*/, 180473 /*180473*/, 180476 /*180476*/, 180479 /*180479*/, 180478 /*180478*/, 176190 /*176190*/, 195070 /*195070*/, 194113 /*194113*/, 195007 /*195007*/, 92489 /*92489*/, 92490 /*92490*/, 103687 /*103687*/, 194112 /*194112*/, 17182 /*17182*/, 194211 /*194211*/, 177280 /*177280*/, 208296 /*208296*/, 17183 /*17183*/);
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `IconName`, `unk1`, `size`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `Data6`, `Data7`, `Data8`, `Data9`, `Data10`, `Data11`, `Data12`, `Data13`, `Data14`, `Data15`, `Data16`, `Data17`, `Data18`, `Data19`, `Data20`, `Data21`, `Data22`, `Data23`, `Data24`, `Data25`, `Data26`, `Data27`, `Data28`, `Data29`, `Data30`, `Data31`, `Data32`, `RequiredLevel`, `VerifiedBuild`) VALUES
(195014, 9, 8507, '', '', 2.5, 3567, 0, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195014
(175984, 8, 216, '', '', 1, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 175984
(176291, 8, 200, '', '', 0.94, 4, 10, 2066, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 176291
(194787, 3, 2530, '', '', 1, 43, 27011, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24124, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194787
(195006, 10, 6424, '', '', 1, 0, 13560, 0, 3, 0, 0, 0, 0, 0, 0, 64924, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195006
(2045, 50, 700, '', '', 0.5, 29, 51479, 0, 0, 110, 160, 30, 0, 0, 0, 0, 0, 25, 1, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 2045
(194101, 10, 1787, '', '', 1, 1923, 0, 0, 0, 0, 0, 0, 0, 0, 0, 62824, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194101
(204130, 8, 200, '', '', 1.399414, 4, 10, 2066, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 204130
(194103, 5, 7635, '', '', 1, 0, 1, 0, 0, 0, 13509, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194103
(194102, 5, 5531, '', '', 1, 0, 1, 0, 0, 0, 13509, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194102
(194104, 5, 1988, '', '', 1, 0, 1, 0, 0, 0, 13509, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194104
(194088, 3, 404, '', '', 1.2, 57, 26821, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194088
(176289, 8, 200, '', '', 1.09, 4, 10, 2066, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 176289
(194090, 3, 405, '', '', 1.5, 57, 26821, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194090
(13873, 3, 407, '', '', 3, 57, 2768, 0, 1, 0, 0, 0, 12653, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13873
(194089, 3, 406, '', '', 4, 57, 26821, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194089
(13359, 3, 407, '', '', 3, 57, 2768, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 13359
(194145, 10, 8552, '', '', 2, 93, 13558, 20898, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 27700, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194145
(194771, 2, 8683, '', '', 0.5, 0, 0, 0, 10410, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194771
(194204, 3, 163, '', '', 0.7, 57, 26866, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194204
(194209, 3, 329, '', '', 0.4, 57, 26867, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194209
(194208, 3, 329, '', '', 0.6, 57, 26867, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194208
(180683, 25, 6482, '', '', 1, 4, 17521, 2, 6, 1628, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180683
(91737, 8, 396, '', '', 1, 0, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 91737
(194714, 2, 4391, '', '', 0.75, 0, 9610, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194714
(180663, 25, 6435, '', '', 1, 4, 17502, 4, 8, 1628, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180663
(194130, 5, 6651, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194130
(194124, 10, 676, '', '', 1, 1821, 0, 0, 0, 0, 0, 0, 0, 0, 0, 62141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194124
(194133, 10, 676, '', '', 0.6, 1821, 0, 0, 0, 0, 0, 0, 0, 0, 0, 62141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194133
(194131, 5, 6651, '', '', 0.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194131
(194122, 2, 356, '', '', 1, 0, 9478, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194122
(1621, 50, 271, '', '', 0.4, 29, 1418, 0, 0, 125, 175, 30, 0, 0, 0, 0, 0, 25, 1, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 1621
(266354, 8, 39107, '', '', 1, 4, 10, 31442, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 266354
(35591, 17, 668, 'Fishing', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 35591
(180655, 25, 6434, '', '', 1, 4, 17441, 2, 6, 1628, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180655
(194179, 10, 5151, '', '', 1, 1634, 13562, 20957, 20, 0, 0, 0, 0, 0, 0, 62497, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194179
(1620, 50, 268, '', '', 0.3, 29, 1417, 0, 0, 100, 150, 30, 0, 0, 0, 0, 0, 25, 1, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 1620
(194105, 2, 356, '', '', 1, 0, 9477, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194105
(204228, 8, 9690, '', '', 3, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 204228
(195078, 5, 8833, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195078
(1732, 50, 315, '', '', 0.5, 38, 1503, 0, 0, 100, 150, 30, 0, 0, 0, 0, 0, 25, 1, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 1732
(1926, 8, 192, '', '', 0.85, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 1926
(1923, 8, 192, '', '', 1, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 1923
(194144, 8, 192, '', '', 1, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194144
(194142, 8, 192, '', '', 1, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194142
(194141, 8, 192, '', '', 1, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194141
(195045, 5, 7517, '', '', 0.8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195045
(195044, 5, 7518, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195044
(195056, 10, 6428, '', '', 0.35, 1836, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195056
(195055, 3, 49, '', '', 1, 43, 27251, 0, 1, 0, 0, 0, 0, 13918, 0, 0, 0, 0, 0, 19676, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195055
(175227, 2, 3533, '', '', 1, 259, 3873, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 175227
(195080, 3, 6682, '', '', 1, 1691, 27249, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24982, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195080
(195043, 8, 8757, '', '', 0.75, 1609, 10, 0, 0, 13910, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195043
(195021, 3, 261, '', '', 0.45, 43, 27237, 0, 1, 0, 0, 0, 195061, 0, 0, 0, 0, 0, 0, 21400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195021
(195042, 3, 8688, '', '', 0.75, 1691, 27249, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24982, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195042
(195054, 3, 8763, '', '', 1, 43, 27250, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23645, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195054
(195058, 8, 192, '', '', 0.73, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195058
(194143, 8, 192, '', '', 1, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194143
(61929, 8, 200, '', '', 1.85, 4, 20, 2066, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 61929
(178147, 0, 4154, '', '', 1.5, 0, 0, 6000, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 178147
(17185, 2, 356, '', '', 1.04, 0, 571, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17185
(1921, 8, 192, '', '', 1, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 1921
(194139, 8, 192, '', '', 1.08, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194139
(194150, 10, 8553, 'Attack', '', 0.55, 99, 13572, 0, 196608, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 24585, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194150
(194140, 8, 6411, '', '', 1, 4, 10, 2066, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194140
(194100, 3, 7816, 'GatherHerbs', '', 1, 1702, 26825, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194100
(195057, 5, 7292, '', '', 3, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195057
(194114, 5, 249, '', '', 1.25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194114
(194106, 8, 6821, '', '', 1, 1595, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194106
(16393, 10, 249, '', '', 1, 80, 957, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 23420), -- 16393
(180474, 8, 475, '', '', 0.671141, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180474
(195071, 22, 1327, '', '', 1, 65409, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195071
(180475, 8, 475, '', '', 0.671141, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180475
(180480, 8, 475, '', '', 1, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180480
(180477, 8, 475, '', '', 0.671141, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180477
(180473, 8, 475, '', '', 0.671141, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180473
(180476, 8, 475, '', '', 0.671141, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180476
(180479, 8, 475, '', '', 0.671141, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180479
(180478, 8, 475, '', '', 0.671141, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 180478
(176190, 2, 4153, '', '', 1, 259, 3871, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 176190
(195070, 8, 680, '', '', 1, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195070
(194113, 5, 7069, '', '', 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194113
(195007, 3, 2630, '', '', 1, 43, 27217, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 195007
(92489, 8, 273, '', '', 1, 1, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 92489
(92490, 8, 305, '', '', 1, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 92490
(103687, 8, 192, '', '', 1, 4, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 103687
(194112, 5, 7069, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194112
(17182, 2, 356, '', '', 1.539999, 0, 569, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17182
(194211, 5, 181, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 194211
(177280, 8, 1407, '', '', 1, 883, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 177280
(208296, 8, 192, '', '', 1.16992, 4, 10, 2061, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420), -- 208296
(17183, 2, 356, '', '', 1.19, 0, 570, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420); -- 17183


DELETE FROM `gameobject_questitem` WHERE (`GameObjectEntry`=194787 AND `Idx`=0) OR (`GameObjectEntry`=194088 AND `Idx`=0) OR (`GameObjectEntry`=194090 AND `Idx`=0) OR (`GameObjectEntry`=194089 AND `Idx`=0) OR (`GameObjectEntry`=194204 AND `Idx`=0) OR (`GameObjectEntry`=194209 AND `Idx`=0) OR (`GameObjectEntry`=194208 AND `Idx`=0) OR (`GameObjectEntry`=195055 AND `Idx`=0) OR (`GameObjectEntry`=195080 AND `Idx`=0) OR (`GameObjectEntry`=195021 AND `Idx`=0) OR (`GameObjectEntry`=195042 AND `Idx`=0) OR (`GameObjectEntry`=195054 AND `Idx`=0) OR (`GameObjectEntry`=194100 AND `Idx`=0) OR (`GameObjectEntry`=195007 AND `Idx`=0);
INSERT INTO `gameobject_questitem` (`GameObjectEntry`, `Idx`, `ItemId`, `VerifiedBuild`) VALUES
(194787, 0, 45944, 23420), -- 194787
(194088, 0, 44830, 23420), -- 194088
(194090, 0, 44830, 23420), -- 194090
(194089, 0, 44830, 23420), -- 194089
(194204, 0, 44968, 23420), -- 194204
(194209, 0, 44976, 23420), -- 194209
(194208, 0, 44976, 23420), -- 194208
(195055, 0, 46702, 23420), -- 195055
(195080, 0, 46384, 23420), -- 195080
(195021, 0, 46356, 23420), -- 195021
(195042, 0, 46384, 23420), -- 195042
(195054, 0, 46386, 23420), -- 195054
(194100, 0, 44850, 23420), -- 194100
(195007, 0, 44960, 23420); -- 195007


DELETE FROM `npc_text` WHERE `ID` IN (14469 /*14469*/, 14510 /*14510*/, 14468 /*14468*/, 14506 /*14506*/, 14447 /*14447*/, 14505 /*14505*/, 14299 /*14299*/, 14296 /*14296*/, 14293 /*14293*/, 14292 /*14292*/, 14295 /*14295*/, 14297 /*14297*/, 14291 /*14291*/, 14290 /*14290*/, 14300 /*14300*/, 14298 /*14298*/, 14454 /*14454*/, 14508 /*14508*/, 5121 /*5121*/, 17859 /*17859*/, 10652 /*10652*/, 14304 /*14304*/, 20326 /*20326*/, 4437 /*4437*/, 5722 /*5722*/, 31032 /*31032*/, 538 /*538*/, 21709 /*21709*/, 4993 /*4993*/, 4794 /*4794*/, 5725 /*5725*/, 824 /*824*/, 5473 /*5473*/, 9384 /*9384*/, 17861 /*17861*/, 10654 /*10654*/, 14526 /*14526*/, 14524 /*14524*/, 14511 /*14511*/, 14518 /*14518*/, 14517 /*14517*/, 14541 /*14541*/, 16615 /*16615*/, 14547 /*14547*/, 14548 /*14548*/, 14509 /*14509*/, 14264 /*14264*/, 14259 /*14259*/, 14276 /*14276*/, 14274 /*14274*/, 14275 /*14275*/, 14260 /*14260*/);
INSERT INTO `npc_text` (`ID`, `Probability0`, `Probability1`, `Probability2`, `Probability3`, `Probability4`, `Probability5`, `Probability6`, `Probability7`, `BroadcastTextId0`, `BroadcastTextId1`, `BroadcastTextId2`, `BroadcastTextId3`, `BroadcastTextId4`, `BroadcastTextId5`, `BroadcastTextId6`, `BroadcastTextId7`, `VerifiedBuild`) VALUES
(14469, 1, 0, 0, 0, 0, 0, 0, 0, 34221, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14469
(14510, 1, 0, 0, 0, 0, 0, 0, 0, 34472, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14510
(14468, 1, 0, 0, 0, 0, 0, 0, 0, 34215, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14468
(14506, 1, 0, 0, 0, 0, 0, 0, 0, 34464, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14506
(14447, 1, 0, 0, 0, 0, 0, 0, 0, 34036, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14447
(14505, 1, 0, 0, 0, 0, 0, 0, 0, 34463, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14505
(14299, 1, 0, 0, 0, 0, 0, 0, 0, 33150, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14299
(14296, 1, 0, 0, 0, 0, 0, 0, 0, 33162, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14296
(14293, 1, 0, 0, 0, 0, 0, 0, 0, 33159, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14293
(14292, 1, 0, 0, 0, 0, 0, 0, 0, 33158, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14292
(14295, 1, 0, 0, 0, 0, 0, 0, 0, 33161, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14295
(14297, 1, 0, 0, 0, 0, 0, 0, 0, 33147, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14297
(14291, 1, 0, 0, 0, 0, 0, 0, 0, 33157, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14291
(14290, 1, 0, 0, 0, 0, 0, 0, 0, 33156, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14290
(14300, 1, 0, 0, 0, 0, 0, 0, 0, 33151, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14300
(14298, 1, 0, 0, 0, 0, 0, 0, 0, 33149, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14298
(14454, 1, 1, 1, 1, 1, 1, 1, 1, 34095, 34096, 34097, 34098, 34099, 34100, 34101, 34102, 23420), -- 14454
(14508, 1, 0, 0, 0, 0, 0, 0, 0, 34469, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14508
(5121, 1, 0, 0, 0, 0, 0, 0, 0, 7805, 0, 0, 0, 0, 0, 0, 0, 23420), -- 5121
(17859, 1, 0, 0, 0, 0, 0, 0, 0, 50840, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17859
(10652, 1, 0, 0, 0, 0, 0, 0, 0, 20133, 0, 0, 0, 0, 0, 0, 0, 23420), -- 10652
(14304, 1, 0, 0, 0, 0, 0, 0, 0, 33327, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14304
(20326, 1, 0, 0, 0, 0, 0, 0, 0, 64116, 0, 0, 0, 0, 0, 0, 0, 23420), -- 20326
(4437, 1, 0, 0, 0, 0, 0, 0, 0, 7165, 0, 0, 0, 0, 0, 0, 0, 23420), -- 4437
(5722, 1, 0, 0, 0, 0, 0, 0, 0, 8300, 0, 0, 0, 0, 0, 0, 0, 23420), -- 5722
(31032, 1, 0, 0, 0, 0, 0, 0, 0, 125290, 0, 0, 0, 0, 0, 0, 0, 23420), -- 31032
(538, 1, 0, 0, 0, 0, 0, 0, 0, 2502, 0, 0, 0, 0, 0, 0, 0, 23420), -- 538
(21709, 1, 0, 0, 0, 0, 0, 0, 0, 49490, 0, 0, 0, 0, 0, 0, 0, 23420), -- 21709
(4993, 1, 0, 0, 0, 0, 0, 0, 0, 7642, 0, 0, 0, 0, 0, 0, 0, 23420), -- 4993
(4794, 1, 0, 0, 0, 0, 0, 0, 0, 7477, 0, 0, 0, 0, 0, 0, 0, 23420), -- 4794
(5725, 1, 0, 0, 0, 0, 0, 0, 0, 8303, 0, 0, 0, 0, 0, 0, 0, 23420), -- 5725
(824, 100, 0, 0, 0, 0, 0, 0, 0, 2827, 0, 0, 0, 0, 0, 0, 0, 23420), -- 824
(5473, 1, 0, 0, 0, 0, 0, 0, 0, 8098, 0, 0, 0, 0, 0, 0, 0, 23420), -- 5473
(9384, 1, 0, 0, 0, 0, 0, 0, 0, 15411, 0, 0, 0, 0, 0, 0, 0, 23420), -- 9384
(17861, 1, 0, 0, 0, 0, 0, 0, 0, 50850, 0, 0, 0, 0, 0, 0, 0, 23420), -- 17861
(10654, 1, 0, 0, 0, 0, 0, 0, 0, 20135, 0, 0, 0, 0, 0, 0, 0, 23420), -- 10654
(14526, 1, 0, 0, 0, 0, 0, 0, 0, 34515, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14526
(14524, 1, 0, 0, 0, 0, 0, 0, 0, 34512, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14524
(14511, 1, 0, 0, 0, 0, 0, 0, 0, 34473, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14511
(14518, 1, 0, 0, 0, 0, 0, 0, 0, 34487, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14518
(14517, 1, 0, 0, 0, 0, 0, 0, 0, 34485, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14517
(14541, 1, 0, 0, 0, 0, 0, 0, 0, 34584, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14541
(16615, 1, 0, 0, 0, 0, 0, 0, 0, 44625, 0, 0, 0, 0, 0, 0, 0, 23420), -- 16615
(14547, 1, 0, 0, 0, 0, 0, 0, 0, 34637, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14547
(14548, 1, 0, 0, 0, 0, 0, 0, 0, 34638, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14548
(14509, 1, 0, 0, 0, 0, 0, 0, 0, 34470, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14509
(14264, 1, 0, 0, 0, 0, 0, 0, 0, 33185, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14264
(14259, 1, 0, 0, 0, 0, 0, 0, 0, 33174, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14259
(14276, 1, 0, 0, 0, 0, 0, 0, 0, 33210, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14276
(14274, 1, 0, 0, 0, 0, 0, 0, 0, 33206, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14274
(14275, 1, 0, 0, 0, 0, 0, 0, 0, 33208, 0, 0, 0, 0, 0, 0, 0, 23420), -- 14275
(14260, 1, 0, 0, 0, 0, 0, 0, 0, 33175, 0, 0, 0, 0, 0, 0, 0, 23420); -- 14260