-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_0_23222
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/26/2017 00:03:39


DELETE FROM `areatrigger_template` WHERE `Id`=10003;
INSERT INTO `areatrigger_template` (`Id`, `Type`, `Flags`, `Data0`, `Data1`, `Data2`, `Data3`, `Data4`, `Data5`, `VerifiedBuild`) VALUES
(10003, 0, 1030, 1.5, 1.5, 0, 0, 0, 0, 23222);


DELETE FROM `gameobject_template_locale` WHERE (`entry`=239271 AND `locale`='deDE') OR (`entry`=239284 AND `locale`='deDE') OR (`entry`=239282 AND `locale`='deDE') OR (`entry`=239285 AND `locale`='deDE') OR (`entry`=239281 AND `locale`='deDE') OR (`entry`=239280 AND `locale`='deDE') OR (`entry`=239279 AND `locale`='deDE') OR (`entry`=239278 AND `locale`='deDE') OR (`entry`=239277 AND `locale`='deDE') OR (`entry`=239276 AND `locale`='deDE') OR (`entry`=237886 AND `locale`='deDE') OR (`entry`=239273 AND `locale`='deDE') OR (`entry`=239272 AND `locale`='deDE') OR (`entry`=239275 AND `locale`='deDE') OR (`entry`=239274 AND `locale`='deDE') OR (`entry`=238668 AND `locale`='deDE') OR (`entry`=236995 AND `locale`='deDE') OR (`entry`=236994 AND `locale`='deDE') OR (`entry`=236993 AND `locale`='deDE') OR (`entry`=235883 AND `locale`='deDE') OR (`entry`=236219 AND `locale`='deDE') OR (`entry`=238944 AND `locale`='deDE') OR (`entry`=238945 AND `locale`='deDE') OR (`entry`=237622 AND `locale`='deDE') OR (`entry`=237621 AND `locale`='deDE') OR (`entry`=237618 AND `locale`='deDE') OR (`entry`=225789 AND `locale`='deDE') OR (`entry`=236184 AND `locale`='deDE') OR (`entry`=239439 AND `locale`='deDE') OR (`entry`=237620 AND `locale`='deDE') OR (`entry`=237619 AND `locale`='deDE') OR (`entry`=238946 AND `locale`='deDE') OR (`entry`=238667 AND `locale`='deDE') OR (`entry`=228653 AND `locale`='deDE') OR (`entry`=228646 AND `locale`='deDE') OR (`entry`=228651 AND `locale`='deDE') OR (`entry`=228642 AND `locale`='deDE') OR (`entry`=228644 AND `locale`='deDE') OR (`entry`=228649 AND `locale`='deDE') OR (`entry`=239438 AND `locale`='deDE') OR (`entry`=237290 AND `locale`='deDE') OR (`entry`=237291 AND `locale`='deDE') OR (`entry`=237289 AND `locale`='deDE') OR (`entry`=236979 AND `locale`='deDE') OR (`entry`=236815 AND `locale`='deDE') OR (`entry`=236806 AND `locale`='deDE') OR (`entry`=236814 AND `locale`='deDE') OR (`entry`=236804 AND `locale`='deDE') OR (`entry`=236805 AND `locale`='deDE') OR (`entry`=236817 AND `locale`='deDE') OR (`entry`=236813 AND `locale`='deDE') OR (`entry`=236803 AND `locale`='deDE') OR (`entry`=236812 AND `locale`='deDE') OR (`entry`=236811 AND `locale`='deDE') OR (`entry`=236800 AND `locale`='deDE') OR (`entry`=236816 AND `locale`='deDE') OR (`entry`=236818 AND `locale`='deDE') OR (`entry`=236802 AND `locale`='deDE') OR (`entry`=236799 AND `locale`='deDE') OR (`entry`=236801 AND `locale`='deDE') OR (`entry`=236808 AND `locale`='deDE') OR (`entry`=236809 AND `locale`='deDE') OR (`entry`=236810 AND `locale`='deDE') OR (`entry`=233475 AND `locale`='deDE') OR (`entry`=236807 AND `locale`='deDE') OR (`entry`=233474 AND `locale`='deDE') OR (`entry`=233479 AND `locale`='deDE') OR (`entry`=233476 AND `locale`='deDE') OR (`entry`=232978 AND `locale`='deDE') OR (`entry`=236825 AND `locale`='deDE') OR (`entry`=236824 AND `locale`='deDE') OR (`entry`=233481 AND `locale`='deDE') OR (`entry`=236827 AND `locale`='deDE') OR (`entry`=233473 AND `locale`='deDE') OR (`entry`=233464 AND `locale`='deDE') OR (`entry`=233480 AND `locale`='deDE') OR (`entry`=236836 AND `locale`='deDE') OR (`entry`=236834 AND `locale`='deDE') OR (`entry`=233465 AND `locale`='deDE') OR (`entry`=233466 AND `locale`='deDE') OR (`entry`=236832 AND `locale`='deDE') OR (`entry`=236835 AND `locale`='deDE') OR (`entry`=233463 AND `locale`='deDE') OR (`entry`=233467 AND `locale`='deDE') OR (`entry`=236831 AND `locale`='deDE') OR (`entry`=236821 AND `locale`='deDE') OR (`entry`=236833 AND `locale`='deDE') OR (`entry`=236830 AND `locale`='deDE') OR (`entry`=236822 AND `locale`='deDE') OR (`entry`=233472 AND `locale`='deDE') OR (`entry`=233468 AND `locale`='deDE') OR (`entry`=233477 AND `locale`='deDE') OR (`entry`=233469 AND `locale`='deDE') OR (`entry`=233470 AND `locale`='deDE') OR (`entry`=233478 AND `locale`='deDE') OR (`entry`=233471 AND `locale`='deDE') OR (`entry`=237259 AND `locale`='deDE') OR (`entry`=266354 AND `locale`='deDE');
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(239271, 'deDE', 'Bank', '', NULL, 23222),
(239284, 'deDE', 'Bank', '', NULL, 23222),
(239282, 'deDE', 'Bank', '', NULL, 23222),
(239285, 'deDE', 'Bank', '', NULL, 23222),
(239281, 'deDE', 'Bank', '', NULL, 23222),
(239280, 'deDE', 'Bank', '', NULL, 23222),
(239279, 'deDE', 'Bank', '', NULL, 23222),
(239278, 'deDE', 'Bank', '', NULL, 23222),
(239277, 'deDE', 'Bank', '', NULL, 23222),
(239276, 'deDE', 'Stuhl', '', NULL, 23222),
(237886, 'deDE', 'Gesetzbuch des Gefängniswärters', '', NULL, 23222),
(239273, 'deDE', 'Bank', '', NULL, 23222),
(239272, 'deDE', 'Bank', '', NULL, 23222),
(239275, 'deDE', 'Bank', '', NULL, 23222),
(239274, 'deDE', 'Bank', '', NULL, 23222),
(238668, 'deDE', 'Briefkasten', '', NULL, 23222),
(236995, 'deDE', 'Holzbank', '', NULL, 23222),
(236994, 'deDE', 'Stuhl', '', NULL, 23222),
(236993, 'deDE', 'Stuhl', '', NULL, 23222),
(235883, 'deDE', 'Portal nach Sturmwind', '', NULL, 23222),
(236219, 'deDE', 'Stuhl', '', NULL, 23222),
(238944, 'deDE', 'Hocker', '', NULL, 23222),
(238945, 'deDE', 'Hocker', '', NULL, 23222),
(237622, 'deDE', 'Amboss', '', NULL, 23222),
(237621, 'deDE', 'Amboss', '', NULL, 23222),
(237618, 'deDE', 'Bank', '', NULL, 23222),
(225789, 'deDE', 'Osterei', '', NULL, 23222),
(236184, 'deDE', 'Zerbrochenes Schwert', '', NULL, 23222),
(239439, 'deDE', 'Portal nach Darnassus', '', NULL, 23222),
(237620, 'deDE', 'Hocker', '', NULL, 23222),
(237619, 'deDE', 'Hocker', '', NULL, 23222),
(238946, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(238667, 'deDE', 'Briefkasten', '', NULL, 23222),
(228653, 'deDE', 'Zwinge', '', NULL, 23222),
(228646, 'deDE', 'Maschine', '', NULL, 23222),
(228651, 'deDE', 'Hocker', '', NULL, 23222),
(228642, 'deDE', 'Motor', '', NULL, 23222),
(228644, 'deDE', 'Kochbot', '', NULL, 23222),
(228649, 'deDE', 'Post-o-Tron 9001', '', NULL, 23222),
(239438, 'deDE', 'Portal nach Eisenschmiede', '', NULL, 23222),
(237290, 'deDE', 'Schmelzofen', '', NULL, 23222),
(237291, 'deDE', 'Amboss', '', NULL, 23222),
(237289, 'deDE', 'Amboss', '', NULL, 23222),
(236979, 'deDE', 'Waffen', '', NULL, 23222),
(236815, 'deDE', 'Stuhl', '', NULL, 23222),
(236806, 'deDE', 'Holzbank', '', NULL, 23222),
(236814, 'deDE', 'Stuhl', '', NULL, 23222),
(236804, 'deDE', 'Holzbank', '', NULL, 23222),
(236805, 'deDE', 'Holzbank', '', NULL, 23222),
(236817, 'deDE', 'Holzbank', '', NULL, 23222),
(236813, 'deDE', 'Stuhl', '', NULL, 23222),
(236803, 'deDE', 'Holzbank', '', NULL, 23222),
(236812, 'deDE', 'Stuhl', '', NULL, 23222),
(236811, 'deDE', 'Stuhl', '', NULL, 23222),
(236800, 'deDE', 'Holzbank', '', NULL, 23222),
(236816, 'deDE', 'Holzbank', '', NULL, 23222),
(236818, 'deDE', 'Holzbank', '', NULL, 23222),
(236802, 'deDE', 'Holzbank', '', NULL, 23222),
(236799, 'deDE', 'Holzbank', '', NULL, 23222),
(236801, 'deDE', 'Holzbank', '', NULL, 23222),
(236808, 'deDE', 'Stuhl', '', NULL, 23222),
(236809, 'deDE', 'Stuhl', '', NULL, 23222),
(236810, 'deDE', 'Stuhl', '', NULL, 23222),
(233475, 'deDE', 'Holzbank', '', NULL, 23222),
(236807, 'deDE', 'Holzbank', '', NULL, 23222),
(233474, 'deDE', 'Holzbank', '', NULL, 23222),
(233479, 'deDE', 'Stuhl', '', NULL, 23222),
(233476, 'deDE', 'Holzbank', '', NULL, 23222),
(232978, 'deDE', 'Wegweiser', '', NULL, 23222),
(236825, 'deDE', 'Poolpony', '', NULL, 23222),
(236824, 'deDE', 'Flamingo', '', NULL, 23222),
(233481, 'deDE', 'Stuhl', '', NULL, 23222),
(236827, 'deDE', 'Wegweisendes Schild', '', NULL, 23222),
(233473, 'deDE', 'Stuhl', '', NULL, 23222),
(233464, 'deDE', 'Stuhl', '', NULL, 23222),
(233480, 'deDE', 'Holzbank', '', NULL, 23222),
(236836, 'deDE', 'Gnomische Werkzeugkiste', '', NULL, 23222),
(236834, 'deDE', 'Zange des Schmieds', '', NULL, 23222),
(233465, 'deDE', 'Stuhl', '', NULL, 23222),
(233466, 'deDE', 'Stuhl', '', NULL, 23222),
(236832, 'deDE', 'Gnomische Greifzange', '', NULL, 23222),
(236835, 'deDE', 'Gnomischer Bohrer', '', NULL, 23222),
(233463, 'deDE', 'Stuhl', '', NULL, 23222),
(233467, 'deDE', 'Stuhl', '', NULL, 23222),
(236831, 'deDE', 'Gnomischer Schraubbolzen', '', NULL, 23222),
(236821, 'deDE', 'Topf mit Krabbe', '', NULL, 23222),
(236833, 'deDE', 'Gnomischer Schraubenschlüssel', '', NULL, 23222),
(236830, 'deDE', 'Klemme', '', NULL, 23222),
(236822, 'deDE', 'Wetterfahne', '', NULL, 23222),
(233472, 'deDE', 'Stuhl', '', NULL, 23222),
(233468, 'deDE', 'Stuhl', '', NULL, 23222),
(233477, 'deDE', 'Stuhl', '', NULL, 23222),
(233469, 'deDE', 'Stuhl', '', NULL, 23222),
(233470, 'deDE', 'Stuhl', '', NULL, 23222),
(233478, 'deDE', 'Stuhl', '', NULL, 23222),
(233471, 'deDE', 'Stuhl', '', NULL, 23222),
(237259, 'deDE', 'Briefkasten', '', NULL, 23222),
(266354, 'deDE', 'Einfaches Lagerfeuer', '', NULL, 23222);


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=17334 AND `id`=11) OR (`menu_id`=17334 AND `id`=10) OR (`menu_id`=17334 AND `id`=9) OR (`menu_id`=17334 AND `id`=8) OR (`menu_id`=17334 AND `id`=7) OR (`menu_id`=17334 AND `id`=6) OR (`menu_id`=17334 AND `id`=5) OR (`menu_id`=17334 AND `id`=4) OR (`menu_id`=17334 AND `id`=3) OR (`menu_id`=17334 AND `id`=2) OR (`menu_id`=17334 AND `id`=1) OR (`menu_id`=17334 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(17334, 11, '', '', 'Gegenstandsaufwertung', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17334, 10, '', '', 'Verkäufer', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17334, 9, '', '', 'Transmogrifikation & Leerenlager', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17334, 8, '', '', 'Stallmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17334, 7, '', '', 'Berufsausbilder', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17334, 6, '', '', 'Kampfhaustiertrainer', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17334, 5, '', '', 'Portale', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17334, 4, '', '', 'Briefkasten', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17334, 3, '', '', 'Gasthaus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17334, 2, '', '', 'Flugmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17334, 1, '', '', 'Bank', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17334, 0, '', '', 'Auktionshaus', '', '', '', '', '', '', '', '', '', '', '', '', '');


DELETE FROM `locales_creature_text` WHERE (`entry`=86763 AND `groupid`=0) OR (`entry`=88181 AND `groupid`=0) OR (`entry`=85950 AND `groupid`=0) OR (`entry`=86136 AND `groupid`=0) OR (`entry`=87663 AND `groupid`=0) OR (`entry`=86413 AND `groupid`=0);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(86763, 0, 0, 'Äußerst merkwürdige Unfälle. Solide Höhlenwände zerbröckeln plötzlich und stürzen auf einen Gräber. Maschinen bekommen Fehlfunktionen und machen Arbeiter zu Kleinholz. Angeblich passiert so etwas häufiger, je tiefer sie graben.', '', '', '', '', ''),),
(88181, 0, 0, 'Ihr scheint besorgt zu sein, Myshal. Was plagt Euch?', '', '', '', '', ''),
(85950, 0, 0, 'Kommandant! Würdet Ihr meinen Laden unterstützen?', '', '', '', '', ''),
(86136, 0, 0, 'Frisches Brot! Frisches Brot zu verkaufen!', '', '', '', '', ''),
(87663, 0, 0, 'Oh ja? Wo habt Ihr das gefunden?', '', '', '', '', ''),
(86413, 0, 0, 'Wenn sie weiterhin unsere Soldaten so angreifen, dann könnte genau das passieren.', '', '', '', '', ''),;


DELETE FROM `creature_template_locale` WHERE (`entry`=86418 /*86418*/ AND `locale`='deDE') OR (`entry`=86424 /*86424*/ AND `locale`='deDE') OR (`entry`=85930 /*85930*/ AND `locale`='deDE') OR (`entry`=87049 /*87049*/ AND `locale`='deDE') OR (`entry`=85927 /*85927*/ AND `locale`='deDE') OR (`entry`=85916 /*85916*/ AND `locale`='deDE') OR (`entry`=85910 /*85910*/ AND `locale`='deDE') OR (`entry`=87052 /*87052*/ AND `locale`='deDE') OR (`entry`=89715 /*89715*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(86418, 'deDE', 'Wache des Sturmschilds', '', NULL, NULL, 23222), -- 86418
(86424, 'deDE', 'Navic Grünborke', '', NULL, NULL, 23222), -- 86424
(85930, 'deDE', 'Telys Rankenheiler', '', 'Erste Hilfe', NULL, 23222), -- 85930
(87049, 'deDE', 'Steven Cochrane', '', 'Schnittmuster', NULL, 23222), -- 87049
(85927, 'deDE', 'Manda Darlowe', '', 'Archäologielehrerin', NULL, 23222), -- 85927
(85916, 'deDE', 'Juwelenkonstrukteurin Nissea', '', 'Juwelierskunstlehrerin', NULL, 23222), -- 85916
(85910, 'deDE', 'Joshua Fuesting', '', 'Schneiderlehrer', NULL, 23222), -- 85910
(87052, 'deDE', 'Juwelenkonstrukteur Harlaan', '', 'Juwelenschliffdesigns', NULL, 23222), -- 87052
(89715, 'deDE', 'Franklin Martin', '', 'Transporter', NULL, 23222); -- 89715