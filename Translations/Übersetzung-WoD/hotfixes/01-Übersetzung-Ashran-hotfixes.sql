-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_0_23222
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/26/2017 00:03:39


SET NAMES 'utf8';
INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(91453, 'Wonach sucht ihr?', 'Wonach sucht ihr?', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23222);

DELETE FROM `broadcast_text_locale` WHERE (`ID`=91453 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(91453, 'deDE', 'Wonach sucht ihr?', 'Wonach sucht ihr?', 23222);