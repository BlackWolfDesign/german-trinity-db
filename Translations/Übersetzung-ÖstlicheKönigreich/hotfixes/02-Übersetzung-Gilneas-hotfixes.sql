-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/24/2017 16:36:17


SET NAMES 'utf8';
INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID0`, `EmoteID1`, `EmoteID2`, `EmoteDelay0`, `EmoteDelay1`, `EmoteDelay2`, `UnkEmoteID`, `Language`, `Type`, `SoundID0`, `SoundID1`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(50429, "Seid gegr��t, $n.", "Seid gegr��t, $n.", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(49789, "Unser Meister Malfurion ist aus dem Traum wiedergekehrt, $C. Er erstrahlt wie ein Leuchtfeuer, das uns den Weg weist, und wir m�ssen alles in unserer Macht Stehende tun, um dieses Feuer zu bewahren.", "Unser Meister Malfurion ist aus dem Traum wiedergekehrt, $C. Er erstrahlt wie ein Leuchtfeuer, das uns den Weg weist, und wir m�ssen alles in unserer Macht Stehende tun, um dieses Feuer zu bewahren.", 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(50711, "", "Nach allem, was wir durchgestanden haben, haben wir Gilneer nicht die Hoffnung aufgegeben.", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(24118, "", "Wir haben geschworen, stets unser Bestes zu geben, um die Fahrg�ste der Bravado zu besch�tzen. Die Seestra�e zwischen Rut'theran und dem Hafen von Sturmwind muss sicher bleiben.", 1, 0, 0, 0, 0, 0, 0, 7, 1, 0, 0, 0, 23420),
(8099, "Um unsere Verbindung zum Festland aufrechtzuerhalten, haben wir Hippogryphen, die regelm��ig zwischen Rut'theran und Dunkelk�ste hin und her fliegen.", "", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420);


DELETE FROM `broadcast_text_locale` WHERE (`ID`=50429 AND `locale`='deDE') OR (`ID`=49789 AND `locale`='deDE') OR (`ID`=50711 AND `locale`='deDE') OR (`ID`=24118 AND `locale`='deDE') OR (`ID`=8099 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(50429, 'deDE', "Seid gegr��t, $n.", "Seid gegr��t, $n.", 23420),
(49789, 'deDE', "Unser Meister Malfurion ist aus dem Traum wiedergekehrt, $C. Er erstrahlt wie ein Leuchtfeuer, das uns den Weg weist, und wir m�ssen alles in unserer Macht Stehende tun, um dieses Feuer zu bewahren.", "Unser Meister Malfurion ist aus dem Traum wiedergekehrt, $C. Er erstrahlt wie ein Leuchtfeuer, das uns den Weg weist, und wir m�ssen alles in unserer Macht Stehende tun, um dieses Feuer zu bewahren.", 23420),
(50711, 'deDE', "", "Nach allem, was wir durchgestanden haben, haben wir Gilneer nicht die Hoffnung aufgegeben.", 23420),
(24118, 'deDE', "", "Wir haben geschworen, stets unser Bestes zu geben, um die Fahrg�ste der Bravado zu besch�tzen. Die Seestra�e zwischen Rut'theran und dem Hafen von Sturmwind muss sicher bleiben.", 23420),
(8099, 'deDE', "Um unsere Verbindung zum Festland aufrechtzuerhalten, haben wir Hippogryphen, die regelm��ig zwischen Rut'theran und Dunkelk�ste hin und her fliegen.", "", 23420);

