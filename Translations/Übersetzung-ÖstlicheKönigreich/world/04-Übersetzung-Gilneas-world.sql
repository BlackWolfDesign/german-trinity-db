-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/24/2017 16:36:17


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=24602 AND `locale`='deDE') OR (`ID`=24678 AND `locale`='deDE') OR (`ID`=24920 AND `locale`='deDE') OR (`ID`=24903 AND `locale`='deDE') OR (`ID`=24902 AND `locale`='deDE') OR (`ID`=24904 AND `locale`='deDE') OR (`ID`=24676 AND `locale`='deDE') OR (`ID`=24674 AND `locale`='deDE') OR (`ID`=24575 AND `locale`='deDE') OR (`ID`=24675 AND `locale`='deDE') OR (`ID`=24677 AND `locale`='deDE') OR (`ID`=24592 AND `locale`='deDE') OR (`ID`=24672 AND `locale`='deDE') OR (`ID`=24673 AND `locale`='deDE') OR (`ID`=24593 AND `locale`='deDE') OR (`ID`=24646 AND `locale`='deDE') OR (`ID`=24628 AND `locale`='deDE') OR (`ID`=24627 AND `locale`='deDE') OR (`ID`=24617 AND `locale`='deDE') OR (`ID`=24616 AND `locale`='deDE') OR (`ID`=24578 AND `locale`='deDE') OR (`ID`=24501 AND `locale`='deDE') OR (`ID`=24495 AND `locale`='deDE') OR (`ID`=24484 AND `locale`='deDE') OR (`ID`=24483 AND `locale`='deDE') OR (`ID`=24472 AND `locale`='deDE') OR (`ID`=24468 AND `locale`='deDE') OR (`ID`=24438 AND `locale`='deDE') OR (`ID`=14467 AND `locale`='deDE') OR (`ID`=14466 AND `locale`='deDE') OR (`ID`=14465 AND `locale`='deDE') OR (`ID`=14402 AND `locale`='deDE') OR (`ID`=14401 AND `locale`='deDE') OR (`ID`=14400 AND `locale`='deDE') OR (`ID`=14399 AND `locale`='deDE') OR (`ID`=14412 AND `locale`='deDE') OR (`ID`=14404 AND `locale`='deDE') OR (`ID`=14416 AND `locale`='deDE') OR (`ID`=14406 AND `locale`='deDE') OR (`ID`=14403 AND `locale`='deDE') OR (`ID`=14398 AND `locale`='deDE') OR (`ID`=14397 AND `locale`='deDE') OR (`ID`=14395 AND `locale`='deDE') OR (`ID`=14396 AND `locale`='deDE') OR (`ID`=14386 AND `locale`='deDE') OR (`ID`=14368 AND `locale`='deDE') OR (`ID`=14382 AND `locale`='deDE') OR (`ID`=14369 AND `locale`='deDE') OR (`ID`=14367 AND `locale`='deDE') OR (`ID`=14366 AND `locale`='deDE') OR (`ID`=14348 AND `locale`='deDE') OR (`ID`=14347 AND `locale`='deDE') OR (`ID`=14336 AND `locale`='deDE') OR (`ID`=14321 AND `locale`='deDE') OR (`ID`=14320 AND `locale`='deDE') OR (`ID`=14313 AND `locale`='deDE') OR (`ID`=14375 AND `locale`='deDE') OR (`ID`=14222 AND `locale`='deDE') OR (`ID`=14221 AND `locale`='deDE') OR (`ID`=14218 AND `locale`='deDE') OR (`ID`=14212 AND `locale`='deDE') OR (`ID`=14294 AND `locale`='deDE') OR (`ID`=14293 AND `locale`='deDE') OR (`ID`=14214 AND `locale`='deDE') OR (`ID`=14204 AND `locale`='deDE') OR (`ID`=31889 AND `locale`='deDE') OR (`ID`=14159 AND `locale`='deDE') OR (`ID`=26129 AND `locale`='deDE') OR (`ID`=14154 AND `locale`='deDE') OR (`ID`=28850 AND `locale`='deDE') OR (`ID`=24930 AND `locale`='deDE') OR (`ID`=14157 AND `locale`='deDE') OR (`ID`=14288 AND `locale`='deDE') OR (`ID`=14277 AND `locale`='deDE') OR (`ID`=14094 AND `locale`='deDE') OR (`ID`=14098 AND `locale`='deDE') OR (`ID`=14093 AND `locale`='deDE') OR (`ID`=43301 AND `locale`='deDE') OR (`ID`=43283 AND `locale`='deDE') OR (`ID`=43300 AND `locale`='deDE') OR (`ID`=43297 AND `locale`='deDE') OR (`ID`=43285 AND `locale`='deDE') OR (`ID`=43291 AND `locale`='deDE') OR (`ID`=43298 AND `locale`='deDE') OR (`ID`=43286 AND `locale`='deDE') OR (`ID`=43296 AND `locale`='deDE') OR (`ID`=43292 AND `locale`='deDE') OR (`ID`=43284 AND `locale`='deDE') OR (`ID`=43299 AND `locale`='deDE') OR (`ID`=44911 AND `locale`='deDE') OR (`ID`=44910 AND `locale`='deDE') OR (`ID`=44909 AND `locale`='deDE') OR (`ID`=44908 AND `locale`='deDE') OR (`ID`=44891 AND `locale`='deDE') OR (`ID`=43303 AND `locale`='deDE') OR (`ID`=43179 AND `locale`='deDE') OR (`ID`=42422 AND `locale`='deDE') OR (`ID`=42421 AND `locale`='deDE') OR (`ID`=42420 AND `locale`='deDE') OR (`ID`=42234 AND `locale`='deDE') OR (`ID`=42233 AND `locale`='deDE') OR (`ID`=42170 AND `locale`='deDE') OR (`ID`=41761 AND `locale`='deDE') OR (`ID`=14091 AND `locale`='deDE') OR (`ID`=14078 AND `locale`='deDE') OR (`ID`=13518 AND `locale`='deDE') OR (`ID`=26385 AND `locale`='deDE') OR (`ID`=28517 AND `locale`='deDE') OR (`ID`=14434 AND `locale`='deDE') OR (`ID`=26706 AND `locale`='deDE') OR (`ID`=24681 AND `locale`='deDE') OR (`ID`=24680 AND `locale`='deDE') OR (`ID`=24679 AND `locale`='deDE') OR (`ID`=43242 AND `locale`='deDE') OR (`ID`=43245 AND `locale`='deDE') OR (`ID`=43287 AND `locale`='deDE') OR (`ID`=43288 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(24602, 'deDE', 'Zur Ruhe gebettet', 'Besorgt 5 freigelegte Andenken.', 'Bei unserer überstürzten Flucht haben wir die Ruhestätten unserer Vorfahren gestört, $n. Eine Todsünde sozusagen.$B$BEs ist Tradition, ein kleines Andenken in der flachen Erde auf dem Grab eines Einwohners von Gilneas zu hinterlassen. Als die Leute panisch aus diesen Tunneln geflohen sind, wurden diese Andenken freigelegt. Das hat die Geister der Verstorbenen erzürnt.$B$BFindet diese Andenken und bringt sie mir. Wir werden dafür sorgen, dass die Toten zu ihrem ewigen Schlaf zurückkehren.', '', '', '', '', '', '', 23420),
(24678, 'deDE', 'Knietief', 'Nehmt die halb abgebrannte Fackel, um an die andere Seite des Tunnels unter Gilneas zu gelangen.', 'Es ist Zeit, zusammenzupacken, $n. Die meisten unserer Leute haben es auf die andere Seite geschafft. Jetzt seid Ihr dran.$B$BNehmt diese Fackel, Ihr werdet sie noch brauchen. Im Tunnel wimmelt es nur so von widerlichen Kreaturen der übelsten Sorte, die Euch in die Knie beißen werden. Verwendet die Fackel, um sie Euch fern zu halten.$B$BSprecht auf der anderen Seite mit Krennan.', '', '', '', '', '', '', 23420),
(24920, 'deDE', 'Das Unvermeidliche verlangsamen', 'Benutzt die gefangene Reitfledermaus, um 6 Katapulte der Verlassenen und 40 einfallende Verlassene zu vernichten.', 'So gerne ich schnelle und brutale Rache üben würde… muss ich erst die Erinnerung an meinen Sohn ehren. Für ihn stand das Wohl seines Volkes immer an erster Stelle.$B$BDer Totengräberpass verläuft unterirdisch unter dem Nordtorfluss hindurch. Ihr müsst verhindern, dass die Seuche sich ausbreitet, wenn wir unsere Männer, Frauen und Kinder zu Aderics Ruhestätte evakuieren.$B$BWir haben eine der gefürchteten Bombenfledermäuse der Verlassenen gefangen. Ich möchte, dass Ihr diese Höllenbestie so gut wie möglich fliegt und denen, die die Seuche zu unserem Volk bringen wollen, die Hölle heiß macht. Beeilt Euch!', '', '', '', '', '', '', 23420),
(24903, 'deDE', 'Rache oder Überleben', 'Sprecht mit König Genn Graumähne auf dem Graumähnenhof in Gilneas.', 'Sagt Graumähne Bescheid, dass wir Sylvanas unsere Fährtenleser hinterhergeschickt haben und berichtet ihm von der Seuche.$B$BNun muss er entscheiden: Starten wir einen großangelegten Angriff auf die Verlassenen oder versuchen wir, unsere Überlebenden zu evakuieren?', '', '', '', '', '', '', 23420),
(24902, 'deDE', 'Jagd nach Sylvanas', 'Folgt Tobias Dunstmantel auf der Jagd nach Sylvanas.', 'Dann ist es also wahr? Gilneas hat seinen liebsten Sohn verloren.$B$BWir werden trauern, sobald all dies vorbei ist, aber keinen Tag früher.$B$BWir gönnen uns den Luxus der Trauer nicht, solange der Feind noch gilnearische Luft atmet.$B$BDie Männer meines Vaters sind bereit, zuzuschlagen, solange wir die Fährte der Bansheekönigin noch verfolgen können. Sagt einfach Bescheid, wenn Ihr bereit seid. Liam muss so schnell wie möglich gerächt werden.', 'Jagd nach Sylvanas', '', '', '', '', 'Kehrt zu Lorna Crowley auf dem Graumähnenhof in Gilneas zurück.', 23420),
(24904, 'deDE', 'Schlacht um Gilneas', 'Erringt den Sieg in der Schlacht um Gilneas.', 'Die Zeit ist gekommen, $n. Wir haben jeden einzelnen Gilneer zusammengetrommelt und ihm eine Waffe in die Hand gedrückt.$B$BEs ist Zeit, dass wir uns dem Feind direkt stellen. Zeit, Sylvanas und ihren verlassenen Hunden ein für alle Mal eine Lektion zu erteilen.$B$BLiam führt den Angriff von diesem Tor aus an. König Graumähne und mein Vater überwachen die Angriffe in den anderen Vierteln.$B$BAuch Ihr spielt bei der Sache eine wichtige Rolle. Nehmt dieses Rapier. Verwendet es, um unsere Männer und Frauen in der Schlacht anzufeuern. Sprecht mit Krennan Aranas, um an der Schlacht um Gilneas teilzunehmen.', '', '', '', '', '', '', 23420),
(24676, 'deDE', 'Vertreibt sie!', 'Tötet 4 Infanteristen der Verlassenen, Exekutor Cornell und Valnov den Verrückten in Glutstein.', 'Unsere Leute sind frei, aber die Stadt wird immer noch von den Verlassenen belagert. Sie sind von ihren Kerntruppen abgeschnitten. Wenn wir sie also vertreiben wollen, ist jetzt die beste Gelegenheit.$B$BIhr Anführer, Exekutor Cornell, ist für seine grenzenlose Rücksichtslosigkeit bekannt. Unter seinen Männern befindet sich auch Valnov der Verrückte, ein so genannter Wissenschaftler, der grausame Experimente an Dorfbewohnern durchführt, die zu alt sind, um in den Minen zu arbeiten.$B$BVernichtet sie alle.', '', '', '', '', '', '', 23420),
(24674, 'deDE', 'Niemandes Sklaven', 'Tötet Brothogg den Sklavenmeister in der Glutsteinmine.', 'Ich war einer der wenigen, die entkommen sind, $n. Nachdem die Verlassenen unser Dorf und unsere Mine eingenommen hatten, haben sie uns mit höchster Grausamkeit behandelt.$B$BDiejenigen von uns, die schwach waren und keine schweren Arbeiten aushielten, ließ dieses abscheuliche Wesen namens Brothogg bis in den Tod schuften.$B$BMeine Frau war eines seiner Opfer, $C, und ich habe geschworen, dass ich für ihren Tod Rache nehme. Ich bin jetzt zu schwach, aber Ihr könnt für Gerechtigkeit sorgen. Für meine Frau... für Gilneas!', '', 'Erledigt... diese... MONSTROSITÄT.', 'Brothogg der Sklavenmeister', '', '', '', 23420),
(24575, 'deDE', 'Befreiung', 'Befreit 5 versklavte Dorfbewohner in der Glutsteinmine.', 'Wir haben kürzlich herausgefunden, dass die Dorfbewohner in Glutstein die schlimmsten Verheerungen des Fluchs überlebt haben.$B$BLeider hatte ihr Dorf beim Einmarsch der Verlassenen nicht so viel Glück.$B$BDie Verlassenen haben die Dorfbewohner gefangen genommen und zwingen sie, in der nahegelegenen Mine als Sklaven zu arbeiten.', '', '', '', '', '', '', 23420),
(24675, 'deDE', 'Die letzte Mahlzeit', 'Besorgt 10 Stück Hirschfleisch.', 'Ich bin nicht dumm, $n. Ich weiß, was uns erwartet.$B$BUnser Volk wird diese Tore durchschreiten und gegen einen Feind kämpfen, der besser ausgebildet ist, zahlreicher ist und bessere Waffen als wir hat.$B$BSelbst wenn wir Erfolg haben, werden wir bitter dafür bezahlen müssen. Für die meisten von uns wird das Essen heute Abend die letzte Mahlzeit sein.$B$BIch kann nicht kämpfen, aber ich weiß, wie man kocht. Bringt mir Fleisch von den Hirschen in der Gegend. Ich werde sicherstellen, dass dieses letzte Mahl das Beste wird, was unsere Leute je gegessen haben.', '', '', '', '', '', '', 23420),
(24677, 'deDE', 'Flankiert die Verlassenen', 'Sprecht mit Lord Hewell, um Euch ein Pferd zur Pferdewechselstation zu organisieren, und meldet Euch bei Lorna Crowley.', 'Ich danke Euch, $n. Wir können uns jedoch nicht auf unseren Lorbeeren ausruhen.$B$BWir haben von einem Bergbaudorf erfahren, das derzeit von den Verlassenen besetzt wird. Einige unserer Männer sind zusammen mit Crowleys Tochter zu den nahegelegenen Pferdeställen aufgebrochen, um von dort aus das Kommando zu übernehmen. Liam sollte in Kürze zu ihnen stoßen.$B$BIch bleibe hier und organisiere die letzte Angriffswelle in Gilneas. Sprecht mit Lord Hewell, um Euch ein Pferd zu besorgen, das Euch dorthin bringen kann.', '', '', '', '', '', '', 23420),
(24592, 'deDE', 'Verrat an der Witterfront', 'Trinkt Krennans Tarnungstrank an der Witterfront und tötet Baron Ashbury und Lord Walden.', 'Lord Godfrey hat König Graumähne an der Witterfront gefangen genommen!$B$BEr konnte die beiden Fürsten aus dem Osten überzeugen, sich seinem Plan anzuschließen. Der Narr scheint zu denken, dass er eine Vereinbarung mit den Verlassenen treffen kann, wenn er ihnen Graumähne überlässt.$B$BHört genau zu, $n. Wir müssen diese Angelegenheit mit so wenig Blutvergießen wie möglich klären.$B$BMit diesem Trank könnt Ihr Euch unerkannt hineinschleichen. Ermordet die verräterischen Fürsten, bevor die Dinge vollkommen außer Kontrolle geraten!', '', '', '', '', '', '', 23420),
(24672, 'deDE', 'Vorwärts und hoch hinaus', 'Sucht Krennan Aranas auf der Straße zur Witterfront.', 'Ich werde das tun, worum Darius gebeten hat, und unsere Leute durch den Schwarzforst führen.$B$BIhr solltet zu König Graumähne aufschließen. Er und Krennan sind zur Witterfront aufgebrochen, nachdem Lord Godfrey bestätigt hatte, dass dort ein paar Überlebende ihr Lager aufgeschlagen haben.$B$BFolgt der Straße aus Sturmsiel in die Berge. Ich habe Krennan gebeten, an der Brücke vor der Witterfront auf Euch zu warten.', '', '', '', '', '', '', 23420),
(24673, 'deDE', 'Rückkehr nach Sturmsiel', 'Sprecht mit Gwen Armstead in Sturmsiel.', 'Es wird lange dauern, bis unsere Nation wieder zusammenfindet. Die früheren politischen Streitereien verblassen im Gegensatz dazu, was uns heute entzweit.$B$BDoch wenn es je eine Zeit gegeben hat, da wir uns zusammentun und gegen einen gemeinsamen Feind verbünden müssen, dann jetzt. Und Ihr, $n, seid es, $gder:die; sich als fähig erwiesen hat, diese Lücke zwischen unseren Leuten zu schließen.$B$BKehrt nach Sturmsiel zurück und sagt ihnen, dass der Weg nach Witterfront durch den Schwarzforst sicher ist. Wir kümmern uns um die Verlassenen und folgen ihnen.', '', '', '', '', '', '', 23420),
(24593, 'deDE', 'Weder Mensch noch Tier', 'Trinkt vom Brunnen der Ruhe, dem Brunnen des Gleichgewichts und dem Brunnen des Zorns.', 'Es ist Zeit, $n. Diese… Alchemie, die die Bestie in Euch zügelt, hält nicht mehr lange an.$B$BDie ersten verfluchten Worgen waren Nachtelfdruiden, genau wie unsere neuen Verbündeten. Sie haben dem Gleichgewicht, das der Rest ihres Volkes ehrt, den Rücken gekehrt und zugelassen, dass die Bestie in ihrem Inneren die Kontrolle über ihren Verstand übernimmt.$B$BDaher ist es nur Recht, dass ihre Brüder und Schwestern uns von diesem Fluch befreien werden. Geht, $C. Trinkt von den Wassern des Tal''doren und schließt Frieden mit Euch selbst.', '', '', '', '', '', '', 23420),
(24646, 'deDE', 'Holt zurück, was uns gehört', 'Blast an der Waldrandhütte in das Horn von Tal''doren und beschafft Euch das mysteriöse Artefakt aus ihrem Inneren.', 'Seit die Verlassenen in Gilneas einmarschiert sind, haben wir uns bemüht, den Aufbewahrungsort der Sichel von Elune geheim zu halten. Wir haben sie im Schutz der Nacht an einen von vielen sicheren Orten gebracht. Die dunklen Waldläufer sind ihr gefährlich nahe gekommen, haben sie aber nie gefunden... bis jetzt.$B$BZuletzt wurden sie an der Waldrandhütte westlich von hier gesichtet. Stoßt in dieses Horn, wenn Ihr sie gefunden habt. Tobias und seine Fährtenleser werden Euch zu Hilfe eilen und die dunklen Waldläufer bekämpfen.$B$BNur dann könnt Ihr Euch in die Hütte schleichen und die Sichel zurückholen. Schnell, $n. Sylvanas darf sie nicht in die Finger bekommen.', '', '', '', '', '', '', 23420),
(24628, 'deDE', 'Vorbereitungen', 'Bringt 6 Mondblätter zu Vassandra.', 'Was Ihr als Fluch kennt begann als Ketzerei unter unseren Brüdern.$B$BIm Angesicht der sicheren Niederlage gegen dämonische Mächte haben die Druiden des Rudels dem Gleichgewicht, dem sie sich verschworen hatten, den Rücken gekehrt und zugelassen, dass das Tier in ihnen die Kontrolle übernimmt.$B$BSie wurden in den Smaragdgrünen Traum verbannt, wo sie unter einem Baum wie diesem in ewigen Schlaf versinken mussten. Doch ihre Seuche hatte sich schon auf andere ausgebreitet.$B$BHeute stellen wir in unserer Zeremonie das Gleichgewicht in Eurem Volk wieder her. Bringt mir das Blatt, das nur in der Nähe des Tal''doren wächst.', '', '', '', '', '', '', 23420),
(24627, 'deDE', 'Direkt vor unserer Tür', 'Tötet 6 heulende Banshees.', 'Die Nachtelfen haben uns in dieser Zeit viel beigebracht, $n. Sie lehrten uns, wer wir sind und wo wir herkommen.$B$BDank ihnen… dank dieses Orts… sind wir keine wilden Tiere mehr. Wir haben unseren freien Willen zurückgewonnen.$B$BDie Verlassenen haben es nicht nur auf unser Land abgesehen.$B$BSie suchen nach etwas, das uns gehört. Ein Artefakt, mit dem sie den Fluch unkontrollierbar auf die gesamte Menschheit ausbreiten können. Wir dürfen nicht zulassen, dass sie es finden.$B$BSylvanas'' Banshees rücken immer näher. Helft meinen Spurenlesern, sie zu beseitigen.', '', '', '', '', '', '', 23420),
(24617, 'deDE', 'Tal''doren, die Heimstatt in der Wildnis', 'Sprecht mit Darius Crowley beim Tal''doren im Herzen des Schwarzforsts.', 'Es gibt andere wie Euch, $n. Und auch sie waren verwirrt und wussten nicht, wohin.$B$BEs gibt einen Ort, an den Ihr gehört. Tal''doren, die Heimstatt in der Wildnis, war einst das Zuhause eines Druidenordens, der die Form von Wölfen annehmen konnte.$B$BDort findet Ihr die Antworten, die Ihr sucht. Und dort wird Euch jemand helfen können. Ein alter Freund.', '', '', '', '', '', '', 23420),
(24616, 'deDE', 'Verfolger abschütteln', 'Verwendet Belysras Talisman, um die dunkle Späherin zu besiegen.', 'Ihr müsst so viele Fragen haben, $n. Und Ihr werdet auch bald Antworten darauf erhalten.$B$BIch weiß, warum Ihr hier seid und wonach Ihr sucht.$B$BIch kann Euch nicht dahin führen, solange der Feind Euch folgt.$B$BEine von Sylvanas Späherinnen ist Euch auf den Fersen. Ihr dürft den Verlassenen auf keinen Fall den Ort zeigen, den Ihr sucht. Nehmt diesen Talisman und geht einfach die Straße direkt nördlich von hier entlang.$B$BLasst zu, dass die Späherin Euch in ihre Falle lockt, und nutzt dann die Macht des Talismans, um Euch zu befreien und einen Gegenangriff zu starten.', '', '', '', '', '', '', 23420),
(24578, 'deDE', 'Der Schwarzforst', 'Untersucht die Bradshawmühle direkt außerhalb von Sturmsiel.', 'Es hat ein wenig gedauert, aber ich glaube, ich habe endlich herausgefunden, was hier passiert ist. Zumindest teilweise.$B$BIn dieser Stadt gab es einige Überlebende der Seuche. Sie haben sich alle den anderen in den Bergen angeschlossen, an der Witterfront.$B$BAlle sind geflohen, bis auf den Mann, der dieses Tagebuch geführt hat, der alte Bradshaw. Er bemerkte, dass die Angriffe der Worgen plötzlich aufgehört hatten, und entschloss sich, in den Schwarzforst zu reisen.$B$BDirekt nordöstlich der Stadt gibt es eine verlassene Mühle. Dort lebte Bradshaw früher. Schaut Euch da nach Hinweisen um.', '', '', '', '', '', '', 23420),
(24501, 'deDE', 'Spinne am Morgen bringt Kummer und Sorgen', 'Tötet Rygna in Rygnas Hort.', 'Die Flut der Spinnen nimmt kein Ende, $n. Ihr müsst in ihr Nest gehen und die Brutmutter töten.$B$BWir haben die riesige Spinne namens Rygna zuletzt am Rand des Schwarzforstes gesehen, in einer Gegend, die ganz mit dicken Netzen bedeckt war. Es wird gefährlich, aber wir verlassen uns auf Euch.', '', 'Findet sie in dieser Höhle und kommt lebend zurück. Wir zählen auf Euch.', 'Rygna', '', '', '', 23420),
(24495, 'deDE', 'Fetzen der Vergangenheit', 'Sammelt 6 alte Tagebuchseiten.', 'Ich habe das Tagebuch eines Mannes namens Bradshaw gefunden. Leider fehlen viele Seiten, besonders gegen Ende.$B$BWenn Ihr genügend Seiten zusammentragt, kann ich vielleicht herausfinden, was nach dem Ausbruch des Fluchs hier geschehen ist.$B$BWer weiß… Vielleicht haben die Leute in diesem Dorf einen Fluchtweg gefunden. Vielleicht gibt es da draußen noch Überlebende.$B$BVielleicht ist mein Vater noch am Leben.', '', '', '', '', '', '', 23420),
(24484, 'deDE', 'Schädlingsbekämpfung', 'Tötet 6 Giftbruthuscher.', 'Was sagt Ihr dazu, $n? Die ganze Zeit über war Sturmsiel halbwegs intakt.$B$BSchade, dass die Verlassenen noch immer unterwegs ins Landesinnere sind, aber immerhin ist es ein guter Platz, um für ein oder zwei Nächte die Füße hochzulegen.$B$BDie andere Hälfte der Stadt ist voll von Spinnen, deswegen habe ich den Leuten gesagt, sie sollen sich von dort fernhalten.$B$BVielleicht könntet Ihr ja helfen, die Spinnen zu beseitigen? Unsere Verwundeten können bestimmt besser schlafen, wenn sie wissen, dass jemand diese achtbeinigen Ungeheuer in Schach hält.', '', '', '', '', '', '', 23420),
(24483, 'deDE', 'Sturmsiel', 'Sprecht mit Gwen Armstead in Sturmsiel.', 'Unsere Kutschenfahrer wurden angewiesen, sich im Fischerdorf Sturmsiel zu sammeln. Die Späher berichten, dass wir dort Schutz und Vorräte finden können.$B$BTrefft Euch dort mit Gwen Armstead. Sie ist für die Leute verantwortlich.', '', '', '', '', '', '', 23420),
(24472, 'deDE', 'Vorstellen ist angebracht', 'Tötet 4 Ogerdiener und besorgt Euch Koroths Banner in Koroths Bau.', 'Die Verlassenen sind uns dicht auf den Fersen und nun versuchen auch diese Oger, uns in die Zange zu nehmen, $n.$B$BSie sind Diener des Ettins Koroth, dem bösartigsten und zerstörerischsten Wesen in ganz Gilneas.$B$BWenn wir Koroth dazu bringen könnten, die Vorhut der Verlassenen anzugreifen, können wir uns vielleicht sicher zurückziehen und etwas Zeit gewinnen.$B$BAber das gelingt uns nur, wenn wir Koroth wütend machen. Seine Diener zu töten reicht nicht. Wir müssen uns seinen wertvollsten Besitz schnappen, sein Banner aus Tigerfell.', '', '', '', '', '', '', 23420),
(24468, 'deDE', 'Gestrandet im Moor', 'Rettet 5 Unfallüberlebende aus dem Hagelwaldmoor.', 'Die Oger haben zwei unserer Postkutschen erwischt: diese hier und eine, die nicht weit von hier im Norden ins Moor gestürzt ist.$B$BGeht dorthin und rettet die Überlebenden, während wir uns überlegen, wie wir mit diesen Ogern fertigwerden.', '', '', '', '', '', '', 23420),
(24438, 'deDE', 'Exodus', 'Geht an Bord eines Karrens unterhalb des Graumähnenanwesens.', 'Wir haben keine Wahl, $n. Wir müssen weiter ins Landesinnere.$B$BEs gefällt mir zwar überhaupt nicht, aber hier sind wir verwundbar.$B$BKehrt zu den Ställen unterhalb des Anwesens zurück. Lasst uns alle Mann auf Karren verfrachten und ostwärts schicken. Ich hoffe nur, es ist noch nicht zu spät.', '', '', '', '', '', '', 23420),
(14467, 'deDE', 'Oh weh, Gilneas!', '', '', '', '', '', '', '', '', 23420),
(14466, 'deDE', 'Das Observatorium des Königs', 'Sprecht mit König Genn Graumähne im Observatorium des Graumähnenanwesens.', 'Ihr solltet mit Genn sprechen. Seit dieses Martyrium begonnen hat, hat er sich in seinem Observatorium eingeschlossen.$B$bIch bin mir sicher, dass er an einem Plan für sein Volk arbeitet. Er ist ein sehr eigensinniger Zeitgenosse.', '', '', '', '', '', '', 23420),
(14465, 'deDE', 'Zum Graumähnenanwesen', 'Sprecht mit Königin Mia Graumähne im Graumähnenanwesen.', 'Es ist an der Zeit, alle in Sicherheit zu bringen. Das gilt auch für Euch, $n. Begebt Euch zum Graumähnenanwesen. Die anderen werden Euch in Kürze folgen.$B$BMacht Euch um mich keine Sorgen. Ich kümmere mich darum, dass alle heil hier rauskommen. Ja, sogar die paar Wilden, die wir in den Ställen eingesperrt haben.$B$BSobald Ihr dort angekommen seid, sprecht mit Königin Graumähne über die Dringlichkeit der Lage.', '', '', '', '', '', '', 23420),
(14402, 'deDE', 'Abmarschbereit', 'Sprecht mit Gwen Armstead in Dämmerhafen.', 'Ich schätze, jetzt aufzubrechen ist genauso gut wie zu jedem anderen Zeitpunkt.$B$bIch habe meine schönste Kleidung, mein Lieblingsbuch und meine geliebte Katze bei mir.$B$BIch hoffe, ich habe nichts vergessen.$B$BSagt dieser Bürgermeisterin, dass ich auf dem Weg bin. Dann sehe ich Euch wohl zusammen mit den anderen in der Stadt!', '', '', '', '', '', '', 23420),
(14401, 'deDE', 'Omas Katze', 'Holt Felix, die Katze.', 'Genau! Was die Evakuierung angeht, Liebes.$B$BIch kann nicht ohne meine Katze gehen. Er ist ein zuckersüßer orangefarbener Tigerkater.$B$BFelix spielt am liebsten in der Nähe eines alten Karrens in der Nähe einer Laube nordöstlich von hier.$B$BBringt ihn zu mir, dann können wir sofort aufbrechen.', '', '', '', '', '', '', 23420),
(14400, 'deDE', 'So gehe ich nicht außer Haus', 'Besorgt Omas gute Kleider.', 'Was wolltet Ihr nochmal?$B$BOh, stimmt ja. Der Evakuierungsbefehl.$B$BAber so kann ich ja schlecht unter die Leute gehen, oder?$B$BSeid so lieb und holt mir meine guten Kleider. Sie hängen draußen auf der Wäscheleine.$B$B<Ihr versucht vergeblich, Oma Wahl davon zu überzeugen, dass ihre jetzige Kleidung völlig angemessen ist.>$B$BIhr jungen Leute habt einfach keine Manieren. Wurdet Ihr von Ogern großgezogen? Und jetzt geht und bringt mir meine Kleider, Liebes.', '', '', '', '', '', '', 23420),
(14399, 'deDE', 'Oma hat sie nicht mehr alle', 'Findet das leinengebundene Buch.', 'Gehen? Jetzt?$B$BNun, was sein muss, muss wohl sein. Aber wärt Ihr ein Engel und würdet etwas für mich suchen?$B$BIch habe mein Lieblingsbuch verlegt und werde ganz sicher nicht ohne es gehen.$B$B<Nachdem Ihr Oma die Dringlichkeit der Lage erläutert habt, schüttelt sie herablassend den Kopf.>$B$BLiebes, sucht einfach das Buch für mich, ja?', '', '', '', '', '', '', 23420),
(14412, 'deDE', 'Angespült', 'Tötet 6 schiffbrüchige Verlassene.', 'Die Schiffe der Verlassenen sind viel zu nahe an unserer Fischerei gesunken. All die Überlebenden machen sich auf den Weg hierher. Wie Ihr sehen könnt, sind wir zahlenmäßig unterlegen.$B$BHelft uns, sie zurückzuhalten, während ich die Boote fertig repariere.', '', '', '', '', '', '', 23420),
(14404, 'deDE', 'Nicht alles im Lot auf''m Boot', 'Besorgt Holzplanken, Kohlenteer und Schiffbauerwerkzeuge.', 'Evakuierung, sagt Ihr? Nun, nachdem die Verlassenen hier aufgetaucht sind, kam uns diese Idee von ganz alleine.$B$BWir dachten, zur See wäre der sicherste Fluchtweg.$B$BLeider haben unsere Boote das letzte Erdbeben nicht unbeschadet überstanden.$B$BIch benötige noch ein wenig Material, um meine Reparaturen abzuschließen: Holz, Teer und meine Werkzeuge. Besorgt sie mir, damit ich und meine Brüder von hier entkommen können.', '', '', '', '', '', '', 23420),
(14416, 'deDE', 'Der hungrige Ettin', 'Bringt 5 Gebirgspferde zu Lorna Crowley bei Crowleys Obstgarten.', 'Hört zu, $n. Wir werden es ohne ein Transportmittel nicht weit schaffen, nicht wahr?$B$BWenn wir uns hier einfach nur so aus dem Staub machen, werden die Verlassenen uns im Handumdrehen einholen.$B$BNormalerweise wäre es überhaupt kein Problem, ein paar Pferde in den Stallungen im Südosten aufzutreiben, aber da gibt es einen Haken.$B$BDas letzte Erdbeben muss Koroth den Ettin aus den Bergen herunter zu uns vertrieben haben. Wenn wir uns die Pferde nicht bald schnappen, fürchte ich, dass er uns zuvorkommt.', '', '', '', '', '', '', 23420),
(14406, 'deDE', 'Crowleys Obstgarten', 'Sprecht mit Lorna Crowley bei Crowleys Obstgarten.', 'Ihr erinnert Euch doch sicher an Lord Darius Crowleys Tochter Lorna.$B$BSie hat sich nie ganz vom Verlust ihres Vaters in Gilneas erholt. Seitdem fristet sie ihr Leben als Einsiedlerin. Der einzige Besucher, den sie je in ihr Haus gelassen hat, war Krennan... und er verliert kein Wort über die ganze Sache.$B$BBenachrichtigt sie über die Evakuierung.', '', '', '', '', '', '', 23420),
(14403, 'deDE', 'Die Brüder Hayward', 'Sprecht mit Sebastian Hayward in der Fischerei Hayward.', 'Weit im Süden findet Ihr die Fischerei Hayward. Die Brüder Hayward führen sie schon seit vielen Jahren.$B$BSie kommen nicht oft in die Stadt, nur um ihre Fische zu verkaufen. Gebt ihnen Bescheid, dass die Evakuierung eingeleitet wurde.', '', '', '', '', '', '', 23420),
(14398, 'deDE', 'Oma Wahl', 'Findet Oma Wahl in Wahls Landhaus.', 'Es wird nicht leicht, Oma Wahl dazu zu bewegen, mit uns zu kommen. Ihr müsst dennoch alles in Eurer Macht Stehende tun, um sie zu überzeugen.$B$BIhr findet sie in Wahls Landhaus. Bitte, $n, seid geduldig mit ihr. Sie ist schon sehr alt und ihr Geist wird immer schwächer.', '', '', '', '', '', '', 23420),
(14397, 'deDE', 'Evakuierung', 'Sprecht mit Gwen Armstead in Dämmerhafen.', 'Wir sind dem Tod nur durch schieres Glück entkommen, $n. Aber unser Glück ist langsam erschöpft.$B$BNoch während wir hier sprechen wird die Küste überflutet. Und auch die Landmasse unter uns scheint sich noch nicht beruhigt zu haben.$B$BIch sage es nicht gerne, aber wir müssen hier weg. Verdammt, $n... Ich darf mein Heimatland nicht zweimal in meinem Leben verlieren. Aber diese Erdbeben sind kein Feind, den wir besiegen können.$B$BSagt Gwen Armstead, dass sie die Evakuierung einleiten soll.', '', '', '', '', '', '', 23420),
(14395, 'deDE', 'Atemlos', 'Rettet 4 ertrinkende Wachleute.', '$n! Ich war gerade unterwegs, als es passiert ist.$B$BWir haben keine Zeit zu verlieren. Gut die Hälfte der Wachleute der Stadt hat sich in dem Bereich aufgehalten, der eingestürzt ist. Geht dorthin und rettet so viele, wie Ihr könnt.$B$BZieht sie aus dem Wasser ans Ufer. Ich kümmere mich darum, dass sie die benötigte Hilfe bekommen.', '', '', '', '', '', '', 23420),
(14396, 'deDE', 'Als das Land zerbarst', 'Sprecht mit Prinz Liam Graumähne auf Allens Bauernhof.', 'Was in aller Welt war das, $n?$B$BDas waren keine Kanonen oder Katapulte, so viel steht fest.$B$BGeht nach draußen und seht nach, was los ist.', '', '', '', '', '', '', 23420),
(14386, 'deDE', 'Der Leithund', 'Benutzt die Mastiffpfeife, um Hilfe dabei zu bekommen, die dunkle Waldläuferin Thyala zu töten.', 'Ausgezeichnete Arbeit bisher, $n. Unsere Späher haben die Anführerin des Bodenangriffs der Verlassenen ermittelt.$B$BEine von Sylvanas persönlich ausgewählte dunkle Waldläuferin kontrolliert das Schlachtgeschehen vom Haus der Waldens bei der Küste aus. Nehmt diese Pfeife mit und benutzt sie, wenn Ihr in ihrer Nähe seid.$B$BIch befehle meinen Männern dann, unsere Angriffsmastiffs loszulassen, um sie zur Strecke zu bringen. Seid vorsichtig, $n. Ihr solltet ihr nicht allein gegenübertreten.', '', 'Vergesst die Pfeife nicht!', 'Dunkle Waldläuferin Thyala', '', '', '', 23420),
(14368, 'deDE', 'Rettet die Kinder!', 'Rettet Cynthia, Ashley und James in Hammonds Bauernhof.', 'Die Soldaten lassen mich nicht aus dem Keller, aber meine Kinder sind noch auf dem Bauernhof! Bitte, Ihr müsst sie finden.$B$BDie Verlassenen haben kein Mitgefühl mit den Unschuldigen. Meine Kinder sind in großer Gefahr!$B$BSie heißen Cynthia, Ashley und James. Wenn Ihr sie findet, bringt sie hierher. Ich stünde auf ewig in Eurer Schuld.', '', '', '', '', '', '', 23420),
(14382, 'deDE', 'Nimm mich mit, Kapitän, auf die Reise', 'Benutzt die Katapulte der Verlassenen, um auf die Schiffe zu gelangen und tötet Kapitän Morris und Kapitän Anson.', 'Die Katapulte der Verlassenen machen uns ganz schön zu schaffen, $n. Und selbst wenn wir ihrer Herr würden, stehen da noch zwei Schiffe mit zwei Reihen Kanonen.$B$BIch habe jedoch einen Plan, mit dem wir zwei Fliegen mit einer Klappe schlagen. Ihr seid bei diesem Plan die Klappe.$B$BBeseitigt die Maschinisten an den Katapulten. Nutzt danach die Katapulte, um Euch selbst an Bord der Schiffe der Verlassenen zu katapultieren.$B$BWenn Ihr nicht gerade komplett daneben zielt, solltet Ihr sicher landen. Sobald Ihr an Bord seid, geht unter Deck und erledigt den Kapitän jedes Schiffes. Viel Erfolg, $C.', '', '', '', '', '', '', 23420),
(14369, 'deDE', 'Entfesselt die Bestie', 'Tötet 8 Fußsoldaten der Verlassenen oder Seeleute der Verlassenen.', 'Ich werde Euch nicht belügen, $n. Der Gedanke, einen Worgen in unseren Reihen zu haben, gefällt mir ganz und gar nicht.$B$BWer weiß schon, wie lange Krennans Gebräu anhält und wann Ihr versuchen werdet, einem von uns den Kopf abzubeißen?$B$BAber bis dahin können wir uns Eure Wildheit zunutze machen.$B$BGeht hinaus auf das Schlachtfeld und tötet so viele dieser Verlassenen, wie Ihr könnt, und zwar so brutal, wie Ihr könnt.$B$BWir werden ihnen schon beibringen, die Worgen zu fürchten, $n.', '', '', '', '', '', '', 23420),
(14367, 'deDE', 'Allens Sturmkeller', 'Sprecht mit Lord Godfrey im Sturmkeller von Allens Bauernhof in Gilneas.', 'Lord Godfrey führt den Angriff auf die Verlassenen in der Nähe ihres Anlegeplatzes an. Schaut, wie Ihr ihm dabei helfen könnt, die Verlassenen zurückzuhalten.$B$BIch habe gehört, dass Godfrey den Sturmkeller in Allens Bauernhof weit im Westen unter sein Kommando gebracht hat. Sucht ihn dort.', '', '', '', '', '', '', 23420),
(14366, 'deDE', 'Durchhalten', 'Sprecht mit Gwen Armstead in Dämmerhafen.', 'Ab hier schaffen wir es alleine, $n. Schaut Ihr nach Bürgermeisterin Armstead.$B$BDies hier ist nur ein kleiner Teil der Streitkräfte der Verlassenen. Wenn wir nicht schnell handeln, werden wir bald noch größeren Problemen gegenüberstehen.$B$BSchaut, was Ihr tun könnt, um Euch der Armee der Verlassenen an ihrem Anlegeplatz zu stellen.', '', '', '', '', '', '', 23420),
(14348, 'deDE', 'Ihr schafft sie nicht alleine', 'Jagt mit den Schwarzpulverfässern 4 entsetzliche Monstrositäten in die Luft.', 'Schaut her, $n. Die besonders großen, massigen, auch Monstrositäten genannt, sind nicht so leicht zu bezwingen. Versucht also nicht, alleine mit ihnen fertigzuwerden.$B$BAber ich habe eine Idee, die so verrückt ist, dass sie tatsächlich funktionieren könnte. Sie könnte allerdings auch Euren Tod bedeuten, wenn wir nicht vorsichtig genug sind.$B$BHinter den Schuppen und in der Nähe der Windmühlen liegen einige Fässer mit Sprengpulver. Schnappt sie Euch und werft sie den Monstrositäten direkt an den Kopf. Den Rest erledige ich mit meiner treuen alten Donnerbüchse.', '', '', '', '', '', '', 23420),
(14347, 'deDE', 'Bleibt standhaft', 'Tötet 10 Eindringlinge der Verlassenen.', '$n! Ich bin mir nicht sicher, ob Ihr lebendig oder tot seid… Mensch oder Worgen…$B$BIch bin mir nicht einmal sicher, ob ich wache oder träume. Aber EINER Sache bin ich sicher…$B$BWir werden einen ganzen Haufen dieser mutterlosen Verlassenen töten.', '', '', '', '', '', '', 23420),
(14336, 'deDE', 'Töten oder getötet werden', 'Sprecht mit Prinz Liam Graumähne außerhalb von Dämmerhafen.', 'Die Riffe haben uns bisher immer vor einem Angriff vom Wasser aus bewahrt, doch die Erdbeben scheinen einen Weg für die Schiffe der Verlassenen geöffnet zu haben.$B$BSchnell, $n. Verlangsamt die Angriffe, während ich den Rest der Miliz bereit mache.$B$BIch glaube, ich höre den Prinz und seine Männer schon gegen die Verlassenen kämpfen. Sprecht mit Liam und schaut, wo Ihr helfen könnt.$B$BOh... und achtet darauf, dass er nicht in den sicheren Tod rennt. In seinem derzeitigen Zustand ist er leider etwas tollkühn.', '', '', '', '', '', '', 23420),
(14321, 'deDE', 'Invasion', 'Sprecht mit Gwen Armstead in Dämmerhafen.', '<Ein langes Messer, dessen Griff mit einem Schädel verziert ist, ragt aus dem Rücken des Milizionärs. Als Ihr Euch nach Hinweisen umseht, entdeckt Ihr ein paar wild aussehende Kriegsschiffe an der Küste.>$B$B<Jemand fällt in Gilneas ein. Berichtet sofort der Bürgermeisterin von Dämmerhafen, Gwen Armstead, davon.>', '', '', '', '', '', '', 23420),
(14320, 'deDE', 'Benötigte Zutaten', 'Findet die Kiste mit Alraunenessenz.', 'Ah, ja. Soweit wir wissen, gibt es kein Heilmittel gegen die Folgen des Fluchs.$B$BMit der richtigen Behandlung können wir jedoch dafür sorgen, dass Euer Verstand der Eurige bleibt, nicht der eines wilden Tiers.$B$BWir haben Glück, dass die Behandlung bei Euch angeschlagen hat. In der Regel lassen sich nur kürzlich erfolgte Infektionen behandeln.$B$BWenn wir eine Hoffnung haben wollen, den Fluch jemals rückgängig zu machen, müsst Ihr weiter Eure Medizin einnehmen.$B$BFür weitere Dosen meines Serums benötige ich Alraunenessenz. Unter einer Hütte südwestlich von hier findet Ihr eine Kiste mit dieser Essenz.', '', '', '', '', '', '', 23420),
(14313, 'deDE', 'Wieder unter Menschen', 'Sprecht mit Krennan Aranas in Dämmerhafen.', 'Krennans Trank hat Euch also nicht umgebracht? Nun, das heißt wohl, dass der Mensch in Euch die Überhand hat.$B$BUnter diesen Umständen werde ich Euch doch nicht erschießen. Zumindest noch nicht.$B$BSprecht mit Krennan Aranas und überbringt ihm die gute Nachricht.$B$BAber vergesst nicht, $n. Ich behalte Euch im Auge. Sobald Ihr irgendetwas Merkwürdiges versucht, jage ich Euch eine Kugel zwischen die Augen.', '', '', '', '', '', '', 23420),
(14375, 'deDE', 'Die letzte Hoffnung auf Menschlichkeit', '', '', '', '', '', '', '', '', 23420),
(14222, 'deDE', 'Das letzte Gefecht', 'Tötet 8 rasende Pirscher.', 'Das hier ist ein perfekter Engpass, $n. Sprecht ruhig ein Gebet, wenn dergleichen Euch liegt.$B$BNehmt Euren Mut zusammen und dann lasst uns diese Köter vernichten!', '', '', '', '', '', '', 23420),
(14221, 'deDE', 'Niemals aufgeben, nur manchmal zurückziehen', 'Sprecht mit Darius Crowley in der Kathedrale des Erwachenden Lichts.', 'Wir hatten genug Munition, um den Bürgerkrieg einen weiteren Monat fortführen zu können. Aber es gibt mittlerweile derart viele Worgen, dass unsere Vorräte fast aufgebraucht sind.$B$BWir können nicht riskieren, dass dies passiert, während wir hier draußen schutzlos sind. Zieht Euch in die Kathedrale zurück und berichtet dem Boss. Bin mir sicher, der alte Crowley weiß, was wir als Nächstes tun sollten.', '', '', '', '', '', '', 23420),
(14218, 'deDE', 'Mit Blut und Asche', 'Tötet 80 Blutfangpirscher mit den Kanonen der Rebellen.', 'Der Augenblick ist gekommen, $n. Wenn wir ihre Zahl signifikant reduzieren wollen, müssen wir jetzt handeln.$B$BWir verfügen über die richtige Position.$B$BWir verfügen über die nötige Feuerkraft.$B$BAlles, was noch fehlt, ist der Beweis, dass wir über ausreichend Mut verfügen... und ich bin mir sicher, dass ich keinen Feigling vor mir stehen sehe.', '', '', '', '', '', '', 23420),
(14212, 'deDE', 'Aufopferung', 'Reitet mit Lord Darius Crowley und treibt 30 Blutfangpirscher zusammen.', 'Ihr müsst das nicht tun, $n. Ihr habt bereits mehr als genug getan.$B$BAber wenn Ihr Euch entschließen solltet, zu bleiben... Ich wäre ein Narr, Euch abzuweisen.$B$BMeine Mannen haben ihre Position in der Kathedrale befestigt und sind bereit für alles, was da kommen mag.$B$BIch werde mich nun auch dorthin aufmachen und so viele von diesen flohzersetzten Teufeln mit mir nehmen, wie ich nur kann. Seid willkommen, wenn Ihr mich zu diesem Tanz begleiten wollt.', '', '', '', '', '', '', 23420),
(14294, 'deDE', 'Neu formieren', 'Sprecht mit König Genn Graumähne im Graumähnenhof in Gilneas.', 'Wir haben hier alles getan, was wir tun konnten. Gebt König Graumähne Bescheid, dass wir etwas Zeit herausschinden konnten.$B$BIch habe das Gefühl, dass es nicht lange dauern wird, bis die Worgen sich neu formieren.$B$BWir werden uns in den Graumähnenhof im Westen zurückziehen müssen, wenn es so weit kommt. Das ist der letzte Ort, an dem wir standhalten können, ohne wie ein Fisch im Netz zu zappeln.', '', '', '', '', '', '', 23420),
(14293, 'deDE', 'Rettet Krennan Aranas', 'Rettet Krennan Aranas.', 'Noch so viele Jahre nach dem Krieg hat Crowley genügend Waffen in diesem Keller versteckt, um das halbe Viertel in die Luft zu jagen.$B$BLeider werden wir das wahrscheinlich auch tun müssen.$B$BWir können das Feuer jedoch noch nicht eröffnen, da ein Zivilist auf der anderen Seite des Gefängnisses in der Falle sitzt.$B$BUnd dabei handelt es sich nicht um irgendeinen Zivilisten. Krennan Aranas ist einer der brillantesten Alchemisten, den die Welt je gesehen hat.$B$BEiner seiner Tränke hat meine Tochter Tess kurz nach der Geburt vor dem sicheren Tod gerettet.$B$BNehmt mein Pferd und rettet ihn. Krennan darf nicht sterben.', '', '', '', '', '', '', 23420),
(14214, 'deDE', 'Nachricht für Graumähne', 'Sprecht mit König Graumähne im Militärviertel.', 'Es ist soweit, $n. Ab hier übernehmen wir.$B$BSagt König Graumähne, dass er über das Arsenal meines Vaters verfügen kann. Es beherbergt ausreichend Feuerkraft, die Worgen von hier bis zum Nordmeer zu schießen.', '', '', '', '', '', '', 23420),
(14204, 'deDE', 'Aus den Schatten', 'Besiegt 6 Blutfanglauerer.', 'Er hat sich in einen von ihnen verwandelt... nicht wahr? Wie sollen wir uns nur gegen einen Gegner behaupten, der etwas Derartiges anrichten kann?$B$BDas Arsenal meines Vaters ist bestimmt ein guter Anfang, denke ich.$B$BWorgen verbergen sich in den umliegenden Gassen. Nehmt einen meiner Mastiffs mit, um sie leichter finden zu können.$B$BWir müssen den Weg freiräumen, wenn wir diese Kanonen über Land bewegen wollen.', '', '', '', '', '', '', 23420),
(31889, 'deDE', 'Kampfhaustierzähmer: Kalimdor', 'Besiegt Gluk den Verräter, Grazzel den Großen, Kela Grimmtotem, Zoltan und Elena Flatterflug in Haustierkämpfen.', 'Jetzt, da Ihr Gelegenheit hattet, Euren Fertigkeiten den letzten Schliff zu verleihen, steht Euch die Welt der Haustierkämpfe uneingeschränkt offen.$B$BIn Kalimdor gibt es fünf Zähmer, die Ihr für mich besiegen sollt: Gluk den Verräter in Feralas, Grazzel den Großen in den Düstermarschen, Kela Grimmtotem in Tausend Nadeln, Zoltan im Teufelswald und Elena Flatterflug in der Mondlichtung.$B$BWenn Ihr sie bezwingen könnt, seid Ihr bereit, voranzuschreiten.', '', '', '', 'Wow! Ihr seid wirklich beeindruckend.', 'Marlene Trichdie', '', 23420),
(14159, 'deDE', 'Das Arsenal des Rebellenlords', 'Sprecht mit Josiah Avery im Militärdistrikt in Gilneas.', 'Das Arsenal, von dem Crowley spricht, befindet sich in einem Keller westlich von hier.$B$BEs gefällt mir zwar nicht, dass meine Gegner Artillerie in die Stadt geschmuggelt haben, aber sie könnte dabei helfen, gilnearische Leben zu retten.$B$BFindet Josiah Avery und requiriert die Rebellenartillerie. Wir müssen diese Waffen sinnvoll einsetzen.', '', '', '', '', '', '', 23420),
(26129, 'deDE', 'Waffenbrüder', 'Sprecht mit König Genn Graumähne im Militärviertel in Gilneas.', 'Hört zu, $n. Zum ersten Mal seit dem Bürgerkrieg bin ich mit Graumähne einer Meinung. Es ist an der Zeit, unsere Fehde endlich beizulegen.$B$BDiesen Bestien scheint es egal zu sein, ob man Rebell oder Königstreuer ist.$B$BÜberbringt Graumähne meine Antwort. Meine Männer werden sich seinen Leuten anschließen.$B$BNicht weit von hier gibt es ein sicheres Versteck, in Josiahs Keller. Meine Jungs haben dort ein wenig schwere Artillerie gelagert.$B$BSagt dem König, dass er sich jederzeit in unserer Waffenkammer bedienen kann.', '', '', '', '', '', '', 23420),
(14154, 'deDE', 'Mit knapper Not', 'Haltet die Worgen im oberen Bereich des Stadtgefängnisses von Gilneas 2 Minuten lang auf.', 'Eine dieser räudigen Flohschleudern hat Dempsey ordentlich erwischt. Wir können ihn nicht fortbringen, bevor seine Blutungen nicht gestoppt sind.$B$BMacht Euch nützlich und haltet diese Bastarde zurück. Ein paar Minuten reichen uns schon, $GBruder:Schwester;!', 'Überlebt, während Ihr 2 Minuten lang die Worgen aufhaltet.', '', '', '', '', 'Sprecht mit Lord Darius Crowley auf der Spitze des Steinwallgefängnisses in Gilneas.', 23420),
(28850, 'deDE', 'Auf dem Gefängnisdach', 'Sprecht mit Darius Crowley oben im Steinwallgefängnis in Gilneas.', 'Ich verstehe nicht, warum der König meine Zeit damit verschwenden will, um diesen Unterschlupf zu kämpfen, aber wenn Ihr Euer Leben aufs Spiel setzen wollt, tut Euch keinen Zwang an.$B$BCrowley und seine Männer verstecken sich oben. Vermutlich schmieden sie gerade Pläne gegen den König.', '', '', '', '', '', '', 23420),
(24930, 'deDE', 'Wenn Ihr schon dabei seid', 'Tötet 5 Blutfangworgen.', 'Wenn König Graumähne möchte, dass Ihr Euer Leben riskiert, um einen bekannten Verräter zu retten, soll mir das Recht sein.$B$BKönntet Ihr mir einen Gefallen tun? Wenn Ihr schon da draußen seid, tut etwas Sinnvolles und tötet diese verdammten Flohsäcke.', '', '', '', '', '', '', 23420),
(14157, 'deDE', 'Alte Differenzen', 'Sprecht mit Hauptmann Broderick im Steinwallgefängnis in Gilneas.', 'Lord Darius Crowley ist schon viele Dinge genannt worden. Rebell. Verräter. Terrorist.$B$BVor dem Bürgerkrieg nannte ich ihn einen... Freund.$B$BIch habe ihn nie dafür verurteilt, einen Aufstand gegen mich angeführt zu haben. Sein Land und seine Bevölkerung waren durch eine zehn Meter hohe Mauer von Gilneas abgeschnitten... Aber wir hatten keine Wahl.$B$BWie auch immer... Crowley ist genau die Person, die wir jetzt brauchen.$B$BBetretet das Steinwallgefängnis und fragt Hauptmann Broderick, wo Crowley zu finden ist. Ich würde ja meine eigenen Männer schicken, aber es existiert immer noch böses Blut zwischen uns.', '', '', '', '', '', '', 23420),
(14288, 'deDE', 'Je mehr, desto sicherer', 'Sprecht mit König Genn Graumähne im Militärviertel.', 'Solange die Worgen es darauf angelegt haben, uns alle Gliedmaßen auszureißen, kann ich Euch leider nicht so viel beibringen, wie ich möchte.$B$BWir sollten wirklich in Betracht ziehen, mit den anderen Überlebenden nach Süden zu ziehen. Wir haben erfahren, dass König Graumähne sich dort aufhält.$B$BWenn wir zusammen bleiben, steigen unsere Überlebenschancen... besonders wenn Ihr neben jemandem steht, der schmackhaft aussieht.', '', '', '', '', '', '', 23420),
(14277, 'deDE', 'Arkane Anfragen', 'Sprecht mit Myriam Zauberwache im Militärviertel.', 'Die meisten von uns haben es lebend hierher geschafft, aber auch in diesem Teil der Stadt gibt es Worgen.$B$BWir werden weiter nach Süden ziehen, sobald sich alle gesammelt haben.$B$BDa fällt mir ein, dass jemand nach Euch gefragt hat. Es war eine Magierin namens Myriam. Sie meinte etwas wie Ihr wärt "bereit".', '', '', '', '', '', '', 23420),
(14094, 'deDE', 'Bergt die Vorräte', 'Bergt 4 Vorratskisten.', 'Hört zu, $n. Wir können nicht zulassen, dass unser Proviant zerstört wird.$B$BDa Gilneas von der Außenwelt abgeschottet ist, werden wir den Winter nicht überleben, wenn wir noch mehr von unseren Vorräten verlieren.$B$BBitte helft mir dabei, zu retten, was zu retten ist.', '', '', '', '', '', '', 23420),
(14098, 'deDE', 'Evakuiert den Händlerplatz', 'Evakuiert 3 Zivilistenhäuser.', 'Die Zivilisten sind hier nicht mehr sicher. Nicht mal mehr in ihren Häusern.$B$BWir tun, was wir können, um die Worgen davon abzuhalten, in die Häuser zu gelangen, aber es sind einfach zu viele.$B$BHelft uns dabei, die Häuser der Zivilisten zu evakuieren. Die Armee meines Vaters im Gefängnisviertel wird sie besser schützen können.', '', '', '', '', '', '', 23420),
(14093, 'deDE', 'Die Hölle bricht los', 'Prinz Liam Graumähne wünscht, dass Ihr 6 tobende Worgen tötet.', 'Worgen! Mein Vater hat mich gewarnt, dass die Schöpfungen des Erzmagiers Arugal irgendwann Amok laufen würden.$B$BAber woher kommen sie?$B$BVermutlich macht es keinen Unterschied. Helft uns, kurzen Prozess mit ihnen zu machen. Wir werden ihnen zeigen, aus welchem Holz die Bewohner von Gilneas geschnitzt sind.', '', '', '', '', '', '', 23420),
(43301, 'deDE', 'Invasion: Azshara', '', '', '', '', '', '', '', '', 23420),
(43283, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23420),
(43300, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23420),
(43297, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23420),
(43285, 'deDE', 'Invasion: Vorgebirge des Hügellands', '', '', '', '', '', '', '', '', 23420),
(43291, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23420),
(43298, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23420),
(43286, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23420),
(43296, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23420),
(43292, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23420),
(43284, 'deDE', 'Invasion: Dun Morogh', '', '', '', '', '', '', '', '', 23420),
(43299, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23420),
(44911, 'deDE', 'Unbenutzt', '', '', '', '', '', '', '', '', 23420),
(44910, 'deDE', 'Unbenutzt', '', '', '', '', '', '', '', '', 23420),
(44909, 'deDE', 'Wöchentliches gewertetes Schlachtfeld', '', '', '', '', '', '', '', '', 23420),
(44908, 'deDE', 'Wöchentliche 3vs3-Quest', '', '', '', '', '', '', '', '', 23420),
(44891, 'deDE', 'Wöchentliche 2vs2-Quest', '', '', '', '', '', '', '', '', 23420),
(43303, 'deDE', 'Reif für Randale!', 'Sprecht mit David Globetrotter und findet heraus, wie man an der Rabenwehrrandale teilnimmt.', 'Willkommen zur Rabenwehrrandale! Es haben schon viele Kämpfer in der Arena der Rabenwehr teilgenommen, um ihre Stärke unter Beweis zu stellen, und jetzt seid Ihr an der Reihe.', '', '', '', '', '', '', 23420),
(43179, 'deDE', 'Die Kirin Tor von Dalaran', 'Schließt 4 Weltquests der Kirin Tor ab.', 'Unterstützt die Kirin Tor von Dalaran, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Kriegsmagier Silva in Dalaran zurück.', 23420),
(42422, 'deDE', 'Die Wächterinnen', 'Schließt 4 Weltquests der Wächterinnen ab.', 'Unterstützt die Wächterinnen, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Marin Klingenflügel auf der Insel der Behüter zurück.', 23420),
(42421, 'deDE', 'Die Nachtsüchtigen', 'Schließt beliebige 4 Weltquests in Suramar ab.', 'Unterstützt die Nachtsüchtigen von Suramar, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zur Ersten Arkanistin Thalyssra in Suramar zurück.', 23420),
(42420, 'deDE', 'Farondis'' Hofstaat', 'Schließt beliebige 4 Weltquests in Azsuna ab.', 'Unterstützt Farondis'' Hofstaat in Azsuna, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Veridis Fallon in Azsuna zurück.', 23420),
(42234, 'deDE', 'Die Valarjar', 'Schließt 4 beliebige Weltquests in Sturmheim ab.', 'Unterstützt die Vrykul von Sturmheim, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Valdemar Sturmsucher in Sturmheim zurück.', 23420),
(42233, 'deDE', 'Hochbergstämme', 'Schließt beliebige 4 Weltquests am Hochberg ab.', 'Unterstützt die Tauren vom Hochberg, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Ransa Graufeder in Donnertotem am Hochberg zurück.', 23420),
(42170, 'deDE', 'Die Traumweber', 'Schließt beliebige 4 Weltquests in Val''sharah ab.', 'Unterstützt die Druiden von Val''sharah, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Sylvia Hirschhorn in Val''sharah zurück.', 23420),
(41761, 'deDE', 'Zusammentreiben der Winterelche', 'Rettet 4 Talbuks auf Kuuros Hof.', 'Dieser Hof gehört meinem Vater Karmaan, und unser Viehbestand besteht größtenteils aus Talbuks. Ein Schotling muss das Tor ihres Pferchs geöffnet haben, denn sie sind alle entkommen!$B$BDie kleinen Mistviecher hätten sie direkt im Gehege töten können, doch sie jagen ihre Beute gerne. Ich befürchte, dass ich ihr nächstes Opfer sein werde, wenn ich noch länger hierbleibe!$B$BKönnt Ihr die Talbuks zusammentreiben und herbringen? Ich schaffe es nicht und mein Vater darf nichts davon erfahren!', '', '', '', '', '', '', 23420),
(14091, 'deDE', 'Da stimmt was nicht', 'Kehrt zu Prinz Liam Graumähne auf dem Händlerplatz zurück.', 'Der Leutnant scheint seinen schweren Wunden erlegen zu sein. Der Prinz wird die Neuigkeiten bestimmt hören wollen.$B$BIhr hört Kampfgeräusche, die aus der Richtung des Marktes kommen.', '', '', '', '', '', '', 23420),
(14078, 'deDE', 'Abgeriegelt!', 'Findet Leutnant Walden am nordwestlichen Ende des Händlerplatzes.', 'Was tut Ihr noch hier, $GBürger:Bürgerin;? Habt Ihr nicht gehört? Über die Stadt wurde eine vollständige Ausgangssperre verhängt.$B$BSprecht mit Leutnant Walden und befolgt seine Evakuierungsanweisungen.', '', '', '', '', '', '', 23420),
(13518, 'deDE', 'Die letzte Welle Überlebender', 'Macht alle überlebenden Flüchtlinge von Auberdine ausfindig.', 'Für Laird ist es zu spät, er hat es nicht mehr rechtzeitig geschafft.$B$BDie letzte Welle Nachtelfen aus Auberdine hat vor ein paar Stunden versucht zu entkommen. Sie sind in ihrem alten Zuhause geblieben und haben sich an die Hoffnung geklammert, dass es gerettet werden kann, aber offenbar mussten sie sich letzten Endes den Naturgewalten beugen. Ich befürchte, dass viele von ihnen es nicht schaffen werden, so wie Laird.$B$BEilt Euch. Geht zur Küste im Westen hinunter und macht so viele Überlebende ausfindig, wie Ihr könnt. Wir schicken Schildwachen und Hippogryphen, aber jeder Augenblick zählt - wir müssen wissen, wer überleben könnte.', '', '', '', '', '', '', 23420),
(26385, 'deDE', 'Unaufhaltsame Veränderungen', 'Sprecht mit Vesprystus in Rut''theran, um nach Lor''danel zu fliegen.', 'Sieht aus, als hättet Ihr Euren ersten Auftrag. Ihr müsst zurück nach Rut''theran reisen, wo wir zuerst ankamen, und mit Vesprystus sprechen, um einen Transport nach Lor''danel zu bekommen.$B$BSeid Ihr erst einmal in Lor''danel angekommen, müsst Ihr den Nachtelfen dort auf jede nur erdenkliche Weise helfen, die Euch möglich ist. Ich vertraue darauf, dass Ihr sie mit dem Respekt behandelt, den sie verdienen, nach allem was sie für uns getan haben.$B$BIch selbst muss in die ferne Stadt Sturmwind reisen und mich mit König Anduin treffen. Ich hoffe, Euer Gesicht bald wieder zu sehen, $n. Bis dahin ein herzliches Lebewohl.', '', '', '', '', '', '', 23420),
(28517, 'deDE', 'Die Heulende Eiche', 'Sprecht mit Genn Graumähne an der Heulenden Eiche in Darnassus.', 'So, das ist sie... unsere neue Heimat. Vielleicht werde ich mich nirgends mehr wirklich zu Hause fühlen.$B$BDoch wir müssen den Nachtelfen dankbar sein. Ihr Volk heißt nicht jeden willkommen. Dass sie uns aufgenommen haben, spricht für ihr Vertrauen und ihren Großmut.$B$BGwen und Graumähne sind an der Heulenden Eiche in der eigentlichen Stadt. Sucht sie auf. Nehmt das Portal unter dem Baum nördlich von uns und begebt Euch zur Heulenden Eiche im Nordteil der Stadt.', '', '', '', '', '', '', 23420),
(14434, 'deDE', 'Rut''theran', '', '', '', '', '', '', '', '', 23420),
(26706, 'deDE', 'Endspiel', 'Springt auf einen Hippogryphen, wenn Tobias Euch das Zeichen dazu gibt, und zerstört das Kanonenboot der Horde.', 'Wir müssen dieses fliegende Kanonenboot loswerden, wenn unser Volk eine Chance auf Flucht haben möchte.$B$BSo lautet der Plan. In Kielwasser trieben sich früher einige sympathisierende Rebellen herum. Ich konnte eine beträchtliche Menge Sprengstoff aus einem unserer Lager besorgen.$B$BWir werden den Feind mit einer Kraft überrumpeln, die so klein ist, dass sie sich unentdeckt durch die Luft bewegen kann. Die Hippogryphen der Nachtelfen eignen sich perfekt dafür.$B$BTobias gibt Euch das Signal, wenn wir bereit sind.', '', '', '', '', '', '', 23420),
(24681, 'deDE', 'Sie haben Verbündete, aber wir auch', 'Benutzt die Glevenschleuder der Nachtelfen, um 40 orcische Räuber, 8 Vorhuten des Wolfschlunds und 4 orcische Kriegsmaschinen zu vernichten.', 'Die Nachtelfen haben ihr Versprechen gehalten. Sie haben uns Schiffe geschickt und bieten uns Schutz in ihrem Land.$B$BLeider sind aber auch die Verbündeten der Verlassenen eingetroffen.$B$BDie Orcs rücken immer näher und das Kanonenboot der Horde schneidet unseren Leuten auf den Transportschiffen den Weg in die Sicherheit ab.$B$BDie Druiden versuchen, den Feind zurückzuhalten, aber lange werden sie nicht durchhalten. Zum Glück haben die Elfenschiffe Belagerungswaffen mitgebracht. Nehmt eine von den Glevenschleudern und tretet dem Feind gegenüber!', '', '', '', '', '', '', 23420),
(24680, 'deDE', 'Kielwasser', 'Sprecht mit Lord Darius Crowley in Kielwasser in Gilneas.', 'Wir haben vielleicht unsere Stadt verloren, aber zumindest scheinen die Toten uns vergeben zu haben.$B$BSchließt Euch den restlichen Überlebenden in Kielwasser an.$B$BIch bin mir sicher, dass es noch viel zu tun gibt, bevor wir das Gröbste überstanden haben. Genn ist noch nicht eingetroffen, also nehme ich an, dass Lord Crowley dort das Sagen hat.', '', '', '', '', '', '', 23420),
(24679, 'deDE', 'Der Segen des Patriarchen', 'Legt die gesegneten Opfergaben auf Aderics Grab.', 'Ihr habt es geschafft, $n. Sogar in Zeiten wie diesen müssen wir unseren Toten die Ehre erweisen.$B$BNehmt diese Opfergaben und legt sie auf Aderics Grab.$B$BHoffen wir, dass sie ausreichen, um die Geister unserer Ahnen zu besänftigen.', '', '', '', '', '', '', 23420),
(43242, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23420),
(43245, 'deDE', 'Invasion: Westfall', '', '', '', '', '', '', '', '', 23420),
(43287, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23420),
(43288, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23420);


DELETE FROM `quest_objectives_locale` WHERE (`ID`=265635) OR (`ID`=265467) OR (`ID`=266403) OR (`ID`=255768) OR (`ID`=261452) OR (`ID`=256573) OR (`ID`=264865) OR (`ID`=264864) OR (`ID`=264863) OR (`ID`=255603) OR (`ID`=265475) OR (`ID`=267253) OR (`ID`=264587) OR (`ID`=269150) OR (`ID`=269149) OR (`ID`=269148) OR (`ID`=269147) OR (`ID`=269145) OR (`ID`=267179) OR (`ID`=285159) OR (`ID`=285073) OR (`ID`=284172) OR (`ID`=284171) OR (`ID`=284170) OR (`ID`=283946) OR (`ID`=283945) OR (`ID`=283830) OR (`ID`=282762) OR (`ID`=267130) OR (`ID`=267129) OR (`ID`=267128) OR (`ID`=267127) OR (`ID`=254200) OR (`ID`=265108);
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(265635, 'deDE', 24920, 1, 'Einfallende Verlassene', 23420),
(265467, 'deDE', 24904, 0, 'Schlacht um Gilneas abgeschlossen', 23420),
(266403, 'deDE', 24575, 0, 'Versklavte Gilneer befreit', 23420),
(255768, 'deDE', 24468, 0, 'Unfallüberlebende gerettet', 23420),
(261452, 'deDE', 14416, 0, 'Gebirgspferd gerettet', 23420),
(256573, 'deDE', 14395, 0, 'Ertrinkenden Wachmann gerettet', 23420),
(264865, 'deDE', 14368, 2, 'James gerettet', 23420),
(264864, 'deDE', 14368, 1, 'Ashley gerettet', 23420),
(264863, 'deDE', 14368, 0, 'Cynthia gerettet', 23420),
(255603, 'deDE', 14369, 0, 'Kämpfer der Verlassenen getötet', 23420),
(265475, 'deDE', 14348, 0, 'Schwarzpulver auf Monstrosität geworfen', 23420),
(267253, 'deDE', 14212, 0, 'Blutfangpirscher zusammengetrieben', 23420),
(264587, 'deDE', 14293, 0, 'Krennan Aranas gerettet', 23420),
(269150, 'deDE', 31889, 0, 'Besiegt Elena Flatterflug', 23420),
(269149, 'deDE', 31889, 3, 'Besiegt Zoltan', 23420),
(269148, 'deDE', 31889, 4, 'Besiegt Kela Grimmtotem', 23420),
(269147, 'deDE', 31889, 2, 'Besiegt Grazzel den Großen', 23420),
(269145, 'deDE', 31889, 5, 'Besiegt Gluk den Verräter', 23420),
(267179, 'deDE', 14098, 0, 'Markthäuser evakuiert', 23420),
(285159, 'deDE', 43303, 0, 'Sprecht mit David Globetrotter', 23420),
(285073, 'deDE', 43179, 0, 'Schließt 3 Weltquests der Kirin Tor ab', 23420),
(284172, 'deDE', 42422, 0, 'Schließt 4 Weltquests der Wächterinnen ab', 23420),
(284171, 'deDE', 42421, 0, 'Schließt 4 Weltquests in Suramar ab', 23420),
(284170, 'deDE', 42420, 0, 'Schließt 4 Weltquests in Azsuna ab', 23420),
(283946, 'deDE', 42234, 0, 'Schließt 4 Weltquests in Sturmheim ab', 23420),
(283945, 'deDE', 42233, 0, 'Schließt 4 Weltquests am Hochberg ab', 23420),
(283830, 'deDE', 42170, 0, 'Schließt 4 Weltquests in Val''sharah ab', 23420),
(282762, 'deDE', 41761, 0, 'Elche zusammengetrieben', 23420),
(267130, 'deDE', 13518, 3, 'Volcor gerettet', 23420),
(267129, 'deDE', 13518, 2, 'Shaldyn gerettet', 23420),
(267128, 'deDE', 13518, 1, 'Gershala Nachtraunen gerettet', 23420),
(267127, 'deDE', 13518, 0, 'Cerellean Weißklaue gerettet', 23420),
(254200, 'deDE', 26706, 0, 'Kanonenboot zerstört', 23420),
(265108, 'deDE', 24679, 0, 'Opfergaben abgelegt', 23420);


DELETE FROM `gameobject_template_locale` WHERE ('entry'=201964 AND `locale`='deDE') OR ('entry'=201871 AND `locale`='deDE') OR ('entry'=201775 AND `locale`='deDE') OR ('entry'=201939 AND `locale`='deDE') OR ('entry'=201951 AND `locale`='deDE') OR ('entry'=201950 AND `locale`='deDE') OR ('entry'=201952 AND `locale`='deDE') OR ('entry'=201914 AND `locale`='deDE') OR ('entry'=1619 AND `locale`='deDE') OR ('entry'=201607 AND `locale`='deDE') OR ('entry'=205005 AND `locale`='deDE') OR ('entry'=205009 AND `locale`='deDE') OR ('entry'=201891 AND `locale`='deDE') OR ('entry'=201594 AND `locale`='deDE') OR ('entry'=202625 AND `locale`='deDE') OR ('entry'=202624 AND `locale`='deDE') OR ('entry'=202634 AND `locale`='deDE') OR ('entry'=202628 AND `locale`='deDE') OR ('entry'=202627 AND `locale`='deDE') OR ('entry'=202626 AND `locale`='deDE') OR ('entry'=202646 AND `locale`='deDE') OR ('entry'=202645 AND `locale`='deDE') OR ('entry'=202644 AND `locale`='deDE') OR ('entry'=202643 AND `locale`='deDE') OR ('entry'=202633 AND `locale`='deDE') OR ('entry'=202632 AND `locale`='deDE') OR ('entry'=202631 AND `locale`='deDE') OR ('entry'=202630 AND `locale`='deDE') OR ('entry'=202629 AND `locale`='deDE') OR ('entry'=202642 AND `locale`='deDE') OR ('entry'=202636 AND `locale`='deDE') OR ('entry'=202635 AND `locale`='deDE') OR ('entry'=202641 AND `locale`='deDE') OR ('entry'=202640 AND `locale`='deDE') OR ('entry'=202639 AND `locale`='deDE') OR ('entry'=202638 AND `locale`='deDE') OR ('entry'=202637 AND `locale`='deDE') OR ('entry'=196473 AND `locale`='deDE') OR ('entry'=196810 AND `locale`='deDE') OR ('entry'=196809 AND `locale`='deDE') OR ('entry'=196808 AND `locale`='deDE') OR ('entry'=197333 AND `locale`='deDE') OR ('entry'=196465 AND `locale`='deDE') OR ('entry'=196466 AND `locale`='deDE') OR ('entry'=196411 AND `locale`='deDE') OR ('entry'=196472 AND `locale`='deDE') OR ('entry'=1618 AND `locale`='deDE') OR ('entry'=1731 AND `locale`='deDE') OR ('entry'=1617 AND `locale`='deDE') OR ('entry'=196403 AND `locale`='deDE') OR ('entry'=196849 AND `locale`='deDE') OR ('entry'=197337 AND `locale`='deDE') OR ('entry'=196394 AND `locale`='deDE') OR ('entry'=196846 AND `locale`='deDE') OR ('entry'=196879 AND `locale`='deDE') OR ('entry'=196880 AND `locale`='deDE') OR ('entry'=196854 AND `locale`='deDE') OR ('entry'=204423 AND `locale`='deDE') OR ('entry'=204132 AND `locale`='deDE') OR ('entry'=204131 AND `locale`='deDE') OR ('entry'=204774 AND `locale`='deDE') OR ('entry'=177278 AND `locale`='deDE') OR ('entry'=194107 AND `locale`='deDE') OR ('entry'=180682 AND `locale`='deDE') OR ('entry'=181646 AND `locale`='deDE') OR ('entry'=164767 AND `locale`='deDE') OR ('entry'=164764 AND `locale`='deDE') OR ('entry'=164763 AND `locale`='deDE') OR ('entry'=164762 AND `locale`='deDE') OR ('entry'=164761 AND `locale`='deDE') OR ('entry'=164760 AND `locale`='deDE') OR ('entry'=164759 AND `locale`='deDE') OR ('entry'=164766 AND `locale`='deDE') OR ('entry'=164765 AND `locale`='deDE') OR ('entry'=92535 AND `locale`='deDE') OR ('entry'=175725 AND `locale`='deDE') OR ('entry'=91672 AND `locale`='deDE') OR ('entry'=193981 AND `locale`='deDE') OR ('entry'=92529 AND `locale`='deDE') OR ('entry'=92530 AND `locale`='deDE') OR ('entry'=175730 AND `locale`='deDE') OR ('entry'=177279 AND `locale`='deDE') OR ('entry'=21581 AND `locale`='deDE') OR ('entry'=92537 AND `locale`='deDE') OR ('entry'=92526 AND `locale`='deDE') OR ('entry'=92525 AND `locale`='deDE') OR ('entry'=193583 AND `locale`='deDE') OR ('entry'=193584 AND `locale`='deDE') OR ('entry'=175760 AND `locale`='deDE') OR ('entry'=175727 AND `locale`='deDE') OR ('entry'=92538 AND `locale`='deDE') OR ('entry'=91673 AND `locale`='deDE') OR ('entry'=208830 AND `locale`='deDE') OR ('entry'=208829 AND `locale`='deDE') OR ('entry'=175731 AND `locale`='deDE') OR ('entry'=207321 AND `locale`='deDE') OR ('entry'=142110 AND `locale`='deDE') OR ('entry'=208815 AND `locale`='deDE') OR ('entry'=208814 AND `locale`='deDE') OR ('entry'=187337 AND `locale`='deDE') OR ('entry'=187296 AND `locale`='deDE') OR ('entry'=176310 AND `locale`='deDE') OR ('entry'=195112 AND `locale`='deDE') OR ('entry'=208831 AND `locale`='deDE') OR ('entry'=204458 AND `locale`='deDE') OR ('entry'=204428 AND `locale`='deDE') OR ('entry'=203428 AND `locale`='deDE') OR ('entry'=207999 AND `locale`='deDE') OR ('entry'=207626 AND `locale`='deDE') OR ('entry'=202319 AND `locale`='deDE');
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(201964, 'deDE', 'Aderics Grab', '', NULL, 23240),
(201871, 'deDE', 'Gestörte Erde', '', NULL, 23240),
(201775, 'deDE', 'Kugel und Kette', '', NULL, 23240),
(201939, 'deDE', 'Verwitterte Kiste', '', NULL, 23240),
(201951, 'deDE', 'Brunnen der Ruhe', '', NULL, 23240),
(201950, 'deDE', 'Brunnen des Zorns', '', NULL, 23240),
(201952, 'deDE', 'Brunnen des Gleichgewichts', '', NULL, 23240),
(201914, 'deDE', 'Mondblatt', '', NULL, 23240),
(1619, 'deDE', 'Erdwurzel', '', NULL, 23240),
(201607, 'deDE', 'Alte Tagebuchseite', '', NULL, 23240),
(205005, 'deDE', 'Amboss', '', NULL, 23240),
(205009, 'deDE', 'Schmiede', '', NULL, 23240),
(201891, 'deDE', 'Gestohlenes Banner', '', NULL, 23240),
(201594, 'deDE', 'Koroths Banner', '', NULL, 23240),
(202625, 'deDE', 'Sitzbank', '', NULL, 23240),
(202624, 'deDE', 'Sitzbank', '', NULL, 23240),
(202634, 'deDE', 'Sitzbank', '', NULL, 23240),
(202628, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202627, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202626, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202646, 'deDE', 'Sitzbank', '', NULL, 23240),
(202645, 'deDE', 'Sitzbank', '', NULL, 23240),
(202644, 'deDE', 'Sitzbank', '', NULL, 23240),
(202643, 'deDE', 'Sitzbank', '', NULL, 23240),
(202633, 'deDE', 'Sitzbank', '', NULL, 23240),
(202632, 'deDE', 'Sitzbank', '', NULL, 23240),
(202631, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202630, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202629, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202642, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202636, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202635, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202641, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202640, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202639, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202638, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(202637, 'deDE', 'Schaukelstuhl', '', NULL, 23240),
(196473, 'deDE', 'Leinengebundenes Buch', '', NULL, 23240),
(196810, 'deDE', 'Schiffsbauerwerkzeuge', '', NULL, 23240),
(196809, 'deDE', 'Holzplanken', '', NULL, 23240),
(196808, 'deDE', 'Fass mit Kohlenteer', '', NULL, 23240),
(197333, 'deDE', 'Gilneas - Drowning Sparkles', '', NULL, 23240),
(196465, 'deDE', 'Aschenquell', '', NULL, 23240),
(196466, 'deDE', 'Aschenquell', '', NULL, 23240),
(196411, 'deDE', 'Kellertür', '', NULL, 23240),
(196472, 'deDE', 'Omas gute Kleider', '', NULL, 23240),
(1618, 'deDE', 'Friedensblume', '', NULL, 23240),
(1731, 'deDE', 'Kupferader', '', NULL, 23240),
(1617, 'deDE', 'Silberblatt', '', NULL, 23240),
(196403, 'deDE', 'Schwarzpulverfass', '', NULL, 23240),
(196849, 'deDE', 'Kohlenpfanne', '', NULL, 23240),
(197337, 'deDE', 'Gilneas Invasion Camera', '', NULL, 23240),
(196394, 'deDE', 'Kiste mit Alraunenessenz', '', NULL, 23240),
(196846, 'deDE', 'Briefkasten', '', NULL, 23240),
(196879, 'deDE', 'Schmiede', '', NULL, 23240),
(196880, 'deDE', 'Amboss', '', NULL, 23240),
(196854, 'deDE', 'Kochtopf', '', NULL, 23240),
(204423, 'deDE', 'Kanonenboot der Orcs', '', NULL, 23240),
(204132, 'deDE', 'Schmiede', '', NULL, 23240),
(204131, 'deDE', 'Amboss', '', NULL, 23240),
(204774, 'deDE', 'Briefkasten', '', NULL, 23240),
(177278, 'deDE', 'Mondbrunnen', '', NULL, 23240),
(194107, 'deDE', 'Verkrustete Muschel', '', NULL, 23240),
(180682, 'deDE', 'Ein Schwarm öliger Schwarzmaulfische', '', NULL, 23240),
(181646, 'deDE', 'Exodar', '', NULL, 23240),
(164767, 'deDE', 'Holzstuhl', '', NULL, 23240),
(164764, 'deDE', 'Holzstuhl', '', NULL, 23240),
(164763, 'deDE', 'Holzstuhl', '', NULL, 23240),
(164762, 'deDE', 'Holzstuhl', '', NULL, 23240),
(164761, 'deDE', 'Holzstuhl', '', NULL, 23240),
(164760, 'deDE', 'Holzstuhl', '', NULL, 23240),
(164759, 'deDE', 'Holzstuhl', '', NULL, 23240),
(164766, 'deDE', 'Holzstuhl', '', NULL, 23240),
(164765, 'deDE', 'Holzstuhl', '', NULL, 23240),
(92535, 'deDE', 'Allgemeines Handwerk', '', NULL, 23240),
(175725, 'deDE', 'Die Alten Götter und die Ordnung von Azeroth', '', NULL, 23240),
(91672, 'deDE', 'Holzstuhl', '', NULL, 23240),
(193981, 'deDE', 'Lexikon der Macht', '', NULL, 23240),
(92529, 'deDE', 'Verzauberkunst', '', NULL, 23240),
(92530, 'deDE', 'Verzauberkunst', '', NULL, 23240),
(175730, 'deDE', 'Der Weltenbaum und der Smaragdgrüne Traum', '', NULL, 23240),
(177279, 'deDE', 'Mondbrunnen', '', NULL, 23240),
(21581, 'deDE', 'Folgen des Zweiten Krieges', '', NULL, 23240),
(92537, 'deDE', 'Erste Hilfe', '', NULL, 23240),
(92526, 'deDE', 'Alchemie', '', NULL, 23240),
(92525, 'deDE', 'Argentumdämmerung', '', NULL, 23240),
(193583, 'deDE', 'Gnomischer Werkzeugeimer', '', NULL, 23240),
(193584, 'deDE', 'Gnomischer Werkzeugeimer', '', NULL, 23240),
(175760, 'deDE', 'Aufstieg der Blutelfen', '', NULL, 23240),
(175727, 'deDE', 'Der Krieg der Ahnen', '', NULL, 23240),
(92538, 'deDE', 'Kochkunst', '', NULL, 23240),
(91673, 'deDE', 'Herd', '', NULL, 23240),
(208830, 'deDE', 'Reishäufchen', '', NULL, 23240),
(208829, 'deDE', 'Reiskorb', '', NULL, 23240),
(175731, 'deDE', 'Die Verbannung der Hochelfen', '', NULL, 23240),
(207321, 'deDE', 'Heldenaufruf', '', NULL, 23240),
(142110, 'deDE', 'Briefkasten', '', NULL, 23240),
(208815, 'deDE', 'Vergrabener Krug', '', NULL, 23240),
(208814, 'deDE', 'Vergrabener Kimchikrug', '', NULL, 23240),
(187337, 'deDE', 'Gildentresor', '', NULL, 23240),
(187296, 'deDE', 'Gildentresor', '', NULL, 23240),
(176310, 'deDE', 'Schiff (Die Bravado)', '', NULL, 23240),
(195112, 'deDE', 'Tür des Grabhügels', '', NULL, 23240),
(208831, 'deDE', 'Teldrassilmuschel', '', NULL, 23240),
(204458, 'deDE', 'Brandsatz', '', NULL, 23240),
(204428, 'deDE', 'Seil', '', NULL, 23240),
(203428, 'deDE', 'Kanonenboot der Orcs', '', NULL, 23240),
(207999, 'deDE', 'Liams Sargdeckel', '', NULL, 23240),
(207626, 'deDE', 'Liam''s Coffin [INTERNAL]', '', NULL, 23240),
(202319, 'deDE', 'Gesegnete Opfergaben', '', NULL, 23240);


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text    xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_offer_reward` WHERE `ID` IN (24678 /*24678*/, 24920 /*24920*/, 24903 /*24903*/, 24902 /*24902*/, 24904 /*24904*/, 24676 /*24676*/, 24674 /*24674*/, 24675 /*24675*/, 24575 /*24575*/, 24677 /*24677*/, 24592 /*24592*/, 24672 /*24672*/, 24673 /*24673*/, 24593 /*24593*/, 24646 /*24646*/, 24628 /*24628*/, 24627 /*24627*/, 24617 /*24617*/, 24616 /*24616*/, 24578 /*24578*/, 24501 /*24501*/, 24495 /*24495*/, 24484 /*24484*/, 24483 /*24483*/, 24472 /*24472*/, 24468 /*24468*/, 24438 /*24438*/, 14467 /*14467*/, 14466 /*14466*/, 14465 /*14465*/, 14402 /*14402*/, 14401 /*14401*/, 14400 /*14400*/, 14399 /*14399*/, 14398 /*14398*/, 14412 /*14412*/, 14404 /*14404*/, 14403 /*14403*/, 14416 /*14416*/, 14406 /*14406*/, 14397 /*14397*/, 14395 /*14395*/, 14396 /*14396*/, 14386 /*14386*/, 14382 /*14382*/, 14369 /*14369*/, 14368 /*14368*/, 14367 /*14367*/, 14366 /*14366*/, 14348 /*14348*/, 14347 /*14347*/, 14336 /*14336*/, 14321 /*14321*/, 14320 /*14320*/, 14313 /*14313*/, 14375 /*14375*/, 14222 /*14222*/, 14221 /*14221*/, 14218 /*14218*/, 14212 /*14212*/, 14294 /*14294*/, 14293 /*14293*/, 14214 /*14214*/, 14204 /*14204*/, 14159 /*14159*/, 26129 /*26129*/, 14154 /*14154*/, 28850 /*28850*/, 14157 /*14157*/, 24930 /*24930*/, 14288 /*14288*/, 14277 /*14277*/, 14099 /*14099*/, 14098 /*14098*/, 14093 /*14093*/, 14094 /*14094*/, 14091 /*14091*/, 14078 /*14078*/, 26385 /*26385*/, 28517 /*28517*/, 14434 /*14434*/, 26706 /*26706*/, 24681 /*24681*/, 24680 /*24680*/, 24679 /*24679*/, 24602 /*24602*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(24678, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr seid hier, $n. Die meisten haben es auf die andere Seite geschafft, aber jetzt sitzen wir hier, umringt von unseren eigenen… Toten!', 23420), -- 24678
(24920, 0, 0, 0, 0, 0, 0, 0, 0, 'Das habt Ihr gut gemacht, $n. Fast alle sind heil auf die andere Seite gekommen.', 23420), -- 24920
(24903, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr stellt mich vor die schwierigste Entscheidung meines Lebens, $n.', 23420), -- 24903
(24902, 0, 0, 0, 0, 0, 0, 0, 0, 'Die Seuche? Etwas so Hinterhältiges, dass nicht einmal die Orcs mit ihrem Einsatz einverstanden sind? Wir müssen König Graumähne davon berichten.', 23420), -- 24902
(24904, 0, 0, 0, 0, 0, 0, 0, 0, 'Wir haben die Verlassenen zurückgedrängt. Wir haben die Kontrolle über drei der vier Viertel.$B$BAber zu welchem Preis…', 23420), -- 24904
(24676, 1, 0, 0, 0, 0, 0, 0, 0, 'Jetzt haben wir dank Euch die Bewohner von Glutstein auf unserer Seite.', 23420), -- 24676
(24674, 0, 0, 0, 0, 0, 0, 0, 0, 'Ich danke Euch, $n. Unser Volk wird nicht länger unter dem Joch dieser Ausgeburt leiden.', 23420), -- 24674
(24675, 0, 0, 0, 0, 0, 0, 0, 0, 'Ich danke Euch, $n. Unsere Männer und Frauen werden ein wundervolles letztes Mahl genießen, bevor sie in die Schlacht aufbrechen.', 23420), -- 24675
(24575, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr habt es wieder geschafft, $n. Die befreiten Dorfbewohner wollen uns mit allen Kräften im Kampf gegen die Verlassenen unterstützen.', 23420), -- 24575
(24677, 1, 0, 0, 0, 0, 0, 0, 0, 'Ich bin froh, dass Ihr hier seid, $n. Wir sind von allen Seiten von den Verlassenen umzingelt und können jede Hilfe brauchen, die wir kriegen können.', 23420), -- 24677
(24592, 1, 0, 0, 0, 0, 0, 0, 0, 'Ich wünschte, wir hätten es anders lösen können, $n. Lasst uns sichergehen, dass wir die Sache ohne weiteres Blutvergießen beilegen.', 23420), -- 24592
(24672, 0, 0, 0, 0, 0, 0, 0, 0, '$n!$B$BGenn… Sie haben Genn… Sie haben… unseren König verschleppt!', 23420), -- 24672
(24673, 0, 0, 0, 0, 0, 0, 0, 0, 'König Graumähne hat mir den Plan kurz erläutert, bevor er in den Schwarzforst aufgebrochen ist. Er klingt trotzdem nicht weniger verrückt.', 23420), -- 24673
(24593, 0, 0, 0, 0, 0, 0, 0, 0, 'Also ist es vollbracht, $n. Ihr gehört jetzt zu uns.', 23420), -- 24593
(24646, 66, 0, 0, 0, 0, 0, 0, 0, 'Ich wusste, dass ich mich auf Euch verlassen kann. Ihr habt Eure Sache gut gemacht, $n.', 23420), -- 24646
(24628, 1, 0, 0, 0, 0, 0, 0, 0, 'Diese einfachen Blätter wachsen durch Elunes Gnade. Sie werden Eurem Geist helfen zu verstehen, warum wir das Gleichgewicht brauchen, und Eurer Seele für alle Zeit lehren, das Tier in Euch zu beherrschen.', 23420), -- 24628
(24627, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr seid so gut, wie ich Euch in Erinnerung habe, $n. Wie schön, dass Ihr wieder da seid.', 23420), -- 24627
(24617, 1, 0, 0, 0, 0, 0, 0, 0, 'Es geht Euch gut, $n! Ich habe schon so lange auf diesen Tag gewartet. Es ist eine wahre Freude, Euch zu sehen, $gFreund:Freundin;.$B$BIch habe gehört, was Ihr getan habt, und ich bin Euch so dankbar… besonders für Lorna. Sie ist alles, was mir geblieben ist. Ich lasse sie sofort herholen.', 23420), -- 24617
(24616, 1, 66, 0, 0, 0, 0, 0, 0, 'Gut gemacht, $n. Die Späherin hatte nicht die geringste Chance gegen Euch.', 23420), -- 24616
(24578, 0, 0, 0, 0, 0, 0, 0, 0, 'Ich habe Euch erwartet, $n. Habt keine Angst.$B$BMein Name ist Belysra. Ich bin eine Priesterin des Mondes... eine Nachtelfe.$B$BIhr kennt Meinesgleichen vielleicht nicht, aber seit der Fluch Euch befiel, sind die Schicksale unserer Völker miteinander verbunden.', 23420), -- 24578
(24501, 0, 0, 0, 0, 0, 0, 0, 0, 'Gute Arbeit, $n. Wir haben gehört, dass es weiter oben in den Bergen noch Überlebende geben soll. Jetzt können wir endlich Späher dorthin schicken.', 23420), -- 24501
(24495, 0, 0, 0, 0, 0, 0, 0, 0, 'Danke, $n. Es wird ein wenig dauern, aber ich versuche, in dem was wir haben einen Sinn zu finden.', 23420), -- 24495
(24484, 0, 0, 0, 0, 0, 0, 0, 0, 'Das habt Ihr gut gemacht, $n. Aber diese Spinnen sind überall und ich fürchte, ihre Zahl hat kaum abgenommen.', 23420), -- 24484
(24483, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr habt es also geschafft, $n. Anscheinend haben es fast alle geschafft.$B$BFür eine Ausnahmesituation schlagen wir uns nicht schlecht.', 23420), -- 24483
(24472, 1, 273, 0, 0, 0, 0, 0, 0, 'Ihr habt den Ettin wirklich sehr wütend gemacht, $n. Ich habe ihn selbst gehört.$B$BHoffen wir, dass es funktioniert.', 23420), -- 24472
(24468, 1, 0, 0, 0, 0, 0, 0, 0, 'Ihr habt es wieder geschafft, $n. Ihr habt meinen Dank.', 23420), -- 24468
(24438, 1, 25, 0, 0, 0, 0, 0, 0, 'Danke, dass Ihr angehalten habt, $n. Unseren Karren hat es ziemlich übel erwischt.$B$BAber der vor uns ist noch schlimmer dran.', 23420), -- 24438
(14467, 0, 0, 0, 0, 0, 0, 0, 0, 'Schaut, $n! Schaut nur, was aus Dämmerhafen geworden ist!$B$BSchaut, was aus dem letzten sicheren Ort in Gilneas geworden ist!', 23420), -- 14467
(14466, 0, 0, 0, 0, 0, 0, 0, 0, 'Da seid Ihr ja, $n. Ich habe Euch erwartet.$B$BIch habe von Eurer Genesung gehört und… Wartet! Spürt Ihr das?', 23420), -- 14466
(14465, 2, 0, 0, 0, 0, 0, 0, 0, '$n. Ich habe schon so viel von Euch gehört.$B$BWie ich hörte habt Ihr beim Überleben meiner Familie während des Ausbruchs in Gilneas eine wichtige Rolle gespielt.', 23420), -- 14465
(14402, 273, 0, 0, 0, 0, 0, 0, 0, 'Ihr habt das Unmögliche geschafft, $n! Ihr habt Oma Wahl überredet, ihr Zuhause zu verlassen.$B$BIch muss schon sagen, Ihr seid etwas ganz Besonderes.', 23420), -- 14402
(14401, 0, 0, 0, 0, 0, 0, 0, 0, 'Da ist er ja! Omas kleiner Liebling!$B$BVielen Dank, dass Ihr ihn gefunden habt. Ich hoffe, es hat Euch nicht allzu viele Umstände bereitet.$B$BOh je… meine Fingernägel sind ja ganz dreckig!', 23420), -- 14401
(14400, 1, 0, 0, 0, 0, 0, 0, 0, 'Ihr seid ein Goldschatz, $n! Vielen Dank!', 23420), -- 14400
(14399, 0, 0, 0, 0, 0, 0, 0, 0, 'Vielen Dank, $n. Ich hoffe, Ihr habt nicht hineingelinst!$B$BWo bleiben Eure Manieren, Herzchen?', 23420), -- 14399
(14398, 0, 0, 0, 0, 0, 0, 0, 0, 'Oh, Besuch! Bleibt Ihr auf eine Tasse Tee, Herzchen?', 23420), -- 14398
(14412, 0, 0, 0, 0, 0, 0, 0, 0, 'Ausgezeichnet! Ich bin hier fast fertig.', 23420), -- 14412
(14404, 1, 0, 0, 0, 0, 0, 0, 0, 'Toll, $n. Jetzt kann ich die Reparaturen im Handumdrehen fertigstellen.', 23420), -- 14404
(14403, 0, 0, 0, 0, 0, 0, 0, 0, 'Seid Ihr hier, um uns zu helfen?', 23420), -- 14403
(14416, 0, 0, 0, 0, 0, 0, 0, 0, 'Wir haben die Pferde. Ich stelle sicher, dass sie nach Dämmerhafen gelangen.', 23420), -- 14416
(14406, 0, 0, 0, 0, 0, 0, 0, 0, 'Bleibt zurück! Sonst werde ich…$B$BSeid Ihr das? Beim Licht! Ihr seid es, $n!', 23420), -- 14406
(14397, 0, 0, 0, 0, 0, 0, 0, 0, 'Liam hat Recht. Wir müssen in ein höher gelegenes Gebiet fliehen.$B$BHelft mir, die Leute zu benachrichtigen, während ich die Organisation der Evakuierung übernehme.', 23420), -- 14397
(14395, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr habt getan, was Ihr konntet, $n. Mit etwas Glück schaffen es noch ein paar andere ans Ufer.', 23420), -- 14395
(14396, 0, 0, 0, 0, 0, 0, 0, 0, 'Das Meer, $n! Es hat alles verschlungen… das Land… die Verlassenen… unsere Männer!', 23420), -- 14396
(14386, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr habt es geschafft, $n. Das sollte ihnen den Wind aus den Segeln nehmen.', 23420), -- 14386
(14382, 0, 0, 0, 0, 0, 0, 0, 0, 'Gut gemacht, $n. Ihr seid vielleicht eine blutrünstige Bestie, aber Ihr seid unsere Bestie.', 23420), -- 14382
(14369, 0, 0, 0, 0, 0, 0, 0, 0, 'Nicht schlecht, $n. Gut, dass Ihr auf unserer Seite steht.', 23420), -- 14369
(14368, 0, 0, 0, 0, 0, 0, 0, 0, 'Meine Kinder sind in Sicherheit! Ihr habt etwas Wundervolles getan, $n. Ich weiß nicht, wie ich Euch danken soll.', 23420), -- 14368
(14367, 0, 0, 0, 0, 0, 0, 0, 0, 'Die Verlassenen sind hier und es sind viele, $n. Wir haben kaum genug Männer, um sie zurückzuhalten.', 23420), -- 14367
(14366, 0, 0, 0, 0, 0, 0, 0, 0, 'Gute Nachrichten, $n. Ich habe die verbleibenden Milizen an die Küste geschickt, damit sie der Streitmacht der Verlassenen direkt gegenübertreten.', 23420), -- 14366
(14348, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr und ich, $n. Wir sind ein tolles Team…$B$BEs ist schön, dass Ihr wieder da seid.', 23420), -- 14348
(14347, 0, 0, 0, 0, 0, 0, 0, 0, 'Ja, $n! Genau wie in alten Zeiten…$B$BIch muss mich sehr zurückhalten, um Euch nicht zu erschießen, aber Krennan hat uns alles erklärt.', 23420), -- 14347
(14336, 0, 0, 0, 0, 0, 0, 0, 0, '$n!!! Ihr seid also WIRKLICH am Leben!$B$BIch dachte, ich hätte von alten Zeiten geträumt, als ich Eure Stimme hörte…', 23420), -- 14336
(14321, 0, 0, 0, 0, 0, 0, 0, 0, 'Verlassene! Schnell, $n! Wir müssen eine Verteidigung zusammenstellen!', 23420), -- 14321
(14320, 0, 0, 0, 0, 0, 0, 0, 0, 'Die Kiste wurde offensichtlich zerschlagen. Alle darin enthaltenen Phiolen sind zerbrochen.', 23420), -- 14320
(14313, 0, 0, 0, 0, 0, 0, 0, 0, 'Es hat funktioniert! Beim Licht, es hat funktioniert!', 23420), -- 14313
(14375, 0, 0, 0, 0, 0, 0, 0, 0, 'Ich muss Euch irgendwie durchkriegen, $n. Diese Dosis ist stark genug, um ein Pferd zu töten.$B$BAber ich kenne Euch. Ich weiß, aus welchem Holz Ihr geschnitzt seid. Ihr werdet es schon schaffen.$B$BVertraut mir. Ich weiß genau, was Ihr durchmacht.$B$BJetzt trinkt leer und schließt Eure Augen.', 23420), -- 14375
(14222, 0, 0, 0, 0, 0, 0, 0, 0, 'Es... Es kommen keine weiteren.$B$BNein, $n. Das verheißt nichts Gutes.', 23420), -- 14222
(14221, 0, 0, 0, 0, 0, 0, 0, 0, 'Wir haben ihnen alles entgegengeworfen, was wir haben... aber sie kommen noch immer. Sorgt Euch nicht, $n, wir werden noch viel mehr von ihnen über den Jordan schicken, bevor der Tag vorbei ist.', 23420), -- 14221
(14218, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr habt Euch wohl geschlagen, $n. Ihr habt mehr für uns getan, als von jedem Gilneer verlangt werden könnte.$B$BDie Munition geht uns aus. Wir müssen uns drinnen neu formieren.', 23420), -- 14218
(14212, 0, 0, 0, 0, 0, 0, 0, 0, 'Macht Euch bereit, $n.$B$BHier kommen sie!', 23420), -- 14212
(14294, 0, 0, 0, 0, 0, 0, 0, 0, 'Viele Möglichkeiten stehen uns nicht zur Auswahl, $n. Unser nächster Schritt wird eine wichtige Entscheidung.', 23420), -- 14294
(14293, 0, 0, 0, 0, 0, 0, 0, 0, 'Gut gemacht, $n. Wir werden dafür sorgen, dass Krennan heil aus der Stadt fliehen kann.', 23420), -- 14293
(14214, 0, 0, 0, 0, 0, 0, 0, 0, 'Ich wusste, dass Crowley uns helfen würde. Seine Waffen werden uns äußerst nützlich sein.', 23420), -- 14214
(14204, 0, 0, 0, 0, 0, 0, 0, 0, 'Hervorragend. Ich werde ein paar Leute zusammentrommeln, um diese Kanonen in Position zu bringen.', 23420), -- 14204
(14159, 0, 0, 0, 0, 0, 0, 0, 0, 'Seht mich nicht an! Lasst mich in Ruhe!', 23420), -- 14159
(26129, 0, 0, 0, 0, 0, 0, 0, 0, 'Eine Waffenkammer der Rebellen? In meiner eigenen Stadt?$B$BWas zur Hölle führte Crowley nur im Schilde?', 23420), -- 26129
(14154, 0, 0, 0, 0, 0, 0, 0, 0, 'Wir haben''s geschafft, $n. Dank Euch hat ein guter Mann überlebt.', 23420), -- 14154
(28850, 0, 0, 0, 0, 0, 0, 0, 0, 'Graumähne hat Recht. Diese Bestien scheren sich einen Dreck um unsere Politik.$B$BWir in Gilneas müssen zusammenhalten.', 23420), -- 28850
(14157, 0, 0, 0, 0, 0, 0, 0, 0, 'Graumähne will Crowley retten? Hat er den Verstand verloren?', 23420), -- 14157
(24930, 0, 0, 0, 0, 0, 0, 0, 0, 'Wenn ich nicht selbst gesehen hätte, dass Ihr ein paar von diesen verlausten Kötern getötet habt, hätte ich geschworen, dass Ihr Eure Aufgabe nicht erledigt habt.$B$BWir scheinen ihre Zahl nicht einmal annähernd dezimiert zu haben.', 23420), -- 24930
(14288, 0, 0, 0, 0, 0, 0, 0, 0, 'Es war klug von Euch, hierher zu kommen. Wenn die Einwohner von Gilneas zusammenhalten, können wir diesen schrecklichen Feind vielleicht doch noch besiegen.', 23420), -- 14288
(14277, 0, 0, 0, 0, 0, 0, 0, 0, 'Ein Schüler der arkanen Künste findet immer einen Weg nach vorne. Ich freue mich, Euch zu sehen, $n.', 23420), -- 14277
(14099, 0, 0, 0, 0, 0, 0, 0, 0, 'Es geht uns allen gut. Wir sind ein wenig mitgenommen, aber... wir leben.', 23420), -- 14099
(14098, 0, 0, 0, 0, 0, 0, 0, 0, 'Gute Arbeit, $n. Dank Euch werden viele Bürger Gilneas'' den morgigen Tag erleben.', 23420), -- 14098
(14093, 0, 0, 0, 0, 0, 0, 0, 0, 'Es hat keinen Zweck, $n! Sie lassen nicht nach.', 23420), -- 14093
(14094, 0, 0, 0, 0, 0, 0, 0, 0, 'Hervorragend! Ich werde sicherstellen, dass sie an einen sicheren Ort gebracht werden.', 23420), -- 14094
(14091, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr seid es wieder. Sieht aus, als ob Ihr niemand wärt, der im Angesicht der Gefahr den Schwanz einziehen würde. Nun dann.', 23420), -- 14091
(14078, 0, 0, 0, 0, 0, 0, 0, 0, 'Die Leiche des Mannes ist von tiefen Klauenspuren gezeichnet.', 23420), -- 14078
(26385, 6, 0, 0, 0, 0, 0, 0, 0, 'Unsere Verbündeten eilen zu unserer Hilfe? Elune sei Dank! Es war weise von Tyrande, Eurem Volk mit offenen Armen zu begegnen.$B$BWir brauchen Hilfe. Wir brauchen jede helfende Hand, die wir bekommen können.', 23420), -- 26385
(28517, 1, 0, 0, 0, 0, 0, 0, 0, 'Diese Eiche entstand mit Hilfe der Druiden aus einem Samen, der aus Gilneas kam. Sie steht für alles, was wir erlitten und alles, was wir erreicht haben.$B$BMöge sie uns stets daran erinnern, dass unser Fluch auch ein Segen ist.', 23420), -- 28517
(14434, 0, 0, 0, 0, 0, 0, 0, 0, 'Seid Ihr bereit, die Segel zu setzen, $n? Euer Volk hat im Land der Kaldorei eine Zuflucht gefunden.$B$BSeid unbesorgt, $R. Euer Volk wird die Chance haben, wieder für Gilneas zu kämpfen. Dieses Mal mit der ganzen Macht der Allianz.', 23420), -- 14434
(26706, 0, 0, 0, 0, 0, 0, 0, 0, 'Wir haben es geschafft, $n. Wir haben die Evakuierung in die Wege geleitet. Wenn wir uns bald auf den Weg machen, sieht die Flotte der Verlassenen bald nur noch eine Staubwolke von uns.', 23420), -- 26706
(24681, 66, 0, 0, 0, 0, 0, 0, 0, 'Es ist fast vorbei, $n. Nur noch ein Hindernis steht zwischen uns und unserem Überleben.', 23420), -- 24681
(24680, 0, 0, 0, 0, 0, 0, 0, 0, 'Schön, Euch wiederzusehen, $n.', 23420), -- 24680
(24679, 0, 0, 0, 0, 0, 0, 0, 0, 'Es ist vollbracht, $n. Die Toten versinken wieder in ewigen Schlaf.', 23420), -- 24679
(24602, 0, 0, 0, 0, 0, 0, 0, 0, 'Danke, $n. Hoffen wir, dass es funktioniert.', 23420); -- 24602


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text    xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_request_items` WHERE `ID` IN (24675 /*24675*/, 24592 /*24592*/, 24646 /*24646*/, 24628 /*24628*/, 24616 /*24616*/, 24495 /*24495*/, 24501 /*24501*/, 24472 /*24472*/, 14401 /*14401*/, 14400 /*14400*/, 14399 /*14399*/, 14404 /*14404*/, 14416 /*14416*/, 14395 /*14395*/, 14386 /*14386*/, 14204 /*14204*/, 14154 /*14154*/, 14094 /*14094*/, 24602 /*24602*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(24675, 0, 0, 0, 0, 'Habt Ihr bekommen, was ich brauche, $n?', 23420), -- 24675
(24592, 0, 0, 0, 0, 'Seid vorsichtig, $n.', 23420), -- 24592
(24646, 0, 0, 0, 0, 'Habt Ihr die Sichel, $n?', 23420), -- 24646
(24628, 0, 0, 0, 0, 'Ihr seid zurückgekehrt, $n.', 23420), -- 24628
(24616, 0, 0, 0, 0, 'Ist es vollbracht, $n?', 23420), -- 24616
(24495, 0, 0, 0, 0, 'Hattet Ihr schon Erfolg bei der Suche nach diesen Seiten?', 23420), -- 24495
(24501, 0, 0, 0, 0, 'Habt Ihr Rygna schon vernichtet, $n?', 23420), -- 24501
(24472, 0, 0, 0, 0, 'Wie ist es gelaufen, $n?', 23420), -- 24472
(14401, 0, 0, 0, 0, 'Ihr seid so $gein bezaubernder junger Mann:eine bezaubernde junge Dame;, $n.', 23420), -- 14401
(14400, 0, 0, 0, 0, 'Habt Ihr alles gefunden, Schätzchen?', 23420), -- 14400
(14399, 0, 0, 0, 0, 'Wie läuft es, Schätzchen? Bleibt Ihr zum Tee?', 23420), -- 14399
(14404, 0, 0, 0, 0, 'Habt Ihr mein Material, $n?', 23420), -- 14404
(14416, 0, 0, 0, 0, 'Ihr seid zurück, $n.', 23420), -- 14416
(14395, 0, 0, 0, 0, 'Es sind nicht mehr viele von uns übrig, $n. Wir müssen uns um die letzten verbliebenen Gilneer kümmern.', 23420), -- 14395
(14386, 0, 0, 0, 0, 'Ist es schon vollbracht, $n?', 23420), -- 14386
(14204, 0, 0, 0, 0, 'Ist die Aufgabe erledigt, $n?', 23420), -- 14204
(14154, 0, 0, 0, 0, 'Wir haben''s gleich! Vorsichtig, oder wir verlieren ihn!', 23420), -- 14154
(14094, 0, 0, 0, 0, 'Habt Ihr die Vorräte geborgen, $n?', 23420), -- 14094
(24602, 0, 0, 0, 0, 'Ihr seid zurück, $n.', 23420); -- 24602


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=83 AND `id`=0) OR (`menu_id`=12693 AND `id`=0) OR (`menu_id`=11061 AND `id`=0) OR (`menu_id`=10837 AND `id`=0) OR (`menu_id`=10841 AND `id`=0) OR (`menu_id`=9821 AND `id`=2) OR (`menu_id`=9821 AND `id`=1) OR (`menu_id`=11079 AND `id`=0) OR (`menu_id`=11672 AND `id`=0) OR (`menu_id`=5665 AND `id`=0) OR (`menu_id`=12180 AND `id`=12) OR (`menu_id`=12180 AND `id`=11) OR (`menu_id`=12188 AND `id`=15) OR (`menu_id`=12188 AND `id`=14) OR (`menu_id`=12188 AND `id`=13) OR (`menu_id`=12188 AND `id`=12) OR (`menu_id`=12188 AND `id`=11) OR (`menu_id`=12185 AND `id`=14) OR (`menu_id`=12185 AND `id`=13) OR (`menu_id`=12185 AND `id`=12) OR (`menu_id`=12185 AND `id`=11) OR (`menu_id`=12189 AND `id`=16) OR (`menu_id`=12189 AND `id`=15) OR (`menu_id`=12189 AND `id`=14) OR (`menu_id`=12189 AND `id`=13) OR (`menu_id`=12189 AND `id`=12) OR (`menu_id`=12189 AND `id`=11) OR (`menu_id`=12190 AND `id`=14) OR (`menu_id`=12190 AND `id`=13) OR (`menu_id`=12190 AND `id`=12) OR (`menu_id`=12190 AND `id`=11) OR (`menu_id`=12186 AND `id`=19) OR (`menu_id`=12186 AND `id`=18) OR (`menu_id`=12186 AND `id`=17) OR (`menu_id`=12186 AND `id`=16) OR (`menu_id`=12186 AND `id`=15) OR (`menu_id`=12186 AND `id`=14) OR (`menu_id`=12186 AND `id`=13) OR (`menu_id`=12186 AND `id`=12) OR (`menu_id`=12186 AND `id`=11) OR (`menu_id`=12199 AND `id`=14) OR (`menu_id`=12199 AND `id`=13) OR (`menu_id`=12199 AND `id`=12) OR (`menu_id`=12199 AND `id`=11) OR (`menu_id`=12198 AND `id`=14) OR (`menu_id`=12198 AND `id`=13) OR (`menu_id`=12198 AND `id`=12) OR (`menu_id`=12198 AND `id`=11) OR (`menu_id`=12197 AND `id`=14) OR (`menu_id`=12197 AND `id`=13) OR (`menu_id`=12197 AND `id`=12) OR (`menu_id`=12197 AND `id`=11) OR (`menu_id`=12196 AND `id`=14) OR (`menu_id`=12196 AND `id`=13) OR (`menu_id`=12196 AND `id`=12) OR (`menu_id`=12196 AND `id`=11) OR (`menu_id`=12195 AND `id`=14) OR (`menu_id`=12195 AND `id`=13) OR (`menu_id`=12195 AND `id`=12) OR (`menu_id`=12195 AND `id`=11) OR (`menu_id`=12193 AND `id`=14) OR (`menu_id`=12193 AND `id`=13) OR (`menu_id`=12193 AND `id`=12) OR (`menu_id`=12193 AND `id`=11) OR (`menu_id`=12192 AND `id`=14) OR (`menu_id`=12192 AND `id`=13) OR (`menu_id`=12192 AND `id`=12) OR (`menu_id`=12192 AND `id`=11) OR (`menu_id`=12191 AND `id`=15) OR (`menu_id`=12191 AND `id`=14) OR (`menu_id`=12191 AND `id`=13) OR (`menu_id`=12191 AND `id`=12) OR (`menu_id`=12191 AND `id`=11) OR (`menu_id`=10842 AND `id`=0) OR (`menu_id`=5855 AND `id`=0) OR (`menu_id`=5853 AND `id`=0) OR (`menu_id`=4302 AND `id`=0) OR (`menu_id`=12677 AND `id`=2) OR (`menu_id`=12677 AND `id`=1) OR (`menu_id`=12677 AND `id`=0) OR (`menu_id`=12609 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(83, 0, '', '', 'Bringt mich ins Leben zurück.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12693, 0, '', '', 'Wir müssen das nochmal versuchen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11061, 0, '', '', 'Wir werden uns unsere Stadt zurückholen!', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10837, 0, '', '', 'Ich wünsche weitere Ausbildung.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10841, 0, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9821, 2, '', '', 'Ich würde gern meine Kampfhaustiere heilen und wiederbeleben.', '', '', '', '', '', '', '', '', '', '', '', 'Es wird eine kleine Gebühr für die medizinische Hilfe erhoben.', ''),
(9821, 1, '', '', 'Ich suche nach einem verlorenen Gefährten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11079, 0, '', '', 'Ich benötige ein Pferd.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11672, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5665, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12180, 12, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12180, 11, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12188, 15, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12188, 14, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12188, 13, '', '', 'Erzählt mir mehr über die Inschriftenkunde.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12188, 12, '', '', 'Erzählt mir mehr über Alchemie.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12188, 11, '', '', 'Unterrichtet mich in Kräuterkunde.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12185, 14, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12185, 13, '', '', 'Erzählt mir mehr über das Kürschnern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12185, 12, '', '', 'Erzählt mir mehr über Bergbau.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12185, 11, '', '', 'Erzählt mir mehr über Kräuterkunde.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12189, 16, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12189, 15, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12189, 14, '', '', 'Erzählt mir mehr über die Juwelierskunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12189, 13, '', '', 'Erzählt mir mehr über die Ingenieurskunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12189, 12, '', '', 'Erzählt mir mehr über die Schmiedekunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12189, 11, '', '', 'Unterrichtet mich in Bergbau.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12190, 14, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12190, 13, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12190, 12, '', '', 'Erzählt mir mehr über Lederverarbeitung.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12190, 11, '', '', 'Unterrichtet mich im Kürschnern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12186, 19, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12186, 18, '', '', 'Erzählt mir mehr über das Schneidern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12186, 17, '', '', 'Erzählt mir mehr über Lederverarbeitung.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12186, 16, '', '', 'Erzählt mir mehr über die Juwelierskunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12186, 15, '', '', 'Erzählt mir mehr über die Inschriftenkunde.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12186, 14, '', '', 'Erzählt mir mehr über die Ingenieurskunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12186, 13, '', '', 'Erzählt mir mehr über die Verzauberkunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12186, 12, '', '', 'Erzählt mir mehr über die Schmiedekunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12186, 11, '', '', 'Erzählt mir mehr über Alchemie.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12199, 14, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12199, 13, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12199, 12, '', '', 'Erzählt mir mehr über die Verzauberkunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12199, 11, '', '', 'Unterrichtet mich im Schneidern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12198, 14, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12198, 13, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12198, 12, '', '', 'Erzählt mir mehr über das Kürschnern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12198, 11, '', '', 'Unterrichtet mich in Lederverarbeitung.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12197, 14, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12197, 13, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12197, 12, '', '', 'Erzählt mir mehr über Bergbau.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12197, 11, '', '', 'Unterrichtet mich in der Juwelierskunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12196, 14, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12196, 13, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12196, 12, '', '', 'Erzählt mir mehr über Kräuterkunde.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12196, 11, '', '', 'Unterrichtet mich in Inschriftenkunde.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12195, 14, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12195, 13, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12195, 12, '', '', 'Erzählt mir mehr über Bergbau.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12195, 11, '', '', 'Unterrichtet mich in der Ingenieurskunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12193, 14, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12193, 13, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12193, 12, '', '', 'Erzählt mir mehr über das Schneidern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12193, 11, '', '', 'Unterrichtet mich in der Verzauberkunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12192, 14, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12192, 13, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12192, 12, '', '', 'Erzählt mir mehr über Bergbau.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12192, 11, '', '', 'Unterrichtet mich in der Schmiedekunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12191, 15, '', '', 'Erzählt mir etwas über Produktionsberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12191, 14, '', '', 'Erzählt mir etwas über Sammelberufe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12191, 13, '', '', 'Erzählt mir mehr über die Inschriftenkunde.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12191, 12, '', '', 'Erzählt mir mehr über Kräuterkunde.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12191, 11, '', '', 'Unterrichtet mich in Alchemie.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10842, 0, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5855, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5853, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4302, 0, '', '', 'Ich brauche einen Flug.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12677, 2, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12677, 1, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12677, 0, '', '', 'Ich kann mich nicht mehr gut daran erinnern, wie ich nach Dämmerhafen gekommen bin. Erzählt mir, was Ihr wisst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12609, 0, '', '', 'Ich bin bereit.', '', '', '', '', '', '', '', '', '', '', '', '', '');


DELETE FROM `locales_creature_text` WHERE (`entry`=38051 AND `groupid`=0) OR (`entry`=37873 AND `groupid`=0) OR (`entry`=38474 AND `groupid`=0) OR (`entry`=50474 AND `groupid`=0) OR (`entry`=38537 AND `groupid`=0) OR (`entry`=36132 AND `groupid`=0) OR (`entry`=36283 AND `groupid`=0) OR (`entry`=37875 AND `groupid`=0) OR (`entry`=35229 AND `groupid`=0) OR (`entry`=38617 AND `groupid`=0) OR (`entry`=37735 AND `groupid`=0) OR (`entry`=37953 AND `groupid`=0) OR (`entry`=3787 AND `groupid`=0) OR (`entry`=35907 AND `groupid`=0) OR (`entry`=34981 AND `groupid`=0) OR (`entry`=37701 AND `groupid`=0) OR (`entry`=36332 AND `groupid`=0) OR (`entry`=38507 AND `groupid`=0) OR (`entry`=38768 AND `groupid`=0) OR (`entry`=3847 AND `groupid`=0) OR (`entry`=38415 AND `groupid`=0) OR (`entry`=38766 AND `groupid`=0) OR (`entry`=35463 AND `groupid`=0) OR (`entry`=3629 AND `groupid`=0) OR (`entry`=38218 AND `groupid`=0) OR (`entry`=34884 AND `groupid`=0) OR (`entry`=34571 AND `groupid`=0) OR (`entry`=36231 AND `groupid`=0) OR (`entry`=35906 AND `groupid`=0) OR (`entry`=36294 AND `groupid`=0) OR (`entry`=43727 AND `groupid`=0) OR (`entry`=38533 AND `groupid`=0) OR (`entry`=38553 AND `groupid`=0) OR (`entry`=50902 AND `groupid`=0) OR (`entry`=34851 AND `groupid`=0) OR (`entry`=36287 AND `groupid`=0) OR (`entry`=36288 AND `groupid`=0) OR (`entry`=43566 AND `groupid`=0) OR (`entry`=36331 AND `groupid`=0) OR (`entry`=38331 AND `groupid`=0) OR (`entry`=33115 AND `groupid`=0) OR (`entry`=50893 AND `groupid`=0) OR (`entry`=36291 AND `groupid`=0) OR (`entry`=37694 AND `groupid`=0) OR (`entry`=35112 AND `groupid`=0) OR (`entry`=35456 AND `groupid`=0) OR (`entry`=37195 AND `groupid`=0) OR (`entry`=35552 AND `groupid`=0) OR (`entry`=38767 AND `groupid`=0) OR (`entry`=35911 AND `groupid`=0) OR (`entry`=3523 AND `groupid`=0) OR (`entry`=36207 AND `groupid`=0) OR (`entry`=37876 AND `groupid`=0) OR (`entry`=35836 AND `groupid`=0) OR (`entry`=35627 AND `groupid`=0) OR (`entry`=36461 AND `groupid`=0) OR (`entry`=50415 AND `groupid`=0) OR (`entry`=35505 AND `groupid`=0) OR (`entry`=38469 AND `groupid`=0) OR (`entry`=32971 AND `groupid`=0) OR (`entry`=36289 AND `groupid`=0) OR (`entry`=3853 AND `groupid`=0) OR (`entry`=35115 AND `groupid`=0) OR (`entry`=36814 AND `groupid`=0) OR (`entry`=35118 AND `groupid`=0) OR (`entry`=3633 AND `groupid`=0) OR (`entry`=3555 AND `groupid`=0) OR (`entry`=3644 AND `groupid`=0) OR (`entry`=51409 AND `groupid`=0) OR (`entry`=38426 AND `groupid`=0) OR (`entry`=50881 AND `groupid`=0) OR (`entry`=34913 AND `groupid`=0) OR (`entry`=3654 AND `groupid`=0) OR (`entry`=38781 AND `groupid`=0) OR (`entry`=36458 AND `groupid`=0) OR (`entry`=35188 AND `groupid`=0) OR (`entry`=38029 AND `groupid`=0) OR (`entry`=37065 AND `groupid`=0); 
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(38051, 0, 0, '', '', 'Darius! Die dunklen Waldläufer haben die Sichel! Sie haben sie vor uns erreicht.', '', '', '', '', ''),
(37873, 0, 0, '', '', 'So wie Daral''nir die verfluchten Druiden besänftigt, die der Bestie in ihnen die Macht überlassen und das Geichgewicht vernachlässigen, so lasst Tal''doren $n besänftigen.', '', '', '', '', ''),
(38474, 0, 0, '', '', 'VATER!!!', '', '', '', '', ''),
(50474, 0, 0, '', '', 'Sie verwandeln unser Volk in MONSTER!!', '', '', '', '', ''),
(38537, 0, 0, '', '', 'Herrin! Soll ich meinen Männern befehlen, den Einsatz der Seuche einzustellen? Oder fahren wir fort wie geplant?', '', '', '', '', ''),
(36132, 0, 0, '', '', 'Seid vorsichtig. Erwartet nicht, dass Euch die Leute jetzt schon in ihre Häuser einladen werden. Aber wenigstens werden sie nicht auf Euch schießen.', '', '', '', '', ''),
(36283, 0, 0, '', '', 'Ihr müsst zur Abendnebelküste zurückkehren!', '', '', '', '', ''),
(37875, 0, 0, '', '', 'Nein... Eher sterbe ich, als einen von Euch als meinen König anzuerkennen!', '', '', '', '', ''),
(35229, 0, 0, '', '', '%s gerät in Raserei!', '', '', '', '', ''),
(38617, 0, 0, '', '', 'Stellt sicher, dass alles bereit ist. Der orcische Abgesandte ist bereit, Sylvanas in der Kathedrale zu treffen.', '', '', '', '', ''),
(37735, 0, 0, '', '', '%s wird wütend!', '', '', '', '', ''),
(37953, 0, 0, '', '', 'Welch leichte Beute. Sylvanas wird sehr erfreut sein!', '', '', '', '', ''),
(37870, 0, 0, '', '', 'So wie Goldrinns Geist einst unsere Druiden segnete, so lasst $n gesegnet sein mit der Weisheit $gseines:ihres; Volkes und der Wildheit des Wolfgotts.', '', '', '', '', ''),
(35907, 0, 0, '', '', 'Danke! Ich schulde Euch mein Leben.', '', '', '', '', ''),
(34981, 0, 0, '', '', 'Hier ist es nicht sicher. Verschwinden wir von hier!', '', '', '', '', ''),
(37701, 0, 0, '', '', 'Arbeitet härter, Ihr Würmer! Dieses Erz wird sich nicht selbst abbauen!', '', '', '', '', ''),
(36332, 0, 0, '', '', 'Sagt mir, Godfrey, diejenigen, die in der Stadt blieben, damit wir überleben konnten... Folgten sie dem Protokoll?', '', '', '', '', ''),
(38507, 0, 0, '', '', 'Lasst uns sofort aufbrechen. Wir werden Sylvanas aufspüren. Für Gilneas!', '', '', '', '', ''),
(38768, 0, 0, '', '', 'Vater!', '', '', '', '', ''),
(38470, 0, 0, '', '', 'Versperr ihnen den Rückweg, Liam! Sie sind genau da, wo wir sie haben wollen!', '', '', '', '', ''),
(38415, 0, 0, '', '', 'Er ist zu stark! Benutzt die Katapulte!', '', '', '', '', ''),
(38766, 0, 0, '', '', 'Crowley! Euch und Euren elfischen Verbündeten ergeht hiermit der Befehl, an der Seite der Armee des Königs zu kämpfen. Verflucht oder nicht, Ihr unterliegt noch immer den Gesetzen Gilneas''!', '', '', '', '', ''),
(35463, 0, 0, '', '', '%s wird wütend!', '', '', '', '', ''),
(36290, 0, 0, '', '', 'Haltet die Stellung, Männer!', '', '', '', '', ''),
(38218, 0, 0, '', '', 'Angriff!', '', '', '', '', ''),
(34884, 0, 0, '', '', '%s wird wütend!', '', '', '', '', ''),
(34571, 0, 0, '', '', 'Willkommen zurück, $n. Ihr habt Glück gehabt. Krennans Behandlungen funktionieren nicht immer so gut bei Leuten, auf denen so lange der Fluch liegt, wie bei Euch.', '', '', '', '', ''),
(36231, 0, 0, '', '', 'Fass riechen nach Schießpulver...', '', '', '', '', ''),
(35906, 0, 0, '', '', 'Wir haben Aranas! Feuer frei!', '', '', '', '', ''),
(36294, 0, 0, '', '', 'Wer wagt es, Koroths Banner anzufassen?', '', '', '', '', ''),
(43727, 0, 0, '', '', 'Alle an Bord der Schiffe, sofort!', '', '', '', '', ''),
(38533, 0, 0, '', '', 'Es scheint so, als würdet Ihr die Kontrolle über Gilneas verlieren, Sylvanas. Garrosh fürchtet, dass er die Invasion selbst ausführen muss.', '', '', '', '', ''),
(38553, 0, 0, '', '', 'Es ist an der Zeit, sich ins Gefecht zu stürzen, $n! Ihr an unserer Seite werdet das Zünglein an der Waage sein in diesem Kampf!', '', '', '', '', ''),
(50902, 0, 0, '', '', 'Gilneas wird sich für immer an Euren Mut erinnern, Liam.', '', '', '', '', ''),
(34851, 0, 0, '', '', 'Was... Was sind diese Dinger da auf den Dächern?', '', '', '', '', ''),
(36287, 0, 0, '', '', 'Ihr macht mir Angst! Ich will zu meiner Mami!', '', '', '', '', ''),
(36288, 0, 0, '', '', 'Seid Ihr einer von den guten Worgen, $gFremder:Fremde;? Habt Ihr Cynthia gesehen? Sie versteckt sich bei den Hütten draußen.', '', '', '', '', ''),
(43566, 0, 0, '', '', 'Sichert die Sparren!', '', '', '', '', ''),
(36331, 0, 0, '', '', 'Ich werde Euch nicht aufgeben, $n. Noch habe ich kein Heilmittel... aber es gibt Behandlungen. Ihr werdet die Kontrolle zurückbekommen.', '', '', '', '', ''),
(38331, 0, 0, '', '', 'Faulblut zerquetscht mickrige Worgen!!', '', '', '', '', ''),
(33115, 0, 0, '', '', 'Priesterinnen, ich bringe Euch Nachricht, dass noch mehr Überlebende kommen. Sie tun alles was sie können, um nach jenen zu suchen, die noch am Leben sind. Bitte schickt alle Hilfe, die Ihr geben könnt!', '', '', '', '', ''),
(50893, 0, 0, '', '', 'Möge das Licht die Geister unserer Vorfahren segnen, denn sie haben erlaubt, dass mein Sohn in diesem heiligen Boden ruhen darf.', '', '', '', '', ''),
(36291, 0, 0, '', '', 'Was ist los?!', '', '', '', '', ''),
(37694, 0, 0, '', '', 'Die Verlassenen werden für das bezahlen, was sie getan haben!', '', '', '', '', ''),
(35112, 0, 0, '', '', 'Genug! Ich habe mich entschieden. Wir brauchen Crowley auf unserer Seite.', '', '', '', '', ''),
(35456, 0, 0, '', '', '%s gerät in Raserei!', '', '', '', '', ''),
(37195, 0, 0, '', '', 'Bringt unsere Männer sofort in Stellung, Tobias. Wir dürfen die Sichel nicht in die Hände der Verlassenen gelangen lassen!', '', '', '', '', ''),
(35552, 0, 0, '', '', 'Wir müssen die Aufmerksamkeit der Worgen auf die Stadt lenken, Genn. Das ist die einzige Chance für die Überlebenden, Dämmerhafen zu erreichen.', '', '', '', '', ''),
(38767, 0, 0, '', '', 'Weder noch, alter Freund. Ich komme als Gleichgestellter.', '', '', '', '', ''),
(35911, 0, 0, '', '', 'Wenn wir es hinter die Tore von Dämmerhafen schaffen, sind wir in Sicherheit. Die östlichen Berge sind praktisch unpassierbar.', '', '', '', '', ''),
(35230, 0, 0, '', '', 'Lasst uns so viele wie möglich zu uns locken. Jeder Worgen, der hinter uns her ist, ist einer weniger, der die Überlebenden jagt.', '', '', '', '', ''),
(36207, 0, 0, '', '', 'Gilneas wird schon bald den Verlassenen gehören!', '', '', '', '', ''),
(37876, 0, 0, '', '', 'Es ist vorbei, Godfrey. Ihr habt keine Unterstützung mehr unter den Fürsten des Ostens.', '', '', '', '', ''),
(35836, 0, 0, '', '', 'Da ist einer hinter mir her!', '', '', '', '', ''),
(35627, 0, 0, '', '', '%s gerät in Raserei!', '', '', '', '', ''),
(36461, 0, 0, '', '', 'Ich werde diese Katze an mich nehmen. Sie scheint der perfekte Köder zu sein. Macht Euch bereit zu sterben, Ihr Narr!', '', '', '', '', ''),
(50415, 0, 0, '', '', 'Ihr seid von einem Worgen gebissen worden. Wahrscheinlich ist es nicht weiter schlimm, aber es tut schon ein wenig weh.$B$B|TInterface\\Icons\\INV_Misc_monsterfang_02.blp:32|t', '', '', '', '', ''),
(35505, 0, 0, '', '', '%s gerät in Raserei!', '', '', '', '', ''),
(38469, 0, 0, '', '', 'Genug!', '', '', '', '', ''),
(32971, 0, 0, '', '', 'Ihr da! Wir brauchen so schnell wie möglich brauchbare Leute. Es hängen Leben davon ab!', '', '', '', '', ''),
(36289, 0, 0, '', '', 'Tut mir nicht weh! Ich habe nur nach meinen Schwestern gesucht! Ich glaube, dass Ashley in dem Haus da ist!', '', '', '', '', ''),
(38530, 0, 0, '', '', 'Ihr könnt Garrosh versichern, dass dies nur ein kleiner Rückschlag ist. Unser Sieg in Gilneas wird endgültig sein.', '', '', '', '', ''),
(35115, 0, 0, '', '', 'Wir sollten Crowley mit diesen Bestien im Gefängnis zurücklassen!', '', '', '', '', ''),
(36814, 0, 0, '', '', 'Lasst die Sichel das lösen, was nicht verbunden sein sollte! Lasst die Seele die Bestie beherrschen, auf dass die Bestie nicht über die Seele herrsche!', '', '', '', '', ''),
(35118, 0, 0, '', '', '%s gerät in Raserei!', '', '', '', '', ''),
(36330, 0, 0, '', '', 'Hört auf, Krennan. Es ist an der Zeit, $gihn:sie; einzuschläfern. So will es das Protokoll.', '', '', '', '', ''),
(35550, 0, 0, '', '', 'Feuer!', '', '', '', '', ''),
(36440, 0, 0, '', '', 'Das Land hat einfach unter unseren Füßen nachgegeben... Ich dachte, ich wäre tot. Dafür schulde ich Euch was.', '', '', '', '', ''),
(51409, 0, 0, '', '', 'Passt auf! Wir werden angegriffen!', '', '', '', '', ''),
(38426, 0, 0, '', '', 'Als Dank für die Hilfe in Glutstein haben uns die Bewohner eine Kleinigkeit gebracht, die uns im Kampf gegen die Verlassenen helfen wird!', '', '', '', '', ''),
(50881, 0, 0, '', '', 'Du warst wahrlich einer des Volkes, Liam. Ganz anders als jeder andere Adelige. Dafür werden sie bezahlen.', '', '', '', '', ''),
(34913, 0, 0, '', '', 'Drängt sie zurück!', '', '', '', '', ''),
(36540, 0, 0, '', '', 'Ihr könnt das Gebirgspferd nur in Crowleys Stallungen und Crowleys Obstgarten reiten.', '', '', '', '', ''),
(38781, 0, 0, '', '', 'Ihr habt es geschafft! Wir sind gerettet!', '', '', '', '', ''),
(36458, 0, 0, '', '', 'Mit meinem Kätzchen stellt Ihr keine Schweinereien an - Ihr Sohn einer Promenadenmischung!', '', '', '', '', ''),
(35188, 0, 0, '', '', '%s gerät in Raserei!', '', '', '', '', ''),
(38029, 0, 0, '', '', 'Haltet sie beschäftigt, meine Brüder! Ermöglicht es $n, die Sichel zurückzugewinnen!', '', '', '', '', ''),
(37065, 0, 0, '', '', 'Genau zur richtigen Zeit, $n. Hier kommen die Verlassenen.', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=38781 /*38781*/ AND `locale`='deDE') OR (`entry`=38144 /*38144*/ AND `locale`='deDE') OR (`entry`=37885 /*37885*/ AND `locale`='deDE') OR (`entry`=37884 /*37884*/ AND `locale`='deDE') OR (`entry`=38365 /*38365*/ AND `locale`='deDE') OR (`entry`=38364 /*38364*/ AND `locale`='deDE') OR (`entry`=38389 /*38389*/ AND `locale`='deDE') OR (`entry`=38363 /*38363*/ AND `locale`='deDE') OR (`entry`=38344 /*38344*/ AND `locale`='deDE') OR (`entry`=38287 /*38287*/ AND `locale`='deDE') OR (`entry`=38150 /*38150*/ AND `locale`='deDE') OR (`entry`=38615 /*38615*/ AND `locale`='deDE') OR (`entry`=37892 /*37892*/ AND `locale`='deDE') OR (`entry`=44388 /*44388*/ AND `locale`='deDE') OR (`entry`=37891 /*37891*/ AND `locale`='deDE') OR (`entry`=37889 /*37889*/ AND `locale`='deDE') OR (`entry`=38618 /*38618*/ AND `locale`='deDE') OR (`entry`=38617 /*38617*/ AND `locale`='deDE') OR (`entry`=38616 /*38616*/ AND `locale`='deDE') OR (`entry`=38614 /*38614*/ AND `locale`='deDE') OR (`entry`=38539 /*38539*/ AND `locale`='deDE') OR (`entry`=39017 /*39017*/ AND `locale`='deDE') OR (`entry`=39016 /*39016*/ AND `locale`='deDE') OR (`entry`=39015 /*39015*/ AND `locale`='deDE') OR (`entry`=38474 /*38474*/ AND `locale`='deDE') OR (`entry`=38613 /*38613*/ AND `locale`='deDE') OR (`entry`=38611 /*38611*/ AND `locale`='deDE') OR (`entry`=38470 /*38470*/ AND `locale`='deDE') OR (`entry`=38473 /*38473*/ AND `locale`='deDE') OR (`entry`=38469 /*38469*/ AND `locale`='deDE') OR (`entry`=38331 /*38331*/ AND `locale`='deDE') OR (`entry`=38415 /*38415*/ AND `locale`='deDE') OR (`entry`=38348 /*38348*/ AND `locale`='deDE') OR (`entry`=38426 /*38426*/ AND `locale`='deDE') OR (`entry`=38425 /*38425*/ AND `locale`='deDE') OR (`entry`=38424 /*38424*/ AND `locale`='deDE') OR (`entry`=38464 /*38464*/ AND `locale`='deDE') OR (`entry`=38420 /*38420*/ AND `locale`='deDE') OR (`entry`=38210 /*38210*/ AND `locale`='deDE') OR (`entry`=38192 /*38192*/ AND `locale`='deDE') OR (`entry`=28332 /*28332*/ AND `locale`='deDE') OR (`entry`=38377 /*38377*/ AND `locale`='deDE') OR (`entry`=38218 /*38218*/ AND `locale`='deDE') OR (`entry`=38465 /*38465*/ AND `locale`='deDE') OR (`entry`=38466 /*38466*/ AND `locale`='deDE') OR (`entry`=38221 /*38221*/ AND `locale`='deDE') OR (`entry`=38467 /*38467*/ AND `locale`='deDE') OR (`entry`=38468 /*38468*/ AND `locale`='deDE') OR (`entry`=37686 /*37686*/ AND `locale`='deDE') OR (`entry`=37685 /*37685*/ AND `locale`='deDE') OR (`entry`=37692 /*37692*/ AND `locale`='deDE') OR (`entry`=37694 /*37694*/ AND `locale`='deDE') OR (`entry`=37701 /*37701*/ AND `locale`='deDE') OR (`entry`=24042 /*24042*/ AND `locale`='deDE') OR (`entry`=37802 /*37802*/ AND `locale`='deDE') OR (`entry`=38553 /*38553*/ AND `locale`='deDE') OR (`entry`=38143 /*38143*/ AND `locale`='deDE') OR (`entry`=37803 /*37803*/ AND `locale`='deDE') OR (`entry`=37783 /*37783*/ AND `locale`='deDE') OR (`entry`=42853 /*42853*/ AND `locale`='deDE') OR (`entry`=37784 /*37784*/ AND `locale`='deDE') OR (`entry`=883 /*883*/ AND `locale`='deDE') OR (`entry`=37786 /*37786*/ AND `locale`='deDE') OR (`entry`=37785 /*37785*/ AND `locale`='deDE') OR (`entry`=1933 /*1933*/ AND `locale`='deDE') OR (`entry`=37718 /*37718*/ AND `locale`='deDE') OR (`entry`=38764 /*38764*/ AND `locale`='deDE') OR (`entry`=37876 /*37876*/ AND `locale`='deDE') OR (`entry`=37875 /*37875*/ AND `locale`='deDE') OR (`entry`=37716 /*37716*/ AND `locale`='deDE') OR (`entry`=37733 /*37733*/ AND `locale`='deDE') OR (`entry`=37735 /*37735*/ AND `locale`='deDE') OR (`entry`=37874 /*37874*/ AND `locale`='deDE') OR (`entry`=38029 /*38029*/ AND `locale`='deDE') OR (`entry`=38022 /*38022*/ AND `locale`='deDE') OR (`entry`=37873 /*37873*/ AND `locale`='deDE') OR (`entry`=37870 /*37870*/ AND `locale`='deDE') OR (`entry`=37195 /*37195*/ AND `locale`='deDE') OR (`entry`=36814 /*36814*/ AND `locale`='deDE') OR (`entry`=42953 /*42953*/ AND `locale`='deDE') OR (`entry`=37489 /*37489*/ AND `locale`='deDE') OR (`entry`=37197 /*37197*/ AND `locale`='deDE') OR (`entry`=37822 /*37822*/ AND `locale`='deDE') OR (`entry`=37045 /*37045*/ AND `locale`='deDE') OR (`entry`=50570 /*50570*/ AND `locale`='deDE') OR (`entry`=36813 /*36813*/ AND `locale`='deDE') OR (`entry`=38797 /*38797*/ AND `locale`='deDE') OR (`entry`=41561 /*41561*/ AND `locale`='deDE') OR (`entry`=38795 /*38795*/ AND `locale`='deDE') OR (`entry`=43558 /*43558*/ AND `locale`='deDE') OR (`entry`=38798 /*38798*/ AND `locale`='deDE') OR (`entry`=37815 /*37815*/ AND `locale`='deDE') OR (`entry`=38799 /*38799*/ AND `locale`='deDE') OR (`entry`=38796 /*38796*/ AND `locale`='deDE') OR (`entry`=38792 /*38792*/ AND `locale`='deDE') OR (`entry`=37102 /*37102*/ AND `locale`='deDE') OR (`entry`=38794 /*38794*/ AND `locale`='deDE') OR (`entry`=37499 /*37499*/ AND `locale`='deDE') OR (`entry`=38793 /*38793*/ AND `locale`='deDE') OR (`entry`=6827 /*6827*/ AND `locale`='deDE') OR (`entry`=17467 /*17467*/ AND `locale`='deDE') OR (`entry`=37757 /*37757*/ AND `locale`='deDE') OR (`entry`=37492 /*37492*/ AND `locale`='deDE') OR (`entry`=37807 /*37807*/ AND `locale`='deDE') OR (`entry`=37806 /*37806*/ AND `locale`='deDE') OR (`entry`=37805 /*37805*/ AND `locale`='deDE') OR (`entry`=37808 /*37808*/ AND `locale`='deDE') OR (`entry`=36294 /*36294*/ AND `locale`='deDE') OR (`entry`=36293 /*36293*/ AND `locale`='deDE') OR (`entry`=36882 /*36882*/ AND `locale`='deDE') OR (`entry`=2914 /*2914*/ AND `locale`='deDE') OR (`entry`=37067 /*37067*/ AND `locale`='deDE') OR (`entry`=1420 /*1420*/ AND `locale`='deDE') OR (`entry`=38762 /*38762*/ AND `locale`='deDE') OR (`entry`=37065 /*37065*/ AND `locale`='deDE') OR (`entry`=36743 /*36743*/ AND `locale`='deDE') OR (`entry`=36962 /*36962*/ AND `locale`='deDE') OR (`entry`=36742 /*36742*/ AND `locale`='deDE') OR (`entry`=36606 /*36606*/ AND `locale`='deDE') OR (`entry`=51409 /*51409*/ AND `locale`='deDE') OR (`entry`=37946 /*37946*/ AND `locale`='deDE') OR (`entry`=43907 /*43907*/ AND `locale`='deDE') OR (`entry`=36138 /*36138*/ AND `locale`='deDE') OR (`entry`=44928 /*44928*/ AND `locale`='deDE') OR (`entry`=38755 /*38755*/ AND `locale`='deDE') OR (`entry`=43338 /*43338*/ AND `locale`='deDE') OR (`entry`=36461 /*36461*/ AND `locale`='deDE') OR (`entry`=36458 /*36458*/ AND `locale`='deDE') OR (`entry`=36671 /*36671*/ AND `locale`='deDE') OR (`entry`=36455 /*36455*/ AND `locale`='deDE') OR (`entry`=36454 /*36454*/ AND `locale`='deDE') OR (`entry`=36492 /*36492*/ AND `locale`='deDE') OR (`entry`=36491 /*36491*/ AND `locale`='deDE') OR (`entry`=36488 /*36488*/ AND `locale`='deDE') OR (`entry`=36456 /*36456*/ AND `locale`='deDE') OR (`entry`=36528 /*36528*/ AND `locale`='deDE') OR (`entry`=36459 /*36459*/ AND `locale`='deDE') OR (`entry`=36540 /*36540*/ AND `locale`='deDE') OR (`entry`=36512 /*36512*/ AND `locale`='deDE') OR (`entry`=36457 /*36457*/ AND `locale`='deDE') OR (`entry`=36452 /*36452*/ AND `locale`='deDE') OR (`entry`=1412 /*1412*/ AND `locale`='deDE') OR (`entry`=36440 /*36440*/ AND `locale`='deDE') OR (`entry`=36653 /*36653*/ AND `locale`='deDE') OR (`entry`=36451 /*36451*/ AND `locale`='deDE') OR (`entry`=36693 /*36693*/ AND `locale`='deDE') OR (`entry`=36460 /*36460*/ AND `locale`='deDE') OR (`entry`=36405 /*36405*/ AND `locale`='deDE') OR (`entry`=36396 /*36396*/ AND `locale`='deDE') OR (`entry`=36312 /*36312*/ AND `locale`='deDE') OR (`entry`=36399 /*36399*/ AND `locale`='deDE') OR (`entry`=36397 /*36397*/ AND `locale`='deDE') OR (`entry`=36287 /*36287*/ AND `locale`='deDE') OR (`entry`=36288 /*36288*/ AND `locale`='deDE') OR (`entry`=36236 /*36236*/ AND `locale`='deDE') OR (`entry`=36289 /*36289*/ AND `locale`='deDE') OR (`entry`=36690 /*36690*/ AND `locale`='deDE') OR (`entry`=36291 /*36291*/ AND `locale`='deDE') OR (`entry`=36290 /*36290*/ AND `locale`='deDE') OR (`entry`=36779 /*36779*/ AND `locale`='deDE') OR (`entry`=385 /*385*/ AND `locale`='deDE') OR (`entry`=89715 /*89715*/ AND `locale`='deDE') OR (`entry`=39660 /*39660*/ AND `locale`='deDE') OR (`entry`=4076 /*4076*/ AND `locale`='deDE') OR (`entry`=36283 /*36283*/ AND `locale`='deDE') OR (`entry`=36292 /*36292*/ AND `locale`='deDE') OR (`entry`=36231 /*36231*/ AND `locale`='deDE') OR (`entry`=36140 /*36140*/ AND `locale`='deDE') OR (`entry`=36211 /*36211*/ AND `locale`='deDE') OR (`entry`=34511 /*34511*/ AND `locale`='deDE') OR (`entry`=38881 /*38881*/ AND `locale`='deDE') OR (`entry`=36809 /*36809*/ AND `locale`='deDE') OR (`entry`=36714 /*36714*/ AND `locale`='deDE') OR (`entry`=36205 /*36205*/ AND `locale`='deDE') OR (`entry`=36200 /*36200*/ AND `locale`='deDE') OR (`entry`=38791 /*38791*/ AND `locale`='deDE') OR (`entry`=50574 /*50574*/ AND `locale`='deDE') OR (`entry`=50567 /*50567*/ AND `locale`='deDE') OR (`entry`=36630 /*36630*/ AND `locale`='deDE') OR (`entry`=36190 /*36190*/ AND `locale`='deDE') OR (`entry`=44125 /*44125*/ AND `locale`='deDE') OR (`entry`=36632 /*36632*/ AND `locale`='deDE') OR (`entry`=36717 /*36717*/ AND `locale`='deDE') OR (`entry`=36695 /*36695*/ AND `locale`='deDE') OR (`entry`=36651 /*36651*/ AND `locale`='deDE') OR (`entry`=36629 /*36629*/ AND `locale`='deDE') OR (`entry`=36713 /*36713*/ AND `locale`='deDE') OR (`entry`=36628 /*36628*/ AND `locale`='deDE') OR (`entry`=36652 /*36652*/ AND `locale`='deDE') OR (`entry`=50252 /*50252*/ AND `locale`='deDE') OR (`entry`=36631 /*36631*/ AND `locale`='deDE') OR (`entry`=34571 /*34571*/ AND `locale`='deDE') OR (`entry`=36132 /*36132*/ AND `locale`='deDE') OR (`entry`=50247 /*50247*/ AND `locale`='deDE') OR (`entry`=36453 /*36453*/ AND `locale`='deDE') OR (`entry`=14881 /*14881*/ AND `locale`='deDE') OR (`entry`=36170 /*36170*/ AND `locale`='deDE') OR (`entry`=36798 /*36798*/ AND `locale`='deDE') OR (`entry`=36698 /*36698*/ AND `locale`='deDE') OR (`entry`=36602 /*36602*/ AND `locale`='deDE') OR (`entry`=36797 /*36797*/ AND `locale`='deDE') OR (`entry`=36449 /*36449*/ AND `locale`='deDE') OR (`entry`=36286 /*36286*/ AND `locale`='deDE') OR (`entry`=36198 /*36198*/ AND `locale`='deDE') OR (`entry`=41015 /*41015*/ AND `locale`='deDE') OR (`entry`=35627 /*35627*/ AND `locale`='deDE') OR (`entry`=35566 /*35566*/ AND `locale`='deDE') OR (`entry`=44429 /*44429*/ AND `locale`='deDE') OR (`entry`=35618 /*35618*/ AND `locale`='deDE') OR (`entry`=36057 /*36057*/ AND `locale`='deDE') OR (`entry`=35551 /*35551*/ AND `locale`='deDE') OR (`entry`=44455 /*44455*/ AND `locale`='deDE') OR (`entry`=44464 /*44464*/ AND `locale`='deDE') OR (`entry`=44427 /*44427*/ AND `locale`='deDE') OR (`entry`=35911 /*35911*/ AND `locale`='deDE') OR (`entry`=44465 /*44465*/ AND `locale`='deDE') OR (`entry`=35554 /*35554*/ AND `locale`='deDE') OR (`entry`=44470 /*44470*/ AND `locale`='deDE') OR (`entry`=44468 /*44468*/ AND `locale`='deDE') OR (`entry`=44463 /*44463*/ AND `locale`='deDE') OR (`entry`=44461 /*44461*/ AND `locale`='deDE') OR (`entry`=44460 /*44460*/ AND `locale`='deDE') OR (`entry`=44459 /*44459*/ AND `locale`='deDE') OR (`entry`=35552 /*35552*/ AND `locale`='deDE') OR (`entry`=44469 /*44469*/ AND `locale`='deDE') OR (`entry`=35916 /*35916*/ AND `locale`='deDE') OR (`entry`=35229 /*35229*/ AND `locale`='deDE') OR (`entry`=35914 /*35914*/ AND `locale`='deDE') OR (`entry`=35915 /*35915*/ AND `locale`='deDE') OR (`entry`=50474 /*50474*/ AND `locale`='deDE') OR (`entry`=50471 /*50471*/ AND `locale`='deDE') OR (`entry`=6491 /*6491*/ AND `locale`='deDE') OR (`entry`=35753 /*35753*/ AND `locale`='deDE') OR (`entry`=35463 /*35463*/ AND `locale`='deDE') OR (`entry`=35505 /*35505*/ AND `locale`='deDE') OR (`entry`=35509 /*35509*/ AND `locale`='deDE') OR (`entry`=35504 /*35504*/ AND `locale`='deDE') OR (`entry`=35906 /*35906*/ AND `locale`='deDE') OR (`entry`=35550 /*35550*/ AND `locale`='deDE') OR (`entry`=38844 /*38844*/ AND `locale`='deDE') OR (`entry`=35378 /*35378*/ AND `locale`='deDE') OR (`entry`=35369 /*35369*/ AND `locale`='deDE') OR (`entry`=50371 /*50371*/ AND `locale`='deDE') OR (`entry`=35123 /*35123*/ AND `locale`='deDE') OR (`entry`=35081 /*35081*/ AND `locale`='deDE') OR (`entry`=35115 /*35115*/ AND `locale`='deDE') OR (`entry`=35112 /*35112*/ AND `locale`='deDE') OR (`entry`=35232 /*35232*/ AND `locale`='deDE') OR (`entry`=35839 /*35839*/ AND `locale`='deDE') OR (`entry`=47091 /*47091*/ AND `locale`='deDE') OR (`entry`=35912 /*35912*/ AND `locale`='deDE') OR (`entry`=35873 /*35873*/ AND `locale`='deDE') OR (`entry`=35872 /*35872*/ AND `locale`='deDE') OR (`entry`=35871 /*35871*/ AND `locale`='deDE') OR (`entry`=35870 /*35870*/ AND `locale`='deDE') OR (`entry`=35869 /*35869*/ AND `locale`='deDE') OR (`entry`=35124 /*35124*/ AND `locale`='deDE') OR (`entry`=35077 /*35077*/ AND `locale`='deDE') OR (`entry`=35457 /*35457*/ AND `locale`='deDE') OR (`entry`=34936 /*34936*/ AND `locale`='deDE') OR (`entry`=34913 /*34913*/ AND `locale`='deDE') OR (`entry`=33175 /*33175*/ AND `locale`='deDE') OR (`entry`=3841 /*3841*/ AND `locale`='deDE') OR (`entry`=32974 /*32974*/ AND `locale`='deDE') OR (`entry`=32973 /*32973*/ AND `locale`='deDE') OR (`entry`=32972 /*32972*/ AND `locale`='deDE') OR (`entry`=32936 /*32936*/ AND `locale`='deDE') OR (`entry`=43431 /*43431*/ AND `locale`='deDE') OR (`entry`=43420 /*43420*/ AND `locale`='deDE') OR (`entry`=33359 /*33359*/ AND `locale`='deDE') OR (`entry`=49939 /*49939*/ AND `locale`='deDE') OR (`entry`=49923 /*49923*/ AND `locale`='deDE') OR (`entry`=43439 /*43439*/ AND `locale`='deDE') OR (`entry`=32979 /*32979*/ AND `locale`='deDE') OR (`entry`=32978 /*32978*/ AND `locale`='deDE') OR (`entry`=32977 /*32977*/ AND `locale`='deDE') OR (`entry`=32971 /*32971*/ AND `locale`='deDE') OR (`entry`=51997 /*51997*/ AND `locale`='deDE') OR (`entry`=43428 /*43428*/ AND `locale`='deDE') OR (`entry`=11037 /*11037*/ AND `locale`='deDE') OR (`entry`=43429 /*43429*/ AND `locale`='deDE') OR (`entry`=43424 /*43424*/ AND `locale`='deDE') OR (`entry`=33313 /*33313*/ AND `locale`='deDE') OR (`entry`=63084 /*63084*/ AND `locale`='deDE') OR (`entry`=63083 /*63083*/ AND `locale`='deDE') OR (`entry`=49968 /*49968*/ AND `locale`='deDE') OR (`entry`=49963 /*49963*/ AND `locale`='deDE') OR (`entry`=49940 /*49940*/ AND `locale`='deDE') OR (`entry`=49927 /*49927*/ AND `locale`='deDE') OR (`entry`=32912 /*32912*/ AND `locale`='deDE') OR (`entry`=10085 /*10085*/ AND `locale`='deDE') OR (`entry`=4187 /*4187*/ AND `locale`='deDE') OR (`entry`=43436 /*43436*/ AND `locale`='deDE') OR (`entry`=34056 /*34056*/ AND `locale`='deDE') OR (`entry`=721 /*721*/ AND `locale`='deDE') OR (`entry`=49942 /*49942*/ AND `locale`='deDE') OR (`entry`=33864 /*33864*/ AND `locale`='deDE') OR (`entry`=32969 /*32969*/ AND `locale`='deDE') OR (`entry`=64375 /*64375*/ AND `locale`='deDE') OR (`entry`=32935 /*32935*/ AND `locale`='deDE') OR (`entry`=32928 /*32928*/ AND `locale`='deDE') OR (`entry`=6145 /*6145*/ AND `locale`='deDE') OR (`entry`=33115 /*33115*/ AND `locale`='deDE') OR (`entry`=25018 /*25018*/ AND `locale`='deDE') OR (`entry`=25015 /*25015*/ AND `locale`='deDE') OR (`entry`=25011 /*25011*/ AND `locale`='deDE') OR (`entry`=25009 /*25009*/ AND `locale`='deDE') OR (`entry`=25016 /*25016*/ AND `locale`='deDE') OR (`entry`=25014 /*25014*/ AND `locale`='deDE') OR (`entry`=25012 /*25012*/ AND `locale`='deDE') OR (`entry`=25010 /*25010*/ AND `locale`='deDE') OR (`entry`=25013 /*25013*/ AND `locale`='deDE') OR (`entry`=25017 /*25017*/ AND `locale`='deDE') OR (`entry`=14380 /*14380*/ AND `locale`='deDE') OR (`entry`=14379 /*14379*/ AND `locale`='deDE') OR (`entry`=4167 /*4167*/ AND `locale`='deDE') OR (`entry`=11042 /*11042*/ AND `locale`='deDE') OR (`entry`=4786 /*4786*/ AND `locale`='deDE') OR (`entry`=4783 /*4783*/ AND `locale`='deDE') OR (`entry`=4226 /*4226*/ AND `locale`='deDE') OR (`entry`=4784 /*4784*/ AND `locale`='deDE') OR (`entry`=4229 /*4229*/ AND `locale`='deDE') OR (`entry`=4160 /*4160*/ AND `locale`='deDE') OR (`entry`=11041 /*11041*/ AND `locale`='deDE') OR (`entry`=47584 /*47584*/ AND `locale`='deDE') OR (`entry`=30731 /*30731*/ AND `locale`='deDE') OR (`entry`=30715 /*30715*/ AND `locale`='deDE') OR (`entry`=11070 /*11070*/ AND `locale`='deDE') OR (`entry`=4228 /*4228*/ AND `locale`='deDE') OR (`entry`=4213 /*4213*/ AND `locale`='deDE') OR (`entry`=48738 /*48738*/ AND `locale`='deDE') OR (`entry`=48737 /*48737*/ AND `locale`='deDE') OR (`entry`=48736 /*48736*/ AND `locale`='deDE') OR (`entry`=48735 /*48735*/ AND `locale`='deDE') OR (`entry`=61757 /*61757*/ AND `locale`='deDE') OR (`entry`=50498 /*50498*/ AND `locale`='deDE') OR (`entry`=50502 /*50502*/ AND `locale`='deDE') OR (`entry`=36479 /*36479*/ AND `locale`='deDE') OR (`entry`=50499 /*50499*/ AND `locale`='deDE') OR (`entry`=4242 /*4242*/ AND `locale`='deDE') OR (`entry`=50520 /*50520*/ AND `locale`='deDE') OR (`entry`=50519 /*50519*/ AND `locale`='deDE') OR (`entry`=50518 /*50518*/ AND `locale`='deDE') OR (`entry`=50517 /*50517*/ AND `locale`='deDE') OR (`entry`=50516 /*50516*/ AND `locale`='deDE') OR (`entry`=50513 /*50513*/ AND `locale`='deDE') OR (`entry`=50510 /*50510*/ AND `locale`='deDE') OR (`entry`=50509 /*50509*/ AND `locale`='deDE') OR (`entry`=50508 /*50508*/ AND `locale`='deDE') OR (`entry`=50507 /*50507*/ AND `locale`='deDE') OR (`entry`=50506 /*50506*/ AND `locale`='deDE') OR (`entry`=50521 /*50521*/ AND `locale`='deDE') OR (`entry`=50505 /*50505*/ AND `locale`='deDE') OR (`entry`=50504 /*50504*/ AND `locale`='deDE') OR (`entry`=50501 /*50501*/ AND `locale`='deDE') OR (`entry`=50500 /*50500*/ AND `locale`='deDE') OR (`entry`=52644 /*52644*/ AND `locale`='deDE') OR (`entry`=50497 /*50497*/ AND `locale`='deDE') OR (`entry`=55285 /*55285*/ AND `locale`='deDE') OR (`entry`=2041 /*2041*/ AND `locale`='deDE') OR (`entry`=55273 /*55273*/ AND `locale`='deDE') OR (`entry`=55272 /*55272*/ AND `locale`='deDE') OR (`entry`=52645 /*52645*/ AND `locale`='deDE') OR (`entry`=4138 /*4138*/ AND `locale`='deDE') OR (`entry`=4243 /*4243*/ AND `locale`='deDE') OR (`entry`=3468 /*3468*/ AND `locale`='deDE') OR (`entry`=34396 /*34396*/ AND `locale`='deDE') OR (`entry`=4244 /*4244*/ AND `locale`='deDE') OR (`entry`=4205 /*4205*/ AND `locale`='deDE') OR (`entry`=4146 /*4146*/ AND `locale`='deDE') OR (`entry`=10056 /*10056*/ AND `locale`='deDE') OR (`entry`=4211 /*4211*/ AND `locale`='deDE') OR (`entry`=10089 /*10089*/ AND `locale`='deDE') OR (`entry`=52643 /*52643*/ AND `locale`='deDE') OR (`entry`=52637 /*52637*/ AND `locale`='deDE') OR (`entry`=52636 /*52636*/ AND `locale`='deDE') OR (`entry`=35168 /*35168*/ AND `locale`='deDE') OR (`entry`=52642 /*52642*/ AND `locale`='deDE') OR (`entry`=14602 /*14602*/ AND `locale`='deDE') OR (`entry`=14556 /*14556*/ AND `locale`='deDE') OR (`entry`=12358 /*12358*/ AND `locale`='deDE') OR (`entry`=4730 /*4730*/ AND `locale`='deDE') OR (`entry`=14555 /*14555*/ AND `locale`='deDE') OR (`entry`=12360 /*12360*/ AND `locale`='deDE') OR (`entry`=12359 /*12359*/ AND `locale`='deDE') OR (`entry`=4753 /*4753*/ AND `locale`='deDE') OR (`entry`=4223 /*4223*/ AND `locale`='deDE') OR (`entry`=4210 /*4210*/ AND `locale`='deDE') OR (`entry`=4215 /*4215*/ AND `locale`='deDE') OR (`entry`=4163 /*4163*/ AND `locale`='deDE') OR (`entry`=4423 /*4423*/ AND `locale`='deDE') OR (`entry`=4214 /*4214*/ AND `locale`='deDE') OR (`entry`=49748 /*49748*/ AND `locale`='deDE') OR (`entry`=5782 /*5782*/ AND `locale`='deDE') OR (`entry`=24110 /*24110*/ AND `locale`='deDE') OR (`entry`=62178 /*62178*/ AND `locale`='deDE') OR (`entry`=61071 /*61071*/ AND `locale`='deDE') OR (`entry`=50307 /*50307*/ AND `locale`='deDE') OR (`entry`=50305 /*50305*/ AND `locale`='deDE') OR (`entry`=49842 /*49842*/ AND `locale`='deDE') OR (`entry`=40552 /*40552*/ AND `locale`='deDE') OR (`entry`=11700 /*11700*/ AND `locale`='deDE') OR (`entry`=10878 /*10878*/ AND `locale`='deDE') OR (`entry`=7316 /*7316*/ AND `locale`='deDE') OR (`entry`=51371 /*51371*/ AND `locale`='deDE') OR (`entry`=62177 /*62177*/ AND `locale`='deDE') OR (`entry`=13321 /*13321*/ AND `locale`='deDE') OR (`entry`=4209 /*4209*/ AND `locale`='deDE') OR (`entry`=4208 /*4208*/ AND `locale`='deDE') OR (`entry`=4155 /*4155*/ AND `locale`='deDE') OR (`entry`=3681 /*3681*/ AND `locale`='deDE') OR (`entry`=3838 /*3838*/ AND `locale`='deDE') OR (`entry`=42970 /*42970*/ AND `locale`='deDE') OR (`entry`=42968 /*42968*/ AND `locale`='deDE') OR (`entry`=49778 /*49778*/ AND `locale`='deDE') OR (`entry`=15384 /*15384*/ AND `locale`='deDE') OR (`entry`=7907 /*7907*/ AND `locale`='deDE') OR (`entry`=7916 /*7916*/ AND `locale`='deDE') OR (`entry`=10118 /*10118*/ AND `locale`='deDE') OR (`entry`=49728 /*49728*/ AND `locale`='deDE') OR (`entry`=4262 /*4262*/ AND `locale`='deDE') OR (`entry`=3607 /*3607*/ AND `locale`='deDE') OR (`entry`=53522 /*53522*/ AND `locale`='deDE') OR (`entry`=43567 /*43567*/ AND `locale`='deDE') OR (`entry`=43767 /*43767*/ AND `locale`='deDE') OR (`entry`=43764 /*43764*/ AND `locale`='deDE') OR (`entry`=43791 /*43791*/ AND `locale`='deDE') OR (`entry`=40350 /*40350*/ AND `locale`='deDE') OR (`entry`=43651 /*43651*/ AND `locale`='deDE') OR (`entry`=43793 /*43793*/ AND `locale`='deDE') OR (`entry`=43703 /*43703*/ AND `locale`='deDE') OR (`entry`=42141 /*42141*/ AND `locale`='deDE') OR (`entry`=43566 /*43566*/ AND `locale`='deDE') OR (`entry`=43718 /*43718*/ AND `locale`='deDE') OR (`entry`=37939 /*37939*/ AND `locale`='deDE') OR (`entry`=37921 /*37921*/ AND `locale`='deDE') OR (`entry`=37938 /*37938*/ AND `locale`='deDE') OR (`entry`=37916 /*37916*/ AND `locale`='deDE') OR (`entry`=50274 /*50274*/ AND `locale`='deDE') OR (`entry`=43749 /*43749*/ AND `locale`='deDE') OR (`entry`=43747 /*43747*/ AND `locale`='deDE') OR (`entry`=50275 /*50275*/ AND `locale`='deDE') OR (`entry`=50273 /*50273*/ AND `locale`='deDE') OR (`entry`=50271 /*50271*/ AND `locale`='deDE') OR (`entry`=43727 /*43727*/ AND `locale`='deDE') OR (`entry`=38783 /*38783*/ AND `locale`='deDE') OR (`entry`=38149 /*38149*/ AND `locale`='deDE') OR (`entry`=37914 /*37914*/ AND `locale`='deDE') OR (`entry`=38780 /*38780*/ AND `locale`='deDE') OR (`entry`=620 /*620*/ AND `locale`='deDE') OR (`entry`=36616 /*36616*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(38781, 'deDE', 'Gilnearischer Überlebender', '', NULL, NULL, 23420), -- 38781
(38144, 'deDE', 'Krennan Aranas', '', 'Königlicher Chemiker', NULL, 23420), -- 38144
(37885, 'deDE', 'Ruheloser Vorfahr', '', NULL, NULL, 23420), -- 37885
(37884, 'deDE', 'Aufgewühlter Geist', '', NULL, NULL, 23420), -- 37884
(38365, 'deDE', 'Slime Spigot Bunny', '', NULL, NULL, 23420), -- 38365
(38364, 'deDE', 'Seuchenschmied der Verlassenen', '', NULL, NULL, 23420), -- 38364
(38389, 'deDE', 'Plague Cloud Bunny Non-Large AOI', '', NULL, NULL, 23420), -- 38389
(38363, 'deDE', 'Eindringling der Verlassenen', '', NULL, NULL, 23420), -- 38363
(38344, 'deDE', 'Plague Cloud Bunny', '', NULL, NULL, 23420), -- 38344
(38287, 'deDE', 'Katapult der Verlassenen', '', NULL, NULL, 23420), -- 38287
(38150, 'deDE', 'Glevenschleuder', '', NULL, 'vehichleCursor', 23420), -- 38150
(38615, 'deDE', 'Gefangene Reitfledermaus', '', NULL, 'vehichleCursor', 23420), -- 38615
(37892, 'deDE', 'Eitrige Made', '', NULL, NULL, 23420), -- 37892
(44388, 'deDE', 'Gilnearischer Milizsoldat', '', NULL, NULL, 23420), -- 44388
(37891, 'deDE', 'Untergrundspinne', '', NULL, NULL, 23420), -- 37891
(37889, 'deDE', 'Friedhofsratte', '', NULL, NULL, 23420), -- 37889
(38618, 'deDE', 'Unteroffizier der Verlassenen', '', NULL, NULL, 23420), -- 38618
(38617, 'deDE', 'General der Verlassenen', '', NULL, NULL, 23420), -- 38617
(38616, 'deDE', 'Infanterist der Verlassenen', '', NULL, NULL, 23420), -- 38616
(38614, 'deDE', 'Prinz Liam Graumähne', '', NULL, NULL, 23420), -- 38614
(38539, 'deDE', 'König Genn Graumähne', '', NULL, NULL, 23420), -- 38539
(39017, 'deDE', 'Gilnearischer Milizsoldat', '', NULL, NULL, 23420), -- 39017
(39016, 'deDE', 'Worgenkrieger', '', NULL, NULL, 23420), -- 39016
(39015, 'deDE', 'Eindringling der Verlassenen', '', NULL, NULL, 23420), -- 39015
(38474, 'deDE', 'Prinz Liam Graumähne', '', NULL, NULL, 23420), -- 38474
(38613, 'deDE', 'Worgenkrieger', '', NULL, NULL, 23420), -- 38613
(38611, 'deDE', 'Lorna Crowley', '', NULL, NULL, 23420), -- 38611
(38470, 'deDE', 'König Genn Graumähne', '', NULL, NULL, 23420), -- 38470
(38473, 'deDE', 'Seelengekettete Banshee', '', NULL, NULL, 23420), -- 38473
(38469, 'deDE', 'Fürstin Sylvanas Windläufer', '', 'Bansheekönigin', NULL, 23420), -- 38469
(38331, 'deDE', 'Faulblut', '', NULL, NULL, 23420), -- 38331
(38415, 'deDE', 'Lord Darius Crowley', '', NULL, NULL, 23420), -- 38415
(38348, 'deDE', 'Worgenkrieger', '', NULL, NULL, 23420), -- 38348
(38426, 'deDE', 'Lorna Crowley', '', NULL, NULL, 23420), -- 38426
(38425, 'deDE', 'Befreiter Bürger von Glutstein', '', NULL, NULL, 23420), -- 38425
(38424, 'deDE', 'Glutsteinkanone', '', NULL, 'Gunner', 23420), -- 38424
(38464, 'deDE', 'Elitekämpferin der dunklen Waldläufer', '', NULL, NULL, 23420), -- 38464
(38420, 'deDE', 'Üble Monstrosität', '', NULL, NULL, 23420), -- 38420
(38210, 'deDE', 'Armbrustschütze der Verlassenen', '', NULL, NULL, 23420), -- 38210
(38192, 'deDE', 'Infanterist der Verlassenen', '', NULL, NULL, 23420), -- 38192
(28332, 'deDE', 'Allgemeiner Auslöser LAB (Large AOI)', '', NULL, NULL, 23420), -- 28332
(38377, 'deDE', 'Beschädigtes Katapult', '', NULL, 'vehichleCursor', 23420), -- 38377
(38218, 'deDE', 'Prinz Liam Graumähne', '', NULL, NULL, 23420), -- 38218
(38465, 'deDE', 'Myriam Zauberwache', '', 'Magierlehrerin', NULL, 23420), -- 38465
(38466, 'deDE', 'Schwester Almyra', '', 'Priesterlehrerin', NULL, 23420), -- 38466
(38221, 'deDE', 'Gilnearischer Milizsoldat', '', NULL, NULL, 23420), -- 38221
(38467, 'deDE', 'Jäger Blake', '', 'Jägerlehrer', NULL, 23420), -- 38467
(38468, 'deDE', 'Gilnearischer Mastiff', '', NULL, NULL, 23420), -- 38468
(37686, 'deDE', 'Exekutor Cornell', '', NULL, NULL, 23420), -- 37686
(37685, 'deDE', 'Valnov der Verrückte', '', NULL, NULL, 23420), -- 37685
(37692, 'deDE', 'Infanterist der Verlassenen', '', NULL, NULL, 23420), -- 37692
(37694, 'deDE', 'Versklavter Dorfbewohner', '', NULL, NULL, 23420), -- 37694
(37701, 'deDE', 'Sklaventreiber der Verlassenen', '', NULL, NULL, 23420), -- 37701
(24042, 'deDE', 'Generic Trigger LAB - OLD', '', NULL, NULL, 23420), -- 24042
(37802, 'deDE', 'Brothogg der Sklavenmeister', '', NULL, NULL, 23420), -- 37802
(38553, 'deDE', 'Krennan Aranas', '', 'Königlicher Chemiker', NULL, 23420), -- 38553
(38143, 'deDE', 'Magda Weißwall', '', NULL, NULL, 23420), -- 38143
(37803, 'deDE', 'Marcus', '', NULL, NULL, 23420), -- 37803
(37783, 'deDE', 'Lorna Crowley', '', NULL, NULL, 23420), -- 37783
(42853, 'deDE', 'Karen Murray', '', 'Gemischtwarenhändlerin', NULL, 23420), -- 42853
(37784, 'deDE', 'Gilnearischer Milizsoldat', '', NULL, NULL, 23420), -- 37784
(883, 'deDE', 'Reh', '', NULL, NULL, 23420), -- 883
(37786, 'deDE', 'Braunhirsch', '', NULL, NULL, 23420), -- 37786
(37785, 'deDE', 'Wildpferd', '', NULL, NULL, 23420), -- 37785
(1933, 'deDE', 'Schaf', '', NULL, NULL, 23420), -- 1933
(37718, 'deDE', 'Gebirgsmastiff', '', NULL, NULL, 23420), -- 37718
(38764, 'deDE', 'Lord Hewell', '', NULL, NULL, 23420), -- 38764
(37876, 'deDE', 'König Genn Graumähne', '', NULL, NULL, 23420), -- 37876
(37875, 'deDE', 'Lord Godfrey', '', NULL, NULL, 23420), -- 37875
(37716, 'deDE', 'Wachmann von Witterfront', '', NULL, NULL, 23420), -- 37716
(37733, 'deDE', 'Lord Walden', '', NULL, NULL, 23420), -- 37733
(37735, 'deDE', 'Baron Ashbury', '', NULL, NULL, 23420), -- 37735
(37874, 'deDE', 'Krennan Aranas', '', 'Königlicher Chemiker', NULL, 23420), -- 37874
(38029, 'deDE', 'Tobias Dunstmantel', '', NULL, NULL, 23420), -- 38029
(38022, 'deDE', 'Veteranin der dunklen Waldläufer', '', NULL, NULL, 23420), -- 38022
(37873, 'deDE', 'Vassandra Sturmklaue', '', NULL, NULL, 23420), -- 37873
(37870, 'deDE', 'Lyros Flinkwind', '', NULL, NULL, 23420), -- 37870
(37195, 'deDE', 'Lord Darius Crowley', '', NULL, NULL, 23420), -- 37195
(36814, 'deDE', 'Talran aus der Wildnis', '', NULL, NULL, 23420), -- 36814
(42953, 'deDE', 'Benjamin Sykes', '', 'Lebensmittelverkäufer', NULL, 23420), -- 42953
(37489, 'deDE', 'Wilder Behüter', '', NULL, NULL, 23420), -- 37489
(37197, 'deDE', 'Wilder Worgen', '', NULL, NULL, 23420), -- 37197
(37822, 'deDE', 'Belysra Sternenhauch', '', 'Mondpriesterin', NULL, 23420), -- 37822
(37045, 'deDE', 'Rygna', '', 'Giftbrutmatriarchin', NULL, 23420), -- 37045
(50570, 'deDE', 'Whilsey Bottomtooth', '', 'Angellehrer', NULL, 23420), -- 50570
(36813, 'deDE', 'Giftbruthuscher', '', NULL, NULL, 23420), -- 36813
(38797, 'deDE', 'Vitus Dunkelwandler', '', 'Hexenmeisterlehrer', NULL, 23420), -- 38797
(41561, 'deDE', 'Misstrauischer Mastiff', '', NULL, NULL, 23420), -- 41561
(38795, 'deDE', 'Schwester Almyra', '', 'Priesterlehrerin', NULL, 23420), -- 38795
(43558, 'deDE', 'Marie Allen', '', 'Gemischtwarenhändlerin', NULL, 23420), -- 43558
(38798, 'deDE', 'Jäger Blake', '', 'Jägerlehrer', NULL, 23420), -- 38798
(37815, 'deDE', 'Lorna Crowley', '', NULL, NULL, 23420), -- 37815
(38799, 'deDE', 'Celestine Erntedank', '', 'Druidenlehrerin', NULL, 23420), -- 38799
(38796, 'deDE', 'Loren die Hehlerin', '', NULL, NULL, 23420), -- 38796
(38792, 'deDE', 'Willa Arnes', '', 'Gastwirtin', NULL, 23420), -- 38792
(37102, 'deDE', 'Gwen Armstead', '', NULL, NULL, 23420), -- 37102
(38794, 'deDE', 'Myriam Zauberwache', '', 'Magierlehrerin', NULL, 23420), -- 38794
(37499, 'deDE', 'Überlebender von Dämmerhafen', '', NULL, NULL, 23420), -- 37499
(38793, 'deDE', 'Unteroffizier Cleese', '', 'Kriegerlehrer', NULL, 23420), -- 38793
(6827, 'deDE', 'Strandkrebs', '', NULL, NULL, 23420), -- 6827
(17467, 'deDE', 'Stinktier', '', NULL, NULL, 23420), -- 17467
(37757, 'deDE', 'Heulende Banshee', '', NULL, NULL, 23420), -- 37757
(37492, 'deDE', 'Schwarzforstfuchs', '', NULL, NULL, 23420), -- 37492
(37807, 'deDE', 'Katapult der Verlassenen', '', NULL, 'vehichleCursor', 23420), -- 37807
(37806, 'deDE', 'Hauptmann Asther', '', NULL, NULL, 23420), -- 37806
(37805, 'deDE', 'Soldat der Verlassenen', '', NULL, NULL, 23420), -- 37805
(37808, 'deDE', 'Koroth der Hügelbrecher', '', NULL, NULL, 23420), -- 37808
(36294, 'deDE', 'Koroth der Hügelbrecher', '', NULL, NULL, 23420), -- 36294
(36293, 'deDE', 'Ogerdiener', '', 'Diener von Koroth', NULL, 23420), -- 36293
(36882, 'deDE', 'Sumpfkrokilisk', '', NULL, NULL, 23420), -- 36882
(2914, 'deDE', 'Schlange', '', NULL, NULL, 23420), -- 2914
(37067, 'deDE', 'Unfallüberlebender', '', NULL, NULL, 23420), -- 37067
(1420, 'deDE', 'Kröte', '', NULL, NULL, 23420), -- 1420
(38762, 'deDE', 'Ogerwegelagerer', '', 'Diener von Koroth', NULL, 23420), -- 38762
(37065, 'deDE', 'Prinz Liam Graumähne', '', NULL, NULL, 23420), -- 37065
(36743, 'deDE', 'König Genn Graumähne', '', NULL, NULL, 23420), -- 36743
(36962, 'deDE', 'Verletzter Dorfbewohner', '', NULL, NULL, 23420), -- 36962
(36742, 'deDE', 'Prinzessin Tess Graumähne', '', NULL, NULL, 23420), -- 36742
(36606, 'deDE', 'Königin Mia Graumähne', '', NULL, NULL, 23420), -- 36606
(51409, 'deDE', 'Lorna Crowley', '', NULL, NULL, 23420), -- 51409
(37946, 'deDE', 'Wachmann von Dämmerhafen', '', NULL, NULL, 23420), -- 37946
(43907, 'deDE', 'Wachmann von Dämmerhafen', '', NULL, NULL, 23420), -- 43907
(36138, 'deDE', 'Krennan Aranas', '', 'Königlicher Chemiker', NULL, 23420), -- 36138
(44928, 'deDE', 'Postkutschenfuhrwerk', '', NULL, 'vehichleCursor', 23420), -- 44928
(38755, 'deDE', 'Postkutschenharnisch', '', NULL, 'vehichleCursor', 23420), -- 38755
(43338, 'deDE', 'Postkutschenpferd', '', NULL, NULL, 23420), -- 43338
(36461, 'deDE', 'Lucius der Grausame', '', NULL, NULL, 23420), -- 36461
(36458, 'deDE', 'Oma Wahl', '', NULL, NULL, 23420), -- 36458
(36671, 'deDE', 'Späher der Verlassenen', '', NULL, NULL, 23420), -- 36671
(36455, 'deDE', 'Walt Hayward', '', NULL, NULL, 23420), -- 36455
(36454, 'deDE', 'Tim Hayward', '', NULL, NULL, 23420), -- 36454
(36492, 'deDE', 'Ron Hayward', '', NULL, NULL, 23420), -- 36492
(36491, 'deDE', 'Trent Hayward', '', NULL, NULL, 23420), -- 36491
(36488, 'deDE', 'Schiffbrüchiger Verlassener', '', NULL, NULL, 23420), -- 36488
(36456, 'deDE', 'Sebastian Hayward', '', NULL, NULL, 23420), -- 36456
(36528, 'deDE', 'Koroth der Hügelbrecher', '', NULL, NULL, 23420), -- 36528
(36459, 'deDE', 'Felix', '', NULL, 'Pickup', 23420), -- 36459
(36540, 'deDE', 'Gebirgspferd', '', NULL, 'vehichleCursor', 23420), -- 36540
(36512, 'deDE', 'Fuchs', '', NULL, NULL, 23420), -- 36512
(36457, 'deDE', 'Lorna Crowley', '', NULL, NULL, 23420), -- 36457
(36452, 'deDE', 'Gwen Armstead', '', 'Bürgermeisterin von Dämmerhafen', NULL, 23420), -- 36452
(1412, 'deDE', 'Eichhörnchen', '', NULL, NULL, 23420), -- 1412
(36440, 'deDE', 'Ertrinkender Wachmann', '', NULL, 'Interact', 23420), -- 36440
(36653, 'deDE', 'Wachmann von Dämmerhafen', '', NULL, NULL, 23420), -- 36653
(36451, 'deDE', 'Prinz Liam Graumähne', '', NULL, NULL, 23420), -- 36451
(36693, 'deDE', 'Bewusstloser Wachmann', '', NULL, 'Interact', 23420), -- 36693
(36460, 'deDE', 'Überlebender der Verlassenen', '', NULL, NULL, 23420), -- 36460
(36405, 'deDE', 'Kampfmastiff', '', NULL, NULL, 23420), -- 36405
(36396, 'deDE', 'Seemann der Verlassenen', '', NULL, NULL, 23420), -- 36396
(36312, 'deDE', 'Dunkle Waldläuferin Thyala', '', NULL, NULL, 23420), -- 36312
(36399, 'deDE', 'Kapitän Morris', '', NULL, NULL, 23420), -- 36399
(36397, 'deDE', 'Kapitän Anson', '', NULL, NULL, 23420), -- 36397
(36287, 'deDE', 'Cynthia', '', NULL, 'Speak', 23420), -- 36287
(36288, 'deDE', 'Ashley', '', NULL, 'Speak', 23420), -- 36288
(36236, 'deDE', 'Fußsoldat der Verlassenen', '', NULL, NULL, 23420), -- 36236
(36289, 'deDE', 'James', '', NULL, 'Speak', 23420), -- 36289
(36690, 'deDE', 'Wachmann von Dämmerhafen', '', NULL, NULL, 23420), -- 36690
(36291, 'deDE', 'Melinda Hammond', '', NULL, NULL, 23420), -- 36291
(36290, 'deDE', 'Lord Godfrey', '', NULL, NULL, 23420), -- 36290
(36779, 'deDE', 'Marie Allen', '', 'Gemischtwarenhändlerin', NULL, 23420), -- 36779
(385, 'deDE', 'Pferd', '', NULL, NULL, 23420), -- 385
(89715, 'deDE', 'Franklin Martin', '', 'Transporter', NULL, 23420), -- 89715
(39660, 'deDE', 'Geistheiler', '', NULL, NULL, 23420), -- 39660
(4076, 'deDE', 'Schabe', '', NULL, NULL, 23420), -- 4076
(36283, 'deDE', 'Katapult der Verlassenen', '', NULL, 'vehichleCursor', 23420), -- 36283
(36292, 'deDE', 'Maschinist der Verlassenen', '', NULL, NULL, 23420), -- 36292
(36231, 'deDE', 'Entsetzliche Monstrosität', '', NULL, NULL, 23420), -- 36231
(36140, 'deDE', 'Prinz Liam Graumähne', '', NULL, NULL, 23420), -- 36140
(36211, 'deDE', 'Wachmann von Dämmerhafen', '', NULL, NULL, 23420), -- 36211
(34511, 'deDE', 'Eindringling der Verlassenen', '', NULL, NULL, 23420), -- 34511
(38881, 'deDE', 'Fuchs', '', NULL, NULL, 23420), -- 38881
(36809, 'deDE', 'Eindringling der Verlassenen', '', NULL, NULL, 23420), -- 36809
(36714, 'deDE', 'Sanftmütiges Schaf', '', NULL, NULL, 23420), -- 36714
(36205, 'deDE', 'Erschlagener Wachmann', '', NULL, NULL, 23420), -- 36205
(36200, 'deDE', 'Tremors Credit', '', NULL, NULL, 23420), -- 36200
(38791, 'deDE', 'Willa Arnes', '', 'Gastwirtin', NULL, 23420), -- 38791
(50574, 'deDE', 'Amelia Atherton', '', 'Lehrerin für Erste Hilfe', NULL, 23420), -- 50574
(50567, 'deDE', 'Fielding Chesterhill', '', 'Kochkunstlehrer', NULL, 23420), -- 50567
(36630, 'deDE', 'Loren die Hehlerin', '', NULL, NULL, 23420), -- 36630
(36190, 'deDE', 'Prinz Liam Graumähne', '', NULL, NULL, 23420), -- 36190
(44125, 'deDE', 'Chris Moller', '', 'Kuchen- und Pastetenverkäufer', NULL, 23420), -- 44125
(36632, 'deDE', 'Schwester Almyra', '', 'Priesterlehrerin', NULL, 23420), -- 36632
(36717, 'deDE', 'Gerard Walthorn', '', 'Waffenverkäufer', NULL, 23420), -- 36717
(36695, 'deDE', 'Samantha Buckley', '', 'Gemischtwarenhändlerin', NULL, 23420), -- 36695
(36651, 'deDE', 'Unteroffizier Cleese', '', 'Kriegerlehrer', NULL, 23420), -- 36651
(36629, 'deDE', 'Jäger Blake', '', 'Jägerlehrer', NULL, 23420), -- 36629
(36713, 'deDE', 'Gilnearischer Mastiff', '', NULL, NULL, 23420), -- 36713
(36628, 'deDE', 'Celestine Erntedank', '', 'Druidenlehrerin', NULL, 23420), -- 36628
(36652, 'deDE', 'Vitus Dunkelwandler', '', 'Hexenmeisterlehrer', NULL, 23420), -- 36652
(50252, 'deDE', 'Mary Oxworth', '', 'Bankierin', NULL, 23420), -- 50252
(36631, 'deDE', 'Myriam Zauberwache', '', 'Magierlehrerin', NULL, 23420), -- 36631
(34571, 'deDE', 'Gwen Armstead', '', 'Bürgermeisterin von Dämmerhafen', NULL, 23420), -- 34571
(36132, 'deDE', 'Krennan Aranas', '', 'Königlicher Chemiker', NULL, 23420), -- 36132
(50247, 'deDE', 'Hans Gassendampf', '', 'Berufsausbilder', NULL, 23420), -- 50247
(36453, 'deDE', 'Bewohner von Dämmerhafen', '', NULL, NULL, 23420), -- 36453
(14881, 'deDE', 'Spinne', '', NULL, NULL, 23420), -- 14881
(36170, 'deDE', 'Lord Godfrey', '', NULL, NULL, 23420), -- 36170
(36798, 'deDE', 'Gefangener Worgen', '', NULL, NULL, 23420), -- 36798
(36698, 'deDE', 'Gefangener Worgen', '', NULL, NULL, 23420), -- 36698
(36602, 'deDE', 'Wachmann von Dämmerhafen', '', NULL, NULL, 23420), -- 36602
(36797, 'deDE', 'Gefangener Worgen', '', NULL, NULL, 23420), -- 36797
(36449, 'deDE', 'Generic Trigger LAB - Multiphase (Gigantic)', '', NULL, NULL, 23420), -- 36449
(36286, 'deDE', 'Generic Trigger LAB - Multiphase (Gigantic AOI)', '', NULL, NULL, 23420), -- 36286
(36198, 'deDE', 'Generic Trigger LAB - Multiphase (Ground)', '', NULL, NULL, 23420), -- 36198
(41015, 'deDE', 'Rebell des Nordtors', '', NULL, NULL, 23420), -- 41015
(35627, 'deDE', 'Rasender Pirscher', '', NULL, NULL, 23420), -- 35627
(35566, 'deDE', 'Lord Darius Crowley', '', NULL, NULL, 23420), -- 35566
(44429, 'deDE', 'Crowleys Pferd', '', NULL, 'vehichleCursor', 23420), -- 44429
(35618, 'deDE', 'Tobias Dunstmantel', '', NULL, NULL, 23420), -- 35618
(36057, 'deDE', 'Rebell des Nordtors', '', NULL, NULL, 23420), -- 36057
(35551, 'deDE', 'Prinz Liam Graumähne', '', NULL, NULL, 23420), -- 35551
(44455, 'deDE', 'Unteroffizier Cleese', '', 'Kriegerlehrer', NULL, 23420), -- 44455
(44464, 'deDE', 'Loren die Hehlerin', '', NULL, NULL, 23420), -- 44464
(44427, 'deDE', 'Crowleys Pferd', '', NULL, 'vehichleCursor', 23420), -- 44427
(35911, 'deDE', 'König Genn Graumähne', '', NULL, NULL, 23420), -- 35911
(44465, 'deDE', 'Myriam Zauberwache', '', 'Magierlehrer', NULL, 23420), -- 44465
(35554, 'deDE', 'Erschütterte Überlebende', '', NULL, NULL, 23420), -- 35554
(44470, 'deDE', 'Verletzter Bürger', '', NULL, NULL, 23420), -- 44470
(44468, 'deDE', 'Schwester Almyra', '', 'Priesterlehrerin', NULL, 23420), -- 44468
(44463, 'deDE', 'Baron', '', 'Begleiter von Jäger Blake', NULL, 23420), -- 44463
(44461, 'deDE', 'Jäger Blake', '', 'Jägerlehrer', NULL, 23420), -- 44461
(44460, 'deDE', 'Gwen Armstead', '', NULL, NULL, 23420), -- 44460
(44459, 'deDE', 'Celestine Erntedank', '', 'Druidenlehrerin', NULL, 23420), -- 44459
(35552, 'deDE', 'Lord Darius Crowley', '', NULL, NULL, 23420), -- 35552
(44469, 'deDE', 'Vitus Dunkelwandler', '', 'Hexenmeisterlehrer', NULL, 23420), -- 44469
(35916, 'deDE', 'Blutfangreißer', '', NULL, NULL, 23420), -- 35916
(35229, 'deDE', 'Blutfangpirscher', '', NULL, NULL, 23420), -- 35229
(35914, 'deDE', 'Konfiszierte Kanone', '', NULL, NULL, 23420), -- 35914
(35915, 'deDE', 'Stadtwache von Gilneas', '', NULL, NULL, 23420), -- 35915
(50474, 'deDE', 'Stadtwache von Gilneas', '', NULL, NULL, 23420), -- 50474
(50471, 'deDE', 'Befallener Gilneer', '', NULL, NULL, 23420), -- 50471
(6491, 'deDE', 'Geistheiler', '', NULL, NULL, 23420), -- 6491
(35753, 'deDE', 'Krennan Aranas', '', 'Geselle des Chemiehandwerks', NULL, 23420), -- 35753
(35463, 'deDE', 'Blutfanglauerer', '', NULL, NULL, 23420), -- 35463
(35505, 'deDE', 'Blutfangreißer', '', NULL, NULL, 23420), -- 35505
(35509, 'deDE', 'Stadtwache von Gilneas', '', NULL, NULL, 23420), -- 35509
(35504, 'deDE', 'Stadtwache von Gilneas', '', NULL, NULL, 23420), -- 35504
(35906, 'deDE', 'Lord Godfrey', '', NULL, NULL, 23420), -- 35906
(35550, 'deDE', 'König Genn Graumähne', '', NULL, NULL, 23420), -- 35550
(38844, 'deDE', 'Gilnearischer Mastiff', '', NULL, NULL, 23420), -- 38844
(35378, 'deDE', 'Lorna Crowley', '', NULL, NULL, 23420), -- 35378
(35369, 'deDE', 'Josiah Avery', '', NULL, NULL, 23420), -- 35369
(50371, 'deDE', 'Hauptmann Broderick', '', NULL, NULL, 23420), -- 50371
(35123, 'deDE', 'Vincent Hersham', '', NULL, NULL, 23420), -- 35123
(35081, 'deDE', 'Sean Dempsey', '', NULL, NULL, 23420), -- 35081
(35115, 'deDE', 'Lord Godfrey', '', NULL, NULL, 23420), -- 35115
(35112, 'deDE', 'König Genn Graumähne', '', NULL, NULL, 23420), -- 35112
(35232, 'deDE', 'Gilnearische Königswache', '', NULL, NULL, 23420), -- 35232
(35839, 'deDE', 'Unteroffizier Cleese', '', 'Kriegerlehrer', NULL, 23420), -- 35839
(47091, 'deDE', 'Verwundete Wache', '', NULL, NULL, 23420),
(35912, 'deDE', 'Verletzter Bürger', '', NULL, NULL, 23420), -- 35912
(35873, 'deDE', 'Celestine Erntedank', '', 'Druidenlehrerin', NULL, 23420), -- 35873
(35872, 'deDE', 'Myriam Zauberwache', '', 'Magierlehrerin', NULL, 23420), -- 35872
(35871, 'deDE', 'Loren die Hehlerin', '', NULL, NULL, 23420), -- 35871
(35870, 'deDE', 'Schwester Almyra', '', 'Priesterlehrerin', NULL, 23420), -- 35870
(35869, 'deDE', 'Vitus Dunkelwandler', '', 'Hexenmeisterlehrer', NULL, 23420), -- 35869
(35124, 'deDE', 'Tobias Dunstmantel', '', NULL, NULL, 23420), -- 35124
(35077, 'deDE', 'Lord Darius Crowley', '', NULL, NULL, 23420), -- 35077
(35457, 'deDE', 'Blutfangtöter', '', NULL, NULL, 23420), -- 35457
(34936, 'deDE', 'Gwen Armstead', '', NULL, NULL, 23420), -- 34936
(34913, 'deDE', 'Prinz Liam Graumähne', '', NULL, NULL, 23420), -- 34913
(33175, 'deDE', 'Johnathan Staats', '', NULL, NULL, 23420), -- 33175
(3841, 'deDE', 'Teldira Mondfeder', '', 'Hippogryphenmeisterin', NULL, 23420), -- 3841
(32974, 'deDE', 'Laird', '', NULL, NULL, 23420), -- 32974
(32973, 'deDE', 'Dentaria Silbertal', '', 'Mondpriesterin', NULL, 23420), -- 32973
(32972, 'deDE', 'Serendia Eichwisper', '', 'Mondpriesterin', NULL, 23420), -- 32972
(32936, 'deDE', 'Gezeitenkriecherjungtier', '', NULL, NULL, 23420), -- 32936
(43431, 'deDE', 'Periale', '', 'Bergbaulehrerin', NULL, 23420), -- 43431
(43420, 'deDE', 'Gastwirtin Kyteran', '', 'Gastwirtin', NULL, 23420), -- 43420
(33359, 'deDE', 'Nachtsäblerreiterin', '', NULL, NULL, 23420), -- 33359
(49939, 'deDE', 'Kenral Nachtwind', '', 'Schurkenlehrer', NULL, 23420), -- 49939
(49923, 'deDE', 'Schildwache Mondschwinge', '', 'Kriegerlehrerin', NULL, 23420), -- 49923
(43439, 'deDE', 'Nyrisse', '', 'Lederrüstungen', NULL, 23420), -- 43439
(32979, 'deDE', 'Gorbold Stahlhand', '', 'Handwerkswaren', NULL, 23420), -- 32979
(32978, 'deDE', 'Tharnariun Baumwahrer', '', NULL, NULL, 23420), -- 32978
(32977, 'deDE', 'Hexknall Kurbeldreh', '', NULL, NULL, 23420), -- 32977
(32971, 'deDE', 'Waldläuferin Glynda Nal''Shea', '', NULL, NULL, 23420), -- 32971
(51997, 'deDE', 'Stephanie Krutsick', '', 'Archäologielehrerin', NULL, 23420), -- 51997
(43428, 'deDE', 'Faeyrin Weidenmond', '', 'Schneiderlehrer', NULL, 23420), -- 43428
(11037, 'deDE', 'Jenna Lemkenilli', '', 'Ingenieurskunstlehrerin', NULL, 23420), -- 11037
(43429, 'deDE', 'Taryel Feuerschlag', '', 'Schmiedekunstlehrer', NULL, 23420), -- 43429
(43424, 'deDE', 'Ayriala', '', 'Gemischtwaren', NULL, 23420), -- 43424
(33313, 'deDE', 'Geflecktes Reh', '', NULL, 'LootAll', 23420), -- 33313
(63084, 'deDE', 'Poe', '', 'Kampfhaustier', NULL, 23420), -- 63084
(63083, 'deDE', 'Will Larsons', '', 'Kampfhaustiertrainer', NULL, 23420), -- 63083
(49968, 'deDE', 'Lareth Beld', '', 'Magierlehrer', NULL, 23420), -- 49968
(49963, 'deDE', 'Laera Dubois', '', 'Hexenmeisterlehrerin', NULL, 23420), -- 49963
(49940, 'deDE', 'Irlara Morgenglanz', '', 'Priesterlehrerin', NULL, 23420), -- 49940
(49927, 'deDE', 'Lanla Bogenblatt', '', 'Jägerlehrerin', NULL, 23420), -- 49927
(32912, 'deDE', 'Schildwache Lendra', '', NULL, NULL, 23420), -- 32912
(10085, 'deDE', 'Jaelysia', '', 'Stallmeisterin', NULL, 23420), -- 10085
(4187, 'deDE', 'Harlon Dornenwacht', '', 'Rüstungs- & Schildschmied', NULL, 23420), -- 4187
(43436, 'deDE', 'Ceriale Abendwisper', '', 'Tuchmacherin', NULL, 23420), -- 43436
(34056, 'deDE', 'Wachsamer Beschützer', '', NULL, NULL, 23420), -- 34056
(721, 'deDE', 'Kaninchen', '', NULL, NULL, 23420), -- 721
(49942, 'deDE', 'Dular', '', 'Druidenlehrer', NULL, 23420), -- 49942
(33864, 'deDE', 'Übler Schrecken', '', NULL, NULL, 23420), -- 33864
(32969, 'deDE', 'Schildwache von Lor''danel', '', NULL, NULL, 23420), -- 32969
(64375, 'deDE', 'Schimmerschnecke', '', NULL, 'wildpetcatptureable', 23420), -- 64375
(32935, 'deDE', 'Verderbter Gezeitenkriecher', '', NULL, NULL, 23420), -- 32935
(32928, 'deDE', 'Widerwärtige Gischt', '', NULL, NULL, 23420), -- 32928
(6145, 'deDE', 'Fischschwarm', '', NULL, NULL, 23420), -- 6145
(33115, 'deDE', 'Schildwache von Lor''danel', '', NULL, NULL, 23420), -- 33115
(25018, 'deDE', 'Seefahrerin Steinferse', '', 'Die Bravado', NULL, 23420), -- 25018
(25015, 'deDE', 'Navigator Landerson', '', 'Die Bravado', NULL, 23420), -- 25015
(25011, 'deDE', 'Erster Maat Wellensang', '', 'Die Bravado', NULL, 23420), -- 25011
(25009, 'deDE', 'Kapitänin Angelina Soluna', '', 'Die Bravado', NULL, 23420), -- 25009
(25016, 'deDE', 'Seefahrer Wills', '', 'Die Bravado', NULL, 23420), -- 25016
(25014, 'deDE', 'Schildwache Wintertau', '', NULL, NULL, 23420), -- 25014
(25012, 'deDE', 'Smutje Schlaz', '', 'Die Bravado', NULL, 23420), -- 25012
(25010, 'deDE', 'Techniker Blankschnalle', '', 'Die Bravado', NULL, 23420), -- 25010
(25013, 'deDE', 'Schildwache Blattglanz', '', NULL, NULL, 23420), -- 25013
(25017, 'deDE', 'Seefahrerin Feenvolk', '', 'Die Bravado', NULL, 23420), -- 25017
(14380, 'deDE', 'Jägerin Laubschleicher', '', NULL, NULL, 23420), -- 14380
(14379, 'deDE', 'Jägerin Rabeneiche', '', NULL, NULL, 23420), -- 14379
(4167, 'deDE', 'Dendrythis', '', 'Speis & Trank', NULL, 23420), -- 4167
(11042, 'deDE', 'Sylvanna Mondwald', '', 'Alchemielehrling', NULL, 23420), -- 11042
(4786, 'deDE', 'Dämmerungsbehüter Shaedlass', '', 'Die Argentumdämmerung', NULL, 23420), -- 4786
(4783, 'deDE', 'Dämmerungsbehüter Selgorm', '', 'Die Argentumdämmerung', NULL, 23420), -- 4783
(4226, 'deDE', 'Ulthir', '', 'Alchemiebedarf', NULL, 23420), -- 4226
(4784, 'deDE', 'Argentumwache Manados', '', 'Die Argentumdämmerung', NULL, 23420), -- 4784
(4229, 'deDE', 'Mythrin''dir', '', 'Handwerkswaren', NULL, 23420), -- 4229
(4160, 'deDE', 'Ainethil', '', 'Alchemielehrerin', NULL, 23420), -- 4160
(11041, 'deDE', 'Milla Fairancora', '', 'Alchemielehrling', NULL, 23420), -- 11041
(47584, 'deDE', 'Aladrel Weißspitz', '', NULL, NULL, 23420), -- 47584
(30731, 'deDE', 'Illianna Mondschreiber', '', 'Inschriftenkundebedarf', NULL, 23420), -- 30731
(30715, 'deDE', 'Feyden Tintenrot', '', 'Inschriftenkundelehrer', NULL, 23420), -- 30715
(11070, 'deDE', 'Lalina Sommermond', '', 'Verzauberkunstlehrling', NULL, 23420), -- 11070
(4228, 'deDE', 'Vaean', '', 'Verzauberkunstbedarf', NULL, 23420), -- 4228
(4213, 'deDE', 'Taladan', '', 'Verzauberkunstmeister', NULL, 23420), -- 4213
(48738, 'deDE', 'Malfurion Sturmgrimm', '', NULL, NULL, 23420), -- 48738
(48737, 'deDE', 'Tyrande Wisperwind', '', 'Hohepriesterin von Elune', NULL, 23420), -- 48737
(48736, 'deDE', 'Genn Graumähne', '', NULL, NULL, 23420), -- 48736
(48735, 'deDE', 'Gwen Armstead', '', 'Gastwirtin', NULL, 23420), -- 48735
(61757, 'deDE', 'Rotschwänziges Streifenhörnchen', '', NULL, 'wildpetcapturable', 23420), -- 61757
(50498, 'deDE', 'Loren die Hehlerin', '', 'Schurkenlehrerin', NULL, 23420), -- 50498
(50502, 'deDE', 'Vitus Dunkelwandler', '', 'Hexenmeisterlehrer', NULL, 23420), -- 50502
(36479, 'deDE', 'Erzmagier Mordent Schattenfall', '', 'Die Hochgeborenen', NULL, 23420), -- 36479
(50499, 'deDE', 'Myriam Zauberwache', '', 'Magierlehrerin', NULL, 23420), -- 50499
(4242, 'deDE', 'Frostsäblergefährtin', '', NULL, NULL, 23420), -- 4242
(50520, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50520
(50519, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50519
(50518, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50518
(50517, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50517
(50516, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50516
(50513, 'deDE', 'Jamie Harriott', '', NULL, NULL, 23420), -- 50513
(50510, 'deDE', 'Rachel DeSimone', '', NULL, NULL, 23420), -- 50510
(50509, 'deDE', 'Jenn Stravaganza', '', NULL, NULL, 23420), -- 50509
(50508, 'deDE', 'Carrie Eileen Steen', '', NULL, NULL, 23420), -- 50508
(50507, 'deDE', 'Vassandra Sturmklaue', '', 'Druidenlehrerin', NULL, 23420), -- 50507
(50506, 'deDE', 'Talran aus der Wildnis', '', 'Druidenlehrer', NULL, 23420), -- 50506
(50521, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50521
(50505, 'deDE', 'Lyros Flinkwind', '', 'Druidenlehrer', NULL, 23420), -- 50505
(50504, 'deDE', 'Belysra Sternenhauch', '', 'Mondpriesterin', NULL, 23420), -- 50504
(50501, 'deDE', 'Schwester Almyra', '', 'Priesterlehrerin', NULL, 23420), -- 50501
(50500, 'deDE', 'Unteroffizier Cleese', '', 'Kriegerlehrer', NULL, 23420), -- 50500
(52644, 'deDE', 'Tarien Silbertau', '', 'Juwelierskunstbedarf', NULL, 23420), -- 52644
(50497, 'deDE', 'Jäger Blake', '', 'Jägerlehrer', NULL, 23420), -- 50497
(55285, 'deDE', 'Astrid Langstrumpf', '', 'Bergpferdehändlerin', NULL, 23420), -- 55285
(2041, 'deDE', 'Uralter Beschützer', '', NULL, NULL, 23420), -- 2041
(55273, 'deDE', 'Schnelles Bergpferd', '', NULL, NULL, 23420), -- 55273
(55272, 'deDE', 'Bergpferd', '', NULL, NULL, 23420), -- 55272
(52645, 'deDE', 'Aessa Silbertau', '', 'Juwelierskunstlehrerin', NULL, 23420), -- 52645
(4138, 'deDE', 'Jeen''ra Nachtläufer', '', 'Jägerlehrerin', NULL, 23420), -- 4138
(4243, 'deDE', 'Nachtschatten', '', 'Jeen''ras Tier', NULL, 23420), -- 4243
(3468, 'deDE', 'Urtum der Lehren', '', NULL, NULL, 23420), -- 3468
(34396, 'deDE', 'Weißschwanzhirschkuh', '', NULL, NULL, 23420), -- 34396
(4244, 'deDE', 'Schatten', '', 'Dorions Tier', NULL, 23420), -- 4244
(4205, 'deDE', 'Dorion', '', 'Jägerlehrer', NULL, 23420), -- 4205
(4146, 'deDE', 'Jocaste', '', 'Jägerlehrerin', NULL, 23420), -- 4146
(10056, 'deDE', 'Alassin', '', 'Stallmeister', NULL, 23420), -- 10056
(4211, 'deDE', 'Dannelor', '', 'Lehrer für Erste Hilfe', NULL, 23420), -- 4211
(10089, 'deDE', 'Silvaria', '', 'Tierausbilderin', NULL, 23420), -- 10089
(52643, 'deDE', 'Rissa Halding', '', 'Bergbaubedarf', NULL, 23420), -- 52643
(52637, 'deDE', 'Hugo Lentner', '', 'Ingenieursbedarf', NULL, 23420), -- 52637
(52636, 'deDE', 'Tana Lentner', '', 'Ingenieurskunstlehrerin', NULL, 23420), -- 52636
(35168, 'deDE', 'Gestreifter Dämmersäbler', '', NULL, NULL, 23420), -- 35168
(52642, 'deDE', 'Großknecht Pernic', '', 'Bergbaulehrer', NULL, 23420), -- 52642
(14602, 'deDE', 'Schneller Sturmsäbler', '', NULL, NULL, 23420), -- 14602
(14556, 'deDE', 'Schneller Frostsäbler', '', NULL, NULL, 23420), -- 14556
(12358, 'deDE', 'Gestreifter Frostsäbler', '', NULL, NULL, 23420), -- 12358
(4730, 'deDE', 'Lelanai', '', 'Säblerführerin', NULL, 23420), -- 4730
(14555, 'deDE', 'Schneller Schattensäbler', '', NULL, NULL, 23420), -- 14555
(12360, 'deDE', 'Gestreifter Nachtsäbler', '', NULL, NULL, 23420), -- 12360
(12359, 'deDE', 'Gefleckter Frostsäbler', '', NULL, NULL, 23420), -- 12359
(4753, 'deDE', 'Jartsam', '', 'Reitlehrer', NULL, 23420), -- 4753
(4223, 'deDE', 'Fyldan', '', 'Kochbedarf', NULL, 23420), -- 4223
(4210, 'deDE', 'Alegorn', '', 'Kochkunstlehrer', NULL, 23420), -- 4210
(4215, 'deDE', 'Anishar', '', 'Schurkenlehrer', NULL, 23420), -- 4215
(4163, 'deDE', 'Syurna', '', 'Schurkenlehrerin', NULL, 23420), -- 4163
(4423, 'deDE', 'Beschützer von Darnassus', '', NULL, NULL, 23420), -- 4423
(4214, 'deDE', 'Erion Schattenflüsterer', '', 'Schurkenlehrer', NULL, 23420), -- 4214
(49748, 'deDE', 'Heldenherold', '', NULL, NULL, 23420), -- 49748
(5782, 'deDE', 'Crildor', '', NULL, NULL, 23420), -- 5782
(24110, 'deDE', 'ELM General Purpose Bunny Large', '', NULL, NULL, 23420), -- 24110
(62178, 'deDE', 'Elfisches Kaninchen', '', NULL, 'wildpetcapturable', 23420), -- 62178
(61071, 'deDE', 'Kleiner Frosch', '', NULL, 'wildpetcapturable', 23420), -- 61071
(50307, 'deDE', 'Lord Candren', '', 'Rüstmeister von Gilneas', NULL, 23420), -- 50307
(50305, 'deDE', 'Mondpriesterin Lasara', '', 'Rüstmeisterin von Darnassus', NULL, 23420), -- 50305
(49842, 'deDE', 'Waldmotte', '', NULL, NULL, 23420), -- 49842
(40552, 'deDE', 'Leora', '', 'Hippogryphenmeisterin', NULL, 23420), -- 40552
(11700, 'deDE', 'Sarin Sternenfeuer', '', NULL, NULL, 23420), -- 11700
(10878, 'deDE', 'Herold Mondpirscher', '', NULL, NULL, 23420), -- 10878
(7316, 'deDE', 'Schwester Aquinne', '', 'Priester-Novizin', NULL, 23420), -- 7316
(51371, 'deDE', 'Hippogryphenreiter von Darnassus', '', NULL, NULL, 23420), -- 51371
(62177, 'deDE', 'Waldmotte', '', NULL, 'wildpetcapturable', 23420), -- 62177
(13321, 'deDE', 'Kleiner Frosch', '', NULL, NULL, 23420), -- 13321
(4209, 'deDE', 'Garryeth', '', 'Bankier', NULL, 23420), -- 4209
(4208, 'deDE', 'Lairn', '', 'Bankier', NULL, 23420), -- 4208
(4155, 'deDE', 'Idriana', '', 'Bankierin', NULL, 23420), -- 4155
(3681, 'deDE', 'Irrwisch', '', NULL, NULL, 23420), -- 3681
(3838, 'deDE', 'Vesprystus', '', 'Hippogryphenmeister', NULL, 23420), -- 3838
(42970, 'deDE', 'Flüchtling aus Gilneas', '', NULL, NULL, 23420), -- 42970
(42968, 'deDE', 'Krennan Aranas', '', NULL, NULL, 23420), -- 42968
(49778, 'deDE', 'Rotschwänziges Streifenhörnchen', '', NULL, NULL, 23420), -- 49778
(15384, 'deDE', 'ALTWorld Trigger', '', NULL, NULL, 23420), -- 15384
(7907, 'deDE', 'Daryn Leuchtwind', '', 'Bewahrerin der Cenarischen Lehren', NULL, 23420), -- 7907
(7916, 'deDE', 'Erelas Himmelsbern', '', NULL, NULL, 23420), -- 7916
(10118, 'deDE', 'Nessa Schattensang', '', 'Angelbedarf', NULL, 23420), -- 10118
(49728, 'deDE', 'Elfisches Kaninchen', '', NULL, NULL, 23420), -- 49728
(4262, 'deDE', 'Schildwache von Darnassus', '', NULL, 'Directions', 23420), -- 4262
(3607, 'deDE', 'Androl Eichenhand', '', 'Angellehrer', NULL, 23420), -- 3607
(53522, 'deDE', 'Babyoktopus', '', NULL, 'Pickup', 23420), -- 53522
(43567, 'deDE', 'Korm Knochenschind', '', 'Der Maschinist', NULL, 23420), -- 43567
(43767, 'deDE', 'Navigator', '', NULL, NULL, 23420), -- 43767
(43764, 'deDE', 'Admiral Sturmblut', '', NULL, NULL, 23420), -- 43764
(43791, 'deDE', 'Wyvern', '', NULL, 'vehichleCursor', 23420), -- 43791
(40350, 'deDE', 'Generic Trigger LAB', '', NULL, NULL, 23420), -- 40350
(43651, 'deDE', 'Worgenkrieger', '', NULL, NULL, 23420), -- 43651
(43793, 'deDE', 'Abschleppleine von Gilneas', '', NULL, NULL, 23420), -- 43793
(43703, 'deDE', 'Gilnearischer Scharfschütze', '', NULL, NULL, 23420), -- 43703
(42141, 'deDE', 'Grunzer des Kanonenboots', '', NULL, NULL, 23420), -- 42141
(43566, 'deDE', 'Lorna Crowley', '', NULL, NULL, 23420), -- 43566
(43718, 'deDE', 'Generic Trigger LAB (Gigantic AOI)', '', NULL, NULL, 23420), -- 43718
(37939, 'deDE', 'Reitkriegswolf', '', NULL, NULL, 23420), -- 37939
(37921, 'deDE', 'Orcische Kriegsmaschine', '', NULL, NULL, 23420), -- 37921
(37938, 'deDE', 'Vorhut des Wolfsschlunds', '', NULL, NULL, 23420), -- 37938
(37916, 'deDE', 'Orcischer Räuber', '', NULL, NULL, 23420), -- 37916
(50274, 'deDE', 'Ashley', '', NULL, NULL, 23420), -- 50274
(43749, 'deDE', 'Tobias Dunstmantel', '', NULL, NULL, 23420), -- 43749
(43747, 'deDE', 'Hippogryph', '', NULL, 'vehichleCursor', 23420), -- 43747
(50275, 'deDE', 'James', '', NULL, NULL, 23420), -- 50275
(50273, 'deDE', 'Cynthia', '', NULL, NULL, 23420), -- 50273
(50271, 'deDE', 'Melinda Hammond', '', NULL, NULL, 23420), -- 50271
(43727, 'deDE', 'Lorna Crowley', '', NULL, NULL, 23420), -- 43727
(38783, 'deDE', 'Marie Allen', '', 'Gemischtwarenhändlerin', NULL, 23420), -- 38783
(38149, 'deDE', 'Lord Darius Crowley', '', NULL, NULL, 23420), -- 38149
(37914, 'deDE', 'Gewaltiges Urtum', '', NULL, NULL, 23420), -- 37914
(38780, 'deDE', 'Nachtelfische Schildwache', '', NULL, NULL, 23420), -- 38780
(620, 'deDE', 'Huhn', '', NULL, NULL, 23420), -- 620
(36616, 'deDE', 'Admiral Nachtwind', '', NULL, NULL, 23420);