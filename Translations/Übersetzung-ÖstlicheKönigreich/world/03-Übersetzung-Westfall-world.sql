-- TrinityCore - WowPacketParser
-- File name: 23420_2017-02-23_20-44-06.pkt
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/23/2017 23:56:01


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=26503 AND `locale`='deDE') OR (`ID`=26288 AND `locale`='deDE') OR (`ID`=26761 AND `locale`='deDE') OR (`ID`=40753 AND `locale`='deDE') OR (`ID`=31593 AND `locale`='deDE') OR (`ID`=31821 AND `locale`='deDE') OR (`ID`=26365 AND `locale`='deDE') OR (`ID`=26370 AND `locale`='deDE') OR (`ID`=26322 AND `locale`='deDE') OR (`ID`=26356 AND `locale`='deDE') OR (`ID`=26355 AND `locale`='deDE') OR (`ID`=26354 AND `locale`='deDE') OR (`ID`=32008 AND `locale`='deDE') OR (`ID`=26353 AND `locale`='deDE') OR (`ID`=26349 AND `locale`='deDE') OR (`ID`=26348 AND `locale`='deDE') OR (`ID`=26347 AND `locale`='deDE') OR (`ID`=35586 AND `locale`='deDE') OR (`ID`=26320 AND `locale`='deDE') OR (`ID`=26319 AND `locale`='deDE') OR (`ID`=26297 AND `locale`='deDE') OR (`ID`=26296 AND `locale`='deDE') OR (`ID`=26295 AND `locale`='deDE') OR (`ID`=26292 AND `locale`='deDE') OR (`ID`=26291 AND `locale`='deDE') OR (`ID`=26290 AND `locale`='deDE') OR (`ID`=26510 AND `locale`='deDE') OR (`ID`=26289 AND `locale`='deDE') OR (`ID`=26286 AND `locale`='deDE') OR (`ID`=26287 AND `locale`='deDE') OR (`ID`=26371 AND `locale`='deDE') OR (`ID`=26271 AND `locale`='deDE') OR (`ID`=26266 AND `locale`='deDE') OR (`ID`=26270 AND `locale`='deDE') OR (`ID`=26257 AND `locale`='deDE') OR (`ID`=26252 AND `locale`='deDE') OR (`ID`=26241 AND `locale`='deDE') OR (`ID`=26237 AND `locale`='deDE') OR (`ID`=26236 AND `locale`='deDE') OR (`ID`=26232 AND `locale`='deDE') OR (`ID`=26230 AND `locale`='deDE') OR (`ID`=26229 AND `locale`='deDE') OR (`ID`=26228 AND `locale`='deDE') OR (`ID`=26215 AND `locale`='deDE') OR (`ID`=26214 AND `locale`='deDE') OR (`ID`=26213 AND `locale`='deDE') OR (`ID`=28562 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(26503, 'deDE', 'Weitere Einschätzung der Gefahr', 'Stehlt die Schlachtpläne der Gnolle, die Befehle der Gnolle und den Strategieführer der Gnolle.', 'Diese verdammten, wertlosen Gnolle führen nichts Gutes im Schilde. Ich spüre es in meinen Knochen. Die Gnollaktivität hat sich in den letzten zwei Wochen verdreifacht. Ich befürchte, dass sie einen Angriff auf Seenhain vorbereiten.$B$BIch möchte, dass Ihr die Lager der Gnolle in dieser Gegend aufsucht und dort alle Informationen sammelt, die Ihr finden könnt. Durchsucht die Lager unmittelbar nördlich von hier, östlich von hier und südöstlich von hier. Bringt mit, was Ihr dort findet.', '', '', '', '', '', '', 23420),
(26288, 'deDE', 'Jango Fleckfell', 'Tötet 5 Mystiker der Flusspfoten, 5 Zuchtmeister der Flusspfoten und Jango Fleckfell.', 'Südlich von hier befindet sich ein Landstrich von Westfall, den wir Staubebenen nennen. Soweit wir wissen, haben die Gnolle der Region die Staubebenen zu ihrem inoffiziellen Hauptquartier erklärt. Wie bei jedem Hauptquartier gibt es auch bei diesem einen Anführer. Wenn wir diesen Angriffen einen Riegel vorschieben wollen, dann müssen wir den Gnoll ausfindig machen, der den Ton angibt, und ihm das Handwerk legen.$B$BBegebt Euch zu den Staubebenen, findet den Anführer der Gnolle, Jango Fleckfell, und tötet ihn. Erledigt auch sämtliche Gnolle, die sich Euch in den Weg stellen.', '', '', '', '', '', '', 23420),
(26761, 'deDE', 'Eine Bedrohung für das Königreich', 'Meldet Euch bei Magistrat Solomon in Seenhain im Rotkammgebirge.', 'Es besteht kein Zweifel, dass die Defias eine ernsthafte Bedrohung für das Königreich darstellen, aber wir können nicht einfach so deren Versteck betreten und hoffen, dass sich alles zum Guten wendet. Nein, ein Angriff auf die Defias erfordert Zeit und Planung. Überlasst die Logistik mir. Unser Königreich hat auch noch andere Sorgen, gegen die umgehend etwas unternommen werden muss!$B$BReist nach Osten, über Elwynn ins Rotkammgebirge und meldet Euch bei Magistrat Solomon in Seenhain. Ich lasse nach Euch schicken, wenn wir bereit sind, die Defias anzugreifen.', '', '', '', '', '', '', 23420),
(40753, 'deDE', 'Blingtron 6000', '', '', '', '', '', '', '', '', 23420),
(31593, 'deDE', 'Hab'' eins!', 'Fangt ein Kampfhaustier.', 'So weit, so gut, $n! Wenn Ihr so weitermacht, macht Euch bei dem ganzen Haustierkram bald keiner mehr was vor. Jetzt solltet Ihr auch mal ein eigenes Haustier fangen!$b$bZieht wieder los, sucht Euch ein Kampfhaustier, gegen das Ihr kämpfen könnt, greift es an und werft Eure Falle, sobald seine Gesundheit niedrig genug ist. Dann bringt mir Euren Fang zur Begutachtung.$B$BUnd immer dran denken: Ihr braucht mindestens ein Haustier der Stufe 3, um Eure Falle verwenden zu können!', '', '', '', '', '', '', 23420),
(31821, 'deDE', 'Stufenaufstieg', 'Lasst ein Haustier auf Stufe 3 aufsteigen.', 'Gut, gut. Es ist wichtig, dass Ihr Eure Haustiere bei guter Gesundheit und kampfbereit haltet!$b$bAls nächstes möchte ich, dass Ihr mehrere Kämpfe bestreitet und ein Haustier auf Stufe 3 bringt. Danach kehrt Ihr zu mir zurück und wir überlegen, wie Ihr Eure eigenen Haustiere fangen könnt!', '', '', '', '', '', '', 23420),
(26365, 'deDE', 'Heldenaufruf: Rotkammgebirge!', 'Meldet Euch bei Wachhauptmann Parker an der Turmwacht im Rotkammgebirge.', 'Die Lage hier ist zwar schlimm, aber in anderen Gegenden bebt es wortwörtlich nach dem großen Aufruhr immer noch.$B$BIch habe hier einen Bericht von Wachhauptmann Parker aus dem Rotkammgebirge, eines unserer Gebiete östlich vom Wald von Elwynn. Er ersucht die Hilfe eines Helden.$B$BSo, wie wir im Moment aufgestellt sind, kann ich keine weiteren Truppen losschicken. Könnt Ihr Parker aufsuchen und sehen, was er braucht? Ihr könnt Wachhauptmann Parker auf dem Turm bei den Drei Ecken finden.', '', '', '', '', '', '', 23420),
(26370, 'deDE', 'Rückkehr zur Späherkuppe', 'Erstattet Marschall Gryan Starkmantel an der Späherkuppe in Westfall Bericht.', 'Die Bruderschaft der Defias sollte man nicht auf die leichte Schulter nehmen, $n. Was Ihr da gesehen habt, war die Geburtsstunde einer skrupellosen Verbrecherorganisation, die vor nichts Halt machen wird, bevor Sturmwind nicht in Schutt und Asche liegt.$B$BIch möchte, dass Ihr zur Späherkuppe zurückkehrt und Gryan wissen lasst, dass ich ihm Truppen schicke. Ich hoffe, es ist noch nicht zu spät.', '', '', '', '', '', '', 23420),
(26322, 'deDE', 'Aufstieg der Bruderschaft', 'Werdet Zeuge des Aufstiegs der Bruderschaft.', 'Ich hatte ja keine Ahnung, dass van Cleef eine Tochter hatte, $n. Wenn es etwas gab, in dem der Mann außerordentlich gut war, dann war es, Geheimnisse zu bewahren.$B$BWie tragisch. Das arme Ding hat eine schreckliche Tat mit angesehen. Hätte ich doch nur etwas geahnt.$B$BWas war das? Habt Ihr etwas gehört? Scheint so, als ziehen sich die Gnolle zurück... wo sind meine Wachen?$B$BPASST AUF, $n!', 'Zeuge des Aufstiegs der Bruderschaft geworden', '', '', '', '', 'Erstattet König Anduin Wrynn in der Burg Sturmwind in Sturmwind Bericht.', 23420),
(26356, 'deDE', 'Käpt''n Sanders'' versteckter Schatz', 'Findet Käpt''n Sanders'' Truhe und öffnet sie. Darin befindet sich Eure Belohnung.', 'Und tatsächlich, ganz unten, fast am Boden des alten Henkelkruges findet sich der nächste Hinweis auf Sanders'' Schatz. An manchen Stellen ist die Tinte verwischt und das Papier riecht nach Whisky, aber zum Teil kann man den Text noch lesen: Jetz'', wo Ihr mein''n alten Whiskykrug gefunden habt, habt Ihr den Schatz schon so gut wie in der Tasche. Von der Flasche aus müsst Ihr Euch einfach nach Westen halten und runter zum Strand geh''n. Und dann heißt es schwimmen! Schwimmt schnurstracks nach Westen, bis Ihr an der Insel seid, da wo meine Schatztruhe drauf is''!', '', '', '', '', '', '', 23420),
(26355, 'deDE', 'Käpt''n Sanders'' versteckter Schatz', 'Durchsucht den leeren Henkelkrug neben der Windmühle nach dem nächsten Hinweis.', 'Als Ihr das Fass durchsucht, findet Ihr ein weiteres Stück Pergament. Darauf steht: Also, von dem Fass dort müsst Ihr Euch nach Norden halten tun. Un'' dann geht Ihr immer der Nase nach schnurstracks geradeaus, bis Ihr den leeren Henkelkrug sehen tut, der da irgendwo bei so ''ner einzeln stehenden Windmühle oben auf dem Steilhang über dem Meer liegt. Wenn Ihr Euch den Henkelkrug mal genau anguckt, dann findet Ihr vielleicht sogar, was Ihr sucht.', '', '', '', '', '', '', 23420),
(26354, 'deDE', 'Käpt''n Sanders'' versteckter Schatz', 'Findet das alte Fass in der Nähe des verfallenen Kamins und durchsucht es nach Eurem nächsten Hinweis.', 'Der Hinweis auf den Schatz lautet folgendermaßen: Gut gemacht, Kumpel! Jetz'' müsst Ihr Euch imma nach Osten halten. Im Osten die Steilhänge hoch, dann weiter nach Osten bis zur Straße. Schaut Euch nach so ''nem ollen verfallenen Kamin nahe bei der Straße um. Dort findet Ihr ''n altes Fass mit Eurem nächsten Hinweis drin.', '', '', '', '', '', '', 23420),
(32008, 'deDE', 'Marlene Trichdie', 'Meldet Euch bei Marlene Trichdie in Sturmwind.', 'Seid gegrüßt, $n! Ich bin Marlene Trichdie. Man hört in letzter Zeit ziemlich viel von Euch. Ihr scheint zu $geinem hervorragenden Zähmer:einer hervorragenden Zähmerin; heranzureifen!$B$BIhr solltet Euch mit mir in Sturmwind auf ein Gespräch treffen. Ich stehe mit den besten Zähmern in ganz Azeroth in Verbindung und kann Euch helfen, noch besser zu werden!', '', 'Ich kann es kaum erwarten, Euch zu treffen!', 'Marlene Trichdie', '', '', '', 23420),
(26353, 'deDE', 'Käpt''n Sanders'' versteckter Schatz', 'Findet Käpt''n Sanders'' Schließkiste und sucht darin nach dem nächsten Hinweis.', 'Wenn Ihr das hier lesen tut, dann heißt das, dass der alte Käpt''n Sanders in den Fluten begraben is''. Also tut mein Schatz jetz'' Euch gehör''n, Ihr braucht bloß den Hinweisen folgen. $b$bErst müsst Ihr ma'' meine Schließkiste finden. Wahrscheinlich is'' die jetz'' da an der Westküste von Westfall in der Nähe von dem Schiffswrack schon halb im Sand verschwunden. ''s gibt da an der Küste massig Schiffswracks, aber bloß einen rostigen Anker. Wenn Ihr den Anker finden tut, dann habt Ihr auch meine Schließkiste gefunden. Da müsst ihr reingucken, denn da steht der nächste Hinweis drin.', '', '', '', '', '', '', 23420),
(26349, 'deDE', 'Bedrohung an der Küste', 'Bringt Kapitän Grausohn am Leuchtturm von Westfall eine Schuppe von Trübauge dem Alten.', 'Als ich auf den Felsen mein Leben aushauchte, hatte ich keine Ahnung, was das Leben nach dem Tode für mich bereithalten würde. In jener Nacht war der Leuchtturm erloschen, weil Trübauge der Alte die Familie des Leuchtturmwärters vertrieben hatte. Die Familie kehrte zurück und zündete die Flamme wieder an, doch Trübauge brachte die willensschwächeren Murlocs dazu, den Leuchtturm noch einmal mit ihm zusammen anzugreifen. Beim zweiten Mal hatte die Familie nicht so viel Glück. Vor meinen Augen starben sie alle, ohne sich wehren zu können.$b$bTötet Trübauge den Alten, wenn Ihr ihn am Ufer seht, und bringt mir eine seiner Schuppen, dann werdet Ihr eine Belohnung erhalten.', '', '', '', '', '', '', 23420),
(26348, 'deDE', 'Ärger an der Küste', 'Tötet 7 Murlocgezeitenjäger und 7 Murlocorakel.', 'Die vielen Schiffswracks entlang der Küste sind Euch ja vermutlich schon aufgefallen. Das Große Meer ist überaus tückisch. Die Küste von Westfall muss freigehalten werden, damit die Seeleute sicher sind, wenn sie unsere Ufer erreichen. Aber die Murlocs sind ein Problem.$b$bTötet Gezeitenjäger und Orakel, dann sorge ich dafür, dass Ihr belohnt werdet.', '', '', '', '', '', '', 23420),
(26347, 'deDE', 'Bewahrer der Flamme', 'Bringt 5 Schluchtenschlamme zu Kapitän Grausohn am Leuchtturm von Westfall.', 'Die Nacht, als die Leuchtturmwärterfamilie starb, war schrecklich. Ich musste hilflos zusehen, während Trübauge der Alte den Angriff anführte. Aber die Vergangenheit lässt sich nicht ändern und meine Hauptsorge gilt jetzt den Leben der Seeleute auf dem Großen Meer. Ihre Schiffe kommen den gefährlichen Felsen entlang der Küste oft zu nahe. Da niemand mehr auf das Leuchtfeuer aufpasst, fällt diese Aufgabe jetzt mir zu.$B$BHelft mir, die Flamme am Leben zu erhalten, indem Ihr mir Schluchtenschlamm von den Schleimen im Rasenden Schlund im Nordosten bringt.', '', '', '', '', '', '', 23420),
(35586, 'deDE', 'Deadmines Reward Quest', '', '', '', '', '', '', '', '', 23420),
(26320, 'deDE', 'Eine Vision aus der Vergangenheit', 'Betretet das Versteck der Defias und folgt der Spur der Obdachlosen in die Todesminen. Betretet den Dungeon "Die Todesminen" und benutzt das Räuchergefäß, um in die Vergangenheit zu blicken.', 'Dieser Weihrauch wird Euren Körper und Euren Geist in tiefe Meditation versetzen. Sobald die Trance beginnt, werdet Ihr in die Geisterwelt übertreten. Wehrt Euch nicht gegen die Trance, sobald sie begonnen hat, $n. Erlaubt den Geistern, Euch zu zeigen, was sie wissen.$B$BIhr müsst nun aufbrechen. Begebt Euch zu den Todesminen, ein Ort, der angeblich eine große Rolle in der Geschichte Westfalls gespielt hat. Sobald Ihr im Inneren seid, benutzt den Weihrauch.$B$BIhr gelangt durch das alte Bauernhaus südlich von hier in die Todesminen. Folgt den Obdachlosen.$B$BKehrt zu Gryan zurück und berichtet ihm von dem, was Ihr erfahren habt.', 'Vision der Vergangenheit entdeckt', '', '', '', '', 'Kehrt zu Marschall Gryan Starkmantel an der Späherkuppe in Westfall zurück.', 23420),
(26319, 'deDE', 'Gelöste Geheimnisse', 'Sprecht mit Thoralius dem Weisen in Mondbruch in Westfall.', 'Es gibt hier einen Schamanen, Thoralius, der uns vielleicht helfen kann. Er ist ein Freund von Gryan und ist vor Kurzem nach Westfall gekommen, um die elementare Verwüstung zu untersuchen, die über uns hereingebrochen ist.$B$BSprecht mit Thoralius und seht, ob er etwas Licht auf die Identität dieser merkwürdigen Gestalt werfen kann.', '', '', '', '', '', '', 23420),
(26297, 'deDE', 'Der Anbruch eines neuen Tages', 'Sammelt Informationen bei der Versammlung von Mondbruch.', 'Sieht so aus, als fängt die Versammlung, von der die merkwürdige Gestalt sprach, bald an. Die Obdachlosen setzen sich in Bewegung. Ich möchte, dass Ihr an der Kundgebung teilnehmt und Informationen sammelt, $n.$B$BBegebt Euch zur Stadtmitte Mondbruchs und hört Euch die Ansprache an. Kehrt zu mir zurück und berichtet, was Ihr herausfinden konntet.', '', '', '', '', '', '', 23420),
(26296, 'deDE', 'Beweissammlung', 'Sammelt 6 rote Kopftücher.', 'Dieses Kopftuch besteht aus dem gleichen Stoff, der auch am Tatort des Brauenwirbelmordes gefunden wurde. Ihr solltet noch mehr rote Kopftücher als Beweis sammeln und Euren Fund zu Hauptmann Alpert bringen.', '', '', '', '', '', '', 23420),
(26295, 'deDE', 'Propaganda', 'Erhaltet ein Informationsflugblatt, ein geheimes Tagebuch, eine Ausgabe des Mondbrucher Blatts und mysteriöse Propaganda.', 'Uns ist ein Anstieg der Gewalttaten unter den Obdachlosen in dieser Gegend aufgefallen. Irgendetwas oder irgendjemand versetzt sie ganz schön in Aufruhr. Wer auch immer dahinter steckt, organisiert sich immer besser. Erst vor Kurzem ist eine Gruppe Schläger hier durchgekommen und hat Flugblätter verteilt.$B$BGeht nach Mondbruch und sucht nach Beweisen. Propaganda, Handzettel, egal was... Bringt mir alles, was Ihr finden könnt. Vielleicht können wir ja den Ursprung der Schriften finden.', '', '', '', '', '', '', 23420),
(26292, 'deDE', 'Auf nach Mondbruch!', 'Meldet Euch bei Hauptmann Alpert in Mondbruch in Westfall.', 'Wir haben schon eine ganze Weile ein Lager in der Nähe von Mondbruch, $n. Einer meiner besten Soldaten ist zusammen mit einer Kompanie von der Späherkuppe in diesem Gebiet stationiert. Meldet Euch umgehend bei Hauptmann Alpert und findet heraus, ob er irgendwelche Neuigkeiten über die Vorgänge in Mondbruch hat.$B$BEin weiterer Verbündeter von mir ist auch in dem Mondbrucher Lager. Sein Name ist Thoralius. Ich habe Thoralius in Nordend während meiner Dienstzeit in Valgarde kennengelernt. Er ist ein weiser und mächtiger Schamane. Vielleicht können uns seine Fähigkeiten dabei helfen, diesen Fall zu lösen.', '', '', '', '', '', '', 23420),
(26291, 'deDE', 'Viel Ärger in Mondbruch', 'Berichtet Marschall Gryan Starkmantel an der Späherkuppe in Westfall, was Ihr herausgefunden habt.', 'Ich werde bald meinen Bericht schreiben und nach Sturmwind zurückkehren. Der SI:7 muss über die Situation in Westfall informiert werden. Ich schlage vor, Ihr tut das gleiche und erstattet Gryan Bericht.', '', '', '', '', '', '', 23420),
(26290, 'deDE', 'Geheimnisse des Turms', 'Betretet Jagdrufs Turm und macht Helix Ritzelbrecher ausfindig.', 'Helix hat sich oben im alten Turm von Klaven Jagdruf verschanzt. Ich werde Euch nicht belügen, $n, er wird schwer bewacht. Ich habe allein im Freien sechs bewaffnete Söldner gezählt. Ihr schafft es nie allein in den Turm. Zu Eurem Glück sind Claire und ich hier.$B$B<Kearnen tätschelt ihr Heckenschützengewehr.>$B$BIch gebe Euch Deckungsfeuer.$B$BNehmt diesen Verschleierungstrank; Ihr werdet ihn brauchen, wenn Ihr es ins Innere geschafft habt. Trinkt ihn, sobald Ihr den Turm betretet und steigt dann den Turm hinauf. Findet heraus, was Helix dort oben versteckt!', 'Helix'' Geheimnis gelüftet', '', '', '', '', 'Kehrt zu Agentin Kearnen in den Staubebenen in Westfall zurück.', 23420),
(26510, 'deDE', 'Wir müssen uns vorbereiten!', 'Findet den Gnomcorder.', 'Jeden Tag verschwinden weitere Bürger. Wir glauben, dass die Gnolle dafür verantwortlich sind, aber wir wissen nicht, wo sie unsere Bürger hinbringen! Wenn Ihr uns helfen wollt, die Gnolle zu besiegen und unsere Bürger zu finden, müsst Ihr ordentlich ausgerüstet sein. Leider haben wir keine Gnomcorder mehr. Ohne einen Gnomcorder können wir während Eures Einsatzes nicht mit Euch kommunizieren.$B$BUnseren letzten funktionstüchtigen Gnomcorder haben die Murlocs letzte Woche bei einem Überfall gestohlen. Begebt Euch östlich von hier zum Immerruhsee und bringt ihn an Euch!', '', '', '', '', '', '', 23420),
(26289, 'deDE', 'Findet Agentin Kearnen', 'Findet Agentin Kearnen.', 'Der SI:7 hat die Bewegungen von Helix Ritzelbrecher genau beobachtet, seit er vor zwei Wochen seinen Fuß auf Allianzboden gesetzt hat. Der Brief, den ich von Mathias Shaw erhalten habe, erklärt, dass er eine Agentin namens Kearnen nach Westfall entsandt hat, um Helix zu beschatten. Findet Agentin Kearnen, dann werdet Ihr auch Helix Ritzelbrecher finden.$B$BKearnen war vor zwei Tagen in der Stadt, um Vorräte zu besorgen. Sie hat erwähnt, dass sie in der Nähe von Jagdrufs Turm südöstlich von hier in den Staubebenen zu finden sein würde.$B$BAbmarsch, $n.', '', '', '', '', '', '', 23420),
(26286, 'deDE', 'Die Verteidigung Westfalls', 'Erhaltet die Angriffsbefehle der Gnolle.', 'Nachdem meine Dienstzeit in den Grizzlyhügeln beendet war, bin ich nach Westfall zurückgekehrt und musste das Land in einem schlimmeren Zustand als vor meinem Einsatz vorfinden.$B$BWir haben den Krieg mit tausenden von obdachlosen Bürgern bezahlt. Jetzt wandern sie hierhin aus und lassen sich auf den verlassenen Bauernhöfen und Siedlungen dieser Gegend nieder.$B$BZu allem Überfluss werden wir jetzt auch noch von den Gnollen angegriffen. Die Westfallbrigade braucht Eure Hilfe, $n. Tötet die angreifenden Gnolle und nehmt ihnen ihre Befehle ab. Da zieht jemand im Hintergrund die Fäden und wir müssen herausfinden, wer das ist!', '', '', '', '', '', '', 23420),
(26287, 'deDE', 'Die Westfallbrigade', 'Tötet 12 beliebige Gnolle der Flusspfoten, die die Späherkuppe angreifen.', 'Wir waren einst als Volksmiliz bekannt. Heutzutage kämpfen wir unter der Flagge von Sturmwind mit der vollen Unterstützung von König Anduin Wrynn. Wir haben viele Ziele hier in Westfall: die von den Defias hinterlassene Unordnung aufzuräumen, das Hoheitsgebiet der Allianz auszuweiten und Ordnung zu bewahren. Bisher versagen wir auf der ganzen Linie.$B$BDie Zeiten sind jetzt noch schlimmer, als zu dem Zeitpunkt, als die Defias Amok gelaufen sind. Jetzt, da die einheimischen Gnolle sich ebenfalls bewaffnet haben, sind wir in eine Ecke gedrängt worden. Helft uns, $n!', '', '', '', '', '', '', 23420),
(26371, 'deDE', 'Die Legende des Kapitän Grausohn', 'Findet Kapitän Grausohn beim Leuchtturm von Westfall in Westfall.', 'Habt Ihr von Kapitän Grausohn gehört? Einige sagen, er ist die Erfindung eines betrunkenen Seemannsgehirns, aber jeder in Westfall wird Euch sagen, dass Grausohn existiert! Er ist ein Geist, der den Leuchtturm von Westfall ganz im Westen, vor der Endlosen Küste, heimsucht.$B$BWagt Ihr es, das zu untersuchen?', '', '', '', '', '', '', 23420),
(26271, 'deDE', 'Die Hungrigen und die Hoffnungslosen speisen', 'Benutzt den Westfalleintopf, um 20 Obdachlose an der Späherkuppe in Westfall zu speisen.', 'Ja, ich habe von den Brauenwirbelmorden gehört. Es ist sehr tragisch, aber solche Tragödien sind in Westfall an der Tagesordnung. Hier gibt es viele Leute, die einfach nicht mehr die Mittel oder den Willen haben, sich selbst zu versorgen. Sie haben die Hoffnung aufgegeben. Könnt Ihr Euch vorstellen, wie es ist, so hoffnungslos zu sein, dass man sich seines Lebens nicht mehr erfreuen kann? Wir müssen uns gemeinsam darum bemühen, ihnen zu zeigen, dass es noch Hoffnung gibt. Dass jeder neue Tag ein besseres Leben bringt.$B$BNehmt den Westfalleintopf meiner Mutter und helft dabei, die Obdachlosen an der Späherkuppe zu versorgen.', '', '', '', '', '', '', 23420),
(26266, 'deDE', 'Hoffnung für das Volk', 'Bringt den Westfalleintopf zu Hope Saldean an der Späherkuppe.', 'Vor ungefähr vier Jahren stolperte ein kleines Mädchen mitten in der Nacht in unser Farmhaus und brach zusammen. Niemand kannte sie. Das arme, kleine Ding hatte keine Erinnerung daran, wer sie sein könnte oder woher sie kam - wahrlich eine verirrte Seele.$B$BBald darauf adoptierten wir die Kleine und nannten sie Hope - denn für uns bedeutete sie Hoffnung.$B$BJetzt ist sie eine junge Dame und kümmert sich um die Obdach- und Mittellosen von Westfall. Bringt meinen Westfalleintopf zu ihr zur Späherkuppe südlich von hier.', '', '', '', '', '', '', 23420),
(26270, 'deDE', 'Unser Dank ist Euch gewiss', 'Sprecht mit Salma Saldean auf Saldeans Hof in Westfall.', 'Es ist viele Jahre her, dass uns Hilfe angeboten wurde, $n. Wärt Ihr nicht gewesen, hätten wir es womöglich nicht über den nächsten Winter geschafft. Ihr habt Euch als eine selbstlose Person erwiesen - jemand, dem wir vertrauen können. Ich wünschte nur, wir hätten etwas von Wert, das wir Euch anbieten könnten.$B$BIch habe zwar keine Informationen, die Euch bei Eurem Vorhaben, die Mörder der Brauenwirbels zur Strecke zu bringen, von Nutzen sein werden, aber vielleicht kann Euch meine Frau weiterhelfen. Sprecht mit Salma im Haus und sie wird Euch sagen, was Ihr wissen müsst, bevor Ihr Euch wieder auf den Weg macht.', '', '', '', '', '', '', 23420),
(26257, 'deDE', 'Es lebt!', 'Wendet das Herz eines Erntebehüters auf einen überladenen Erntegolem an und benutzt dann den überladenen Erntegolem, um 25 aufgeladene Ernteschnitter zu töten.', 'Soweit ich gehört habe, müsst Ihr nur einen geeigneten Erntegolem finden und rein mit dem Herzen. Sobald das erledigt ist, solltet Ihr die volle Kontrolle über das Geschöpf haben.$B$BIch glaube, es gibt ein ganzes Feld mit überladenen Erntegolems westlich von hier auf dem Hof der Molsens. Haltet nach den Lichtbögen Ausschau.$B$BFalls Ihr einen zum Laufen bringen könnt, benutzt den Golem, um die aufgeladenen Ernteschnitter auf dem alten Molsenhof zu zerstören. Die Monster dort sind viel zerstörerischer als die auf unserem Hof hier.', '', '', '', '', '', '', 23420),
(26252, 'deDE', 'Herz des Behüters', 'Bringt das Herz eines Erntebehüters zurück zu Bauer Saldean auf Saldeans Hof.', 'Ihr habt etwas gefunden, das offenbar das Herz eines Erntebehüters ist.$B$BDem Geräusch nach zu urteilen, das von dem Gerät ausgeht, ist es noch immer voll funktionsfähig. Vielleicht solltet Ihr es zu Bauer Saldean zurückbringen.', '', '', '', '', '', '', 23420),
(26241, 'deDE', 'Westfalleintopf', 'Sammelt 6 Okraschoten, 6 Stück sehniges Fleischreißerfleisch und 6 Geiferzahnflanken.', 'Hallo, $gmein Lieber:meine Liebe;. Willkommen in unserem Zuhause. Hmm, Ihr seht aber nicht aus, als ob Ihr obdachlos wärt. Seid Ihr hier, um uns zu helfen? Wenn das so ist, dann kommt Ihr gerade rechtzeitig! Ich plane ein Abendessen für die Kinder und ich brauche ein paar einfache Zutaten.$B$BWenn Ihr mir ein paar Okraschoten, Geiferzahnflanken und sehniges Fleischreißerfleisch bringt, kann ich meinen berühmten Westfalleintopf zubereiten. Ihr könnt all diese Zutaten in der Nähe unserer Farm finden. Nehmt Euch nur vor den Erntebehütern in Acht. Das sind gnadenlose Vernichtungsmaschinen.', '', '', '', '', '', '', 23420),
(26237, 'deDE', 'Die Zeiten sind schwer', 'Tötet 10 Erntebehüter.', 'Das sind schwere Zeiten, $GBruder:Schwester;. Da jetzt die geknechteten Massen aus Sturmwind hierher nach Westfall auswandern, sind unsere Vorräte knapper denn je. Das Volk ist unruhig und braucht Hilfe.$B$BUnser Farmhaus dient schon seit Jahren als Behelfsunterkunft. Salma gibt sich alle Mühe, unsere Gäste am Leben zu erhalten, aber manchmal werden sie von den Erntebehütern erwischt. Das sind diese missratenen Ungetüme, die sich auf meinem Feld herumtreiben - ein kleines Geschenk, das die Defias zurückgelassen haben, als sie vertrieben wurden. Helft uns!', '', '', '', '', '', '', 23420),
(26236, 'deDE', 'Die Vernehmung der Saldeans', 'Sprecht mit Bauer Saldean auf Saldeans Hof in Westfall.', 'Was wissen wir also bisher? Wir müssen unseren Anhaltspunkten folgen.$B$BDrüben auf Jansens Hof habt Ihr einen durchnässten Brief und ein paar rote Stofffetzen gefunden. Hier auf dem Hof der Brauenwirbels habt Ihr ein Gespräch zwischen einer merkwürdigen Gestalt und einem Ogermagier belauscht. Ihr habt außerdem die Geständnisse von ein paar Schlägern, den Mord begangen zu haben - woraufhin Ihr die Schläger getötet habt.$B$BDa passt Einiges nicht zusammen, Grünschnabel.$B$BSüdöstlich von hier lebt ein älteres Ehepaar auf Saldeans Hof. Macht Euch auf den Weg dorthin und sprecht mit Bauer Saldean. Findet heraus, was er weiß.', '', '', '', '', '', '', 23420),
(26232, 'deDE', 'Lous Abschiedsworte', 'Belauscht die Rohlinge hinter dem Bauernhaus auf Brauenwirbels Kürbishof in Westfall.', 'So ein paar Rohlinge sind vor kurzem auf dem Gehöft aufgetaucht und haben ''ne Menge Ärger verursacht. Keine Ahnung, wo die herkommen oder für wen die arbeiten, aber ich bin mir SICHER, dass sie nichts Gutes im Schilde führen. Ich hab'' sie vielleicht über so ein paar Dinge sprechen hören, die Euch interessieren könnten.$B$BWenn Ihr Interesse habt, findet ihr die Rohlinge hinter''m Bauernhaus. Wenn man Euch ertappt oder umbringt, ich weiß nichts von Euch. Hab Euch noch nie im Leben gesehen!$B$BViel Glück, $gJunge:Mädel;.', '', '', '', '', '', '', 23420),
(26230, 'deDE', 'Festmahl oder Hungersnot', 'Sammelt 6 Kojotenschwänze und 5 Handvoll frischen Matsch.', 'Das Leben in Westfall ist schwer, $g Söhnchen:Mädchen;! Jeder Tag ist ein Kampf. Manchmal vergehen Wochen zwischen anständigen Mahlzeiten. Deswegen ist es so wichtig, dass wir uns gegenseitig beistehen. Vielleicht könnt Ihr ja mit anpacken, hm? Ich backe Matschkuchen und benötige noch ein paar Zutaten.$B$BBesorgt mir etwas frischen Matsch vom Kürbisbeet vor dem Haus und ein paar Kojotenschwänze. Ihr könnt überall in der Wildnis von Westfall Kojoten finden.$B$BAn die Arbeit! Wir haben Mägen zu füllen.', '', '', '', '', '', '', 23420),
(26229, 'deDE', '"Ich NEHMEN Kerze!"', 'Tötet 12 Koboldbuddler.', 'Bevor diese verdammten Kobolde mir mein Hinkebein verpasst haben, war ich William Pestles bester Kerzenlieferant. Von mir kommt sogar der Ausspruch "ICH NEHMEN KERZE!" Jep, das war ich. Die Kobolde haben immer gerufen "Du nicht nehmen Kerze!" und ich hab geantwortet "ICH NEHMEN KERZE!" und ihnen den Schädel eingeschlagen!$B$B<Jimb träumt einen Moment lang.>$B$BDas waren gute Zeiten... Die besten Zeiten.$B$BUnd jetzt? Ich kann noch nicht mal ohne Hilfe auf die Toilette gehen. Wollt Ihr mir helfen? Tötet ein paar Kobolde für mich. Ihr solltet einen ganzen Haufen von denen in der Mine hier in der Nähe finden.', '', '', '', '', '', '', 23420),
(26228, 'deDE', 'Wie die Made im Speck', 'Benutzt Lou Zweischuhs altes Haus am Ende des Jangoschachtes in Westfall.', 'Hört zu, eigentlich sollte ich gar nich'' mit Euch reden, aber ich schulde Horatio den einen oder anderen Gefallen. Ich weiß eigenlich gar nichts darüber, was mit den Brauenwirbels passiert is'', aber vielleicht kann ich Euch trotzdem weiterhelfen.$B$B<Lou reicht Euch eine große Kiste.>$B$BDiese Kiste war mein Zuhause, bevor ich reich geworden bin. Jetzt führ'' ich ein Leben, von dem jeder Landstreicher träumt!$B$BAlso, Ihr schnappt Euch jetzt mal diese Wohnkiste hier und macht Euch auf zum Jangoschacht südwestlich von hier. Dringt bis ans Ende der Mine vor und versteckt Euch dort in der Kiste.', 'Wie die Made im Speck.', '', '', '', '', 'Kehrt zu Lou Zweischuh auf Brauenwirbels Kürbishof in Westfall zurück.', 23420),
(26215, 'deDE', 'Ein Treffen mit Lou Zweischuh', 'Sprecht mit Lou Zweischuh auf Brauenwirbels Kürbishof in Westfall.', 'Anscheinend haben wir hier einen echten Fall, Grünschnabel. Leider wollen die Anwohner offenbar nicht reden und die Anhaltspunkte, die Ihr von den Gnollen und Murlocs gewonnen habt, sind so gut wie wertlos. Wir müssen auf Plan... "Haltet nach Lou Zweischuh Ausschau" zurückgreifen.$B$BLou Zweischuh ist ein alter Informant von mir, der sich auf dem alten Kürbishof der Brauenwirbels niedergelassen hat. Geht nach Westen zu dem Gehöft und findet heraus, was Lou weiß.$B$BFalls er Euch irgendwelchen Mumpitz erzählt, sagt ihm, dass Horatio Euch schickt.', '', '', '', '', '', '', 23420),
(26214, 'deDE', 'Auf der Spur: Murlocs', 'Findet den Murlochinweis.', 'Egal für wie sinnlos ich diese Sache halte, wir müssen allen unseren Anhaltspunkten auf den Grund gehen.$B$BIch möchte, dass Ihr Euch hinaus zur Endlosen Küste im Westen begebt und den Murlocs dort ein wenig einheizt. Versucht einen Hinweis oder Informationen aufzuspüren, die etwas Licht in diesen Mordfall bringen können.$B$BKehrt zu mir zurück, falls Ihr Erfolg habt.', '', '', '', '', '', '', 23420),
(26213, 'deDE', 'Auf heißer Spur: Der Flusspfotenklan', 'Erhaltet den Hinweis der Flusspfotengnolle.', 'Da wir keine anderen Anhaltspunkte haben, können wir genauso gut auch den Informationen nachgehen, die Ihr erhalten habt.$B$BEs gibt einige Lager der Flusspfotengnolle hier in der Nähe. Macht sie ausfindig und sucht nach Hinweisen. Bringt mir was Ihr finden könnt. Beschränkt Eure Suche auf die Gnolle und die Späher der Flusspfoten.', '', '', '', '', '', '', 23420),
(28562, 'deDE', 'Heldenaufruf: Westfall!', 'Meldet Euch bei Leutnant Horatio Laine auf Jansens Hof in Westfall.', 'Auf Anweisung seiner königlichen Hoheit König Anduin Wrynn ergeht hiermit an alle tauglichen Mitglieder der Allianz der Befehl, sich umgehend bei Leutnant Horatio Laine auf Jansens Hof einzufinden.$B$BWestfall wird von Bettlern und Vertriebenen überrannt, was zu einem massiven Anstieg der Kriminalität geführt hat. Helft bei der Wiederherstellung der öffentlichen Ordnung. Jansens Hof liegt in Westfall, westlich des Walds von Elwynn.$B$BFür den Ruhm und die Ehre der Allianz!', '', '', '', '', '', '', 23420);



DELETE FROM `quest_objectives_locale` WHERE (`ID`=268815 AND `locale`='deDE') OR (`ID`=269059 AND `locale`='deDE') OR (`ID`=266967 AND `locale`='deDE') OR (`ID`=255541 AND `locale`='deDE') OR (`ID`=265535 AND `locale`='deDE') OR (`ID`=256435 AND `locale`='deDE') OR (`ID`=265232 AND `locale`='deDE');
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(268815, 'deDE', 31593, 0, 'Fangt ein Haustier', 23420),
(269059, 'deDE', 31821, 0, 'Haustier der Stufe 3', 23420),
(266967, 'deDE', 26297, 0, 'Informationen während der Versammlung von Mondbruch gesammelt', 23420),
(255541, 'deDE', 26287, 0, 'Angreifenden Gnoll der Flusspfoten getötet', 23420),
(265535, 'deDE', 26271, 0, 'Obdachlose Westfalls verköstigt', 23420),
(256435, 'deDE', 26257, 0, 'Überladenen Erntegolem aktiviert', 23420),
(265232, 'deDE', 26232, 0, 'Rohlinge belauschen', 23420);


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text     xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_offer_reward` WHERE `ID` IN (26365 /*26365*/, 26288 /*26288*/, 26287 /*26287*/, 26370 /*26370*/, 40753 /*40753*/, 31821 /*31821*/, 32008 /*32008*/, 26322 /*26322*/, 26347 /*26347*/, 26320 /*26320*/, 26356 /*26356*/, 26355 /*26355*/, 26354 /*26354*/, 26353 /*26353*/, 26348 /*26348*/, 26349 /*26349*/, 26371 /*26371*/, 26319 /*26319*/, 26297 /*26297*/, 26295 /*26295*/, 26296 /*26296*/, 26292 /*26292*/, 26291 /*26291*/, 26290 /*26290*/, 26289 /*26289*/, 26286 /*26286*/, 26271 /*26271*/, 26266 /*26266*/, 26270 /*26270*/, 26237 /*26237*/, 26252 /*26252*/, 26241 /*26241*/, 26236 /*26236*/, 26232 /*26232*/, 26228 /*26228*/, 26230 /*26230*/, 26229 /*26229*/, 26215 /*26215*/, 26214 /*26214*/, 26213 /*26213*/, 26209 /*26209*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(26365, 0, 0, 0, 0, 0, 0, 0, 0, 'Ich habe gehört, dass Westfall wirklich in der Klemme steckt.$B$B<Parker sieht Euch nicht einmal an, als er mit Euch spricht.>$B$BNun, wir haben hier selbst Probleme. Seht Ihr die Gnolle dort? Es sieht so aus, als haben sie wieder etwas vor. Das gefällt mir nicht. Das gefällt mir überhaupt nicht.', 23420), -- 26365
(26288, 4, 396, 2, 0, 0, 0, 0, 0, 'Ausgezeichnete Arbeit, $n. Die Flusspfoten werden Wochen brauchen, um sich von dem Schlag zu erholen, den Ihr ihnen heute zugefügt habt. Bitte wählt einen Gegenstand als Belohnung für Eure Tat aus.', 23420), -- 26288
(26287, 396, 396, 0, 0, 0, 0, 0, 0, 'Gut gemacht, $n. Ihr habt uns eine Atempause von dem Sturmangriff der Gnolle verschafft, aber es gibt noch mehr zu tun.', 23420), -- 26287
(26370, 1, 1, 0, 0, 0, 0, 0, 0, 'Fünf Jahre Arbeit innerhalb von fünf Minuten zu Asche verbrannt. Verfluchte Defias!$B$BUnd das ist erst der Anfang, $n! Wir haben einen langen, anstrengenden Weg vor uns.', 23420), -- 26370
(40753, 0, 0, 0, 0, 0, 0, 0, 0, 'Ich bin darauf programmiert, Geschenke zu verteilen, während Ihr meiner Beobachtung nach darauf programmiert seid, Geschenke zu empfangen.$b$bSollen wir eine für beide Seiten vorteilhafte Transaktion einleiten?', 23420), -- 40753
(31821, 0, 0, 0, 0, 0, 0, 0, 0, 'Gut gemacht, $n! Als Nächstes müsst Ihr lernen, Eure eigenen Kampfhaustiere zu fangen!', 23420), -- 31821
(32008, 1, 0, 0, 0, 0, 0, 0, 0, 'Ach, Ihr seid $n. Wunderbar! Dann machen wir uns gleich an die Arbeit, ja?', 23420), -- 32008
(26322, 5, 1, 0, 0, 0, 0, 0, 0, 'Ich werde sofort ein Bataillon nach Westfall entsenden! Nun da die Bruderschaft der Defias neu gegründet worden ist, ist eine alte Bedrohung für das Königreich wiederauferstanden.', 23420), -- 26322
(26347, 396, 1, 0, 0, 0, 0, 0, 0, 'Ich muss Euch für Euren Mut loben, $C. Die Felsen an der Küste Westfalls werden dank Eurer harten Arbeit beleuchtet sein. Solange das Feuer brennt, werden viele Leben verschont bleiben.$B$BMein Tod an diesem Strand war unnötig. Meine Aufgabe im Jenseits ist es, dafür zu sorgen, dass niemand dem gleichen Schicksal erliegt.', 23420), -- 26347
(26320, 5, 1, 0, 0, 0, 0, 0, 0, 'WAS? Van Cleef hatte eine Tochter?', 23420), -- 26320
(26356, 0, 0, 0, 0, 0, 0, 0, 0, 'Die Scharniere der alten Truhe sind rostig, aber sie funktionieren noch. Ihr brecht die Truhe auf und nehmt die Beute heraus.$b$bHerzlichen Glückwunsch!', 23420), -- 26356
(26355, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr seid bei Eurer Schatzsuche auf dem richtigen Weg!', 23420), -- 26355
(26354, 0, 0, 0, 0, 0, 0, 0, 0, 'Gut gemacht! Das war der nächste Schritt auf der Schatzsuche.', 23420), -- 26354
(26353, 0, 0, 0, 0, 0, 0, 0, 0, 'Der Schließkasten knarrt als Ihr ihn langsam öffnet. Sand und Wasser scheinen der einzige Inhalt zu sein. Aber halt! Eine kleine Krabbe huscht mit einem Hinweis auf den Schatz in den Scheren davon.', 23420), -- 26353
(26348, 0, 0, 0, 0, 0, 0, 0, 0, 'Gut gemacht, $n. Ihr habt ein ziemliches Talent für den Kampf. Dank Euch ist die Küste Westfalls jetzt sicherer als zuvor.', 23420), -- 26348
(26349, 0, 0, 0, 0, 0, 0, 0, 0, 'Also ist Trübauge, dieser garstige Heide, endlich tot. Gut gemacht, $n. Durch Eure Hand ist ein Leben beendet worden, doch vielleicht wurden gerade durch diese Tat viele weitere Leben gerettet. So gefährlich das Große Meer auch ist, diese Nacht wird es dank Eurer heldenhaften Tat ein wenig sicherer sein.', 23420), -- 26349
(26371, 1, 1, 0, 0, 0, 0, 0, 0, 'Ja... Ich war auch einst aus Fleisch und Blut, genau wie Ihr, $gmein Junge:meine Liebe;. Bis zu dem schicksalsträchtigen Tag, als die Murlocs den Leuchtturm angriffen...', 23420), -- 26371
(26319, 273, 1, 0, 0, 0, 0, 0, 0, 'Hmm, ja, vielleicht kann ich Euch helfen. Als ich in Nordend war, habe ich den Einwohnern von Valgarde dabei geholfen, einige der Geheimnisse der Vrykul aufzudecken, indem ich in die Geisterwelt eingetaucht bin. Vielleicht kann ich dasselbe auch hier tun.', 23420), -- 26319
(26297, 1, 1, 0, 0, 0, 0, 0, 0, 'Ich konnte die Jubelschreie von hier aus hören. Wir haben es anscheinend mit einem riesigen Problem zu tun, $n. Wir müssen die Identität dieser schattenhaften Bedrohung aufdecken, bevor wir die Hilfe Sturmwinds anfordern. Vielleicht gibt es eine andere Möglichkeit...', 23420), -- 26297
(26295, 6, 396, 0, 0, 0, 0, 0, 0, 'V? Der einzige V, den ich kenne, ist van Cleef und der ist vor vielen Jahren gestorben.', 23420), -- 26295
(26296, 1, 6, 0, 0, 0, 0, 0, 0, 'Ich habe seit fünf Jahren keine roten Kopftücher mehr gesehen. Ob diese merkwürdige Gestalt wohl versucht, die Bruderschaft der Defias neu zu gründen?', 23420), -- 26296
(26292, 396, 1, 1, 0, 0, 0, 0, 0, 'Die Einheimischen werden unruhig, $n. Ich weiß nicht, was los ist, aber die Aktivitäten der Obdachlosen in dieser Gegend haben sich in den letzten zwei Tagen vervierfacht. Wenn ich es nicht besser wüsste, würde ich sagen, die hecken etwas aus...', 23420), -- 26292
(26291, 6, 1, 0, 0, 0, 0, 0, 0, 'Wer ist wohl diese merkwürdige Gestalt? Wir müssen nach Mondbruch gelangen.', 23420), -- 26291
(26290, 0, 0, 0, 0, 0, 0, 0, 0, 'Was um alles in der Welt könnte denn in Mondbruch los sein? Das alles schmeckt mir überhaupt nicht, $n.', 23420), -- 26290
(26289, 0, 0, 0, 0, 0, 0, 0, 0, 'Helix steckt hinter den Gnollangriffen?$B$B<Kearnen nickt dem Turm zu.>$B$BEr ist da drin. Er ist schon seit Tagen dort. Ich warte nur auf einen Grund, ihn hopszunehmen.', 23420), -- 26289
(26286, 6, 1, 0, 0, 0, 0, 0, 0, 'Helix? Den Namen habe ich doch schon mal gehört. Und zwar vor gar nicht so langer Zeit, aber wer ist dieser Admiral?', 23420), -- 26286
(26271, 1, 1, 0, 0, 0, 0, 0, 0, 'Ihr habt ein gutes Herz, $n. Ihr habt heute eine gute Tat vollbracht und das wird Euch am jüngsten Tag nicht vergessen werden.', 23420), -- 26271
(26266, 5, 4, 0, 0, 0, 0, 0, 0, 'Ich rieche Westfalleintopf! Hat Mutter Euch geschickt!?', 23420), -- 26266
(26270, 1, 1, 0, 0, 0, 0, 0, 0, 'Aber natürlich, $gmein Lieber:meine Liebe;. Unsere kleine Tochter arbeitet an der Späherkuppe und steht den Obdachlosen von Westfall sehr nahe. Vielleicht hat sie ja etwas mitbekommen. Bevor Ihr geht, lasst mich den Westfalleintopf einpacken und Euch ein wenig über unsere liebe kleine Tochter erzählen.', 23420), -- 26270
(26237, 1, 2, 0, 0, 0, 0, 0, 0, 'Danke, $GBruder:Schwester;. Ihr habt Salma und mir einen großen Dienst erwiesen und ich verspreche Euch, dass wir das so schnell nicht vergessen werden. Viel Geld habe ich nicht, aber Ihr könnt Euch gern an dem bisschen bedienen, das ich habe.', 23420), -- 26237
(26252, 25, 1, 0, 0, 0, 0, 0, 0, 'Ist das etwa... Ist das das Herz eines der Behüter? Ich habe Geschichten darüber gehört, wie man eins von diesen Dingern dazu benutzen kann, einen Behüter für den Eigengebrauch anzutreiben. Natürlich bin ich selbst nicht mehr in der Verfassung, solche Dinge selbst zu versuchen.', 23420), -- 26252
(26241, 1, 1, 0, 0, 0, 0, 0, 0, 'Seid gesegnet, $gmein Lieber:meine Liebe;. Diese armen Waisen haben schon seit Tagen keine warme Mahlzeit mehr bekommen.$B$BEs sieht so aus, als hätten wir genug übrig, um noch viele weitere Obdachlose Westfalls zu versorgen.', 23420), -- 26241
(26236, 0, 0, 0, 0, 0, 0, 0, 0, 'Horatio Laine? Der Mann ist ein Drecksack. So dreckig wie meine Unterwäsche.$B$BHört zu, $gmein Freund:meine Liebe;, ich erteile nicht oft solche Ratschläge, aber Ihr scheint mir $gein aufrichtiger Mann:eine aufrichtige Frau; zu sein. VERSCHWINDET AUS WESTFALL. Ihr steckt schon bis über beide Ohren im Mist. Was Ihr da gerade macht ist so, als würdet Ihr Euren Kopf unter''s Beil legen.', 23420), -- 26236
(26232, 1, 1, 0, 0, 0, 0, 0, 0, 'Wir haben es hier mit einer Organisation zu tun, Grünschnabel. Niemand bringt so einfach am helllichten Tage den reichsten Landstreicher von Westfall um, ohne dabei Zeugen zu hinterlassen. Hinter diesen Morden steckt jemand mit einer Menge Macht. Was habt Ihr bisher in Erfahrung bringen können?', 23420), -- 26232
(26228, 274, 1, 5, 274, 0, 0, 0, 0, '<Lou hält sich die Ohren zu und fängt an, laut zu sprechen.>$B$BHALT! HALT! HALT! Ich will''s gar nich'' hören! Ich will''s nich'' wissen und egal is'' es mir auch! Solche Informationen können einen in der Gegend hier das Leben kosten.$B$BEins kann ich Euch noch verraten und das war''s dann!', 23420), -- 26228
(26230, 396, 0, 0, 0, 0, 0, 0, 0, 'Mir knurrt schon der Magen, wenn ich Euch nur rieche! Wir werden heut'' Abend fürstlich essen!', 23420), -- 26230
(26229, 4, 5, 1, 273, 0, 0, 0, 0, 'Ah, SÜSSE Gerechtigkeit! Diese Kobolde abzuschlachten wird mich nicht dazu bringen, wieder wie ein normaler Mensch zu laufen, und es wird mir auch mit Sicherheit keine Arbeit einbringen, aber es tut auf jeden Fall verdammt gut.$B$B<Jimb greift in einen schmutzigen, alten Beutel.>$B$BHier, ich habe da etwas für Euch aus meiner Zeit als Koboldjäger. Vielleicht könnt Ihr damit etwas anfangen.', 23420), -- 26229
(26215, 274, 1, 24, 0, 0, 0, 0, 0, 'Ganz ehrlich, wir haben einfach keinen Platz mehr für...$B$BHoratio schickt Euch? Na gut... ähm, wie kann ich Euch denn helfen?$B$BEr is'' doch nich'' hier, oder? Haha, er weiß doch sicher, dass ich mich nich'' vor ihm versteckt hab. Hab mich nur für ein Weilchen bedeckt gehalten, versteht Ihr?', 23420), -- 26215
(26214, 1, 1, 1, 0, 0, 0, 0, 0, 'Na klasse. Ich glaube, davon habe ich einen ganzen Stapel auf meinem Schreibtisch im Wachhaus. Wir stehen also wieder ganz am Anfang. Was wissen wir? Jemand, der gern Geschichten über die Vergangenheit schreibt hat den Brauenwirbels einen Brief geschickt.$B$BWir stecken anscheinend mitten in einer Geschichts...geschichte.', 23420), -- 26214
(26213, 6, 274, 0, 0, 0, 0, 0, 0, 'Soll das so eine Art Witz sein? Ich lache nämlich nicht. Fetzen von rotem Stoff? Was zur Hölle soll das denn heißen?', 23420), -- 26213
(26209, 6, 5, 274, 0, 0, 0, 0, 0, 'Gnolle und Murlocs? Reiner Pferdemist, $gmein Junge:junge Dame;! Das waren keine Gnolle oder Murlocs. Ich habe gesehen, was Gnolle und Murlocs mit ihren Opfern machen, und so... sieht das nicht aus. Zu hübsch. Zu... perfekt.$B$BAußerdem haben die Brauenwirbels fünf Jahre lang auf diesem Gehöft gehockt. Nein, wer auch immer sie umgebracht hat, hatte einen Grund. Das hier ist Mord, ganz einfach, und wir werden der Sache auf den Grund gehen...', 23420); -- 26209


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text     xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_greeting` WHERE (`ID`=107574 AND `Type`=0) OR (`ID`=392 AND `Type`=0) OR (`ID`=42425 AND `Type`=0) OR (`ID`=821 AND `Type`=0) OR (`ID`=42308 AND `Type`=0);
INSERT INTO `quest_greeting` (`ID`, `Type`, `GreetEmoteType`, `GreetEmoteDelay`, `Greeting`, `VerifiedBuild`) VALUES
(107574, 0, 66, 0, 'Mein Vater war ein großartiger Mann und ein ehrenhafter König. Sein Tod ist ein großer Verlust für die Allianz.', 23420), -- 107574
(392, 0, 0, 0, 'Erschreckt nicht, $R. Ich bin seit langem schon aus diesem Land geschieden, aber ich habe nicht vor, Euresgleichen Schaden zuzufügen. Ich habe in meinem Leben zu viel Tod gesehen. Mein einziger Wunsch ist der nach Frieden. Vielleicht könnt Ihr mir dabei helfen.', 23420), -- 392
(42425, 0, 1, 0, 'Achtet auf Euren Rücken, $n. Räuber und Söldner lauern in den Schatten.', 23420), -- 42425
(821, 0, 0, 0, 'Die Diebe glauben, sie könnten uns von unserem Land vertreiben. Wer so wie wir an der Seite von Lord Starkmantel zu den Waffen gegriffen hat, kämpft nicht nur, um seinen Hof zurückzugewinnen, er kämpft für die Menschenwürde. Nehmt auch Ihr am Kreuzzug von Lord Starkmantel teil, um Westfall von dieser Tyrannei zu befreien.', 23420), -- 821
(42308, 0, 396, 0, 'Passt auf, wo Ihr hintretet, Rekrut.', 23420); -- 42308


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text     xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_request_items` WHERE `ID` IN (26347 /*26347*/, 26356 /*26356*/, 26355 /*26355*/, 26354 /*26354*/, 26349 /*26349*/, 26295 /*26295*/, 26296 /*26296*/, 26286 /*26286*/, 26266 /*26266*/, 26252 /*26252*/, 26241 /*26241*/, 26230 /*26230*/, 26214 /*26214*/, 26213 /*26213*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(26347, 0, 0, 0, 0, 'Das Feuer wird ohne Schlamm nicht sehr lang brennen, $n. Früher haben wir Öl benutzt, aber das war schwer zu beschaffen!', 23420), -- 26347
(26356, 0, 0, 0, 0, 'Der verlorene Schatz von Käpt''n Sanders wartet!', 23420), -- 26356
(26355, 0, 0, 0, 0, 'Es steckt ein Hinweiszettel in dem leeren Henkelkrug.', 23420), -- 26355
(26354, 0, 0, 0, 0, 'Das sieht wie das richtige Fass aus.', 23420), -- 26354
(26349, 0, 0, 0, 0, 'Habt Ihr der Bedrohung namens Trübauge schon den Garaus gemacht? Er ist gesehen worden, wie er an der Küste von Westfall umherstreifte.$b$bKehrt zu mir zurück, sobald das garstige Ungeheuer tot ist.', 23420), -- 26349
(26295, 6, 0, 0, 0, 'Konntet Ihr etwas finden?', 23420), -- 26295
(26296, 5, 0, 0, 0, 'Rote Kopftücher?!', 23420), -- 26296
(26286, 0, 0, 0, 0, 'Habt ihr die Angriffsbefehle?', 23420), -- 26286
(26266, 2, 0, 0, 0, 'Guten Tag $gwerter Herr:werte Dame;. Wie kann ich Euch dienen?', 23420), -- 26266
(26252, 6, 0, 0, 0, 'Was habt Ihr denn da, $n?', 23420), -- 26252
(26241, 1, 0, 0, 0, 'Das Essen ist fertig, sobald ich alle Zutaten habe. Beeilt Euch!', 23420), -- 26241
(26230, 396, 0, 0, 0, 'Matschkuchen backen sich nicht von selbst - normalerweise.', 23420), -- 26230
(26214, 6, 0, 0, 0, 'Wie läuft die Murlocjagd?', 23420), -- 26214
(26213, 6, 0, 0, 0, 'Habt Ihr etwas über die Gnolle herausgefunden?', 23420); -- 26213



DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=4106 AND `id`=1) OR (`menu_id`=4106 AND `id`=0) OR (`menu_id`=704 AND `id`=0) OR (`menu_id`=19171 AND `id`=0) OR (`menu_id`=14991 AND `id`=1) OR (`menu_id`=14991 AND `id`=0) OR (`menu_id`=4107 AND `id`=0) OR (`menu_id`=1781 AND `id`=0) OR (`menu_id`=11635 AND `id`=1) OR (`menu_id`=11635 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(4106, 1, '', '', 'Ich muss ganz schnell verreisen! Die Nachricht ist von größter Wichtigkeit!', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4106, 0, '', '', 'Ich brauche einen Flug.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(704, 0, '', '', 'Ich brauche einen Flug.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19171, 0, '', '', 'Lasst uns feiern!', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14991, 1, '', '', 'Wollt Ihr Eure Kampfsteine eintauschen?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14991, 0, '', '', 'Möchtet Ihr einige seltene Begleiter fangen?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4107, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1781, 0, '', '', 'Ich würde gern sehen, was Ihr zu verkaufen habt.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11635, 1, '', '', 'Vielleicht lockern ein paar Kupfermünzen Eure Zunge. Sagt mir jetzt, habt Ihr gesehen, wer die Brauenwirbels getötet hat?', '', '', '', '', '', '', '', 'Seid Ihr sicher, dass Ihr diesem Landstreicher Geld geben wollt?', '', '', '', '', ''),
(11635, 0, '', '', 'Habt Ihr gesehen, wer die Brauenwirbels getötet hat?', '', '', '', '', '', '', '', '', '', '', '', '', '');


INSERT IGNORE INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `OptionBroadcastTextID`, `action_poi_id`, `box_coded`, `box_money`, `box_text`, `BoxBroadcastTextID`) VALUES
(4106, 1, 2, 'Ich muss ganz schnell verreisen! Die Nachricht ist von größter Wichtigkeit!', 0, 0, 0, 0, '', 0),
(4106, 0, 2, 'Ich brauche einen Flug.', 0, 0, 0, 0, '', 0),
(704, 0, 2, 'Ich brauche einen Flug.', 0, 0, 0, 0, '', 0),
(19171, 0, 0, 'Lasst uns feiern!', 0, 0, 0, 0, '', 0),
(14991, 1, 1, 'Wollt Ihr Eure Kampfsteine eintauschen?', 0, 0, 0, 0, '', 0),
(14991, 0, 3, 'Möchtet Ihr einige seltene Begleiter fangen?', 0, 0, 0, 0, '', 0),
(4107, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(1781, 0, 1, 'Ich würde gern sehen, was Ihr zu verkaufen habt.', 0, 0, 0, 0, '', 0),
(11635, 1, 0, 'Vielleicht lockern ein paar Kupfermünzen Eure Zunge. Sagt mir jetzt, habt Ihr gesehen, wer die Brauenwirbels getötet hat?', 0, 0, 0, 2, 'Seid Ihr sicher, dass Ihr diesem Landstreicher Geld geben wollt?', 0),
(11635, 0, 0, 'Habt Ihr gesehen, wer die Brauenwirbels getötet hat?', 0, 0, 0, 0, '', 0);


-- unknown broadcasttextid
INSERT IGNORE INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `comment`) VALUES
(98, 0, 0, 'Grrr... Frischfleisch!', 12, 0, 100, 0, 0, 0, 0, 'Zuchtmeister der Flusspfoten to Player'),
(98, 1, 0, '%s versucht zu flüchten!', 16, 0, 100, 0, 0, 0, 0, 'Zuchtmeister der Flusspfoten'),
(98, 2, 0, '%s sieht den Vorarbeiter sterben und rennt angsterfüllt fort!', 16, 0, 100, 0, 0, 0, 0, 'Zuchtmeister der Flusspfoten to Jango Fleckfell'),
(234, 0, 0, 'Ihr habt unsere volle Unterstützung, Leutnant. Die Brauenwirbels waren gute Leute. Ihr Tod wird nicht ungestraft bleiben.', 12, 0, 100, 1, 0, 0, 0, 'Marschall Gryan Starkmantel to Player'),
(234, 1, 0, 'Wir haben dieses Untier erwischt, wie es im Umland Schafe abschlachtete. Er ist halsstarrig und leicht reizbar. Haltet Euch von ihm fern, Leutnant.', 12, 0, 100, 1, 0, 0, 0, 'Marschall Gryan Starkmantel to Player'),
(234, 2, 0, 'Wisst Ihr etwas darüber, Untier? Wer ist der Admiral? Sprecht!', 12, 0, 100, 6, 0, 0, 0, 'Marschall Gryan Starkmantel to Player'),
(234, 3, 0, 'Leeres Geschwätz eines schwachsinnigen Wolfs. Pah!', 12, 0, 100, 5, 0, 0, 0, 'Marschall Gryan Starkmantel to Player'),
(235, 0, 0, 'Habt Dank, $n. Eure Freundlichkeit wird nicht vergessen werden.', 12, 7, 100, 2, 0, 0, 0, 'Salma Saldean to Player'),
(235, 1, 0, 'Abendessen ist fertig! Kommt herein und esst!', 14, 7, 100, 22, 0, 0, 0, 'Salma Saldean to Player'),
(391, 0, 0, '%s wird wütend!', 16, 0, 100, 0, 0, 0, 0, 'Trübauge der Alte'),
(453, 0, 0, 'Mehr Knochen zum Nagen...', 12, 0, 100, 0, 0, 0, 0, 'Mystiker der Flusspfoten to Player'),
(1236, 0, 0, 'Du nicht nehmen Kerze!', 12, 0, 100, 0, 0, 0, 0, 'Koboldbuddler to Player'),
(3518, 0, 0, 'Frisch gebackenes Brot zu verkaufen!', 12, 7, 100, 0, 0, 0, 0, 'Thomas Müller to Ereignisgenerator 001'),
(7024, 0, 0, 'Keine Chance.', 15, 0, 100, 0, 0, 0, 0, 'Agentin Kearnen to Player'),
(7024, 1, 0, 'Hab ihn!', 15, 0, 100, 0, 0, 0, 0, 'Agentin Kearnen to Player'),
(7024, 2, 0, 'Kopfschuss!', 15, 0, 100, 0, 0, 0, 0, 'Agentin Kearnen to Player'),
(7024, 3, 0, 'Kinderkram!', 15, 0, 100, 0, 0, 0, 0, 'Agentin Kearnen to Player'),
(28347, 0, 0, 'Tu das nächste Mal nur die Hälfte rein.', 12, 0, 100, 0, 0, 0, 0, 'Miles Sidney to Wright Williams'),
(42308, 0, 0, 'Das freut mich zu hören, Marschall. Obwohl $n den Tätern ihre gerechte Strafe hat zukommen lassen, befindet sich ihr Anführer noch auf freiem Fuß. Vielleicht werden wir mit Hilfe der Westfallbrigade den Fall endlich aufklären können.', 12, 0, 100, 1, 0, 0, 0, 'Leutnant Horatio Laine to Player'),
(42308, 1, 0, 'Darf ich fragen, was mit dem Worgen im Verschlag ist?', 12, 0, 100, 6, 0, 0, 0, 'Leutnant Horatio Laine to Player'),
(42308, 2, 0, 'Oha, der Marschall hat nicht gescherzt.', 12, 0, 100, 0, 0, 0, 0, 'Leutnant Horatio Laine to Player'),
(42308, 3, 0, 'Aber vielleicht bellt Ihr auch nur...', 12, 0, 100, 0, 0, 0, 0, 'Leutnant Horatio Laine to Player'),
(42308, 4, 0, '... und beißt nicht wirklich.', 12, 0, 100, 0, 0, 0, 0, 'Leutnant Horatio Laine to Player'),
(42371, 0, 0, 'Vati...', 12, 0, 100, 0, 0, 0, 0, 'Vanessa van Cleef to Edwin van Cleef'),
(42383, 0, 0, 'Ich frage mich, ob man nicht auch Steine essen könnte. Hier gibt es haufenweise Steine. Stellt Euch das doch bloß mal vor! Durch diese Entdeckung wäre ich die reichste Person der Welt!', 12, 7, 100, 4, 0, 0, 0, 'Durchreisender'),
(42383, 1, 0, 'Ich vermisse es geradezu, von den Defias ausgeraubt zu werden. Zumindest lassen sie einem von Zeit zu Zeit etwas zu essen da.', 12, 7, 100, 396, 0, 0, 0, 'Durchreisender'),
(42383, 2, 0, 'Wer hat die Brauenwirbels getötet? Ich werde Euch sagen, wer die Brauenwirbels getötet hat: KÖNIG VARIAN WRYNN WAR ES! Und er wird uns Übrige ebenfalls töten. Einen nach dem anderen. Das Einzige, was ich Euch sagen kann, dass ich ein paar Stunden, bevor die Polizei eintraf, ein paar Gnolle verschwinden sah.', 12, 0, 100, 5, 0, 0, 0, 'Durchreisender to Player'),
(42383, 3, 0, 'HE! HE, IHR DA! VERSCHWINDET VON MEINEM GRUND UND BODEN!', 12, 7, 100, 22, 0, 0, 0, 'Durchreisender'),
(42383, 4, 0, 'Westfalleintopf? Diesen Augenblick vergesse ich nie!', 12, 7, 100, 5, 0, 0, 0, 'Durchreisender to Westfalleintopf'),
(42384, 0, 0, 'Hört zu, $g Kumpel:meine Dame;, ich möchte keinen Ärger, verstanden? Ich habe nicht gesehen, wer sie ermordet hat, doch ganz sicher gehört! Es war ein Riesengeschrei. Habt Ihr menschliche Stimmen darunter ausgemacht? Jetzt verschwindet, bevor ich es mir anders überlege, Euch zusammenschlage und Eure Schuhe mitnehme.', 12, 0, 100, 5, 0, 0, 0, 'Obdachloser Bürger von Sturmwind to Player'),
(42384, 1, 0, 'Unter uns, Murlocs haben die Brauenwirbels getötet. Ja, ich habe sie mit eigenen Augen gesehen. Seit Tagen hatten sie die Gegend beobachtet, vielleicht auch schon seit Wochen. Als sie die Spurensicherung und die dümmliche Brigade dort drüben witterten, hatten sie es sehr eilig...', 12, 0, 100, 273, 0, 0, 0, 'Obdachloser Bürger von Sturmwind to Player'),
(42384, 2, 0, 'Vielleicht werde ich Euch diese Großherzigkeit eines Tages vergelten.', 12, 7, 100, 1, 0, 0, 0, 'Obdachloser Bürger von Sturmwind to Westfalleintopf'),
(42384, 3, 0, 'Diesen Duft erkenne ich überall! Salmas berühmter Eintopf!', 12, 7, 100, 5, 0, 0, 0, 'Obdachloser Bürger von Sturmwind to Westfalleintopf'),
(42384, 4, 0, 'Was gibt Euch das Recht, uns nicht hereinzulassen?', 12, 7, 100, 6, 0, 0, 0, 'Obdachloser Bürger von Sturmwind'),
(42385, 0, 0, 'Ich bin so glücklich. Danke, Mama, und habt Dank, $n.', 12, 7, 100, 1, 0, 0, 0, 'Waise to Player'),
(42385, 1, 0, '%s weint.', 16, 7, 100, 1, 0, 0, 0, 'Waise to Player'),
(42385, 2, 0, 'EIN HOCH AUF $n!', 12, 7, 100, 1, 0, 0, 0, 'Waise to Player'),
(42386, 0, 0, 'VOLLTREFFER!', 12, 7, 100, 5, 0, 0, 0, 'Obdachlose Bürgerin von Sturmwind'),
(42386, 1, 0, 'Sieht aus als habe ich uns ein schmackhaftes und sauberes Stück Dreck aufgetrieben! Heute Abend speisen wir wie Könige, Herr Pinguin! Selbstverständlich teile ich es mit Euch. Ihr seid mein bester Freund!', 12, 7, 100, 0, 0, 0, 0, 'Obdachlose Bürgerin von Sturmwind'),
(42386, 2, 0, 'Ich... Ich habe nichts gesehen! Er... er starb eines natürlichen Todes.', 12, 7, 100, 0, 0, 0, 0, 'Obdachlose Bürgerin von Sturmwind to Player'),
(42386, 3, 0, 'Danke, freundlicher und liebenswürdiger Fremder.', 12, 7, 100, 1, 0, 0, 0, 'Obdachlose Bürgerin von Sturmwind to Westfalleintopf'),
(42387, 0, 0, 'Seid Ihr... Seid Ihr ihr begegnet?', 12, 7, 100, 6, 0, 0, 0, 'Rohling to Player'),
(42387, 1, 0, 'Ja. Sie existiert wirklich.', 12, 7, 100, 396, 0, 0, 0, 'Rohling to Player'),
(42387, 2, 0, 'Sie beauftragte mich, Euch zu sagen, dass sie die Sache mit den Brauenwirbels, die wir auf ihr Geheiß hin ausgeführt haben, honoriert. Sie gab mir einen Haufen Gold, den ich unter Euch allen aufteilen soll.', 12, 7, 100, 396, 0, 0, 0, 'Rohling to Player'),
(42387, 3, 0, 'Habt Ihr ihr Gesicht gesehen? Ist es wirklich...', 12, 0, 100, 6, 0, 0, 0, 'Rohling to Player'),
(42387, 4, 0, 'Hoppla, was haben wir denn da? Anscheinend befindet sich ein Horcher unter uns, Jungs.', 12, 0, 100, 5, 0, 0, 0, 'Rohling to Player'),
(42387, 5, 0, 'Mit einem niederträchtigen, nichtsnutzigen Horcher verfährt man folgendermaßen.', 12, 0, 100, 397, 0, 0, 0, 'Rohling to Player'),
(42387, 6, 0, 'STERBT!', 12, 0, 100, 53, 0, 0, 0, 'Rohling to Player'),
(42390, 0, 0, 'Nieder mit dem König!', 12, 7, 100, 15, 0, 0, 0, 'Schmalspurganove'),
(42391, 0, 0, 'HAHAHAH! Sehr gut, Herr Pinguin! SEHR GUT!', 12, 7, 100, 153, 0, 0, 0, 'Streunerin der Westlichen Ebenen'),
(42391, 1, 0, 'Ich habe nicht gesehen, wer sie getötet hat, $g Kumpel:meine Dame;, doch ich habe eine Ahnung. Haben nach Geld gerochen, ein bisschen so wie Ihr. So eine Schande auch. Die Brauenwirbels waren angesehen und beliebt hier. Nette Leute, immer herzlich und gastfreundlich.', 12, 0, 100, 396, 0, 0, 0, 'Streunerin der Westlichen Ebenen to Player'),
(42391, 2, 0, 'Seid gesegnet, Freund.', 12, 7, 100, 1, 0, 0, 0, 'Streunerin der Westlichen Ebenen to Westfalleintopf'),
(42492, 0, 0, 'Was will kleines Menschie? Warum Ihr rufen Glubtok?', 12, 0, 100, 396, 0, 0, 0, 'Glubtok to Player'),
(42492, 1, 0, 'Glubtok Euch zerquetschen!', 12, 0, 100, 15, 0, 0, 0, 'Glubtok to Player'),
(42492, 2, 0, 'Was Option zwei?', 12, 0, 100, 396, 0, 0, 0, 'Glubtok to Player'),
(42492, 3, 0, 'Also Glubtok haben zwei Möglichkeiten: Sterben oder reich und mächtig sein?', 12, 0, 100, 396, 0, 0, 0, 'Glubtok to Player'),
(42492, 4, 0, 'Glubtok nehmen Option zwei.', 12, 0, 100, 396, 0, 0, 0, 'Glubtok to Player'),
(42515, 0, 0, 'Traurig... Ist das das Leben, das Ihr Euch erhofft habt, Glubtok? Ein Schmalspurerpressungsunternehmen, geführt aus einer Höhle heraus?', 12, 0, 100, 0, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42515, 1, 0, 'Oh, tatsächlich? Wagt Ihr es, diese Grenze zu überschreiten und Euer Leben zu riskieren?', 12, 0, 100, 0, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42515, 2, 0, 'Ihr könnt versuchen, mich zu töten - und versagen - oder Ihr könnt Option zwei wählen.', 12, 0, 100, 0, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42515, 3, 0, 'Ihr schließt Euch mir an und ich werde Euch mit Reichtum und Macht überschütten.', 12, 0, 100, 396, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42515, 4, 0, 'Ich habe mir schon gedacht, dass Ihr es von meiner Warte aus sehen werdet.', 12, 0, 100, 153, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42515, 5, 0, 'Ich werde Euch Bescheid geben, wenn die Zeit gekommen ist.', 12, 0, 100, 397, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42559, 0, 0, 'Ihr seid genau hier gestanden! Was in aller Welt habt Ihr gesehen? Sprecht!', 12, 7, 100, 0, 0, 0, 0, 'Ermittler von Sturmwind to Player'),
(42562, 0, 0, 'Eilt zurück zu Brauenwirbels Hütte!', 42, 0, 100, 0, 0, 0, 0, 'Lous Abschiedsgedanken to Player'),
(42635, 0, 0, 'Wenn ich ausbreche, werde ich Euer Herz herausreißen und verschlingen!', 12, 0, 100, 0, 0, 0, 0, 'Knurrreißer to Player'),
(42635, 1, 0, 'Der Tag bricht an, Starkmantel, und er bringt Euer Ende.', 12, 0, 100, 0, 0, 0, 0, 'Knurrreißer to Player'),
(42655, 0, 0, 'Die Gnolle haben versagt, Herrin.', 12, 0, 100, 1, 0, 0, 0, 'Helix Ritzelbrecher to Helix'' Trampel'),
(42655, 1, 0, 'Aber Herrin, der Admiral ist immer noch...', 12, 0, 100, 1, 0, 0, 0, 'Helix Ritzelbrecher to Merkwürdige Gestalt'),
(42655, 2, 0, 'Ja, Herrin.', 12, 0, 100, 1, 0, 0, 0, 'Helix Ritzelbrecher to Merkwürdige Gestalt'),
(42655, 3, 0, 'Mondbruch, Herrin?', 12, 0, 100, 6, 0, 0, 0, 'Helix Ritzelbrecher to Merkwürdige Gestalt'),
(42662, 0, 0, 'Sie sorgten für die nötige Ablenkung. Wir machen weiter wie geplant.', 12, 0, 100, 1, 0, 0, 0, 'Merkwürdige Gestalt to Helix Ritzelbrecher'),
(42662, 1, 0, 'Wir werden den Admiral im Morgengrauen befreien.', 12, 0, 100, 1, 0, 0, 0, 'Merkwürdige Gestalt to Helix Ritzelbrecher'),
(42662, 2, 0, 'Der Tag des Jüngsten Gerichts steht kurz bevor, Helix.', 12, 0, 100, 1, 0, 0, 0, 'Merkwürdige Gestalt to Helix Ritzelbrecher'),
(42662, 3, 0, 'Ruft die Leute zusammen. Ich möchte ein allerletztes Mal mit ihnen sprechen, bevor der Morgen dämmert.', 12, 0, 100, 1, 0, 0, 0, 'Merkwürdige Gestalt to Helix Ritzelbrecher'),
(42662, 4, 0, 'Ja. Heute Nacht.', 12, 0, 100, 273, 0, 0, 0, 'Merkwürdige Gestalt to Helix Ritzelbrecher'),
(42677, 0, 0, 'Wer nicht für uns ist, ist gegen uns! STERBT!', 12, 0, 100, 0, 0, 0, 0, 'Rohling von Mondbruch to Player'),
(42677, 1, 0, 'Das Spiel ist aus, Kumpel!', 12, 0, 100, 0, 0, 0, 0, 'Rohling von Mondbruch to Player'),
(42677, 2, 0, 'Was glaubt Ihr, wohin Ihr geht?', 12, 0, 100, 0, 0, 0, 0, 'Rohling von Mondbruch to Player'),
(42680, 0, 0, 'Die Zusammenkunft beginnt in Kürze!', 42, 0, 100, 0, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42680, 1, 0, 'Brüder und Schwestern, versammelt Euch! Kommt alle her und hört zu!', 14, 0, 100, 22, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42680, 2, 0, 'Brüder. Schwestern. Man hat uns im STICH GELASSEN. Wir sind die verwaisten Kinder Sturmwinds.', 14, 0, 100, 0, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42680, 3, 0, 'Unser "König" sitzt auf seinem goldenen Thron und zuckt angesichts unserer Not die Achseln!', 14, 0, 100, 0, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42680, 4, 0, 'Währenddessen sterben unsere Kinder auf den Straßen vor Hunger!', 14, 0, 100, 0, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42680, 5, 0, 'SEIN Krieg, nicht der unsere, kostet uns die Lebensgrundlage. WIR bezahlen für die Siege der Allianz mit unserem Blut und dem unserer Lieben.', 14, 0, 100, 0, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42680, 6, 0, 'Es ist an der Zeit, Brüder und Schwestern, diese Ungerechtigkeit zu beenden!', 14, 0, 100, 0, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42680, 7, 0, 'Die Regierung von Sturmwind und der ALLIANZ muss für das, was sie uns angetan hat, zur Rechenschaft gezogen werden!', 14, 0, 100, 0, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42680, 8, 0, 'Heute ist der Tag unserer Wiedergeburt! Heute beziehen wir eine klare Position als Männer und Frauen, nicht als namen- und gesichtslose Meute!', 14, 0, 100, 0, 0, 0, 0, 'Merkwürdige Gestalt to Player'),
(42693, 0, 0, '|cFF768EBDVor fünf Jahren im Herzen der Todesminen...|r |TInterface\\QuestFrame\\UI-QUESTLOG-BOOKICON.BLP:32|t', 42, 0, 100, 0, 0, 0, 0, 'Vision der Vergangenheit to Player'),
(42697, 0, 0, 'Niemand darf die Bruderschaft herausfordern!', 14, 0, 100, 0, 0, 5780, 0, 'Edwin van Cleef to Krieger der Allianz'),
(42697, 1, 0, 'Schoßhündchen, alle miteinander!', 14, 0, 100, 0, 0, 5782, 0, 'Edwin van Cleef'),
(42697, 2, 0, '%s ruft weitere Verbündete aus den Schatten.', 16, 0, 100, 0, 0, 0, 0, 'Edwin van Cleef'),
(42697, 3, 0, 'Narren! Unsere Sache ist gerecht!', 14, 0, 100, 0, 0, 5783, 0, 'Edwin van Cleef'),
(42697, 4, 0, 'Die Bruderschaft wird siegen!', 14, 0, 100, 0, 0, 5784, 0, 'Edwin van Cleef'),
(42698, 0, 0, '%s springt aus den Schatten hervor!', 16, 0, 100, 0, 0, 5780, 0, 'Lump der Defias to Krieger der Allianz'),
(42699, 0, 0, 'Es gibt keine Versteckmöglichkeit mehr, van Cleef! Die Defias sind AM ENDE!', 14, 0, 100, 22, 0, 0, 0, 'Krieger der Allianz to Player'),
(42699, 1, 0, 'Wir sind gekommen, um der Sache ein Ende zu setzen! Stellt Euch, Feigling!', 14, 0, 100, 397, 0, 0, 0, 'Krieger der Allianz to Player'),
(42699, 2, 0, 'Sieg der Allianz! Ruhm für Sturmwind!', 14, 0, 100, 5, 0, 0, 0, 'Krieger der Allianz to Edwin van Cleef'),
(42699, 3, 0, 'Kehren wir zur Späherkuppe zurück, Verbündete, und informieren Gryan über den Tod van Cleefs!', 12, 0, 100, 396, 0, 0, 0, 'Krieger der Allianz to Edwin van Cleef'),
(42744, 0, 0, 'Verratet mir nur eins, Vanessa.', 12, 0, 100, 0, 0, 0, 0, 'Leutnant Horatio Laine to Player'),
(42744, 1, 0, 'Weshalb habt Ihr die Brauenwirbels umbringen lassen?', 12, 0, 100, 0, 0, 0, 0, 'Leutnant Horatio Laine to Player'),
(42748, 0, 0, 'Danke, meine Liebe!', 12, 0, 100, 1, 0, 0, 0, 'Admiral Knurrreißer to Player'),
(42749, 0, 0, 'Ihr Bastarde werdet für Eure Taten brennen.', 12, 0, 100, 25, 0, 0, 0, 'Hope Saldean to Player'),
(42749, 1, 0, 'Hope. Wie die Hoffnung. Hätte ich Hoffnung hegen sollen, als ich sah, wie mein Vater von Euren Spießgesellen geköpft wurde?', 12, 0, 100, 0, 0, 0, 0, 'Hope Saldean to Player'),
(42749, 2, 0, 'Hoffnung ist ein grausamer Streich, den uns eine gefühllose und brutale Welt spielt. Es gibt keinerlei Hoffnung, es gibt nur Vanessa. Vanessa van Cleef.', 12, 0, 100, 0, 0, 0, 0, 'Hope Saldean to Player'),
(42749, 3, 0, 'ERHEBT EUCH, BRÜDER UND SCHWESTERN! DER TAG BRICHT AN!', 14, 0, 100, 22, 0, 0, 0, 'Vanessa van Cleef to Player'),
(42749, 4, 0, 'Bindet sie fest!', 12, 0, 100, 25, 0, 0, 0, 'Vanessa van Cleef to Player'),
(42749, 5, 0, 'Admiral, Euer Hut.', 12, 0, 100, 1, 0, 0, 0, 'Vanessa van Cleef to Player'),
(42749, 6, 0, 'Was Euch betrifft, $n, so will ich Euer Leben schonen. Ihr habt, wenn auch unwissentlich, viel zu unserer Unterstützung beigetragen, doch bei unserer nächsten Begegnung stehen wir uns als Feinde gegenüber.', 12, 0, 100, 25, 0, 0, 0, 'Vanessa van Cleef to Player'),
(42749, 7, 0, 'Ich hatte keine Wahl, Leutnant. Sie haben mich wiedererkannt. Die einzigen Menschen der Welt, die überhaupt von meiner Existenz wussten, erkannten mein Gesicht von meiner Kinderzeit.', 12, 0, 100, 0, 0, 0, 0, 'Vanessa van Cleef to Player'),
(42749, 8, 0, 'Ich empfand keine Freude über ihren Tod.', 12, 0, 100, 1, 0, 0, 0, 'Vanessa van Cleef to Player'),
(42749, 9, 0, 'Lasst nichts als Asche zurück! Brennt die Späherkuppe nieder!', 14, 0, 100, 22, 0, 0, 0, 'Vanessa van Cleef to Vanessa van Cleef'),
(42750, 0, 0, 'Das gefällt mir nicht, $n. Bleibt wachsam!', 12, 0, 100, 0, 0, 0, 0, 'Marschall Gryan Starkmantel to Player'),
(42750, 1, 0, 'Hope! Wa...', 12, 0, 100, 5, 0, 0, 0, 'Marschall Gryan Starkmantel to Player'),
(42750, 2, 0, '$n, macht Euch auf nach Sturmwind. Erzählt König Wrynn alles. ALLES! GEHT JETZT!', 12, 0, 100, 0, 0, 0, 0, 'Marschall Gryan Starkmantel to Player'),
(43515, 0, 0, 'Folgt dem Zug der Heimatlosen zum Eingang des Todesminendungeons.', 42, 0, 100, 0, 0, 0, 0, 'Moonbrook Player Trigger to Player'),
(65648, 0, 0, 'Ich hatte einst einen Hof, aber das ist schon lange her. Ich ziehe eine gute Schlacht vor, Ihr nicht auch?', 12, 0, 100, 0, 0, 0, 0, 'Mac Donald to Player');

DELETE FROM `locales_creature_text` WHERE(`entry`=98 AND `groupid`=0) OR (`entry`=98 AND `groupid`=1) OR (`entry`=98 AND `groupid`=2) OR (`entry`=234 AND `groupid`=0) OR (`entry`=234 AND `groupid`=1) OR (`entry`=234 AND `groupid`=2) OR (`entry`=234 AND `groupid`=3) OR (`entry`=235 AND `groupid`=0) OR (`entry`=235 AND `groupid`=1) OR (`entry`=391 AND `groupid`=0) OR (`entry`=453 AND `groupid`=0) OR (`entry`=1236 AND `groupid`=0) OR (`entry`=3518 AND `groupid`=0) OR (`entry`=7024 AND `groupid`=0) OR (`entry`=7024 AND `groupid`=1) OR (`entry`=7024 AND `groupid`=2) OR (`entry`=7024 AND `groupid`=3) OR (`entry`=28347 AND `groupid`=0) OR (`entry`=42308 AND `groupid`=0) OR (`entry`=42308 AND `groupid`=1) OR (`entry`=42308 AND `groupid`=2) OR (`entry`=42308 AND `groupid`=3) OR (`entry`=42308 AND `groupid`=4) OR (`entry`=42371 AND `groupid`=0) OR (`entry`=42383 AND `groupid`=0) OR (`entry`=42383 AND `groupid`=1) OR (`entry`=42383 AND `groupid`=2) OR (`entry`=42383 AND `groupid`=3) OR (`entry`=42383 AND `groupid`=4) OR (`entry`=42384 AND `groupid`=0) OR (`entry`=42384 AND `groupid`=1) OR (`entry`=42384 AND `groupid`=2) OR (`entry`=42384 AND `groupid`=3) OR (`entry`=42384 AND `groupid`=4) OR (`entry`=42385 AND `groupid`=0) OR (`entry`=42385 AND `groupid`=1) OR (`entry`=42385 AND `groupid`=2) OR (`entry`=42386 AND `groupid`=0) OR (`entry`=42386 AND `groupid`=1) OR (`entry`=42386 AND `groupid`=2) OR (`entry`=42386 AND `groupid`=3) OR (`entry`=42387 AND `groupid`=0) OR (`entry`=42387 AND `groupid`=1) OR (`entry`=42387 AND `groupid`=2) OR (`entry`=42387 AND `groupid`=3) OR (`entry`=42387 AND `groupid`=4) OR (`entry`=42387 AND `groupid`=5) OR (`entry`=42387 AND `groupid`=6) OR (`entry`=42390 AND `groupid`=0) OR (`entry`=42391 AND `groupid`=0) OR (`entry`=42391 AND `groupid`=1) OR (`entry`=42391 AND `groupid`=2) OR (`entry`=42492 AND `groupid`=0) OR (`entry`=42492 AND `groupid`=1) OR (`entry`=42492 AND `groupid`=2) OR (`entry`=42492 AND `groupid`=3) OR (`entry`=42492 AND `groupid`=4) OR (`entry`=42515 AND `groupid`=0) OR (`entry`=42515 AND `groupid`=1) OR (`entry`=42515 AND `groupid`=2) OR (`entry`=42515 AND `groupid`=3) OR (`entry`=42515 AND `groupid`=4) OR (`entry`=42515 AND `groupid`=5) OR (`entry`=42559 AND `groupid`=0) OR (`entry`=42562 AND `groupid`=0) OR (`entry`=42635 AND `groupid`=0) OR (`entry`=42635 AND `groupid`=1) OR (`entry`=42655 AND `groupid`=0) OR (`entry`=42655 AND `groupid`=1) OR (`entry`=42655 AND `groupid`=2) OR (`entry`=42655 AND `groupid`=3) OR (`entry`=42662 AND `groupid`=0) OR (`entry`=42662 AND `groupid`=1) OR (`entry`=42662 AND `groupid`=2) OR (`entry`=42662 AND `groupid`=3) OR (`entry`=42662 AND `groupid`=4) OR (`entry`=42677 AND `groupid`=0) OR (`entry`=42677 AND `groupid`=1) OR (`entry`=42677 AND `groupid`=2) OR (`entry`=42680 AND `groupid`=0) OR (`entry`=42680 AND `groupid`=1) OR (`entry`=42680 AND `groupid`=2) OR (`entry`=42680 AND `groupid`=3) OR (`entry`=42680 AND `groupid`=4) OR (`entry`=42680 AND `groupid`=5) OR (`entry`=42680 AND `groupid`=6) OR (`entry`=42680 AND `groupid`=7) OR (`entry`=42680 AND `groupid`=8) OR (`entry`=42693 AND `groupid`=0) OR (`entry`=42697 AND `groupid`=0) OR (`entry`=42697 AND `groupid`=1) OR (`entry`=42697 AND `groupid`=2) OR (`entry`=42697 AND `groupid`=3) OR (`entry`=42697 AND `groupid`=4) OR (`entry`=42698 AND `groupid`=0) OR (`entry`=42699 AND `groupid`=0) OR (`entry`=42699 AND `groupid`=1) OR (`entry`=42699 AND `groupid`=2) OR (`entry`=42699 AND `groupid`=3) OR (`entry`=42744 AND `groupid`=0) OR (`entry`=42744 AND `groupid`=1) OR (`entry`=42748 AND `groupid`=0) OR (`entry`=42749 AND `groupid`=0) OR (`entry`=42749 AND `groupid`=1) OR (`entry`=42749 AND `groupid`=2) OR (`entry`=42749 AND `groupid`=3) OR (`entry`=42749 AND `groupid`=4) OR (`entry`=42749 AND `groupid`=5) OR (`entry`=42749 AND `groupid`=6) OR (`entry`=42749 AND `groupid`=7) OR (`entry`=42749 AND `groupid`=8) OR (`entry`=42749 AND `groupid`=9) OR (`entry`=42750 AND `groupid`=0) OR (`entry`=42750 AND `groupid`=1) OR (`entry`=42750 AND `groupid`=2) OR (`entry`=43515 AND `groupid`=0) OR (`entry`=65648 AND `groupid`=0);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(98, 0, 0, '', '', 'Grrr... Frischfleisch!', '', '', '', '', ''),
(98, 1, 0, '', '', '%s versucht zu flüchten!', '', '', '', '', ''),
(98, 2, 0, '', '', '%s sieht den Vorarbeiter sterben und rennt angsterfüllt fort!', '', '', '', '', ''),
(234, 0, 0, '', '', 'Ihr habt unsere volle Unterstützung, Leutnant. Die Brauenwirbels waren gute Leute. Ihr Tod wird nicht ungestraft bleiben.', '', '', '', '', ''),
(234, 1, 0, '', '', 'Wir haben dieses Untier erwischt, wie es im Umland Schafe abschlachtete. Er ist halsstarrig und leicht reizbar. Haltet Euch von ihm fern, Leutnant.', '', '', '', '', ''),
(234, 2, 0, '', '', 'Wisst Ihr etwas darüber, Untier? Wer ist der Admiral? Sprecht!', '', '', '', '', ''),
(234, 3, 0, '', '', 'Leeres Geschwätz eines schwachsinnigen Wolfs. Pah!', '', '', '', '', ''),
(235, 0, 0, '', '', 'Habt Dank, $n. Eure Freundlichkeit wird nicht vergessen werden.', '', '', '', '', ''),
(235, 1, 0, '', '', 'Abendessen ist fertig! Kommt herein und esst!', '', '', '', '', ''),
(391, 0, 0, '', '', '%s wird wütend!', '', '', '', '', ''),
(453, 0, 0, '', '', 'Mehr Knochen zum Nagen...', '', '', '', '', ''),
(1236, 0, 0, '', '', 'Du nicht nehmen Kerze!', '', '', '', '', ''),
(3518, 0, 0, '', '', 'Frisch gebackenes Brot zu verkaufen!', '', '', '', '', ''),
(7024, 0, 0, '', '', 'Keine Chance.', '', '', '', '', ''),
(7024, 1, 0, '', '', 'Hab ihn!', '', '', '', '', ''),
(7024, 2, 0, '', '', 'Kopfschuss!', '', '', '', '', ''),
(7024, 3, 0, '', '', 'Kinderkram!', '', '', '', '', ''),
(28347, 0, 0, '', '', 'Tu das nächste Mal nur die Hälfte rein.', '', '', '', '', ''),
(42308, 0, 0, '', '', 'Das freut mich zu hören, Marschall. Obwohl $n den Tätern ihre gerechte Strafe hat zukommen lassen, befindet sich ihr Anführer noch auf freiem Fuß. Vielleicht werden wir mit Hilfe der Westfallbrigade den Fall endlich aufklären können.', '', '', '', '', ''),
(42308, 1, 0, '', '', 'Darf ich fragen, was mit dem Worgen im Verschlag ist?', '', '', '', '', ''),
(42308, 2, 0, '', '', 'Oha, der Marschall hat nicht gescherzt.', '', '', '', '', ''),
(42308, 3, 0, '', '', 'Aber vielleicht bellt Ihr auch nur...', '', '', '', '', ''),
(42308, 4, 0, '', '', '... und beißt nicht wirklich.', '', '', '', '', ''),
(42371, 0, 0, '', '', 'Vati...', '', '', '', '', ''),
(42383, 0, 0, '', '', 'Ich frage mich, ob man nicht auch Steine essen könnte. Hier gibt es haufenweise Steine. Stellt Euch das doch bloß mal vor! Durch diese Entdeckung wäre ich die reichste Person der Welt!', '', '', '', '', ''),
(42383, 1, 0, '', '', 'Ich vermisse es geradezu, von den Defias ausgeraubt zu werden. Zumindest lassen sie einem von Zeit zu Zeit etwas zu essen da.', '', '', '', '', ''),
(42383, 2, 0, '', '', 'Wer hat die Brauenwirbels getötet? Ich werde Euch sagen, wer die Brauenwirbels getötet hat: KÖNIG VARIAN WRYNN WAR ES! Und er wird uns Übrige ebenfalls töten. Einen nach dem anderen. Das Einzige, was ich Euch sagen kann, dass ich ein paar Stunden, bevor die Polizei eintraf, ein paar Gnolle verschwinden sah.', '', '', '', '', ''),
(42383, 3, 0, '', '', 'HE! HE, IHR DA! VERSCHWINDET VON MEINEM GRUND UND BODEN!', '', '', '', '', ''),
(42383, 4, 0, '', '', 'Westfalleintopf? Diesen Augenblick vergesse ich nie!', '', '', '', '', ''),
(42384, 0, 0, '', '', 'Hört zu, $g Kumpel:meine Dame;, ich möchte keinen Ärger, verstanden? Ich habe nicht gesehen, wer sie ermordet hat, doch ganz sicher gehört! Es war ein Riesengeschrei. Habt Ihr menschliche Stimmen darunter ausgemacht? Jetzt verschwindet, bevor ich es mir anders überlege, Euch zusammenschlage und Eure Schuhe mitnehme.', '', '', '', '', ''),
(42384, 1, 0, '', '', 'Unter uns, Murlocs haben die Brauenwirbels getötet. Ja, ich habe sie mit eigenen Augen gesehen. Seit Tagen hatten sie die Gegend beobachtet, vielleicht auch schon seit Wochen. Als sie die Spurensicherung und die dümmliche Brigade dort drüben witterten, hatten sie es sehr eilig...', '', '', '', '', ''),
(42384, 2, 0, '', '', 'Vielleicht werde ich Euch diese Großherzigkeit eines Tages vergelten.', '', '', '', '', ''),
(42384, 3, 0, '', '', 'Diesen Duft erkenne ich überall! Salmas berühmter Eintopf!', '', '', '', '', ''),
(42384, 4, 0, '', '', 'Was gibt Euch das Recht, uns nicht hereinzulassen?', '', '', '', '', ''),
(42385, 0, 0, '', '', 'Ich bin so glücklich. Danke, Mama, und habt Dank, $n.', '', '', '', '', ''),
(42385, 1, 0, '', '', '%s weint.', '', '', '', '', ''),
(42385, 2, 0, '', '', 'EIN HOCH AUF $n!', '', '', '', '', ''),
(42386, 0, 0, '', '', 'VOLLTREFFER!', '', '', '', '', ''),
(42386, 1, 0, '', '', 'Sieht aus als habe ich uns ein schmackhaftes und sauberes Stück Dreck aufgetrieben! Heute Abend speisen wir wie Könige, Herr Pinguin! Selbstverständlich teile ich es mit Euch. Ihr seid mein bester Freund!', '', '', '', '', ''),
(42386, 2, 0, '', '', 'Ich... Ich habe nichts gesehen! Er... er starb eines natürlichen Todes.', '', '', '', '', ''),
(42386, 3, 0, '', '', 'Danke, freundlicher und liebenswürdiger Fremder.', '', '', '', '', ''),
(42387, 0, 0, '', '', 'Seid Ihr... Seid Ihr ihr begegnet?', '', '', '', '', ''),
(42387, 1, 0, '', '', 'Ja. Sie existiert wirklich.', '', '', '', '', ''),
(42387, 2, 0, '', '', 'Sie beauftragte mich, Euch zu sagen, dass sie die Sache mit den Brauenwirbels, die wir auf ihr Geheiß hin ausgeführt haben, honoriert. Sie gab mir einen Haufen Gold, den ich unter Euch allen aufteilen soll.', '', '', '', '', ''),
(42387, 3, 0, '', '', 'Habt Ihr ihr Gesicht gesehen? Ist es wirklich...', '', '', '', '', ''),
(42387, 4, 0, '', '', 'Hoppla, was haben wir denn da? Anscheinend befindet sich ein Horcher unter uns, Jungs.', '', '', '', '', ''),
(42387, 5, 0, '', '', 'Mit einem niederträchtigen, nichtsnutzigen Horcher verfährt man folgendermaßen.', '', '', '', '', ''),
(42387, 6, 0, '', '', 'STERBT!', '', '', '', '', ''),
(42390, 0, 0, '', '', 'Nieder mit dem König!', '', '', '', '', ''),
(42391, 0, 0, '', '', 'HAHAHAH! Sehr gut, Herr Pinguin! SEHR GUT!', '', '', '', '', ''),
(42391, 1, 0, '', '', 'Ich habe nicht gesehen, wer sie getötet hat, $g Kumpel:meine Dame;, doch ich habe eine Ahnung. Haben nach Geld gerochen, ein bisschen so wie Ihr. So eine Schande auch. Die Brauenwirbels waren angesehen und beliebt hier. Nette Leute, immer herzlich und gastfreundlich.', '', '', '', '', ''),
(42391, 2, 0, '', '', 'Seid gesegnet, Freund.', '', '', '', '', ''),
(42492, 0, 0, '', '', 'Was will kleines Menschie? Warum Ihr rufen Glubtok?', '', '', '', '', ''),
(42492, 1, 0, '', '', 'Glubtok Euch zerquetschen!', '', '', '', '', ''),
(42492, 2, 0, '', '', 'Was Option zwei?', '', '', '', '', ''),
(42492, 3, 0, '', '', 'Also Glubtok haben zwei Möglichkeiten: Sterben oder reich und mächtig sein?', '', '', '', '', ''),
(42492, 4, 0, '', '', 'Glubtok nehmen Option zwei.', '', '', '', '', ''),
(42515, 0, 0, '', '', 'Traurig... Ist das das Leben, das Ihr Euch erhofft habt, Glubtok? Ein Schmalspurerpressungsunternehmen, geführt aus einer Höhle heraus?', '', '', '', '', ''),
(42515, 1, 0, '', '', 'Oh, tatsächlich? Wagt Ihr es, diese Grenze zu überschreiten und Euer Leben zu riskieren?', '', '', '', '', ''),
(42515, 2, 0, '', '', 'Ihr könnt versuchen, mich zu töten - und versagen - oder Ihr könnt Option zwei wählen.', '', '', '', '', ''),
(42515, 3, 0, '', '', 'Ihr schließt Euch mir an und ich werde Euch mit Reichtum und Macht überschütten.', '', '', '', '', ''),
(42515, 4, 0, '', '', 'Ich habe mir schon gedacht, dass Ihr es von meiner Warte aus sehen werdet.', '', '', '', '', ''),
(42515, 5, 0, '', '', 'Ich werde Euch Bescheid geben, wenn die Zeit gekommen ist.', '', '', '', '', ''),
(42559, 0, 0, '', '', 'Ihr seid genau hier gestanden! Was in aller Welt habt Ihr gesehen? Sprecht!', '', '', '', '', ''),
(42562, 0, 0, '', '', 'Eilt zurück zu Brauenwirbels Hütte!', '', '', '', '', ''),
(42635, 0, 0, '', '', 'Wenn ich ausbreche, werde ich Euer Herz herausreißen und verschlingen!', '', '', '', '', ''),
(42635, 1, 0, '', '', 'Der Tag bricht an, Starkmantel, und er bringt Euer Ende.', '', '', '', '', ''),
(42655, 0, 0, '', '', 'Die Gnolle haben versagt, Herrin.', '', '', '', '', ''),
(42655, 1, 0, '', '', 'Aber Herrin, der Admiral ist immer noch...', '', '', '', '', ''),
(42655, 2, 0, '', '', 'Ja, Herrin.', '', '', '', '', ''),
(42655, 3, 0, '', '', 'Mondbruch, Herrin?', '', '', '', '', ''),
(42662, 0, 0, '', '', 'Sie sorgten für die nötige Ablenkung. Wir machen weiter wie geplant.', '', '', '', '', ''),
(42662, 1, 0, '', '', 'Wir werden den Admiral im Morgengrauen befreien.', '', '', '', '', ''),
(42662, 2, 0, '', '', 'Der Tag des Jüngsten Gerichts steht kurz bevor, Helix.', '', '', '', '', ''),
(42662, 3, 0, '', '', 'Ruft die Leute zusammen. Ich möchte ein allerletztes Mal mit ihnen sprechen, bevor der Morgen dämmert.', '', '', '', '', ''),
(42662, 4, 0, '', '', 'Ja. Heute Nacht.', '', '', '', '', ''),
(42677, 0, 0, '', '', 'Wer nicht für uns ist, ist gegen uns! STERBT!', '', '', '', '', ''),
(42677, 1, 0, '', '', 'Das Spiel ist aus, Kumpel!', '', '', '', '', ''),
(42677, 2, 0, '', '', 'Was glaubt Ihr, wohin Ihr geht?', '', '', '', '', ''),
(42680, 0, 0, '', '', 'Die Zusammenkunft beginnt in Kürze!', '', '', '', '', ''),
(42680, 1, 0, '', '', 'Brüder und Schwestern, versammelt Euch! Kommt alle her und hört zu!', '', '', '', '', ''),
(42680, 2, 0, '', '', 'Brüder. Schwestern. Man hat uns im STICH GELASSEN. Wir sind die verwaisten Kinder Sturmwinds.', '', '', '', '', ''),
(42680, 3, 0, '', '', 'Unser "König" sitzt auf seinem goldenen Thron und zuckt angesichts unserer Not die Achseln!', '', '', '', '', ''),
(42680, 4, 0, '', '', 'Währenddessen sterben unsere Kinder auf den Straßen vor Hunger!', '', '', '', '', ''),
(42680, 5, 0, '', '', 'SEIN Krieg, nicht der unsere, kostet uns die Lebensgrundlage. WIR bezahlen für die Siege der Allianz mit unserem Blut und dem unserer Lieben.', '', '', '', '', ''),
(42680, 6, 0, '', '', 'Es ist an der Zeit, Brüder und Schwestern, diese Ungerechtigkeit zu beenden!', '', '', '', '', ''),
(42680, 7, 0, '', '', 'Die Regierung von Sturmwind und der ALLIANZ muss für das, was sie uns angetan hat, zur Rechenschaft gezogen werden!', '', '', '', '', ''),
(42680, 8, 0, '', '', 'Heute ist der Tag unserer Wiedergeburt! Heute beziehen wir eine klare Position als Männer und Frauen, nicht als namen- und gesichtslose Meute!', '', '', '', '', ''),
(42693, 0, 0, '', '', '|cFF768EBDVor fünf Jahren im Herzen der Todesminen...|r |TInterface\\QuestFrame\\UI-QUESTLOG-BOOKICON.BLP:32|t', '', '', '', '', ''),
(42697, 0, 0, '', '', 'Niemand darf die Bruderschaft herausfordern!', '', '', '', '', ''),
(42697, 1, 0, '', '', 'Schoßhündchen, alle miteinander!', '', '', '', '', ''),
(42697, 2, 0, '', '', '%s ruft weitere Verbündete aus den Schatten.', '', '', '', '', ''),
(42697, 3, 0, '', '', 'Narren! Unsere Sache ist gerecht!', '', '', '', '', ''),
(42697, 4, 0, '', '', 'Die Bruderschaft wird siegen!', '', '', '', '', ''),
(42698, 0, 0, '', '', '%s springt aus den Schatten hervor!', '', '', '', '', ''),
(42699, 0, 0, '', '', 'Es gibt keine Versteckmöglichkeit mehr, van Cleef! Die Defias sind AM ENDE!', '', '', '', '', ''),
(42699, 1, 0, '', '', 'Wir sind gekommen, um der Sache ein Ende zu setzen! Stellt Euch, Feigling!', '', '', '', '', ''),
(42699, 2, 0, '', '', 'Sieg der Allianz! Ruhm für Sturmwind!', '', '', '', '', ''),
(42699, 3, 0, '', '', 'Kehren wir zur Späherkuppe zurück, Verbündete, und informieren Gryan über den Tod van Cleefs!', '', '', '', '', ''),
(42744, 0, 0, '', '', 'Verratet mir nur eins, Vanessa.', '', '', '', '', ''),
(42744, 1, 0, '', '', 'Weshalb habt Ihr die Brauenwirbels umbringen lassen?', '', '', '', '', ''),
(42748, 0, 0, '', '', 'Danke, meine Liebe!', '', '', '', '', ''),
(42749, 0, 0, '', '', 'Ihr Bastarde werdet für Eure Taten brennen.', '', '', '', '', ''),
(42749, 1, 0, '', '', 'Hope. Wie die Hoffnung. Hätte ich Hoffnung hegen sollen, als ich sah, wie mein Vater von Euren Spießgesellen geköpft wurde?', '', '', '', '', ''),
(42749, 2, 0, '', '', 'Hoffnung ist ein grausamer Streich, den uns eine gefühllose und brutale Welt spielt. Es gibt keinerlei Hoffnung, es gibt nur Vanessa. Vanessa van Cleef.', '', '', '', '', ''),
(42749, 3, 0, '', '', 'ERHEBT EUCH, BRÜDER UND SCHWESTERN! DER TAG BRICHT AN!', '', '', '', '', ''),
(42749, 4, 0, '', '', 'Bindet sie fest!', '', '', '', '', ''),
(42749, 5, 0, '', '', 'Admiral, Euer Hut.', '', '', '', '', ''),
(42749, 6, 0, '', '', 'Was Euch betrifft, $n, so will ich Euer Leben schonen. Ihr habt, wenn auch unwissentlich, viel zu unserer Unterstützung beigetragen, doch bei unserer nächsten Begegnung stehen wir uns als Feinde gegenüber.', '', '', '', '', ''),
(42749, 7, 0, '', '', 'Ich hatte keine Wahl, Leutnant. Sie haben mich wiedererkannt. Die einzigen Menschen der Welt, die überhaupt von meiner Existenz wussten, erkannten mein Gesicht von meiner Kinderzeit.', '', '', '', '', ''),
(42749, 8, 0, '', '', 'Ich empfand keine Freude über ihren Tod.', '', '', '', '', ''),
(42749, 9, 0, '', '', 'Lasst nichts als Asche zurück! Brennt die Späherkuppe nieder!', '', '', '', '', ''),
(42750, 0, 0, '', '', 'Das gefällt mir nicht, $n. Bleibt wachsam!', '', '', '', '', ''),
(42750, 1, 0, '', '', 'Hope! Wa...', '', '', '', '', ''),
(42750, 2, 0, '', '', '$n, macht Euch auf nach Sturmwind. Erzählt König Wrynn alles. ALLES! GEHT JETZT!', '', '', '', '', ''),
(43515, 0, 0, '', '', 'Folgt dem Zug der Heimatlosen zum Eingang des Todesminendungeons.', '', '', '', '', ''),
(65648, 0, 0, '', '', 'Ich hatte einst einen Hof, aber das ist schon lange her. Ich ziehe eine gute Schlacht vor, Ihr nicht auch?', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=4076 /*4076*/ AND `locale`='deDE') OR (`entry`=712 /*712*/ AND `locale`='deDE') OR (`entry`=426 /*426*/ AND `locale`='deDE') OR (`entry`=49996 /*49996*/ AND `locale`='deDE') OR (`entry`=423 /*423*/ AND `locale`='deDE') OR (`entry`=44140 /*44140*/ AND `locale`='deDE') OR (`entry`=43081 /*43081*/ AND `locale`='deDE') OR (`entry`=464 /*464*/ AND `locale`='deDE') OR (`entry`=547 /*547*/ AND `locale`='deDE') OR (`entry`=379 /*379*/ AND `locale`='deDE') OR (`entry`=43178 /*43178*/ AND `locale`='deDE') OR (`entry`=43080 /*43080*/ AND `locale`='deDE') OR (`entry`=43083 /*43083*/ AND `locale`='deDE') OR (`entry`=43173 /*43173*/ AND `locale`='deDE') OR (`entry`=61167 /*Cola de algodón montesa*/ AND `locale`='deDE') OR (`entry`=442 /*442*/ AND `locale`='deDE') OR (`entry`=428 /*428*/ AND `locale`='deDE') OR (`entry`=844 /*844*/ AND `locale`='deDE') OR (`entry`=44020 /*44020*/ AND `locale`='deDE') OR (`entry`=590 /*590*/ AND `locale`='deDE') OR (`entry`=6180 /*6180*/ AND `locale`='deDE') OR (`entry`=589 /*589*/ AND `locale`='deDE') OR (`entry`=594 /*594*/ AND `locale`='deDE') OR (`entry`=449 /*449*/ AND `locale`='deDE') OR (`entry`=71529 /*71529*/ AND `locale`='deDE') OR (`entry`=79779 /*79779*/ AND `locale`='deDE') OR (`entry`=101527 /*Joyatrón 6000*/ AND `locale`='deDE') OR (`entry`=29238 /*Aparición de la Plaga*/ AND `locale`='deDE') OR (`entry`=106550 /*106550*/ AND `locale`='deDE') OR (`entry`=70005 /*70005*/ AND `locale`='deDE') OR (`entry`=63849 /*Rana toro con tripa amarilla*/ AND `locale`='deDE') OR (`entry`=33776 /*33776*/ AND `locale`='deDE') OR (`entry`=86081 /*Engendro abisal AND `locale`='deDE') OR (`entry`=engendro de engendro abisal*/ AND `locale`='deDE') OR (`entry`=25498 /*25498*/ AND `locale`='deDE') OR (`entry`=62419 /*Aysa Canción Etérea*/ AND `locale`='deDE') OR (`entry`=60931 /*60931*/ AND `locale`='deDE') OR (`entry`=65066 /*65066*/ AND `locale`='deDE') OR (`entry`=65051 /*65051*/ AND `locale`='deDE') OR (`entry`=65061 /*65061*/ AND `locale`='deDE') OR (`entry`=69823 /*69823*/ AND `locale`='deDE') OR (`entry`=69822 /*69822*/ AND `locale`='deDE') OR (`entry`=69334 /*69334*/ AND `locale`='deDE') OR (`entry`=68868 /*68868*/ AND `locale`='deDE') OR (`entry`=65048 /*65048*/ AND `locale`='deDE') OR (`entry`=65060 /*65060*/ AND `locale`='deDE') OR (`entry`=70296 /*70296*/ AND `locale`='deDE') OR (`entry`=65076 /*65076*/ AND `locale`='deDE') OR (`entry`=65074 /*65074*/ AND `locale`='deDE') OR (`entry`=65072 /*65072*/ AND `locale`='deDE') OR (`entry`=65068 /*65068*/ AND `locale`='deDE') OR (`entry`=65058 /*65058*/ AND `locale`='deDE') OR (`entry`=65065 /*65065*/ AND `locale`='deDE') OR (`entry`=65078 /*65078*/ AND `locale`='deDE') OR (`entry`=65071 /*65071*/ AND `locale`='deDE') OR (`entry`=62106 /*62106*/ AND `locale`='deDE') OR (`entry`=43451 /*43451*/ AND `locale`='deDE') OR (`entry`=44396 /*44396*/ AND `locale`='deDE') OR (`entry`=50088 /*50088*/ AND `locale`='deDE') OR (`entry`=63607 /*63607*/ AND `locale`='deDE') OR (`entry`=63596 /*63596*/ AND `locale`='deDE') OR (`entry`=65063 /*65063*/ AND `locale`='deDE') OR (`entry`=49908 /*49908*/ AND `locale`='deDE') OR (`entry`=49907 /*49907*/ AND `locale`='deDE') OR (`entry`=43840 /*43840*/ AND `locale`='deDE') OR (`entry`=43825 /*43825*/ AND `locale`='deDE') OR (`entry`=43824 /*43824*/ AND `locale`='deDE') OR (`entry`=43823 /*43823*/ AND `locale`='deDE') OR (`entry`=43725 /*43725*/ AND `locale`='deDE') OR (`entry`=43724 /*43724*/ AND `locale`='deDE') OR (`entry`=43723 /*43723*/ AND `locale`='deDE') OR (`entry`=44393 /*44393*/ AND `locale`='deDE') OR (`entry`=1649 /*1649*/ AND `locale`='deDE') OR (`entry`=44394 /*44394*/ AND `locale`='deDE') OR (`entry`=43034 /*43034*/ AND `locale`='deDE') OR (`entry`=20407 /*20407*/ AND `locale`='deDE') OR (`entry`=5516 /*5516*/ AND `locale`='deDE') OR (`entry`=5515 /*5515*/ AND `locale`='deDE') OR (`entry`=44392 /*44392*/ AND `locale`='deDE') OR (`entry`=5517 /*5517*/ AND `locale`='deDE') OR (`entry`=2879 /*2879*/ AND `locale`='deDE') OR (`entry`=11069 /*11069*/ AND `locale`='deDE') OR (`entry`=44243 /*44243*/ AND `locale`='deDE') OR (`entry`=44241 /*44241*/ AND `locale`='deDE') OR (`entry`=44239 /*44239*/ AND `locale`='deDE') OR (`entry`=44238 /*44238*/ AND `locale`='deDE') OR (`entry`=93307 /*93307*/ AND `locale`='deDE') OR (`entry`=93296 /*93296*/ AND `locale`='deDE') OR (`entry`=88537 /*88537*/ AND `locale`='deDE') OR (`entry`=88535 /*88535*/ AND `locale`='deDE') OR (`entry`=35365 /*35365*/ AND `locale`='deDE') OR (`entry`=24729 /*24729*/ AND `locale`='deDE') OR (`entry`=34998 /*34998*/ AND `locale`='deDE') OR (`entry`=34997 /*34997*/ AND `locale`='deDE') OR (`entry`=1751 /*1751*/ AND `locale`='deDE') OR (`entry`=15187 /*15187*/ AND `locale`='deDE') OR (`entry`=1750 /*1750*/ AND `locale`='deDE') OR (`entry`=17103 /*17103*/ AND `locale`='deDE') OR (`entry`=2285 /*2285*/ AND `locale`='deDE') OR (`entry`=1439 /*1439*/ AND `locale`='deDE') OR (`entry`=107574 /*Rey Anduin Wrynn*/ AND `locale`='deDE') OR (`entry`=4960 /*4960*/ AND `locale`='deDE') OR (`entry`=1752 /*1752*/ AND `locale`='deDE') OR (`entry`=58167 /*58167*/ AND `locale`='deDE') OR (`entry`=51938 /*51938*/ AND `locale`='deDE') OR (`entry`=47688 /*Oficial Connelly*/ AND `locale`='deDE') OR (`entry`=17252 /*Guardia vil*/ AND `locale`='deDE') OR (`entry`=112144 /*Corgnelius*/ AND `locale`='deDE') OR (`entry`=84915 /*Corgi de magma*/ AND `locale`='deDE') OR (`entry`=85009 /*Murkidan*/ AND `locale`='deDE') OR (`entry`=99403 /*Gatito de sable resucitado*/ AND `locale`='deDE') OR (`entry`=113211 /*113211*/ AND `locale`='deDE') OR (`entry`=78158 /*Guardia apocalíptico*/ AND `locale`='deDE') OR (`entry`=36911 /*Monje pandaren*/ AND `locale`='deDE') OR (`entry`=55367 /*Zepelín de la Luna Negra*/ AND `locale`='deDE') OR (`entry`=111202 /*Entusiasta de la Alianza*/ AND `locale`='deDE') OR (`entry`=1413 /*1413*/ AND `locale`='deDE') OR (`entry`=106549 /*106549*/ AND `locale`='deDE') OR (`entry`=11673 /*11673*/ AND `locale`='deDE') OR (`entry`=17447 /*17447*/ AND `locale`='deDE') OR (`entry`=42778 /*42778*/ AND `locale`='deDE') OR (`entry`=42372 /*42372*/ AND `locale`='deDE') OR (`entry`=42769 /*42769*/ AND `locale`='deDE') OR (`entry`=42753 /*42753*/ AND `locale`='deDE') OR (`entry`=42755 /*42755*/ AND `locale`='deDE') OR (`entry`=42749 /*42749*/ AND `locale`='deDE') OR (`entry`=42793 /*42793*/ AND `locale`='deDE') OR (`entry`=42752 /*42752*/ AND `locale`='deDE') OR (`entry`=42751 /*42751*/ AND `locale`='deDE') OR (`entry`=42748 /*42748*/ AND `locale`='deDE') OR (`entry`=42771 /*42771*/ AND `locale`='deDE') OR (`entry`=42745 /*42745*/ AND `locale`='deDE') OR (`entry`=42744 /*42744*/ AND `locale`='deDE') OR (`entry`=42750 /*42750*/ AND `locale`='deDE') OR (`entry`=7543 /*Vástago oscuro*/ AND `locale`='deDE') OR (`entry`=513 /*513*/ AND `locale`='deDE') OR (`entry`=42669 /*42669*/ AND `locale`='deDE') OR (`entry`=171 /*171*/ AND `locale`='deDE') OR (`entry`=458 /*458*/ AND `locale`='deDE') OR (`entry`=6250 /*6250*/ AND `locale`='deDE') OR (`entry`=520 /*520*/ AND `locale`='deDE') OR (`entry`=34281 /*34281*/ AND `locale`='deDE') OR (`entry`=391 /*391*/ AND `locale`='deDE') OR (`entry`=4305 /*4305*/ AND `locale`='deDE') OR (`entry`=517 /*517*/ AND `locale`='deDE') OR (`entry`=127 /*127*/ AND `locale`='deDE') OR (`entry`=1216 /*1216*/ AND `locale`='deDE') OR (`entry`=78217 /*Infernal*/ AND `locale`='deDE') OR (`entry`=42371 /*42371*/ AND `locale`='deDE') OR (`entry`=42698 /*42698*/ AND `locale`='deDE') OR (`entry`=42697 /*42697*/ AND `locale`='deDE') OR (`entry`=42699 /*42699*/ AND `locale`='deDE') OR (`entry`=42703 /*42703*/ AND `locale`='deDE') OR (`entry`=42700 /*42700*/ AND `locale`='deDE') OR (`entry`=42701 /*42701*/ AND `locale`='deDE') OR (`entry`=42702 /*42702*/ AND `locale`='deDE') OR (`entry`=645 /*645*/ AND `locale`='deDE') OR (`entry`=646 /*646*/ AND `locale`='deDE') OR (`entry`=636 /*636*/ AND `locale`='deDE') OR (`entry`=647 /*647*/ AND `locale`='deDE') OR (`entry`=1732 /*1732*/ AND `locale`='deDE') OR (`entry`=3947 /*3947*/ AND `locale`='deDE') OR (`entry`=657 /*657*/ AND `locale`='deDE') OR (`entry`=42693 /*42693*/ AND `locale`='deDE') OR (`entry`=47296 /*47296*/ AND `locale`='deDE') OR (`entry`=47297 /*47297*/ AND `locale`='deDE') OR (`entry`=43778 /*43778*/ AND `locale`='deDE') OR (`entry`=48445 /*48445*/ AND `locale`='deDE') OR (`entry`=48280 /*48280*/ AND `locale`='deDE') OR (`entry`=47404 /*47404*/ AND `locale`='deDE') OR (`entry`=47403 /*47403*/ AND `locale`='deDE') OR (`entry`=48505 /*48505*/ AND `locale`='deDE') OR (`entry`=48439 /*48439*/ AND `locale`='deDE') OR (`entry`=48351 /*48351*/ AND `locale`='deDE') OR (`entry`=48502 /*48502*/ AND `locale`='deDE') OR (`entry`=48417 /*48417*/ AND `locale`='deDE') OR (`entry`=48441 /*48441*/ AND `locale`='deDE') OR (`entry`=48442 /*48442*/ AND `locale`='deDE') OR (`entry`=48342 /*48342*/ AND `locale`='deDE') OR (`entry`=48341 /*48341*/ AND `locale`='deDE') OR (`entry`=48340 /*48340*/ AND `locale`='deDE') OR (`entry`=48338 /*48338*/ AND `locale`='deDE') OR (`entry`=48343 /*48343*/ AND `locale`='deDE') OR (`entry`=48279 /*48279*/ AND `locale`='deDE') OR (`entry`=48440 /*48440*/ AND `locale`='deDE') OR (`entry`=48278 /*48278*/ AND `locale`='deDE') OR (`entry`=47677 /*47677*/ AND `locale`='deDE') OR (`entry`=47162 /*47162*/ AND `locale`='deDE') OR (`entry`=47282 /*47282*/ AND `locale`='deDE') OR (`entry`=48262 /*48262*/ AND `locale`='deDE') OR (`entry`=47284 /*47284*/ AND `locale`='deDE') OR (`entry`=47242 /*47242*/ AND `locale`='deDE') OR (`entry`=48266 /*48266*/ AND `locale`='deDE') OR (`entry`=48284 /*48284*/ AND `locale`='deDE') OR (`entry`=48446 /*48446*/ AND `locale`='deDE') OR (`entry`=48230 /*48230*/ AND `locale`='deDE') OR (`entry`=48229 /*48229*/ AND `locale`='deDE') OR (`entry`=46612 /*46612*/ AND `locale`='deDE') OR (`entry`=46614 /*46614*/ AND `locale`='deDE') OR (`entry`=46613 /*46613*/ AND `locale`='deDE') OR (`entry`=50595 /*50595*/ AND `locale`='deDE') OR (`entry`=53488 /*53488*/ AND `locale`='deDE') OR (`entry`=392 /*392*/ AND `locale`='deDE') OR (`entry`=626 /*626*/ AND `locale`='deDE') OR (`entry`=624 /*624*/ AND `locale`='deDE') OR (`entry`=625 /*625*/ AND `locale`='deDE') OR (`entry`=623 /*623*/ AND `locale`='deDE') OR (`entry`=596 /*596*/ AND `locale`='deDE') OR (`entry`=42683 /*42683*/ AND `locale`='deDE') OR (`entry`=42680 /*42680*/ AND `locale`='deDE') OR (`entry`=43515 /*43515*/ AND `locale`='deDE') OR (`entry`=42679 /*42679*/ AND `locale`='deDE') OR (`entry`=42677 /*42677*/ AND `locale`='deDE') OR (`entry`=43948 /*43948*/ AND `locale`='deDE') OR (`entry`=42651 /*42651*/ AND `locale`='deDE') OR (`entry`=42425 /*42425*/ AND `locale`='deDE') OR (`entry`=42426 /*42426*/ AND `locale`='deDE') OR (`entry`=452 /*452*/ AND `locale`='deDE') OR (`entry`=42653 /*42653*/ AND `locale`='deDE') OR (`entry`=453 /*453*/ AND `locale`='deDE') OR (`entry`=42662 /*42662*/ AND `locale`='deDE') OR (`entry`=42654 /*42654*/ AND `locale`='deDE') OR (`entry`=42655 /*42655*/ AND `locale`='deDE') OR (`entry`=42656 /*42656*/ AND `locale`='deDE') OR (`entry`=98 /*98*/ AND `locale`='deDE') OR (`entry`=115 /*115*/ AND `locale`='deDE') OR (`entry`=7024 /*7024*/ AND `locale`='deDE') OR (`entry`=2630 /*2630*/ AND `locale`='deDE') OR (`entry`=51915 /*51915*/ AND `locale`='deDE') OR (`entry`=42652 /*42652*/ AND `locale`='deDE') OR (`entry`=42357 /*42357*/ AND `locale`='deDE') OR (`entry`=154 /*154*/ AND `locale`='deDE') OR (`entry`=42617 /*42617*/ AND `locale`='deDE') OR (`entry`=49760 /*49760*/ AND `locale`='deDE') OR (`entry`=49745 /*49745*/ AND `locale`='deDE') OR (`entry`=43011 /*43011*/ AND `locale`='deDE') OR (`entry`=8934 /*8934*/ AND `locale`='deDE') OR (`entry`=49769 /*49769*/ AND `locale`='deDE') OR (`entry`=49749 /*49749*/ AND `locale`='deDE') OR (`entry`=49741 /*49741*/ AND `locale`='deDE') OR (`entry`=49736 /*49736*/ AND `locale`='deDE') OR (`entry`=8931 /*8931*/ AND `locale`='deDE') OR (`entry`=10045 /*10045*/ AND `locale`='deDE') OR (`entry`=1670 /*1670*/ AND `locale`='deDE') OR (`entry`=1668 /*1668*/ AND `locale`='deDE') OR (`entry`=843 /*843*/ AND `locale`='deDE') OR (`entry`=876 /*876*/ AND `locale`='deDE') OR (`entry`=870 /*870*/ AND `locale`='deDE') OR (`entry`=6670 /*6670*/ AND `locale`='deDE') OR (`entry`=52190 /*52190*/ AND `locale`='deDE') OR (`entry`=842 /*842*/ AND `locale`='deDE') OR (`entry`=54372 /*54372*/ AND `locale`='deDE') OR (`entry`=874 /*874*/ AND `locale`='deDE') OR (`entry`=54373 /*54373*/ AND `locale`='deDE') OR (`entry`=54371 /*54371*/ AND `locale`='deDE') OR (`entry`=487 /*487*/ AND `locale`='deDE') OR (`entry`=490 /*490*/ AND `locale`='deDE') OR (`entry`=523 /*523*/ AND `locale`='deDE') OR (`entry`=878 /*878*/ AND `locale`='deDE') OR (`entry`=821 /*821*/ AND `locale`='deDE') OR (`entry`=820 /*820*/ AND `locale`='deDE') OR (`entry`=489 /*489*/ AND `locale`='deDE') OR (`entry`=234 /*234*/ AND `locale`='deDE') OR (`entry`=42575 /*42575*/ AND `locale`='deDE') OR (`entry`=491 /*491*/ AND `locale`='deDE') OR (`entry`=488 /*488*/ AND `locale`='deDE') OR (`entry`=42635 /*42635*/ AND `locale`='deDE') OR (`entry`=42407 /*42407*/ AND `locale`='deDE') OR (`entry`=42342 /*42342*/ AND `locale`='deDE') OR (`entry`=124 /*124*/ AND `locale`='deDE') OR (`entry`=42381 /*42381*/ AND `locale`='deDE') OR (`entry`=869 /*869*/ AND `locale`='deDE') OR (`entry`=233 /*233*/ AND `locale`='deDE') OR (`entry`=235 /*235*/ AND `locale`='deDE') OR (`entry`=42560 /*42560*/ AND `locale`='deDE') OR (`entry`=42559 /*42559*/ AND `locale`='deDE') OR (`entry`=42558 /*42558*/ AND `locale`='deDE') OR (`entry`=1424 /*1424*/ AND `locale`='deDE') OR (`entry`=501 /*501*/ AND `locale`='deDE') OR (`entry`=123 /*123*/ AND `locale`='deDE') OR (`entry`=846 /*846*/ AND `locale`='deDE') OR (`entry`=572 /*572*/ AND `locale`='deDE') OR (`entry`=42515 /*42515*/ AND `locale`='deDE') OR (`entry`=42492 /*42492*/ AND `locale`='deDE') OR (`entry`=42500 /*42500*/ AND `locale`='deDE') OR (`entry`=1065 /*1065*/ AND `locale`='deDE') OR (`entry`=1236 /*1236*/ AND `locale`='deDE') OR (`entry`=42406 /*42406*/ AND `locale`='deDE') OR (`entry`=42562 /*42562*/ AND `locale`='deDE') OR (`entry`=2620 /*2620*/ AND `locale`='deDE') OR (`entry`=42497 /*42497*/ AND `locale`='deDE') OR (`entry`=42405 /*42405*/ AND `locale`='deDE') OR (`entry`=42390 /*42390*/ AND `locale`='deDE') OR (`entry`=42387 /*42387*/ AND `locale`='deDE') OR (`entry`=42498 /*42498*/ AND `locale`='deDE') OR (`entry`=462 /*462*/ AND `locale`='deDE') OR (`entry`=61143 /*Ratón*/ AND `locale`='deDE') OR (`entry`=114 /*114*/ AND `locale`='deDE') OR (`entry`=1109 /*1109*/ AND `locale`='deDE') OR (`entry`=157 /*157*/ AND `locale`='deDE') OR (`entry`=831 /*831*/ AND `locale`='deDE') OR (`entry`=519 /*519*/ AND `locale`='deDE') OR (`entry`=126 /*126*/ AND `locale`='deDE') OR (`entry`=60761 /*60761*/ AND `locale`='deDE') OR (`entry`=456 /*456*/ AND `locale`='deDE') OR (`entry`=61158 /*Cangrejo de costa*/ AND `locale`='deDE') OR (`entry`=515 /*515*/ AND `locale`='deDE') OR (`entry`=830 /*830*/ AND `locale`='deDE') OR (`entry`=42413 /*42413*/ AND `locale`='deDE') OR (`entry`=117 /*117*/ AND `locale`='deDE') OR (`entry`=500 /*500*/ AND `locale`='deDE') OR (`entry`=42385 /*42385*/ AND `locale`='deDE') OR (`entry`=61141 /*Perro de la pradera*/ AND `locale`='deDE') OR (`entry`=2914 /*2914*/ AND `locale`='deDE') OR (`entry`=199 /*199*/ AND `locale`='deDE') OR (`entry`=42400 /*42400*/ AND `locale`='deDE') OR (`entry`=42402 /*42402*/ AND `locale`='deDE') OR (`entry`=42401 /*42401*/ AND `locale`='deDE') OR (`entry`=42399 /*42399*/ AND `locale`='deDE') OR (`entry`=42403 /*42403*/ AND `locale`='deDE') OR (`entry`=61142 /*Culebra*/ AND `locale`='deDE') OR (`entry`=833 /*833*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(4076, 'deDE', 'Schabe', NULL, NULL, NULL, 23420), -- 4076
(712, 'deDE', 'Hauer der Rotkammgnolle', NULL, NULL, NULL, 23420), -- 712
(426, 'deDE', 'Schläger der Rotkammgnolle', NULL, NULL, NULL, 23420), -- 426
(49996, 'deDE', 'Bergbaumwollschwänzchen', NULL, NULL, NULL, 23420), -- 49996
(423, 'deDE', 'Bastard der Rotkammgnolle', NULL, NULL, NULL, 23420), -- 423
(44140, 'deDE', 'PetWait Trigger', NULL, NULL, NULL, 23420), -- 44140
(43081, 'deDE', 'Wache Battmann', NULL, NULL, NULL, 23420), -- 43081
(464, 'deDE', 'Wachhauptmann Parker', NULL, NULL, NULL, 23420), -- 464
(547, 'deDE', 'Großer Geiferzahn', NULL, NULL, NULL, 23420), -- 547
(379, 'deDE', 'Darcy Parker', NULL, NULL, NULL, 23420), -- 379
(43178, 'deDE', 'Libby Parker', NULL, NULL, NULL, 23420), -- 43178
(43080, 'deDE', 'Wache Flippit', NULL, NULL, NULL, 23420), -- 43080
(43083, 'deDE', 'Rotkammfuchs', NULL, NULL, NULL, 23420), -- 43083
(43173, 'deDE', 'Garnisonswachmann des Rotkammgebirges', NULL, NULL, NULL, 23420), -- 43173
(61167, 'deDE', 'Bergbaumwollschwänzchen', NULL, NULL, 'wildpetcapturable', 23420), -- Cola de algodón montesa
(442, 'deDE', 'Tarantel', NULL, NULL, NULL, 23420), -- 442
(428, 'deDE', 'Terrorkondor', NULL, NULL, NULL, 23420), -- 428
(844, 'deDE', 'Antonio Perelli', NULL, 'Fliegender Händler', NULL, 23420), -- 844
(44020, 'deDE', 'Schleiereule', NULL, NULL, NULL, 23420), -- 44020
(590, 'deDE', 'Ausplünderer der Defias', 'Ausplünderin der Defias', NULL, NULL, 23420), -- 590
(6180, 'deDE', 'Räuber der Defias', 'Räuberin der Defias', NULL, NULL, 23420), -- 6180
(589, 'deDE', 'Plünderer der Defias', 'Plünderin der Defias', NULL, NULL, 23420), -- 589
(594, 'deDE', 'Handlanger der Defias', NULL, NULL, NULL, 23420), -- 594
(449, 'deDE', 'Knöchelhauer der Defias', 'Knöchelhauerin der Defias', NULL, NULL, 23420), -- 449
(71529, 'deDE', 'Thok der Blutrünstige', NULL, NULL, NULL, 23420), -- 71529
(79779, 'deDE', 'Unterholzpirscher', NULL, NULL, NULL, 23420), -- 79779
(101527, 'deDE', 'Blingtron 6000', NULL, NULL, NULL, 23420), -- Joyatrón 6000
(29238, 'deDE', 'Spukgestalt der Geißel', NULL, NULL, NULL, 23420), -- Aparición de la Plaga
(106550, 'deDE', 'Hati', NULL, NULL, NULL, 23420), -- 106550
(70005, 'deDE', 'Junger Urteufelssaurier', NULL, NULL, NULL, 23420), -- 70005
(63849, 'deDE', 'Gelbbäuchiger Ochsenfrosch', NULL, NULL, 'wildpetcapturable', 23420), -- Rana toro con tripa amarilla
(33776, 'deDE', 'Gondria', NULL, NULL, NULL, 23420), -- 33776
(86081, 'deDE', 'Netherbrut, Brut der Netherbrut', NULL, NULL, NULL, 23420), -- Engendro abisal, engendro de engendro abisal
(25498, 'deDE', 'Aspatha die Brutmutter', NULL, NULL, NULL, 23420), -- 25498
(62419, 'deDE', 'Aysa Wolkensänger', NULL, 'Mönchslehrerin', NULL, 23420), -- Aysa Canción Etérea
(60931, 'deDE', 'Übungsziel', NULL, NULL, NULL, 23420), -- 60931
(65066, 'deDE', 'Jojo Eisenstirn', NULL, NULL, NULL, 23420), -- 65066
(65051, 'deDE', 'Mönch der Tushui', NULL, NULL, NULL, 23420), -- 65051
(65061, 'deDE', 'Braune Drachenschildkröte', NULL, NULL, NULL, 23420), -- 65061
(69823, 'deDE', 'Waffelhans', NULL, NULL, NULL, 23420), -- 69823
(69822, 'deDE', 'Kelly Aguilar', NULL, NULL, NULL, 23420), -- 69822
(69334, 'deDE', 'Anhänger Hanjun', NULL, 'Rüstmeister der Tushui', NULL, 23420), -- 69334
(68868, 'deDE', 'Meng Meng', NULL, NULL, NULL, 23420), -- 68868
(65048, 'deDE', 'Schüler der Tushui', 'Schülerin der Tushui', NULL, NULL, 23420), -- 65048
(65060, 'deDE', 'Blaue Drachenschildkröte', NULL, NULL, NULL, 23420), -- 65060
(70296, 'deDE', 'Mei Lin', NULL, 'Reitlehrerin', NULL, 23420), -- 70296
(65076, 'deDE', 'Große braune Drachenschildkröte', NULL, NULL, NULL, 23420), -- 65076
(65074, 'deDE', 'Große blaue Drachenschildkröte', NULL, NULL, NULL, 23420), -- 65074
(65072, 'deDE', 'Große schwarze Drachenschildkröte', NULL, NULL, NULL, 23420), -- 65072
(65068, 'deDE', 'Der alte Weißnase', NULL, 'Drachenschildkrötenzüchter', NULL, 23420), -- 65068
(65058, 'deDE', 'Schwarze Drachenschildkröte', NULL, NULL, NULL, 23420), -- 65058
(65065, 'deDE', 'Rote Drachenschildkröte', NULL, NULL, NULL, 23420), -- 65065
(65078, 'deDE', 'Große purpurne Drachenschildkröte', NULL, NULL, NULL, 23420), -- 65078
(65071, 'deDE', 'Große grüne Drachenschildkröte', NULL, NULL, NULL, 23420), -- 65071
(62106, 'deDE', 'Große rote Drachenschildkröte', NULL, NULL, NULL, 23420), -- 62106
(43451, 'deDE', 'Olivia Jayne', NULL, NULL, NULL, 23420), -- 43451
(44396, 'deDE', 'Celestine Erntedank', NULL, 'Druidenlehrerin', NULL, 23420), -- 44396
(50088, 'deDE', 'Wasser der Weitsicht', NULL, NULL, NULL, 23420), -- 50088
(63607, 'deDE', 'Eichhörnchen', NULL, NULL, NULL, 23420), -- 63607
(63596, 'deDE', 'Marlene Trichdie', NULL, 'Kampfhaustiertrainerin', NULL, 23420), -- 63596
(65063, 'deDE', 'Purpurne Drachenschildkröte', NULL, NULL, NULL, 23420), -- 65063
(49908, 'deDE', 'Otto von Pummel', NULL, 'Gefährte von Dylan', NULL, 23420), -- 49908
(49907, 'deDE', 'Dylan Aguilar', NULL, NULL, NULL, 23420), -- 49907
(43840, 'deDE', 'Fineas G. Banktauglich', NULL, 'Bankier', NULL, 23420), -- 43840
(43825, 'deDE', 'Warren Wainwright', NULL, 'Bankier', NULL, 23420), -- 43825
(43824, 'deDE', 'Ann Wainwright', NULL, 'Bankierin', NULL, 23420), -- 43824
(43823, 'deDE', 'Leslie Wainwright', NULL, 'Bankierin', NULL, 23420), -- 43823
(43725, 'deDE', 'Curtis Crester', NULL, 'Bankier', NULL, 23420), -- 43725
(43724, 'deDE', 'Lee Crester', NULL, 'Bankier', NULL, 23420), -- 43724
(43723, 'deDE', 'Jamie Crester', NULL, 'Bankierin', NULL, 23420), -- 43723
(44393, 'deDE', 'Bolner Hammerschnabel', NULL, 'Schamanenlehrer', NULL, 23420), -- 44393
(1649, 'deDE', 'Bolgor', NULL, NULL, NULL, 23420), -- 1649
(44394, 'deDE', 'Dalga Hammerschnabel', NULL, 'Schamanenlehrerin', NULL, 23420), -- 44394
(43034, 'deDE', 'Colin Feld', NULL, 'Schankwirt', NULL, 23420), -- 43034
(20407, 'deDE', 'Scharfseherin Umbrua', NULL, 'Schamanenlehrerin', NULL, 23420), -- 20407
(5516, 'deDE', 'Ulfir Eisenbart', NULL, 'Jägerlehrer', NULL, 23420), -- 5516
(5515, 'deDE', 'Einris Prunkspeer', NULL, 'Jägerlehrerin', NULL, 23420), -- 5515
(44392, 'deDE', 'Fang', NULL, 'Karrinas Begleiter', NULL, 23420), -- 44392
(5517, 'deDE', 'Thorfin Steinschild', NULL, 'Jägerlehrer', NULL, 23420), -- 5517
(2879, 'deDE', 'Karrina Mekenda', NULL, 'Tierausbilderin', NULL, 23420), -- 2879
(11069, 'deDE', 'Jenova Steinschild', NULL, 'Stallmeisterin', NULL, 23420), -- 11069
(44243, 'deDE', 'Darnella Winford', NULL, 'Studentin der Archäologie', NULL, 23420), -- 44243
(44241, 'deDE', 'Lana Dubing', NULL, 'Studentin der Archäologie', NULL, 23420), -- 44241
(44239, 'deDE', 'Janric Moller', NULL, 'Student der Archäologie', NULL, 23420), -- 44239
(44238, 'deDE', 'Harrison Jones', NULL, 'Archäologielehrer', NULL, 23420), -- 44238
(93307, 'deDE', 'Rygarius', NULL, NULL, NULL, 23420), -- 93307
(93296, 'deDE', 'Kaffee', NULL, NULL, NULL, 23420), -- 93296
(88537, 'deDE', 'Magier von Sturmwind', NULL, NULL, NULL, 23420), -- 88537
(88535, 'deDE', 'Magierin von Sturmwind', NULL, NULL, NULL, 23420), -- 88535
(35365, 'deDE', 'Behsten', NULL, 'Erfahrungseliminierer', NULL, 23420), -- 35365
(24729, 'deDE', 'Alicia', NULL, NULL, NULL, 23420), -- 24729
(34998, 'deDE', 'Alison Devay', NULL, 'Kampfmeisterin', NULL, 23420), -- 34998
(34997, 'deDE', 'Devon Ferntal', NULL, 'Kampfmeister', NULL, 23420), -- 34997
(1751, 'deDE', 'Mithras Eisenberg', NULL, NULL, NULL, 23420), -- 1751
(15187, 'deDE', 'Cenarius'' Abgesandte Jademond', NULL, NULL, NULL, 23420), -- 15187
(1750, 'deDE', 'Großadmiralin Jes-Tereth', NULL, NULL, NULL, 23420), -- 1750
(17103, 'deDE', 'Abgesandter Taluun', NULL, NULL, NULL, 23420), -- 17103
(2285, 'deDE', 'Graf Remington Kronenbrunn', NULL, 'Adelshaus', NULL, 23420), -- 2285
(1439, 'deDE', 'Lord Baurles K. Wischock', NULL, 'Adelshaus', NULL, 23420), -- 1439
(107574, 'deDE', 'Anduin Wrynn', NULL, 'König von Sturmwind', NULL, 23420), -- Rey Anduin Wrynn
(4960, 'deDE', 'Bischof DeLavey', NULL, NULL, NULL, 23420), -- 4960
(1752, 'deDE', 'Caledra Morgenhauch', NULL, NULL, NULL, 23420), -- 1752
(58167, 'deDE', 'Colton Schmidt', NULL, NULL, NULL, 23420), -- 58167
(51938, 'deDE', 'Crithto', NULL, NULL, NULL, 23420), -- 51938
(47688, 'deDE', 'Offizierin Connelly', NULL, NULL, NULL, 23420), -- Oficial Connelly
(17252, 'deDE', 'Teufelswache', NULL, NULL, NULL, 23420), -- Guardia vil
(112144, 'deDE', 'Corgnelius', NULL, NULL, NULL, 23420), -- Corgnelius
(84915, 'deDE', 'Geschmolzenes Kernhündchen', NULL, NULL, NULL, 23420), -- Corgi de magma
(85009, 'deDE', 'Murkidan', NULL, NULL, NULL, 23420), -- Murkidan
(99403, 'deDE', 'Auferstandenes Säblerkätzchen', NULL, NULL, NULL, 23420), -- Gatito de sable resucitado
(113211, 'deDE', 'Offizier Schönufer', NULL, NULL, NULL, 23420), -- 113211
(78158, 'deDE', 'Verdammniswache', NULL, NULL, NULL, 23420), -- Guardia apocalíptico
(36911, 'deDE', 'Pandarenmönch', NULL, NULL, NULL, 23420), -- Monje pandaren
(55367, 'deDE', 'Dunkelmond-Zeppelin', NULL, NULL, NULL, 23420), -- Zepelín de la Luna Negra
(111202, 'deDE', 'Enthusiast der Allianz', NULL, NULL, NULL, 23420), -- Entusiasta de la Alianza
(1413, 'deDE', 'Janey Amschiff', NULL, NULL, NULL, 23420), -- 1413
(106549, 'deDE', 'Hati', NULL, NULL, NULL, 23420), -- 106549
(11673, 'deDE', 'Uralter Kernhund', NULL, NULL, NULL, 23420), -- 11673
(17447, 'deDE', 'Kurken', NULL, NULL, NULL, 23420), -- 17447
(42778, 'deDE', 'Admiral Knurrreißer', NULL, NULL, NULL, 23420), -- 42778
(42372, 'deDE', 'Vanessa van Cleef', 'Vanessa van Cleef', NULL, NULL, 23420), -- 42372
(42769, 'deDE', 'Lump der Defias', 'Lumpin der Defias', NULL, NULL, 23420), -- 42769
(42753, 'deDE', 'Helix Ritzelbrecher', NULL, NULL, NULL, 23420), -- 42753
(42755, 'deDE', 'Glubtok', NULL, 'Der Vorarbeiter', NULL, 23420), -- 42755
(42749, 'deDE', 'Hope Saldean', 'Hope Saldean', NULL, NULL, 23420), -- 42749
(42793, 'deDE', 'Sentinel Hill Fire Trigger', NULL, NULL, NULL, 23420), -- 42793
(42752, 'deDE', 'Späher Galiaan', NULL, 'Die Westfallbrigade', NULL, 23420), -- 42752
(42751, 'deDE', 'Hauptmann Danuvin', NULL, 'Die Westfallbrigade', NULL, 23420), -- 42751
(42748, 'deDE', 'Knurrreißer', NULL, NULL, NULL, 23420), -- 42748
(42771, 'deDE', 'Rise of the Brotherhood Event Dummy', NULL, NULL, NULL, 23420), -- 42771
(42745, 'deDE', 'Ermittler von Sturmwind', NULL, NULL, NULL, 23420), -- 42745
(42744, 'deDE', 'Leutnant Horatio Laine', NULL, NULL, NULL, 23420), -- 42744
(42750, 'deDE', 'Marschall Gryan Starkmantel', NULL, 'Die Westfallbrigade', NULL, 23420), -- 42750
(7543, 'deDE', 'Dunkelwelpling', NULL, NULL, NULL, 23420), -- Vástago oscuro
(513, 'deDE', 'Murlocnetzer', NULL, NULL, NULL, 23420), -- 513
(42669, 'deDE', 'Schluchtschleim', NULL, NULL, NULL, 23420), -- 42669
(171, 'deDE', 'Murlockrieger', NULL, NULL, NULL, 23420), -- 171
(458, 'deDE', 'Murlocjäger', NULL, NULL, NULL, 23420), -- 458
(6250, 'deDE', 'Kriecher', NULL, NULL, NULL, 23420), -- 6250
(520, 'deDE', 'Brack', NULL, NULL, NULL, 23420), -- 520
(34281, 'deDE', '[DND]Azeroth Children''s Week Trigger', NULL, NULL, NULL, 23420), -- 34281
(391, 'deDE', 'Trübauge der Alte', NULL, NULL, NULL, 23420), -- 391
(4305, 'deDE', 'Kriggon Talsone', NULL, 'Angler', NULL, 23420), -- 4305
(517, 'deDE', 'Murlocorakel', NULL, NULL, NULL, 23420), -- 517
(127, 'deDE', 'Murlocgezeitenjäger', NULL, NULL, NULL, 23420), -- 127
(1216, 'deDE', 'Küstenkriecher', NULL, NULL, NULL, 23420), -- 1216
(78217, 'deDE', 'Höllenbestie', NULL, NULL, NULL, 23420), -- Infernal
(42371, 'deDE', 'Vanessa van Cleef', NULL, NULL, NULL, 23420), -- 42371
(42698, 'deDE', 'Lump der Defias', 'Lumpin der Defias', NULL, NULL, 23420), -- 42698
(42697, 'deDE', 'Edwin van Cleef', NULL, 'Herrscher der Defias', NULL, 23420), -- 42697
(42699, 'deDE', 'Krieger der Allianz', NULL, NULL, NULL, 23420), -- 42699
(42703, 'deDE', 'Priesterin der Allianz', NULL, NULL, NULL, 23420), -- 42703
(42700, 'deDE', 'Schurke der Allianz', NULL, NULL, NULL, 23420), -- 42700
(42701, 'deDE', 'Jäger der Allianz', NULL, NULL, NULL, 23420), -- 42701
(42702, 'deDE', 'Magier der Allianz', 'Magierin der Allianz', NULL, NULL, 23420), -- 42702
(645, 'deDE', 'Krümel', NULL, 'Der Smutje', NULL, 23420), -- 645
(646, 'deDE', 'Handlanger Pein', NULL, 'Der Erste Maat', NULL, 23420), -- 646
(636, 'deDE', 'Lump der Defias', 'Lumpin der Defias', NULL, NULL, 23420), -- 636
(647, 'deDE', 'Kapitän Grünhaut', NULL, NULL, NULL, 23420), -- 647
(1732, 'deDE', 'Wellenformer der Defias', 'Wellenformerin der Defias', NULL, NULL, 23420), -- 1732
(3947, 'deDE', 'Goblinschiffbauer', NULL, NULL, NULL, 23420), -- 3947
(657, 'deDE', 'Pirat der Defias', NULL, NULL, NULL, 23420), -- 657
(42693, 'deDE', 'Vision der Vergangenheit', NULL, NULL, NULL, 23420), -- 42693
(47296, 'deDE', 'Helix Ritzelbrecher', NULL, NULL, NULL, 23420), -- 47296
(47297, 'deDE', 'Trampel', NULL, NULL, NULL, 23420), -- 47297
(43778, 'deDE', 'Feindschnitter 5000', NULL, NULL, NULL, 23420), -- 43778
(48445, 'deDE', 'Dümmlicher Lakai', NULL, NULL, NULL, 23420), -- 48445
(48280, 'deDE', 'Goblinhandwerker', NULL, NULL, NULL, 23420), -- 48280
(47404, 'deDE', 'Behüter der Defias', NULL, NULL, 'vehichleCursor', 23420), -- 47404
(47403, 'deDE', 'Schnitter der Defias', NULL, NULL, 'vehichleCursor', 23420), -- 47403
(48505, 'deDE', 'Schattenwächter der Defias', NULL, NULL, NULL, 23420), -- 48505
(48439, 'deDE', 'Gobliningenieur', NULL, NULL, NULL, 23420), -- 48439
(48351, 'deDE', 'Minenhase', NULL, 'Erfrischungen', NULL, 23420), -- 48351
(48502, 'deDE', 'Vollstrecker der Defias', 'Vollstreckerin der Defias', NULL, NULL, 23420), -- 48502
(48417, 'deDE', 'Blutzauberer der Defias', 'Blutzauberin der Defias', NULL, NULL, 23420), -- 48417
(48441, 'deDE', 'Bergbauaffe', NULL, NULL, NULL, 23420), -- 48441
(48442, 'deDE', 'Bergbauaffe', NULL, NULL, NULL, 23420), -- 48442
(48342, 'deDE', 'Goblinischer Cocktail', NULL, NULL, 'Pickup', 23420), -- 48342
(48341, 'deDE', 'Goblinischer Cocktail', NULL, NULL, 'Pickup', 23420), -- 48341
(48340, 'deDE', 'Getränketablett', NULL, NULL, NULL, 23420), -- 48340
(48338, 'deDE', 'Minenhase', NULL, 'Erfrischungen', NULL, 23420), -- 48338
(48343, 'deDE', 'Goblinischer Cocktail', NULL, NULL, 'Pickup', 23420), -- 48343
(48279, 'deDE', 'Goblinischer Aufseher', NULL, NULL, NULL, 23420), -- 48279
(48440, 'deDE', 'Bergbauaffe', NULL, NULL, NULL, 23420), -- 48440
(48278, 'deDE', 'Bergbauaffe', NULL, NULL, NULL, 23420), -- 48278
(47677, 'deDE', 'General Purpose Bunny JMF (Look 3)', NULL, NULL, NULL, 23420), -- 47677
(47162, 'deDE', 'Glubtok', NULL, 'Der Vorarbeiter', NULL, 23420), -- 47162
(47282, 'deDE', 'Fire Blossom Bunny', NULL, NULL, NULL, 23420), -- 47282
(48262, 'deDE', 'Ogerleibwächter', NULL, NULL, NULL, 23420), -- 48262
(47284, 'deDE', 'Frost Blossom Bunny', NULL, NULL, NULL, 23420), -- 47284
(47242, 'deDE', 'General Purpose Bunny JMF (Look 2)', NULL, NULL, NULL, 23420), -- 47242
(48266, 'deDE', 'Kanone der Defias', NULL, NULL, 'vehichleCursor', 23420), -- 48266
(48284, 'deDE', 'Bergbausprengstoff', NULL, NULL, NULL, 23420), -- 48284
(48446, 'deDE', 'Scorch Mark Bunny JMF', NULL, NULL, NULL, 23420), -- 48446
(48230, 'deDE', 'Ogerhandlanger', NULL, NULL, NULL, 23420), -- 48230
(48229, 'deDE', 'Koboldbuddler', NULL, NULL, NULL, 23420), -- 48229
(46612, 'deDE', 'Leutnant Horatio Laine', NULL, NULL, NULL, 23420), -- 46612
(46614, 'deDE', 'Ermittler von Sturmwind', NULL, NULL, NULL, 23420), -- 46614
(46613, 'deDE', 'Tatort-Alarm-o-Bot', NULL, NULL, NULL, 23420), -- 46613
(50595, 'deDE', 'Verteidiger von Sturmwind', 'Verteidigerin von Sturmwind', NULL, NULL, 23420), -- 50595
(53488, 'deDE', 'Summon Enabler Stalker', NULL, NULL, NULL, 23420), -- 53488
(392, 'deDE', 'Kapitän Grausohn', NULL, NULL, NULL, 23420), -- 392
(626, 'deDE', 'Großknecht Distelklette', NULL, NULL, NULL, 23420), -- 626
(624, 'deDE', 'Untoter Ausgräber', NULL, NULL, NULL, 23420), -- 624
(625, 'deDE', 'Untoter Dynamitexperte', NULL, NULL, NULL, 23420), -- 625
(623, 'deDE', 'Skelettminenarbeiter', NULL, NULL, NULL, 23420), -- 623
(596, 'deDE', 'Manipulierter Adliger', NULL, NULL, NULL, 23420), -- 596
(42683, 'deDE', 'Banner der Bruderschaft', NULL, NULL, NULL, 23420), -- 42683
(42680, 'deDE', 'Merkwürdige Gestalt', NULL, NULL, NULL, 23420), -- 42680
(43515, 'deDE', 'Moonbrook Player Trigger', NULL, NULL, NULL, 23420), -- 43515
(42679, 'deDE', 'VanCleef Event Dummy', NULL, NULL, NULL, 23420), -- 42679
(42677, 'deDE', 'Rohling von Mondbruch', NULL, NULL, NULL, 23420), -- 42677
(43948, 'deDE', 'Gefreiter Jackson', NULL, 'Versorger', NULL, 23420), -- 43948
(42651, 'deDE', 'Thoralius der Weise', NULL, NULL, NULL, 23420), -- 42651
(42425, 'deDE', 'Hauptmann Alpert', NULL, 'Die Westfallbrigade', NULL, 23420), -- 42425
(42426, 'deDE', 'Tina Himmelshorst', NULL, 'Greifenmeisterin', NULL, 23420), -- 42426
(452, 'deDE', 'Bandit der Flusspfoten', NULL, NULL, NULL, 23420), -- 452
(42653, 'deDE', 'Jango Fleckfell', NULL, NULL, NULL, 23420), -- 42653
(453, 'deDE', 'Mystiker der Flusspfoten', NULL, NULL, NULL, 23420), -- 453
(42662, 'deDE', 'Merkwürdige Gestalt', NULL, NULL, NULL, 23420), -- 42662
(42654, 'deDE', 'Helix'' Trampel', NULL, NULL, NULL, 23420), -- 42654
(42655, 'deDE', 'Helix Ritzelbrecher', NULL, NULL, NULL, 23420), -- 42655
(42656, 'deDE', 'Söldner', NULL, NULL, NULL, 23420), -- 42656
(98, 'deDE', 'Zuchtmeister der Flusspfoten', NULL, NULL, NULL, 23420), -- 98
(115, 'deDE', 'Ernteschnitter', NULL, NULL, NULL, 23420), -- 115
(7024, 'deDE', 'Agentin Kearnen', NULL, NULL, NULL, 23420), -- 7024
(2630, 'deDE', 'Totem der Erdbindung', NULL, NULL, NULL, 23420), -- 2630
(51915, 'deDE', 'Wache der Westfallbrigade', 'Wache der Westfallbrigade', NULL, NULL, 23420), -- 51915
(42652, 'deDE', 'Riverpaw Gnoll Proxy', NULL, NULL, NULL, 23420), -- 42652
(42357, 'deDE', 'Bulliger Geiferzahn', NULL, NULL, NULL, 23420), -- 42357
(154, 'deDE', 'Großer Fleischreißer', NULL, NULL, NULL, 23420), -- 154
(42617, 'deDE', 'Westfalleintopf', NULL, NULL, NULL, 23420), -- 42617
(49760, 'deDE', 'Zalna Webhellt', NULL, 'Magierlehrerin', NULL, 23420), -- 49760
(49745, 'deDE', 'Sern Heilich', NULL, 'Schurkenlehrer', NULL, 23420), -- 49745
(43011, 'deDE', 'Linda Hollister', NULL, 'Jägerlehrerin', NULL, 23420), -- 43011
(8934, 'deDE', 'Christopher Klopf', NULL, 'Handwerkswaren', NULL, 23420), -- 8934
(49769, 'deDE', 'Deborah Fain', NULL, 'Hexenmeisterlehrerin', NULL, 23420), -- 49769
(49749, 'deDE', 'Priesterin Cocoa Anderson', NULL, 'Priesterlehrerin', NULL, 23420), -- 49749
(49741, 'deDE', 'Schwester Darnhalt', NULL, 'Paladinlehrerin', NULL, 23420), -- 49741
(49736, 'deDE', 'Kalle Stanner', NULL, 'Kriegerlehrer', NULL, 23420), -- 49736
(8931, 'deDE', 'Gastwirtin Heather', NULL, 'Gastwirtin', NULL, 23420), -- 8931
(10045, 'deDE', 'Kirk Maxwell', NULL, 'Stallmeister', NULL, 23420), -- 10045
(1670, 'deDE', 'Mike Müller', NULL, 'Brothändler', NULL, 23420), -- 1670
(1668, 'deDE', 'William MacGregor', NULL, 'Bogenmacher', NULL, 23420), -- 1668
(843, 'deDE', 'Gina MacGregor', NULL, 'Handwerkswaren', NULL, 23420), -- 843
(876, 'deDE', 'Beschützer Leick', NULL, 'Die Westfallbrigade', NULL, 23420), -- 876
(870, 'deDE', 'Beschützerin Deni', NULL, 'Die Westfallbrigade', NULL, 23420), -- 870
(6670, 'deDE', 'Holzarbeiter von Westfall', NULL, NULL, NULL, 23420), -- 6670
(52190, 'deDE', 'Twilber Drillzang', NULL, 'Dampfpanzermechaniker', NULL, 23420), -- 52190
(842, 'deDE', 'Holzfäller', NULL, NULL, NULL, 23420),
(54372, 'deDE', 'Schläger der Flusspfoten', NULL, NULL, NULL, 23420), -- 54372
(874, 'deDE', 'Beschützer Korelor', NULL, 'Die Westfallbrigade', NULL, 23420), -- 874
(54373, 'deDE', 'Kräuterkundiger der Flusspfoten', NULL, NULL, NULL, 23420), -- 54373
(54371, 'deDE', 'Bandit der Flusspfoten', NULL, NULL, NULL, 23420), -- 54371
(487, 'deDE', 'Beschützer Bialon', NULL, 'Die Westfallbrigade', NULL, 23420), -- 487
(490, 'deDE', 'Beschützerin Gariel', NULL, 'Die Westfallbrigade', NULL, 23420), -- 490
(523, 'deDE', 'Thor', NULL, 'Greifenmeister', NULL, 23420), -- 523
(878, 'deDE', 'Späher Galiaan', NULL, 'Die Westfallbrigade', NULL, 23420), -- 878
(821, 'deDE', 'Hauptmann Danuvin', NULL, 'Die Westfallbrigade', NULL, 23420), -- 821
(820, 'deDE', 'Späherin Riell', NULL, 'Die Westfallbrigade', NULL, 23420), -- 820
(489, 'deDE', 'Beschützer Dutfeld', NULL, 'Die Westfallbrigade', NULL, 23420), -- 489
(234, 'deDE', 'Marschall Gryan Starkmantel', NULL, 'Die Westfallbrigade', NULL, 23420), -- 234
(42575, 'deDE', 'Hope Saldean', NULL, NULL, NULL, 23420), -- 42575
(491, 'deDE', 'Rüstmeister Lewis', NULL, 'Rüstmeister', NULL, 23420), -- 491
(488, 'deDE', 'Beschützer Weber', NULL, 'Die Westfallbrigade', NULL, 23420), -- 488
(42635, 'deDE', 'Knurrreißer', NULL, NULL, NULL, 23420), -- 42635
(42407, 'deDE', 'Wache der Späherkuppe', NULL, NULL, 'Directions', 23420), -- 42407
(42342, 'deDE', 'Aufgeladener Ernteschnitter', NULL, NULL, NULL, 23420), -- 42342
(124, 'deDE', 'Schläger der Flusspfoten', NULL, NULL, NULL, 23420), -- 124
(42381, 'deDE', 'Überladener Erntegolem', NULL, NULL, NULL, 23420), -- 42381
(869, 'deDE', 'Beschützerin Dorana', NULL, 'Die Westfallbrigade', NULL, 23420), -- 869
(233, 'deDE', 'Bauer Saldean', NULL, NULL, NULL, 23420), -- 233
(235, 'deDE', 'Salma Saldean', NULL, NULL, NULL, 23420), -- 235
(42560, 'deDE', 'Lou Zweischuh', NULL, NULL, NULL, 23420), -- 42560
(42559, 'deDE', 'Ermittler von Sturmwind', NULL, NULL, NULL, 23420), -- 42559
(42558, 'deDE', 'Leutnant Horatio Laine', NULL, NULL, NULL, 23420), -- 42558
(1424, 'deDE', 'Meisterbuddler', NULL, NULL, NULL, 23420), -- 1424
(501, 'deDE', 'Kräuterkundiger der Flusspfoten', NULL, NULL, NULL, 23420), -- 501
(123, 'deDE', 'Bastard der Flusspfoten', NULL, NULL, NULL, 23420), -- 123
(846, 'deDE', 'Verwester Ghul', NULL, NULL, NULL, 23420), -- 846
(572, 'deDE', 'Leprithus', NULL, NULL, NULL, 23420), -- 572
(42515, 'deDE', 'Merkwürdige Gestalt', NULL, NULL, NULL, 23420), -- 42515
(42492, 'deDE', 'Glubtok', NULL, 'Der Vorarbeiter', NULL, 23420), -- 42492
(42500, 'deDE', 'Lou Zweischuhs altes Haus', NULL, NULL, NULL, 23420), -- 42500
(1065, 'deDE', 'Schamane der Flusspfoten', NULL, NULL, NULL, 23420), -- 1065
(1236, 'deDE', 'Koboldbuddler', NULL, NULL, NULL, 23420), -- 1236
(42406, 'deDE', 'Hobohansa', NULL, 'Greifenmeister', NULL, 23420), -- 42406
(42562, 'deDE', 'Lous Abschiedsgedanken', NULL, NULL, NULL, 23420), -- 42562
(2620, 'deDE', 'Präriehund', NULL, NULL, NULL, 23420), -- 2620
(42497, 'deDE', 'Mama Celeste', NULL, NULL, NULL, 23420), -- 42497
(42405, 'deDE', 'Lou Zweischuh', NULL, NULL, NULL, 23420), -- 42405
(42390, 'deDE', 'Schmalspurganove', NULL, NULL, NULL, 23420), -- 42390
(42387, 'deDE', 'Rohling', NULL, NULL, NULL, 23420), -- 42387
(42498, 'deDE', 'Jimb "die Kerze" McHannigan', NULL, NULL, NULL, 23420), -- 42498
(462, 'deDE', 'Vultros', NULL, NULL, NULL, 23420), -- 462
(61143, 'deDE', 'Maus', NULL, NULL, 'wildpetcapturable', 23420), -- Ratón
(114, 'deDE', 'Erntebehüter', NULL, NULL, NULL, 23420), -- 114
(1109, 'deDE', 'Fleischreißer', NULL, NULL, NULL, 23420), -- 1109
(157, 'deDE', 'Geiferzahn', NULL, NULL, NULL, 23420), -- 157
(831, 'deDE', 'Meereskriecher', NULL, NULL, NULL, 23420), -- 831
(519, 'deDE', 'Slark', NULL, NULL, NULL, 23420), -- 519
(126, 'deDE', 'Murlocküstenläufer', NULL, NULL, NULL, 23420), -- 126
(60761, 'deDE', 'Uferkrebs', NULL, NULL, NULL, 23420), -- 60761
(456, 'deDE', 'Schwaches Murlocorakel', NULL, NULL, NULL, 23420), -- 456
(61158, 'deDE', 'Uferkrebs', NULL, NULL, 'wildpetcapturable', 23420), -- Cangrejo de costa
(515, 'deDE', 'Murlocräuber', NULL, NULL, NULL, 23420), -- 515
(830, 'deDE', 'Sandkriecher', NULL, NULL, NULL, 23420), -- 830
(42413, 'deDE', 'Gassenkind', 'Gassenkind', NULL, NULL, 23420), -- 42413
(117, 'deDE', 'Gnoll der Flusspfoten', NULL, NULL, NULL, 23420), -- 117
(500, 'deDE', 'Späher der Flusspfoten', NULL, NULL, NULL, 23420), -- 500
(42385, 'deDE', 'Waise', 'Waise', NULL, NULL, 23420), -- 42385
(61141, 'deDE', 'Präriehund', NULL, NULL, 'wildpetcapturable', 23420), -- Perro de la pradera
(2914, 'deDE', 'Schlange', NULL, NULL, NULL, 23420), -- 2914
(199, 'deDE', 'Junger Fleischreißer', NULL, NULL, NULL, 23420), -- 199
(42400, 'deDE', 'Streuner der Westlichen Ebenen', 'Streunerin der Westlichen Ebenen', NULL, NULL, 23420), -- 42400
(42402, 'deDE', 'Hobowaren (Aa)', NULL, NULL, NULL, 23420), -- 42402
(42401, 'deDE', 'Hobowaren (Tasche)', NULL, NULL, NULL, 23420), -- 42401
(42399, 'deDE', 'Hobokarren', NULL, NULL, NULL, 23420), -- 42399
(42403, 'deDE', 'Hobowaren (Stiefel)', NULL, NULL, NULL, 23420), -- 42403
(61142, 'deDE', 'Schlange', NULL, NULL, 'wildpetcapturable', 23420), -- Culebra
(833, 'deDE', 'Kojotenrudelanführer', NULL, NULL, NULL, 23420); -- 833