-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/24/2017 17:01:08


SET NAMES 'utf8';
INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(67893, "Wenn sich irgendjemand mit Steinen und Felsen auskennt, dann ein Zwerg!", "", 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(49842, "", "Tiefenheim hat sich in den letzten Jahren so sehr ver�ndert, es ist kaum wiederzuerkennen.$B$BUnd doch bin ich hier... frei von Verderbnis. Frei von dem verderbenden Einfluss der Uralten G�tter. Es scheint, als m�sste einem manchmal die Freiheit genommen werden, damit man ihren wahren Wert wieder zu sch�tzen wei�.", 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 23420),
(43562, "", "Dieser Steintrogg steckt hinter dem Auswuchs auf der Rubinspanne.$B$BIch sp�re eine seltsame Macht an diesem Ort... Seid vorsichtig.", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(109217, "Das sind beunruhigende Worte, Champion.", "", 0, 0, 0, 0, 0, 0, 1, 0, 0, 67030, 0, 0, 23420),
(109218, "Wir m�ssen den Schattenhammer besiegen und das Gleichgewicht in Tiefenheim wiederherstellen.", "", 0, 0, 0, 0, 0, 0, 1, 0, 0, 67031, 0, 0, 23420);


DELETE FROM  `broadcast_text_locale` WHERE (`ID`=67893 AND `locale`='deDE') OR (`ID`=49842 AND `locale`='deDE') OR (`ID`=43562 AND `locale`='deDE') OR (`ID`=109217 AND `locale`='deDE') OR (`ID`=109218 AND `locale`='deDE') OR 
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(67893, 'deDE', "Wenn sich irgendjemand mit Steinen und Felsen auskennt, dann ein Zwerg!", "", 23420),
(49842, 'deDE', "", "Tiefenheim hat sich in den letzten Jahren so sehr ver�ndert, es ist kaum wiederzuerkennen.$B$BUnd doch bin ich hier... frei von Verderbnis. Frei von dem verderbenden Einfluss der Uralten G�tter. Es scheint, als m�sste einem manchmal die Freiheit genommen werden, damit man ihren wahren Wert wieder zu sch�tzen wei�.", 23420),
(43562, 'deDE', "", "Dieser Steintrogg steckt hinter dem Auswuchs auf der Rubinspanne.$B$BIch sp�re eine seltsame Macht an diesem Ort... Seid vorsichtig.", 23420),
(109217, 'deDE', "Das sind beunruhigende Worte, Champion.", "", 23420),
(109218, 'deDE', "Wir m�ssen den Schattenhammer besiegen und das Gleichgewicht in Tiefenheim wiederherstellen.", "", 23420);
