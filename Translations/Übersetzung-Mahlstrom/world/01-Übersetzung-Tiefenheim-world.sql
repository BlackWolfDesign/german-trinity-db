-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/24/2017 17:01:08


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=29735 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(29735, 'deDE', 'Urahne Steinsiegel', '', '', '', '', '', '', '', '', 23420);


DELETE FROM `gameobject_template_locale` WHERE (`entry`=205386 AND `locale`='deDE') OR (`entry`=249399 AND `locale`='deDE') OR (`entry`=249406 AND `locale`='deDE') OR (`entry`=249408 AND `locale`='deDE') OR (`entry`=249407 AND `locale`='deDE') OR (`entry`=249400 AND `locale`='deDE') OR (`entry`=249398 AND `locale`='deDE') OR (`entry`=249395 AND `locale`='deDE') OR (`entry`=249394 AND `locale`='deDE') OR (`entry`=204390 AND `locale`='deDE') OR (`entry`=204392 AND `locale`='deDE') OR (`entry`=204391 AND `locale`='deDE') OR (`entry`=204274 AND `locale`='deDE') OR (`entry`=205145 AND `locale`='deDE') OR (`entry`=205193 AND `locale`='deDE') OR (`entry`=203750 AND `locale`='deDE') OR (`entry`=203749 AND `locale`='deDE') OR (`entry`=203748 AND `locale`='deDE') OR (`entry`=204376 AND `locale`='deDE') OR (`entry`=204377 AND `locale`='deDE') OR (`entry`=205208 AND `locale`='deDE') OR (`entry`=204374 AND `locale`='deDE') OR (`entry`=204375 AND `locale`='deDE') OR (`entry`=204348 AND `locale`='deDE') OR (`entry`=191707 AND `locale`='deDE') OR (`entry`=206562 AND `locale`='deDE') OR (`entry`=205196 AND `locale`='deDE') OR (`entry`=207597 AND `locale`='deDE') OR (`entry`=207593 AND `locale`='deDE') OR (`entry`=204837 AND `locale`='deDE') OR (`entry`=205360 AND `locale`='deDE') OR (`entry`=204420 AND `locale`='deDE') OR (`entry`=207494 AND `locale`='deDE') OR (`entry`=204045 AND `locale`='deDE') OR (`entry`=205147 AND `locale`='deDE') OR (`entry`=202739 AND `locale`='deDE') OR (`entry`=205152 AND `locale`='deDE') OR (`entry`=205197 AND `locale`='deDE') OR (`entry`=205207 AND `locale`='deDE') OR (`entry`=202778 AND `locale`='deDE') OR (`entry`=205151 AND `locale`='deDE') OR (`entry`=202741 AND `locale`='deDE') OR (`entry`=204959 AND `locale`='deDE') OR (`entry`=207390 AND `locale`='deDE') OR (`entry`=205195 AND `locale`='deDE') OR (`entry`=205098 AND `locale`='deDE') OR (`entry`=205134 AND `locale`='deDE') OR (`entry`=205097 AND `locale`='deDE') OR (`entry`=205146 AND `locale`='deDE') OR (`entry`=206980 AND `locale`='deDE') OR (`entry`=206982 AND `locale`='deDE') OR (`entry`=206984 AND `locale`='deDE') OR (`entry`=206983 AND `locale`='deDE') OR (`entry`=206981 AND `locale`='deDE') OR (`entry`=204297 AND `locale`='deDE') OR (`entry`=204296 AND `locale`='deDE') OR (`entry`=205161 AND `locale`='deDE') OR (`entry`=204253 AND `locale`='deDE') OR (`entry`=202750 AND `locale`='deDE') OR (`entry`=208261 AND `locale`='deDE') OR (`entry`=208257 AND `locale`='deDE') OR (`entry`=208256 AND `locale`='deDE') OR (`entry`=208258 AND `locale`='deDE') OR (`entry`=208260 AND `locale`='deDE') OR (`entry`=208259 AND `locale`='deDE') OR (`entry`=205273 AND `locale`='deDE') OR (`entry`=204407 AND `locale`='deDE');
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(205386, 'deDE', 'Fass', '', NULL, 23420),
(249399, 'deDE', 'Zwielichtrunenzirkel', '', NULL, 23420),
(249406, 'deDE', 'Tagebuch der Meisterin, Teil 1', '', NULL, 23420),
(249408, 'deDE', 'Tagebuch der Meisterin, Teil 3', '', NULL, 23420),
(249407, 'deDE', 'Tagebuch der Meisterin, Teil 2', '', NULL, 23420),
(249400, 'deDE', 'Zwielichtaltar', '', NULL, 23420),
(249398, 'deDE', 'Zwielichtrunenzirkel', '', NULL, 23420),
(249395, 'deDE', 'Zwielichtdrachenei', '', NULL, 23420),
(249394, 'deDE', 'Zwielichtdrachenei', '', NULL, 23420),
(204390, 'deDE', 'Patronenhülsenglitzern', '', NULL, 23420),
(204392, 'deDE', 'Kanonier Sparkles', '', NULL, 23420),
(204391, 'deDE', 'Glitzerzeug des Kapitäns', '', NULL, 23420),
(204274, 'deDE', 'Logbuch des Kapitäns', '', NULL, 23420),
(205145, 'deDE', 'Einweg-Dechiffrierungsmaschine', '', NULL, 23420),
(205193, 'deDE', 'Steinschuppenmatriarchinfokus', '', NULL, 23420),
(203750, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(203749, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(203748, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204376, 'deDE', 'Katapultteil', '', NULL, 23420),
(204377, 'deDE', 'Katapultteil', '', NULL, 23420),
(205208, 'deDE', 'Katapultteil', '', NULL, 23420),
(204374, 'deDE', 'Katapultteil', '', NULL, 23420),
(204375, 'deDE', 'Katapultteil', '', NULL, 23420),
(204348, 'deDE', 'Donnerstein', '', NULL, 23420),
(191707, 'deDE', 'Schattenmondrune 1', '', NULL, 23420),
(206562, 'deDE', 'Des Steinvaters Tresor', '', NULL, 23420),
(205196, 'deDE', 'Funken', '', NULL, 23420),
(207597, 'deDE', 'Lost Isles Tree Fire 02 (Large)', '', NULL, 23420),
(207593, 'deDE', 'Lost Isles Tree Fire 02 - Medium (Large)', '', NULL, 23420),
(204837, 'deDE', 'Blasser Resonanzkristall', '', NULL, 23420),
(205360, 'deDE', 'Portal zum Tempel der Erde', '', NULL, 23420),
(204420, 'deDE', 'Gruftpilz', '', NULL, 23420),
(207494, 'deDE', 'Stabile Schatzkiste', '', NULL, 23420),
(204045, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(205147, 'deDE', 'Sprießender Rubinpilz', '', NULL, 23420),
(202739, 'deDE', 'Reiches Obsidiumvorkommen', '', NULL, 23420),
(205152, 'deDE', 'Sprießender Rubinpilz', '', NULL, 23420),
(205197, 'deDE', 'Troggkiste', '', NULL, 23420),
(205207, 'deDE', 'Maziels Tagebuch', '', NULL, 23420),
(202778, 'deDE', 'Ein Schwarm Albinohöhlenfische', '', NULL, 23420),
(205151, 'deDE', 'Sprießender Rubinpilz', '', NULL, 23420),
(202741, 'deDE', 'Reiche Elementiumader', '', NULL, 23420),
(204959, 'deDE', 'Riesiger Painithaufen', '', NULL, 23420),
(207390, 'deDE', 'Tiefenquarzkristallbrocken', '', NULL, 23420),
(205195, 'deDE', 'Painitsplitter', '', NULL, 23420),
(205098, 'deDE', 'Kugelfessel', '', NULL, 23420),
(205134, 'deDE', 'Logbuch des Schmiedemeisters', '', NULL, 23420),
(205097, 'deDE', 'Kasten des Sklavenmeisters', '', NULL, 23420),
(205146, 'deDE', 'Sprießender Rubinpilz', '', NULL, 23420),
(206980, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206982, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206984, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206983, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206981, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204297, 'deDE', 'Kalkhaltige Kristallformation', '', NULL, 23420),
(204296, 'deDE', 'Kalkhaltige Kristallformation', '', NULL, 23420),
(205161, 'deDE', 'Torsteuerung', '', NULL, 23420),
(204253, 'deDE', 'Jadekristallformation', '', NULL, 23420),
(202750, 'deDE', 'Herzblüte', '', NULL, 23420),
(208261, 'deDE', 'Schmiede', '', NULL, 23420),
(208257, 'deDE', 'Amboss', '', NULL, 23420),
(208256, 'deDE', 'Tragbarer Briefkasten der Forscherliga', '', NULL, 23420),
(208258, 'deDE', 'Herbeigezauberter Reliktbriefkasten', '', NULL, 23420),
(208260, 'deDE', 'Amboss', '', NULL, 23420),
(208259, 'deDE', 'Schmiede', '', NULL, 23420),
(205273, 'deDE', 'Portal nach Orgrimmar', '', NULL, 23420),
(204407, 'deDE', 'Räuchergefäß', '', NULL, 23420);


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text     xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_offer_reward` WHERE `ID`=29735;
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(29735, 1, 0, 0, 0, 0, 0, 0, 0, 'Ich spüre das Feuer des Lebens in Euch, $C. Ich akzeptiere Eure Ehrerbietung! Nehmt dies als Zeichen meiner Dankbarkeit...', 23420); -- 29735


-- someone unknown
INSERT IGNORE INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `comment`) VALUES
(105802, 0, 0, 'Ihr wollt den Hammer aufhalten? Das glaube ich nicht!', 12, 0, 100, 53, 0, 0, 0, 'Zwielichterdbrecher to Player'),
(105814, 0, 0, 'Die Stunde des Zwielichts ist angebrochen!', 12, 0, 100, 0, 0, 0, 0, 'Zwielichtaszendent to Player');


DELETE FROM `locales_creature_text` WHERE (`entry`= 105802 AND `groupid`=0) OR (`entry`= 105814 AND `groupid`=0);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(105802, 0, 0, '', '', 'Ihr wollt den Hammer aufhalten? Das glaube ich nicht!', '', '', '', '', ''),
(105814, 0, 0, '', '', 'Die Stunde des Zwielichts ist angebrochen!', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=44931 /*44931*/ AND `locale`='deDE') OR (`entry`=43967 /*43967*/ AND `locale`='deDE') OR (`entry`=44930 /*44930*/ AND `locale`='deDE') OR (`entry`=44221 /*44221*/ AND `locale`='deDE') OR (`entry`=43966 /*43966*/ AND `locale`='deDE') OR (`entry`=45363 /*45363*/ AND `locale`='deDE') OR (`entry`=45361 /*45361*/ AND `locale`='deDE') OR (`entry`=45364 /*45364*/ AND `locale`='deDE') OR (`entry`=44222 /*44222*/ AND `locale`='deDE') OR (`entry`=44010 /*44010*/ AND `locale`='deDE') OR (`entry`=44046 /*44046*/ AND `locale`='deDE') OR (`entry`=44039 /*44039*/ AND `locale`='deDE') OR (`entry`=43992 /*43992*/ AND `locale`='deDE') OR (`entry`=105796 /*105796*/ AND `locale`='deDE') OR (`entry`=105797 /*105797*/ AND `locale`='deDE') OR (`entry`=105798 /*105798*/ AND `locale`='deDE') OR (`entry`=106263 /*106263*/ AND `locale`='deDE') OR (`entry`=106262 /*106262*/ AND `locale`='deDE') OR (`entry`=105794 /*105794*/ AND `locale`='deDE') OR (`entry`=105802 /*105802*/ AND `locale`='deDE') OR (`entry`=105821 /*105821*/ AND `locale`='deDE') OR (`entry`=105329 /*105329*/ AND `locale`='deDE') OR (`entry`=105312 /*105312*/ AND `locale`='deDE') OR (`entry`=105311 /*105311*/ AND `locale`='deDE') OR (`entry`=105965 /*105965*/ AND `locale`='deDE') OR (`entry`=105814 /*105814*/ AND `locale`='deDE') OR (`entry`=105804 /*105804*/ AND `locale`='deDE') OR (`entry`=43046 /*43046*/ AND `locale`='deDE') OR (`entry`=43044 /*43044*/ AND `locale`='deDE') OR (`entry`=43032 /*43032*/ AND `locale`='deDE') OR (`entry`=49758 /*49758*/ AND `locale`='deDE') OR (`entry`=43048 /*43048*/ AND `locale`='deDE') OR (`entry`=43026 /*43026*/ AND `locale`='deDE') OR (`entry`=44849 /*44849*/ AND `locale`='deDE') OR (`entry`=44847 /*44847*/ AND `locale`='deDE') OR (`entry`=43971 /*43971*/ AND `locale`='deDE') OR (`entry`=43981 /*43981*/ AND `locale`='deDE') OR (`entry`=44372 /*44372*/ AND `locale`='deDE') OR (`entry`=41960 /*41960*/ AND `locale`='deDE') OR (`entry`=41956 /*41956*/ AND `locale`='deDE') OR (`entry`=44649 /*44649*/ AND `locale`='deDE') OR (`entry`=44619 /*44619*/ AND `locale`='deDE') OR (`entry`=41946 /*41946*/ AND `locale`='deDE') OR (`entry`=41945 /*41945*/ AND `locale`='deDE') OR (`entry`=44839 /*44839*/ AND `locale`='deDE') OR (`entry`=42861 /*42861*/ AND `locale`='deDE') OR (`entry`=47195 /*47195*/ AND `locale`='deDE') OR (`entry`=45043 /*45043*/ AND `locale`='deDE') OR (`entry`=25670 /*25670*/ AND `locale`='deDE') OR (`entry`=43984 /*43984*/ AND `locale`='deDE') OR (`entry`=43995 /*43995*/ AND `locale`='deDE') OR (`entry`=43897 /*43897*/ AND `locale`='deDE') OR (`entry`=43954 /*43954*/ AND `locale`='deDE') OR (`entry`=43960 /*43960*/ AND `locale`='deDE') OR (`entry`=43952 /*43952*/ AND `locale`='deDE') OR (`entry`=45064 /*45064*/ AND `locale`='deDE') OR (`entry`=43755 /*43755*/ AND `locale`='deDE') OR (`entry`=43753 /*43753*/ AND `locale`='deDE') OR (`entry`=43456 /*43456*/ AND `locale`='deDE') OR (`entry`=43752 /*43752*/ AND `locale`='deDE') OR (`entry`=43319 /*43319*/ AND `locale`='deDE') OR (`entry`=43169 /*43169*/ AND `locale`='deDE') OR (`entry`=43168 /*43168*/ AND `locale`='deDE') OR (`entry`=44972 /*44972*/ AND `locale`='deDE') OR (`entry`=43233 /*43233*/ AND `locale`='deDE') OR (`entry`=43234 /*43234*/ AND `locale`='deDE') OR (`entry`=43228 /*43228*/ AND `locale`='deDE') OR (`entry`=43250 /*43250*/ AND `locale`='deDE') OR (`entry`=43229 /*43229*/ AND `locale`='deDE') OR (`entry`=43174 /*43174*/ AND `locale`='deDE') OR (`entry`=55216 /*55216*/ AND `locale`='deDE') OR (`entry`=47197 /*47197*/ AND `locale`='deDE') OR (`entry`=44970 /*44970*/ AND `locale`='deDE') OR (`entry`=44204 /*44204*/ AND `locale`='deDE') OR (`entry`=43871 /*43871*/ AND `locale`='deDE') OR (`entry`=43160 /*43160*/ AND `locale`='deDE') OR (`entry`=43071 /*43071*/ AND `locale`='deDE') OR (`entry`=43134 /*43134*/ AND `locale`='deDE') OR (`entry`=44220 /*44220*/ AND `locale`='deDE') OR (`entry`=44855 /*44855*/ AND `locale`='deDE') OR (`entry`=44218 /*44218*/ AND `locale`='deDE') OR (`entry`=44143 /*44143*/ AND `locale`='deDE') OR (`entry`=44126 /*44126*/ AND `locale`='deDE') OR (`entry`=43232 /*43232*/ AND `locale`='deDE') OR (`entry`=43170 /*43170*/ AND `locale`='deDE') OR (`entry`=43138 /*43138*/ AND `locale`='deDE') OR (`entry`=47184 /*47184*/ AND `locale`='deDE') OR (`entry`=42521 /*42521*/ AND `locale`='deDE') OR (`entry`=42469 /*42469*/ AND `locale`='deDE') OR (`entry`=43344 /*43344*/ AND `locale`='deDE') OR (`entry`=43339 /*43339*/ AND `locale`='deDE') OR (`entry`=43182 /*43182*/ AND `locale`='deDE') OR (`entry`=43765 /*43765*/ AND `locale`='deDE') OR (`entry`=42466 /*42466*/ AND `locale`='deDE') OR (`entry`=43763 /*43763*/ AND `locale`='deDE') OR (`entry`=44035 /*44035*/ AND `locale`='deDE') OR (`entry`=44049 /*44049*/ AND `locale`='deDE') OR (`entry`=43616 /*43616*/ AND `locale`='deDE') OR (`entry`=43598 /*43598*/ AND `locale`='deDE') OR (`entry`=45191 /*45191*/ AND `locale`='deDE') OR (`entry`=43181 /*43181*/ AND `locale`='deDE') OR (`entry`=43545 /*43545*/ AND `locale`='deDE') OR (`entry`=42522 /*42522*/ AND `locale`='deDE') OR (`entry`=43586 /*43586*/ AND `locale`='deDE') OR (`entry`=44947 /*44947*/ AND `locale`='deDE') OR (`entry`=48520 /*48520*/ AND `locale`='deDE') OR (`entry`=45408 /*45408*/ AND `locale`='deDE') OR (`entry`=45407 /*45407*/ AND `locale`='deDE') OR (`entry`=45033 /*45033*/ AND `locale`='deDE') OR (`entry`=44945 /*44945*/ AND `locale`='deDE') OR (`entry`=44973 /*44973*/ AND `locale`='deDE') OR (`entry`=43806 /*43806*/ AND `locale`='deDE') OR (`entry`=43805 /*43805*/ AND `locale`='deDE') OR (`entry`=43804 /*43804*/ AND `locale`='deDE') OR (`entry`=42465 /*42465*/ AND `locale`='deDE') OR (`entry`=43503 /*43503*/ AND `locale`='deDE') OR (`entry`=43513 /*43513*/ AND `locale`='deDE') OR (`entry`=43372 /*43372*/ AND `locale`='deDE') OR (`entry`=42543 /*42543*/ AND `locale`='deDE') OR (`entry`=43368 /*43368*/ AND `locale`='deDE') OR (`entry`=43388 /*43388*/ AND `locale`='deDE') OR (`entry`=44425 /*44425*/ AND `locale`='deDE') OR (`entry`=42475 /*42475*/ AND `locale`='deDE') OR (`entry`=43374 /*43374*/ AND `locale`='deDE') OR (`entry`=43480 /*43480*/ AND `locale`='deDE') OR (`entry`=42472 /*42472*/ AND `locale`='deDE') OR (`entry`=44967 /*44967*/ AND `locale`='deDE') OR (`entry`=44875 /*44875*/ AND `locale`='deDE') OR (`entry`=44879 /*44879*/ AND `locale`='deDE') OR (`entry`=44936 /*44936*/ AND `locale`='deDE') OR (`entry`=43780 /*43780*/ AND `locale`='deDE') OR (`entry`=43785 /*43785*/ AND `locale`='deDE') OR (`entry`=41957 /*41957*/ AND `locale`='deDE') OR (`entry`=62916 /*62916*/ AND `locale`='deDE') OR (`entry`=43158 /*43158*/ AND `locale`='deDE') OR (`entry`=44988 /*44988*/ AND `locale`='deDE') OR (`entry`=49847 /*49847*/ AND `locale`='deDE') OR (`entry`=53894 /*53894*/ AND `locale`='deDE') OR (`entry`=53739 /*53739*/ AND `locale`='deDE') OR (`entry`=43370 /*43370*/ AND `locale`='deDE') OR (`entry`=49822 /*49822*/ AND `locale`='deDE') OR (`entry`=49865 /*49865*/ AND `locale`='deDE') OR (`entry`=47072 /*47072*/ AND `locale`='deDE') OR (`entry`=49816 /*49816*/ AND `locale`='deDE') OR (`entry`=49867 /*49867*/ AND `locale`='deDE') OR (`entry`=49866 /*49866*/ AND `locale`='deDE') OR (`entry`=42711 /*42711*/ AND `locale`='deDE') OR (`entry`=43367 /*43367*/ AND `locale`='deDE') OR (`entry`=47071 /*47071*/ AND `locale`='deDE') OR (`entry`=43373 /*43373*/ AND `locale`='deDE') OR (`entry`=49824 /*49824*/ AND `locale`='deDE') OR (`entry`=62927 /*62927*/ AND `locale`='deDE') OR (`entry`=49815 /*49815*/ AND `locale`='deDE') OR (`entry`=44258 /*44258*/ AND `locale`='deDE') OR (`entry`=95893 /*95893*/ AND `locale`='deDE') OR (`entry`=40789 /*40789*/ AND `locale`='deDE') OR (`entry`=44403 /*44403*/ AND `locale`='deDE') OR (`entry`=48543 /*48543*/ AND `locale`='deDE') OR (`entry`=50060 /*50060*/ AND `locale`='deDE') OR (`entry`=42766 /*42766*/ AND `locale`='deDE') OR (`entry`=44259 /*44259*/ AND `locale`='deDE') OR (`entry`=44768 /*44768*/ AND `locale`='deDE') OR (`entry`=62117 /*62117*/ AND `locale`='deDE') OR (`entry`=43218 /*43218*/ AND `locale`='deDE') OR (`entry`=45084 /*45084*/ AND `locale`='deDE') OR (`entry`=45988 /*45988*/ AND `locale`='deDE') OR (`entry`=62915 /*62915*/ AND `locale`='deDE') OR (`entry`=42921 /*42921*/ AND `locale`='deDE') OR (`entry`=42823 /*42823*/ AND `locale`='deDE') OR (`entry`=42926 /*42926*/ AND `locale`='deDE') OR (`entry`=42900 /*42900*/ AND `locale`='deDE') OR (`entry`=42468 /*42468*/ AND `locale`='deDE') OR (`entry`=43101 /*43101*/ AND `locale`='deDE') OR (`entry`=42525 /*42525*/ AND `locale`='deDE') OR (`entry`=42523 /*42523*/ AND `locale`='deDE') OR (`entry`=50041 /*50041*/ AND `locale`='deDE') OR (`entry`=43115 /*43115*/ AND `locale`='deDE') OR (`entry`=42524 /*42524*/ AND `locale`='deDE') OR (`entry`=48692 /*48692*/ AND `locale`='deDE') OR (`entry`=44889 /*44889*/ AND `locale`='deDE') OR (`entry`=44887 /*44887*/ AND `locale`='deDE') OR (`entry`=44890 /*44890*/ AND `locale`='deDE') OR (`entry`=44885 /*44885*/ AND `locale`='deDE') OR (`entry`=42924 /*42924*/ AND `locale`='deDE') OR (`entry`=42917 /*42917*/ AND `locale`='deDE') OR (`entry`=42925 /*42925*/ AND `locale`='deDE') OR (`entry`=44189 /*44189*/ AND `locale`='deDE') OR (`entry`=44257 /*44257*/ AND `locale`='deDE') OR (`entry`=44888 /*44888*/ AND `locale`='deDE') OR (`entry`=44886 /*44886*/ AND `locale`='deDE') OR (`entry`=43358 /*43358*/ AND `locale`='deDE') OR (`entry`=49929 /*49929*/ AND `locale`='deDE') OR (`entry`=42899 /*42899*/ AND `locale`='deDE') OR (`entry`=42467 /*42467*/ AND `locale`='deDE') OR (`entry`=62922 /*62922*/ AND `locale`='deDE') OR (`entry`=43356 /*43356*/ AND `locale`='deDE') OR (`entry`=62925 /*62925*/ AND `locale`='deDE') OR (`entry`=44891 /*44891*/ AND `locale`='deDE') OR (`entry`=44835 /*44835*/ AND `locale`='deDE') OR (`entry`=49858 /*49858*/ AND `locale`='deDE') OR (`entry`=42606 /*42606*/ AND `locale`='deDE') OR (`entry`=42607 /*42607*/ AND `locale`='deDE') OR (`entry`=42574 /*42574*/ AND `locale`='deDE') OR (`entry`=62924 /*62924*/ AND `locale`='deDE') OR (`entry`=43258 /*43258*/ AND `locale`='deDE') OR (`entry`=43254 /*43254*/ AND `locale`='deDE') OR (`entry`=49770 /*49770*/ AND `locale`='deDE') OR (`entry`=43123 /*43123*/ AND `locale`='deDE') OR (`entry`=66805 /*66805*/ AND `locale`='deDE') OR (`entry`=66804 /*66804*/ AND `locale`='deDE') OR (`entry`=66802 /*66802*/ AND `locale`='deDE') OR (`entry`=49771 /*49771*/ AND `locale`='deDE') OR (`entry`=66815 /*66815*/ AND `locale`='deDE') OR (`entry`=45294 /*45294*/ AND `locale`='deDE') OR (`entry`=45289 /*45289*/ AND `locale`='deDE') OR (`entry`=44802 /*44802*/ AND `locale`='deDE') OR (`entry`=44799 /*44799*/ AND `locale`='deDE') OR (`entry`=45305 /*45305*/ AND `locale`='deDE') OR (`entry`=45297 /*45297*/ AND `locale`='deDE') OR (`entry`=45293 /*45293*/ AND `locale`='deDE') OR (`entry`=45290 /*45290*/ AND `locale`='deDE') OR (`entry`=44823 /*44823*/ AND `locale`='deDE') OR (`entry`=44818 /*44818*/ AND `locale`='deDE') OR (`entry`=45039 /*45039*/ AND `locale`='deDE') OR (`entry`=45037 /*45037*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(44931, 'deDE', 'Elementartor', '', NULL, NULL, 23420), -- 44931
(43967, 'deDE', 'Zwielichtschuppenschwester', '', NULL, NULL, 23420), -- 43967
(44930, 'deDE', 'Zwielichttor', '', NULL, NULL, 23420), -- 44930
(44221, 'deDE', 'Kultist der Eidschuppen', '', NULL, NULL, 23420), -- 44221
(43966, 'deDE', 'Zwielichtdrachenbrut', '', NULL, NULL, 23420), -- 43966
(45363, 'deDE', 'Erdheiler Doros', '', 'Reparaturen', NULL, 23420), -- 45363
(45361, 'deDE', 'Hagrid Flammenschwinge', '', 'Gemischtwaren', NULL, 23420), -- 45361
(45364, 'deDE', 'Leichnam eines Schattenhammerkultisten', '', NULL, NULL, 23420), -- 45364
(44222, 'deDE', 'Seher Galekk', '', NULL, NULL, 23420), -- 44222
(44010, 'deDE', 'Sturmruferin Mylra', '', NULL, NULL, 23420), -- 44010
(44046, 'deDE', 'Schamane des Irdenen Rings', '', NULL, NULL, 23420), -- 44046
(44039, 'deDE', 'Gefangener Schattenhammerkultist', '', NULL, NULL, 23420), -- 44039
(43992, 'deDE', 'Zwielichtdrachenjäger', '', NULL, NULL, 23420), -- 43992
(105796, 'deDE', 'Terrune der Unnachgiebige', '', NULL, 'questinteract', 23420), -- 105796
(105797, 'deDE', 'Urgar der Unerschütterliche', '', NULL, 'questinteract', 23420), -- 105797
(105798, 'deDE', 'Grognak der Brutale', '', NULL, 'questinteract', 23420), -- 105798
(106263, 'deDE', 'Schamanin des Irdenen Rings', '', 'Der Irdene Ring', NULL, 23420), -- 106263
(106262, 'deDE', 'Schamane des Irdenen Rings', '', 'Der Irdene Ring', NULL, 23420), -- 106262
(105794, 'deDE', 'Zwielichtsklave', '', NULL, 'questinteract', 23420), -- 105794
(105802, 'deDE', 'Zwielichterdbrecher', '', NULL, NULL, 23420), -- 105802
(105821, 'deDE', 'Bezauberter Kristallwüter', '', NULL, NULL, 23420), -- 105821
(105329, 'deDE', 'Zwielichtverderbnisbringer', '', NULL, NULL, 23420), -- 105329
(105312, 'deDE', 'Zwielichtrufer der Dunkelheit', '', NULL, NULL, 23420), -- 105312
(105311, 'deDE', 'Herrin des Zwielichts', '', NULL, NULL, 23420), -- 105311
(105965, 'deDE', 'Wyrmschwingenpirscher', '', 'Zwielichtdrachenschwarm', NULL, 23420), -- 105965
(105814, 'deDE', 'Zwielichtaszendent', '', NULL, NULL, 23420), -- 105814
(105804, 'deDE', 'Drachenwelpe', '', 'Zwielichtdrachenschwarm', NULL, 23420), -- 105804
(43046, 'deDE', 'Nicht explodierte Artilleriegranate', '', NULL, 'Inspect', 23420), -- 43046
(43044, 'deDE', 'Nicht explodierte Artilleriegranate', '', NULL, 'Inspect', 23420), -- 43044
(43032, 'deDE', 'Erschlagener Kanonier', '', NULL, 'Inspect', 23420), -- 43032
(49758, 'deDE', 'Mitreisende Ratte', '', NULL, NULL, 23420), -- 49758
(43048, 'deDE', 'Kapitän Schädeltrümmerer', '', NULL, 'Inspect', 23420), -- 43048
(43026, 'deDE', 'Tiefensteinelementar', '', NULL, NULL, 23420), -- 43026
(44849, 'deDE', 'Zwielichtzermalmer', '', NULL, NULL, 23420), -- 44849
(44847, 'deDE', 'Zwielichtwaffenmeister', '', NULL, NULL, 23420), -- 44847
(43971, 'deDE', 'Steinschuppendrache', '', NULL, NULL, 23420), -- 43971
(43981, 'deDE', 'Jaderückenbasilisk', '', NULL, NULL, 23420), -- 43981
(44372, 'deDE', 'Lavinius', '', 'Elementarlord', NULL, 23420), -- 44372
(41960, 'deDE', 'Deepholm Stalker Beam Target 04', '', NULL, NULL, 23420), -- 41960
(41956, 'deDE', 'Deepholm Stalker Beam Target 02', '', NULL, NULL, 23420), -- 41956
(44649, 'deDE', 'Zwielichtzenturio', '', NULL, NULL, 23420), -- 44649
(44619, 'deDE', 'Zwielichtbinder', '', NULL, NULL, 23420), -- 44619
(41946, 'deDE', 'Deepholm Stalker Beam Target 01', '', NULL, NULL, 23420), -- 41946
(41945, 'deDE', 'Deepholm Stalker Beam Target 00', '', NULL, NULL, 23420), -- 41945
(44839, 'deDE', 'Fly Over Kill Credit', '', NULL, NULL, 23420), -- 44839
(42861, 'deDE', 'Deepholm Stalker Beam Target 06', '', NULL, NULL, 23420), -- 42861
(47195, 'deDE', 'Schiefer Treibsand', '', 'Erzgeomant', NULL, 23420), -- 47195
(45043, 'deDE', 'Spitz Schleifstein', '', 'Katapultfahrer', NULL, 23420), -- 45043
(25670, 'deDE', 'ELM General Purpose Bunny (scale x3)', '', NULL, NULL, 23420), -- 25670
(43984, 'deDE', 'Deaktiviertes Kriegskonstrukt', '', NULL, NULL, 23420), -- 43984
(43995, 'deDE', 'Mystiker der Nadelfelsen', '', NULL, NULL, 23420), -- 43995
(43897, 'deDE', 'Pyrium Leitstein', '', 'Katapultkommandant', NULL, 23420), -- 43897
(43954, 'deDE', 'Fungusschrecken', '', NULL, NULL, 23420), -- 43954
(43960, 'deDE', 'Verstärkung der Steintroggs', '', NULL, NULL, 23420), -- 43960
(43952, 'deDE', 'Katapult der Irdenen', '', NULL, 'Gunner', 23420), -- 43952
(45064, 'deDE', 'Katapultfahrer', '', NULL, NULL, 23420), -- 45064
(43755, 'deDE', 'Deepholm Stalker Beam Target 12', '', NULL, NULL, 23420), -- 43755
(43753, 'deDE', 'Deepholm Stalker Beam Target 10', '', NULL, NULL, 23420), -- 43753
(43456, 'deDE', 'Troggzor der Erdinator', '', 'Schlachtenführer der Steintroggs', NULL, 23420), -- 43456
(43752, 'deDE', 'Deepholm Stalker Beam Target 08', '', NULL, NULL, 23420), -- 43752
(43319, 'deDE', 'Erdheiler Tiefvene', '', 'Sanitäter', NULL, 23420), -- 43319
(43169, 'deDE', 'Klei Lehmachse', '', 'Ingenieur', NULL, 23420), -- 43169
(43168, 'deDE', 'Kies Langfels', '', NULL, NULL, 23420), -- 43168
(44972, 'deDE', 'Rocky Klippkant', '', 'Waffenhändler', NULL, 23420), -- 44972
(43233, 'deDE', 'Geomeister von Steinruh', '', NULL, NULL, 23420), -- 43233
(43234, 'deDE', 'Erdberster der Steintroggs', '', NULL, NULL, 23420), -- 43234
(43228, 'deDE', 'Berserker der Steintroggs', '', NULL, NULL, 23420), -- 43228
(43250, 'deDE', 'Todesreiter der Nadelfelsen', '', NULL, NULL, 23420), -- 43250
(43229, 'deDE', 'Verletzter Irdener', '', NULL, NULL, 23420), -- 43229
(43174, 'deDE', 'Buddler der Steintroggs', '', NULL, NULL, 23420), -- 43174
(55216, 'deDE', 'Urahne Tiefenschmiede', '', NULL, NULL, 23420), -- 55216
(47197, 'deDE', 'Flint Erzmantel', '', 'Sohn des Steinvaters', NULL, 23420), -- 47197
(44970, 'deDE', 'Schiefer Tiefbohrer', '', 'Reagenzien & Giftzutaten', NULL, 23420), -- 44970
(44204, 'deDE', 'Steinvater Erzmantel', '', 'Anführer der Irdenen', NULL, 23420), -- 44204
(43871, 'deDE', 'Kriegskonstrukt', '', NULL, NULL, 23420), -- 43871
(43160, 'deDE', 'Erdbrecher Dolomit', '', NULL, NULL, 23420), -- 43160
(43071, 'deDE', 'Spalt Steinbrecher', '', NULL, NULL, 23420), -- 43071
(43134, 'deDE', 'Wegelagerer der Steintroggs', '', NULL, NULL, 23420), -- 43134
(44220, 'deDE', 'Jadewüter', '', NULL, NULL, 23420), -- 44220
(44855, 'deDE', 'Zwielichtkryptomant', '', NULL, NULL, 23420), -- 44855
(44218, 'deDE', 'Smaragdkoloss', '', NULL, NULL, 23420), -- 44218
(44143, 'deDE', 'Schiefer Treibsand', '', 'Erzgeomant', NULL, 23420), -- 44143
(44126, 'deDE', 'Kriegswächter', '', NULL, NULL, 23420), -- 44126
(43232, 'deDE', 'Irdener Champion', '', NULL, NULL, 23420), -- 43232
(43170, 'deDE', 'Irdener Geomant', '', NULL, NULL, 23420), -- 43170
(43138, 'deDE', 'Verteidiger von Steinruh', '', NULL, NULL, 23420), -- 43138
(47184, 'deDE', 'Fungumant der Steintroggs', '', NULL, NULL, 23420), -- 47184
(42521, 'deDE', 'Jaspisspitzenschwärmer', '', NULL, NULL, 23420), -- 42521
(42469, 'deDE', 'Kor der Unbewegliche', '', NULL, NULL, 23420), -- 42469
(43344, 'deDE', 'Berracit', '', NULL, NULL, 23420), -- 43344
(43339, 'deDE', 'Gorgonit', '', NULL, NULL, 23420), -- 43339
(43182, 'deDE', 'Erstarrte Steinfledermaus', '', NULL, 'LootAll', 23420), -- 43182
(43765, 'deDE', 'Deepholm Stalker Beam Target 17', '', NULL, NULL, 23420), -- 43765
(42466, 'deDE', 'Terrath der Beständige', '', NULL, NULL, 23420), -- 42466
(43763, 'deDE', 'Deepholm Stalker Beam Target 15', '', NULL, NULL, 23420), -- 43763
(44035, 'deDE', 'Fungusmonster', '', NULL, NULL, 23420), -- 44035
(44049, 'deDE', 'Riesenpilz', '', NULL, NULL, 23420), -- 44049
(43616, 'deDE', 'Erdwüter der Steintroggs', '', NULL, NULL, 23420), -- 43616
(43598, 'deDE', 'Bestienzähmer der Steintroggs', '', NULL, NULL, 23420), -- 43598
(45191, 'deDE', 'Felsplattform', '', NULL, 'vehichleCursor', 23420), -- 45191
(43181, 'deDE', 'Schieferhautbasilisk', '', NULL, NULL, 23420), -- 43181
(43545, 'deDE', 'Opalschimmernder Wächter', '', NULL, NULL, 23420), -- 43545
(42522, 'deDE', 'Steindrache', '', NULL, NULL, 23420), -- 42522
(43586, 'deDE', 'Opalsteinwerfer', '', NULL, NULL, 23420), -- 43586
(44947, 'deDE', 'Pyritsteinling', '', NULL, NULL, 23420), -- 44947
(48520, 'deDE', 'Steindrache der Kristallschwingen', '', NULL, 'vehichleCursor', 23420), -- 48520
(45408, 'deDE', 'D''lom der Sammler', '', 'Rüstmeister von Therazane', NULL, 23420), -- 45408
(45407, 'deDE', 'Ibdil der Heiler', '', 'Reparaturen', NULL, 23420), -- 45407
(45033, 'deDE', 'Ma''haat der Unbeugsame', '', NULL, NULL, 23420), -- 45033
(44945, 'deDE', 'Pyritsteinhüter', '', NULL, NULL, 23420), -- 44945
(44973, 'deDE', 'Ruberick', '', NULL, NULL, 23420), -- 44973
(43806, 'deDE', 'Terrath der Beständige', '', NULL, NULL, 23420), -- 43806
(43805, 'deDE', 'Felsen der Ausdauernde', '', NULL, NULL, 23420), -- 43805
(43804, 'deDE', 'Gorsik der Tosende', '', NULL, NULL, 23420), -- 43804
(42465, 'deDE', 'Therazane', '', 'Die Steinmutter', NULL, 23420), -- 42465
(43503, 'deDE', 'Erdheilerin Norsala', '', 'Der Irdene Ring', NULL, 23420), -- 43503
(43513, 'deDE', 'Säulenbrecher von Verlok', '', NULL, NULL, 23420), -- 43513
(43372, 'deDE', 'Fungumant Glop', '', NULL, NULL, 23420), -- 43372
(42543, 'deDE', 'Kristallrachenbasilisk', '', NULL, NULL, 23420), -- 42543
(43368, 'deDE', 'Pilzgärtner von Verlok', '', NULL, NULL, 23420), -- 43368
(43388, 'deDE', 'Verdammnispilz', '', NULL, NULL, 23420), -- 43388
(44425, 'deDE', 'Rotschieferspinne', '', NULL, NULL, 23420), -- 44425
(42475, 'deDE', 'Fungusungetüm', '', NULL, NULL, 23420), -- 42475
(43374, 'deDE', 'Pulsierende Geode', '', NULL, NULL, 23420), -- 43374
(43480, 'deDE', 'Temperamentvoller Rumpler', '', NULL, NULL, 23420), -- 43480
(42472, 'deDE', 'Gorsik der Tosende', '', NULL, NULL, 23420), -- 42472
(44967, 'deDE', 'Maziel', '', 'Quecksilberaszendent', NULL, 23420), -- 44967
(44875, 'deDE', 'Millhaus Manasturm', '', 'Der Schmiedemeister', NULL, 23420), -- 44875
(44879, 'deDE', 'Ogerleibwächter', '', NULL, NULL, 23420), -- 44879
(44936, 'deDE', 'Düstersteintrogg', '', NULL, NULL, 23420), -- 44936
(43780, 'deDE', 'Deepholm Stalker Beam Target 19', '', NULL, NULL, 23420), -- 43780
(43785, 'deDE', 'Deepholm Stalker Beam Target 21', '', NULL, NULL, 23420), -- 43785
(41957, 'deDE', 'Deepholm Stalker Beam Target', '', NULL, NULL, 23420), -- 41957
(62916, 'deDE', 'Fungusmotte', '', NULL, 'wildpetcapturable', 23420), -- 62916
(43158, 'deDE', 'Quecksilberschlamm', '', NULL, NULL, 23420), -- 43158
(44988, 'deDE', 'Großer Quecksilberschlamm', '', NULL, NULL, 23420), -- 44988
(49847, 'deDE', 'Fungusmotte', '', NULL, NULL, 23420), -- 49847
(53894, 'deDE', 'Kernstein der Geduld', '', NULL, NULL, 23420), -- 53894
(53739, 'deDE', 'Element der Geduld', '', NULL, NULL, 23420), -- 53739
(43370, 'deDE', 'Roter Nebel', '', NULL, NULL, 23420), -- 43370
(49822, 'deDE', 'Jadezahn', '', NULL, NULL, 23420), -- 49822
(49865, 'deDE', 'Deep Celestite Bunny', '', NULL, NULL, 23420), -- 49865
(47072, 'deDE', 'Amthea', '', NULL, NULL, 23420), -- 47072
(49816, 'deDE', 'Tiefenspinne', '', NULL, NULL, 23420), -- 49816
(49867, 'deDE', 'Deep Garnet Bunny', '', NULL, NULL, 23420), -- 49867
(49866, 'deDE', 'Deep Amethyst Bunny', '', NULL, NULL, 23420), -- 49866
(42711, 'deDE', 'Basiliskenreiter von Verlok', '', NULL, NULL, 23420), -- 42711
(43367, 'deDE', 'Madenklopfer von Verlok', '', NULL, NULL, 23420), -- 43367
(47071, 'deDE', 'Tiefenspinne', '', NULL, NULL, 23420), -- 47071
(43373, 'deDE', 'Tobender Kristallwandler', '', NULL, NULL, 23420), -- 43373
(49824, 'deDE', 'Deep Alabaster Bunny', '', NULL, NULL, 23420), -- 49824
(62927, 'deDE', 'Purpurrote Geode', '', NULL, 'wildpetcapturable', 23420), -- 62927
(49815, 'deDE', 'Tiefenspinne', '', NULL, NULL, 23420), -- 49815
(44258, 'deDE', 'Kolossaler Schleifwurm', '', NULL, NULL, 23420), -- 44258
(95893, 'deDE', 'Forinn Steinherz', '', 'Bergbaulehrling', NULL, 23420), -- 95893
(40789, 'deDE', 'Generic Controller Bunny (CSA)', '', NULL, NULL, 23420), -- 40789
(44403, 'deDE', 'Generic Controller Bunny, Gigantic (CSA)', '', NULL, NULL, 23420), -- 44403
(48543, 'deDE', 'Falling Rubble Bunny', '', NULL, NULL, 23420), -- 48543
(50060, 'deDE', 'Terborus', '', NULL, NULL, 23420), -- 50060
(42766, 'deDE', 'Gesättigter Schleifwurm', '', NULL, NULL, 23420), -- 42766
(44259, 'deDE', 'Gesättigter Schleifwurm', '', NULL, NULL, 23420), -- 44259
(44768, 'deDE', 'Versklavter Minenarbeiter', '', NULL, NULL, 23420), -- 44768
(62117, 'deDE', 'Zwielichtspinne', '', NULL, 'wildpetcapturable', 23420), -- 62117
(43218, 'deDE', 'Zwielichtblutformer', '', NULL, NULL, 23420), -- 43218
(45084, 'deDE', 'Elementaraufseher', '', NULL, NULL, 23420), -- 45084
(45988, 'deDE', 'Zwielichtblutschmied', '', NULL, NULL, 23420), -- 45988
(62915, 'deDE', 'Smaragdschieferjungtier', '', NULL, 'wildpetcapturable', 23420), -- 62915
(42921, 'deDE', 'Dragul Riesenschlächter', '', NULL, NULL, 23420), -- 42921
(42823, 'deDE', 'Zwielichtpriesterin', '', NULL, NULL, 23420), -- 42823
(42926, 'deDE', 'Rockling Chain Bunny', '', NULL, NULL, 23420), -- 42926
(42900, 'deDE', 'Quarzsteinling', '', NULL, NULL, 23420), -- 42900
(42468, 'deDE', 'Felsen der Ausdauernde', '', NULL, NULL, 23420), -- 42468
(43101, 'deDE', 'Sohn von Kor', '', NULL, NULL, 23420), -- 43101
(42525, 'deDE', 'Jaspisspitzenverheerer', '', NULL, NULL, 23420), -- 42525
(42523, 'deDE', 'Steinfledermaus', '', NULL, NULL, 23420), -- 42523
(50041, 'deDE', 'Myzrael', '', NULL, NULL, 23420), -- 50041
(43115, 'deDE', 'Schlafender steingebundener Elementar', '', NULL, 'Interact', 23420), -- 43115
(42524, 'deDE', 'Jaspisspitzenbohrer', '', NULL, NULL, 23420), -- 42524
(48692, 'deDE', 'Zwielichtspinne', '', NULL, NULL, 23420), -- 48692
(44889, 'deDE', 'Fire Ward Kill Credit', '', NULL, NULL, 23420), -- 44889
(44887, 'deDE', 'Gebundener Feuerelementar', '', NULL, NULL, 23420), -- 44887
(44890, 'deDE', 'Air Ward Kill Credit', '', NULL, NULL, 23420), -- 44890
(44885, 'deDE', 'Gebundener Luftelementar', '', NULL, NULL, 23420), -- 44885
(42924, 'deDE', 'Zwielichthilfsarbeiter', '', NULL, NULL, 23420), -- 42924
(42917, 'deDE', 'Zwielichtdämmerungswächter', '', NULL, NULL, 23420), -- 42917
(42925, 'deDE', 'Gefräßiger Tunnelgräber', '', NULL, NULL, 23420), -- 42925
(44189, 'deDE', 'Zwielichtpyroschlund', '', NULL, NULL, 23420), -- 44189
(44257, 'deDE', 'Schleifwurm', '', NULL, NULL, 23420), -- 44257
(44888, 'deDE', 'Water Ward Kill Credit', '', NULL, NULL, 23420), -- 44888
(44886, 'deDE', 'Gebundener Wasserelementar', '', NULL, NULL, 23420), -- 44886
(43358, 'deDE', 'Feldspat der Ewige', '', NULL, NULL, 23420), -- 43358
(49929, 'deDE', 'Rotschieferjungtier', '', NULL, NULL, 23420), -- 49929
(42899, 'deDE', 'Quarzsteinhüter', '', NULL, NULL, 23420), -- 42899
(42467, 'deDE', 'Diamant der Geduldige', '', NULL, NULL, 23420), -- 42467
(62922, 'deDE', 'Rotschieferjungtier', '', NULL, 'wildpetcapturable', 23420), -- 62922
(43356, 'deDE', 'Porezit der Stille', '', NULL, NULL, 23420), -- 43356
(62925, 'deDE', 'Kristallkäfer', '', NULL, 'wildpetcapturable', 23420), -- 62925
(44891, 'deDE', 'ELM General Purpose Bunny Gigantic (scale x4)', '', NULL, NULL, 23420), -- 44891
(44835, 'deDE', 'Haethen Kaul', '', NULL, NULL, 23420), -- 44835
(49858, 'deDE', 'Amethystschieferjungtier', '', NULL, NULL, 23420), -- 49858
(42606, 'deDE', 'Steinschneiderschinder', '', NULL, NULL, 23420), -- 42606
(42607, 'deDE', 'Steinschneiderreißer', '', NULL, NULL, 23420), -- 42607
(42574, 'deDE', 'Initiand Goldmine', '', NULL, NULL, 23420), -- 42574
(62924, 'deDE', 'Tiefenheimkakerlake', '', NULL, 'wildpetcapturable', 23420), -- 62924
(43258, 'deDE', 'Leitsteinelementar', '', NULL, NULL, 23420), -- 43258
(43254, 'deDE', 'Aufgeladene Geode', '', NULL, NULL, 23420), -- 43254
(49770, 'deDE', 'Tiefenheimkakerlake', '', NULL, NULL, 23420), -- 49770
(43123, 'deDE', 'Lebendes Blut', '', NULL, NULL, 23420), -- 43123
(66805, 'deDE', 'Rubin', '', NULL, NULL, 23420), -- 66805
(66804, 'deDE', 'Kristallus', '', NULL, NULL, 23420), -- 66804
(66802, 'deDE', 'Fraktur', '', NULL, NULL, 23420), -- 66802
(49771, 'deDE', 'Kristallkäfer', '', NULL, NULL, 23420), -- 49771
(66815, 'deDE', 'Bordin Ruhigfaust', '', 'Meistertierzähmer', NULL, 23420), -- 66815
(45294, 'deDE', 'Dugsley Tiefgräber', '', 'Ingenieursbedarf', NULL, 23420), -- 45294
(45289, 'deDE', 'Magdala Kupferstich', '', 'Gemischtwaren', NULL, 23420), -- 45289
(44802, 'deDE', 'Ausgrabungsleiterin Brauer', '', 'Forscherliga', NULL, 23420), -- 44802
(44799, 'deDE', 'Entdeckerin Mowi', '', 'Forscherliga', NULL, 23420), -- 44799
(45305, 'deDE', 'Lastenkodo der Archäologischen Akademie', '', NULL, NULL, 23420), -- 45305
(45297, 'deDE', 'Tierführer Rostklemme', '', 'Stallmeister', NULL, 23420), -- 45297
(45293, 'deDE', 'Rixi "Die Bohrmaschine" Bombenbuddler', '', 'Ingenieursbedarf', NULL, 23420), -- 45293
(45290, 'deDE', 'Varx Vielfeilscher', '', 'Gemischtwaren', NULL, 23420), -- 45290
(44823, 'deDE', 'Prüfer Rowe', '', 'Die Archäologische Akademie', NULL, 23420), -- 44823
(44818, 'deDE', 'Reliquarin Jes''ca Finstersonne', '', 'Die Archäologische Akademie', NULL, 23420), -- 44818
(45039, 'deDE', 'Tharm Wildfeuer', '', 'Der Irdene Ring', NULL, 23420), -- 45039
(45037, 'deDE', 'Tawn Winterfels', '', 'Der Irdene Ring', NULL, 23420);