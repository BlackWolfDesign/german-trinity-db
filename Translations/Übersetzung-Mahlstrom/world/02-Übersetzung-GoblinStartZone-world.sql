-- TrinityCore - WowPacketParser
-- File name: goblin_starting_zone_ part1_LvL1-5.pkt
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/24/2017 03:05:06


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=14474 AND `locale`='deDE') OR (`ID`=14239 AND `locale`='deDE') OR (`ID`=14126 AND `locale`='deDE') OR (`ID`=14125 AND `locale`='deDE') OR (`ID`=14124 AND `locale`='deDE') OR (`ID`=14121 AND `locale`='deDE') OR (`ID`=14123 AND `locale`='deDE') OR (`ID`=14122 AND `locale`='deDE') OR (`ID`=14120 AND `locale`='deDE') OR (`ID`=14116 AND `locale`='deDE') OR (`ID`=14115 AND `locale`='deDE') OR (`ID`=14153 AND `locale`='deDE') OR (`ID`=14110 AND `locale`='deDE') OR (`ID`=26711 AND `locale`='deDE') OR (`ID`=14070 AND `locale`='deDE') OR (`ID`=24520 AND `locale`='deDE') OR (`ID`=24503 AND `locale`='deDE') OR (`ID`=24502 AND `locale`='deDE') OR (`ID`=24488 AND `locale`='deDE') OR (`ID`=24567 AND `locale`='deDE') OR (`ID`=31813 AND `locale`='deDE') OR (`ID`=14071 AND `locale`='deDE') OR (`ID`=28349 AND `locale`='deDE') OR (`ID`=25473 AND `locale`='deDE') OR (`ID`=14069 AND `locale`='deDE') OR (`ID`=14075 AND `locale`='deDE') OR (`ID`=14138 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(14474, 'deDE', 'Rettungskapseln der Goblins', 'Rettet 6 überlebende Goblins.', '$n, es tut mir leid. Ich dachte, Ihr wärt tot!$B$BEs ist einfach Irrsinn! Wir sind direkt in einen Kampf zwischen der Allianz und der Horde gesegelt.$B$BWir müssen unsere Brüder und Schwestern retten.$B$B<Er zeigt auf die Rettungskapseln, die um Euch herum im Wasser verstreut sind.>$B$BIch glaube, ich bleibe hier noch ein wenig sitzen und komme zu Atem. Könntet Ihr aufs Meer schwimmen und sie herauslassen? Nehmt meine thermohydraulischen Flossen, damit könnt Ihr Euch schneller fortbewegen.$B$BLasst Sassy wissen, dass ich gleich losschwimme.', '', '', '', '', '', '', 23420),
(14239, 'deDE', 'Geht nicht ins Licht!', '', '', '', '', '', '', '', '', 23420),
(14126, 'deDE', 'Lebensersparnisse', 'Übergebt Handelsprinz Gallywix auf Gallywix'' Yacht auf Kezan Eure Lebensersparnisse.', 'Wir haben die ganze Knete zusammen. Jetzt müssen wir nur noch eines tun, Boss: Euch zur Yacht des Handelsprinzen schaffen. Sie legt bald ab!$B$BWir müssen uns beeilen. Springt rein! Ich fahre!', '', '', '', '', '', '', 23420),
(14125, 'deDE', 'Versicherungsbetrug', 'Überlastet den defekten Generator, macht den undichten Ofen an und lasst eine Zigarre auf das leicht entzündliche Bett fallen. Benutzt dann den Benzinbot, um den Hauptgeschäftssitz der HGK in Brand zu setzen!', 'Wir kommen der Sache näher, aber wir haben immer noch nicht genug Moneten. Zu Eurem Glück, Boss, habe ich das vorausgesehen und einen Plan entwickelt. Wir werden die Versicherung des Hauptquartiers einstreichen!$B$BIch habe mir die Freiheit genommen, das übriggebliebene Feuerwerk von Eurer Party an diesem Ort zu verteilen. Alles, was Ihr tun müsst, ist dorthin zu gehen und ein wenig Sabotage zu betreiben.$B$BDarf ich vielleicht den defekten Generator, den undichten Ofen und das leicht entzündliche Bett vorschlagen? Danach geht Ihr wieder nach draußen und schickt einen Benzinbot hinein.$B$BEin narrensicherer Plan!', '', '', '', '', '', '', 23420),
(14124, 'deDE', 'Schnappt Euch das Kaja''mit', 'Sammelt 12 Kaja''mitbrocken.', '$GMein Herr:Meine Dame;, wir brauchen einen Haufen Knete, um den Handelsprinzen zu bezahlen und von Kezan weg zu kommen! Und dieser Haufen liegt genau da unten und wartet darauf, mitgenommen zu werden.$B$BDies sind die letzten bekannten Kaja''mitvorkommen. Das ist der Rohstoff, der uns zu den genialen Tüftlern und Alchemisten gemacht hat, die wir heute sind. Wenn das keine Bazillion Moneten wert ist, dann weiß ich auch nicht!$B$BNehmt ein paar meiner Kabummbomben und jagt die Vorkommen in die Luft. Sammelt danach die Kaja''mitbrocken auf.$B$BAber gebt auf die rebellischen Trolle Acht!', '', '', '', '', '', '', 23420),
(14121, 'deDE', 'Plündert die Plünderer', 'Benutzt den Hot Rod, um 12 angeheuerte Plünderer umzufahren, und sammelt dabei 12 Einheiten gestohlene Beute.', 'Wir werden alle sterben!!!$B$B<Ihr gebt Megs eine Ohrfeige, damit sie sich beruhigt.>$B$BTut mir leid, $GHerr:Frau; $n! Es wird nicht mehr vorkommen. Wir müssen einen Weg finden, ganz schnell ganz viel Knete zu scheffeln.$B$BEs gibt bereits Berichte, dass der Handelsprinz angeheuerte Plünderer auf die Straßen des Bilgepiers geschickt hat.$B$BNehmt den Hot Rod, fahrt sie über den Haufen und nehmt Euch die Beute, die sie sich unrechtmäßig angeeignet haben. Daran wäre doch nichts Falsches, oder?$B$BSchließlich ist es für eine gute Sache!', '', '', '', '', '', '', 23420),
(14123, 'deDE', 'Einfach hereinspaziert', 'Beschafft Euch Maltes Falken, die Mona Goblisa und die ultimative Bombe.', 'Ich habe von unseren Geldproblemen gehört. Es gibt nur einen Weg, wie wir eine Bazillion Moneten zusammenkriegen können: Wir stehlen es von den Reichen!$B$BDieser Schleimer, der Handelsprinz, hat alle möglichen Antiquitäten und wertvolle Kunstgegenstände gestohlen. Es ist an der Zeit, es ihm zu nehmen und den Bedürftigen zu geben... natürlich vor allem uns!$B$BHobart hat eine Kappe zurückgelassen, an der er gearbeitet hat – eine Verkleidung, mit der Ihr genauso ausseht, wie einer von Gallywix'' Schergen. Ihr müsst einfach nur in seine Villa im Westen spazieren und diese Gegenstände "sicherstellen".$B$BPasst aber auf seine Schweine auf. Sie werden Euren Geruch hinter der Verkleidung erkennen!', '', '', '', '', '', '', 23420),
(14122, 'deDE', 'Der große Banküberfall', 'Knackt den Tresor der Ersten Bank von Kezan, um an Eure privaten Reichtümer zu gelangen.', 'All Eure Besitztümer wurden eingefroren und die Erste Bank von Kezan hat geschlossen!$B$BIhr braucht wirklich einen Haufen Knete, Boss. Ich fürchte, Ihr müsst Euer eigenes Geld aus der Bank rauben! Mit den verschiedenen Apparaten in Eurem goblinischen Alles-in-1-zigartigen Gürtel solltet Ihr alles dabei haben, was Ihr braucht, um den Tresor zu knacken.$B$BIch hoffe nur, dass Ihr etwas für schlechte Zeiten gespart habt, wie ich es Euch immer geraten habe. Und falls Ihr es noch nicht gemerkt haben solltet: Die Zeiten sind gerade hundsmiserabel!', '', '', '', '', '', '', 23420),
(14120, 'deDE', 'Eine Bazillion Moneten?!', 'Sprecht mit Sassy Hartzang im Hauptgeschäftssitz der HGK auf Kezan.', 'Ihr seid ein sehr ehrgeiziger junger Goblin, $n. Vielleicht ein wenig ehrgeiziger, als es für Euch gut wäre. Ich erkenne mich selbst in Euch, als ich noch jung war.$B$BIch habe einen Vorschlag. Vielleicht ist Euch ja schon aufgefallen, dass der Kajaro explodiert. Alle auf Kezan werden sterben!$B$BAber, wenn Ihr mir eine Bazillion Moneten bringt, bevor meine Yacht die Insel verlässt, werde ich dafür sorgen, dass Ihr unter den wenigen Glücklichen seid, die entkommen.$B$BWenn Ihr also leben wollt, solltet Ihr Euch lieber schnell überlegen, wo Ihr so viel Knete für mich auftreibt!', '', '', '', '', '', '', 23420),
(14116, 'deDE', 'Der ungeladene Gast', 'Sprecht mit Handelsprinz Gallywix oben im Hauptgebäude des Hauptgeschäftssitzes der HGK auf Kezan.', 'Während Ihr Euch um die Piraten gekümmert habt, habe ich bemerkt, wie der Handelsprinz sich nach oben geschlichen hat. Er ist auf dem Balkon oben an der Treppe.$B$B$n, ich glaube, er hat die Piraten geschickt.$B$B<Sassy erschaudert.>$B$BIhr solltet lieber so schnell wie möglich nach oben gehen. Lasst ihn nicht warten!', '', '', '', '', '', '', 23420),
(14115, 'deDE', 'Ungeladene Piraten', 'Tötet 12 ungeladene Piraten, um Eure Karriere zu retten.', 'Oh je! Piraten!$B$B$n, da sind ungeladene Südmeerpiraten auf Eurer Party!$B$BIhr müsst sofort etwas dagegen unternehmen oder Eure Karriere und jede Hoffnung, $gder nächste Handelsprinz:die nächste Handelsprinzessin; zu werden, sind dahin.', '', '', '', '', '', '', 23420),
(14153, 'deDE', 'Der Mittelpunkt der Party', 'Zieht Eure großartige Partyausstattung an (Coole Sonnenbrille, Hippes neues Outfit und Strahlende Klunker). Begebt Euch danach auf die Party am Pool des Hauptgeschäftssitzes der HGK und unterhaltet 10 kezanische Feiernde.', 'Oh, wow, Baby! Alle sind gekommen! Wen interessiert da schon so ein blöder Drache?$B$BWir veranstalten die Party am Pool. Klingt so, als würden sich alle schon köstlich amüsieren. Du wirst so toll aussehen!$B$BWarum gehst Du nicht schon einmal rüber zum Pool und unterhältst die Leute? Ich bin in einer Minute bei Dir... Ich muss nur noch meine Sachen holen.', '', '', '', '', '', '', 23420),
(14110, 'deDE', 'Ein ganz neues Ihr', 'Kauft strahlende Klunker von Gappy Silberzahn, ein hippes neues Outfit von Szabo und eine coole Sonnenbrille von Missa Spekkies in der Gaunergasse.', 'Ein neues Outfit? Was Ihr nicht sagt! Eure Party ist in aller Munde. Alles, was auf der Insel Rang und Namen hat, wird da sein.$B$BNun, dann wünsche ich Euch viel Spaß beim Einkaufen. In der Gaunergasse könnt Ihr die besten Schnäppchen ergattern!', '', '', '', '', '', '', 23420),
(26711, 'deDE', 'Ab zur Bank', 'Sprecht mit dem Kassierer der EBvK in der Ersten Bank von Kezan auf Kezan.', 'Jo, Schätzchen. Was geht? Schon bereit für die Party?$B$BNein?! Du weißt doch, dass ich Dich heiß und innig liebe, also weiß ich nicht ganz, wie ich es sagen soll, aber… Ich glaube, Du solltest mal in der Stadt nach einem neuen Outfit für die Party suchen.$B$BGuck mich nicht so an... Du willst doch klasse für mich aussehen, ooooder? Willst Du für mich nicht superschick sein?$B$BSchau erst bei der Bank vorbei und hol Dir ausreichend Moneten. Du willst ja schließlich keinen Billigkram kaufen!', '', '', '', '', '', '', 23420),
(14070, 'deDE', 'Selbst ist der Goblin', 'Verprügelt Bruno Flammhemmer, Frankie Gangschalter, Jack den Hammer und Sudsy Magee.', 'Wir müssen gleich mehreren Schuldnern eine Nachricht zukommen lassen. Natürlich versteckt sich dieser Abschaum im Malocherviertel. Ich weiß, wie ungern Ihr in die Slums geht, $GHerr:Frau; Boss, aber was soll man schon machen?$B$B<Sassy blättert durch einen Stapel Papiere.>$B$BIch habe alle Namen hier.$B$BDarf ich vielleicht vorschlagen, dass Ihr ihnen einen "persönlichen Besuch" abstattet? Ein paar strategisch geplante Schlägereien setzen deutliche Zeichen für die anderen Schuldner UND Eure Konkurrenten.$B$B$n ist kein Goblin, mit dem man sich anlegen sollte!', '', '', '', '', '', '', 23420),
(24520, 'deDE', 'Neuigkeiten für Sassy', 'Sprecht mit Sassy Hartzang im Hauptgeschäftssitz der HGK auf Kezan.', 'Ich würde ja mit Euch feiern, aber ich fürchte, dass der Drache dem guten alten Kajaro ganz schön zugesetzt hat. Ich… äh… Ich glaube, ich packe meine Sachen und nehme das erste Schiff, das die Insel verlässt.$B$BIhr solltet jedenfalls zum Hauptgeschäftssitz zurückkehren und Sassy erzählen, dass Ihr den Fußbombentitel für uns gewonnen habt. Ich bin mir sicher, dass sie auch über den Drachen und den Kajaro Bescheid wissen möchte.$B$BViel Glück!', '', '', '', '', '', '', 23420),
(24503, 'deDE', 'Drölfmeterschießen', 'Schießt das Siegertor, indem Ihr die Fußbombe zwischen die beiden Schornsteine hinter dem gegnerischen Tor schießt.', 'Jetzt liegt es an Euch, $GJunge:Mädel;! Ihr habt die Dampfdruckhaie vernichtet. Jetzt müsst Ihr nur noch das Siegtor schießen.$B$BJetzt mal unter uns. Ich finde, wir sollten dieses Mal alles richtig machen. Ich möchte nicht, dass Ihr einfach nur ein Tor schießt... Ich möchte, dass Ihr so weit schießt, dass die Bombe bis zwischen die beiden Schornsteine hinter dem Tor fliegt!$B$BIch habe die Fußbombe so präpariert, dass sie dafür die richtige Zusatzleistung hat.$B$BUnd jetzt raus da und macht mich stolz!', '', '', '', '', '', '', 23420),
(24502, 'deDE', 'Notwendige Gewaltanwendung', 'Benutzt den Bilgewasserbukanier und werft die Fußbombe auf 8 Dampfdruckhaie.', 'Wir haben nur noch einen Schredder. Als gerade niemand hingesehen hat, habe ich ihn mit, nun ja, sagen wir, modifizierten Fußbomben geladen.$B$BIhr müsst diesen Schredder nehmen und ihnen das Leben zur Hölle machen!', '', '', '', '', '', '', 23420),
(24488, 'deDE', 'Der Ersatz', 'Sammelt 6 Ersatzteile.', 'Meine Jungs sitzen alle verletzt auf der Bank! Unsere Schredder sind nur noch Schrott! Der Schiri pfeift das Spiel gleich ab!$B$BDie letzte Minute läuft und ich brauche Euch, $n!!!$B$BWir müssen einen dieser Schredder auf Vordermann bringen. Im Bilgepier liegen genügend Ersatzteile herum. Jeder grünblütige Kezanier überlässt sie Euch sicher gerne, wenn die Bukaniere dann eine Chance auf den Sieg haben.$B$BWir werden diesen nichtsnutzigen Dampfdruckhaien schon zeigen, wie man Fußbombe spielt.', '', '', '', '', '', '', 23420),
(24567, 'deDE', 'Meldet Euch für ein Testspiel', 'Sprecht mit Trainer Blutgrätsch auf dem Kajarofeld auf Kezan.', '$n, Trainer Blutgrätsch, der Kopf unserer furchtlosen Bilgewasserbukaniere, sucht Euch.$B$BEr sagt, dass der Ligatitel gegen die Dampfdruckhaie in Gefahr ist und dass nur Ihr den Pokal nach Hause holen könnt!$B$BGeht nach Westen in die Stadt. Ihr könnt das Kajarofeld nicht verfehlen.$B$BEin Fußbombenstar. Das wird sich hervorragend auf Euer Image auswirken!', '', '', '', '', '', '', 23420),
(31813, 'deDE', 'Dagra die Grimmige', 'Besiegt Dagra die Grimmige in einem Haustierkampf.', 'Da unten gibt''s noch ''ne Trainerin, der Ihr einen Kampf liefern solltet, um Euer Können auf die Probe zu stellen.$b$bDagra die Grimmige erwartet Euch am Nordhang des Dornenhügels östlich des Wegekreuzes. Wenn Ihr sie schlagt, habt Ihr womöglich drauf, was es braucht.', '', '', '', '', '', '', 23420),
(14071, 'deDE', 'Cruisen', 'Fahrt mit dem Hot Rod durch die Gegend und Holt Eure Freunde, Ass, Gobber und Izzy ab.', '$n, ich habe gehört, dass Ihr die Mutter aller Firmenpartys ausrichten werdet. Laut meinen Berechnungen sollte Euer Beförderungsquotient raketenhaft in die Höhe schießen und explodieren, wenn man sieht, dass Ihr auf der Party Spaß habt!$B$BDaher schlage ich vor, Ihr unternehmt eine kleine Spritztour mit dem Hot Rod und holt Eure Freunde in der Stadt ab.$B$BVergesst mich nur nicht, wenn alles am Schnürchen läuft und Ihr $gder neue Handelsprinz:die neue Handelsprinzessin; werdet, ja?', '', '', '', '', '', '', 23420),
(28349, 'deDE', 'Megs vom Marketing', 'Sprecht mit Megs Schredderschreck beim HGK-Hauptquartier in Kezan.', 'Boss, während Ihr unten in den Minen wart, hat Megs Schredderschreck aus dem Marketing nach Euch gefragt. Es ging um ein neues Gefährt für Euch.$B$BSie hält sich außerhalb des Gebäudes auf der linken Seite auf.', '', '', '', '', '', '', 23420),
(25473, 'deDE', 'Kaja''Cola', 'Bringt das Sechserpack Kaja''Cola zu Sassy Hartzang im Hauptquartier der HGK in Kezan.', 'Sassy Hartzang hat mich damit gelöchert, ihr etwas Kaja''Cola für die Party zu Euren Ehren heute Abend zu besorgen.$B$BUps, war das eine Überraschung? Von mir habt Ihr es nicht!$B$BNaja, aber mit dem Produktionstief der letzten paar Tage ist das hier alles, was ich herstellen konnte. Könntet Ihr es ihr bringen?', '', '', '', '', '', '', 23420),
(14069, 'deDE', 'Gutes Personal ist schwer zu finden', 'Verbessert die Arbeitsmoral von 8 widerspenstigen Trollen.', 'Ich habe versagt! Ich habe alles versucht, um unser Kaja''Cola-Soll zu erreichen, aber Ihr wisst doch, wie das mit den Trollsklaven heutzutage ist. Gutes Personal ist schwer zu finden!$B$BIch glaube, Ihr müsst hier selbst Hand anlegen. Ein kleiner Stromschlag mit der Batterie Eures goblinischen Alles-in-1-zigartigen Gürtels sollte Wunder wirken. Wenn Ihr die Arbeitsmoral von ein paar Trollen da unten anpasst, ziehen die anderen sicher schnell nach.$B$BDie Kaja''Cola-Produktion muss weitergehen, sonst macht uns der Handelsprinz einen Kopf kürzer!', '', '', '', '', '', '', 23420),
(14075, 'deDE', 'Ärger in den Minen', 'Tötet 6 Tunnelwürmer.', 'Wir haben schon wieder Ärger in der Kaja''mine, Boss. Die Tunnelwürmer sind zurück und fressen wortwörtlich den gesamten Profit der Handelsgesellschaft von Kajaro auf!$B$BWir können kein Kaja''mit abbauen, was die Produktion von Kaja''Cola schwer beeinträchtigt!$B$BIch habe schon versucht, den Kammerjäger zu rufen, aber er hat sich mit unserer letzten Zahlung aus dem Staub gemacht.$B$BMeint Ihr, Ihr könntet selbst in die drei Minen gehen und Euch darum kümmern? Ich würde Euch ja helfen, aber ich habe noch nicht am vom Kartell vorgeschriebenen Nahkampftraining teilgenommen.', '', '', '', '', '', '', 23420),
(14138, 'deDE', 'Dienst ist Dienst und Cola ist Cola', 'Bringt Sassys Motivation zu Vorarbeiter Feuchtlunte im Hauptgeschäftssitz der HGK auf Kezan.', 'Wir haben so viele Probleme unten in der Kaja''mine, dass Vorarbeiter Feuchtlunte bald schon Purzelbäume schlägt, so verzweifelt wie er nach Euch sucht. Er murmelte etwas von ''widerspenstigen Trollen''.$B$BDas Kaja''mit, das unsere Trolle abbauen, ist ein wesentlicher Bestandteil unserer Kaja''Cola. Dieses blubberige Zeug bringt uns ein Vermögen und Euch sicher die Beförderung $gzum Handelsprinzen:zur Handelsprinzessin; ein!$B$BAber zunächst muss die Produktion wieder ins Rollen kommen. Ihr findet den Vorarbeiter im Osten. Hier, gebt ihm das als Motivation, damit er sich wieder zusammenreißt.', '', '', '', '', '', '', 23420);


DELETE FROM `quest_objectives_locale` WHERE (`ID`=267213) OR (`ID`=266734) OR (`ID`=266733) OR (`ID`=266732) OR (`ID`=266731) OR (`ID`=266678) OR (`ID`=255509) OR (`ID`=265430) OR (`ID`=265429) OR (`ID`=265428) OR (`ID`=265427) OR (`ID`=252165) OR (`ID`=266619) OR (`ID`=266618) OR (`ID`=269049) OR (`ID`=265518) OR (`ID`=265517) OR (`ID`=265516) OR (`ID`=265515) OR (`ID`=265334);
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(267213, 'deDE', 14474, 0, 'Überlebende Goblins gerettet', 23420),
(266734, 'deDE', 14125, 3, 'Hauptgeschäftssitz der HGK mit Benzinbot in Brand gesetzt!', 23420),
(266733, 'deDE', 14125, 2, 'Zigarre auf das leicht entzündliche Bett fallenlassen', 23420),
(266732, 'deDE', 14125, 1, 'Undichten Ofen angemacht', 23420),
(266731, 'deDE', 14125, 0, 'Defekten Generator überlastet', 23420),
(266678, 'deDE', 14122, 0, 'Tresor der Ersten Bank von Kezan', 23420),
(255509, 'deDE', 14153, 0, 'Feiernde unterhalten', 23420),
(265430, 'deDE', 14070, 3, 'Sudsy Magee verprügelt', 23420),
(265429, 'deDE', 14070, 2, 'Präz den Lufthammer verprügelt', 23420),
(265428, 'deDE', 14070, 1, 'Frankie Gangschalter verprügelt', 23420),
(265427, 'deDE', 14070, 0, 'Bruno Flammhemmer verprügelt', 23420),
(252165, 'deDE', 24503, 0, 'Fußbombe zwischen die beiden Schornsteine geschossen', 23420),
(266619, 'deDE', 24502, 1, 'Dampfdruckhaie gefußbombt', 23420),
(266618, 'deDE', 24502, 0, 'Bilgewasserbukanier', 23420),
(269049, 'deDE', 31813, 0, 'Besiegt Dagra die Grimmige', 23420),
(265518, 'deDE', 14071, 3, 'Gobber abgeholt', 23420),
(265517, 'deDE', 14071, 2, 'Ass abgeholt', 23420),
(265516, 'deDE', 14071, 1, 'Izzy abgeholt', 23420),
(265515, 'deDE', 14071, 0, 'Schlüssel für den Hot Rod benutzt', 23420),
(265334, 'deDE', 14069, 0, 'Arbeitsmoral verbessert', 23420);


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text     xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_offer_reward` WHERE `ID` IN (14239 /*14239*/, 14126 /*14126*/, 14125 /*14125*/, 14121 /*14121*/, 14122 /*14122*/, 14123 /*14123*/, 14124 /*14124*/, 14120 /*14120*/, 14116 /*14116*/, 14115 /*14115*/, 14153 /*14153*/, 14110 /*14110*/, 14070 /*14070*/, 26711 /*26711*/, 24520 /*24520*/, 24503 /*24503*/, 24502 /*24502*/, 24488 /*24488*/, 24567 /*24567*/, 14071 /*14071*/, 28349 /*28349*/, 25473 /*25473*/, 14069 /*14069*/, 14075 /*14075*/, 14138 /*14138*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(14239, 4, 0, 0, 0, 0, 0, 0, 0, 'Es liegt jetzt an Euch. Kehrt Ihr zurück ins Leben und seid $gder Held, den:die Heldin, die; die restlichen Überlebenden so dringend brauchen, oder bleibt Ihr bis in alle Ewigkeit hier?$B$BNur Ihr könnt das entscheiden.$B$BGeht nicht ins Licht, $n!', 23420), -- 14239
(14126, 1, 5, 11, 0, 0, 0, 0, 0, 'Glückwunsch, Ihr habt es gerade noch geschafft. Wie ich sehe, habt Ihr mir alles gebracht, worum ich Euch gebeten habe, sogar den Hot Rod.$B$B<Ein bösartiges Grinsen huscht über das Gesicht des Handelsprinzen.>$B$BEine Bazillion Moneten war sicher nicht leicht zu beschaffen. Jetzt bin ich der reichste und mächtigste Goblin in ganz Azeroth.$B$BZU DUMM, DASS ICH DIE BEDINGUNGEN UNSERER VEREINBARUNG JETZT ÄNDERE!$B$BIhr seid ab sofort mein Sklave. Geht unter Deck und leistet Euren Beitrag beim Kohleschaufeln. Nächster Halt: Azshara!$B$BMuhahahahahahaha!', 23420), -- 14126
(14125, 5, 25, 0, 0, 0, 0, 0, 0, 'Das ist noch gar nichts. Ihr solltet den Bilgepier mal sehen… Dort steht alles in Flammen!$B$BGut, gut, ich habe keine Zeit, den Ort unter die Lupe zu nehmen. Hier ist die Knete aus der Versicherung des Bilgewasserkartells.$B$BIch muss einen Weg von der Insel herunter finden. Viel Glück, $n!', 23420), -- 14125
(14121, 6, 0, 0, 0, 0, 0, 0, 0, 'Ihr habt es wirklich geschafft?! Na, ich schätze, das haben sie verdient.$B$BDas ist aber ganz schön viel Beute. Meint Ihr, es reicht, um Euch einen Platz auf der Yacht des Handelsprinzen zu erkaufen?$B$BKönnt Ihr jemanden mitnehmen? Ich habe im Marketing gerade nicht allzu viel zu tun.', 23420), -- 14121
(14122, 4, 0, 0, 0, 0, 0, 0, 0, 'He, Ihr habt ja gespart! Ich bin so stolz auf Euch!$B$B<Sassy zählt all Eure Moneten.>$B$BÄh, Boss… Das reicht nicht einmal ansatzweise.', 23420), -- 14122
(14123, 5, 0, 0, 0, 0, 0, 0, 0, 'Oh, toll! Dieser Kram ist ein Vermögen wert!$B$B<Slinky prüft alles mit geschulten, gierigen Augen.>$B$BIch hoffe nur, dass der Handelsprinz die Sachen nicht bemerkt, wenn Ihr sie ihm mit dem Rest Eures Reichtums übergebt…', 23420), -- 14123
(14124, 21, 0, 0, 0, 0, 0, 0, 0, 'Gute Arbeit, $Gmein Herr:meine Dame;, wirklich gute Arbeit! Da habt Ihr ein echtes Vermögen in Euren Taschen.$B$BDamit lässt uns der Handelsprinz sicher auf seine Yacht!', 23420), -- 14124
(14120, 5, 0, 0, 0, 0, 0, 0, 0, 'Eine Bazillion Moneten?!? Wo sollen wir denn so viel Knete her bekommen?$B$B<Sassy überlegt einen Augenblick.>$B$BÄh… hat der Handelsprinz erwähnt, ob ich mit Euch zusammen auf seiner Yacht fliehen darf?', 23420), -- 14120
(14116, 1, 0, 0, 0, 0, 0, 0, 0, 'So, so, so, wenn das nicht mein abtrünniger Schützling $GHerr:Frau; $n ist.$B$BIch bin tief verletzt und enttäuscht. Ihr habt mich nicht zu Eurer kleinen Party da draußen eingeladen. Tut mir leid wegen der ungebetenen Gäste… Ihr wisst ja, wie Piraten sein können.$B$BAlso, kommen wir zum Geschäft?', 23420), -- 14116
(14115, 1, 0, 0, 0, 0, 0, 0, 0, 'Puh, das war knapp, Boss. Und leider habe ich noch mehr schlechte Nachrichten.$B$BBitte, erschießt mich nicht…', 23420), -- 14115
(14153, 4, 6, 0, 0, 0, 0, 0, 0, 'Sehr gut, $gmein Herr:meine Dame;. Die Party war anscheinend ein voller Erfolg!$B$BÄh… was ist das denn?', 23420), -- 14153
(14110, 4, 0, 0, 0, 0, 0, 0, 0, 'Oh, du wirst so scharf aussehen! Bei deinem Tempo wirst du den Handelsprinzen im Handumdrehen vom Thron stoßen!$B$BIch kann es kaum abwarten, endlich ein gemachter Mann zu sein!$B$BLass mich dir die ganzen Sachen abnehmen. Kurz bevor die Party losgeht werde ich dir beim Styling helfen.', 23420), -- 14110
(14070, 4, 0, 0, 0, 0, 0, 0, 0, 'Das nenne ich einen absoluten Erfolg, $Gmein Herr:meine Dame;! Eure Schuldner werden es sich jetzt sicher zweimal überlegen, bevor sie Euch nicht bezahlen!$B$B<Sassy kaut einen Augenblick gedankenverloren auf ihrem Stift herum.>$B$BVielleicht sollten wir die Verleihzinsen erhöhen?', 23420), -- 14070
(26711, 25, 5, 0, 0, 0, 0, 0, 0, 'Oh, Ihr seid es, Madame! Tut mir leid, bei dem ganzen Pöbel, der hier herumschwirrt, habe ich Euch nicht gleich erkannt.$B$BIhr rumlaufenden Leute! Macht mal langsam hier! $n ist da!$B$BHier sind Eure Moneten, $gmein Herr:meine Dame;. Kommt jederzeit wieder, wenn Ihr mehr braucht.', 23420), -- 26711
(24520, 5, 273, 0, 0, 0, 1000, 0, 0, 'Ihr macht Witze, oder?! TODESSCHWINGE?!!!$B$B<Sassy sieht einen Moment lang panisch aus.>$B$BOkay, das wird schon. Wir veranstalten Eure Party, und bis wir damit fertig sind, ist der Drache sicher schon lange weg und der Boden hat zu zittern aufgehört.$B$BFalls nicht, können wir immer noch ein Schiff chartern und von hier verschwinden.', 23420), -- 24520
(24503, 5, 0, 0, 0, 1000, 0, 0, 0, 'Äh… Ihr habt es geschafft, $GJunge:Mädel;. Ihr habt es ECHT geschafft! Wir haben das Spiel gewonnen und… $B$BHABT IHR DIESEN DRACHEN GESEHEN?!!!', 23420), -- 24503
(24502, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr habt es geschafft, $GJunge:Mädel;! Jetzt müssen wir nur noch ein Tor machen und der Titel ist unser!', 23420), -- 24502
(24488, 71, 0, 0, 0, 0, 0, 0, 0, 'Das ist mein $GJunge:Mädel;! Jetzt muss ich diesen wertlosen Metallschrott nur noch in einen funktionierenden Zustand bringen und Euch raus aufs Spielfeld schicken!', 23420), -- 24488
(24567, 5, 0, 0, 0, 0, 0, 0, 0, 'Da seid Ihr ja, mein $gJunge:Mädchen;! Schnell, wir haben keine Zeit zu verlieren! Wir müssen diese Schredder wieder funktionstüchtig machen.$B$BIch hole Euch von der Ersatzbank… Ihr werdet das Spiel und den Titel für uns gewinnen!', 23420), -- 24567
(14071, 4, 0, 0, 0, 0, 0, 0, 0, 'Gut gemacht, $gmein Herr:meine Dame;. Der Hot Rod steht Euch wirklich ausgezeichnet. Ihr habt die Bevölkerung sicher schwer beeindruckt.', 23420), -- 14071
(28349, 3, 0, 0, 0, 0, 0, 0, 0, 'Da ist $ger:sie; ja! Ich glaube, das hier wird Euch gefallen!', 23420), -- 28349
(25473, 1, 25, 0, 0, 0, 1000, 0, 0, 'Nur dass Ihr es wisst: Kaja''Cola gibt es überall hier im Ort. Es ist das meistverkaufte Produkt des Bilgewasserkartells aller Zeiten! $B$BVergesst nicht, dass ein gepflegtes Image das A und O ist, wenn Ihr auf eine weitere Beförderung aus seid!$B$BNein, wartet! Profit ist das A und O! Aber Explosionen kommen gleich danach. Und dann: Image!', 23420), -- 25473
(14069, 2, 0, 0, 0, 0, 0, 0, 0, 'Ich hätte es selbst nicht besser machen können. Und das habe ich auch nicht.$B$BIhr seid uns allen eine Inspiration, $n. Ich bin mir sicher, dass die Großartigkeit, die Macht und, wenn ich hinzufügen darf, der extreme Reichtum $gdes neuen Handelsprinzen:der neuen Handelsprinzessin; $n bald in aller Munde sein wird!', 23420), -- 14069
(14075, 1, 0, 0, 0, 0, 0, 0, 0, 'Auf Nimmerwiedersehen, Ungeziefer!', 23420), -- 14075
(14138, 6, 0, 0, 0, 0, 0, 0, 0, 'Ein Geschenk von Sassy? Ich hoffe, es ist ein Bonus!', 23420); -- 14138


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text     xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_request_items` WHERE `ID` IN (14126 /*14126*/, 14121 /*14121*/, 14122 /*14122*/, 14123 /*14123*/, 14124 /*14124*/, 14110 /*14110*/, 24488 /*24488*/, 14071 /*14071*/, 25473 /*25473*/, 14138 /*14138*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(14126, 1, 0, 0, 0, 'Oh, $n, Ihr seid es. Welch unerwartete Überraschung.$B$B<Der Handelsprinz reibt die Handflächen aneinander.>', 23420), -- 14126
(14121, 6, 0, 0, 0, 'Konntet Ihr etwas von der gestohlenen Beute zurückstehlen?', 23420), -- 14121
(14122, 0, 0, 0, 0, 'Habt Ihr sie? Habt Ihr den Tresor geknackt und Eure privaten Reichtümer aus der Bank geholt?', 23420), -- 14122
(14123, 6, 0, 0, 0, 'Habt Ihr die Waren, Boss?', 23420), -- 14123
(14124, 6, 0, 0, 0, 'Wie viel Kaja''mit habt Ihr?', 23420), -- 14124
(14110, 6, 0, 0, 0, 'Hast du alles beisammen, $n? Ich kann es kaum erwarten, die neuen Sachen an dir zu sehen.$B$BMmm hmm!', 23420), -- 14110
(24488, 397, 0, 0, 0, 'Wir verlassen uns auf Euch, $n!', 23420), -- 24488
(14071, 6, 0, 0, 0, 'Die Party geht bald los. Wie war Eure Spritztour in die Stadt?', 23420), -- 14071
(25473, 5, 0, 1000, 0, '<Sassy beobachtet das Sechserpack als würde sie erwarten dass es explodiert, sobald Ihr es ihr gegeben habt.>$B$BWir brauchen mehr Kaja''Cola als das.$B$BIch schwöre, ich werde Feuchtlunte feuern, wenn ich ihn nicht zuerst umbringe!', 23420), -- 25473
(14138, 3, 0, 0, 0, 'Äh… alles läuft super. Ja.$B$BWas ist das?', 23420); -- 14138


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=12494 AND `id`=0) OR (`menu_id`=10620 AND `id`=0) OR (`menu_id`=10624 AND `id`=0) OR (`menu_id`=10622 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(12494, 0, '', '', 'Okay, Sassy, ich bin bereit aufzubrechen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10620, 0, '', '', 'Besorgt mir den fettesten, stahlendsten Klunker, den Ihr habt!', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10624, 0, '', '', 'Ich brauche eine coole Sonnenbrille. Was bekomme ich für zwei Stapel Moneten?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10622, 0, '', '', 'Szabo, ich brauche ein hippes neues Outfit für die Party, die ich geben werde!', '', '', '', '', '', '', '', '', '', '', '', '', '');


INSERT IGNORE INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `OptionBroadcastTextID`, `action_poi_id`, `box_coded`, `box_money`, `box_text`, `BoxBroadcastTextID`) VALUES
(12494, 0, 0, 'Okay, Sassy, ich bin bereit aufzubrechen.', 0, 0, 0, 0, '', 0),
(10620, 0, 0, 'Besorgt mir den fettesten, stahlendsten Klunker, den Ihr habt!', 0, 0, 0, 0, '', 0),
(10624, 0, 0, 'Ich brauche eine coole Sonnenbrille. Was bekomme ich für zwei Stapel Moneten?', 0, 0, 0, 0, '', 0),
(10622, 0, 0, 'Szabo, ich brauche ein hippes neues Outfit für die Party, die ich geben werde!', 0, 0, 0, 0, '', 0);


--unknown broadcasttextid
INSERT IGNORE INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `comment`) VALUES
(34668, 0, 0, 'Haha! Das wird ein Spaß!', 12, 0, 100, 11, 0, 0, 0, 'Sassy Hartzang to Player'),
(34668, 1, 0, 'Er besorgt mir später mehr? Wie sollen wir Euch denn $g zum Handelsprinzen : zur Handelsprinzessin; machen, wenn unser bestes Produkt nicht in den Regalen steht?!', 12, 0, 100, 274, 0, 0, 0, 'Sassy Hartzang to Player'),
(34668, 2, 0, 'Ass, Izzy, Gobber, Ihr drei geht mit $n mit und helft $g ihm : ihr;, sich um diese Schnorrer zu kümmern!', 12, 0, 100, 1, 0, 0, 0, 'Sassy Hartzang to Player'),
(34668, 3, 0, 'Holt sie Euch, Boss!', 12, 0, 100, 25, 0, 15978, 0, 'Sassy Hartzang to Player'),
(34668, 4, 0, 'Ihr solltet besser sofort hochgehen, Boss. Pronto sozusagen. Handelsprinz Gallywix wartet auf Euch.', 12, 0, 100, 6, 0, 0, 0, 'Sassy Hartzang to Player'),
(34668, 5, 0, '$n, Ihr müsst Euch beeilen und Eure Knete aus der Bank holen!', 12, 0, 100, 5, 0, 2304, 0, 'Sassy Hartzang to Player'),
(34668, 6, 0, 'Ich kann gar nicht mehr hinsehen. So viele gute Erinnerungen. So viel Profit.', 12, 0, 100, 18, 0, 0, 0, 'Sassy Hartzang to Player'),
(34668, 7, 0, 'Die Yacht des Handelsprinzen setzt in Kürze die Segel. Na los, alle, zum Dock! Gebt mir die Schlüssel, Boss. Ich fahre. Lasst mich einfach wissen, wenn Ihr los wollt.', 12, 0, 100, 1, 0, 0, 0, 'Sassy Hartzang to Player'),
(34692, 0, 0, 'Ich hoffe, die Extraseelenstärke macht Euch nichts aus, Boss.', 12, 0, 100, 0, 0, 0, 0, 'Schwester Goldglanz to Player'),
(34692, 1, 0, 'Ich sollte dafür wirklich eine Gebühr verlangen.', 12, 0, 100, 0, 0, 0, 0, 'Schwester Goldglanz to Player'),
(34693, 0, 0, 'Ich setze das Ding hier auf Autoaktivierung, sobald Ihr in der Villa seid. Stellt bloß sicher, dass Ihr diesen Schweinen aus dem Weg geht!', 12, 0, 100, 1, 0, 0, 0, 'Slinky Scharfklinge to Player'),
(34695, 0, 0, 'Wie habt Ihr zwei es bloß jemand in die Geschäftsführung geschafft?', 12, 0, 100, 0, 0, 0, 0, 'Lawinus Maximus'),
(34695, 1, 0, 'Ihr zwei Nulpen haltet die Klappe. Der Boss ist hier!', 12, 0, 100, 0, 0, 0, 0, 'Lawinus Maximus'),
(34830, 0, 0, 'Ich melde Euch beim Betriebsrat!', 12, 0, 100, 92, 0, 12007, 0, 'Widerspenstiger Troll to Player'),
(34830, 1, 0, 'Tut mir nicht weh, Mann!', 12, 0, 100, 92, 0, 12007, 0, 'Widerspenstiger Troll to Player'),
(34830, 2, 0, 'Tschuldigung, Mann. Wird nicht wieder vorkommen.', 12, 0, 100, 5, 0, 12007, 0, 'Widerspenstiger Troll to Player'),
(34830, 3, 0, 'Autsch! Das tat weh, Mann!', 12, 0, 100, 5, 0, 12007, 0, 'Widerspenstiger Troll to Player'),
(34830, 4, 0, 'Ich geh'' ja schon, ich geh'' ja schon.', 12, 0, 100, 92, 0, 12007, 0, 'Widerspenstiger Troll to Player'),
(34835, 0, 0, 'Ich war gerade auf dem Weg zu Euch. Wirklich!', 12, 0, 100, 0, 0, 0, 0, 'Bruno Flammhemmer to Player'),
(34835, 1, 0, 'Verdammt, Ihr habt mich erwischt!', 12, 0, 100, 0, 0, 0, 0, 'Bruno Flammhemmer'),
(34872, 0, 0, 'Immer nett, wenn einem der stellvertretende Leiter höchstpersönlich ein Geschenk überbringt. Das werde ich sofort auspacken!', 12, 0, 100, 432, 0, 0, 0, 'Vorarbeiter Feuchtlunte to Player'),
(34872, 1, 0, 'AUTSCH!', 14, 0, 100, 0, 0, 18495, 0, 'Vorarbeiter Feuchtlunte to Player'),
(34872, 2, 0, 'Schon gut! Ich habe die Botschaft verstanden!', 12, 0, 100, 5, 0, 0, 0, 'Vorarbeiter Feuchtlunte to Player'),
(34872, 3, 0, 'Ich schwöre, ihr Trolle seid der schlimmste Trupp, den wir jemals hatten!', 14, 0, 100, 0, 0, 0, 0, 'Vorarbeiter Feuchtlunte'),
(34872, 4, 0, 'Boss, sagt Sassy, dass ich ihr mehr Kaja''Cola besorge, sobald die Produktion wieder läuft.', 12, 0, 100, 1, 0, 0, 0, 'Vorarbeiter Feuchtlunte to Player'),
(34872, 5, 0, 'Ich kann einfach nicht glauben, wie dreist diese Trolle sind. Die rebellieren einfach, nach allem, was ich für sie getan habe!', 12, 0, 100, 5, 0, 0, 0, 'Vorarbeiter Feuchtlunte to Player'),
(34874, 0, 0, 'Ooh, ich glaube, das wird Euch gefallen, Boss!', 12, 0, 100, 273, 0, 0, 0, 'Megs Schredderschreck to Player'),
(34874, 1, 0, 'Da sind sie! Okay, Ihr drei werdet $g ihm : ihr; heute aushelfen. $G Er : Sie; hat noch ordentlich was zu tun, bevor die Party anfängt!', 12, 0, 100, 1, 0, 0, 0, 'Megs Schredderschreck to Player'),
(34876, 0, 0, 'Was? Okay, okay... Ich gebe meine Schutzknete Euren Schlägern.', 12, 0, 100, 0, 0, 0, 0, 'Frankie Gangschalter'),
(34878, 0, 0, 'Ich versuche hier, ein Geschäft zu führen!', 12, 0, 100, 0, 0, 0, 0, 'Sudsy Magee to Player'),
(34878, 1, 0, 'Nicht ins Gesicht, nicht ins Gesicht!', 12, 0, 100, 0, 0, 0, 0, 'Sudsy Magee'),
(34957, 0, 0, 'Was war das Ding, $g Kumpel : Herzchen;?', 12, 0, 100, 6, 0, 0, 0, 'Das Ass to Player'),
(34957, 1, 0, 'Ist das der Schnorrer?', 12, 0, 100, 0, 0, 0, 0, 'Das Ass'),
(34959, 0, 0, 'Warum bebt die Erde, $n?!', 12, 0, 100, 5, 0, 0, 0, 'Izzy to Player'),
(34959, 1, 0, 'Darauf habe ich den ganzen Tag gewartet!', 12, 0, 100, 0, 0, 0, 0, 'Izzy'),
(35054, 0, 0, 'Geht sicher, dass Ihr ordentlich Zaster aus der Bank holt. Ich möchte nicht, dass Ihr Euch wieder mit diesen billigen Klamotten eindeckt, die Ihr sonst so gerne tragt. Diese Party ist wichtig!', 12, 0, 100, 23, 0, 0, 0, 'Chip Endale to Player'),
(35054, 1, 0, 'Nun schau dich mal an! So viel Schwein wie ich hat niemand hier auf der Insel!', 12, 0, 100, 0, 0, 15978, 0, 'Chip Endale to Player'),
(35054, 2, 0, 'Das mit uns ist vorbei, $n! Ich bin jetzt mit Vani zusammen.', 12, 0, 100, 11, 0, 0, 0, 'Chip Endale to Player'),
(35063, 0, 0, 'Der Handelsprinz wird hiervon erfahren, $n!', 14, 0, 100, 5, 0, 1411, 0, 'Bürger von Kezan to Player'),
(35063, 1, 0, 'Lernt gefälligst, wie man fährt, Ihr Irrer!', 14, 0, 100, 1, 0, 1411, 0, 'Bürger von Kezan to Player'),
(35063, 2, 0, 'He, seht, es ist $n. Ich wette, $g er : sie; ist stinkreich. Ich habe gehört, $g er : sie; hat es auf die Stelle des Handelsprinzen abgesehen!', 15, 0, 100, 6, 0, 0, 0, 'Bürger von Kezan to Player'),
(35063, 3, 0, 'Szabo. Er ist gerade total angesagt!', 12, 0, 100, 25, 0, 0, 0, 'Bürger von Kezan to Player'),
(35063, 4, 0, 'Mein Nacken! Ich werde Euch verklagen!', 14, 0, 100, 5, 0, 1411, 0, 'Bürger von Kezan to Player'),
(35063, 5, 0, 'Ich kriege Euch, $n!', 14, 0, 100, 5, 0, 12924, 0, 'Bürger von Kezan to Player'),
(35075, 0, 0, 'Ihr werdet von meinem Anwalt hören!', 14, 0, 100, 94, 0, 18500, 0, 'Bürgerin von Kezan to Player'),
(35075, 1, 0, 'Ihr seid ein öffentliches Ärgernis, $n!', 14, 0, 100, 5, 0, 18500, 0, 'Bürgerin von Kezan to Player'),
(35120, 0, 0, 'Der Nächste, bitte!', 12, 0, 100, 5, 0, 0, 0, 'Kassierer der EBvK'),
(35120, 1, 0, 'Wartet mal ''ne Runde, Leute! $n hier wird $G der nächste Handelsprinz : die nächste Handelsprinzessin; !', 12, 0, 100, 5, 0, 0, 0, 'Kassierer der EBvK to Player'),
(35175, 0, 0, 'Hätte ich nur jemanden, mit dem ich tanzen kann.', 12, 0, 100, 0, 0, 15978, 0, 'Kezanischer Feiernder to Kezanischer Feiernder'),
(35175, 1, 0, 'Ich könnte wirklich noch einen Schluck von meinem Drink hier vertragen.', 12, 0, 100, 0, 0, 15978, 0, 'Kezanischer Feiernder to Kezanischer Feiernder'),
(35175, 2, 0, 'Das ist köstlich! Sind noch ein paar Hors d''oeuvre da?', 12, 0, 100, 0, 0, 15978, 0, 'Kezanischer Feiernder to Kezanischer Feiernder'),
(35175, 3, 0, 'Ich liebe Feuerwerk!', 12, 0, 100, 0, 0, 15978, 0, 'Kezanischer Feiernder to Kezanischer Feiernder'),
(35175, 4, 0, 'Oh, mein Kopf tut weh...', 12, 0, 100, 0, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35175, 5, 0, 'Uff... Ich brauche einen Eimer!', 12, 0, 100, 0, 0, 15978, 0, 'Kezanischer Feiernder to Kezanischer Feiernder'),
(35175, 6, 0, 'Dieser prickelnde Weißwein ist sehr lecker! Wo habt Ihr das bloß her?', 12, 0, 100, 113, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35175, 7, 0, 'Danke. Ich war schon fast ohnmächtig. HUNGER!', 12, 0, 100, 397, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35175, 8, 0, 'Wir sollten hier eine Tanzshow veranstalten! Mit Ausziehen!', 12, 0, 100, 4, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35175, 9, 0, 'Eine Diskokugel?! Abgefahren!', 12, 0, 100, 4, 0, 2304, 0, 'Kezanischer Feiernder to Player'),
(35175, 10, 0, 'Ein frisches Glas Schampus. Genau das, was der Doktor empfehlen würde, $n.', 12, 0, 100, 113, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35175, 11, 0, 'Ich fühle mich jetscht schon viel bescher... Hicks!', 12, 0, 100, 0, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35175, 12, 0, 'Ihr scheut ja wirklich keine Kosten und Mühen, $n! Großartig!', 12, 0, 100, 153, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35175, 13, 0, 'Danke!', 12, 0, 100, 0, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35175, 14, 0, 'Wow! Das schlägt diese mickrige kleine Wunderkerze ja um Längen!', 12, 0, 100, 153, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35175, 15, 0, 'Absolut großartig!', 12, 0, 100, 397, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35175, 16, 0, 'Schüttel, was du hast, Baby!', 12, 0, 100, 4, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35186, 0, 0, 'Woohoo, ein Feuerwerk! Mehr, mehr!', 12, 0, 100, 153, 0, 15978, 0, 'Kezanischer Feiernder to Player'),
(35200, 0, 0, 'Wir sind hinter dem Gold her!', 12, 0, 100, 92, 0, 0, 0, 'Ungeladene Piratin to Kezanischer Feiernder'),
(35200, 1, 0, 'Lasst uns ein Tänzchen wagen!', 12, 0, 100, 0, 0, 0, 0, 'Ungeladener Pirat to Kezanischer Feiernder'),
(35222, 0, 0, 'Trinkt Kaja''Cola!', 14, 0, 100, 5, 0, 19549, 0, 'Handelsprinz Gallywix to Player'),
(35222, 1, 0, 'Ihr Schergen haltet Ausschau nach Plünderern! Und vergesst nicht: Gewährt ihnen kein Pardon!', 14, 0, 100, 0, 0, 0, 0, 'Handelsprinz Gallywix'),
(35294, 0, 0, 'Ihr holt den großen, bösen Drachen her und zerstört die Insel!', 12, 0, 100, 71, 0, 0, 0, 'Rebellischer Troll to Player'),
(35304, 0, 0, 'Die Trolle kämpfen wollen?', 12, 0, 100, 6, 0, 0, 0, 'Brutaler Vollstrecker to Player'),
(35304, 1, 0, 'Trolle sagen schlimme Dinge, Boss.', 12, 0, 100, 1, 0, 0, 0, 'Brutaler Vollstrecker to Player'),
(35486, 0, 0, 'Ihr brecht in den Tresor ein, um Eure privaten Reichtümer in Euren Besitz zu bringen!', 42, 0, 100, 0, 0, 0, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(35486, 1, 0, '', 42, 0, 100, 0, 0, 0, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(35486, 2, 0, 'Benutzt, was benötigt wird, aus Eurem goblinischen Alles-in-1-zigartigen Gürtel, um den Tresor aufzubrechen!$B|TInterface\\Icons\\INV_Misc_EngGizmos_20.blp:64|t |TInterface\\Icons\\INV_Misc_Bomb_07.blp:64|t |TInterface\\Icons\\INV_Misc_Ear_NightElf_02.blp:64|t |TInterface\\Icons\\INV_Misc_EngGizmos_swissArmy.blp:64|t |TInterface\\Icons\\INV_Weapon_ShortBlade_21.blp:64|t', 42, 0, 100, 0, 0, 0, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(35486, 3, 0, 'Der Tresor ist aufgebrochen, wenn der |cFFFF2222Fortschrittsbalken des Tresoreinbruchs 100 Prozent erreicht hat!|r$B|TInterface\\Icons\\INV_Misc_coin_02.blp:64|t$BWenn Ihr die falsche Sache zur falschen Zeit tut, wird der Fortschritt auf dem Balken verringert.', 42, 0, 100, 0, 0, 0, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(35486, 4, 0, 'Viel Glück!', 42, 0, 100, 0, 0, 0, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(35486, 5, 0, 'Benutzt Euer |cFFFF2222Ohr-O-Skop!|r$B|TInterface\\Icons\\INV_Misc_Ear_NightElf_02.blp:64|t', 42, 0, 100, 0, 0, 0, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(35486, 6, 0, 'Richtig!', 42, 0, 100, 0, 0, 11595, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(35486, 7, 0, 'Benutzt Euren |cFFFF2222Unglaublichen G-Strahl!|r$B|TInterface\\Icons\\INV_Misc_EngGizmos_20.blp:64|t', 42, 0, 100, 0, 0, 0, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(35486, 8, 0, 'Benutzt Euren |cFFFF2222Kaja''mitbohrer!|r$B|TInterface\\Icons\\INV_Weapon_ShortBlade_21.blp:64|t', 42, 0, 100, 0, 0, 0, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(35486, 9, 0, 'Benutzt Euren |cFFFF2222Infinifachdietrich!|r$B|TInterface\\Icons\\INV_Misc_EngGizmos_swissArmy.blp:64|t', 42, 0, 100, 0, 0, 0, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(35486, 10, 0, 'Benutzt Eure |cFFFF2222Sprengpellets!|r$B|TInterface\\Icons\\INV_Misc_Bomb_07.blp:64|t', 42, 0, 100, 0, 0, 0, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(35486, 11, 0, 'Triumph! Ihr habt Eure privaten Reichtümer an Euch gebracht!$B$B|TInterface\\Icons\\INV_Misc_coin_02.blp:64|t', 42, 0, 100, 0, 0, 11595, 0, 'Tresor der Ersten Bank von Kezan to Player'),
(36600, 0, 0, 'Das ist $n?! Entschuldigt, Doktor, ich dachte, $g er : sie; sei tot!', 12, 0, 100, 0, 0, 0, 0, 'Gizmo Gangzwinger to Player'),
(36600, 1, 0, 'Ja, da ist noch ein Haufen Leute in den Fluchtkapseln gefangen, Boss. Oh, wartet. Ich vermute, Ihr seid nicht mehr der Boss. Wie dem auch sei, sie werden wohl alle sterben, wenn Ihr sie nicht herausholt.', 12, 0, 100, 1, 0, 0, 0, 'Gizmo Gangzwinger to Player'),
(36608, 0, 0, 'Gizmo, was sitzt Ihr da herum? Erkennt Ihr nicht, wer da neben Euch liegt?!', 12, 0, 100, 396, 0, 0, 0, 'Doc Knalldüse to Player'),
(36608, 1, 0, 'Das ist $n! $G Er: Sie; ist der alleinige Grund, weshalb wir noch atmen und nicht knusprig gebraten auf Kezan sind.', 12, 0, 100, 396, 0, 0, 0, 'Doc Knalldüse to Player'),
(36608, 2, 0, 'Tretet zurück, ich werde $g ihn : sie; wiederbeleben! Ich hoffe, diese nassen Überbrückungskabel bringen uns nicht alle um!', 12, 0, 100, 396, 0, 0, 0, 'Doc Knalldüse to Player'),
(36608, 3, 0, 'Kommt schon! Bleibt weg!', 12, 0, 100, 396, 0, 0, 0, 'Doc Knalldüse to Player'),
(36608, 4, 0, 'Das ist alles, was ich tun kann. Es liegt nun an $g ihm : ihr;. Hört Ihr mich, $n? Kommt schon, kriegt Euch wieder ein! Geht nicht ins Licht!', 12, 0, 100, 396, 0, 0, 0, 'Doc Knalldüse to Player'),
(36608, 5, 0, 'Ihr habt die richtige Wahl getroffen. Wir alle stehen tief in Eurer Schuld, $n. Versucht Euch hier draußen nicht töten zu lassen.', 12, 0, 100, 396, 0, 0, 0, 'Doc Knalldüse to Player'),
(36608, 6, 0, 'Es gibt noch mehr Überlebende, um die ich mich kümmern muss. Wir sehen uns an der Küste.', 12, 0, 100, 397, 0, 0, 0, 'Doc Knalldüse to Player'),
(37106, 0, 0, 'Ihr müsst Euch beeilen, Kleines. Wir müssen Euch ins Spiel bringen. Aber zunächst müssen wir die Ersatzteile für den Schredder besorgen!', 12, 0, 100, 5, 0, 0, 0, 'Trainer Blutgrätsch to Player'),
(37106, 1, 0, 'Setzt Euch in den Schredder und gewinnt das Spiel. Das Bilgewasserkartell zählt auf Euch!', 12, 0, 100, 25, 0, 0, 0, 'Trainer Blutgrätsch to Player'),
(37106, 2, 0, 'Ihr habt Mumm, Kleines, so viel ist sicher! Zu blöd, dass der Drache angekommen ist und alles ruiniert hat. Ist aber auch egal, Ihr habt uns stolz gemacht. Und jetzt zurück mit Euch zum Hauptquartier.', 12, 0, 100, 4, 0, 0, 0, 'Trainer Blutgrätsch to Player'),
(37179, 0, 0, 'Werft Eure Fußbomben auf diese Dampfdruckhaie!', 42, 0, 100, 0, 0, 4294967295, 0, 'Bilgewasserbukanier to Player'),
(37213, 0, 0, 'Schießt die Fußbombe mit einem Tritt zwischen die Schornsteine und hinter das gegnerische Tor!', 42, 0, 100, 0, 0, 17466, 0, 'Bilgewasserbukanier to Player'),
(37500, 0, 0, 'Vulkangestein! Zeitlich begrenztes Angebot! Echtheitszeugnis und alles drum und dran!', 14, 0, 100, 0, 0, 18500, 0, 'Vinny Spalthieb'),
(37500, 1, 0, 'Vulkangestein zu verkaufen! Frisch vom Berg! Ihr wollt sie, ich habe sie!', 14, 0, 100, 5, 0, 0, 0, 'Vinny Spalthieb'),
(37500, 2, 0, 'Holt Euch Euer Vulkangestein hier! Großartig für Katzen!', 14, 0, 100, 5, 0, 11595, 0, 'Vinny Spalthieb'),
(37602, 0, 0, 'Noch ein fehlerhafter-elektrischer-entflammbarer Gas-Bett-Feuerwerk-Unfall?!', 14, 0, 100, 0, 0, 0, 0, 'Schadensreferent to Player'),
(37680, 0, 0, 'Wir müssen drum herumgehen.', 12, 0, 100, 0, 0, 18500, 0, 'Sassy Hartzang to Player'),
(37680, 1, 0, 'AUS DEM WEG!', 14, 0, 100, 0, 0, 18500, 0, 'Sassy Hartzang to Player'),
(37680, 2, 0, 'So, wir sind gesund und... OH NEIN! Man hat uns betrogen!', 12, 0, 100, 0, 0, 0, 0, 'Sassy Hartzang to Player'),
(37804, 0, 0, 'Ganz neue Kaja''Cola! Jetzt mit 100 Prozent mehr Ideen!', 14, 0, 100, 0, 0, 17466, 0, 'Kaja''Colaballon'),
(37804, 1, 0, 'Nehmt teil am Kaja''Cola-Geschmackstest-Wettbewerb!', 14, 0, 100, 0, 0, 0, 0, 'Kaja''Colaballon'),
(37804, 2, 0, 'Kaja''Cola! Hier kommen die IDEEN!', 14, 0, 100, 0, 0, 0, 0, 'Kaja''Colaballon'),
(48494, 0, 0, 'Bereitet das Mikromechahuhn vor!', 14, 0, 100, 25, 0, 0, 0, 'Hobart Wurfhammer to ELM General Purpose Bunny Hide Body'),
(48494, 1, 0, 'Es gibt eine ganze Menge, was Ihr nicht versteht, meine liebe Greely. Darum seid Ihr ja auch die Assistentin und ich... ich bin Hobart Wurfhammer!', 12, 0, 100, 5, 0, 0, 0, 'Hobart Wurfhammer to ELM General Purpose Bunny Hide Body'),
(48494, 2, 0, 'Der Himmel fällt uns auf den Kopf!', 14, 0, 100, 5, 0, 15978, 0, 'Hobart Wurfhammer'),
(48494, 3, 0, 'Packt alles zusammen! Wir müssen einen Weg finden, wie wir von Kezan wegkommen! Wir springen in die nächstgelegene Instastadt! Sie werden sie an Bord der Yacht des Handelsprinzen bringen.', 14, 0, 100, 273, 0, 0, 0, 'Hobart Wurfhammer'),
(48496, 0, 0, 'Ihr müsst nicht so schreien, Hobart, ich bin doch direkt hier. Pssst!', 12, 0, 100, 274, 0, 0, 0, 'Assistentin Greely to ELM General Purpose Bunny Hide Body'),
(48496, 1, 0, 'Da, bitte. Ein Mikromechahuhn. Ich werde nie verstehen, wie Ihr auf diese Namen kommt.', 12, 0, 100, 25, 0, 0, 0, 'Assistentin Greely to ELM General Purpose Bunny Hide Body'),
(48496, 2, 0, 'Ja, ja, ich kümmere mich drum. Aber, Hobart, Ihr müsst Euch beruhigen. Euer Blutdruck! Ihr lauft ja schon dunkelgrün an!', 12, 0, 100, 0, 0, 4294967295, 0, 'Assistentin Greely to Hobart Wurfhammer'),
(48572, 0, 0, 'Die Sonne geht unter für diese sterbliche Welt, Ihr Narren. Macht Euren Frieden mit Eurem Ende, denn die Stunde des Zwielichts ist gekommen!', 14, 0, 100, 0, 0, 23228, 0, 'Todesschwinge to Player');


DELETE FROM `locales_creature_text` WHERE (`entry`=34668 AND `groupid`=0) OR (`entry`=34668 AND `groupid`=1) OR (`entry`=34668 AND `groupid`=2) OR (`entry`=34668 AND `groupid`=3) OR (`entry`=34668 AND `groupid`=4) OR (`entry`=34668 AND `groupid`=5) OR (`entry`=34668 AND `groupid`=6) OR (`entry`=34668 AND `groupid`=7) OR (`entry`=34692 AND `groupid`=0) OR (`entry`=34692 AND `groupid`=1) OR (`entry`=34693 AND `groupid`=0) OR (`entry`=34695 AND `groupid`=0) OR (`entry`=34695 AND `groupid`=1) OR (`entry`=34830 AND `groupid`=0) OR (`entry`=34830 AND `groupid`=1) OR (`entry`=34830 AND `groupid`=2) OR (`entry`=34830 AND `groupid`=3) OR (`entry`=34830 AND `groupid`=4) OR (`entry`=34835 AND `groupid`=0) OR (`entry`=34835 AND `groupid`=1) OR (`entry`=34872 AND `groupid`=0) OR (`entry`=34872 AND `groupid`=1) OR (`entry`=34872 AND `groupid`=2) OR (`entry`=34872 AND `groupid`=3) OR (`entry`=34872 AND `groupid`=4) OR (`entry`=34872 AND `groupid`=5) OR (`entry`=34874 AND `groupid`=0) OR (`entry`=34874 AND `groupid`=1) OR (`entry`=34876 AND `groupid`=0) OR (`entry`=34878 AND `groupid`=0) OR (`entry`=34878 AND `groupid`=1) OR (`entry`=34957 AND `groupid`=0) OR (`entry`=34957 AND `groupid`=1) OR (`entry`=34959 AND `groupid`=0) OR (`entry`=34959 AND `groupid`=1) OR (`entry`=35054 AND `groupid`=0) OR (`entry`=35054 AND `groupid`=1) OR (`entry`=35054 AND `groupid`=2) OR (`entry`=35063 AND `groupid`=0) OR (`entry`=35063 AND `groupid`=1) OR (`entry`=35063 AND `groupid`=2) OR (`entry`=35063 AND `groupid`=3) OR (`entry`=35063 AND `groupid`=4) OR (`entry`=35063 AND `groupid`=5) OR (`entry`=35075 AND `groupid`=0) OR (`entry`=35075 AND `groupid`=1) OR (`entry`=35120 AND `groupid`=0) OR (`entry`=35120 AND `groupid`=1) OR (`entry`=35175 AND `groupid`=0) OR (`entry`=35175 AND `groupid`=1) OR (`entry`=35175 AND `groupid`=2) OR (`entry`=35175 AND `groupid`=3) OR (`entry`=35175 AND `groupid`=4) OR (`entry`=35175 AND `groupid`=5) OR (`entry`=35175 AND `groupid`=6) OR (`entry`=35175 AND `groupid`=7) OR (`entry`=35175 AND `groupid`=8) OR (`entry`=35175 AND `groupid`=9) OR (`entry`=35175 AND `groupid`=10) OR (`entry`=35175 AND `groupid`=11) OR (`entry`=35175 AND `groupid`=12) OR (`entry`=35175 AND `groupid`=13) OR (`entry`=35175 AND `groupid`=14) OR (`entry`=35175 AND `groupid`=15) OR (`entry`=35175 AND `groupid`=16) OR (`entry`=35186 AND `groupid`=0) OR (`entry`=35200 AND `groupid`=0) OR (`entry`=35200 AND `groupid`=1) OR (`entry`=35222 AND `groupid`=0) OR (`entry`=35222 AND `groupid`=1) OR (`entry`=35294 AND `groupid`=0) OR (`entry`=35304 AND `groupid`=0) OR (`entry`=35304 AND `groupid`=1) OR (`entry`=35486 AND `groupid`=0) OR (`entry`=35486 AND `groupid`=1) OR (`entry`=35486 AND `groupid`=2) OR (`entry`=35486 AND `groupid`=3) OR (`entry`=35486 AND `groupid`=4) OR (`entry`=35486 AND `groupid`=5) OR (`entry`=35486 AND `groupid`=6) OR (`entry`=35486 AND `groupid`=7) OR (`entry`=35486 AND `groupid`=8) OR (`entry`=35486 AND `groupid`=9) OR (`entry`=35486 AND `groupid`=10) OR (`entry`=35486 AND `groupid`=11) OR (`entry`=36600 AND `groupid`=0) OR (`entry`=36600 AND `groupid`=1) OR (`entry`=36608 AND `groupid`=0) OR (`entry`=36608 AND `groupid`=1) OR (`entry`=36608 AND `groupid`=2) OR (`entry`=36608 AND `groupid`=3) OR (`entry`=36608 AND `groupid`=4) OR (`entry`=36608 AND `groupid`=5) OR (`entry`=36608 AND `groupid`=6) OR (`entry`=37106 AND `groupid`=0) OR (`entry`=37106 AND `groupid`=1) OR (`entry`=37106 AND `groupid`=2) OR (`entry`=37179 AND `groupid`=0) OR (`entry`=37213 AND `groupid`=0) OR (`entry`=37500 AND `groupid`=0) OR (`entry`=37500 AND `groupid`=1) OR (`entry`=37500 AND `groupid`=2) OR (`entry`=37602 AND `groupid`=0) OR (`entry`=37680 AND `groupid`=0) OR (`entry`=37680 AND `groupid`=1) OR (`entry`=37680 AND `groupid`=2) OR (`entry`=37804 AND `groupid`=0) OR (`entry`=37804 AND `groupid`=1) OR (`entry`=37804 AND `groupid`=2) OR (`entry`=48494 AND `groupid`=0) OR (`entry`=48494 AND `groupid`=1) OR (`entry`=48494 AND `groupid`=2) OR (`entry`=48494 AND `groupid`=3) OR (`entry`=48496 AND `groupid`=0) OR (`entry`=48496 AND `groupid`=1) OR (`entry`=48496 AND `groupid`=2) OR (`entry`=48572 AND `groupid`=0); 
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(34668, 0, 0, '', '', 'Haha! Das wird ein Spaß!', '', '', '', '', ''),
(34668, 1, 0, '', '', 'Er besorgt mir später mehr? Wie sollen wir Euch denn $g zum Handelsprinzen : zur Handelsprinzessin; machen, wenn unser bestes Produkt nicht in den Regalen steht?!', '', '', '', '', ''),
(34668, 2, 0, '', '', 'Ass, Izzy, Gobber, Ihr drei geht mit $n mit und helft $g ihm : ihr;, sich um diese Schnorrer zu kümmern!', '', '', '', '', ''),
(34668, 3, 0, '', '', 'Holt sie Euch, Boss!', '', '', '', '', ''),
(34668, 4, 0, '', '', 'Ihr solltet besser sofort hochgehen, Boss. Pronto sozusagen. Handelsprinz Gallywix wartet auf Euch.', '', '', '', '', ''),
(34668, 5, 0, '', '', '$n, Ihr müsst Euch beeilen und Eure Knete aus der Bank holen!', '', '', '', '', ''),
(34668, 6, 0, '', '', 'Ich kann gar nicht mehr hinsehen. So viele gute Erinnerungen. So viel Profit.', '', '', '', '', ''),
(34668, 7, 0, '', '', 'Die Yacht des Handelsprinzen setzt in Kürze die Segel. Na los, alle, zum Dock! Gebt mir die Schlüssel, Boss. Ich fahre. Lasst mich einfach wissen, wenn Ihr los wollt.', '', '', '', '', ''),
(34692, 0, 0, '', '', 'Ich hoffe, die Extraseelenstärke macht Euch nichts aus, Boss.', '', '', '', '', ''),
(34692, 1, 0, '', '', 'Ich sollte dafür wirklich eine Gebühr verlangen.', '', '', '', '', ''),
(34693, 0, 0, '', '', 'Ich setze das Ding hier auf Autoaktivierung, sobald Ihr in der Villa seid. Stellt bloß sicher, dass Ihr diesen Schweinen aus dem Weg geht!', '', '', '', '', ''),
(34695, 0, 0, '', '', 'Wie habt Ihr zwei es bloß jemand in die Geschäftsführung geschafft?', '', '', '', '', ''),
(34695, 1, 0, '', '', 'Ihr zwei Nulpen haltet die Klappe. Der Boss ist hier!', '', '', '', '', ''),
(34830, 0, 0, '', '', 'Ich melde Euch beim Betriebsrat!', '', '', '', '', ''),
(34830, 1, 0, '', '', 'Tut mir nicht weh, Mann!', '', '', '', '', ''),
(34830, 2, 0, '', '', 'Tschuldigung, Mann. Wird nicht wieder vorkommen.', '', '', '', '', ''),
(34830, 3, 0, '', '', 'Autsch! Das tat weh, Mann!', '', '', '', '', ''),
(34830, 4, 0, '', '', 'Ich geh'' ja schon, ich geh'' ja schon.', '', '', '', '', ''),
(34835, 0, 0, '', '', 'Ich war gerade auf dem Weg zu Euch. Wirklich!', '', '', '', '', ''),
(34835, 1, 0, '', '', 'Verdammt, Ihr habt mich erwischt!', '', '', '', '', ''),
(34872, 0, 0, '', '', 'Immer nett, wenn einem der stellvertretende Leiter höchstpersönlich ein Geschenk überbringt. Das werde ich sofort auspacken!', '', '', '', '', ''),
(34872, 1, 0, '', '', 'AUTSCH!', '', '', '', '', ''),
(34872, 2, 0, '', '', 'Schon gut! Ich habe die Botschaft verstanden!', '', '', '', '', ''),
(34872, 3, 0, '', '', 'Ich schwöre, ihr Trolle seid der schlimmste Trupp, den wir jemals hatten!', '', '', '', '', ''),
(34872, 4, 0, '', '', 'Boss, sagt Sassy, dass ich ihr mehr Kaja''Cola besorge, sobald die Produktion wieder läuft.', '', '', '', '', ''),
(34872, 5, 0, '', '', 'Ich kann einfach nicht glauben, wie dreist diese Trolle sind. Die rebellieren einfach, nach allem, was ich für sie getan habe!', '', '', '', '', ''),
(34874, 0, 0, '', '', 'Ooh, ich glaube, das wird Euch gefallen, Boss!', '', '', '', '', ''),
(34874, 1, 0, '', '', 'Da sind sie! Okay, Ihr drei werdet $g ihm : ihr; heute aushelfen. $G Er : Sie; hat noch ordentlich was zu tun, bevor die Party anfängt!', '', '', '', '', ''),
(34876, 0, 0, '', '', 'Was? Okay, okay... Ich gebe meine Schutzknete Euren Schlägern.', '', '', '', '', ''),
(34878, 0, 0, '', '', 'Ich versuche hier, ein Geschäft zu führen!', '', '', '', '', ''),
(34878, 1, 0, '', '', 'Nicht ins Gesicht, nicht ins Gesicht!', '', '', '', '', ''),
(34957, 0, 0, '', '', 'Was war das Ding, $g Kumpel : Herzchen;?', '', '', '', '', ''),
(34957, 1, 0, '', '', 'Ist das der Schnorrer?', '', '', '', '', ''),
(34959, 0, 0, '', '', 'Warum bebt die Erde, $n?!', '', '', '', '', ''),
(34959, 1, 0, '', '', 'Darauf habe ich den ganzen Tag gewartet!', '', '', '', '', ''),
(35054, 0, 0, '', '', 'Geht sicher, dass Ihr ordentlich Zaster aus der Bank holt. Ich möchte nicht, dass Ihr Euch wieder mit diesen billigen Klamotten eindeckt, die Ihr sonst so gerne tragt. Diese Party ist wichtig!', '', '', '', '', ''),
(35054, 1, 0, '', '', 'Nun schau dich mal an! So viel Schwein wie ich hat niemand hier auf der Insel!', '', '', '', '', ''),
(35054, 2, 0, '', '', 'Das mit uns ist vorbei, $n! Ich bin jetzt mit Vani zusammen.', '', '', '', '', ''),
(35063, 0, 0, '', '', 'Der Handelsprinz wird hiervon erfahren, $n!', '', '', '', '', ''),
(35063, 1, 0, '', '', 'Lernt gefälligst, wie man fährt, Ihr Irrer!', '', '', '', '', ''),
(35063, 2, 0, '', '', 'He, seht, es ist $n. Ich wette, $g er : sie; ist stinkreich. Ich habe gehört, $g er : sie; hat es auf die Stelle des Handelsprinzen abgesehen!', '', '', '', '', ''),
(35063, 3, 0, '', '', 'Szabo. Er ist gerade total angesagt!', '', '', '', '', ''),
(35063, 4, 0, '', '', 'Mein Nacken! Ich werde Euch verklagen!', '', '', '', '', ''),
(35063, 5, 0, '', '', 'Ich kriege Euch, $n!', '', '', '', '', ''),
(35075, 0, 0, '', '', 'Ihr werdet von meinem Anwalt hören!', '', '', '', '', ''),
(35075, 1, 0, '', '', 'Ihr seid ein öffentliches Ärgernis, $n!', '', '', '', '', ''),
(35120, 0, 0, '', '', 'Der Nächste, bitte!', '', '', '', '', ''),
(35120, 1, 0, '', '', 'Wartet mal ''ne Runde, Leute! $n hier wird $G der nächste Handelsprinz : die nächste Handelsprinzessin; !', '', '', '', '', ''),
(35175, 0, 0, '', '', 'Hätte ich nur jemanden, mit dem ich tanzen kann.', '', '', '', '', ''),
(35175, 1, 0, '', '', 'Ich könnte wirklich noch einen Schluck von meinem Drink hier vertragen.', '', '', '', '', ''),
(35175, 2, 0, '', '', 'Das ist köstlich! Sind noch ein paar Hors d''oeuvre da?', '', '', '', '', ''),
(35175, 3, 0, '', '', 'Ich liebe Feuerwerk!', '', '', '', '', ''),
(35175, 4, 0, '', '', 'Oh, mein Kopf tut weh...', '', '', '', '', ''),
(35175, 5, 0, '', '', 'Uff... Ich brauche einen Eimer!', '', '', '', '', ''),
(35175, 6, 0, '', '', 'Dieser prickelnde Weißwein ist sehr lecker! Wo habt Ihr das bloß her?', '', '', '', '', ''),
(35175, 7, 0, '', '', 'Danke. Ich war schon fast ohnmächtig. HUNGER!', '', '', '', '', ''),
(35175, 8, 0, '', '', 'Wir sollten hier eine Tanzshow veranstalten! Mit Ausziehen!', '', '', '', '', ''),
(35175, 9, 0, '', '', 'Eine Diskokugel?! Abgefahren!', '', '', '', '', ''),
(35175, 10, 0, '', '', 'Ein frisches Glas Schampus. Genau das, was der Doktor empfehlen würde, $n.', '', '', '', '', ''),
(35175, 11, 0, '', '', 'Ich fühle mich jetscht schon viel bescher... Hicks!', '', '', '', '', ''),
(35175, 12, 0, '', '', 'Ihr scheut ja wirklich keine Kosten und Mühen, $n! Großartig!', '', '', '', '', ''),
(35175, 13, 0, '', '', 'Danke!', '', '', '', '', ''),
(35175, 14, 0, '', '', 'Wow! Das schlägt diese mickrige kleine Wunderkerze ja um Längen!', '', '', '', '', ''),
(35175, 15, 0, '', '', 'Absolut großartig!', '', '', '', '', ''),
(35175, 16, 0, '', '', 'Schüttel, was du hast, Baby!', '', '', '', '', ''),
(35186, 0, 0, '', '', 'Woohoo, ein Feuerwerk! Mehr, mehr!', '', '', '', '', ''),
(35200, 0, 0, '', '', 'Wir sind hinter dem Gold her!', '', '', '', '', ''),
(35200, 1, 0, '', '', 'Lasst uns ein Tänzchen wagen!', '', '', '', '', ''),
(35222, 0, 0, '', '', 'Trinkt Kaja''Cola!', '', '', '', '', ''),
(35222, 1, 0, '', '', 'Ihr Schergen haltet Ausschau nach Plünderern! Und vergesst nicht: Gewährt ihnen kein Pardon!', '', '', '', '', ''),
(35294, 0, 0, '', '', 'Ihr holt den großen, bösen Drachen her und zerstört die Insel!', '', '', '', '', ''),
(35304, 0, 0, '', '', 'Die Trolle kämpfen wollen?', '', '', '', '', ''),
(35304, 1, 0, '', '', 'Trolle sagen schlimme Dinge, Boss.', '', '', '', '', ''),
(35486, 0, 0, '', '', 'Ihr brecht in den Tresor ein, um Eure privaten Reichtümer in Euren Besitz zu bringen!', '', '', '', '', ''),
(35486, 1, 0, '', '', '', '', '', '', '', ''),
(35486, 2, 0, '', '', 'Benutzt, was benötigt wird, aus Eurem goblinischen Alles-in-1-zigartigen Gürtel, um den Tresor aufzubrechen!$B|TInterface\\Icons\\INV_Misc_EngGizmos_20.blp:64|t |TInterface\\Icons\\INV_Misc_Bomb_07.blp:64|t |TInterface\\Icons\\INV_Misc_Ear_NightElf_02.blp:64|t |TInterface\\Icons\\INV_Misc_EngGizmos_swissArmy.blp:64|t |TInterface\\Icons\\INV_Weapon_ShortBlade_21.blp:64|t', '', '', '', '', ''),
(35486, 3, 0, '', '', 'Der Tresor ist aufgebrochen, wenn der |cFFFF2222Fortschrittsbalken des Tresoreinbruchs 100 Prozent erreicht hat!|r$B|TInterface\\Icons\\INV_Misc_coin_02.blp:64|t$BWenn Ihr die falsche Sache zur falschen Zeit tut, wird der Fortschritt auf dem Balken verringert.', '', '', '', '', ''),
(35486, 4, 0, '', '', 'Viel Glück!', '', '', '', '', ''),
(35486, 5, 0, '', '', 'Benutzt Euer |cFFFF2222Ohr-O-Skop!|r$B|TInterface\\Icons\\INV_Misc_Ear_NightElf_02.blp:64|t', '', '', '', '', ''),
(35486, 6, 0, '', '', 'Richtig!', '', '', '', '', ''),
(35486, 7, 0, '', '', 'Benutzt Euren |cFFFF2222Unglaublichen G-Strahl!|r$B|TInterface\\Icons\\INV_Misc_EngGizmos_20.blp:64|t', '', '', '', '', ''),
(35486, 8, 0, '', '', 'Benutzt Euren |cFFFF2222Kaja''mitbohrer!|r$B|TInterface\\Icons\\INV_Weapon_ShortBlade_21.blp:64|t', '', '', '', '', ''),
(35486, 9, 0, '', '', 'Benutzt Euren |cFFFF2222Infinifachdietrich!|r$B|TInterface\\Icons\\INV_Misc_EngGizmos_swissArmy.blp:64|t', '', '', '', '', ''),
(35486, 10, 0, '', '', 'Benutzt Eure |cFFFF2222Sprengpellets!|r$B|TInterface\\Icons\\INV_Misc_Bomb_07.blp:64|t', '', '', '', '', ''),
(35486, 11, 0, '', '', 'Triumph! Ihr habt Eure privaten Reichtümer an Euch gebracht!$B$B|TInterface\\Icons\\INV_Misc_coin_02.blp:64|t', '', '', '', '', ''),
(36600, 0, 0, '', '', 'Das ist $n?! Entschuldigt, Doktor, ich dachte, $g er : sie; sei tot!', '', '', '', '', ''),
(36600, 1, 0, '', '', 'Ja, da ist noch ein Haufen Leute in den Fluchtkapseln gefangen, Boss. Oh, wartet. Ich vermute, Ihr seid nicht mehr der Boss. Wie dem auch sei, sie werden wohl alle sterben, wenn Ihr sie nicht herausholt.', '', '', '', '', ''),
(36608, 0, 0, '', '', 'Gizmo, was sitzt Ihr da herum? Erkennt Ihr nicht, wer da neben Euch liegt?!', '', '', '', '', ''),
(36608, 1, 0, '', '', 'Das ist $n! $G Er: Sie; ist der alleinige Grund, weshalb wir noch atmen und nicht knusprig gebraten auf Kezan sind.', '', '', '', '', ''),
(36608, 2, 0, '', '', 'Tretet zurück, ich werde $g ihn : sie; wiederbeleben! Ich hoffe, diese nassen Überbrückungskabel bringen uns nicht alle um!', '', '', '', '', ''),
(36608, 3, 0, '', '', 'Kommt schon! Bleibt weg!', '', '', '', '', ''),
(36608, 4, 0, '', '', 'Das ist alles, was ich tun kann. Es liegt nun an $g ihm : ihr;. Hört Ihr mich, $n? Kommt schon, kriegt Euch wieder ein! Geht nicht ins Licht!', '', '', '', '', ''),
(36608, 5, 0, '', '', 'Ihr habt die richtige Wahl getroffen. Wir alle stehen tief in Eurer Schuld, $n. Versucht Euch hier draußen nicht töten zu lassen.', '', '', '', '', ''),
(36608, 6, 0, '', '', 'Es gibt noch mehr Überlebende, um die ich mich kümmern muss. Wir sehen uns an der Küste.', '', '', '', '', ''),
(37106, 0, 0, '', '', 'Ihr müsst Euch beeilen, Kleines. Wir müssen Euch ins Spiel bringen. Aber zunächst müssen wir die Ersatzteile für den Schredder besorgen!', '', '', '', '', ''),
(37106, 1, 0, '', '', 'Setzt Euch in den Schredder und gewinnt das Spiel. Das Bilgewasserkartell zählt auf Euch!', '', '', '', '', ''),
(37106, 2, 0, '', '', 'Ihr habt Mumm, Kleines, so viel ist sicher! Zu blöd, dass der Drache angekommen ist und alles ruiniert hat. Ist aber auch egal, Ihr habt uns stolz gemacht. Und jetzt zurück mit Euch zum Hauptquartier.', '', '', '', '', ''),
(37179, 0, 0, '', '', 'Werft Eure Fußbomben auf diese Dampfdruckhaie!', '', '', '', '', ''),
(37213, 0, 0, '', '', 'Schießt die Fußbombe mit einem Tritt zwischen die Schornsteine und hinter das gegnerische Tor!', '', '', '', '', ''),
(37500, 0, 0, '', '', 'Vulkangestein! Zeitlich begrenztes Angebot! Echtheitszeugnis und alles drum und dran!', '', '', '', '', ''),
(37500, 1, 0, '', '', 'Vulkangestein zu verkaufen! Frisch vom Berg! Ihr wollt sie, ich habe sie!', '', '', '', '', ''),
(37500, 2, 0, '', '', 'Holt Euch Euer Vulkangestein hier! Großartig für Katzen!', '', '', '', '', ''),
(37602, 0, 0, '', '', 'Noch ein fehlerhafter-elektrischer-entflammbarer Gas-Bett-Feuerwerk-Unfall?!', '', '', '', '', ''),
(37680, 0, 0, '', '', 'Wir müssen drum herumgehen.', '', '', '', '', ''),
(37680, 1, 0, '', '', 'AUS DEM WEG!', '', '', '', '', ''),
(37680, 2, 0, '', '', 'So, wir sind gesund und... OH NEIN! Man hat uns betrogen!', '', '', '', '', ''),
(37804, 0, 0, '', '', 'Ganz neue Kaja''Cola! Jetzt mit 100 Prozent mehr Ideen!', '', '', '', '', ''),
(37804, 1, 0, '', '', 'Nehmt teil am Kaja''Cola-Geschmackstest-Wettbewerb!', '', '', '', '', ''),
(37804, 2, 0, '', '', 'Kaja''Cola! Hier kommen die IDEEN!', '', '', '', '', ''),
(48494, 0, 0, '', '', 'Bereitet das Mikromechahuhn vor!', '', '', '', '', ''),
(48494, 1, 0, '', '', 'Es gibt eine ganze Menge, was Ihr nicht versteht, meine liebe Greely. Darum seid Ihr ja auch die Assistentin und ich... ich bin Hobart Wurfhammer!', '', '', '', '', ''),
(48494, 2, 0, '', '', 'Der Himmel fällt uns auf den Kopf!', '', '', '', '', ''),
(48494, 3, 0, '', '', 'Packt alles zusammen! Wir müssen einen Weg finden, wie wir von Kezan wegkommen! Wir springen in die nächstgelegene Instastadt! Sie werden sie an Bord der Yacht des Handelsprinzen bringen.', '', '', '', '', ''),
(48496, 0, 0, '', '', 'Ihr müsst nicht so schreien, Hobart, ich bin doch direkt hier. Pssst!', '', '', '', '', ''),
(48496, 1, 0, '', '', 'Da, bitte. Ein Mikromechahuhn. Ich werde nie verstehen, wie Ihr auf diese Namen kommt.', '', '', '', '', ''),
(48496, 2, 0, '', '', 'Ja, ja, ich kümmere mich drum. Aber, Hobart, Ihr müsst Euch beruhigen. Euer Blutdruck! Ihr lauft ja schon dunkelgrün an!', '', '', '', '', ''),
(48572, 0, 0, '', '', 'Die Sonne geht unter für diese sterbliche Welt, Ihr Narren. Macht Euren Frieden mit Eurem Ende, denn die Stunde des Zwielichts ist gekommen!', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=39169 /*39169*/ AND `locale`='deDE') OR (`entry`=36608 /*36608*/ AND `locale`='deDE') OR (`entry`=36735 /*36735*/ AND `locale`='deDE') OR (`entry`=36719 /*36719*/ AND `locale`='deDE') OR (`entry`=36600 /*36600*/ AND `locale`='deDE') OR (`entry`=34763 /*34763*/ AND `locale`='deDE') OR (`entry`=37683 /*37683*/ AND `locale`='deDE') OR (`entry`=49218 /*49218*/ AND `locale`='deDE') OR (`entry`=37680 /*37680*/ AND `locale`='deDE') OR (`entry`=37676 /*37676*/ AND `locale`='deDE') OR (`entry`=37682 /*37682*/ AND `locale`='deDE') OR (`entry`=37602 /*37602*/ AND `locale`='deDE') OR (`entry`=37599 /*37599*/ AND `locale`='deDE') OR (`entry`=37598 /*37598*/ AND `locale`='deDE') OR (`entry`=37594 /*37594*/ AND `locale`='deDE') OR (`entry`=37590 /*37590*/ AND `locale`='deDE') OR (`entry`=37561 /*37561*/ AND `locale`='deDE') OR (`entry`=48925 /*48925*/ AND `locale`='deDE') OR (`entry`=35613 /*35613*/ AND `locale`='deDE') OR (`entry`=51411 /*51411*/ AND `locale`='deDE') OR (`entry`=48961 /*48961*/ AND `locale`='deDE') OR (`entry`=48949 /*48949*/ AND `locale`='deDE') OR (`entry`=35609 /*35609*/ AND `locale`='deDE') OR (`entry`=35486 /*35486*/ AND `locale`='deDE') OR (`entry`=37500 /*37500*/ AND `locale`='deDE') OR (`entry`=35234 /*35234*/ AND `locale`='deDE') OR (`entry`=35294 /*35294*/ AND `locale`='deDE') OR (`entry`=23837 /*23837*/ AND `locale`='deDE') OR (`entry`=43359 /*43359*/ AND `locale`='deDE') OR (`entry`=35236 /*35236*/ AND `locale`='deDE') OR (`entry`=37748 /*37748*/ AND `locale`='deDE') OR (`entry`=37710 /*37710*/ AND `locale`='deDE') OR (`entry`=37709 /*37709*/ AND `locale`='deDE') OR (`entry`=37708 /*37708*/ AND `locale`='deDE') OR (`entry`=49338 /*49338*/ AND `locale`='deDE') OR (`entry`=49339 /*49339*/ AND `locale`='deDE') OR (`entry`=35238 /*35238*/ AND `locale`='deDE') OR (`entry`=35235 /*35235*/ AND `locale`='deDE') OR (`entry`=35237 /*35237*/ AND `locale`='deDE') OR (`entry`=35212 /*35212*/ AND `locale`='deDE') OR (`entry`=35213 /*35213*/ AND `locale`='deDE') OR (`entry`=35211 /*35211*/ AND `locale`='deDE') OR (`entry`=48806 /*48806*/ AND `locale`='deDE') OR (`entry`=48805 /*48805*/ AND `locale`='deDE') OR (`entry`=35202 /*35202*/ AND `locale`='deDE') OR (`entry`=35200 /*35200*/ AND `locale`='deDE') OR (`entry`=35209 /*35209*/ AND `locale`='deDE') OR (`entry`=35210 /*35210*/ AND `locale`='deDE') OR (`entry`=35207 /*35207*/ AND `locale`='deDE') OR (`entry`=35185 /*35185*/ AND `locale`='deDE') OR (`entry`=48719 /*48719*/ AND `locale`='deDE') OR (`entry`=35201 /*35201*/ AND `locale`='deDE') OR (`entry`=35186 /*35186*/ AND `locale`='deDE') OR (`entry`=48721 /*48721*/ AND `locale`='deDE') OR (`entry`=35175 /*35175*/ AND `locale`='deDE') OR (`entry`=35126 /*35126*/ AND `locale`='deDE') OR (`entry`=35120 /*35120*/ AND `locale`='deDE') OR (`entry`=48984 /*48984*/ AND `locale`='deDE') OR (`entry`=48572 /*48572*/ AND `locale`='deDE') OR (`entry`=37203 /*37203*/ AND `locale`='deDE') OR (`entry`=37213 /*37213*/ AND `locale`='deDE') OR (`entry`=35222 /*35222*/ AND `locale`='deDE') OR (`entry`=37114 /*37114*/ AND `locale`='deDE') OR (`entry`=37179 /*37179*/ AND `locale`='deDE') OR (`entry`=35128 /*35128*/ AND `locale`='deDE') OR (`entry`=34877 /*34877*/ AND `locale`='deDE') OR (`entry`=34878 /*34878*/ AND `locale`='deDE') OR (`entry`=34876 /*34876*/ AND `locale`='deDE') OR (`entry`=48526 /*48526*/ AND `locale`='deDE') OR (`entry`=37106 /*37106*/ AND `locale`='deDE') OR (`entry`=35130 /*35130*/ AND `locale`='deDE') OR (`entry`=34957 /*34957*/ AND `locale`='deDE') OR (`entry`=37054 /*37054*/ AND `locale`='deDE') OR (`entry`=37057 /*37057*/ AND `locale`='deDE') OR (`entry`=37056 /*37056*/ AND `locale`='deDE') OR (`entry`=37055 /*37055*/ AND `locale`='deDE') OR (`entry`=37046 /*37046*/ AND `locale`='deDE') OR (`entry`=49133 /*49133*/ AND `locale`='deDE') OR (`entry`=6827 /*6827*/ AND `locale`='deDE') OR (`entry`=49131 /*49131*/ AND `locale`='deDE') OR (`entry`=49774 /*49774*/ AND `locale`='deDE') OR (`entry`=34892 /*34892*/ AND `locale`='deDE') OR (`entry`=34958 /*34958*/ AND `locale`='deDE') OR (`entry`=49772 /*49772*/ AND `locale`='deDE') OR (`entry`=35075 /*35075*/ AND `locale`='deDE') OR (`entry`=35063 /*35063*/ AND `locale`='deDE') OR (`entry`=34954 /*34954*/ AND `locale`='deDE') OR (`entry`=34959 /*34959*/ AND `locale`='deDE') OR (`entry`=34840 /*34840*/ AND `locale`='deDE') OR (`entry`=34890 /*34890*/ AND `locale`='deDE') OR (`entry`=2110 /*2110*/ AND `locale`='deDE') OR (`entry`=35239 /*35239*/ AND `locale`='deDE') OR (`entry`=34830 /*34830*/ AND `locale`='deDE') OR (`entry`=89713 /*89713*/ AND `locale`='deDE') OR (`entry`=34865 /*34865*/ AND `locale`='deDE') OR (`entry`=35304 /*35304*/ AND `locale`='deDE') OR (`entry`=48519 /*48519*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(39169, 'deDE', 'Postvogel der Verlorenen Inseln', NULL, NULL, NULL, 23420), -- 39169
(36608, 'deDE', 'Doc Knalldüse', NULL, NULL, NULL, 23420), -- 36608
(36735, 'deDE', 'Teraptormatriarchin', NULL, NULL, NULL, 23420), -- 36735
(36719, 'deDE', 'Pterrordaxstrauchdieb', NULL, NULL, NULL, 23420), -- 36719
(36600, 'deDE', 'Gizmo Gangzwinger', NULL, NULL, NULL, 23420), -- 36600
(34763, 'deDE', 'Nadelzahnhai', NULL, NULL, NULL, 23420), -- 34763
(37683, 'deDE', 'Villascherge', NULL, NULL, NULL, 23420), -- 37683
(49218, 'deDE', 'Villascherge', NULL, NULL, NULL, 23420), -- 49218
(37680, 'deDE', 'Sassy Hartzang', NULL, 'Eure Vorstandsassistentin', NULL, 23420), -- 37680
(37676, 'deDE', 'Hot Rod', NULL, NULL, NULL, 23420), -- 37676
(37682, 'deDE', '447 Fireworks Bunny', NULL, NULL, NULL, 23420), -- 37682
(37602, 'deDE', 'Schadensreferent', NULL, 'Lorenhallversicherungen GmbH', NULL, 23420), -- 37602
(37599, 'deDE', 'Benzinbot', NULL, NULL, NULL, 23420), -- 37599
(37598, 'deDE', 'Benzinbot', NULL, NULL, NULL, 23420), -- 37598
(37594, 'deDE', 'Schwelendes Bett', NULL, NULL, NULL, 23420), -- 37594
(37590, 'deDE', 'Ofenleck', NULL, NULL, NULL, 23420), -- 37590
(37561, 'deDE', 'Überladener Generator', NULL, NULL, NULL, 23420), -- 37561
(48925, 'deDE', 'Schergenverkleidung', NULL, NULL, NULL, 23420), -- 48925
(35613, 'deDE', 'Spürnasenhängebauchschwein', NULL, NULL, NULL, 23420), -- 35613
(51411, 'deDE', 'Neill Penny', NULL, NULL, NULL, 23420), -- 51411
(48961, 'deDE', 'Bourgeois von Kezan', NULL, NULL, NULL, 23420), -- 48961
(48949, 'deDE', 'Bourgeois von Kezan', NULL, NULL, NULL, 23420), -- 48949
(35609, 'deDE', 'Villascherge', NULL, NULL, NULL, 23420), -- 35609
(35486, 'deDE', 'Tresor der Ersten Bank von Kezan', NULL, NULL, NULL, 23420), -- 35486
(37500, 'deDE', 'Vinny Spalthieb', NULL, 'Unternehmer', NULL, 23420), -- 37500
(35234, 'deDE', 'Angeheuerter Plünderer', 'Angeheuerte Plünderin', NULL, NULL, 23420), -- 35234
(35294, 'deDE', 'Rebellischer Troll', NULL, NULL, NULL, 23420), -- 35294
(23837, 'deDE', 'ELM General Purpose Bunny', NULL, NULL, NULL, 23420), -- 23837
(43359, 'deDE', 'ELM General Purpose Bunny Infinite Hide Body', NULL, NULL, NULL, 23420), -- 43359
(35236, 'deDE', 'Kezanischer Feiernder', NULL, NULL, NULL, 23420), -- 35236
(37748, 'deDE', 'A Bazillion Macaroons Fiery Boulder Caster', NULL, NULL, NULL, 23420), -- 37748
(37710, 'deDE', 'Gobber', NULL, NULL, NULL, 23420), -- 37710
(37709, 'deDE', 'Das Ass', NULL, NULL, NULL, 23420), -- 37709
(37708, 'deDE', 'Izzy', NULL, NULL, NULL, 23420), -- 37708
(49338, 'deDE', 'Kellnerin der HGK', NULL, NULL, NULL, 23420), -- 49338
(49339, 'deDE', 'Kellner der HGK', NULL, NULL, NULL, 23420), -- 49339
(35238, 'deDE', 'Kezanischer Feiernder', NULL, NULL, NULL, 23420), -- 35238
(35235, 'deDE', 'Kezanischer Feiernder', NULL, NULL, NULL, 23420), -- 35235
(35237, 'deDE', 'Ungeladener Pirat', 'Ungeladene Piratin', 'Südmeerfreibeuter', NULL, 23420), -- 35237
(35212, 'deDE', 'Gobber', NULL, NULL, NULL, 23420), -- 35212
(35213, 'deDE', 'Izzy', NULL, NULL, NULL, 23420), -- 35213
(35211, 'deDE', 'Das Ass', NULL, NULL, NULL, 23420), -- 35211
(48806, 'deDE', 'Kellnerin der HGK', NULL, NULL, NULL, 23420), -- 48806
(48805, 'deDE', 'Kellner der HGK', NULL, NULL, NULL, 23420), -- 48805
(35202, 'deDE', 'Kezanischer Feiernder', NULL, NULL, NULL, 23420), -- 35202
(35200, 'deDE', 'Ungeladener Pirat', 'Ungeladene Piratin', 'Südmeerfreibeuter', NULL, 23420), -- 35200
(35209, 'deDE', 'Gobber', NULL, NULL, NULL, 23420), -- 35209
(35210, 'deDE', 'Izzy', NULL, NULL, NULL, 23420), -- 35210
(35207, 'deDE', 'Das Ass', NULL, NULL, NULL, 23420), -- 35207
(35185, 'deDE', 'Kezanischer Feiernder', 'Kezanische Feiernde', NULL, NULL, 23420), -- 35185
(48719, 'deDE', 'Kellner der HGK', NULL, NULL, NULL, 23420), -- 48719
(35201, 'deDE', 'Kezanischer Feiernder', 'Kezanische Feiernde', NULL, NULL, 23420), -- 35201
(35186, 'deDE', 'Kezanischer Feiernder', NULL, NULL, NULL, 23420), -- 35186
(48721, 'deDE', 'Kellnerin der HGK', NULL, NULL, NULL, 23420), -- 48721
(35175, 'deDE', 'Kezanischer Feiernder', NULL, NULL, NULL, 23420), -- 35175
(35126, 'deDE', 'Gappy Silberzahn', NULL, 'Klunkerhändler', NULL, 23420), -- 35126
(35120, 'deDE', 'Kassierer der EBvK', NULL, NULL, NULL, 23420), -- 35120
(48984, 'deDE', 'Trolltagelöhner', NULL, NULL, NULL, 23420), -- 48984
(48572, 'deDE', 'Todesschwinge', NULL, 'Aspekt des Todes', NULL, 23420), -- 48572
(37203, 'deDE', 'Fourth And Goal Target', NULL, NULL, NULL, 23420), -- 37203
(37213, 'deDE', 'Bilgewasserbukanier', NULL, NULL, NULL, 23420), -- 37213
(35222, 'deDE', 'Handelsprinz Gallywix', NULL, 'Anführer des Bilgewasserkartells', NULL, 23420), -- 35222
(37114, 'deDE', 'Dampfdruckhai', NULL, NULL, NULL, 23420), -- 37114
(37179, 'deDE', 'Bilgewasserbukanier', NULL, NULL, NULL, 23420), -- 37179
(35128, 'deDE', 'Szabo', NULL, 'Maßschneider', NULL, 23420), -- 35128
(34877, 'deDE', 'Präz der Lufthammer', NULL, NULL, NULL, 23420), -- 34877
(34878, 'deDE', 'Sudsy Magee', NULL, NULL, NULL, 23420), -- 34878
(34876, 'deDE', 'Frankie Gangschalter', NULL, NULL, NULL, 23420), -- 34876
(48526, 'deDE', 'Bilgewasserbukanier', NULL, NULL, 'vehichleCursor', 23420), -- 48526
(37106, 'deDE', 'Trainer Blutgrätsch', NULL, 'Die Bilgewasserbukaniere', NULL, 23420), -- 37106
(35130, 'deDE', 'Missa Gläsa', NULL, 'Sonnenbrillenhändlerin', NULL, 23420), -- 35130
(34957, 'deDE', 'Das Ass', NULL, NULL, NULL, 23420), -- 34957
(37054, 'deDE', 'Goblinsupermodel', NULL, 'Unglaublich gutaussehend', NULL, 23420), -- 37054
(37057, 'deDE', 'Goblinsupermodel', NULL, 'Unglaublich gutaussehend', NULL, 23420), -- 37057
(37056, 'deDE', 'Goblinsupermodel', NULL, 'Unglaublich gutaussehend', NULL, 23420), -- 37056
(37055, 'deDE', 'Goblinsupermodel', NULL, 'Unglaublich gutaussehend', NULL, 23420), -- 37055
(37046, 'deDE', 'ELM General Purpose Bunny (Not Floating)', NULL, NULL, NULL, 23420), -- 37046
(49133, 'deDE', 'Episches goblinisches Trike', NULL, NULL, NULL, 23420), -- 49133
(6827, 'deDE', 'Strandkrebs', NULL, NULL, NULL, 23420), -- 6827
(49131, 'deDE', 'Goblinischer Hot Rod', NULL, NULL, NULL, 23420), -- 49131
(49774, 'deDE', 'Tollwütiger Nussschädling 5000', NULL, NULL, NULL, 23420), -- 49774
(34892, 'deDE', 'Das Ass', NULL, NULL, NULL, 23420), -- 34892
(34958, 'deDE', 'Gobber', NULL, NULL, NULL, 23420), -- 34958
(49772, 'deDE', 'Mümmelbot', NULL, NULL, NULL, 23420), -- 49772
(35075, 'deDE', 'Bürgerin von Kezan', NULL, NULL, NULL, 23420), -- 35075
(35063, 'deDE', 'Bürger von Kezan', NULL, NULL, NULL, 23420), -- 35063
(34954, 'deDE', 'Gobber', NULL, NULL, NULL, 23420), -- 34954
(34959, 'deDE', 'Izzy', NULL, NULL, NULL, 23420), -- 34959
(34840, 'deDE', 'Hot Rod', NULL, NULL, NULL, 23420), -- 34840
(34890, 'deDE', 'Izzy', NULL, NULL, NULL, 23420), -- 34890
(2110, 'deDE', 'Schwarze Ratte', NULL, NULL, NULL, 23420), -- 2110
(35239, 'deDE', 'Trollsklave', NULL, NULL, NULL, 23420), -- 35239
(34830, 'deDE', 'Widerspenstiger Troll', NULL, NULL, 'questinteract', 23420), -- 34830
(89713, 'deDE', 'Koak Hoburn', NULL, 'Chauffeur', NULL, 23420), -- 89713
(34865, 'deDE', 'Tunnelwurm', NULL, NULL, NULL, 23420), -- 34865
(35304, 'deDE', 'Brutaler Vollstrecker', NULL, NULL, NULL, 23420), -- 35304
(48519, 'deDE', 'Mikromechahuhn', NULL, NULL, NULL, 23420); -- 48519

INSERT IGNORE INTO `creature_template` (`entry`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `femaleName`, `subname`, `IconName`, `HealthScalingExpansion`, `RequiredExpansion`, `VignetteID`, `rank`, `family`, `type`, `type_flags`, `type_flags2`, `HealthModifier`, `ManaModifier`, `RacialLeader`, `movementId`, `VerifiedBuild`) VALUES
(39169, 0, 0, 31344, 0, 0, 0, 'Postvogel der Verlorenen Inseln', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 32, 0, 1, 1, 0, 121, 23420), -- 39169
(36608, 0, 0, 30227, 0, 0, 0, 'Doc Knalldüse', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 36608
(36735, 0, 0, 30303, 0, 0, 0, 'Teraptormatriarchin', NULL, NULL, NULL, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 121, 23420), -- 36735
(36719, 0, 0, 8410, 0, 0, 0, 'Pterrordaxstrauchdieb', NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 162, 23420), -- 36719
(36600, 0, 0, 30219, 0, 0, 0, 'Gizmo Gangzwinger', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 36600
(34763, 0, 0, 1557, 12199, 12200, 0, 'Nadelzahnhai', NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 121, 23420), -- 34763
(37683, 0, 0, 30262, 0, 0, 0, 'Villascherge', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 37683
(49218, 0, 0, 30262, 0, 0, 0, 'Villascherge', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 49218
(37680, 0, 0, 29464, 0, 0, 0, 'Sassy Hartzang', NULL, 'Eure Vorstandsassistentin', NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 37680
(37676, 0, 0, 31988, 0, 0, 0, 'Hot Rod', NULL, NULL, NULL, 0, 0, 0, 0, 0, 9, 0, 0, 1, 1, 0, 641, 23420), -- 37676
(37682, 0, 0, 1126, 16946, 0, 0, '447 Fireworks Bunny', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 1024, 0, 1, 1, 0, 0, 23420), -- 37682
(37602, 0, 0, 30703, 0, 0, 0, 'Schadensreferent', NULL, 'Lorenhallversicherungen GmbH', NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 37602
(37599, 0, 0, 17200, 0, 0, 0, 'Benzinbot', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 1024, 0, 1, 1, 0, 0, 23420), -- 37599
(37598, 0, 0, 30702, 0, 0, 0, 'Benzinbot', NULL, NULL, NULL, 0, 0, 0, 0, 0, 9, 0, 0, 1, 1, 0, 121, 23420), -- 37598
(37594, 0, 0, 11686, 0, 0, 0, 'Schwelendes Bett', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 1024, 0, 1, 1, 0, 0, 23420), -- 37594
(37590, 0, 0, 11686, 0, 0, 0, 'Ofenleck', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 1024, 0, 1, 1, 0, 0, 23420), -- 37590
(37561, 0, 0, 11686, 0, 0, 0, 'Überladener Generator', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 1024, 0, 1, 1, 0, 0, 23420), -- 37561
(48925, 0, 0, 30262, 0, 0, 0, 'Schergenverkleidung', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 17826816, 0, 1, 1, 0, 0, 23420), -- 48925
(35613, 0, 0, 26685, 0, 0, 0, 'Spürnasenhängebauchschwein', NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 23420), -- 35613
(51411, 0, 0, 37365, 0, 0, 0, 'Neill Penny', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 51411
(48961, 0, 0, 36346, 36347, 36348, 36349, 'Bourgeois von Kezan', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 48961
(48949, 0, 0, 36350, 36351, 36352, 36353, 'Bourgeois von Kezan', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 48949
(35609, 0, 0, 30262, 0, 0, 0, 'Villascherge', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35609
(35486, 0, 0, 29827, 0, 0, 0, 'Tresor der Ersten Bank von Kezan', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 1024, 0, 1, 0.833333, 0, 0, 23420), -- 35486
(37500, 0, 0, 30622, 0, 0, 0, 'Vinny Spalthieb', NULL, 'Unternehmer', NULL, 0, 0, 0, 0, 0, 7, 134217728, 0, 1, 1, 0, 0, 23420), -- 37500
(35234, 0, 0, 29676, 29677, 29678, 374, 'Angeheuerter Plünderer', 'Angeheuerte Plünderin', NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35234
(35294, 0, 0, 31741, 31742, 31743, 31744, 'Rebellischer Troll', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35294
(23837, 0, 0, 1126, 11686, 0, 0, 'ELM General Purpose Bunny', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 1024, 0, 1, 1, 0, 0, 23420), -- 23837
(43359, 0, 0, 1126, 11686, 0, 0, 'ELM General Purpose Bunny Infinite Hide Body', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 1024, 0, 1, 1, 0, 0, 23420), -- 43359
(35236, 0, 0, 19725, 21342, 0, 0, 'Kezanischer Feiernder', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35236
(37748, 0, 0, 1126, 11686, 0, 0, 'A Bazillion Macaroons Fiery Boulder Caster', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 1056, 0, 1, 1, 0, 0, 23420), -- 37748
(37710, 0, 0, 32385, 0, 0, 0, 'Gobber', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 37710
(37709, 0, 0, 29495, 0, 0, 0, 'Das Ass', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 37709
(37708, 0, 0, 29482, 0, 0, 0, 'Izzy', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 37708
(49338, 0, 0, 36244, 36245, 36246, 36247, 'Kellnerin der HGK', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 49338
(49339, 0, 0, 36240, 36241, 36242, 36243, 'Kellner der HGK', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 49339
(35238, 0, 0, 19725, 21342, 0, 0, 'Kezanischer Feiernder', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35238
(35235, 0, 0, 19725, 21342, 0, 0, 'Kezanischer Feiernder', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35235
(35237, 0, 0, 29676, 29677, 29678, 374, 'Ungeladener Pirat', 'Ungeladene Piratin', 'Südmeerfreibeuter', NULL, 0, 0, 0, 0, 0, 7, 1024, 0, 1, 1, 0, 0, 23420), -- 35237
(35212, 0, 0, 30263, 0, 0, 0, 'Gobber', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35212
(35213, 0, 0, 29482, 0, 0, 0, 'Izzy', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35213
(35211, 0, 0, 29495, 0, 0, 0, 'Das Ass', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35211
(48806, 0, 0, 36244, 36245, 36246, 36247, 'Kellnerin der HGK', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 48806
(48805, 0, 0, 36240, 36241, 36242, 36243, 'Kellner der HGK', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 48805
(35202, 0, 0, 19725, 21342, 0, 0, 'Kezanischer Feiernder', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35202
(35200, 0, 0, 29676, 29677, 29678, 374, 'Ungeladener Pirat', 'Ungeladene Piratin', 'Südmeerfreibeuter', NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35200
(35209, 0, 0, 32385, 0, 0, 0, 'Gobber', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35209
(35210, 0, 0, 29482, 0, 0, 0, 'Izzy', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35210
(35207, 0, 0, 29495, 0, 0, 0, 'Das Ass', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35207
(35185, 0, 0, 29662, 29663, 29664, 29665, 'Kezanischer Feiernder', 'Kezanische Feiernde', NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35185
(48719, 0, 0, 36240, 36241, 36242, 36243, 'Kellner der HGK', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 48719
(35201, 0, 0, 29666, 29631, 29664, 29663, 'Kezanischer Feiernder', 'Kezanische Feiernde', NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35201
(35186, 35175, 0, 29631, 29633, 29668, 29669, 'Kezanischer Feiernder', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35186
(48721, 0, 0, 36244, 36245, 36246, 36247, 'Kellnerin der HGK', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 48721
(35175, 0, 0, 29630, 29632, 29666, 29667, 'Kezanischer Feiernder', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35175
(35126, 0, 0, 29620, 0, 0, 0, 'Gappy Silberzahn', NULL, 'Klunkerhändler', NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35126
(35120, 0, 0, 29617, 0, 0, 0, 'Kassierer der EBvK', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35120
(48984, 0, 0, 31745, 31746, 31747, 31748, 'Trolltagelöhner', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 32, 0, 1, 1, 0, 0, 23420), -- 48984
(48572, 0, 0, 32809, 0, 0, 0, 'Todesschwinge', NULL, 'Aspekt des Todes', NULL, -1, 0, 0, 1, 0, 2, 524324, 0, 1400, 1400, 0, 0, 23420), -- 48572
(37203, 0, 0, 31296, 31295, 0, 0, 'Fourth And Goal Target', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 1572864, 0, 1, 1, 0, 0, 23420), -- 37203
(37213, 0, 0, 26559, 0, 0, 0, 'Bilgewasserbukanier', NULL, NULL, NULL, 0, 0, 0, 0, 0, 9, 0, 0, 1, 1, 0, 0, 23420), -- 37213
(35222, 0, 0, 29680, 0, 0, 0, 'Handelsprinz Gallywix', NULL, 'Anführer des Bilgewasserkartells', NULL, 0, 0, 0, 1, 0, 7, 4, 0, 10, 10, 0, 0, 23420), -- 35222
(37114, 0, 0, 1303, 0, 0, 0, 'Dampfdruckhai', NULL, NULL, NULL, 0, 0, 0, 0, 0, 9, 524288, 0, 1, 1, 0, 0, 23420), -- 37114
(37179, 0, 0, 26559, 0, 0, 0, 'Bilgewasserbukanier', NULL, NULL, NULL, 0, 0, 0, 0, 0, 9, 0, 0, 1, 1, 0, 0, 23420), -- 37179
(35128, 0, 0, 29621, 0, 0, 0, 'Szabo', NULL, 'Maßschneider', NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35128
(34877, 0, 0, 30262, 0, 0, 0, 'Präz der Lufthammer', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 2147483648, 0, 2, 1, 0, 0, 23420), -- 34877
(34878, 0, 0, 29474, 0, 0, 0, 'Sudsy Magee', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 2147483648, 0, 2, 1, 0, 0, 23420), -- 34878
(34876, 0, 0, 29473, 0, 0, 0, 'Frankie Gangschalter', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 2147483648, 0, 2, 1, 0, 0, 23420), -- 34876
(48526, 0, 0, 26559, 0, 0, 0, 'Bilgewasserbukanier', NULL, NULL, 'vehichleCursor', 0, 0, 0, 0, 0, 9, 0, 0, 1, 1, 0, 0, 23420), -- 48526
(37106, 0, 0, 30513, 0, 0, 0, 'Trainer Blutgrätsch', NULL, 'Die Bilgewasserbukaniere', NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 37106
(35130, 0, 0, 29622, 0, 0, 0, 'Missa Gläsa', NULL, 'Sonnenbrillenhändlerin', NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35130
(34957, 0, 0, 29495, 0, 0, 0, 'Das Ass', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 32, 0, 1, 1, 0, 0, 23420), -- 34957
(37054, 0, 0, 30496, 0, 0, 0, 'Goblinsupermodel', NULL, 'Unglaublich gutaussehend', NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 37054
(37057, 0, 0, 30499, 0, 0, 0, 'Goblinsupermodel', NULL, 'Unglaublich gutaussehend', NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 37057
(37056, 0, 0, 30498, 0, 0, 0, 'Goblinsupermodel', NULL, 'Unglaublich gutaussehend', NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 37056
(37055, 0, 0, 30497, 0, 0, 0, 'Goblinsupermodel', NULL, 'Unglaublich gutaussehend', NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 37055
(37046, 0, 0, 1126, 11686, 0, 0, 'ELM General Purpose Bunny (Not Floating)', NULL, NULL, NULL, 0, 0, 0, 0, 0, 10, 1024, 0, 1, 1, 0, 0, 23420), -- 37046
(49133, 0, 0, 35250, 0, 0, 0, 'Episches goblinisches Trike', NULL, NULL, NULL, 0, 0, 0, 0, 0, 9, 0, 0, 1, 1, 0, 181, 23420), -- 49133
(6827, 0, 0, 32789, 32790, 32791, 45880, 'Strandkrebs', NULL, NULL, NULL, 0, 0, 0, 0, 0, 8, 0, 0, 1, 1, 0, 100, 23420), -- 6827
(49131, 0, 0, 31988, 0, 0, 0, 'Goblinischer Hot Rod', NULL, NULL, NULL, 0, 0, 0, 0, 0, 9, 0, 0, 1, 1, 0, 181, 23420), -- 49131
(49774, 0, 0, 26532, 0, 0, 0, 'Tollwütiger Nussschädling 5000', NULL, NULL, NULL, 0, 0, 0, 0, 0, 8, 0, 0, 0.01, 1, 0, 100, 23420), -- 49774
(34892, 0, 0, 29495, 0, 0, 0, 'Das Ass', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 34892
(34958, 0, 0, 32385, 0, 0, 0, 'Gobber', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 32, 0, 1, 1, 0, 0, 23420), -- 34958
(49772, 0, 0, 33559, 0, 0, 0, 'Mümmelbot', NULL, NULL, NULL, 0, 0, 0, 0, 0, 8, 0, 0, 0.01, 1, 0, 100, 23420), -- 49772
(35075, 0, 0, 29564, 29566, 29586, 29587, 'Bürgerin von Kezan', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35075
(35063, 0, 0, 29563, 29565, 29584, 29585, 'Bürger von Kezan', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35063
(34954, 0, 0, 32385, 0, 0, 0, 'Gobber', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 34954
(34959, 0, 0, 29482, 0, 0, 0, 'Izzy', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 32, 0, 1, 1, 0, 0, 23420), -- 34959
(34840, 0, 0, 31988, 0, 0, 0, 'Hot Rod', NULL, NULL, NULL, 0, 0, 0, 0, 0, 9, 40, 0, 1, 1, 0, 181, 23420), -- 34840
(34890, 0, 0, 29482, 0, 0, 0, 'Izzy', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 34890
(2110, 0, 0, 1141, 0, 0, 0, 'Schwarze Ratte', NULL, NULL, NULL, 0, 0, 0, 0, 0, 8, 0, 0, 0.01, 1, 0, 88, 23420), -- 2110
(35239, 0, 0, 31745, 31746, 31747, 31748, 'Trollsklave', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35239
(34830, 0, 0, 31741, 31742, 31743, 31744, 'Widerspenstiger Troll', NULL, NULL, 'questinteract', 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 34830
(89713, 0, 0, 61763, 0, 0, 0, 'Koak Hoburn', NULL, 'Chauffeur', NULL, -1, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 89713
(34865, 0, 0, 36101, 0, 0, 0, 'Tunnelwurm', NULL, NULL, NULL, 0, 0, 0, 0, 42, 4, 0, 0, 1, 1, 0, 0, 23420), -- 34865
(35304, 0, 0, 30244, 30261, 0, 0, 'Brutaler Vollstrecker', NULL, NULL, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 1, 1, 0, 0, 23420), -- 35304
(48519, 0, 0, 36372, 0, 0, 0, 'Mikromechahuhn', NULL, NULL, NULL, 0, 0, 0, 0, 0, 9, 0, 0, 1, 1, 0, 121, 23420); -- 48519
