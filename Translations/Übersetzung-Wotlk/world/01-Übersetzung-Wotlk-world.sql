-- TrinityCore - WowPacketParser
-- File name: multi
-- Toaster's Sniffs
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/23/2017 14:28:29


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=11284 AND `locale`='deDE') OR (`ID`=13087 AND `locale`='deDE') OR (`ID`=43297 AND `locale`='deDE') OR (`ID`=43285 AND `locale`='deDE') OR (`ID`=43286 AND `locale`='deDE') OR (`ID`=43296 AND `locale`='deDE') OR (`ID`=12279 AND `locale`='deDE') OR (`ID`=12113 AND `locale`='deDE') OR (`ID`=13013 AND `locale`='deDE') OR (`ID`=43301 AND `locale`='deDE') OR (`ID`=43300 AND `locale`='deDE') OR (`ID`=43298 AND `locale`='deDE') OR (`ID`=43299 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(11284, 'deDE', 'Der Yeti von nebenan', 'Vorarbeiter Colbey bei Fort Wildervar möchte, dass Ihr Schmetterhorn besiegt.', 'Wenn die Schildwachen mir schon nicht helfen können, würdet Ihr? Ich bin der Vorarbeiter der Mine hinter der Brücke dort. Meine Männer schürfen Erz für die Waffen, die unsere Expeditionstruppe brauchen wird, um einen Pfad durch die Grizzlyhügel zu schlagen.$B$BEin riesiger Yeti ist in die Mine eingedrungen und blockiert den Tunnel, den wir brauchen, um weiter zu arbeiten! Wir haben versucht, ihn durch Schreie zu verscheuchen und aus seinem Unterschlupf zu vertreiben, aber nichts hat geholfen.$B$BVielleicht könntet Ihr ihn wecken, mit einer Portion Fleisch von den Frosthornböcken aus den schneebedeckten Gegenden um die Stadt herum? Bitte... Wir sind verzweifelt!', '', '', '', '', '', '', 23420),
(13087, 'deDE', 'Kochkunst des Nordens', 'Bringt 4 Stück kühles Fleisch zu Brom Bräugießer in Valgarde. Jede Bestie in Nordend verfügt über dieses Fleisch.', 'Also was machen wir denn da, wenn Ihr keinen Eintopf für mich habt? Ich hab'' HUNGER! Ihr könnt mir zumindest ein wenig kühles Fleisch bringen, damit ich mir selbst ''nen Eintopf machen kann! Wenn Ihr das macht, dann kann ich Euch ein Rezept geben, damit Ihr Euren eigenen machen könnt. JETZT GEHT UND BRINGT MIR EIN BISSCHEN FLEISCH!', '', '', '', '', '', '', 23420),
(43297, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23420),
(43285, 'deDE', 'Invasion: Vorgebirge des Hügellands', '', '', '', '', '', '', '', '', 23420),
(43286, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23420),
(43296, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23420),
(12279, 'deDE', 'Bärenhunger', 'Bringt Hugh Glass am Handelsposten von Rotholz 6 Nordlachse.', 'Zum Glück seid Ihr zurück, Hinkejoe! Ich hatte Sorge, dass Griselda und ich ganz allein sein würden.$b$bIch koche uns gerade eine schöne Mahlzeit, aber Griselda will davon nichts wissen.$b$bSie hätte gerne den Fisch, der sich an der Ostwindküste im Osten tummelt. Für mich ist der ja nichts, aber sie war schon immer ziemlich wählerisch. Sechs große Fische sollten eigentlich reichen, um sie glücklich zu machen.$b$bNun, rennt... ähh... hinkt los, Joe. Wir wollen das Mädchen doch nicht warten lassen, oder?', '', '', '', '', '', '', 23420),
(12113, 'deDE', 'Ohne Fleisch kein Preis', 'Harkor in Harkors Lager möchte, dass Ihr 10 Stück feinfasriges Worgfleisch von den streunenden Grauheulern und 10 Schaufelhornsteaks von den Langhufgrasfressern beschafft.', 'Guten Tag, $C! Wollt Ihr Euch nicht an mein Feuer setzen?$b$bIch würde Euch zwar eine gute, heiße Mahlzeit anbieten, nur habe ich keine. Ich sitze hier fest und warte auf einen Kumpanen, ansonsten würde ich mich selbst um die Sache kümmern.$b$bIhr scheint mehr als genug Zeit übrig zu haben und in dieser Gegend gibt es eine Menge Fleisch. Was würdet Ihr davon halten, wenn Ihr mir helfen und etwas zu Essen beschaffen würdet?', '', '', '', '', '', '', 23420),
(13013, 'deDE', 'Urahne Beldak', '', '', '', '', '', '', '', '', 23420),
(43301, 'deDE', 'Invasion: Azshara', '', '', '', '', '', '', '', '', 23420),
(43300, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23420),
(43298, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23420),
(43299, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23420);


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text    xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_offer_reward` WHERE `ID` IN (13087 /*13087*/, 13013 /*13013*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(13087, 0, 0, 0, 0, 0, 0, 0, 0, 'Du hast mein Fleisch! Hier ist dein Rezept - ich hab'' zu kochen!', 23420), -- 13087
(13013, 1, 0, 0, 0, 0, 0, 0, 0, 'Es ist gut zu wissen, dass die Bewohner dieser Welt noch immer die alten Völker verehren und sie nicht vergessen haben. Ich erbiete Euch meinen Respekt, $n. Nehmt dies als Zeichen meiner Dankbarkeit.', 23420); -- 13013


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text    xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_request_items` WHERE `ID`=13087;
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(13087, 0, 0, 0, 0, 'WO IST MEIN FLEISCH!', 23420); -- 13087


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=9478 AND `id`=1) OR (`menu_id`=9478 AND `id`=0) OR (`menu_id`=9010 AND `id`=1) OR (`menu_id`=8982 AND `id`=0) OR (`menu_id`=9985 AND `id`=0) OR (`menu_id`=8806 AND `id`=1) OR (`menu_id`=8806 AND `id`=0) OR (`menu_id`=8808 AND `id`=0) OR (`menu_id`=8803 AND `id`=0) OR (`menu_id`=9894 AND `id`=0) OR (`menu_id`=8802 AND `id`=0) OR (`menu_id`=6944 AND `id`=0) OR (`menu_id`=9484 AND `id`=1) OR (`menu_id`=342 AND `id`=1) OR (`menu_id`=342 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(9478, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9478, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9010, 1, '', '', 'Verkauft Ihr irgendwas von diesem Zeug?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8982, 0, '', '', 'Eh... was war das?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9985, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8806, 1, '', '', 'Ich möchte mir Eure Waren ansehen, Hazel.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8806, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8808, 0, '', '', 'Ich benötige einige Komponenten, Sorely.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8803, 0, '', '', 'Ich brauch ''nen Schnaps, Kahl.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9894, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8802, 0, '', '', 'Ich möchte ausgebildet werden.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6944, 0, '', '', 'Wohin kann ich fliegen?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9484, 1, '', '', 'Zeigt mir, was Ihr zu verkaufen habt, Fallensteller.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(342, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(342, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', '');


DELETE FROM `locales_creature_text` WHERE (`entry`=24027 AND `groupid`=0) OR(`entry`=28068 AND `groupid`=0) OR(`entry`=24719 AND `groupid`=0) OR(`entry`=26646 AND `groupid`=0) OR(`entry`=24014 AND `groupid`=0) OR(`entry`=24088 AND `groupid`=0) OR(`entry`=24023 AND `groupid`=0) OR(`entry`=24642 AND `groupid`=0) OR(`entry`=23732 AND `groupid`=0) OR(`entry`=24720 AND `groupid`=0) OR(`entry`=23967 AND `groupid`=0) OR(`entry`=23667 AND `groupid`=0) OR(`entry`=24718 AND `groupid`=0) OR(`entry`=23658 AND `groupid`=0) OR(`entry`=24176 AND `groupid`=0) OR(`entry`=23989 AND `groupid`=0) OR(`entry`=23963 AND `groupid`=0) OR(`entry`=26282 AND `groupid`=0) OR(`entry`=24717 AND `groupid`=0) OR(`entry`=26334 AND `groupid`=0);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(24027, 0, 0, '', '', 'Goth''  Kag Moth''aga ', '', '', '', '', ''),
(28068, 0, 0, '', '', 'Ich sehe Euch, Insekt! Kommt näher.', '', '', '', '', ''),
(24719, 0, 0, '', '', '%s schüttelt ihren Kopf.', '', '', '', '', ''),
(26646, 0, 0, '', '', 'Shur''nab... shur''nab... Yogg-Saron!', '', '', '', '', ''),
(24014, 0, 0, '', '', 'Es ist immer noch Platz für einen mehr.', '', '', '', '', ''),
(24088, 0, 0, '', '', 'Betet, dass ich nicht hier herauskomme, Abschaum.', '', '', '', '', ''),
(24023, 0, 0, '', '', 'Überlasst mir eure Seelen, auf dass ich den Feind vernichte!', '', '', '', '', ''),
(24642, 0, 0, '', '', '...und er sagt "Klar sehe ich nichts doppelt! Ich hab nur ein Auge!"', '', '', '', '', ''),
(23732, 0, 0, '', '', 'Maeshtro! Mushik ab!', '', '', '', '', ''),
(24720, 0, 0, '', '', '%s grummelt etwas Unverständliches.', '', '', '', '', ''),
(23967, 0, 0, '', '', 'Wenn es nur mehr Zeit gäbe!', '', '', '', '', ''),
(23667, 0, 0, '', '', 'AAARRRGH!', '', '', '', '', ''),
(24718, 0, 0, '', '', 'Mann, wer oder was würde so etwas tun?!', '', '', '', '', ''),
(23658, 0, 0, '', '', '%s lässt vor dem Gefangenen einen Schlüssel baumeln.', '', '', '', '', ''),
(24176, 0, 0, '', '', 'Ihr wisst doch von dem riesigen YETI in der Mine, oder?', '', '', '', '', ''),
(23989, 0, 0, '', '', 'Für Ymiron!', '', '', '', '', ''),
(23963, 0, 0, '', '', 'Feuer!', '', '', '', '', ''),
(26282, 0, 0, '', '', 'Loken wird niemals triumphieren!', '', '', '', '', ''),
(24717, 0, 0, '', '', 'Was meint Ihr''n, wer das gemacht hat, Nadelmeyer?', '', '', '', '', ''),
(26334, 0, 0, '', '', 'Auf in den Kampf, meine Kinder! Zeigt dem Feind keine Gnade!', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=27118 /*27118*/ AND `locale`='deDE') OR (`entry`=27117 /*27117*/ AND `locale`='deDE') OR (`entry`=24897 /*24897*/ AND `locale`='deDE') OR (`entry`=24896 /*24896*/ AND `locale`='deDE') OR (`entry`=24643 /*24643*/ AND `locale`='deDE') OR (`entry`=24767 /*24767*/ AND `locale`='deDE') OR (`entry`=24845 /*24845*/ AND `locale`='deDE') OR (`entry`=24828 /*24828*/ AND `locale`='deDE') OR (`entry`=24827 /*24827*/ AND `locale`='deDE') OR (`entry`=24826 /*24826*/ AND `locale`='deDE') OR (`entry`=24831 /*24831*/ AND `locale`='deDE') OR (`entry`=24829 /*24829*/ AND `locale`='deDE') OR (`entry`=24832 /*24832*/ AND `locale`='deDE') OR (`entry`=25224 /*25224*/ AND `locale`='deDE') OR (`entry`=23886 /*23886*/ AND `locale`='deDE') OR (`entry`=29559 /*29559*/ AND `locale`='deDE') OR (`entry`=23887 /*23887*/ AND `locale`='deDE') OR (`entry`=23983 /*23983*/ AND `locale`='deDE') OR (`entry`=23982 /*23982*/ AND `locale`='deDE') OR (`entry`=23934 /*23934*/ AND `locale`='deDE') OR (`entry`=23780 /*23780*/ AND `locale`='deDE') OR (`entry`=23809 /*23809*/ AND `locale`='deDE') OR (`entry`=1921 /*1921*/ AND `locale`='deDE') OR (`entry`=26955 /*26955*/ AND `locale`='deDE') OR (`entry`=23938 /*23938*/ AND `locale`='deDE') OR (`entry`=23939 /*23939*/ AND `locale`='deDE') OR (`entry`=24350 /*24350*/ AND `locale`='deDE') OR (`entry`=23930 /*23930*/ AND `locale`='deDE') OR (`entry`=24342 /*24342*/ AND `locale`='deDE') OR (`entry`=26964 /*26964*/ AND `locale`='deDE') OR (`entry`=32774 /*32774*/ AND `locale`='deDE') OR (`entry`=24348 /*24348*/ AND `locale`='deDE') OR (`entry`=24341 /*24341*/ AND `locale`='deDE') OR (`entry`=29159 /*29159*/ AND `locale`='deDE') OR (`entry`=27344 /*27344*/ AND `locale`='deDE') OR (`entry`=26953 /*26953*/ AND `locale`='deDE') OR (`entry`=24343 /*24343*/ AND `locale`='deDE') OR (`entry`=23816 /*23816*/ AND `locale`='deDE') OR (`entry`=47568 /*47568*/ AND `locale`='deDE') OR (`entry`=29421 /*29421*/ AND `locale`='deDE') OR (`entry`=29420 /*29420*/ AND `locale`='deDE') OR (`entry`=29419 /*29419*/ AND `locale`='deDE') OR (`entry`=29418 /*29418*/ AND `locale`='deDE') OR (`entry`=26962 /*26962*/ AND `locale`='deDE') OR (`entry`=26960 /*26960*/ AND `locale`='deDE') OR (`entry`=26959 /*26959*/ AND `locale`='deDE') OR (`entry`=26956 /*26956*/ AND `locale`='deDE') OR (`entry`=26952 /*26952*/ AND `locale`='deDE') OR (`entry`=24347 /*24347*/ AND `locale`='deDE') OR (`entry`=29302 /*29302*/ AND `locale`='deDE') OR (`entry`=26957 /*26957*/ AND `locale`='deDE') OR (`entry`=24116 /*24116*/ AND `locale`='deDE') OR (`entry`=26954 /*26954*/ AND `locale`='deDE') OR (`entry`=26951 /*26951*/ AND `locale`='deDE') OR (`entry`=24126 /*24126*/ AND `locale`='deDE') OR (`entry`=23781 /*23781*/ AND `locale`='deDE') OR (`entry`=31716 /*31716*/ AND `locale`='deDE') OR (`entry`=25075 /*25075*/ AND `locale`='deDE') OR (`entry`=26963 /*26963*/ AND `locale`='deDE') OR (`entry`=26961 /*26961*/ AND `locale`='deDE') OR (`entry`=26958 /*26958*/ AND `locale`='deDE') OR (`entry`=24349 /*24349*/ AND `locale`='deDE') OR (`entry`=31715 /*31715*/ AND `locale`='deDE') OR (`entry`=31708 /*31708*/ AND `locale`='deDE') OR (`entry`=31706 /*31706*/ AND `locale`='deDE') OR (`entry`=31705 /*31705*/ AND `locale`='deDE') OR (`entry`=31704 /*31704*/ AND `locale`='deDE') OR (`entry`=26540 /*26540*/ AND `locale`='deDE') OR (`entry`=23779 /*23779*/ AND `locale`='deDE') OR (`entry`=5202 /*5202*/ AND `locale`='deDE') OR (`entry`=29422 /*29422*/ AND `locale`='deDE') OR (`entry`=24128 /*24128*/ AND `locale`='deDE') OR (`entry`=23945 /*23945*/ AND `locale`='deDE') OR (`entry`=23998 /*23998*/ AND `locale`='deDE') OR (`entry`=23778 /*23778*/ AND `locale`='deDE') OR (`entry`=23915 /*23915*/ AND `locale`='deDE') OR (`entry`=23947 /*23947*/ AND `locale`='deDE') OR (`entry`=23885 /*23885*/ AND `locale`='deDE') OR (`entry`=23957 /*23957*/ AND `locale`='deDE') OR (`entry`=23805 /*23805*/ AND `locale`='deDE') OR (`entry`=23916 /*23916*/ AND `locale`='deDE') OR (`entry`=23963 /*23963*/ AND `locale`='deDE') OR (`entry`=23962 /*23962*/ AND `locale`='deDE') OR (`entry`=23866 /*23866*/ AND `locale`='deDE') OR (`entry`=23793 /*23793*/ AND `locale`='deDE') OR (`entry`=23946 /*23946*/ AND `locale`='deDE') OR (`entry`=23917 /*23917*/ AND `locale`='deDE') OR (`entry`=23794 /*23794*/ AND `locale`='deDE') OR (`entry`=23964 /*23964*/ AND `locale`='deDE') OR (`entry`=23784 /*23784*/ AND `locale`='deDE') OR (`entry`=23929 /*23929*/ AND `locale`='deDE') OR (`entry`=24811 /*24811*/ AND `locale`='deDE') OR (`entry`=23968 /*23968*/ AND `locale`='deDE') OR (`entry`=23711 /*23711*/ AND `locale`='deDE') OR (`entry`=23674 /*23674*/ AND `locale`='deDE') OR (`entry`=23676 /*23676*/ AND `locale`='deDE') OR (`entry`=23972 /*23972*/ AND `locale`='deDE') OR (`entry`=23803 /*23803*/ AND `locale`='deDE') OR (`entry`=23867 /*23867*/ AND `locale`='deDE') OR (`entry`=71163 /*71163*/ AND `locale`='deDE') OR (`entry`=24750 /*24750*/ AND `locale`='deDE') OR (`entry`=24751 /*24751*/ AND `locale`='deDE') OR (`entry`=24752 /*24752*/ AND `locale`='deDE') OR (`entry`=24787 /*24787*/ AND `locale`='deDE') OR (`entry`=26934 /*26934*/ AND `locale`='deDE') OR (`entry`=24807 /*24807*/ AND `locale`='deDE') OR (`entry`=24717 /*24717*/ AND `locale`='deDE') OR (`entry`=23675 /*23675*/ AND `locale`='deDE') OR (`entry`=23672 /*23672*/ AND `locale`='deDE') OR (`entry`=24820 /*24820*/ AND `locale`='deDE') OR (`entry`=24817 /*24817*/ AND `locale`='deDE') OR (`entry`=24720 /*24720*/ AND `locale`='deDE') OR (`entry`=24719 /*24719*/ AND `locale`='deDE') OR (`entry`=24718 /*24718*/ AND `locale`='deDE') OR (`entry`=24340 /*24340*/ AND `locale`='deDE') OR (`entry`=24824 /*24824*/ AND `locale`='deDE') OR (`entry`=24271 /*24271*/ AND `locale`='deDE') OR (`entry`=23673 /*23673*/ AND `locale`='deDE') OR (`entry`=24746 /*24746*/ AND `locale`='deDE') OR (`entry`=27922 /*27922*/ AND `locale`='deDE') OR (`entry`=24548 /*24548*/ AND `locale`='deDE') OR (`entry`=23776 /*23776*/ AND `locale`='deDE') OR (`entry`=32573 /*32573*/ AND `locale`='deDE') OR (`entry`=23994 /*23994*/ AND `locale`='deDE') OR (`entry`=24335 /*24335*/ AND `locale`='deDE') OR (`entry`=24334 /*24334*/ AND `locale`='deDE') OR (`entry`=24336 /*24336*/ AND `locale`='deDE') OR (`entry`=24345 /*24345*/ AND `locale`='deDE') OR (`entry`=24030 /*24030*/ AND `locale`='deDE') OR (`entry`=23796 /*23796*/ AND `locale`='deDE') OR (`entry`=24316 /*24316*/ AND `locale`='deDE') OR (`entry`=24212 /*24212*/ AND `locale`='deDE') OR (`entry`=23725 /*23725*/ AND `locale`='deDE') OR (`entry`=28314 /*28314*/ AND `locale`='deDE') OR (`entry`=28313 /*28313*/ AND `locale`='deDE') OR (`entry`=23883 /*23883*/ AND `locale`='deDE') OR (`entry`=24027 /*24027*/ AND `locale`='deDE') OR (`entry`=24161 /*24161*/ AND `locale`='deDE') OR (`entry`=23865 /*23865*/ AND `locale`='deDE') OR (`entry`=24238 /*24238*/ AND `locale`='deDE') OR (`entry`=24634 /*24634*/ AND `locale`='deDE') OR (`entry`=23670 /*23670*/ AND `locale`='deDE') OR (`entry`=24015 /*24015*/ AND `locale`='deDE') OR (`entry`=23884 /*23884*/ AND `locale`='deDE') OR (`entry`=23657 /*23657*/ AND `locale`='deDE') OR (`entry`=24185 /*24185*/ AND `locale`='deDE') OR (`entry`=24183 /*24183*/ AND `locale`='deDE') OR (`entry`=23655 /*23655*/ AND `locale`='deDE') OR (`entry`=24474 /*24474*/ AND `locale`='deDE') OR (`entry`=24458 /*24458*/ AND `locale`='deDE') OR (`entry`=24467 /*24467*/ AND `locale`='deDE') OR (`entry`=24184 /*24184*/ AND `locale`='deDE') OR (`entry`=23653 /*23653*/ AND `locale`='deDE') OR (`entry`=26826 /*26826*/ AND `locale`='deDE') OR (`entry`=29487 /*29487*/ AND `locale`='deDE') OR (`entry`=24178 /*24178*/ AND `locale`='deDE') OR (`entry`=24229 /*24229*/ AND `locale`='deDE') OR (`entry`=23744 /*23744*/ AND `locale`='deDE') OR (`entry`=24062 /*24062*/ AND `locale`='deDE') OR (`entry`=24515 /*24515*/ AND `locale`='deDE') OR (`entry`=24176 /*24176*/ AND `locale`='deDE') OR (`entry`=24376 /*24376*/ AND `locale`='deDE') OR (`entry`=24139 /*24139*/ AND `locale`='deDE') OR (`entry`=24131 /*24131*/ AND `locale`='deDE') OR (`entry`=24054 /*24054*/ AND `locale`='deDE') OR (`entry`=24053 /*24053*/ AND `locale`='deDE') OR (`entry`=24057 /*24057*/ AND `locale`='deDE') OR (`entry`=24532 /*24532*/ AND `locale`='deDE') OR (`entry`=24531 /*24531*/ AND `locale`='deDE') OR (`entry`=24328 /*24328*/ AND `locale`='deDE') OR (`entry`=24282 /*24282*/ AND `locale`='deDE') OR (`entry`=24066 /*24066*/ AND `locale`='deDE') OR (`entry`=24061 /*24061*/ AND `locale`='deDE') OR (`entry`=24534 /*24534*/ AND `locale`='deDE') OR (`entry`=24535 /*24535*/ AND `locale`='deDE') OR (`entry`=24528 /*24528*/ AND `locale`='deDE') OR (`entry`=24052 /*24052*/ AND `locale`='deDE') OR (`entry`=24058 /*24058*/ AND `locale`='deDE') OR (`entry`=24524 /*24524*/ AND `locale`='deDE') OR (`entry`=24055 /*24055*/ AND `locale`='deDE') OR (`entry`=24514 /*24514*/ AND `locale`='deDE') OR (`entry`=24056 /*24056*/ AND `locale`='deDE') OR (`entry`=24117 /*24117*/ AND `locale`='deDE') OR (`entry`=23677 /*23677*/ AND `locale`='deDE') OR (`entry`=23919 /*23919*/ AND `locale`='deDE') OR (`entry`=23875 /*23875*/ AND `locale`='deDE') OR (`entry`=24638 /*24638*/ AND `locale`='deDE') OR (`entry`=23876 /*23876*/ AND `locale`='deDE') OR (`entry`=23874 /*23874*/ AND `locale`='deDE') OR (`entry`=24050 /*24050*/ AND `locale`='deDE') OR (`entry`=26488 /*26488*/ AND `locale`='deDE') OR (`entry`=29301 /*29301*/ AND `locale`='deDE') OR (`entry`=24863 /*24863*/ AND `locale`='deDE') OR (`entry`=24228 /*24228*/ AND `locale`='deDE') OR (`entry`=24033 /*24033*/ AND `locale`='deDE') OR (`entry`=24142 /*24142*/ AND `locale`='deDE') OR (`entry`=24067 /*24067*/ AND `locale`='deDE') OR (`entry`=24032 /*24032*/ AND `locale`='deDE') OR (`entry`=24390 /*24390*/ AND `locale`='deDE') OR (`entry`=24234 /*24234*/ AND `locale`='deDE') OR (`entry`=24135 /*24135*/ AND `locale`='deDE') OR (`entry`=24129 /*24129*/ AND `locale`='deDE') OR (`entry`=24127 /*24127*/ AND `locale`='deDE') OR (`entry`=24362 /*24362*/ AND `locale`='deDE') OR (`entry`=24123 /*24123*/ AND `locale`='deDE') OR (`entry`=24028 /*24028*/ AND `locale`='deDE') OR (`entry`=24256 /*24256*/ AND `locale`='deDE') OR (`entry`=24236 /*24236*/ AND `locale`='deDE') OR (`entry`=24235 /*24235*/ AND `locale`='deDE') OR (`entry`=24031 /*24031*/ AND `locale`='deDE') OR (`entry`=27701 /*27701*/ AND `locale`='deDE') OR (`entry`=27699 /*27699*/ AND `locale`='deDE') OR (`entry`=23740 /*23740*/ AND `locale`='deDE') OR (`entry`=24102 /*24102*/ AND `locale`='deDE') OR (`entry`=24092 /*24092*/ AND `locale`='deDE') OR (`entry`=24094 /*24094*/ AND `locale`='deDE') OR (`entry`=24093 /*24093*/ AND `locale`='deDE') OR (`entry`=23668 /*23668*/ AND `locale`='deDE') OR (`entry`=29486 /*29486*/ AND `locale`='deDE') OR (`entry`=26823 /*26823*/ AND `locale`='deDE') OR (`entry`=26827 /*26827*/ AND `locale`='deDE') OR (`entry`=26825 /*26825*/ AND `locale`='deDE') OR (`entry`=23772 /*23772*/ AND `locale`='deDE') OR (`entry`=24196 /*24196*/ AND `locale`='deDE') OR (`entry`=23678 /*23678*/ AND `locale`='deDE') OR (`entry`=24100 /*24100*/ AND `locale`='deDE') OR (`entry`=23664 /*23664*/ AND `locale`='deDE') OR (`entry`=23667 /*23667*/ AND `locale`='deDE') OR (`entry`=24087 /*24087*/ AND `locale`='deDE') OR (`entry`=23666 /*23666*/ AND `locale`='deDE') OR (`entry`=23669 /*23669*/ AND `locale`='deDE') OR (`entry`=24098 /*24098*/ AND `locale`='deDE') OR (`entry`=23663 /*23663*/ AND `locale`='deDE') OR (`entry`=23661 /*23661*/ AND `locale`='deDE') OR (`entry`=23662 /*23662*/ AND `locale`='deDE') OR (`entry`=23665 /*23665*/ AND `locale`='deDE') OR (`entry`=33303 /*33303*/ AND `locale`='deDE') OR (`entry`=24029 /*24029*/ AND `locale`='deDE') OR (`entry`=24023 /*24023*/ AND `locale`='deDE') OR (`entry`=29393 /*29393*/ AND `locale`='deDE') OR (`entry`=24018 /*24018*/ AND `locale`='deDE') OR (`entry`=23993 /*23993*/ AND `locale`='deDE') OR (`entry`=23992 /*23992*/ AND `locale`='deDE') OR (`entry`=23989 /*23989*/ AND `locale`='deDE') OR (`entry`=24035 /*24035*/ AND `locale`='deDE') OR (`entry`=23990 /*23990*/ AND `locale`='deDE') OR (`entry`=24014 /*24014*/ AND `locale`='deDE') OR (`entry`=24669 /*24669*/ AND `locale`='deDE') OR (`entry`=24073 /*24073*/ AND `locale`='deDE') OR (`entry`=24013 /*24013*/ AND `locale`='deDE') OR (`entry`=23991 /*23991*/ AND `locale`='deDE') OR (`entry`=24901 /*24901*/ AND `locale`='deDE') OR (`entry`=24273 /*24273*/ AND `locale`='deDE') OR (`entry`=24227 /*24227*/ AND `locale`='deDE') OR (`entry`=23891 /*23891*/ AND `locale`='deDE') OR (`entry`=24209 /*24209*/ AND `locale`='deDE') OR (`entry`=24473 /*24473*/ AND `locale`='deDE') OR (`entry`=24401 /*24401*/ AND `locale`='deDE') OR (`entry`=24195 /*24195*/ AND `locale`='deDE') OR (`entry`=24186 /*24186*/ AND `locale`='deDE') OR (`entry`=23033 /*23033*/ AND `locale`='deDE') OR (`entry`=24516 /*24516*/ AND `locale`='deDE') OR (`entry`=24197 /*24197*/ AND `locale`='deDE') OR (`entry`=24517 /*24517*/ AND `locale`='deDE') OR (`entry`=24418 /*24418*/ AND `locale`='deDE') OR (`entry`=24399 /*24399*/ AND `locale`='deDE') OR (`entry`=24400 /*24400*/ AND `locale`='deDE') OR (`entry`=24439 /*24439*/ AND `locale`='deDE') OR (`entry`=24398 /*24398*/ AND `locale`='deDE') OR (`entry`=24262 /*24262*/ AND `locale`='deDE') OR (`entry`=24019 /*24019*/ AND `locale`='deDE') OR (`entry`=23958 /*23958*/ AND `locale`='deDE') OR (`entry`=23959 /*23959*/ AND `locale`='deDE') OR (`entry`=24210 /*24210*/ AND `locale`='deDE') OR (`entry`=24313 /*24313*/ AND `locale`='deDE') OR (`entry`=26844 /*26844*/ AND `locale`='deDE') OR (`entry`=24218 /*24218*/ AND `locale`='deDE') OR (`entry`=24188 /*24188*/ AND `locale`='deDE') OR (`entry`=24152 /*24152*/ AND `locale`='deDE') OR (`entry`=24359 /*24359*/ AND `locale`='deDE') OR (`entry`=24317 /*24317*/ AND `locale`='deDE') OR (`entry`=66635 /*66635*/ AND `locale`='deDE') OR (`entry`=66615 /*66615*/ AND `locale`='deDE') OR (`entry`=66614 /*66614*/ AND `locale`='deDE') OR (`entry`=66613 /*66613*/ AND `locale`='deDE') OR (`entry`=26363 /*26363*/ AND `locale`='deDE') OR (`entry`=27131 /*27131*/ AND `locale`='deDE') OR (`entry`=26785 /*26785*/ AND `locale`='deDE') OR (`entry`=27377 /*27377*/ AND `locale`='deDE') OR (`entry`=27297 /*27297*/ AND `locale`='deDE') OR (`entry`=31889 /*31889*/ AND `locale`='deDE') OR (`entry`=27260 /*27260*/ AND `locale`='deDE') OR (`entry`=27259 /*27259*/ AND `locale`='deDE') OR (`entry`=26591 /*26591*/ AND `locale`='deDE') OR (`entry`=27689 /*27689*/ AND `locale`='deDE') OR (`entry`=27688 /*27688*/ AND `locale`='deDE') OR (`entry`=27292 /*27292*/ AND `locale`='deDE') OR (`entry`=24544 /*24544*/ AND `locale`='deDE') OR (`entry`=23833 /*23833*/ AND `locale`='deDE') OR (`entry`=24460 /*24460*/ AND `locale`='deDE') OR (`entry`=23643 /*23643*/ AND `locale`='deDE') OR (`entry`=24540 /*24540*/ AND `locale`='deDE') OR (`entry`=24485 /*24485*/ AND `locale`='deDE') OR (`entry`=24464 /*24464*/ AND `locale`='deDE') OR (`entry`=24461 /*24461*/ AND `locale`='deDE') OR (`entry`=24459 /*24459*/ AND `locale`='deDE') OR (`entry`=23645 /*23645*/ AND `locale`='deDE') OR (`entry`=23644 /*23644*/ AND `locale`='deDE') OR (`entry`=24048 /*24048*/ AND `locale`='deDE') OR (`entry`=29479 /*29479*/ AND `locale`='deDE') OR (`entry`=24026 /*24026*/ AND `locale`='deDE') OR (`entry`=25233 /*25233*/ AND `locale`='deDE') OR (`entry`=23860 /*23860*/ AND `locale`='deDE') OR (`entry`=23984 /*23984*/ AND `locale`='deDE') OR (`entry`=23985 /*23985*/ AND `locale`='deDE') OR (`entry`=23986 /*23986*/ AND `locale`='deDE') OR (`entry`=23859 /*23859*/ AND `locale`='deDE') OR (`entry`=23974 /*23974*/ AND `locale`='deDE') OR (`entry`=23773 /*23773*/ AND `locale`='deDE') OR (`entry`=29663 /*29663*/ AND `locale`='deDE') OR (`entry`=29662 /*29662*/ AND `locale`='deDE') OR (`entry`=29658 /*29658*/ AND `locale`='deDE') OR (`entry`=23987 /*23987*/ AND `locale`='deDE') OR (`entry`=23978 /*23978*/ AND `locale`='deDE') OR (`entry`=23831 /*23831*/ AND `locale`='deDE') OR (`entry`=26900 /*26900*/ AND `locale`='deDE') OR (`entry`=23937 /*23937*/ AND `locale`='deDE') OR (`entry`=24283 /*24283*/ AND `locale`='deDE') OR (`entry`=24494 /*24494*/ AND `locale`='deDE') OR (`entry`=24333 /*24333*/ AND `locale`='deDE') OR (`entry`=23862 /*23862*/ AND `locale`='deDE') OR (`entry`=23977 /*23977*/ AND `locale`='deDE') OR (`entry`=24356 /*24356*/ AND `locale`='deDE') OR (`entry`=23911 /*23911*/ AND `locale`='deDE') OR (`entry`=24284 /*24284*/ AND `locale`='deDE') OR (`entry`=23908 /*23908*/ AND `locale`='deDE') OR (`entry`=23888 /*23888*/ AND `locale`='deDE') OR (`entry`=23976 /*23976*/ AND `locale`='deDE') OR (`entry`=23749 /*23749*/ AND `locale`='deDE') OR (`entry`=24038 /*24038*/ AND `locale`='deDE') OR (`entry`=23840 /*23840*/ AND `locale`='deDE') OR (`entry`=23895 /*23895*/ AND `locale`='deDE') OR (`entry`=28157 /*28157*/ AND `locale`='deDE') OR (`entry`=23823 /*23823*/ AND `locale`='deDE') OR (`entry`=24289 /*24289*/ AND `locale`='deDE') OR (`entry`=29881 /*29881*/ AND `locale`='deDE') OR (`entry`=24546 /*24546*/ AND `locale`='deDE') OR (`entry`=23771 /*23771*/ AND `locale`='deDE') OR (`entry`=23755 /*23755*/ AND `locale`='deDE') OR (`entry`=23810 /*23810*/ AND `locale`='deDE') OR (`entry`=24478 /*24478*/ AND `locale`='deDE') OR (`entry`=27151 /*27151*/ AND `locale`='deDE') OR (`entry`=27146 /*27146*/ AND `locale`='deDE') OR (`entry`=29966 /*29966*/ AND `locale`='deDE') OR (`entry`=24755 /*24755*/ AND `locale`='deDE') OR (`entry`=28197 /*28197*/ AND `locale`='deDE') OR (`entry`=27145 /*27145*/ AND `locale`='deDE') OR (`entry`=26558 /*26558*/ AND `locale`='deDE') OR (`entry`=27149 /*27149*/ AND `locale`='deDE') OR (`entry`=31916 /*31916*/ AND `locale`='deDE') OR (`entry`=27144 /*27144*/ AND `locale`='deDE') OR (`entry`=24810 /*24810*/ AND `locale`='deDE') OR (`entry`=24763 /*24763*/ AND `locale`='deDE') OR (`entry`=27150 /*27150*/ AND `locale`='deDE') OR (`entry`=27167 /*27167*/ AND `locale`='deDE') OR (`entry`=27148 /*27148*/ AND `locale`='deDE') OR (`entry`=23638 /*23638*/ AND `locale`='deDE') OR (`entry`=24754 /*24754*/ AND `locale`='deDE') OR (`entry`=24786 /*24786*/ AND `locale`='deDE') OR (`entry`=61158 /*61158*/ AND `locale`='deDE') OR (`entry`=24673 /*24673*/ AND `locale`='deDE') OR (`entry`=24797 /*24797*/ AND `locale`='deDE') OR (`entry`=24681 /*24681*/ AND `locale`='deDE') OR (`entry`=24789 /*24789*/ AND `locale`='deDE') OR (`entry`=24677 /*24677*/ AND `locale`='deDE') OR (`entry`=62669 /*62669*/ AND `locale`='deDE') OR (`entry`=24678 /*24678*/ AND `locale`='deDE') OR (`entry`=24900 /*24900*/ AND `locale`='deDE') OR (`entry`=24902 /*24902*/ AND `locale`='deDE') OR (`entry`=24676 /*24676*/ AND `locale`='deDE') OR (`entry`=24913 /*24913*/ AND `locale`='deDE') OR (`entry`=24992 /*24992*/ AND `locale`='deDE') OR (`entry`=17213 /*17213*/ AND `locale`='deDE') OR (`entry`=25026 /*25026*/ AND `locale`='deDE') OR (`entry`=24742 /*24742*/ AND `locale`='deDE') OR (`entry`=24713 /*24713*/ AND `locale`='deDE') OR (`entry`=24899 /*24899*/ AND `locale`='deDE') OR (`entry`=24714 /*24714*/ AND `locale`='deDE') OR (`entry`=24539 /*24539*/ AND `locale`='deDE') OR (`entry`=24542 /*24542*/ AND `locale`='deDE') OR (`entry`=29151 /*29151*/ AND `locale`='deDE') OR (`entry`=24543 /*24543*/ AND `locale`='deDE') OR (`entry`=24639 /*24639*/ AND `locale`='deDE') OR (`entry`=28277 /*28277*/ AND `locale`='deDE') OR (`entry`=24741 /*24741*/ AND `locale`='deDE') OR (`entry`=24788 /*24788*/ AND `locale`='deDE') OR (`entry`=24525 /*24525*/ AND `locale`='deDE') OR (`entry`=27933 /*27933*/ AND `locale`='deDE') OR (`entry`=24537 /*24537*/ AND `locale`='deDE') OR (`entry`=24541 /*24541*/ AND `locale`='deDE') OR (`entry`=24628 /*24628*/ AND `locale`='deDE') OR (`entry`=26503 /*26503*/ AND `locale`='deDE') OR (`entry`=24784 /*24784*/ AND `locale`='deDE') OR (`entry`=24633 /*24633*/ AND `locale`='deDE') OR (`entry`=24642 /*24642*/ AND `locale`='deDE') OR (`entry`=5936 /*5936*/ AND `locale`='deDE') OR (`entry`=24956 /*24956*/ AND `locale`='deDE') OR (`entry`=24973 /*24973*/ AND `locale`='deDE') OR (`entry`=24911 /*24911*/ AND `locale`='deDE') OR (`entry`=24910 /*24910*/ AND `locale`='deDE') OR (`entry`=24637 /*24637*/ AND `locale`='deDE') OR (`entry`=24914 /*24914*/ AND `locale`='deDE') OR (`entry`=60761 /*60761*/ AND `locale`='deDE') OR (`entry`=23940 /*23940*/ AND `locale`='deDE') OR (`entry`=23871 /*23871*/ AND `locale`='deDE') OR (`entry`=23851 /*23851*/ AND `locale`='deDE') OR (`entry`=23844 /*23844*/ AND `locale`='deDE') OR (`entry`=23770 /*23770*/ AND `locale`='deDE') OR (`entry`=28135 /*28135*/ AND `locale`='deDE') OR (`entry`=23839 /*23839*/ AND `locale`='deDE') OR (`entry`=23842 /*23842*/ AND `locale`='deDE') OR (`entry`=23836 /*23836*/ AND `locale`='deDE') OR (`entry`=23870 /*23870*/ AND `locale`='deDE') OR (`entry`=23777 /*23777*/ AND `locale`='deDE') OR (`entry`=23750 /*23750*/ AND `locale`='deDE') OR (`entry`=24884 /*24884*/ AND `locale`='deDE') OR (`entry`=23804 /*23804*/ AND `locale`='deDE') OR (`entry`=25232 /*25232*/ AND `locale`='deDE') OR (`entry`=24887 /*24887*/ AND `locale`='deDE') OR (`entry`=24874 /*24874*/ AND `locale`='deDE') OR (`entry`=25230 /*25230*/ AND `locale`='deDE') OR (`entry`=24875 /*24875*/ AND `locale`='deDE') OR (`entry`=25229 /*25229*/ AND `locale`='deDE') OR (`entry`=24880 /*24880*/ AND `locale`='deDE') OR (`entry`=24877 /*24877*/ AND `locale`='deDE') OR (`entry`=25231 /*25231*/ AND `locale`='deDE') OR (`entry`=24889 /*24889*/ AND `locale`='deDE') OR (`entry`=24876 /*24876*/ AND `locale`='deDE') OR (`entry`=24883 /*24883*/ AND `locale`='deDE') OR (`entry`=24871 /*24871*/ AND `locale`='deDE') OR (`entry`=22515 /*22515*/ AND `locale`='deDE') OR (`entry`=24872 /*24872*/ AND `locale`='deDE') OR (`entry`=24291 /*24291*/ AND `locale`='deDE') OR (`entry`=24252 /*24252*/ AND `locale`='deDE') OR (`entry`=26984 /*26984*/ AND `locale`='deDE') OR (`entry`=24330 /*24330*/ AND `locale`='deDE') OR (`entry`=24251 /*24251*/ AND `locale`='deDE') OR (`entry`=24148 /*24148*/ AND `locale`='deDE') OR (`entry`=24125 /*24125*/ AND `locale`='deDE') OR (`entry`=24157 /*24157*/ AND `locale`='deDE') OR (`entry`=24768 /*24768*/ AND `locale`='deDE') OR (`entry`=24168 /*24168*/ AND `locale`='deDE') OR (`entry`=23155 /*23155*/ AND `locale`='deDE') OR (`entry`=24164 /*24164*/ AND `locale`='deDE') OR (`entry`=51888 /*51888*/ AND `locale`='deDE') OR (`entry`=24155 /*24155*/ AND `locale`='deDE') OR (`entry`=24081 /*24081*/ AND `locale`='deDE') OR (`entry`=27926 /*27926*/ AND `locale`='deDE') OR (`entry`=27927 /*27927*/ AND `locale`='deDE') OR (`entry`=24147 /*24147*/ AND `locale`='deDE') OR (`entry`=24141 /*24141*/ AND `locale`='deDE') OR (`entry`=24154 /*24154*/ AND `locale`='deDE') OR (`entry`=24149 /*24149*/ AND `locale`='deDE') OR (`entry`=33019 /*33019*/ AND `locale`='deDE') OR (`entry`=33018 /*33018*/ AND `locale`='deDE') OR (`entry`=24104 /*24104*/ AND `locale`='deDE') OR (`entry`=24290 /*24290*/ AND `locale`='deDE') OR (`entry`=23921 /*23921*/ AND `locale`='deDE') OR (`entry`=23922 /*23922*/ AND `locale`='deDE') OR (`entry`=24280 /*24280*/ AND `locale`='deDE') OR (`entry`=23680 /*23680*/ AND `locale`='deDE') OR (`entry`=23923 /*23923*/ AND `locale`='deDE') OR (`entry`=24213 /*24213*/ AND `locale`='deDE') OR (`entry`=24258 /*24258*/ AND `locale`='deDE') OR (`entry`=24255 /*24255*/ AND `locale`='deDE') OR (`entry`=24254 /*24254*/ AND `locale`='deDE') OR (`entry`=23924 /*23924*/ AND `locale`='deDE') OR (`entry`=23564 /*23564*/ AND `locale`='deDE') OR (`entry`=24199 /*24199*/ AND `locale`='deDE') OR (`entry`=24198 /*24198*/ AND `locale`='deDE') OR (`entry`=28359 /*28359*/ AND `locale`='deDE') OR (`entry`=23906 /*23906*/ AND `locale`='deDE') OR (`entry`=23689 /*23689*/ AND `locale`='deDE') OR (`entry`=23760 /*23760*/ AND `locale`='deDE') OR (`entry`=23967 /*23967*/ AND `locale`='deDE') OR (`entry`=24518 /*24518*/ AND `locale`='deDE') OR (`entry`=26663 /*26663*/ AND `locale`='deDE') OR (`entry`=24076 /*24076*/ AND `locale`='deDE') OR (`entry`=23693 /*23693*/ AND `locale`='deDE') OR (`entry`=23691 /*23691*/ AND `locale`='deDE') OR (`entry`=24791 /*24791*/ AND `locale`='deDE') OR (`entry`=23690 /*23690*/ AND `locale`='deDE') OR (`entry`=24182 /*24182*/ AND `locale`='deDE') OR (`entry`=24194 /*24194*/ AND `locale`='deDE') OR (`entry`=24193 /*24193*/ AND `locale`='deDE') OR (`entry`=23730 /*23730*/ AND `locale`='deDE') OR (`entry`=27993 /*27993*/ AND `locale`='deDE') OR (`entry`=27992 /*27992*/ AND `locale`='deDE') OR (`entry`=24701 /*24701*/ AND `locale`='deDE') OR (`entry`=23656 /*23656*/ AND `locale`='deDE') OR (`entry`=23654 /*23654*/ AND `locale`='deDE') OR (`entry`=24221 /*24221*/ AND `locale`='deDE') OR (`entry`=24253 /*24253*/ AND `locale`='deDE') OR (`entry`=32642 /*32642*/ AND `locale`='deDE') OR (`entry`=32641 /*32641*/ AND `locale`='deDE') OR (`entry`=24115 /*24115*/ AND `locale`='deDE') OR (`entry`=24112 /*24112*/ AND `locale`='deDE') OR (`entry`=62641 /*62641*/ AND `locale`='deDE') OR (`entry`=24114 /*24114*/ AND `locale`='deDE') OR (`entry`=24113 /*24113*/ AND `locale`='deDE') OR (`entry`=24635 /*24635*/ AND `locale`='deDE') OR (`entry`=24189 /*24189*/ AND `locale`='deDE') OR (`entry`=24122 /*24122*/ AND `locale`='deDE') OR (`entry`=24086 /*24086*/ AND `locale`='deDE') OR (`entry`=24694 /*24694*/ AND `locale`='deDE') OR (`entry`=24089 /*24089*/ AND `locale`='deDE') OR (`entry`=23932 /*23932*/ AND `locale`='deDE') OR (`entry`=24145 /*24145*/ AND `locale`='deDE') OR (`entry`=24475 /*24475*/ AND `locale`='deDE') OR (`entry`=24158 /*24158*/ AND `locale`='deDE') OR (`entry`=24088 /*24088*/ AND `locale`='deDE') OR (`entry`=23658 /*23658*/ AND `locale`='deDE') OR (`entry`=24150 /*24150*/ AND `locale`='deDE') OR (`entry`=23651 /*23651*/ AND `locale`='deDE') OR (`entry`=24090 /*24090*/ AND `locale`='deDE') OR (`entry`=24226 /*24226*/ AND `locale`='deDE') OR (`entry`=23660 /*23660*/ AND `locale`='deDE') OR (`entry`=24151 /*24151*/ AND `locale`='deDE') OR (`entry`=24216 /*24216*/ AND `locale`='deDE') OR (`entry`=24106 /*24106*/ AND `locale`='deDE') OR (`entry`=24249 /*24249*/ AND `locale`='deDE') OR (`entry`=24270 /*24270*/ AND `locale`='deDE') OR (`entry`=24260 /*24260*/ AND `locale`='deDE') OR (`entry`=24172 /*24172*/ AND `locale`='deDE') OR (`entry`=62640 /*62640*/ AND `locale`='deDE') OR (`entry`=24177 /*24177*/ AND `locale`='deDE') OR (`entry`=24272 /*24272*/ AND `locale`='deDE') OR (`entry`=24169 /*24169*/ AND `locale`='deDE') OR (`entry`=24250 /*24250*/ AND `locale`='deDE') OR (`entry`=24077 /*24077*/ AND `locale`='deDE') OR (`entry`=23652 /*23652*/ AND `locale`='deDE') OR (`entry`=24538 /*24538*/ AND `locale`='deDE') OR (`entry`=24051 /*24051*/ AND `locale`='deDE') OR (`entry`=24063 /*24063*/ AND `locale`='deDE') OR (`entry`=23737 /*23737*/ AND `locale`='deDE') OR (`entry`=23732 /*23732*/ AND `locale`='deDE') OR (`entry`=47578 /*47578*/ AND `locale`='deDE') OR (`entry`=26916 /*26916*/ AND `locale`='deDE') OR (`entry`=26908 /*26908*/ AND `locale`='deDE') OR (`entry`=26905 /*26905*/ AND `locale`='deDE') OR (`entry`=26903 /*26903*/ AND `locale`='deDE') OR (`entry`=23731 /*23731*/ AND `locale`='deDE') OR (`entry`=26914 /*26914*/ AND `locale`='deDE') OR (`entry`=26906 /*26906*/ AND `locale`='deDE') OR (`entry`=23802 /*23802*/ AND `locale`='deDE') OR (`entry`=26910 /*26910*/ AND `locale`='deDE') OR (`entry`=23738 /*23738*/ AND `locale`='deDE') OR (`entry`=23733 /*23733*/ AND `locale`='deDE') OR (`entry`=23729 /*23729*/ AND `locale`='deDE') OR (`entry`=24174 /*24174*/ AND `locale`='deDE') OR (`entry`=23791 /*23791*/ AND `locale`='deDE') OR (`entry`=24646 /*24646*/ AND `locale`='deDE') OR (`entry`=23975 /*23975*/ AND `locale`='deDE') OR (`entry`=23548 /*23548*/ AND `locale`='deDE') OR (`entry`=23546 /*23546*/ AND `locale`='deDE') OR (`entry`=24647 /*24647*/ AND `locale`='deDE') OR (`entry`=24707 /*24707*/ AND `locale`='deDE') OR (`entry`=26901 /*26901*/ AND `locale`='deDE') OR (`entry`=24233 /*24233*/ AND `locale`='deDE') OR (`entry`=24191 /*24191*/ AND `locale`='deDE') OR (`entry`=23783 /*23783*/ AND `locale`='deDE') OR (`entry`=23734 /*23734*/ AND `locale`='deDE') OR (`entry`=23551 /*23551*/ AND `locale`='deDE') OR (`entry`=23549 /*23549*/ AND `locale`='deDE') OR (`entry`=24040 /*24040*/ AND `locale`='deDE') OR (`entry`=23557 /*23557*/ AND `locale`='deDE') OR (`entry`=23728 /*23728*/ AND `locale`='deDE') OR (`entry`=23735 /*23735*/ AND `locale`='deDE') OR (`entry`=27930 /*27930*/ AND `locale`='deDE') OR (`entry`=15214 /*15214*/ AND `locale`='deDE') OR (`entry`=26904 /*26904*/ AND `locale`='deDE') OR (`entry`=23736 /*23736*/ AND `locale`='deDE') OR (`entry`=32773 /*32773*/ AND `locale`='deDE') OR (`entry`=62648 /*62648*/ AND `locale`='deDE') OR (`entry`=26915 /*26915*/ AND `locale`='deDE') OR (`entry`=26913 /*26913*/ AND `locale`='deDE') OR (`entry`=26912 /*26912*/ AND `locale`='deDE') OR (`entry`=26911 /*26911*/ AND `locale`='deDE') OR (`entry`=26909 /*26909*/ AND `locale`='deDE') OR (`entry`=26907 /*26907*/ AND `locale`='deDE') OR (`entry`=23801 /*23801*/ AND `locale`='deDE') OR (`entry`=26547 /*26547*/ AND `locale`='deDE') OR (`entry`=23821 /*23821*/ AND `locale`='deDE') OR (`entry`=23739 /*23739*/ AND `locale`='deDE') OR (`entry`=23550 /*23550*/ AND `locale`='deDE') OR (`entry`=23547 /*23547*/ AND `locale`='deDE') OR (`entry`=23785 /*23785*/ AND `locale`='deDE') OR (`entry`=29484 /*29484*/ AND `locale`='deDE') OR (`entry`=28615 /*28615*/ AND `locale`='deDE') OR (`entry`=66639 /*66639*/ AND `locale`='deDE') OR (`entry`=66627 /*66627*/ AND `locale`='deDE') OR (`entry`=66626 /*66626*/ AND `locale`='deDE') OR (`entry`=66624 /*66624*/ AND `locale`='deDE') OR (`entry`=31041 /*31041*/ AND `locale`='deDE') OR (`entry`=29454 /*29454*/ AND `locale`='deDE') OR (`entry`=28866 /*28866*/ AND `locale`='deDE') OR (`entry`=28518 /*28518*/ AND `locale`='deDE') OR (`entry`=28872 /*28872*/ AND `locale`='deDE') OR (`entry`=28867 /*28867*/ AND `locale`='deDE') OR (`entry`=28532 /*28532*/ AND `locale`='deDE') OR (`entry`=28871 /*28871*/ AND `locale`='deDE') OR (`entry`=28865 /*28865*/ AND `locale`='deDE') OR (`entry`=28870 /*28870*/ AND `locale`='deDE') OR (`entry`=28869 /*28869*/ AND `locale`='deDE') OR (`entry`=28868 /*28868*/ AND `locale`='deDE') OR (`entry`=28541 /*28541*/ AND `locale`='deDE') OR (`entry`=29436 /*29436*/ AND `locale`='deDE') OR (`entry`=29459 /*29459*/ AND `locale`='deDE') OR (`entry`=29450 /*29450*/ AND `locale`='deDE') OR (`entry`=29455 /*29455*/ AND `locale`='deDE') OR (`entry`=29928 /*29928*/ AND `locale`='deDE') OR (`entry`=29647 /*29647*/ AND `locale`='deDE') OR (`entry`=29449 /*29449*/ AND `locale`='deDE') OR (`entry`=29452 /*29452*/ AND `locale`='deDE') OR (`entry`=28589 /*28589*/ AND `locale`='deDE') OR (`entry`=28599 /*28599*/ AND `locale`='deDE') OR (`entry`=29856 /*29856*/ AND `locale`='deDE') OR (`entry`=28519 /*28519*/ AND `locale`='deDE') OR (`entry`=29691 /*29691*/ AND `locale`='deDE') OR (`entry`=29689 /*29689*/ AND `locale`='deDE') OR (`entry`=29688 /*29688*/ AND `locale`='deDE') OR (`entry`=29453 /*29453*/ AND `locale`='deDE') OR (`entry`=29468 /*29468*/ AND `locale`='deDE') OR (`entry`=29451 /*29451*/ AND `locale`='deDE') OR (`entry`=28564 /*28564*/ AND `locale`='deDE') OR (`entry`=28565 /*28565*/ AND `locale`='deDE') OR (`entry`=28657 /*28657*/ AND `locale`='deDE') OR (`entry`=28503 /*28503*/ AND `locale`='deDE') OR (`entry`=28879 /*28879*/ AND `locale`='deDE') OR (`entry`=28888 /*28888*/ AND `locale`='deDE') OR (`entry`=28803 /*28803*/ AND `locale`='deDE') OR (`entry`=28666 /*28666*/ AND `locale`='deDE') OR (`entry`=29698 /*29698*/ AND `locale`='deDE') OR (`entry`=29699 /*29699*/ AND `locale`='deDE') OR (`entry`=29848 /*29848*/ AND `locale`='deDE') OR (`entry`=28813 /*28813*/ AND `locale`='deDE') OR (`entry`=28806 /*28806*/ AND `locale`='deDE') OR (`entry`=29656 /*29656*/ AND `locale`='deDE') OR (`entry`=29438 /*29438*/ AND `locale`='deDE') OR (`entry`=29437 /*29437*/ AND `locale`='deDE') OR (`entry`=29439 /*29439*/ AND `locale`='deDE') OR (`entry`=29700 /*29700*/ AND `locale`='deDE') OR (`entry`=29692 /*29692*/ AND `locale`='deDE') OR (`entry`=29686 /*29686*/ AND `locale`='deDE') OR (`entry`=29664 /*29664*/ AND `locale`='deDE') OR (`entry`=29733 /*29733*/ AND `locale`='deDE') OR (`entry`=29690 /*29690*/ AND `locale`='deDE') OR (`entry`=29687 /*29687*/ AND `locale`='deDE') OR (`entry`=28812 /*28812*/ AND `locale`='deDE') OR (`entry`=28807 /*28807*/ AND `locale`='deDE') OR (`entry`=28811 /*28811*/ AND `locale`='deDE') OR (`entry`=28810 /*28810*/ AND `locale`='deDE') OR (`entry`=28809 /*28809*/ AND `locale`='deDE') OR (`entry`=29137 /*29137*/ AND `locale`='deDE') OR (`entry`=28818 /*28818*/ AND `locale`='deDE') OR (`entry`=28618 /*28618*/ AND `locale`='deDE') OR (`entry`=29697 /*29697*/ AND `locale`='deDE') OR (`entry`=29917 /*29917*/ AND `locale`='deDE') OR (`entry`=29916 /*29916*/ AND `locale`='deDE') OR (`entry`=29646 /*29646*/ AND `locale`='deDE') OR (`entry`=28761 /*28761*/ AND `locale`='deDE') OR (`entry`=28617 /*28617*/ AND `locale`='deDE') OR (`entry`=28759 /*28759*/ AND `locale`='deDE') OR (`entry`=28750 /*28750*/ AND `locale`='deDE') OR (`entry`=29100 /*29100*/ AND `locale`='deDE') OR (`entry`=28751 /*28751*/ AND `locale`='deDE') OR (`entry`=28932 /*28932*/ AND `locale`='deDE') OR (`entry`=28931 /*28931*/ AND `locale`='deDE') OR (`entry`=28739 /*28739*/ AND `locale`='deDE') OR (`entry`=28843 /*28843*/ AND `locale`='deDE') OR (`entry`=28802 /*28802*/ AND `locale`='deDE') OR (`entry`=29654 /*29654*/ AND `locale`='deDE') OR (`entry`=28793 /*28793*/ AND `locale`='deDE') OR (`entry`=28397 /*28397*/ AND `locale`='deDE') OR (`entry`=28398 /*28398*/ AND `locale`='deDE') OR (`entry`=28396 /*28396*/ AND `locale`='deDE') OR (`entry`=28778 /*28778*/ AND `locale`='deDE') OR (`entry`=28205 /*28205*/ AND `locale`='deDE') OR (`entry`=28045 /*28045*/ AND `locale`='deDE') OR (`entry`=28874 /*28874*/ AND `locale`='deDE') OR (`entry`=28844 /*28844*/ AND `locale`='deDE') OR (`entry`=28240 /*28240*/ AND `locale`='deDE') OR (`entry`=28181 /*28181*/ AND `locale`='deDE') OR (`entry`=28023 /*28023*/ AND `locale`='deDE') OR (`entry`=72541 /*72541*/ AND `locale`='deDE') OR (`entry`=70757 /*70757*/ AND `locale`='deDE') OR (`entry`=72538 /*72538*/ AND `locale`='deDE') OR (`entry`=55606 /*55606*/ AND `locale`='deDE') OR (`entry`=28303 /*28303*/ AND `locale`='deDE') OR (`entry`=28026 /*28026*/ AND `locale`='deDE') OR (`entry`=29211 /*29211*/ AND `locale`='deDE') OR (`entry`=28029 /*28029*/ AND `locale`='deDE') OR (`entry`=28798 /*28798*/ AND `locale`='deDE') OR (`entry`=28794 /*28794*/ AND `locale`='deDE') OR (`entry`=28309 /*28309*/ AND `locale`='deDE') OR (`entry`=28028 /*28028*/ AND `locale`='deDE') OR (`entry`=28022 /*28022*/ AND `locale`='deDE') OR (`entry`=28059 /*28059*/ AND `locale`='deDE') OR (`entry`=28039 /*28039*/ AND `locale`='deDE') OR (`entry`=28790 /*28790*/ AND `locale`='deDE') OR (`entry`=29133 /*29133*/ AND `locale`='deDE') OR (`entry`=28099 /*28099*/ AND `locale`='deDE') OR (`entry`=28056 /*28056*/ AND `locale`='deDE') OR (`entry`=16509 /*16509*/ AND `locale`='deDE') OR (`entry`=28089 /*28089*/ AND `locale`='deDE') OR (`entry`=28062 /*28062*/ AND `locale`='deDE') OR (`entry`=28246 /*28246*/ AND `locale`='deDE') OR (`entry`=29169 /*29169*/ AND `locale`='deDE') OR (`entry`=28791 /*28791*/ AND `locale`='deDE') OR (`entry`=28792 /*28792*/ AND `locale`='deDE') OR (`entry`=28244 /*28244*/ AND `locale`='deDE') OR (`entry`=28175 /*28175*/ AND `locale`='deDE') OR (`entry`=28141 /*28141*/ AND `locale`='deDE') OR (`entry`=28133 /*28133*/ AND `locale`='deDE') OR (`entry`=30156 /*30156*/ AND `locale`='deDE') OR (`entry`=30140 /*30140*/ AND `locale`='deDE') OR (`entry`=30009 /*30009*/ AND `locale`='deDE') OR (`entry`=30007 /*30007*/ AND `locale`='deDE') OR (`entry`=28143 /*28143*/ AND `locale`='deDE') OR (`entry`=28304 /*28304*/ AND `locale`='deDE') OR (`entry`=28064 /*28064*/ AND `locale`='deDE') OR (`entry`=29129 /*29129*/ AND `locale`='deDE') OR (`entry`=16570 /*16570*/ AND `locale`='deDE') OR (`entry`=28042 /*28042*/ AND `locale`='deDE') OR (`entry`=28330 /*28330*/ AND `locale`='deDE') OR (`entry`=62820 /*62820*/ AND `locale`='deDE') OR (`entry`=28799 /*28799*/ AND `locale`='deDE') OR (`entry`=28797 /*28797*/ AND `locale`='deDE') OR (`entry`=28796 /*28796*/ AND `locale`='deDE') OR (`entry`=28177 /*28177*/ AND `locale`='deDE') OR (`entry`=28176 /*28176*/ AND `locale`='deDE') OR (`entry`=28178 /*28178*/ AND `locale`='deDE') OR (`entry`=29416 /*29416*/ AND `locale`='deDE') OR (`entry`=28800 /*28800*/ AND `locale`='deDE') OR (`entry`=28204 /*28204*/ AND `locale`='deDE') OR (`entry`=28801 /*28801*/ AND `locale`='deDE') OR (`entry`=28623 /*28623*/ AND `locale`='deDE') OR (`entry`=28068 /*28068*/ AND `locale`='deDE') OR (`entry`=28034 /*28034*/ AND `locale`='deDE') OR (`entry`=28036 /*28036*/ AND `locale`='deDE') OR (`entry`=28041 /*28041*/ AND `locale`='deDE') OR (`entry`=28035 /*28035*/ AND `locale`='deDE') OR (`entry`=28221 /*28221*/ AND `locale`='deDE') OR (`entry`=6653 /*6653*/ AND `locale`='deDE') OR (`entry`=61368 /*61368*/ AND `locale`='deDE') OR (`entry`=30098 /*30098*/ AND `locale`='deDE') OR (`entry`=30193 /*30193*/ AND `locale`='deDE') OR (`entry`=30102 /*30102*/ AND `locale`='deDE') OR (`entry`=28162 /*28162*/ AND `locale`='deDE') OR (`entry`=32447 /*32447*/ AND `locale`='deDE') OR (`entry`=28043 /*28043*/ AND `locale`='deDE') OR (`entry`=28324 /*28324*/ AND `locale`='deDE') OR (`entry`=28323 /*28323*/ AND `locale`='deDE') OR (`entry`=28145 /*28145*/ AND `locale`='deDE') OR (`entry`=28090 /*28090*/ AND `locale`='deDE') OR (`entry`=28305 /*28305*/ AND `locale`='deDE') OR (`entry`=28413 /*28413*/ AND `locale`='deDE') OR (`entry`=28412 /*28412*/ AND `locale`='deDE') OR (`entry`=28158 /*28158*/ AND `locale`='deDE') OR (`entry`=28156 /*28156*/ AND `locale`='deDE') OR (`entry`=28284 /*28284*/ AND `locale`='deDE') OR (`entry`=28283 /*28283*/ AND `locale`='deDE') OR (`entry`=28125 /*28125*/ AND `locale`='deDE') OR (`entry`=28117 /*28117*/ AND `locale`='deDE') OR (`entry`=28044 /*28044*/ AND `locale`='deDE') OR (`entry`=29026 /*29026*/ AND `locale`='deDE') OR (`entry`=28137 /*28137*/ AND `locale`='deDE') OR (`entry`=28260 /*28260*/ AND `locale`='deDE') OR (`entry`=28258 /*28258*/ AND `locale`='deDE') OR (`entry`=28274 /*28274*/ AND `locale`='deDE') OR (`entry`=28255 /*28255*/ AND `locale`='deDE') OR (`entry`=28257 /*28257*/ AND `locale`='deDE') OR (`entry`=28352 /*28352*/ AND `locale`='deDE') OR (`entry`=28848 /*28848*/ AND `locale`='deDE') OR (`entry`=28401 /*28401*/ AND `locale`='deDE') OR (`entry`=28403 /*28403*/ AND `locale`='deDE') OR (`entry`=28402 /*28402*/ AND `locale`='deDE') OR (`entry`=28829 /*28829*/ AND `locale`='deDE') OR (`entry`=33007 /*33007*/ AND `locale`='deDE') OR (`entry`=28527 /*28527*/ AND `locale`='deDE') OR (`entry`=28496 /*28496*/ AND `locale`='deDE') OR (`entry`=33008 /*33008*/ AND `locale`='deDE') OR (`entry`=29583 /*29583*/ AND `locale`='deDE') OR (`entry`=28831 /*28831*/ AND `locale`='deDE') OR (`entry`=28827 /*28827*/ AND `locale`='deDE') OR (`entry`=28479 /*28479*/ AND `locale`='deDE') OR (`entry`=30039 /*30039*/ AND `locale`='deDE') OR (`entry`=28828 /*28828*/ AND `locale`='deDE') OR (`entry`=28504 /*28504*/ AND `locale`='deDE') OR (`entry`=28388 /*28388*/ AND `locale`='deDE') OR (`entry`=28575 /*28575*/ AND `locale`='deDE') OR (`entry`=28478 /*28478*/ AND `locale`='deDE') OR (`entry`=28863 /*28863*/ AND `locale`='deDE') OR (`entry`=28387 /*28387*/ AND `locale`='deDE') OR (`entry`=28442 /*28442*/ AND `locale`='deDE') OR (`entry`=28411 /*28411*/ AND `locale`='deDE') OR (`entry`=28452 /*28452*/ AND `locale`='deDE') OR (`entry`=28561 /*28561*/ AND `locale`='deDE') OR (`entry`=28416 /*28416*/ AND `locale`='deDE') OR (`entry`=28417 /*28417*/ AND `locale`='deDE') OR (`entry`=28600 /*28600*/ AND `locale`='deDE') OR (`entry`=28465 /*28465*/ AND `locale`='deDE') OR (`entry`=28597 /*28597*/ AND `locale`='deDE') OR (`entry`=28418 /*28418*/ AND `locale`='deDE') OR (`entry`=28916 /*28916*/ AND `locale`='deDE') OR (`entry`=28902 /*28902*/ AND `locale`='deDE') OR (`entry`=28917 /*28917*/ AND `locale`='deDE') OR (`entry`=28918 /*28918*/ AND `locale`='deDE') OR (`entry`=28882 /*28882*/ AND `locale`='deDE') OR (`entry`=33025 /*33025*/ AND `locale`='deDE') OR (`entry`=30569 /*30569*/ AND `locale`='deDE') OR (`entry`=28988 /*28988*/ AND `locale`='deDE') OR (`entry`=28952 /*28952*/ AND `locale`='deDE') OR (`entry`=26298 /*26298*/ AND `locale`='deDE') OR (`entry`=28717 /*28717*/ AND `locale`='deDE') OR (`entry`=29332 /*29332*/ AND `locale`='deDE') OR (`entry`=29235 /*29235*/ AND `locale`='deDE') OR (`entry`=28851 /*28851*/ AND `locale`='deDE') OR (`entry`=28852 /*28852*/ AND `locale`='deDE') OR (`entry`=28202 /*28202*/ AND `locale`='deDE') OR (`entry`=29237 /*29237*/ AND `locale`='deDE') OR (`entry`=29236 /*29236*/ AND `locale`='deDE') OR (`entry`=28861 /*28861*/ AND `locale`='deDE') OR (`entry`=29334 /*29334*/ AND `locale`='deDE') OR (`entry`=28233 /*28233*/ AND `locale`='deDE') OR (`entry`=28779 /*28779*/ AND `locale`='deDE') OR (`entry`=28672 /*28672*/ AND `locale`='deDE') OR (`entry`=28671 /*28671*/ AND `locale`='deDE') OR (`entry`=28784 /*28784*/ AND `locale`='deDE') OR (`entry`=28477 /*28477*/ AND `locale`='deDE') OR (`entry`=62693 /*62693*/ AND `locale`='deDE') OR (`entry`=29319 /*29319*/ AND `locale`='deDE') OR (`entry`=28404 /*28404*/ AND `locale`='deDE') OR (`entry`=29327 /*29327*/ AND `locale`='deDE') OR (`entry`=27321 /*27321*/ AND `locale`='deDE') OR (`entry`=27275 /*27275*/ AND `locale`='deDE') OR (`entry`=26681 /*26681*/ AND `locale`='deDE') OR (`entry`=27212 /*27212*/ AND `locale`='deDE') OR (`entry`=27075 /*27075*/ AND `locale`='deDE') OR (`entry`=27115 /*27115*/ AND `locale`='deDE') OR (`entry`=26406 /*26406*/ AND `locale`='deDE') OR (`entry`=26410 /*26410*/ AND `locale`='deDE') OR (`entry`=27114 /*27114*/ AND `locale`='deDE') OR (`entry`=26409 /*26409*/ AND `locale`='deDE') OR (`entry`=27177 /*27177*/ AND `locale`='deDE') OR (`entry`=27113 /*27113*/ AND `locale`='deDE') OR (`entry`=26347 /*26347*/ AND `locale`='deDE') OR (`entry`=26414 /*26414*/ AND `locale`='deDE') OR (`entry`=26885 /*26885*/ AND `locale`='deDE') OR (`entry`=26883 /*26883*/ AND `locale`='deDE') OR (`entry`=26407 /*26407*/ AND `locale`='deDE') OR (`entry`=26408 /*26408*/ AND `locale`='deDE') OR (`entry`=27523 /*27523*/ AND `locale`='deDE') OR (`entry`=27018 /*27018*/ AND `locale`='deDE') OR (`entry`=27579 /*27579*/ AND `locale`='deDE') OR (`entry`=27578 /*27578*/ AND `locale`='deDE') OR (`entry`=27580 /*27580*/ AND `locale`='deDE') OR (`entry`=27020 /*27020*/ AND `locale`='deDE') OR (`entry`=27024 /*27024*/ AND `locale`='deDE') OR (`entry`=26811 /*26811*/ AND `locale`='deDE') OR (`entry`=26812 /*26812*/ AND `locale`='deDE') OR (`entry`=26937 /*26937*/ AND `locale`='deDE') OR (`entry`=26494 /*26494*/ AND `locale`='deDE') OR (`entry`=26484 /*26484*/ AND `locale`='deDE') OR (`entry`=26856 /*26856*/ AND `locale`='deDE') OR (`entry`=26857 /*26857*/ AND `locale`='deDE') OR (`entry`=30367 /*30367*/ AND `locale`='deDE') OR (`entry`=26420 /*26420*/ AND `locale`='deDE') OR (`entry`=26855 /*26855*/ AND `locale`='deDE') OR (`entry`=26418 /*26418*/ AND `locale`='deDE') OR (`entry`=26820 /*26820*/ AND `locale`='deDE') OR (`entry`=29277 /*29277*/ AND `locale`='deDE') OR (`entry`=26886 /*26886*/ AND `locale`='deDE') OR (`entry`=26884 /*26884*/ AND `locale`='deDE') OR (`entry`=26919 /*26919*/ AND `locale`='deDE') OR (`entry`=26789 /*26789*/ AND `locale`='deDE') OR (`entry`=26795 /*26795*/ AND `locale`='deDE') OR (`entry`=26797 /*26797*/ AND `locale`='deDE') OR (`entry`=26814 /*26814*/ AND `locale`='deDE') OR (`entry`=26469 /*26469*/ AND `locale`='deDE') OR (`entry`=26470 /*26470*/ AND `locale`='deDE') OR (`entry`=26334 /*26334*/ AND `locale`='deDE') OR (`entry`=26335 /*26335*/ AND `locale`='deDE') OR (`entry`=26468 /*26468*/ AND `locale`='deDE') OR (`entry`=26284 /*26284*/ AND `locale`='deDE') OR (`entry`=26282 /*26282*/ AND `locale`='deDE') OR (`entry`=26514 /*26514*/ AND `locale`='deDE') OR (`entry`=26522 /*26522*/ AND `locale`='deDE') OR (`entry`=26348 /*26348*/ AND `locale`='deDE') OR (`entry`=26268 /*26268*/ AND `locale`='deDE') OR (`entry`=26270 /*26270*/ AND `locale`='deDE') OR (`entry`=26261 /*26261*/ AND `locale`='deDE') OR (`entry`=26260 /*26260*/ AND `locale`='deDE') OR (`entry`=51893 /*51893*/ AND `locale`='deDE') OR (`entry`=26417 /*26417*/ AND `locale`='deDE') OR (`entry`=27646 /*27646*/ AND `locale`='deDE') OR (`entry`=24921 /*24921*/ AND `locale`='deDE') OR (`entry`=26416 /*26416*/ AND `locale`='deDE') OR (`entry`=27626 /*27626*/ AND `locale`='deDE') OR (`entry`=27627 /*27627*/ AND `locale`='deDE') OR (`entry`=104410 /*104410*/ AND `locale`='deDE') OR (`entry`=26971 /*26971*/ AND `locale`='deDE') OR (`entry`=26935 /*26935*/ AND `locale`='deDE') OR (`entry`=26389 /*26389*/ AND `locale`='deDE') OR (`entry`=33224 /*33224*/ AND `locale`='deDE') OR (`entry`=33211 /*33211*/ AND `locale`='deDE') OR (`entry`=26434 /*26434*/ AND `locale`='deDE') OR (`entry`=34381 /*34381*/ AND `locale`='deDE') OR (`entry`=26932 /*26932*/ AND `locale`='deDE') OR (`entry`=26891 /*26891*/ AND `locale`='deDE') OR (`entry`=27582 /*27582*/ AND `locale`='deDE') OR (`entry`=26876 /*26876*/ AND `locale`='deDE') OR (`entry`=26377 /*26377*/ AND `locale`='deDE') OR (`entry`=26375 /*26375*/ AND `locale`='deDE') OR (`entry`=26371 /*26371*/ AND `locale`='deDE') OR (`entry`=26235 /*26235*/ AND `locale`='deDE') OR (`entry`=26234 /*26234*/ AND `locale`='deDE') OR (`entry`=26233 /*26233*/ AND `locale`='deDE') OR (`entry`=26212 /*26212*/ AND `locale`='deDE') OR (`entry`=582 /*582*/ AND `locale`='deDE') OR (`entry`=27944 /*27944*/ AND `locale`='deDE') OR (`entry`=26361 /*26361*/ AND `locale`='deDE') OR (`entry`=26226 /*26226*/ AND `locale`='deDE') OR (`entry`=30357 /*30357*/ AND `locale`='deDE') OR (`entry`=26236 /*26236*/ AND `locale`='deDE') OR (`entry`=26362 /*26362*/ AND `locale`='deDE') OR (`entry`=26205 /*26205*/ AND `locale`='deDE') OR (`entry`=26392 /*26392*/ AND `locale`='deDE') OR (`entry`=26229 /*26229*/ AND `locale`='deDE') OR (`entry`=26374 /*26374*/ AND `locale`='deDE') OR (`entry`=26382 /*26382*/ AND `locale`='deDE') OR (`entry`=26388 /*26388*/ AND `locale`='deDE') OR (`entry`=29285 /*29285*/ AND `locale`='deDE') OR (`entry`=26387 /*26387*/ AND `locale`='deDE') OR (`entry`=26217 /*26217*/ AND `locale`='deDE') OR (`entry`=26264 /*26264*/ AND `locale`='deDE') OR (`entry`=27461 /*27461*/ AND `locale`='deDE') OR (`entry`=27460 /*27460*/ AND `locale`='deDE') OR (`entry`=27459 /*27459*/ AND `locale`='deDE') OR (`entry`=27458 /*27458*/ AND `locale`='deDE') OR (`entry`=26472 /*26472*/ AND `locale`='deDE') OR (`entry`=27581 /*27581*/ AND `locale`='deDE') OR (`entry`=27263 /*27263*/ AND `locale`='deDE') OR (`entry`=26646 /*26646*/ AND `locale`='deDE') OR (`entry`=27322 /*27322*/ AND `locale`='deDE') OR (`entry`=27274 /*27274*/ AND `locale`='deDE') OR (`entry`=32377 /*32377*/ AND `locale`='deDE') OR (`entry`=26356 /*26356*/ AND `locale`='deDE') OR (`entry`=29492 /*29492*/ AND `locale`='deDE') OR (`entry`=27230 /*27230*/ AND `locale`='deDE') OR (`entry`=24261 /*24261*/ AND `locale`='deDE') OR (`entry`=27547 /*27547*/ AND `locale`='deDE') OR (`entry`=29275 /*29275*/ AND `locale`='deDE') OR (`entry`=29270 /*29270*/ AND `locale`='deDE') OR (`entry`=27469 /*27469*/ AND `locale`='deDE') OR (`entry`=27408 /*27408*/ AND `locale`='deDE') OR (`entry`=27047 /*27047*/ AND `locale`='deDE') OR (`entry`=27546 /*27546*/ AND `locale`='deDE') OR (`entry`=27277 /*27277*/ AND `locale`='deDE') OR (`entry`=27088 /*27088*/ AND `locale`='deDE') OR (`entry`=27071 /*27071*/ AND `locale`='deDE') OR (`entry`=27066 /*27066*/ AND `locale`='deDE') OR (`entry`=27391 /*27391*/ AND `locale`='deDE') OR (`entry`=27089 /*27089*/ AND `locale`='deDE') OR (`entry`=27068 /*27068*/ AND `locale`='deDE') OR (`entry`=29244 /*29244*/ AND `locale`='deDE') OR (`entry`=29161 /*29161*/ AND `locale`='deDE') OR (`entry`=27070 /*27070*/ AND `locale`='deDE') OR (`entry`=27062 /*27062*/ AND `locale`='deDE') OR (`entry`=26875 /*26875*/ AND `locale`='deDE') OR (`entry`=27295 /*27295*/ AND `locale`='deDE') OR (`entry`=26880 /*26880*/ AND `locale`='deDE') OR (`entry`=27293 /*27293*/ AND `locale`='deDE') OR (`entry`=27072 /*27072*/ AND `locale`='deDE') OR (`entry`=38453 /*38453*/ AND `locale`='deDE') OR (`entry`=29269 /*29269*/ AND `locale`='deDE') OR (`entry`=27468 /*27468*/ AND `locale`='deDE') OR (`entry`=27371 /*27371*/ AND `locale`='deDE') OR (`entry`=26357 /*26357*/ AND `locale`='deDE') OR (`entry`=26589 /*26589*/ AND `locale`='deDE') OR (`entry`=26588 /*26588*/ AND `locale`='deDE') OR (`entry`=27421 /*27421*/ AND `locale`='deDE') OR (`entry`=27555 /*27555*/ AND `locale`='deDE') OR (`entry`=27554 /*27554*/ AND `locale`='deDE') OR (`entry`=26704 /*26704*/ AND `locale`='deDE') OR (`entry`=26700 /*26700*/ AND `locale`='deDE') OR (`entry`=29693 /*29693*/ AND `locale`='deDE') OR (`entry`=26706 /*26706*/ AND `locale`='deDE') OR (`entry`=27416 /*27416*/ AND `locale`='deDE') OR (`entry`=27484 /*27484*/ AND `locale`='deDE') OR (`entry`=26682 /*26682*/ AND `locale`='deDE') OR (`entry`=26583 /*26583*/ AND `locale`='deDE') OR (`entry`=26458 /*26458*/ AND `locale`='deDE') OR (`entry`=26582 /*26582*/ AND `locale`='deDE') OR (`entry`=26570 /*26570*/ AND `locale`='deDE') OR (`entry`=385 /*385*/ AND `locale`='deDE') OR (`entry`=27499 /*27499*/ AND `locale`='deDE') OR (`entry`=27545 /*27545*/ AND `locale`='deDE') OR (`entry`=26544 /*26544*/ AND `locale`='deDE') OR (`entry`=27615 /*27615*/ AND `locale`='deDE') OR (`entry`=26559 /*26559*/ AND `locale`='deDE') OR (`entry`=27941 /*27941*/ AND `locale`='deDE') OR (`entry`=26461 /*26461*/ AND `locale`='deDE') OR (`entry`=26457 /*26457*/ AND `locale`='deDE') OR (`entry`=27424 /*27424*/ AND `locale`='deDE') OR (`entry`=27676 /*27676*/ AND `locale`='deDE') OR (`entry`=26821 /*26821*/ AND `locale`='deDE') OR (`entry`=27486 /*27486*/ AND `locale`='deDE') OR (`entry`=26708 /*26708*/ AND `locale`='deDE') OR (`entry`=26679 /*26679*/ AND `locale`='deDE') OR (`entry`=27464 /*27464*/ AND `locale`='deDE') OR (`entry`=27423 /*27423*/ AND `locale`='deDE') OR (`entry`=27422 /*27422*/ AND `locale`='deDE') OR (`entry`=27451 /*27451*/ AND `locale`='deDE') OR (`entry`=27456 /*27456*/ AND `locale`='deDE') OR (`entry`=27425 /*27425*/ AND `locale`='deDE') OR (`entry`=27414 /*27414*/ AND `locale`='deDE') OR (`entry`=27413 /*27413*/ AND `locale`='deDE') OR (`entry`=27475 /*27475*/ AND `locale`='deDE') OR (`entry`=27354 /*27354*/ AND `locale`='deDE') OR (`entry`=27463 /*27463*/ AND `locale`='deDE') OR (`entry`=27481 /*27481*/ AND `locale`='deDE') OR (`entry`=27457 /*27457*/ AND `locale`='deDE') OR (`entry`=27482 /*27482*/ AND `locale`='deDE') OR (`entry`=27264 /*27264*/ AND `locale`='deDE') OR (`entry`=26369 /*26369*/ AND `locale`='deDE') OR (`entry`=91632 /*91632*/ AND `locale`='deDE') OR (`entry`=26519 /*26519*/ AND `locale`='deDE') OR (`entry`=23745 /*23745*/ AND `locale`='deDE') OR (`entry`=26516 /*26516*/ AND `locale`='deDE') OR (`entry`=26424 /*26424*/ AND `locale`='deDE') OR (`entry`=26423 /*26423*/ AND `locale`='deDE') OR (`entry`=26604 /*26604*/ AND `locale`='deDE') OR (`entry`=26474 /*26474*/ AND `locale`='deDE') OR (`entry`=26422 /*26422*/ AND `locale`='deDE') OR (`entry`=23766 /*23766*/ AND `locale`='deDE') OR (`entry`=23764 /*23764*/ AND `locale`='deDE') OR (`entry`=23762 /*23762*/ AND `locale`='deDE') OR (`entry`=23565 /*23565*/ AND `locale`='deDE') OR (`entry`=23747 /*23747*/ AND `locale`='deDE') OR (`entry`=29740 /*29740*/ AND `locale`='deDE') OR (`entry`=27719 /*27719*/ AND `locale`='deDE') OR (`entry`=27262 /*27262*/ AND `locale`='deDE') OR (`entry`=27133 /*27133*/ AND `locale`='deDE') OR (`entry`=27134 /*27134*/ AND `locale`='deDE') OR (`entry`=27132 /*27132*/ AND `locale`='deDE') OR (`entry`=27720 /*27720*/ AND `locale`='deDE') OR (`entry`=26852 /*26852*/ AND `locale`='deDE') OR (`entry`=27266 /*27266*/ AND `locale`='deDE') OR (`entry`=26868 /*26868*/ AND `locale`='deDE') OR (`entry`=27388 /*27388*/ AND `locale`='deDE') OR (`entry`=27102 /*27102*/ AND `locale`='deDE') OR (`entry`=27037 /*27037*/ AND `locale`='deDE') OR (`entry`=26863 /*26863*/ AND `locale`='deDE') OR (`entry`=26862 /*26862*/ AND `locale`='deDE') OR (`entry`=26860 /*26860*/ AND `locale`='deDE') OR (`entry`=29160 /*29160*/ AND `locale`='deDE') OR (`entry`=27204 /*27204*/ AND `locale`='deDE') OR (`entry`=27125 /*27125*/ AND `locale`='deDE') OR (`entry`=26864 /*26864*/ AND `locale`='deDE') OR (`entry`=27479 /*27479*/ AND `locale`='deDE') OR (`entry`=27470 /*27470*/ AND `locale`='deDE') OR (`entry`=26869 /*26869*/ AND `locale`='deDE') OR (`entry`=26839 /*26839*/ AND `locale`='deDE') OR (`entry`=62818 /*62818*/ AND `locale`='deDE') OR (`entry`=27265 /*27265*/ AND `locale`='deDE') OR (`entry`=26366 /*26366*/ AND `locale`='deDE') OR (`entry`=62819 /*62819*/ AND `locale`='deDE') OR (`entry`=26513 /*26513*/ AND `locale`='deDE') OR (`entry`=27613 /*27613*/ AND `locale`='deDE') OR (`entry`=27617 /*27617*/ AND `locale`='deDE') OR (`entry`=28243 /*28243*/ AND `locale`='deDE') OR (`entry`=26425 /*26425*/ AND `locale`='deDE') OR (`entry`=27452 /*27452*/ AND `locale`='deDE') OR (`entry`=26447 /*26447*/ AND `locale`='deDE') OR (`entry`=26498 /*26498*/ AND `locale`='deDE') OR (`entry`=26446 /*26446*/ AND `locale`='deDE') OR (`entry`=27532 /*27532*/ AND `locale`='deDE') OR (`entry`=27565 /*27565*/ AND `locale`='deDE') OR (`entry`=27563 /*27563*/ AND `locale`='deDE') OR (`entry`=27606 /*27606*/ AND `locale`='deDE') OR (`entry`=27550 /*27550*/ AND `locale`='deDE') OR (`entry`=27628 /*27628*/ AND `locale`='deDE') OR (`entry`=27593 /*27593*/ AND `locale`='deDE') OR (`entry`=27702 /*27702*/ AND `locale`='deDE') OR (`entry`=27511 /*27511*/ AND `locale`='deDE') OR (`entry`=27830 /*27830*/ AND `locale`='deDE') OR (`entry`=27509 /*27509*/ AND `locale`='deDE') OR (`entry`=27570 /*27570*/ AND `locale`='deDE') OR (`entry`=27496 /*27496*/ AND `locale`='deDE') OR (`entry`=27501 /*27501*/ AND `locale`='deDE') OR (`entry`=27500 /*27500*/ AND `locale`='deDE') OR (`entry`=27602 /*27602*/ AND `locale`='deDE') OR (`entry`=27562 /*27562*/ AND `locale`='deDE') OR (`entry`=27520 /*27520*/ AND `locale`='deDE') OR (`entry`=27549 /*27549*/ AND `locale`='deDE') OR (`entry`=27495 /*27495*/ AND `locale`='deDE') OR (`entry`=27326 /*27326*/ AND `locale`='deDE') OR (`entry`=27783 /*27783*/ AND `locale`='deDE') OR (`entry`=27120 /*27120*/ AND `locale`='deDE') OR (`entry`=26592 /*26592*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(27118, 'deDE', 'Räuber von Burg Siegeswall', '', NULL, NULL, 23420), -- 27118
(27117, 'deDE', 'Späher von Ammertann', '', NULL, NULL, 23420), -- 27117
(24897, 'deDE', '"Winzling" Jimb', '', 'Grezzixs erster Maat', NULL, 23420), -- 24897
(24896, 'deDE', 'Lou der Kabinenjunge', '', NULL, NULL, 23420), -- 24896
(24643, 'deDE', 'Grezzix Achsenbruch', '', 'Meister der Verkleidung', NULL, 23420), -- 24643
(24767, 'deDE', 'Molly', '', NULL, NULL, 23420), -- 24767
(24845, 'deDE', 'Baelgun''s Event Generator (Cave)', '', NULL, NULL, 23420), -- 24845
(24828, 'deDE', 'Transportbot C1->C2', '', NULL, NULL, 23420), -- 24828
(24827, 'deDE', 'Transportbot B1->B2', '', NULL, NULL, 23420), -- 24827
(24826, 'deDE', 'Transportbot A1->A2', '', NULL, NULL, 23420), -- 24826
(24831, 'deDE', 'Transportbot D2', '', NULL, NULL, 23420), -- 24831
(24829, 'deDE', 'Transportbot D1->D2', '', NULL, NULL, 23420), -- 24829
(24832, 'deDE', 'Transportbot D3', '', NULL, NULL, 23420), -- 24832
(25224, 'deDE', 'Rachsüchtiger Kvaldirgeist', '', NULL, NULL, 23420), -- 25224
(23886, 'deDE', 'Löwenrobbenbulle', '', NULL, NULL, 23420), -- 23886
(29559, 'deDE', 'Löwenrobbenwelpe', '', NULL, NULL, 23420), -- 29559
(23887, 'deDE', 'Löwenrobbe', '', NULL, NULL, 23420), -- 23887
(23983, 'deDE', 'Marinesoldat der Nordflotte', '', NULL, NULL, 23420), -- 23983
(23982, 'deDE', 'Deckmatrose der Verlassenen', '', NULL, NULL, 23420), -- 23982
(23934, 'deDE', 'Berger der Nordflotte', '', NULL, NULL, 23420), -- 23934
(23780, 'deDE', 'Hochexekutor Anselm', '', NULL, NULL, 23420), -- 23780
(23809, 'deDE', 'Kanonier des Hafens der Vergeltung', '', NULL, NULL, 23420), -- 23809
(1921, 'deDE', 'Kampfattrappe', '', NULL, NULL, 23420), -- 1921
(26955, 'deDE', 'Jamesina Watterly', '', 'Ingenieurskunstlehrerin', NULL, 23420), -- 26955
(23938, 'deDE', 'Pontius', '', NULL, NULL, 23420), -- 23938
(23939, 'deDE', 'Unhold', '', NULL, NULL, 23420), -- 23939
(24350, 'deDE', 'Robert Clarke', '', 'Stallmeister', NULL, 23420), -- 24350
(23930, 'deDE', 'Dressierter Seuchenhund', '', NULL, NULL, 23420), -- 23930
(24342, 'deDE', 'Timothy Holland', '', 'Gastwirt', NULL, 23420), -- 24342
(26964, 'deDE', 'Alexandra McQueen', '', 'Schneiderlehrerin', NULL, 23420), -- 26964
(32774, 'deDE', 'Sebastian Kranich', '', 'Rüstmeister der Expedition der Horde', NULL, 23420), -- 32774
(24348, 'deDE', 'Patrick Hall', '', 'Gemischtwaren', NULL, 23420), -- 24348
(24341, 'deDE', 'Barnabas Braater', '', 'Handwerkswaren', NULL, 23420), -- 24341
(29159, 'deDE', 'Magister Varenthas', '', NULL, NULL, 23420), -- 29159
(27344, 'deDE', 'Adeline Kammerer', '', 'Fledermausführerin', NULL, 23420), -- 27344
(26953, 'deDE', 'Thomas Kolichio', '', 'Kochkunstlehrer', NULL, 23420), -- 26953
(24343, 'deDE', 'Brock Olson', '', 'Metzger', NULL, 23420), -- 24343
(23816, 'deDE', 'Fledermausführerin Kamilla', '', 'Fledermausführerin', NULL, 23420), -- 23816
(47568, 'deDE', 'Ian Thomas Wall', '', 'Archäologielehrer', NULL, 23420), -- 47568
(29421, 'deDE', 'Nicole Morris', '', NULL, NULL, 23420), -- 29421
(29420, 'deDE', 'Jessica Morris', '', NULL, NULL, 23420), -- 29420
(29419, 'deDE', 'Julian Morris', '', NULL, NULL, 23420), -- 29419
(29418, 'deDE', 'Jason Morris', '', NULL, NULL, 23420), -- 29418
(26962, 'deDE', 'Jonathan Lewis', '', 'Bergbaulehrer', NULL, 23420), -- 26962
(26960, 'deDE', 'Carter Tiffens', '', 'Juwelierskunstlehrer', NULL, 23420), -- 26960
(26959, 'deDE', 'Buchmacher Kells', '', 'Inschriftenkundelehrer', NULL, 23420), -- 26959
(26956, 'deDE', 'Sally Tompkins', '', 'Lehrerin für Erste Hilfe', NULL, 23420), -- 26956
(26952, 'deDE', 'Kristen Schmydt', '', 'Schmiedekunstlehrerin', NULL, 23420), -- 26952
(24347, 'deDE', 'Alexis Wanderer', '', 'Rüstungsschmiedin', NULL, 23420), -- 24347
(29302, 'deDE', 'Todeswache Hemil', '', NULL, NULL, 23420), -- 29302
(26957, 'deDE', 'Angelina Soren', '', 'Angellehrerin', NULL, 23420), -- 26957
(24116, 'deDE', 'Späher der Winterskorn', '', NULL, NULL, 23420), -- 24116
(26954, 'deDE', 'Emil Herbst', '', 'Verzauberkunstlehrer', NULL, 23420), -- 26954
(26951, 'deDE', 'Wilhelmina Renel', '', 'Alchemielehrerin', NULL, 23420), -- 26951
(24126, 'deDE', 'Apotheker Lysander', '', 'Königliche Apothekervereinigung', NULL, 23420), -- 24126
(23781, 'deDE', 'Apothekerin Celestine', '', 'Königliche Apothekervereinigung', NULL, 23420), -- 23781
(31716, 'deDE', 'Himmelskapitänin Kryoflug', '', 'Die Wolkenkuss', NULL, 23420), -- 31716
(25075, 'deDE', 'Zeppelinsteuerung', '', NULL, NULL, 23420), -- 25075
(26963, 'deDE', 'Roberta Jacks', '', 'Kürschnerlehrerin', NULL, 23420), -- 26963
(26961, 'deDE', 'Gunter Hansen', '', 'Lederverarbeitungslehrer', NULL, 23420), -- 26961
(26958, 'deDE', 'Marjory Kains', '', 'Kräuterkundelehrerin', NULL, 23420), -- 26958
(24349, 'deDE', 'Jessica Evans', '', 'Reagenzien & Gifte', NULL, 23420), -- 24349
(31715, 'deDE', 'Todeswache Hicks', '', 'Die Wolkenkuss', NULL, 23420), -- 31715
(31708, 'deDE', 'Todeswache Barth', '', 'Die Wolkenkuss', NULL, 23420), -- 31708
(31706, 'deDE', 'Besatzungsmitglied Spindelbolzen', '', 'Die Wolkenkuss', NULL, 23420), -- 31706
(31705, 'deDE', 'Besatzungsmitglied Spulenspann', '', 'Die Wolkenkuss', NULL, 23420), -- 31705
(31704, 'deDE', 'Besatzungsmitglied Drehrad', '', 'Die Wolkenkuss', NULL, 23420), -- 31704
(26540, 'deDE', 'Drenk Spannfunke', '', 'Zeppelinmeister, Tirisfal', NULL, 23420), -- 26540
(23779, 'deDE', 'Todeswache des Hafens der Vergeltung', '', NULL, NULL, 23420), -- 23779
(5202, 'deDE', 'Zielscheibe', '', NULL, NULL, 23420), -- 5202
(29422, 'deDE', 'Rekrut der Verlassenen', '', NULL, NULL, 23420), -- 29422
(24128, 'deDE', 'Wilder Worg', '', NULL, NULL, 23420), -- 24128
(23945, 'deDE', 'Fjordkrähe', '', NULL, NULL, 23420), -- 23945
(23998, 'deDE', 'Todespirscher Razael', '', NULL, NULL, 23420), -- 23998
(23778, 'deDE', 'Dunkle Waldläuferin Lyana', '', NULL, NULL, 23420), -- 23778
(23915, 'deDE', 'Westguard Cannon Trigger', '', NULL, NULL, 23420), -- 23915
(23947, 'deDE', 'Westguard Ranged Trigger', '', NULL, NULL, 23420), -- 23947
(23885, 'deDE', 'Lyana Trigger', '', NULL, NULL, 23420), -- 23885
(23957, 'deDE', 'Westguard Cannon Credit Trigger East', '', NULL, NULL, 23420), -- 23957
(23805, 'deDE', 'Vengeance Landing Cannon Controller', '', NULL, NULL, 23420), -- 23805
(23916, 'deDE', 'Westguard Cannon Trigger (Water)', '', NULL, NULL, 23420), -- 23916
(23963, 'deDE', 'Unteroffizier Lorric', '', NULL, NULL, 23420), -- 23963
(23962, 'deDE', 'Hauptmann Olster', '', NULL, NULL, 23420), -- 23962
(23866, 'deDE', 'Seemann der Nordflotte', '', NULL, NULL, 23420), -- 23866
(23793, 'deDE', 'Soldat der Nordflotte', '', NULL, NULL, 23420), -- 23793
(23946, 'deDE', 'Schütze der Nordflotte', '', NULL, NULL, 23420), -- 23946
(23917, 'deDE', 'Cannon Source Trigger', '', NULL, NULL, 23420), -- 23917
(23794, 'deDE', 'Sanitäter der Nordflotte', '', NULL, NULL, 23420), -- 23794
(23964, 'deDE', 'Leutnant Celeyne', '', NULL, NULL, 23420), -- 23964
(23784, 'deDE', 'Apotheker Hanes', '', 'Königliche Apothekervereinigung', NULL, 23420), -- 23784
(23929, 'deDE', 'Riesiger Gezeitenkrabbler', '', NULL, NULL, 23420), -- 23929
(24811, 'deDE', 'Donny', '', 'Forscherliga', NULL, 23420), -- 24811
(23968, 'deDE', 'Hanes Fire Trigger', '', NULL, NULL, 23420), -- 23968
(23711, 'deDE', 'Eisenrunenarbeiter', '', NULL, NULL, 23420), -- 23711
(23674, 'deDE', 'Eisenrunenweise', '', NULL, NULL, 23420), -- 23674
(23676, 'deDE', 'Eisenrunenzerstörer', '', NULL, NULL, 23420), -- 23676
(23972, 'deDE', 'Westguard Cannon Credit Trigger West', '', NULL, NULL, 23420), -- 23972
(23803, 'deDE', 'Vengeance Landing Cannon Trigger', '', NULL, NULL, 23420), -- 23803
(23867, 'deDE', 'Vengeance Landing Combatant Trigger', '', NULL, NULL, 23420), -- 23867
(71163, 'deDE', 'Ungeborene Val''kyr', '', NULL, 'wildpetcapturable', 23420), -- 71163
(24750, 'deDE', 'Hidalgo der Meisterfalkner', '', NULL, NULL, 23420), -- 24750
(24751, 'deDE', 'Excelsior', '', 'Hidalgos Begleiter', NULL, 23420), -- 24751
(24752, 'deDE', 'Steinfalke', '', NULL, NULL, 23420), -- 24752
(24787, 'deDE', 'Fjordhabichtmatriarchin', '', NULL, NULL, 23420), -- 24787
(26934, 'deDE', 'Everett McGill', '', 'Versorger der Forscherliga', NULL, 23420), -- 26934
(24807, 'deDE', 'Walt', '', 'Forscherliga', NULL, 23420), -- 24807
(24717, 'deDE', 'Stanwad', '', 'Forscherliga', NULL, 23420), -- 24717
(23675, 'deDE', 'Eisenrunenmeister', '', NULL, NULL, 23420), -- 23675
(23672, 'deDE', 'Eisenrunentagelöhner', '', NULL, NULL, 23420), -- 23672
(24820, 'deDE', 'Eisenzwergrelikt', '', NULL, NULL, 23420), -- 24820
(24817, 'deDE', 'Explorers'' League Event Controller', '', NULL, NULL, 23420), -- 24817
(24720, 'deDE', 'Nadelmeyer', '', 'Forscherliga', NULL, 23420), -- 24720
(24719, 'deDE', 'Gwendolyn', '', 'Forscherliga', NULL, 23420), -- 24719
(24718, 'deDE', 'Lebronski', '', 'Forscherliga', NULL, 23420), -- 24718
(24340, 'deDE', 'Tobender Erdelementar', '', NULL, NULL, 23420), -- 24340
(24824, 'deDE', 'Eisenzwergrelikt', '', NULL, NULL, 23420), -- 24824
(24271, 'deDE', 'Eisenrunengolem', '', NULL, NULL, 23420), -- 24271
(23673, 'deDE', 'Eisenrunenstahlwache', '', NULL, NULL, 23420), -- 23673
(24746, 'deDE', 'Fjordtruthahn', '', NULL, NULL, 23420), -- 24746
(27922, 'deDE', 'Waldläuferhauptmann Areiel', '', NULL, NULL, 23420), -- 27922
(24548, 'deDE', 'Schreiberin Seguine', '', NULL, NULL, 23420), -- 24548
(23776, 'deDE', 'Dunkle Waldläuferin', '', NULL, NULL, 23420), -- 23776
(32573, 'deDE', 'Gefangener von Valgarde', '', NULL, NULL, 23420), -- 32573
(23994, 'deDE', 'Jagdhund der Drachenschinder', '', NULL, NULL, 23420), -- 23994
(24335, 'deDE', 'Runenbeschriebene Kugel', '', NULL, NULL, 23420), -- 24335
(24334, 'deDE', 'Binder Murdis', '', NULL, NULL, 23420), -- 24334
(24336, 'deDE', 'Kugelziel', '', NULL, NULL, 23420), -- 24336
(24345, 'deDE', 'Gefangener Steinriese', '', NULL, NULL, 23420), -- 24345
(24030, 'deDE', 'Eisenrunensteinrufer', '', NULL, NULL, 23420), -- 24030
(23796, 'deDE', 'Eisenrunenbinder', '', NULL, NULL, 23420), -- 23796
(24316, 'deDE', 'Eisenrunenschildwache', '', NULL, NULL, 23420), -- 24316
(24212, 'deDE', 'Eisenrunenwächter', '', NULL, NULL, 23420), -- 24212
(23725, 'deDE', 'Steinriese', '', NULL, NULL, 23420), -- 23725
(28314, 'deDE', 'Fernläufer Nanik', '', NULL, NULL, 23420), -- 28314
(28313, 'deDE', 'Apotheker Scyllis', '', 'Königliche Apothekervereinigung', NULL, 23420), -- 28313
(23883, 'deDE', 'Armbrustschütze der Verlassenen', '', NULL, NULL, 23420), -- 23883
(24027, 'deDE', 'Unteroffizier Gorth', '', NULL, NULL, 23420), -- 24027
(24161, 'deDE', 'Oric der Elende', '', NULL, NULL, 23420), -- 24161
(23865, 'deDE', 'Vergeltungsbringer', '', NULL, NULL, 23420), -- 23865
(24238, 'deDE', 'Björn Halgurdsson', '', 'Häuptling der Winterskorn', NULL, 23420), -- 24238
(24634, 'deDE', 'Leutnant Eishammer', '', NULL, NULL, 23420), -- 24634
(23670, 'deDE', 'Ältester der Winterskorn', '', NULL, NULL, 23420), -- 23670
(24015, 'deDE', 'Verteidiger der Winterskorn', '', NULL, NULL, 23420), -- 24015
(23884, 'deDE', 'Vengeance Landing Crossbow Trigger', '', NULL, NULL, 23420), -- 23884
(23657, 'deDE', 'Skalde der Winterskorn', '', NULL, NULL, 23420), -- 23657
(24185, 'deDE', 'Winterskorn Bridge Credit', '', NULL, NULL, 23420), -- 24185
(24183, 'deDE', 'Winterskorn Watchtower Credit', '', NULL, NULL, 23420), -- 24183
(23655, 'deDE', 'Knochenschinder der Winterskorn', '', NULL, NULL, 23420), -- 23655
(24474, 'deDE', 'Assassine des Hafens der Vergeltung', '', NULL, NULL, 23420), -- 24474
(24458, 'deDE', 'Lydell', '', NULL, NULL, 23420), -- 24458
(24467, 'deDE', 'Leichnam eines Vrykul', '', NULL, NULL, 23420), -- 24467
(24184, 'deDE', 'Winterskorn Barracks Credit', '', NULL, NULL, 23420), -- 24184
(23653, 'deDE', 'Speerträger der Winterskorn', '', NULL, NULL, 23420), -- 23653
(26826, 'deDE', 'Häuptling der Heulenden Wolvar', '', NULL, NULL, 23420), -- 26826
(29487, 'deDE', 'Wilder Schaufelhauer', '', NULL, NULL, 23420), -- 29487
(24178, 'deDE', 'Schmetterhorn', '', NULL, NULL, 23420), -- 24178
(24229, 'deDE', 'Heulender Zyklon', '', NULL, NULL, 23420), -- 24229
(23744, 'deDE', 'Eishöhlenungetüm', '', NULL, NULL, 23420), -- 23744
(24062, 'deDE', 'Minenarbeiter von Wildervar', '', NULL, NULL, 23420), -- 24062
(24515, 'deDE', 'Geflecktes Hippogryphenjungtier', '', NULL, NULL, 23420), -- 24515
(24176, 'deDE', 'Vorarbeiter Colbey', '', NULL, NULL, 23420), -- 24176
(24376, 'deDE', 'Forscher Aderan', '', NULL, NULL, 23420), -- 24376
(24139, 'deDE', 'Gil Grisert', '', NULL, NULL, 23420), -- 24139
(24131, 'deDE', 'Fallensteller Jethan', '', NULL, NULL, 23420), -- 24131
(24054, 'deDE', 'Balar Rumvernichter', '', 'Handwerkswaren', NULL, 23420), -- 24054
(24053, 'deDE', 'Helga Rumvernichter', '', 'Gemischtwaren', NULL, 23420), -- 24053
(24057, 'deDE', 'Christina Daniels', '', 'Gastwirtin', NULL, 23420), -- 24057
(24532, 'deDE', 'Sabetta Wehr', '', NULL, NULL, 23420), -- 24532
(24531, 'deDE', 'Gavin Wehr', '', NULL, NULL, 23420), -- 24531
(24328, 'deDE', 'Ausgrabungsleiter Belvar', '', NULL, NULL, 23420), -- 24328
(24282, 'deDE', 'Leutnant Maeve', '', NULL, NULL, 23420), -- 24282
(24066, 'deDE', 'Artie Grauhand', '', 'Stallmeister', NULL, 23420), -- 24066
(24061, 'deDE', 'James Ormsby', '', 'Greifenmeister', NULL, 23420), -- 24061
(24534, 'deDE', 'Marrod Silberzunge', '', NULL, NULL, 23420), -- 24534
(24535, 'deDE', 'Nordendsiedler', '', NULL, NULL, 23420), -- 24535
(24528, 'deDE', 'Brune Grauklinge', '', NULL, NULL, 23420), -- 24528
(24052, 'deDE', 'Eldrim Hügler', '', 'Schmied', NULL, 23420), -- 24052
(24058, 'deDE', 'Tagelöhner von Wildervar', '', NULL, NULL, 23420), -- 24058
(24524, 'deDE', 'Packmuli der Siedler', '', NULL, NULL, 23420), -- 24524
(24055, 'deDE', 'Dorgan Eisenformer', '', NULL, NULL, 23420), -- 24055
(24514, 'deDE', 'Stahlfeder', '', NULL, NULL, 23420), -- 24514
(24056, 'deDE', 'Christopher Sloan', '', NULL, NULL, 23420), -- 24056
(24117, 'deDE', 'Lurielle', '', NULL, NULL, 23420), -- 24117
(23677, 'deDE', 'Frostnymphe', '', NULL, NULL, 23420), -- 23677
(23919, 'deDE', 'Eiselementar', '', NULL, NULL, 23420), -- 23919
(23875, 'deDE', 'Schwarzbeseelter Bewahrer', '', NULL, NULL, 23420), -- 23875
(24638, 'deDE', 'Hüter Welkblatt', '', NULL, NULL, 23420), -- 24638
(23876, 'deDE', 'Spore', '', NULL, NULL, 23420), -- 23876
(23874, 'deDE', 'Dornrankenkriecher', '', NULL, NULL, 23420), -- 23874
(24050, 'deDE', 'Wachposten von Wildervar', '', NULL, NULL, 23420), -- 24050
(26488, 'deDE', 'Lastkodo der Taunka', '', NULL, NULL, 23420), -- 26488
(29301, 'deDE', 'Wanderer von Camp Winterhuf', '', NULL, NULL, 23420), -- 29301
(24863, 'deDE', 'Frosthornzicklein', '', NULL, NULL, 23420), -- 24863
(24228, 'deDE', 'Eissplitterelementar', '', NULL, NULL, 23420), -- 24228
(24033, 'deDE', 'Bori Wintertotem', '', 'Gastwirt', NULL, 23420), -- 24033
(24142, 'deDE', 'Windreiter vom Lager der Winterhufe', '', NULL, NULL, 23420), -- 24142
(24067, 'deDE', 'Mahana Frosthuf', '', 'Stallmeisterin', NULL, 23420), -- 24067
(24032, 'deDE', 'Celea Frostmähne', '', 'Windreitermeisterin', NULL, 23420), -- 24032
(24390, 'deDE', 'Weiser Edan', '', NULL, NULL, 23420), -- 24390
(24234, 'deDE', 'Junat der Wanderer', '', NULL, NULL, 23420), -- 24234
(24135, 'deDE', 'Großmutter Ankha', '', NULL, NULL, 23420), -- 24135
(24129, 'deDE', 'Häuptling Aschtotem', '', NULL, NULL, 23420), -- 24129
(24127, 'deDE', 'Ahota Weißfrost', '', NULL, NULL, 23420), -- 24127
(24362, 'deDE', 'Fernläufer Pembe', '', NULL, NULL, 23420), -- 24362
(24123, 'deDE', 'Nokoma Schneeseher', '', NULL, NULL, 23420), -- 24123
(24028, 'deDE', 'Talu Frosthuf', '', 'Bogenmacher', NULL, 23420), -- 24028
(24256, 'deDE', 'Windzähmer Kagan', '', NULL, NULL, 23420), -- 24256
(24236, 'deDE', 'Windzähmer', '', NULL, NULL, 23420), -- 24236
(24235, 'deDE', 'Zyklothar', '', NULL, NULL, 23420), -- 24235
(24031, 'deDE', 'Kriegerheld vom Lager der Winterhufe', '', NULL, NULL, 23420), -- 24031
(27701, 'deDE', 'Arbeiter des Gjalerhorn', '', NULL, NULL, 23420), -- 27701
(27699, 'deDE', 'Strauchdieb des Gjalerhorn', '', NULL, NULL, 23420), -- 27699
(23740, 'deDE', 'Frosthornbock', '', NULL, NULL, 23420), -- 23740
(24102, 'deDE', 'Skorn Barracks Bunny', '', NULL, NULL, 23420), -- 24102
(24092, 'deDE', 'Skorn Tower E Bunny', '', NULL, NULL, 23420), -- 24092
(24094, 'deDE', 'Skorn Tower SE Bunny', '', NULL, NULL, 23420), -- 24094
(24093, 'deDE', 'Skorn Tower SW Bunny', '', NULL, NULL, 23420), -- 24093
(23668, 'deDE', 'Runenmagier der Winterskorn', '', NULL, NULL, 23420), -- 23668
(29486, 'deDE', 'Gezähmter Schaufelhauer', '', NULL, NULL, 23420), -- 29486
(26823, 'deDE', 'Ausbilder der Heulenden Wolvar', '', NULL, NULL, 23420), -- 26823
(26827, 'deDE', 'Aufklärer der Heulenden Wolvar', '', NULL, NULL, 23420), -- 26827
(26825, 'deDE', 'Schamane der Heulenden Wolvar', '', NULL, NULL, 23420), -- 26825
(23772, 'deDE', 'Gefleckter Hippogryph', '', NULL, NULL, 23420), -- 23772
(24196, 'deDE', 'Gefangenes Tier', '', NULL, NULL, 23420), -- 24196
(23678, 'deDE', 'Eisnymphe', '', NULL, NULL, 23420), -- 23678
(24100, 'deDE', 'Skorn Longhouse NE Bunny', '', NULL, NULL, 23420), -- 24100
(23664, 'deDE', 'Krieger der Winterskorn', '', NULL, NULL, 23420), -- 23664
(23667, 'deDE', 'Runenseher der Winterskorn', '', NULL, NULL, 23420), -- 23667
(24087, 'deDE', 'Skorn Tower NW Bunny', '', NULL, NULL, 23420), -- 24087
(23666, 'deDE', 'Berserker der Winterskorn', '', NULL, NULL, 23420), -- 23666
(23669, 'deDE', 'Orakel der Winterskorn', '', NULL, NULL, 23420), -- 23669
(24098, 'deDE', 'Skorn Longhouse NW Bunny', '', NULL, NULL, 23420), -- 24098
(23663, 'deDE', 'Schildmaid der Winterskorn', '', NULL, NULL, 23420), -- 23663
(23661, 'deDE', 'Stammesangehöriger der Winterskorn', '', NULL, NULL, 23420), -- 23661
(23662, 'deDE', 'Förster der Winterskorn', '', NULL, NULL, 23420), -- 23662
(23665, 'deDE', 'Räuber der Winterskorn', '', NULL, NULL, 23420), -- 23665
(33303, 'deDE', 'Maid des Winterhauchsees', '', NULL, NULL, 23420), -- 33303
(24029, 'deDE', 'Wyrmrufer Elend', '', NULL, NULL, 23420), -- 24029
(24023, 'deDE', 'Königin Angerboda', '', NULL, NULL, 23420), -- 24023
(29393, 'deDE', 'König Ymiron', '', NULL, NULL, 23420), -- 29393
(24018, 'deDE', 'Nekrogroßfürst Mezhen', '', NULL, NULL, 23420), -- 24018
(23993, 'deDE', 'Grabaufseher', '', NULL, NULL, 23420), -- 23993
(23992, 'deDE', 'Eitrige Kreatur', '', NULL, NULL, 23420), -- 23992
(23989, 'deDE', 'Schlafwächter von Gjalerbron', '', NULL, NULL, 23420), -- 23989
(24035, 'deDE', 'Gefangener von Gjalerbron', '', NULL, NULL, 23420), -- 24035
(23990, 'deDE', 'Runenmagier von Gjalerbron', '', NULL, NULL, 23420), -- 23990
(24014, 'deDE', 'Nekrolord', '', NULL, NULL, 23420), -- 24014
(24669, 'deDE', 'Schlafender Vrykul', '', NULL, NULL, 23420), -- 24669
(24073, 'deDE', 'Furchterregender Schrecken', '', NULL, NULL, 23420), -- 24073
(24013, 'deDE', 'Ewiger Beobachter', '', NULL, NULL, 23420), -- 24013
(23991, 'deDE', 'Krieger von Gjalerbron', '', NULL, NULL, 23420), -- 23991
(24901, 'deDE', 'Wahnsinniges Frosthorn', '', NULL, NULL, 23420), -- 24901
(24273, 'deDE', 'Behüterin Mondblatt', '', NULL, NULL, 23420), -- 24273
(24227, 'deDE', 'Ingenieur Feknut', '', NULL, NULL, 23420), -- 24227
(23891, 'deDE', 'Vorarbeiterin Irena Zasterstein', '', NULL, NULL, 23420), -- 23891
(24209, 'deDE', 'Fernläufer Himmelswolke', '', NULL, NULL, 23420), -- 24209
(24473, 'deDE', 'Chefarchäologe Malzie', '', 'Leiter der archäologischen Expedition', NULL, 23420), -- 24473
(24401, 'deDE', 'Packmuli des Stählernen Tores', '', NULL, NULL, 23420), -- 24401
(24195, 'deDE', 'Fernläufer der Winterhufe', '', NULL, NULL, 23420), -- 24195
(24186, 'deDE', 'Weiser Nebelläufer', '', NULL, NULL, 23420), -- 24186
(23033, 'deDE', 'Unsichtbarer Pirscher', '', NULL, NULL, 23420), -- 23033
(24516, 'deDE', 'Bjomolf', '', NULL, NULL, 23420), -- 24516
(24197, 'deDE', 'Scharfschütze der Westwacht', '', NULL, NULL, 23420), -- 24197
(24517, 'deDE', 'Varg', '', NULL, NULL, 23420), -- 24517
(24418, 'deDE', 'Flugmaschine des Stählernen Tores', '', NULL, 'vehichleCursor', 23420), -- 24418
(24399, 'deDE', 'Chefarchäologe des Stählernen Tores', '', 'Forscherliga', NULL, 23420), -- 24399
(24400, 'deDE', 'Archäologe des Stählernen Tores', '', 'Forscherliga', NULL, 23420), -- 24400
(24439, 'deDE', 'Sack voller Relikte', '', NULL, NULL, 23420), -- 24439
(24398, 'deDE', 'Ausgräber des Stählernen Tores', '', 'Forscherliga', NULL, 23420), -- 24398
(24262, 'deDE', 'Vrykulseele', '', NULL, NULL, 23420), -- 24262
(24019, 'deDE', 'Glacion', '', NULL, NULL, 23420), -- 24019
(23958, 'deDE', 'Bruchwitwe', '', NULL, NULL, 23420), -- 23958
(23959, 'deDE', 'Dunkelklauenfledermaus', '', NULL, NULL, 23420), -- 23959
(24210, 'deDE', 'Bruchwitwenkokon', '', NULL, NULL, 23420), -- 24210
(24313, 'deDE', 'Celina Sommers', '', 'Reagenzien & Gifte', NULL, 23420), -- 24313
(26844, 'deDE', 'Lilleth Radescu', '', 'Fledermausführerin', NULL, 23420), -- 26844
(24218, 'deDE', 'Apotheker Grick', '', NULL, NULL, 23420), -- 24218
(24188, 'deDE', 'Samuel Rosmond', '', 'Versorger', NULL, 23420), -- 24188
(24152, 'deDE', 'Apotheker Malthus', '', 'Königliche Apothekervereinigung', NULL, 23420), -- 24152
(24359, 'deDE', 'Apothekerin Anastasia', '', 'Königliche Apothekervereinigung', NULL, 23420), -- 24359
(24317, 'deDE', 'Todeswache der Expedition', '', NULL, NULL, 23420), -- 24317
(66635, 'deDE', 'Beegli Lunte', '', 'Meistertierzähmer', NULL, 23420), -- 66635
(66615, 'deDE', 'Broiler', '', NULL, NULL, 23420), -- 66615
(66614, 'deDE', 'Gubbel', '', NULL, NULL, 23420), -- 66614
(66613, 'deDE', 'Flöter', '', NULL, NULL, 23420), -- 66613
(26363, 'deDE', 'Großhornhirsch', '', NULL, NULL, 23420), -- 26363
(27131, 'deDE', 'Grizzly', '', NULL, NULL, 23420), -- 27131
(26785, 'deDE', 'Richtrune', '', NULL, NULL, 23420), -- 26785
(27377, 'deDE', 'Than Torvald Eriksson', '', NULL, NULL, 23420), -- 27377
(27297, 'deDE', 'Kette des Flammenbringers', '', NULL, NULL, 23420), -- 27297
(31889, 'deDE', 'Grizzlyeichhörnchen', '', NULL, NULL, 23420), -- 31889
(27260, 'deDE', 'Huscarl der Drachenschinder', '', NULL, NULL, 23420), -- 27260
(27259, 'deDE', 'Flammenbinderin der Drachenschinder', '', NULL, NULL, 23420), -- 27259
(26591, 'deDE', 'Pacer Bunny - Drak Theron Exterior', '', NULL, NULL, 23420), -- 26591
(27689, 'deDE', 'Explosionen auf dem Holzfrachter der Allianz', '', NULL, NULL, 23420), -- 27689
(27688, 'deDE', 'Holzfrachter der Allianz', '', NULL, NULL, 23420), -- 27688
(27292, 'deDE', 'Flammenbringer', '', NULL, NULL, 23420), -- 27292
(24544, 'deDE', 'Alte Eisflosse', '', NULL, NULL, 23420), -- 24544
(23833, 'deDE', 'Forscher Jaren', '', NULL, NULL, 23420), -- 23833
(24460, 'deDE', 'Gezeitenjäger des Frostmaares', '', NULL, NULL, 23420), -- 24460
(23643, 'deDE', 'Instabiler Mur''ghul', '', NULL, NULL, 23420), -- 23643
(24540, 'deDE', 'Nekrotech', '', NULL, NULL, 23420), -- 24540
(24485, 'deDE', 'Schattendiener', '', NULL, NULL, 23420), -- 24485
(24464, 'deDE', 'Geißelnder Kristall', '', NULL, NULL, 23420), -- 24464
(24461, 'deDE', 'Orakel des Frostmaares', '', NULL, NULL, 23420), -- 24461
(24459, 'deDE', 'Küstenläufer des Frostmaares', '', NULL, NULL, 23420), -- 24459
(23645, 'deDE', 'Verderbender Mur''ghul', '', NULL, NULL, 23420), -- 23645
(23644, 'deDE', 'Fleischfressender Mur''ghul', '', NULL, NULL, 23420), -- 23644
(24048, 'deDE', 'Squeeg Götzenjäger', '', 'Forscherliga', NULL, 23420), -- 24048
(29479, 'deDE', 'Schaufelhauerfutterwühler', '', NULL, NULL, 23420), -- 29479
(24026, 'deDE', 'Fangkrallenworg', '', NULL, NULL, 23420), -- 24026
(25233, 'deDE', 'Lunkhauer', '', NULL, NULL, 23420), -- 25233
(23860, 'deDE', 'Lissan Weiß', '', NULL, NULL, 23420), -- 23860
(23984, 'deDE', 'Aufgestalltes Packmuli der Forscherliga', '', NULL, NULL, 23420), -- 23984
(23985, 'deDE', 'Gezähmter Adler der Westwacht', '', NULL, NULL, 23420), -- 23985
(23986, 'deDE', 'Aufgestallter Widder der Westwacht', '', NULL, NULL, 23420), -- 23986
(23859, 'deDE', 'Greer Erzhammer', '', 'Greifenmeister', NULL, 23420), -- 23859
(23974, 'deDE', 'Whisper Gulch Ore Bunny', '', NULL, NULL, 23420), -- 23974
(23773, 'deDE', 'Küchenchefin Schwarzkessel', '', NULL, NULL, 23420), -- 23773
(29663, 'deDE', 'Foxy', '', 'Chelseas Hund', NULL, 23420), -- 29663
(29662, 'deDE', 'Beau', '', 'Chelseas Pferd', NULL, 23420), -- 29662
(29658, 'deDE', 'Chelsea Reese', '', 'Stallmeisterin', NULL, 23420), -- 29658
(23987, 'deDE', 'Aufgestalltes Pferd der Westwacht', '', NULL, NULL, 23420), -- 23987
(23978, 'deDE', 'Forscherin Abigail', '', NULL, NULL, 23420), -- 23978
(23831, 'deDE', 'Der alte Zasterstein', '', NULL, NULL, 23420), -- 23831
(26900, 'deDE', 'Tinky Stabberson', '', 'Reagenzien & Gifte', NULL, 23420), -- 26900
(23937, 'deDE', 'Gastwirtin Celeste Gutstall', '', 'Gastwirtin', NULL, 23420), -- 23937
(24283, 'deDE', 'Peppi Falschdüse', '', NULL, NULL, 23420), -- 24283
(24494, 'deDE', 'Rüstmeister Brevin', '', NULL, NULL, 23420), -- 24494
(24333, 'deDE', 'Jason Gutstall', '', 'Barkeeper', NULL, 23420), -- 24333
(23862, 'deDE', 'Finlay Pfeilmacher', '', NULL, NULL, 23420), -- 23862
(23977, 'deDE', 'Verlassenes Packmuli', '', NULL, NULL, 23420), -- 23977
(24356, 'deDE', 'Glen Roberts', '', 'Handwerker', NULL, 23420), -- 24356
(23911, 'deDE', 'Arbeiter der Westwacht', '', NULL, NULL, 23420), -- 23911
(24284, 'deDE', 'Gefangener Vrykul der Drachenschinder', '', NULL, NULL, 23420), -- 24284
(23908, 'deDE', 'Jhet Eisenbart', '', 'Schmiedekunstbedarf', NULL, 23420), -- 23908
(23888, 'deDE', 'Magierleutnant Malister', '', NULL, NULL, 23420), -- 23888
(23976, 'deDE', 'Pionier Stahlring', '', NULL, NULL, 23420), -- 23976
(23749, 'deDE', 'Hauptmann Adams', '', NULL, NULL, 23420), -- 23749
(24038, 'deDE', 'Vater Levariol', '', NULL, NULL, 23420), -- 24038
(23840, 'deDE', 'Armbrustschütze der Westwacht', '', NULL, NULL, 23420), -- 23840
(23895, 'deDE', 'Bombenschütze Petrov', '', NULL, NULL, 23420), -- 23895
(28157, 'deDE', 'Bombenschütze der Westwacht', '', NULL, NULL, 23420), -- 28157
(23823, 'deDE', 'Eggenmeiser', '', 'Gefangener Zeppelinbesitzer', NULL, 23420), -- 23823
(24289, 'deDE', 'Invisible Westguard Fire', '', NULL, NULL, 23420), -- 24289
(29881, 'deDE', 'Eine unbekannte Stimme', '', NULL, NULL, 23420), -- 29881
(24546, 'deDE', 'Rottkiem', '', NULL, NULL, 23420), -- 24546
(23771, 'deDE', 'Kanone der Blockade', '', NULL, NULL, 23420), -- 23771
(23755, 'deDE', 'Blockadenpirat', '', NULL, NULL, 23420), -- 23755
(23810, 'deDE', 'Blockade Explosion Bunny', '', NULL, NULL, 23420), -- 23810
(24478, 'deDE', 'Fjordkriecher', '', NULL, NULL, 23420), -- 24478
(27151, 'deDE', 'Deniigi', '', 'Speermacher', NULL, 23420), -- 27151
(27146, 'deDE', 'Uukkarnit', '', 'Handwerkswaren', NULL, 23420), -- 27146
(29966, 'deDE', 'Schildkrötenreiter von Kamagua', '', NULL, NULL, 23420), -- 29966
(24755, 'deDE', 'Urahn Atuik', '', NULL, NULL, 23420), -- 24755
(28197, 'deDE', 'Kip Netzschlepper', '', 'Flugmeister', NULL, 23420), -- 28197
(27145, 'deDE', 'Tipvigut', '', 'Gemischtwaren', NULL, 23420), -- 27145
(26558, 'deDE', 'Nutaaq', '', 'Dockmeister', NULL, 23420), -- 26558
(27149, 'deDE', 'Arrluk', '', 'Gifte & Reagenzien', NULL, 23420), -- 27149
(31916, 'deDE', 'Tanaika', '', 'Rüstmeister der Kalu''ak', NULL, 23420), -- 31916
(27144, 'deDE', 'Noatak', '', 'Fischhändler', NULL, 23420), -- 27144
(24810, 'deDE', 'Anuniaq', '', NULL, NULL, 23420), -- 24810
(24763, 'deDE', 'Akiaruk', '', NULL, NULL, 23420), -- 24763
(27150, 'deDE', 'Trapper Shesh', '', 'Stallmeister', NULL, 23420), -- 27150
(27167, 'deDE', 'Speerträger von Kamagua', '', NULL, NULL, 23420), -- 27167
(27148, 'deDE', 'Versorger Iqniq', '', 'Gastwirt', NULL, 23420), -- 27148
(23638, 'deDE', 'Fischer der Langhauer', '', NULL, NULL, 23420), -- 23638
(24754, 'deDE', 'Iskaalflüchtling', '', NULL, NULL, 23420), -- 24754
(24786, 'deDE', 'Riffbulle', '', NULL, NULL, 23420), -- 24786
(61158, 'deDE', 'Uferkrebs', '', NULL, 'wildpetcapturable', 23420), -- 61158
(24673, 'deDE', 'Frostflügelschimäre', '', NULL, NULL, 23420), -- 24673
(24797, 'deDE', 'Riffkuh', '', NULL, NULL, 23420), -- 24797
(24681, 'deDE', 'Inselschaufelhauer', '', NULL, NULL, 23420), -- 24681
(24789, 'deDE', 'Unglückselige Seele', '', NULL, NULL, 23420), -- 24789
(24677, 'deDE', 'Worg der Speerfänge', '', NULL, NULL, 23420), -- 24677
(62669, 'deDE', 'Fjordworgwelpe', '', NULL, 'wildpetcapturable', 23420), -- 62669
(24678, 'deDE', 'Erschlagener Tuskarr', '', NULL, NULL, 23420), -- 24678
(24900, 'deDE', 'Abdul der Wahnsinnige', '', NULL, NULL, 23420), -- 24900
(24902, 'deDE', 'Abdul', '', NULL, NULL, 23420), -- 24902
(24676, 'deDE', 'Wahnsinniger Sklavenhändler des Nordmeeres', '', NULL, NULL, 23420), -- 24676
(24913, 'deDE', 'Kanone "Gnädige Schwester"', '', NULL, NULL, 23420), -- 24913
(24992, 'deDE', 'Die Dicke Berta', '', NULL, 'Interact', 23420), -- 24992
(17213, 'deDE', 'Besen', '', NULL, NULL, 23420), -- 17213
(25026, 'deDE', 'Meuternder Seemann', '', NULL, NULL, 23420), -- 25026
(24742, 'deDE', 'Der verrückte Jonah Sterling', '', NULL, NULL, 23420), -- 24742
(24713, 'deDE', '"Krähenbein" Dan', '', NULL, NULL, 23420), -- 24713
(24899, 'deDE', '"Bodo von der Hermannsklause"', '', NULL, NULL, 23420), -- 24899
(24714, 'deDE', 'Erfahrener Bukanier', '', NULL, NULL, 23420), -- 24714
(24539, 'deDE', '"Silbermond" Harry', '', NULL, NULL, 23420), -- 24539
(24542, 'deDE', 'Rotauge Ben', '', NULL, NULL, 23420), -- 24542
(29151, 'deDE', 'Grille', '', NULL, NULL, 23420), -- 29151
(24543, 'deDE', 'Die tätowierte Marcy', '', NULL, NULL, 23420), -- 24543
(24639, 'deDE', 'Olga, die Ganovendirne', '', NULL, NULL, 23420), -- 24639
(28277, 'deDE', 'Harrys Flugmaschine', '', NULL, NULL, 23420), -- 28277
(24741, 'deDE', 'Annie Bonn', '', NULL, NULL, 23420), -- 24741
(24788, 'deDE', 'Jack Adams', '', NULL, NULL, 23420), -- 24788
(24525, 'deDE', 'Zeh''gehn', '', NULL, NULL, 23420), -- 24525
(27933, 'deDE', 'Alanya', '', '"Silbermond" Harrys Gefährtin', NULL, 23420), -- 27933
(24537, 'deDE', 'Der hübsche Terry', '', NULL, NULL, 23420), -- 24537
(24541, 'deDE', 'Taruk', '', NULL, NULL, 23420), -- 24541
(24628, 'deDE', 'Nordmeerduellant', '', NULL, NULL, 23420), -- 24628
(26503, 'deDE', 'Ganovenfrosch', '', NULL, NULL, 23420), -- 26503
(24784, 'deDE', 'Lukchen Frostbug', '', NULL, NULL, 23420), -- 24784
(24633, 'deDE', 'Tollwütiger Braunbär', '', NULL, NULL, 23420), -- 24633
(24642, 'deDE', 'Betrunkener Nordmeerpirat', '', NULL, NULL, 23420), -- 24642
(5936, 'deDE', 'Orca', '', NULL, NULL, 23420), -- 5936
(24956, 'deDE', 'Juniper', '', NULL, NULL, 23420), -- 24956
(24973, 'deDE', 'Ellis Crew Trigger', '', NULL, NULL, 23420), -- 24973
(24911, 'deDE', 'Verfluchter Seemann', '', NULL, NULL, 23420), -- 24911
(24910, 'deDE', 'Kapitän Ellis', '', NULL, NULL, 23420), -- 24910
(24637, 'deDE', 'Großer Riffhai', '', NULL, NULL, 23420), -- 24637
(24914, 'deDE', 'Sorlof', '', NULL, NULL, 23420), -- 24914
(60761, 'deDE', 'Uferkrebs', '', NULL, NULL, 23420), -- 60761
(23940, 'deDE', 'Skeld Drachensohn', '', NULL, NULL, 23420), -- 23940
(23871, 'deDE', 'Tierführer der Drachenschinder', '', NULL, NULL, 23420), -- 23871
(23851, 'deDE', 'Kavallerist der Westwacht', '', NULL, NULL, 23420), -- 23851
(23844, 'deDE', 'Offizier der Westwacht', '', NULL, NULL, 23420), -- 23844
(23770, 'deDE', 'Kanonier Ely', '', NULL, NULL, 23420), -- 23770
(28135, 'deDE', 'Greif der Westwacht', '', NULL, NULL, 23420), -- 28135
(23839, 'deDE', 'Kanonier der Westwacht', '', NULL, NULL, 23420), -- 23839
(23842, 'deDE', 'Verteidiger der Westwacht', '', NULL, NULL, 23420), -- 23842
(23836, 'deDE', 'Holzfäller der Westwacht', '', NULL, NULL, 23420), -- 23836
(23870, 'deDE', 'Urtum der Glutstätte', '', NULL, NULL, 23420), -- 23870
(23777, 'deDE', 'Protodrachenei', '', NULL, NULL, 23420), -- 23777
(23750, 'deDE', 'Protowelpenjungtier', '', NULL, NULL, 23420), -- 23750
(24884, 'deDE', 'Padwai, Sohn des Orfus', '', NULL, NULL, 23420), -- 24884
(23804, 'deDE', 'Orfus von Kamagua', '', NULL, NULL, 23420), -- 23804
(25232, 'deDE', 'Shield Hill Creature Trigger - Fengir', '', NULL, NULL, 23420), -- 25232
(24887, 'deDE', 'Fengir Quest Credit', '', NULL, NULL, 23420), -- 24887
(24874, 'deDE', 'Fengir der Entehrte', '', NULL, NULL, 23420), -- 24874
(25230, 'deDE', 'Shield Hill Creature Trigger - Windan', '', NULL, NULL, 23420), -- 25230
(24875, 'deDE', 'Windan der Kvaldir', '', NULL, NULL, 23420), -- 24875
(25229, 'deDE', 'Shield Hill Creature Trigger - Isuldof', '', NULL, NULL, 23420), -- 25229
(24880, 'deDE', 'Korf', '', 'Isudolfs Begleiter', NULL, 23420), -- 24880
(24877, 'deDE', 'Isuldolf Eisherz', '', NULL, NULL, 23420), -- 24877
(25231, 'deDE', 'Rodin der Tollkühne', '', NULL, NULL, 23420), -- 25231
(24889, 'deDE', 'Rodin der Tollkühne', '', NULL, NULL, 23420), -- 24889
(24876, 'deDE', 'Rodin der Tollkühne', '', NULL, NULL, 23420), -- 24876
(24883, 'deDE', 'Rodin der Tollkühne', '', NULL, NULL, 23420), -- 24883
(24871, 'deDE', 'Auferstandener Vrykulahn', '', NULL, NULL, 23420), -- 24871
(22515, 'deDE', 'World Trigger', '', NULL, NULL, 23420), -- 22515
(24872, 'deDE', 'Blutschemen', '', NULL, NULL, 23420), -- 24872
(24291, 'deDE', 'Anton', '', NULL, NULL, 23420), -- 24291
(24252, 'deDE', '"Eisensäge" Jenny', '', NULL, NULL, 23420), -- 24252
(26984, 'deDE', 'Stephan Franks', '', 'Reagenzienbedarf', NULL, 23420), -- 26984
(24330, 'deDE', 'Orson Locke', '', 'Scharfe Klingen', NULL, 23420), -- 24330
(24251, 'deDE', 'Oberster Seuchenfürst Harris', '', 'Königliche Apothekervereinigung', NULL, 23420), -- 24251
(24148, 'deDE', 'David Marks', '', 'Gifte', NULL, 23420), -- 24148
(24125, 'deDE', 'Cormath der Kurier', '', NULL, NULL, 23420), -- 24125
(24157, 'deDE', 'Seuchenbringer Tillinghast', '', 'Königliche Apothekervereinigung', NULL, 23420), -- 24157
(24768, 'deDE', 'Todespirscher Hayward', '', NULL, NULL, 23420), -- 24768
(24168, 'deDE', 'Micah Steinbrecher', '', NULL, NULL, 23420), -- 24168
(23155, 'deDE', 'Invisible Stalker (Scale x3)', '', NULL, NULL, 23420), -- 23155
(24164, 'deDE', 'Apotheker Dorne', '', 'Königliche Apothekervereinigung', NULL, 23420), -- 24164
(51888, 'deDE', 'Todeswache von Neu-Agamand', '', NULL, NULL, 23420), -- 51888
(24155, 'deDE', 'Tobias Sarkhoff', '', 'Fledermausführer', NULL, 23420), -- 24155
(24081, 'deDE', 'Apothekersgehilfe', '', 'Königliche Apothekervereinigung', NULL, 23420), -- 24081
(27926, 'deDE', 'Thorvald', '', 'Häuptling von Halgrind', NULL, 23420), -- 27926
(27927, 'deDE', 'Wächter der Drachenschinder', '', NULL, NULL, 23420), -- 27927
(24147, 'deDE', 'Tara Cooper', '', 'Handwerkswaren', NULL, 23420), -- 24147
(24141, 'deDE', 'Stephen Barone', '', 'Gemischtwaren', NULL, 23420), -- 24141
(24154, 'deDE', 'Mary Darrow', '', 'Stallmeisterin', NULL, 23420), -- 24154
(24149, 'deDE', 'Basil Osgud', '', 'Gastwirt', NULL, 23420), -- 24149
(33019, 'deDE', 'Megan Owings', '', 'Schankkellnerin', NULL, 23420), -- 33019
(33018, 'deDE', 'Jennifer Owings', '', 'Speis & Trank', NULL, 23420), -- 33018
(24104, 'deDE', 'Todeswache von Neu-Agamand', '', NULL, NULL, 23420), -- 24104
(24290, 'deDE', 'New Agamand Plague Tank Bunny', '', NULL, NULL, 23420), -- 24290
(23921, 'deDE', 'Halgrind Torch Bunny 01', '', NULL, NULL, 23420), -- 23921
(23922, 'deDE', 'Halgrind Torch Bunny 02', '', NULL, NULL, 23420), -- 23922
(24280, 'deDE', 'Erschlagener Apotheker', '', NULL, NULL, 23420), -- 24280
(23680, 'deDE', 'Verseuchter Protodrache', '', NULL, NULL, 23420), -- 23680
(23923, 'deDE', 'Halgrind Torch Bunny 03', '', NULL, NULL, 23420), -- 23923
(24213, 'deDE', 'Firjus der Seelenmalmer', '', NULL, NULL, 23420), -- 24213
(24258, 'deDE', 'Aufseher der Val''kyr', '', NULL, NULL, 23420), -- 24258
(24255, 'deDE', 'Gefangener der Drachenschinder', '', NULL, NULL, 23420), -- 24255
(24254, 'deDE', 'Gefangener der Drachenschinder', '', NULL, NULL, 23420), -- 24254
(23924, 'deDE', 'Halgrind Torch Bunny 04', '', NULL, NULL, 23420), -- 23924
(23564, 'deDE', 'Verseuchter Stammesangehöriger der Drachenschinder', '', NULL, NULL, 23420), -- 23564
(24199, 'deDE', 'Verseuchter Tierführer der Drachenschinder', '', NULL, NULL, 23420), -- 24199
(24198, 'deDE', 'Verseuchter Runenmagier der Drachenschinder', '', NULL, NULL, 23420), -- 24198
(28359, 'deDE', 'Kundschafter Wisslichs Reittier', '', NULL, NULL, 23420), -- 28359
(23906, 'deDE', 'Kundschafter Wisslich', '', NULL, NULL, 23420), -- 23906
(23689, 'deDE', 'Protodrache', '', NULL, NULL, 23420), -- 23689
(23760, 'deDE', 'Seuchenbringer der Verlassenen', '', NULL, NULL, 23420), -- 23760
(23967, 'deDE', 'Verwirrter Forscher', '', 'Forscherliga', NULL, 23420), -- 23967
(24518, 'deDE', 'Klauenwürger', '', NULL, NULL, 23420), -- 24518
(26663, 'deDE', 'Jägerin der Winterskorn', '', NULL, NULL, 23420), -- 26663
(24076, 'deDE', 'Worg der Winterskorn', '', NULL, NULL, 23420), -- 24076
(23693, 'deDE', 'Dämmerschwingenadler', '', NULL, NULL, 23420), -- 23693
(23691, 'deDE', 'Schaufelhauerhirsch', '', NULL, NULL, 23420), -- 23691
(24791, 'deDE', 'Schaufelhauerkalb', '', NULL, NULL, 23420), -- 24791
(23690, 'deDE', 'Schaufelhauer', '', NULL, NULL, 23420), -- 23690
(24182, 'deDE', 'Winterskorn Dwelling Credit', '', NULL, NULL, 23420), -- 24182
(24194, 'deDE', 'Baleheim Fire Bunny Large', '', NULL, NULL, 23420), -- 24194
(24193, 'deDE', 'Baleheim Fire Bunny', '', NULL, NULL, 23420), -- 24193
(23730, 'deDE', 'Harold Lagras', '', NULL, NULL, 23420), -- 23730
(27993, 'deDE', 'Harpunenkanone der Vrykul', '', NULL, NULL, 23420), -- 27993
(27992, 'deDE', 'Harpunenkanone der Vrykul', '', NULL, NULL, 23420), -- 27992
(24701, 'deDE', 'Große Harpunenkanone der Vrykul', '', NULL, NULL, 23420), -- 24701
(23656, 'deDE', 'Runenseher der Drachenschinder', '', NULL, NULL, 23420), -- 23656
(23654, 'deDE', 'Krieger der Drachenschinder', '', NULL, NULL, 23420), -- 23654
(24221, 'deDE', 'Berserker der Drachenschinder', '', NULL, NULL, 23420), -- 24221
(24253, 'deDE', 'Gefangener der Drachenschinder', '', NULL, NULL, 23420), -- 24253
(32642, 'deDE', 'Mojodishu', '', 'Handelsreisende', NULL, 23420), -- 32642
(32641, 'deDE', 'Drix Finsterzang', '', 'Der Reparaturmeister', NULL, 23420), -- 32641
(24115, 'deDE', 'Leichnam eines Verteidigers von Valgarde', '', NULL, NULL, 23420), -- 24115
(24112, 'deDE', 'Dragonflayer Oracle Corpse', '', NULL, NULL, 23420), -- 24112
(62641, 'deDE', 'Fjordratte', '', NULL, 'wildpetcapturable', 23420), -- 62641
(24114, 'deDE', 'Dragonflayer Thane Corpse', '', NULL, NULL, 23420), -- 24114
(24113, 'deDE', 'Dragonflayer Worg Corpse', '', NULL, NULL, 23420), -- 24113
(24635, 'deDE', 'Harpunierer der Drachenschinder', '', NULL, NULL, 23420), -- 24635
(24189, 'deDE', 'Ares der Schwurgebundene', '', 'Der Argentumkreuzzug', NULL, 23420), -- 24189
(24122, 'deDE', 'Pulroy der Archäologe', '', 'Forscherliga', NULL, 23420), -- 24122
(24086, 'deDE', 'Gefangener Priester von Valgarde', '', NULL, NULL, 23420), -- 24086
(24694, 'deDE', 'Harpunenkanone der Vrykul', '', NULL, NULL, 23420), -- 24694
(24089, 'deDE', 'Gefangener Krieger von Valgarde', '', NULL, NULL, 23420), -- 24089
(23932, 'deDE', 'Yanis der Mystiker', '', 'Herold von Ingvar', NULL, 23420), -- 23932
(24145, 'deDE', 'Zedd', '', 'Forscherliga', NULL, 23420), -- 24145
(24475, 'deDE', 'Blutdürstiger Worg', '', NULL, NULL, 23420), -- 24475
(24158, 'deDE', 'Dragonflayer Oracle Target', '', NULL, NULL, 23420), -- 24158
(24088, 'deDE', 'Gefangener Magier von Valgarde', '', NULL, NULL, 23420), -- 24088
(23658, 'deDE', 'Todesweber der Drachenschinder', '', NULL, NULL, 23420), -- 23658
(24150, 'deDE', 'Glorenfeld', '', 'Forscherliga', NULL, 23420), -- 24150
(23651, 'deDE', 'Stammesangehöriger der Drachenschinder', '', NULL, NULL, 23420), -- 23651
(24090, 'deDE', 'Gefangener Paladin von Valgarde', '', NULL, NULL, 23420), -- 24090
(24226, 'deDE', 'Gefangener der Drachenschinder', '', NULL, NULL, 23420), -- 24226
(23660, 'deDE', 'Than der Drachenschinder', '', NULL, NULL, 23420), -- 23660
(24151, 'deDE', 'Daegarn', '', 'Forscherliga', NULL, 23420), -- 24151
(24216, 'deDE', 'Berserker der Drachenschinder', '', NULL, NULL, 23420), -- 24216
(24106, 'deDE', 'Kundschafterin Valory', '', NULL, NULL, 23420), -- 24106
(24249, 'deDE', 'Seelenhäscher der Drachenschinder', '', NULL, NULL, 23420), -- 24249
(24270, 'deDE', 'Verschlingende Made', '', NULL, NULL, 23420), -- 24270
(24260, 'deDE', 'Seele der Val''kyr', '', NULL, NULL, 23420), -- 24260
(24172, 'deDE', 'Dolchbuchtfalke', '', NULL, NULL, 23420), -- 24172
(62640, 'deDE', 'Verschlingende Made', '', NULL, 'wildpetcapturable', 23420), -- 62640
(24177, 'deDE', 'Verrottender Ghul', '', NULL, NULL, 23420), -- 24177
(24272, 'deDE', 'Behüter der Val''kyr', '', NULL, NULL, 23420), -- 24272
(24169, 'deDE', 'Leutnant der Drachenschinder', '', NULL, NULL, 23420), -- 24169
(24250, 'deDE', 'Fleischreißer der Drachenschinder', '', NULL, NULL, 23420), -- 24250
(24077, 'deDE', 'Aufgespießter Späher von Valgarde', '', NULL, NULL, 23420), -- 24077
(23652, 'deDE', 'Vrykul der Drachenschinder', '', NULL, NULL, 23420), -- 23652
(24538, 'deDE', 'Installation der Drachenschinder', '', NULL, NULL, 23420), -- 24538
(24051, 'deDE', 'Eindringling der Drachenschinder', '', NULL, NULL, 23420), -- 24051
(24063, 'deDE', 'Worg der Drachenschinder', '', NULL, NULL, 23420), -- 24063
(23737, 'deDE', 'Kahl "Der Fremde" Albertson', '', 'Schankkellner', NULL, 23420), -- 23737
(23732, 'deDE', 'Sorely Zwickkling', '', 'Gifte', NULL, 23420), -- 23732
(47578, 'deDE', 'Hugen Goldweis', '', 'Archäologielehrer', NULL, 23420), -- 47578
(26916, 'deDE', 'Mindri Dinkels', '', 'Inschriftenkundelehrerin', NULL, 23420), -- 26916
(26908, 'deDE', 'Helen Süßkind', '', 'Reagenzien', NULL, 23420), -- 26908
(26905, 'deDE', 'Brom Bräugießer', '', 'Kochkunstlehrer', NULL, 23420), -- 26905
(26903, 'deDE', 'Lanolis Tautropfen', '', 'Alchemielehrer', NULL, 23420), -- 26903
(23731, 'deDE', 'Gastwirtin Hazel Lagras', '', 'Gastwirtin', NULL, 23420), -- 23731
(26914, 'deDE', 'Benjamin Clegg', '', 'Schneiderlehrer', NULL, 23420), -- 26914
(26906, 'deDE', 'Elizabeth Jackson', '', 'Verzauberkunstlehrerin', NULL, 23420), -- 26906
(23802, 'deDE', 'Zwinker Rieselresel', '', 'Gemischtwaren & Handwerkswaren', NULL, 23420), -- 23802
(26910, 'deDE', 'Fayin Wisperblatt', '', 'Kräuterkundelehrer', NULL, 23420), -- 26910
(23738, 'deDE', 'Großknecht Miles McMürrisch', '', NULL, NULL, 23420), -- 23738
(23733, 'deDE', 'Horatio der Stalljunge', '', 'Stallmeister', NULL, 23420), -- 23733
(23729, 'deDE', 'Baron Ulrik von Stromheim', '', NULL, NULL, 23420), -- 23729
(24174, 'deDE', 'Fjordratte', '', NULL, NULL, 23420), -- 24174
(23791, 'deDE', 'Arbeiter von Valgarde', '', NULL, NULL, 23420), -- 23791
(24646, 'deDE', 'Installation der Drachenschinder', '', NULL, NULL, 23420), -- 24646
(23975, 'deDE', 'Thoralius der Weise', '', NULL, NULL, 23420), -- 23975
(23548, 'deDE', 'Beltrand McSorf', '', 'Forscherliga', NULL, 23420), -- 23548
(23546, 'deDE', 'Vizeadmiral Keller', '', NULL, NULL, 23420), -- 23546
(24647, 'deDE', 'Installation der Drachenschinder', '', NULL, NULL, 23420), -- 24647
(24707, 'deDE', 'Boje', '', NULL, NULL, 23420), -- 24707
(26901, 'deDE', 'Torik', '', NULL, NULL, 23420), -- 26901
(24233, 'deDE', 'Kleriker des Kreuzzugs', '', 'Der Argentumkreuzzug', NULL, 23420), -- 24233
(24191, 'deDE', 'Lord Irulon Wahrklinge', '', 'Der Argentumkreuzzug', NULL, 23420), -- 24191
(23783, 'deDE', 'Verletzter Verteidiger', '', NULL, NULL, 23420), -- 23783
(23734, 'deDE', 'Anachoretin Yazmina', '', 'Lehrerin für Erste Hilfe', NULL, 23420), -- 23734
(23551, 'deDE', 'Kanonier von Valgarde', '', NULL, NULL, 23420), -- 23551
(23549, 'deDE', 'Rowan Helfgot', '', 'Königliche Gesellschaft der Wissenschaften zu Sturmwind', NULL, 23420), -- 23549
(24040, 'deDE', 'McGoyver', '', 'Pro', NULL, 23420), -- 24040
(23557, 'deDE', 'Räuber der Drachenschinder', '', NULL, NULL, 23420), -- 23557
(23728, 'deDE', 'Wachoffizier Zorek', '', NULL, NULL, 23420), -- 23728
(23735, 'deDE', 'Bartleby Rüstfaust', '', 'Rüstungsschmied', NULL, 23420), -- 23735
(27930, 'deDE', 'Emilune Winterwind', '', NULL, NULL, 23420), -- 27930
(15214, 'deDE', 'Unsichtbarer Pirscher', '', NULL, NULL, 23420), -- 15214
(26904, 'deDE', 'Rosina Kerbstift', '', 'Schmiedekunstlehrerin', NULL, 23420), -- 26904
(23736, 'deDE', 'Pricilla Winterwind', '', 'Greifenmeisterin', NULL, 23420), -- 23736
(32773, 'deDE', 'Logistikoffizier Heller', '', 'Rüstmeister der Vorhut der Allianz', NULL, 23420), -- 32773
(62648, 'deDE', 'Truthahn', '', NULL, 'wildpetcapturable', 23420), -- 62648
(26915, 'deDE', 'Ounhulo', '', 'Juwelierskunstlehrer', NULL, 23420), -- 26915
(26913, 'deDE', 'Frederic Burrhus', '', 'Kürschnerlehrer', NULL, 23420), -- 26913
(26912, 'deDE', 'Grumbol Stämmaxt', '', 'Bergbaulehrer', NULL, 23420), -- 26912
(26911, 'deDE', 'Bernadette Dexter', '', 'Lederverarbeitungslehrerin', NULL, 23420), -- 26911
(26909, 'deDE', 'Byron Welwick', '', 'Angellehrer', NULL, 23420), -- 26909
(26907, 'deDE', 'Tisha Langbrück', '', 'Ingenieurskunstlehrerin', NULL, 23420), -- 26907
(23801, 'deDE', 'Truthahn', '', NULL, NULL, 23420), -- 23801
(26547, 'deDE', 'Basil Raab', '', 'Dockmeister', NULL, 23420), -- 26547
(23821, 'deDE', 'Valgarde Harpoon Target', '', NULL, NULL, 23420), -- 23821
(23739, 'deDE', 'Verteidiger von Valgarde', '', NULL, NULL, 23420), -- 23739
(23550, 'deDE', 'Dienstmann von Valgarde', '', NULL, NULL, 23420), -- 23550
(23547, 'deDE', 'Macalroy', '', NULL, NULL, 23420), -- 23547
(23785, 'deDE', 'Dolchbuchthammerkopf', '', NULL, NULL, 23420), -- 23785
(29484, 'deDE', 'Skelettgreif', '', NULL, NULL, 23420), -- 29484
(28615, 'deDE', 'Bannflug', '', 'Flugmeister', NULL, 23420), -- 28615
(66639, 'deDE', 'Darmwürger', '', 'Meistertierzähmer', NULL, 23420), -- 66639
(66627, 'deDE', 'Kadavus', '', NULL, NULL, 23420), -- 66627
(66626, 'deDE', 'Fleischfetzer', '', NULL, NULL, 23420), -- 66626
(66624, 'deDE', 'Pesthauch', '', NULL, NULL, 23420), -- 66624
(31041, 'deDE', 'Entmutigter Ent', '', NULL, NULL, 23420), -- 31041
(29454, 'deDE', 'Burr', '', NULL, NULL, 23420), -- 29454
(28866, 'deDE', 'Korrosion', '', 'Alchemiebedarf', NULL, 23420), -- 28866
(28518, 'deDE', 'Stefan Vadu', '', NULL, NULL, 23420), -- 28518
(28872, 'deDE', 'Windewurm', '', 'Handwerkswaren', NULL, 23420), -- 28872
(28867, 'deDE', 'Spitzer', '', 'Gemischtwaren', NULL, 23420), -- 28867
(28532, 'deDE', 'Datura Blutrose', '', NULL, NULL, 23420), -- 28532
(28871, 'deDE', 'Degenscheider', '', 'Klingen', NULL, 23420), -- 28871
(28865, 'deDE', 'Posten der Schwarzen Wacht', '', NULL, NULL, 23420), -- 28865
(28870, 'deDE', 'Leichenstaub', '', 'Reagenzien', NULL, 23420), -- 28870
(28869, 'deDE', 'Todestropfen', '', 'Gifte', NULL, 23420), -- 28869
(28868, 'deDE', 'Mulch', '', 'Kräuterkundebedarf', NULL, 23420), -- 28868
(28541, 'deDE', 'Gefangener Späher der Drakkari', '', NULL, NULL, 23420), -- 28541
(29436, 'deDE', 'Eisberührter Erdwüter', '', NULL, NULL, 23420), -- 29436
(29459, 'deDE', 'Vargulattrappe', '', NULL, NULL, 23420), -- 29459
(29450, 'deDE', 'Runenlord der Vargul', '', NULL, NULL, 23420), -- 29450
(29455, 'deDE', 'Gerk', '', NULL, NULL, 23420), -- 29455
(29928, 'deDE', 'Schlossattrappe von Gymer', '', NULL, NULL, 23420), -- 29928
(29647, 'deDE', 'Gymer', '', 'König der Sturmriesen', NULL, 23420), -- 29647
(29449, 'deDE', 'Totenwache der Vargul', '', NULL, NULL, 23420), -- 29449
(29452, 'deDE', 'Pesthund der Vargul', '', NULL, NULL, 23420), -- 29452
(28589, 'deDE', 'Knorpelsack', '', NULL, NULL, 23420), -- 28589
(28599, 'deDE', 'Seuchenkakerlake', '', NULL, NULL, 23420), -- 28599
(29856, 'deDE', 'Glibbriger Ghulsabber', '', NULL, 'LootAll', 23420), -- 29856
(28519, 'deDE', 'Siechender Troll', '', NULL, NULL, 23420), -- 28519
(29691, 'deDE', 'Schilfs Dampfpanzer', '', NULL, NULL, 23420), -- 29691
(29689, 'deDE', 'Kreuzfahrer MacKellar', '', NULL, NULL, 23420), -- 29689
(29688, 'deDE', 'Ingenieur Schilf', '', NULL, NULL, 23420), -- 29688
(29453, 'deDE', 'Seuchenklaue der Vargul', '', NULL, NULL, 23420), -- 29453
(29468, 'deDE', 'Kreuzfahrer Dargath', '', NULL, NULL, 23420), -- 29468
(29451, 'deDE', 'Töter der Vargul', '', NULL, NULL, 23420), -- 29451
(28564, 'deDE', 'Eitrige Monstrosität', '', NULL, NULL, 23420), -- 28564
(28565, 'deDE', 'Zerfallender Ghul', '', NULL, NULL, 23420), -- 28565
(28657, 'deDE', 'Eingesperrter Spuk', '', NULL, NULL, 23420), -- 28657
(28503, 'deDE', 'Oberanführer Drakuru', '', NULL, NULL, 23420), -- 28503
(28879, 'deDE', 'Schieferschwinge', '', NULL, NULL, 23420), -- 28879
(28888, 'deDE', 'Gefangener Häuptling der Drakkari', '', NULL, NULL, 23420), -- 28888
(28803, 'deDE', 'Drakurus Wächter', '', NULL, NULL, 23420), -- 28803
(28666, 'deDE', 'Grindbruch', '', NULL, NULL, 23420), -- 28666
(29698, 'deDE', 'Raptor von Drakuru', '', NULL, NULL, 23420), -- 29698
(29699, 'deDE', 'Raptorenreiter von Drakuru', '', NULL, NULL, 23420), -- 29699
(29848, 'deDE', 'Zornklauenwelpe', '', NULL, NULL, 23420), -- 29848
(28813, 'deDE', 'Elizabeth Hollingsworth', '', NULL, NULL, 23420), -- 28813
(28806, 'deDE', 'Chad Carter', '', 'Handwerkswaren', NULL, 23420), -- 28806
(29656, 'deDE', 'Berserker von Drakuru', '', NULL, NULL, 23420), -- 29656
(29438, 'deDE', 'Primalist der Zornklauen', '', NULL, NULL, 23420), -- 29438
(29437, 'deDE', 'Berserker der Zornklauen', '', NULL, NULL, 23420), -- 29437
(29439, 'deDE', 'Jäger der Zornklauen', '', NULL, NULL, 23420), -- 29439
(29700, 'deDE', 'Fesseln von Drakuru', '', NULL, NULL, 23420), -- 29700
(29692, 'deDE', 'Hüttenfeuer', '', NULL, NULL, 23420), -- 29692
(29686, 'deDE', 'Gefangene Zornklaue', '', NULL, NULL, 23420), -- 29686
(29664, 'deDE', 'Rachmähne', '', NULL, NULL, 23420), -- 29664
(29733, 'deDE', 'Schamanenältester Moky', '', NULL, NULL, 23420), -- 29733
(29690, 'deDE', 'Häuptling Zornklaue', '', NULL, NULL, 23420), -- 29690
(29687, 'deDE', 'Kreuzfahrerlord Lantinga', '', NULL, NULL, 23420), -- 29687
(28812, 'deDE', 'Lapu Sturmhorn', '', 'Fische & Angelbedarf', NULL, 23420), -- 28812
(28807, 'deDE', 'Amarante', '', 'Gemischtwaren', NULL, 23420), -- 28807
(28811, 'deDE', 'Brady Eisenscherbe', '', 'Kochbedarf', NULL, 23420), -- 28811
(28810, 'deDE', 'Lessien', '', 'Schneiderbedarf', NULL, 23420), -- 28810
(28809, 'deDE', 'Vincent Huber', '', 'Reagenzien', NULL, 23420), -- 28809
(29137, 'deDE', 'Unteroffizierin Riannah', '', 'Flugmeisterin', NULL, 23420), -- 29137
(28818, 'deDE', 'Verteidiger der Lichtbresche', '', NULL, NULL, 23420), -- 28818
(28618, 'deDE', 'Danica Heilig', '', 'Flugmeisterin', NULL, 23420), -- 28618
(29697, 'deDE', 'Prophet von Drakuru', '', NULL, NULL, 23420), -- 29697
(29917, 'deDE', 'Field Corpse (Type B)', '', NULL, NULL, 23420), -- 29917
(29916, 'deDE', 'Field Corpse (Type A)', '', NULL, NULL, 23420), -- 29916
(29646, 'deDE', 'Bansheeseelengreifer', '', NULL, NULL, 23420), -- 29646
(28761, 'deDE', 'Geist Spawn Bunny', '', NULL, NULL, 23420), -- 28761
(28617, 'deDE', 'Drakuramas Teleport Bunny 01', '', NULL, NULL, 23420), -- 28617
(28759, 'deDE', 'Fliegendes Scheusal', '', NULL, NULL, 23420), -- 28759
(28750, 'deDE', 'Seuchenspuk', '', NULL, NULL, 23420), -- 28750
(29100, 'deDE', 'Totally Generic Bunny x8.0 (JSB)', '', NULL, NULL, 23420), -- 29100
(28751, 'deDE', 'Geist WP Bunny', '', NULL, NULL, 23420), -- 28751
(28932, 'deDE', 'Blight Effect Bunny', '', NULL, NULL, 23420), -- 28932
(28931, 'deDE', 'Seuchenbluttroll', '', NULL, NULL, 23420), -- 28931
(28739, 'deDE', 'Blight Cauldron Bunny 00', '', NULL, NULL, 23420), -- 28739
(28843, 'deDE', 'Aufgeblähte Monstrosität', '', NULL, NULL, 23420), -- 28843
(28802, 'deDE', 'Diener von Drakuru', '', NULL, NULL, 23420), -- 28802
(29654, 'deDE', 'Bluttrinker von Drakuru', '', NULL, NULL, 23420), -- 29654
(28793, 'deDE', 'Darmuk', '', NULL, NULL, 23420), -- 28793
(28397, 'deDE', 'Fußsoldat Hordrum', '', NULL, NULL, 23420), -- 28397
(28398, 'deDE', 'Kreuzfahrer Brune', '', NULL, NULL, 23420), -- 28398
(28396, 'deDE', 'Gefangener Champion', '', NULL, NULL, 23420), -- 28396
(28778, 'deDE', 'Scourgewagon Bunny', '', NULL, NULL, 23420), -- 28778
(28205, 'deDE', 'Alchemist Finkelstein', '', NULL, NULL, 23420), -- 28205
(28045, 'deDE', 'Hauptmann Arnath', '', NULL, NULL, 23420), -- 28045
(28874, 'deDE', 'Gargoyle', '', NULL, NULL, 23420), -- 28874
(28844, 'deDE', 'Schädelberster der Drakkari', '', NULL, NULL, 23420), -- 28844
(28240, 'deDE', 'Finklestein''s Cauldron Bunny', '', NULL, NULL, 23420), -- 28240
(28181, 'deDE', 'Torauslöser von Zul''Drak', '', NULL, NULL, 23420), -- 28181
(28023, 'deDE', 'Verrottende Monstrosität', '', NULL, NULL, 23420), -- 28023
(72541, 'deDE', 'Speerwache Du''mont', '', NULL, NULL, 23420), -- 72541
(70757, 'deDE', 'Blutlache', '', NULL, NULL, 23420), -- 70757
(72538, 'deDE', 'Folterer Sipe', '', NULL, NULL, 23420), -- 72538
(55606, 'deDE', 'Blutlache', '', NULL, NULL, 23420), -- 55606
(28303, 'deDE', 'Wasserbinder der Drakkari', '', NULL, NULL, 23420), -- 28303
(28026, 'deDE', 'Tobender Spuk', '', NULL, NULL, 23420), -- 28026
(29211, 'deDE', 'Eingeborener Drakkari', '', NULL, NULL, 23420), -- 29211
(28029, 'deDE', 'Argentumkreuzfahrer', '', NULL, NULL, 23420), -- 28029
(28798, 'deDE', 'Claudia Blutrabe', '', 'Lederverarbeitungs- & Kürschnereibedarf', NULL, 23420), -- 28798
(28794, 'deDE', 'Kevin Weber', '', 'Handwerkswaren', NULL, 23420), -- 28794
(28309, 'deDE', 'Unterleutnant Jax', '', NULL, NULL, 23420), -- 28309
(28028, 'deDE', 'Argentumschildträger', '', NULL, NULL, 23420), -- 28028
(28022, 'deDE', 'Madenfresser', '', NULL, NULL, 23420), -- 28022
(28059, 'deDE', 'Kommandant Falstaav', '', NULL, NULL, 23420), -- 28059
(28039, 'deDE', 'Kommandant Kunz', '', NULL, NULL, 23420), -- 28039
(28790, 'deDE', 'Fala Weichhuf', '', 'Stallmeisterin', NULL, 23420), -- 28790
(29133, 'deDE', 'Gestörte Seele', '', NULL, NULL, 23420), -- 29133
(28099, 'deDE', 'Korporal Mala', '', NULL, NULL, 23420), -- 28099
(28056, 'deDE', 'Unteroffizier Stackhammer', '', NULL, NULL, 23420), -- 28056
(16509, 'deDE', 'Argentumschlachtross', '', NULL, NULL, 23420), -- 16509
(28089, 'deDE', 'Sseratus', '', NULL, NULL, 23420), -- 28089
(28062, 'deDE', 'Verhexer Ubungo', '', NULL, NULL, 23420), -- 28062
(28246, 'deDE', 'Himmelsschrecken', '', NULL, NULL, 23420), -- 28246
(29169, 'deDE', 'Magister Teronus III', '', NULL, NULL, 23420), -- 29169
(28791, 'deDE', 'Marissa Immerwacht', '', 'Gastwirtin', NULL, 23420), -- 28791
(28792, 'deDE', 'Noggra', '', 'Gemischtwaren', NULL, 23420), -- 28792
(28244, 'deDE', 'Etrigg', '', NULL, NULL, 23420), -- 28244
(28175, 'deDE', 'Kreuzzugskommandant Korfax', '', NULL, NULL, 23420), -- 28175
(28141, 'deDE', 'Kreuzfahrer Lamoof', '', NULL, NULL, 23420), -- 28141
(28133, 'deDE', 'Kreuzfahrer Jonathan', '', NULL, NULL, 23420), -- 28133
(30156, 'deDE', '[DND] Anguish Spectator Bunny', '', NULL, NULL, 23420), -- 30156
(30140, 'deDE', 'Zena', '', 'Wodins Kriegerkätzchen', NULL, 23420), -- 30140
(30009, 'deDE', 'Wodin der Trolldiener', '', NULL, NULL, 23420), -- 30009
(30007, 'deDE', 'Gurgthock', '', 'Kampfveranstalter', NULL, 23420), -- 30007
(28143, 'deDE', 'Kreuzfahrerin Josephine', '', NULL, NULL, 23420), -- 28143
(28304, 'deDE', 'Drakkaripodest 02', '', NULL, NULL, 23420), -- 28304
(28064, 'deDE', 'Drakkaripodest 01', '', NULL, NULL, 23420), -- 28064
(29129, 'deDE', 'Verlorener Drakkarigeist', '', NULL, NULL, 23420), -- 29129
(16570, 'deDE', 'Wahnsinniger Wassergeist', '', NULL, NULL, 23420), -- 16570
(28042, 'deDE', 'Hauptmann Brandon', '', NULL, NULL, 23420), -- 28042
(28330, 'deDE', 'Ancient Dirt KC Bunny', '', NULL, NULL, 23420), -- 28330
(62820, 'deDE', 'Wasserwellchen', '', NULL, 'wildpetcapturable', 23420), -- 62820
(28799, 'deDE', 'Alanna', '', 'Juwelierskunstbedarf', NULL, 23420), -- 28799
(28797, 'deDE', 'Haley Kupferwender', '', 'Ingenieursbedarf', NULL, 23420), -- 28797
(28796, 'deDE', 'Arlen Blendhammer', '', 'Schmied', NULL, 23420), -- 28796
(28177, 'deDE', 'Rayne', '', 'Der Zirkel des Cenarius', NULL, 23420), -- 28177
(28176, 'deDE', 'Rimblat Erdspalter', '', 'Der Irdene Ring', NULL, 23420), -- 28176
(28178, 'deDE', 'Metz der Rächer', '', NULL, NULL, 23420), -- 28178
(29416, 'deDE', 'Attrappe des Argentumstützpunkts', '', NULL, NULL, 23420), -- 29416
(28800, 'deDE', 'Aidan Stahlauge', '', NULL, NULL, 23420), -- 28800
(28204, 'deDE', 'Lehrling Stößelpot', '', NULL, NULL, 23420), -- 28204
(28801, 'deDE', 'Verteidiger des Argentumstützpunkts', '', NULL, NULL, 23420), -- 28801
(28623, 'deDE', 'Gurric', '', 'Flugmeisterin', NULL, 23420), -- 28623
(28068, 'deDE', 'Prophet von Sseratus', '', NULL, NULL, 23420), -- 28068
(28034, 'deDE', 'Schlangenführer der Drakkari', '', NULL, NULL, 23420), -- 28034
(28036, 'deDE', 'Champion von Sseratus', '', NULL, NULL, 23420), -- 28036
(28041, 'deDE', 'Argentumsoldat', '', NULL, NULL, 23420), -- 28041
(28035, 'deDE', 'Priester von Sseratus', '', NULL, NULL, 23420), -- 28035
(28221, 'deDE', 'Falltürkrabbler', '', NULL, NULL, 23420), -- 28221
(6653, 'deDE', 'Titanische Kröte', '', NULL, NULL, 23420), -- 6653
(61368, 'deDE', 'Titanische Kröte', '', NULL, 'wildpetcapturable', 23420), -- 61368
(30098, 'deDE', 'Verkäufer im Amphitheater', '', 'Snacks', NULL, 23420), -- 30098
(30193, 'deDE', 'Zuschauer im Amphitheater', '', NULL, NULL, 23420), -- 30193
(30102, 'deDE', 'Zuschauer im Amphitheater', '', NULL, NULL, 23420), -- 30102
(28162, 'deDE', 'Drakkarileiche', '', NULL, 'LootAll', 23420), -- 28162
(32447, 'deDE', 'Schildwache von Zul''Drak', '', NULL, NULL, 23420), -- 32447
(28043, 'deDE', 'Hauptmann Grondel', '', NULL, NULL, 23420), -- 28043
(28324, 'deDE', 'Kreuzfahrer Whathah', '', NULL, NULL, 23420), -- 28324
(28323, 'deDE', 'Moosbedeckter Amokläufer', '', NULL, NULL, 23420), -- 28323
(28145, 'deDE', 'Lauernder Basilisk', '', NULL, NULL, 23420), -- 28145
(28090, 'deDE', 'Rekrut des Kreuzzugs', '', NULL, NULL, 23420), -- 28090
(28305, 'deDE', 'Drakkaripodest 03', '', NULL, NULL, 23420), -- 28305
(28413, 'deDE', 'Nerubischer Kokon', '', NULL, NULL, 23420), -- 28413
(28412, 'deDE', 'Brutmeister der Hath''ar', '', NULL, NULL, 23420), -- 28412
(28158, 'deDE', 'Siechender Argentumfußsoldat', '', NULL, NULL, 23420), -- 28158
(28156, 'deDE', 'Besiegter Argentumfußsoldat', '', NULL, NULL, 23420), -- 28156
(28284, 'deDE', 'Spezialistin Zahnrad', '', NULL, NULL, 23420), -- 28284
(28283, 'deDE', 'Unteroffizier Mondsplitter', '', NULL, NULL, 23420), -- 28283
(28125, 'deDE', 'Dr. Rogers', '', NULL, NULL, 23420), -- 28125
(28117, 'deDE', 'Argentumfußsoldat', '', NULL, NULL, 23420), -- 28117
(28044, 'deDE', 'Hauptmann Rupert', '', NULL, NULL, 23420), -- 28044
(29026, 'deDE', 'Schleim von Kolramas', '', NULL, NULL, 23420), -- 29026
(28137, 'deDE', 'Leave No One Behind Bunny', '', NULL, NULL, 23420), -- 28137
(28260, 'deDE', 'Besiegter Argentumfußsoldat', '', NULL, NULL, 23420), -- 28260
(28258, 'deDE', 'Segler der Hath''ar', '', NULL, NULL, 23420), -- 28258
(28274, 'deDE', 'Seuchensprüher', '', NULL, NULL, 23420), -- 28274
(28255, 'deDE', 'Malas der Verderber', '', NULL, NULL, 23420), -- 28255
(28257, 'deDE', 'Nekromagus der Hath''ar', '', NULL, NULL, 23420), -- 28257
(28352, 'deDE', 'Nethurbian Crater KC Bunny', '', NULL, NULL, 23420), -- 28352
(28848, 'deDE', 'Prophet von Har''koa', '', NULL, NULL, 23420), -- 28848
(28401, 'deDE', 'Har''koa', '', NULL, NULL, 23420), -- 28401
(28403, 'deDE', 'Har''koanunterdrücker', '', NULL, NULL, 23420), -- 28403
(28402, 'deDE', 'Klaue von Har''koa', '', NULL, NULL, 23420), -- 28402
(28829, 'deDE', 'Saree', '', 'Alchemiebedarf', NULL, 23420), -- 28829
(33007, 'deDE', 'Frostleopardenmännchen', '', NULL, NULL, 23420), -- 33007
(28527, 'deDE', 'Chronist To''kini', '', NULL, NULL, 23420), -- 28527
(28496, 'deDE', 'Chulo der Verrückte', '', NULL, NULL, 23420), -- 28496
(33008, 'deDE', 'Eispfotenbärenmännchen', '', NULL, NULL, 23420), -- 33008
(29583, 'deDE', 'Pan''ya', '', 'Gastwirt', NULL, 23420), -- 29583
(28831, 'deDE', 'Yamuna', '', 'Handwerkswaren', NULL, 23420), -- 28831
(28827, 'deDE', 'Co''man', '', 'Reagenzien', NULL, 23420), -- 28827
(28479, 'deDE', 'Hexendoktor Khufu', '', NULL, NULL, 23420), -- 28479
(30039, 'deDE', 'Asgari', '', 'Stallmeisterin', NULL, 23420), -- 30039
(28828, 'deDE', 'Ansari', '', 'Kräuterkundebedarf', NULL, 23420), -- 28828
(28504, 'deDE', 'Medizinmann von Jin''alai', '', NULL, NULL, 23420), -- 28504
(28388, 'deDE', 'Krieger von Jin''Alai', '', NULL, NULL, 23420), -- 28388
(28575, 'deDE', 'Rhunoks Folterer', '', NULL, NULL, 23420), -- 28575
(28478, 'deDE', 'Tor des Altars von Quetz''lun', '', NULL, NULL, 23420), -- 28478
(28863, 'deDE', 'Wächter von Zim''Torga', '', NULL, NULL, 23420), -- 28863
(28387, 'deDE', 'Verteidiger von Zim''Torga', '', NULL, NULL, 23420), -- 28387
(28442, 'deDE', 'Prophet von Rhunok', '', NULL, NULL, 23420), -- 28442
(28411, 'deDE', 'Gefrorene Erde', '', NULL, NULL, 23420), -- 28411
(28452, 'deDE', 'Elementarriss', '', NULL, NULL, 23420), -- 28452
(28561, 'deDE', 'Geist von Rhunok', '', NULL, NULL, 23420), -- 28561
(28416, 'deDE', 'Rhunok', '', NULL, NULL, 23420), -- 28416
(28417, 'deDE', 'Priester von Rhunok', '', NULL, NULL, 23420), -- 28417
(28600, 'deDE', 'Kopfgeldjäger der Heb''Drakkar', '', NULL, NULL, 23420), -- 28600
(28465, 'deDE', 'Hetzer der Heb''Drakkar', '', NULL, NULL, 23420), -- 28465
(28597, 'deDE', 'Wächter von Zim''Rhuk', '', NULL, NULL, 23420), -- 28597
(28418, 'deDE', 'Bärentrapper der Drakkari', '', NULL, NULL, 23420), -- 28418
(28916, 'deDE', 'Tiri', '', 'Kriegslord Zol''Maz'' Weib', NULL, 23420), -- 28916
(28902, 'deDE', 'Kriegslord Zol''Maz', '', NULL, NULL, 23420), -- 28902
(28917, 'deDE', 'Yara', '', 'Kriegslord Zol''Maz'' Tochter', NULL, 23420), -- 28917
(28918, 'deDE', 'Drek''Maz', '', 'Kriegslord Zol''Maz'' Sohn', NULL, 23420), -- 28918
(28882, 'deDE', 'Verzauberter Tikikrieger', '', NULL, NULL, 23420), -- 28882
(33025, 'deDE', 'Ha''wana', '', NULL, NULL, 23420), -- 33025
(30569, 'deDE', 'Rafae', '', 'Flugmeister', NULL, 23420), -- 30569
(28988, 'deDE', 'Unterdrücker von Akali', '', NULL, NULL, 23420), -- 28988
(28952, 'deDE', 'Akali', '', NULL, NULL, 23420), -- 28952
(26298, 'deDE', 'ELM General Purpose Bunny (scale x0.01) Large', '', NULL, NULL, 23420), -- 26298
(28717, 'deDE', 'Oberanführer Drakuru', '', NULL, NULL, 23420), -- 28717
(29332, 'deDE', 'Fledermausreiter von Gundrak', '', NULL, NULL, 23420), -- 29332
(29235, 'deDE', 'Wilder der Gundrak', '', NULL, NULL, 23420), -- 29235
(28851, 'deDE', 'Tobendes Mammut', '', NULL, 'vehichleCursor', 23420), -- 28851
(28852, 'deDE', 'Toter Jünger von Mam''toth', '', NULL, NULL, 23420), -- 28852
(28202, 'deDE', 'Ratte von Zul''Drak', '', NULL, 'LootAll', 23420), -- 28202
(29237, 'deDE', 'Feuerfresser der Gundrak', '', NULL, NULL, 23420), -- 29237
(29236, 'deDE', 'Schläger der Gundrak', '', NULL, NULL, 23420), -- 29236
(28861, 'deDE', 'Jünger von Mam''toth', '', NULL, NULL, 23420), -- 28861
(29334, 'deDE', 'Raptor von Gundrak', '', NULL, NULL, 23420), -- 29334
(28233, 'deDE', 'Fledermaus von Zul''Drak', '', NULL, NULL, 23420), -- 28233
(28779, 'deDE', 'Blut von Mam''toth', '', NULL, NULL, 23420), -- 28779
(28672, 'deDE', 'Quetz''lun', '', NULL, NULL, 23420), -- 28672
(28671, 'deDE', 'Prophet von Quetz''lun', '', NULL, NULL, 23420), -- 28671
(28784, 'deDE', 'Altarbehüter', '', NULL, NULL, 23420), -- 28784
(28477, 'deDE', 'Saat von Quetz''lun', '', NULL, NULL, 23420), -- 28477
(62693, 'deDE', 'Arktishase', '', NULL, 'wildpetcapturable', 23420), -- 62693
(29319, 'deDE', 'Eispfotenbär', '', NULL, NULL, 23420), -- 29319
(28404, 'deDE', 'Verfluchter Abkömmling von Har''koa', '', NULL, NULL, 23420), -- 28404
(29327, 'deDE', 'Frostleopard', '', NULL, NULL, 23420), -- 29327
(27321, 'deDE', 'Kodian Conversation Credit', '', NULL, NULL, 23420), -- 27321
(27275, 'deDE', 'Kodian', '', 'Tochter des Ursoc', NULL, 23420), -- 27275
(26681, 'deDE', 'Grumbald Einauge', '', NULL, NULL, 23420), -- 26681
(27212, 'deDE', 'Abbild von Loken', '', NULL, NULL, 23420), -- 27212
(27075, 'deDE', 'Kraftkern von Dun Argol', '', NULL, NULL, 23420), -- 27075
(27115, 'deDE', 'Ausgrabungsleiterin Varana', '', 'Forscherliga', NULL, 23420), -- 27115
(26406, 'deDE', 'Der Amboss', '', 'Zornhammers Wächter', NULL, 23420), -- 26406
(26410, 'deDE', 'Runenschmied Kathorn', '', NULL, NULL, 23420), -- 26410
(27114, 'deDE', 'Ausgrabungsleiter Torgan', '', 'Forscherliga', NULL, 23420), -- 27114
(26409, 'deDE', 'Runenschmied Durar', '', NULL, NULL, 23420), -- 26409
(27177, 'deDE', 'Eisenrunenaufseher', '', NULL, NULL, 23420), -- 27177
(27113, 'deDE', 'Ausgrabungsleiter Gann', '', 'Forscherliga', NULL, 23420), -- 27113
(26347, 'deDE', 'Runenkriegsgolem', '', NULL, NULL, 23420), -- 26347
(26414, 'deDE', 'Runenblitzkanonier', '', NULL, NULL, 23420), -- 26414
(26885, 'deDE', 'Gebirgsjäger Kilian', '', NULL, NULL, 23420), -- 26885
(26883, 'deDE', 'Raegar Brechbraue', '', 'Forscherliga', NULL, 23420), -- 26883
(26407, 'deDE', 'Blitzwache', '', NULL, NULL, 23420), -- 26407
(26408, 'deDE', 'Eisenrunenschmied', '', NULL, NULL, 23420), -- 26408
(27523, 'deDE', 'Gräuelschnauze', '', NULL, NULL, 23420), -- 27523
(27018, 'deDE', 'Schatten von Arugal', '', NULL, NULL, 23420), -- 27018
(27579, 'deDE', 'Varlam', '', 'Arugals Handlanger', NULL, 23420), -- 27579
(27578, 'deDE', 'Blutschlund', '', NULL, NULL, 23420), -- 27578
(27580, 'deDE', 'Selas', '', NULL, NULL, 23420), -- 27580
(27020, 'deDE', 'Worgen von Blutmond', '', NULL, NULL, 23420), -- 27020
(27024, 'deDE', 'Kultist von Blutmond', '', NULL, NULL, 23420), -- 27024
(26811, 'deDE', 'Uralter Kriegstreiber der Drakkari', '', NULL, NULL, 23420), -- 26811
(26812, 'deDE', 'Uralter Sterndeuter der Drakkari', '', NULL, NULL, 23420), -- 26812
(26937, 'deDE', 'Gong Bunny', '', NULL, NULL, 23420), -- 26937
(26494, 'deDE', 'Griselda', '', 'Hughs Haustier', NULL, 23420), -- 26494
(26484, 'deDE', 'Hugh Glass', '', 'Händler', NULL, 23420), -- 26484
(26856, 'deDE', 'Nordstein', '', NULL, NULL, 23420), -- 26856
(26857, 'deDE', 'Südstein', '', NULL, NULL, 23420), -- 26857
(30367, 'deDE', 'Urahne Lunaro', '', NULL, NULL, 23420), -- 30367
(26420, 'deDE', 'Gavrock', '', NULL, NULL, 23420), -- 26420
(26855, 'deDE', 'Oststein', '', NULL, NULL, 23420), -- 26855
(26418, 'deDE', 'Langhufgrasfresser', '', NULL, NULL, 23420), -- 26418
(26820, 'deDE', 'Eisenrunenweber', '', NULL, NULL, 23420), -- 26820
(29277, 'deDE', 'Datalore Klitzekugel', '', 'Handwerkswaren', NULL, 23420), -- 29277
(26886, 'deDE', 'Kraz', '', NULL, NULL, 23420), -- 26886
(26884, 'deDE', 'Harkor', '', NULL, NULL, 23420), -- 26884
(26919, 'deDE', 'Drak''aguul', '', NULL, NULL, 23420), -- 26919
(26789, 'deDE', 'Drakuru''s Bunny 04', '', NULL, NULL, 23420), -- 26789
(26795, 'deDE', 'Orakel der Drakkari', '', NULL, NULL, 23420), -- 26795
(26797, 'deDE', 'Beschützer der Drakkari', '', NULL, NULL, 23420), -- 26797
(26814, 'deDE', 'Harrison Jones', '', NULL, NULL, 23420), -- 26814
(26469, 'deDE', 'Südgebäude', '', NULL, NULL, 23420), -- 26469
(26470, 'deDE', 'Ostgebäude', '', NULL, NULL, 23420), -- 26470
(26334, 'deDE', 'Konstruktionsmeister Damrath', '', NULL, NULL, 23420), -- 26334
(26335, 'deDE', 'Gefallener Irdener Krieger', '', NULL, NULL, 23420), -- 26335
(26468, 'deDE', 'Nordgebäude', '', NULL, NULL, 23420), -- 26468
(26284, 'deDE', 'Runenschlachtgolem', '', NULL, NULL, 23420), -- 26284
(26282, 'deDE', 'Irdener Krieger', '', NULL, NULL, 23420), -- 26282
(26514, 'deDE', 'Feldmesser Orlond', '', 'Forscherliga', NULL, 23420), -- 26514
(26522, 'deDE', 'Unterirdischer Drescher', '', NULL, NULL, 23420), -- 26522
(26348, 'deDE', 'Eisenthan Argrum', '', NULL, NULL, 23420), -- 26348
(26268, 'deDE', 'Runenhäscher', '', NULL, NULL, 23420), -- 26268
(26270, 'deDE', 'Eisenrunenformer', '', NULL, NULL, 23420), -- 26270
(26261, 'deDE', 'Grizzlyhügelriese', '', NULL, NULL, 23420), -- 26261
(26260, 'deDE', 'Kurun', '', NULL, NULL, 23420), -- 26260
(51893, 'deDE', 'Fußsoldat der Westfallbrigade', '', NULL, NULL, 23420), -- 51893
(26417, 'deDE', 'Runenbeschriebener Riese', '', NULL, NULL, 23420), -- 26417
(27646, 'deDE', 'Anya', '', NULL, NULL, 23420), -- 27646
(24921, 'deDE', 'Cosmetic Trigger - LAB', '', NULL, NULL, 23420), -- 24921
(26416, 'deDE', 'Initiand des Wolfskults', '', NULL, NULL, 23420), -- 26416
(27626, 'deDE', 'Tatjanas Pferd', '', NULL, 'vehichleCursor', 23420), -- 27626
(27627, 'deDE', 'Tatjana', '', 'Initiandin des Wolfskults', NULL, 23420), -- 27627
(104410, 'deDE', 'Schrein von Ursol', '', NULL, 'questinteract', 23420), -- 104410
(26971, 'deDE', 'Anatol', '', NULL, NULL, 23420), -- 26971
(26935, 'deDE', 'Sasha', '', NULL, NULL, 23420), -- 26935
(26389, 'deDE', 'Jäger von Julheim', '', NULL, NULL, 23420), -- 26389
(33224, 'deDE', 'Teichfrosch', '', NULL, NULL, 23420), -- 33224
(33211, 'deDE', 'Teichfrosch', '', NULL, NULL, 23420), -- 33211
(26434, 'deDE', 'Trapper der Frostpfoten', '', NULL, NULL, 23420), -- 26434
(34381, 'deDE', '[DND]Northrend Children''s Week Trigger', '', NULL, NULL, 23420), -- 34381
(26932, 'deDE', 'Petrov', '', NULL, NULL, 23420), -- 26932
(26891, 'deDE', 'Untoter Bergarbeiter', '', NULL, NULL, 23420), -- 26891
(27582, 'deDE', 'Soldat Arun', '', NULL, NULL, 23420), -- 27582
(26876, 'deDE', 'Samuel Klarbuch', '', 'Greifenmeister', NULL, 23420), -- 26876
(26377, 'deDE', 'Knappe Percy', '', 'Stallmeister', NULL, 23420), -- 26377
(26375, 'deDE', 'Rüstmeister McCarty', '', 'Gastwirt', NULL, 23420), -- 26375
(26371, 'deDE', 'Knappe Walter', '', 'Gryans Knappe', NULL, 23420), -- 26371
(26235, 'deDE', 'Gefreiter Brauenwirbel', '', NULL, NULL, 23420), -- 26235
(26234, 'deDE', 'Unteroffizier Daelian', '', NULL, NULL, 23420), -- 26234
(26233, 'deDE', 'Unteroffizier Tyric', '', NULL, NULL, 23420), -- 26233
(26212, 'deDE', 'Hauptmann Gryan Starkmantel', '', 'Kommandant der Westfallbrigade', NULL, 23420), -- 26212
(582, 'deDE', 'Graumähne', '', NULL, NULL, 23420), -- 582
(27944, 'deDE', 'Johan', '', NULL, NULL, 23420), -- 27944
(26361, 'deDE', 'Torthen Grabtief', '', 'Forscherliga', NULL, 23420), -- 26361
(26226, 'deDE', 'Brugar Steinscher', '', 'Forscherliga', NULL, 23420), -- 26226
(30357, 'deDE', 'Urahne Beldak', '', NULL, NULL, 23420), -- 30357
(26236, 'deDE', 'Gefreiter Jansen', '', NULL, NULL, 23420), -- 26236
(26362, 'deDE', 'Feldmesser der Forscherliga', '', 'Forscherliga', NULL, 23420), -- 26362
(26205, 'deDE', 'Peer Woll', '', NULL, NULL, 23420), -- 26205
(26392, 'deDE', 'Gefreiter Molsen', '', NULL, NULL, 23420), -- 26392
(26229, 'deDE', 'Tiernan Ambossherz', '', 'Waffenhändler', NULL, 23420), -- 26229
(26374, 'deDE', 'Maevin Weitmond', '', 'Handwerkswaren', NULL, 23420), -- 26374
(26382, 'deDE', 'Balfour Schwarzklinge', '', 'Reagenzien & Gifte', NULL, 23420), -- 26382
(26388, 'deDE', 'Veira Langdon', '', 'Gemischtwaren', NULL, 23420), -- 26388
(29285, 'deDE', 'Späher der Westfallbrigade', '', NULL, NULL, 23420), -- 29285
(26387, 'deDE', 'Vater Michaels', '', NULL, NULL, 23420), -- 26387
(26217, 'deDE', 'Fußsoldat der Westfallbrigade', '', NULL, NULL, 23420), -- 26217
(26264, 'deDE', 'Felsziel', '', NULL, NULL, 23420), -- 26264
(27461, 'deDE', 'Bambina', '', NULL, NULL, 23420), -- 27461
(27460, 'deDE', 'Mutter von Bambina', '', NULL, NULL, 23420), -- 27460
(27459, 'deDE', 'Trommler', '', NULL, NULL, 23420), -- 27459
(27458, 'deDE', 'Flora', '', NULL, NULL, 23420), -- 27458
(26472, 'deDE', 'Hochlandmustang', '', NULL, NULL, 23420), -- 26472
(27581, 'deDE', 'Ruuna die Blinde', '', NULL, NULL, 23420), -- 27581
(27263, 'deDE', 'Vordrassils Herz', '', NULL, NULL, 23420), -- 27263
(26646, 'deDE', 'Saronitschrecken', '', NULL, NULL, 23420), -- 26646
(27322, 'deDE', 'Orsonn Conversation Credit', '', NULL, NULL, 23420), -- 27322
(27274, 'deDE', 'Orsonn', '', 'Sohn des Ursoc', NULL, 23420), -- 27274
(32377, 'deDE', 'Perobas der Blutdürster', '', NULL, NULL, 23420), -- 32377
(26356, 'deDE', 'Jäger der Rotfänge', '', NULL, NULL, 23420), -- 26356
(29492, 'deDE', 'Eschenholzfallensteller', '', NULL, NULL, 23420), -- 29492
(27230, 'deDE', 'Silberfellhirsch', '', NULL, NULL, 23420), -- 27230
(24261, 'deDE', 'Ulfang', '', NULL, NULL, 23420), -- 24261
(27547, 'deDE', 'Vladek', '', 'Initiand des Wolfskults', NULL, 23420), -- 27547
(29275, 'deDE', 'Versorger des Espenhains', '', 'Speis & Trank', NULL, 23420), -- 29275
(29270, 'deDE', 'Händler des Espenhains', '', 'Handwerkswaren', NULL, 23420), -- 29270
(27469, 'deDE', 'Ivan', '', NULL, NULL, 23420), -- 27469
(27408, 'deDE', 'Streunender Grauheuler', '', NULL, NULL, 23420), -- 27408
(27047, 'deDE', 'Invisible Stalker (Floating Only)', '', NULL, NULL, 23420), -- 27047
(27546, 'deDE', 'Jäger von Silberwasser', '', NULL, NULL, 23420), -- 27546
(27277, 'deDE', 'Forstmeister Anderhol', '', NULL, NULL, 23420), -- 27277
(27088, 'deDE', 'Yolanda Haymer', '', 'Reagenzienbedarf', NULL, 23420), -- 27088
(27071, 'deDE', 'Benjamin Jacobs', '', 'Gemischtwaren', NULL, 23420), -- 27071
(27066, 'deDE', 'Jennifer Glock', '', 'Gastwirtin', NULL, 23420), -- 27066
(27391, 'deDE', 'Förster Drach', '', NULL, NULL, 23420), -- 27391
(27089, 'deDE', 'Safran Reynolds', '', 'Gifte', NULL, 23420), -- 27089
(27068, 'deDE', 'Matthias Ackerman', '', 'Stallmeister', NULL, 23420), -- 27068
(29244, 'deDE', 'Jesse Meister', '', 'Metzger', NULL, 23420), -- 29244
(29161, 'deDE', 'Magistrix Haelenai', '', NULL, NULL, 23420), -- 29161
(27070, 'deDE', 'Lisa Philbrook', '', 'Handwerkswaren', NULL, 23420), -- 27070
(27062, 'deDE', 'Brom Starkarm', '', 'Schmiedekunstbedarf', NULL, 23420), -- 27062
(26875, 'deDE', 'Leutnant Dumont', '', NULL, NULL, 23420), -- 26875
(27295, 'deDE', 'Hierophantin Thayreen', '', NULL, NULL, 23420), -- 27295
(26880, 'deDE', 'Vana Grau', '', 'Greifenmeisterin', NULL, 23420), -- 26880
(27293, 'deDE', 'Förster von Ammertann', '', NULL, NULL, 23420), -- 27293
(27072, 'deDE', 'Fußsoldat von Ammertann', '', NULL, NULL, 23420), -- 27072
(38453, 'deDE', 'Arcturis', '', NULL, NULL, 23420), -- 38453
(29269, 'deDE', 'Trapper des Espenhains', '', NULL, NULL, 23420), -- 29269
(27468, 'deDE', 'Unteroffizier Hartsmann', '', NULL, NULL, 23420), -- 27468
(27371, 'deDE', 'Synipus', '', NULL, NULL, 23420), -- 27371
(26357, 'deDE', 'Krieger der Frostpfoten', '', NULL, NULL, 23420), -- 26357
(26589, 'deDE', 'Mr. Floppy', '', 'Emilys Haustier', NULL, 23420), -- 26589
(26588, 'deDE', 'Emily', '', NULL, NULL, 23420), -- 26588
(27421, 'deDE', 'Farnfressermotte', '', NULL, NULL, 23420), -- 27421
(27555, 'deDE', 'Hexendoktor der Drakkari', '', NULL, NULL, 23420), -- 27555
(27554, 'deDE', 'Verletzter Flüchtling der Drakkari', '', NULL, NULL, 23420), -- 27554
(26704, 'deDE', 'Verteidiger der Drakkari', '', NULL, NULL, 23420), -- 26704
(26700, 'deDE', 'Drakuru''s Bunny 03', '', NULL, NULL, 23420), -- 26700
(29693, 'deDE', 'Schlangenverteidiger', '', NULL, NULL, 23420), -- 29693
(26706, 'deDE', 'Infizierter Grizzly', '', NULL, NULL, 23420), -- 26706
(27416, 'deDE', 'Pipthwack', '', NULL, NULL, 23420), -- 27416
(27484, 'deDE', 'Rheanna', '', NULL, NULL, 23420), -- 27484
(26682, 'deDE', 'Leiche eines Mitglieds der Venture Co.', '', NULL, NULL, 23420), -- 26682
(26583, 'deDE', 'Entsetzter Schamane der Drakkari', '', NULL, NULL, 23420), -- 26583
(26458, 'deDE', 'Seuchenfürst der Drakkari', '', NULL, NULL, 23420), -- 26458
(26582, 'deDE', 'Entsetzter Krieger der Drakkari', '', NULL, NULL, 23420), -- 26582
(26570, 'deDE', 'Ausgemergelter Geißeltroll', '', NULL, NULL, 23420), -- 26570
(385, 'deDE', 'Pferd', '', NULL, NULL, 23420), -- 385
(27499, 'deDE', 'Eingesperrter Gefangener', '', NULL, NULL, 23420), -- 27499
(27545, 'deDE', 'Katja', '', NULL, NULL, 23420), -- 27545
(26544, 'deDE', 'Kriegsherr Zim''Bo', '', NULL, NULL, 23420), -- 26544
(27615, 'deDE', 'Todessprecher der Geißel', '', NULL, NULL, 23420), -- 27615
(26559, 'deDE', 'Drakuru''s Bunny 02', '', NULL, NULL, 23420), -- 26559
(27941, 'deDE', 'Seuchenverbreiter der Drakkari', '', NULL, NULL, 23420), -- 27941
(26461, 'deDE', 'Kadaverreißer der Geißel', '', NULL, NULL, 23420), -- 26461
(26457, 'deDE', 'Kranker Drakkari', '', NULL, NULL, 23420), -- 26457
(27424, 'deDE', 'Marodeur von Burg Siegeswall', '', NULL, NULL, 23420), -- 27424
(27676, 'deDE', 'Verteidiger von Silberwasser', '', NULL, NULL, 23420), -- 27676
(26821, 'deDE', 'Gesandter Ducal', '', NULL, NULL, 23420), -- 26821
(27486, 'deDE', 'Sergei', '', NULL, NULL, 23420), -- 27486
(26708, 'deDE', 'Bewohner von Silberwasser', '', NULL, NULL, 23420), -- 26708
(26679, 'deDE', 'Fährtenleser von Silberwasser', '', NULL, NULL, 23420), -- 26679
(27464, 'deDE', 'Aumana', '', NULL, NULL, 23420), -- 27464
(27423, 'deDE', 'Grekk', '', NULL, NULL, 23420), -- 27423
(27422, 'deDE', 'Lurz', '', NULL, NULL, 23420), -- 27422
(27451, 'deDE', 'Kommandant Bargok', '', NULL, NULL, 23420), -- 27451
(27456, 'deDE', 'Scharmützler von Burg Siegeswall', '', NULL, NULL, 23420), -- 27456
(27425, 'deDE', 'Darrok', '', NULL, NULL, 23420), -- 27425
(27414, 'deDE', 'Gordun', '', NULL, NULL, 23420), -- 27414
(27413, 'deDE', 'Log Ride Bunny - Alliance', '', NULL, NULL, 23420), -- 27413
(27475, 'deDE', 'Infanterist der Westfallbrigade', '', NULL, NULL, 23420), -- 27475
(27354, 'deDE', 'Kaputter Schredder', '', NULL, 'vehichleCursor', 23420), -- 27354
(27463, 'deDE', 'Verwundeter Scharmützler', '', NULL, NULL, 23420), -- 27463
(27481, 'deDE', 'Leiche eines Infanteristen von Westfall', '', NULL, NULL, 23420), -- 27481
(27457, 'deDE', 'Leiche eines Scharmützlers', '', NULL, NULL, 23420), -- 27457
(27482, 'deDE', 'Verwundeter Infanterist von Westfall', '', NULL, NULL, 23420), -- 27482
(27264, 'deDE', 'Vordrassils Ast', '', NULL, NULL, 23420), -- 27264
(26369, 'deDE', 'Kaiseradler', '', NULL, NULL, 23420), -- 26369
(91632, 'deDE', 'Remington Brode', '', 'Reisender Holzfäller', NULL, 23420), -- 91632
(26519, 'deDE', 'Prigmon', '', NULL, NULL, 23420), -- 26519
(23745, 'deDE', 'Garg', '', NULL, NULL, 23420), -- 23745
(26516, 'deDE', 'Leiche eines Kriegers der Drakkari', '', NULL, NULL, 23420), -- 26516
(26424, 'deDE', 'Samir', '', NULL, NULL, 23420), -- 26424
(26423, 'deDE', 'Drakuru', '', NULL, NULL, 23420), -- 26423
(26604, 'deDE', 'Mack Fearsen', '', NULL, NULL, 23420), -- 26604
(26474, 'deDE', 'Ameenah', '', 'Reagenzien', NULL, 23420), -- 26474
(26422, 'deDE', 'Budd', '', NULL, NULL, 23420), -- 26422
(23766, 'deDE', 'Morgom', '', NULL, NULL, 23420), -- 23766
(23764, 'deDE', 'Marge', '', NULL, NULL, 23420), -- 23764
(23762, 'deDE', 'Brend', '', NULL, NULL, 23420), -- 23762
(23565, 'deDE', 'Turgore', '', NULL, NULL, 23420), -- 23565
(23747, 'deDE', 'Überarbeiteter Gaul', '', NULL, NULL, 23420), -- 23747
(29740, 'deDE', 'Craga Eisenstich', '', 'Stallmeisterin', NULL, 23420), -- 29740
(27719, 'deDE', 'Grennix Messerfuchtel', '', 'Grubenkampfpromoter', NULL, 23420), -- 27719
(27262, 'deDE', 'Windseherin Grauhorn', '', NULL, NULL, 23420), -- 27262
(27133, 'deDE', 'Seher Yagnar', '', 'Reagenzien & Gifte', NULL, 23420), -- 27133
(27134, 'deDE', 'Schmiedin Prigka', '', 'Rüstungsschmiedin', NULL, 23420), -- 27134
(27132, 'deDE', 'Sani''i', '', 'Handwerkswaren', NULL, 23420), -- 27132
(27720, 'deDE', 'Buchmacher Vel''jen', '', NULL, NULL, 23420), -- 27720
(26852, 'deDE', 'Kragh', '', 'Windreitermeister', NULL, 23420), -- 26852
(27266, 'deDE', 'Unteroffizier Thurkin', '', NULL, NULL, 23420), -- 27266
(26868, 'deDE', 'Versorger Lorkran', '', 'Gemischtwaren', NULL, 23420), -- 26868
(27388, 'deDE', 'Unteroffizier Nazgrim', '', NULL, NULL, 23420), -- 27388
(27102, 'deDE', 'Gorgonna', '', NULL, NULL, 23420), -- 27102
(27037, 'deDE', 'Balghändler Jun''ik', '', 'Lederwaren', NULL, 23420), -- 27037
(26863, 'deDE', 'Sethyel', '', NULL, NULL, 23420), -- 26863
(26862, 'deDE', 'Anthis', '', NULL, NULL, 23420), -- 26862
(26860, 'deDE', 'Eroberin Krenna', '', NULL, NULL, 23420), -- 26860
(29160, 'deDE', 'Magistrix Phaelista', '', NULL, NULL, 23420), -- 29160
(27204, 'deDE', 'Hauptmann Knochenmaul', '', NULL, NULL, 23420), -- 27204
(27125, 'deDE', 'Barackenmeister Rhekku', '', 'Gastwirt', NULL, 23420), -- 27125
(26864, 'deDE', 'Vorhut der Burg Siegeswall', '', NULL, NULL, 23420), -- 26864
(27479, 'deDE', 'Erschlagener Fallensteller', '', NULL, NULL, 23420), -- 27479
(27470, 'deDE', 'Grunzer von Burg Siegeswall', '', NULL, NULL, 23420), -- 27470
(26869, 'deDE', 'Grubenkampfzuschauer', '', NULL, NULL, 23420), -- 26869
(26839, 'deDE', 'Legionär von Burg Siegeswall', '', NULL, NULL, 23420), -- 26839
(62818, 'deDE', 'Grizzlyeichhörnchen', '', NULL, 'wildpetcapturable', 23420), -- 62818
(27265, 'deDE', 'Vordrassils Tränen', '', NULL, NULL, 23420), -- 27265
(26366, 'deDE', 'Entropischer Schlamm', '', NULL, NULL, 23420), -- 26366
(62819, 'deDE', 'Kaiseradlerküken', '', NULL, 'wildpetcapturable', 23420), -- 62819
(26513, 'deDE', 'Leiche eines Schamanen der Drakkari', '', NULL, NULL, 23420), -- 26513
(27613, 'deDE', 'Abgeschlachteter Drakkari', '', NULL, NULL, 23420), -- 27613
(27617, 'deDE', 'Flussdrescher', '', NULL, NULL, 23420), -- 27617
(28243, 'deDE', 'Thrym', '', 'Der Hoffnungstöter', NULL, 23420), -- 28243
(26425, 'deDE', 'Krieger der Drakkari', '', NULL, NULL, 23420), -- 26425
(27452, 'deDE', 'Invisible Stalker Grizzly Hills', '', NULL, NULL, 23420), -- 27452
(26447, 'deDE', 'Schamane der Drakkari', '', NULL, NULL, 23420), -- 26447
(26498, 'deDE', 'Drakuru''s Bunny 01', '', NULL, NULL, 23420), -- 26498
(26446, 'deDE', 'Eisschlange', '', NULL, NULL, 23420), -- 26446
(27532, 'deDE', 'General Khazgar', '', NULL, NULL, 23420), -- 27532
(27565, 'deDE', 'Gurtor', '', NULL, NULL, 23420), -- 27565
(27563, 'deDE', 'Zenturio Kaggrum', '', NULL, NULL, 23420), -- 27563
(27606, 'deDE', 'Steingardist Rachtotem', '', NULL, NULL, 23420), -- 27606
(27550, 'deDE', 'Champion von Burg Siegeswall', '', NULL, NULL, 23420), -- 27550
(27628, 'deDE', 'Bubb Lazarr', '', NULL, NULL, 23420), -- 27628
(27593, 'deDE', 'Raketengetriebener Sprengkopf', '', NULL, 'vehichleCursor', 23420), -- 27593
(27702, 'deDE', 'Holzfrachter der Horde', '', NULL, NULL, 23420), -- 27702
(27511, 'deDE', 'Kapitänin Zorna', '', NULL, NULL, 23420), -- 27511
(27830, 'deDE', 'Evakuierter der Venture Co.', '', NULL, NULL, 23420), -- 27830
(27509, 'deDE', 'Kapitän Hellwasser', '', NULL, NULL, 23420), -- 27509
(27570, 'deDE', 'Nachzügler der Venture Co.', '', NULL, NULL, 23420), -- 27570
(27496, 'deDE', 'Runderneuerter Schredder', '', NULL, 'vehichleCursor', 23420), -- 27496
(27501, 'deDE', 'Marinesoldat der Westfallbrigade', '', NULL, NULL, 23420), -- 27501
(27500, 'deDE', 'Berserker von Burg Siegeswall', '', NULL, NULL, 23420), -- 27500
(27602, 'deDE', 'Unteroffizier Downey', '', 'Westfallbrigade', NULL, 23420), -- 27602
(27562, 'deDE', 'Leutnant Stuart', '', 'Westfallbrigade', NULL, 23420), -- 27562
(27520, 'deDE', 'Baron Freimann', '', 'Westfallbrigade', NULL, 23420), -- 27520
(27549, 'deDE', 'Elitesoldat der Westfallbrigade', '', NULL, NULL, 23420), -- 27549
(27495, 'deDE', 'Barbelfink', '', NULL, NULL, 23420), -- 27495
(27326, 'deDE', 'Outhouse Bunny - Grizzly', '', NULL, NULL, 23420); -- 27326
(27783, 'deDE', 'Späherhauptmann Carter', '', NULL, NULL, 23420), -- 27783
(27120, 'deDE', 'Räuberhauptmann Kronn', '', NULL, NULL, 23420), -- 27120
(26592, 'deDE', 'Graunebeljäger', '', NULL, NULL, 23420); -- 26592