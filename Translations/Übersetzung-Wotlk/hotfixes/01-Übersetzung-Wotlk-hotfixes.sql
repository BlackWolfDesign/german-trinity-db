-- TrinityCore - WowPacketParser
-- File name: multi
-- Toaster's Sniffs
-- Detected build: V7_1_5_23420
-- Detected locale: 'deDE'
-- Targeted database: Legion
-- Parsing date: 02/23/2017 14:28:29


SET NAMES 'utf8';
INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(27931, 'Dieser Posten ist sicher, $R.$b$bDiese Gegend ist seit einiger Zeit frei von Feinden.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(27606, 'Gr��e, $R.$b$bIch f�rchte, Ihr m�sst mich entschuldigen, da ich im Moment furchtbar besch�ftigt bin.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(30433, 'Seht Euch um und lasst die Zerst�rung auf Euch wirken, $C. Auch der letzte Troll, der hier gelebt hat, ist nun entweder tot oder zu den h�heren Ebenen geflohen.$b$bUnd es kommt noch schlimmer.$b$bIn einem Akt der Verzweiflung haben die �berlebenden Trolle der Drakkari ihre eigenen Tierg�tter geopfert, sodass sie vom Blut der G�tter trinken und dadurch ungeahnte Kr�fte entwickeln konnten. W�hrend sie zwar erfolgreich die Gei�el dank ihrer neuen Kr�fte aufhalten konnten, war das Resultat dennoch katastrophal. Das uralte Imperium von Zul''Drak liegt nun als Beweis daf�r in Tr�mmern.', '', 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(30482, 'Bitte seid leise! Welpen schlafen hier!', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(29866, '', 'Beeilt Euch, $R.$b$bHier drau�en geht''s um Leben und Tod!', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(28437, 'Niemand kennt die Gei�el besser als wir Todesritter.$b$bGlaubt mir, $R, es gibt nichts Effektiveres, als Feuer mit Feuer zu bek�mpfen...', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(28525, '', 'Geht weiter. Hier gibt es nichts zu sehen.', 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420);


DELETE FROM `broadcast_text_locale` WHERE (`entry`=27931 AND `locale`='deDE') OR (`entry`=27606 AND `locale`='deDE') OR (`entry`=30433 AND `locale`='deDE') OR (`entry`=30482 AND `locale`='deDE') OR (`entry`=29866 AND `locale`='deDE') OR (`entry`=28437 AND `locale`='deDE') OR (`entry`=28525 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(27931, 'deDE', 'Dieser Posten ist sicher, $R.$b$bDiese Gegend ist seit einiger Zeit frei von Feinden.', '', 23420),
(27606, 'deDE', 'Gr��e, $R.$b$bIch f�rchte, Ihr m�sst mich entschuldigen, da ich im Moment furchtbar besch�ftigt bin.', '', 23420),
(30433, 'deDE', 'Seht Euch um und lasst die Zerst�rung auf Euch wirken, $C. Auch der letzte Troll, der hier gelebt hat, ist nun entweder tot oder zu den h�heren Ebenen geflohen.$b$bUnd es kommt noch schlimmer.$b$bIn einem Akt der Verzweiflung haben die �berlebenden Trolle der Drakkari ihre eigenen Tierg�tter geopfert, sodass sie vom Blut der G�tter trinken und dadurch ungeahnte Kr�fte entwickeln konnten. W�hrend sie zwar erfolgreich die Gei�el dank ihrer neuen Kr�fte aufhalten konnten, war das Resultat dennoch katastrophal. Das uralte Imperium von Zul''Drak liegt nun als Beweis daf�r in Tr�mmern.', '', 23420),
(30482, 'deDE', 'Bitte seid leise! Welpen schlafen hier!', '', 23420),
(29866, 'deDE', '', 'Beeilt Euch, $R.$b$bHier drau�en geht''s um Leben und Tod!', 23420),
(28437, 'deDE', 'Niemand kennt die Gei�el besser als wir Todesritter.$b$bGlaubt mir, $R, es gibt nichts Effektiveres, als Feuer mit Feuer zu bek�mpfen...', '', 23420),
(28525, 'deDE', '', 'Geht weiter. Hier gibt es nichts zu sehen.', 23420);

