-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/25/2017 16:35:33


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=38729 AND `locale`='deDE') OR (`ID`=38728 AND `locale`='deDE') OR (`ID`=39663 AND `locale`='deDE') OR (`ID`=39516 AND `locale`='deDE') OR (`ID`=40051 AND `locale`='deDE') OR (`ID`=40222 AND `locale`='deDE') OR (`ID`=38819 AND `locale`='deDE') OR (`ID`=38725 AND `locale`='deDE') OR (`ID`=38727 AND `locale`='deDE') OR (`ID`=39495 AND `locale`='deDE') OR (`ID`=39262 AND `locale`='deDE') OR (`ID`=38813 AND `locale`='deDE') OR (`ID`=38766 AND `locale`='deDE') OR (`ID`=38765 AND `locale`='deDE') OR (`ID`=39050 AND `locale`='deDE') OR (`ID`=40379 AND `locale`='deDE') OR (`ID`=39049 AND `locale`='deDE') OR (`ID`=38759 AND `locale`='deDE') OR (`ID`=40378 AND `locale`='deDE') OR (`ID`=39279 AND `locale`='deDE') OR (`ID`=43806 AND `locale`='deDE') OR (`ID`=37654 AND `locale`='deDE') OR (`ID`=38407 AND `locale`='deDE') OR (`ID`=44414 AND `locale`='deDE') OR (`ID`=42985 AND `locale`='deDE') OR (`ID`=38643 AND `locale`='deDE') OR (`ID`=40122 AND `locale`='deDE') OR (`ID`=38672 AND `locale`='deDE') OR (`ID`=44701 AND `locale`='deDE') OR (`ID`=44137 AND `locale`='deDE') OR (`ID`=34398 AND `locale`='deDE') OR (`ID`=34209 AND `locale`='deDE') OR (`ID`=33543 AND `locale`='deDE') OR (`ID`=35077 AND `locale`='deDE') OR (`ID`=34655 AND `locale`='deDE') OR (`ID`=33735 AND `locale`='deDE') OR (`ID`=34965 AND `locale`='deDE') OR (`ID`=34795 AND `locale`='deDE') OR (`ID`=33815 AND `locale`='deDE') OR (`ID`=40077 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(38729, 'deDE', 'Rückkehr zum Schwarzen Tempel', 'Benutzt den Sargeritschlüsselstein, um das Teufelsportal zu aktivieren und zum Schwarzen Tempel zurückzukehren.', 'Schnell, $n, benutzt den Sargeritschlüsselstein.\n\nWir müssen zurück zum Schwarzen Tempel gelangen, bevor es zu spät ist.', '', '', '', '', '', '', 23420),
(38728, 'deDE', 'Der Schlüsselstein', 'Tötet Brutkönigin Tyranna und entreißt ihr den Sargeritschlüsselstein. Trefft Euch danach mit den anderen unten in der Portalkammer.', '$n, Brutkönigin Tyranna ist dort im Befehlsstand. Die anderen haben ihn bereits gestürmt. Sie besitzt den Sargeritschlüsselstein.\n\nEndlich ist das, wofür wir so viel geopfert haben, zum Greifen nah.', '', 'Entreißt den Sargeritschlüsselstein ihren kalten, toten Klauen.', 'Brutkönigin Tyranna', '', '', '', 23420),
(39663, 'deDE', 'Der Tod naht auf teuflischen Schwingen', 'Lasst Euch von Izal Weißmond eine Teufelsfledermaus geben und fliegt hinauf zur Teufelshammer.', 'Es wird Zeit. Wir haben den Truppen der Legion einen vernichtenden Schlag zugefügt, von dem sie sich nie erholen werden.\n\nJetzt tragen wir den Kampf zu Tyrannas Kommandozentrum. Schnell, kümmert Euch um eine Flugmöglichkeit.', '', '', '', '', '', '', 23420),
(39516, 'deDE', 'Lehrt die Illidari, was Verwüstung bedeutet!', 'Lehrt Eure Dämonenjäger die Wege der Verwüstung.', 'Darum fürchtet uns die Legion. Nur wir können ihre Geheimnisse gegen sie verwenden und dabei noch stärker werden, als sie es jemals sein könnten.', '', '', '', '', '', '', 23420),
(40051, 'deDE', 'Dämonengeheimnisse', 'Lest den Folianten der Dämonengeheimnisse und entscheidet Euch für eine Spezialisierung: Verwüstung oder Rachsucht.', 'Die Dämonen haben einen entscheidenden Fehler begangen, als sie ihre Geheimnisse in diesem Buch niederschrieben. Die davon ausgehende Teufelsenergie ist überwältigend.\n\nDoch sie reicht nur für eines der Rituale. Es wird das Buch völlig verzehren.\n\nIhr entscheidet, welche Geheimnisse wir gegen die Brennende Legion einsetzen.', '', '', '', '', '', '', 23420),
(40222, 'deDE', 'Der Foliant der Wichtelmutter', 'Beschafft den Folianten der Dämonengeheimnisse.', 'Seht Ihr diese riesige Höhle? Dort lauert eine schreckliche, extrem mächtige Wichtelmutter.\n\nDas ist natürlich nicht die gute Nachricht. Die befindet sich in ihren stinkenden Klauen.\n\nWir hörten, sie ist eine Art dämonische Geheimnisträgerin. Sie führt dort abscheuliche Rituale für die Legion durch und hält sie in einem Buch fest.\n\nWir brauchen diese Macht! Wir brauchen dieses Buch!', '', 'Stehlt ihren Folianten der Dämonengeheimnisse und labt Euch zusammen mit Euren Illidari an seiner Macht.', 'Fecundia', '', '', '', 23420),
(38819, 'deDE', 'Ihre Zahl ist Legion', 'Lasst die Untertanen der Brutkönigin und die Brennende Legion in der Gegend um die Infernospitze Euren Zorn spüren.', 'Ja, diese Verwüster müssen ausgeschaltet werden, doch es gibt noch viel mehr zu tun.\n\nEs steht schlecht, doch wir zählen auf Euch. Die Kommandeure der Brutkönigin kontrollieren den Vulkan, sie müssen sterben. Wir müssen die Legion zerschmettern. Einfach alles, was nötig ist, um den absoluten Sieg zu erringen.', '', '', '', '', '', '', 23420),
(38725, 'deDE', 'Zum Fäulnishort', 'Trefft Euch mit Kor''vas Blutdorn am Fäulnishort.', 'Einer der Dämonen, die ich verhört habe, enthüllte eine besonders delikate Information. Es scheint, im Vulkan lebt eine Wichtelmutter, die Geheimnisse der Legion niederschreibt.\n\nKor''vas ist losgezogen, um diesen Dämon zu finden und ihre Geheimnisse für uns sicherzustellen.', '', '', '', '', '', '', 23420),
(38727, 'deDE', 'Stoppt das Bombardement', 'Zerstört die Verwüster der Legion.', 'Jaces Schutzzauber werden nicht mehr lange halten. Wir brauchen jemanden, der dort raus geht und sicherstellt, dass diese Verwüster der Legion kaltgestellt werden.\n\n<Kayn wirft Euch einen vielsagenden Blick mit seinen magisch verbesserten Augen zu.>', '', '', '', '', '', '', 23420),
(39495, 'deDE', 'Kein Versteckspiel mehr', 'Tötet Teufelslord Caza und beansprucht seine Teufelskraft.', 'Jetzt, wo Ihr die Dämonen enttarnt habt, kann ich sie auch spüren. Ich spüre sogar einen extrem mächtigen Teufelslord tief in der Höhle. Entreißt ihm seine Macht, solange Ihr noch könnt.\n\nWenn Ihr den Anführer ausschaltet, kümmern wir uns um den Rest seiner Truppen in der Höhle.', '', 'Entreißt ihm seine Kraft und schützt Eure Armee beim Vorposten der Illidari vor einem Hinterhalt.', 'Teufelslord Caza', '', '', '', 23420),
(39262, 'deDE', 'Verleiht mir die Augen der Allsicht', 'Sprecht mit Jace Düsterweber und nutzt dann Eure Geistersicht, um die Höhle nach verborgenen Dämonen abzusuchen.', 'Jace Düsterweber schwört, er habe für einen kurzen Moment eine dämonische Präsenz hinter uns gespürt. Er hat mit einigen unserer Truppen die Höhle im Südosten untersucht.\n\nSie haben darin nichts gefunden, doch er ist sicher, dass etwas faul ist.\n\nWürdet Ihr ihm Eure Erfahrung und, wichtiger noch, Eure Geistersicht leihen?', '', '', '', '', '', '', 23420),
(38813, 'deDE', 'Befehle für die Offiziere', 'Erteilt Euren Illidarioffizieren ihre jeweiligen Einsatzbefehle für den Angriff auf die Brennende Legion im Vulkan.', 'Es bleibt nur noch, Eure Befehle an unsere Hauptmänner der Illidari weiterzuleiten. Die stärksten Truppen der Legion wurden im Vulkan zusammengezogen. Doch dort werden sie nicht lange bleiben.\n\nWir müssen zuerst zuschlagen und zwar so hart wie möglich.\n\nSobald sie vernichtet sind, können wir zum Kommandoschiff hinauffliegen, Brutkönigin Tyranna töten und den Schlüsselstein an uns nehmen. Lord Illidan wird höchst zufrieden sein.', '', '', '', '', '', '', 23420),
(38766, 'deDE', 'Überrennt sie, bevor sie Euch überrennen', 'Vernichtet Verdammniskommandant Beliash und macht Euch seine Kraft zu eigen.', 'Der Verdammniswachenkommandant und seine Königin erfordern unsere Aufmerksamkeit. Seine Eredar beschwören Unmengen an Verstärkungstruppen.\n\nWir müssen dieser Bedrohung ein Ende setzen, bevor die Legion unseren Vorposten oben im Vulkan überrennt.\n\nUnd darüber hinaus könnt Ihr bei dieser Gelegenheit seine dämonische Kraft stehlen.', '', 'Das ist der Anführer der Legionstruppen in den niederen Regionen am Fuß des Vulkans. Macht Euch seine Kraft zu eigen.', 'Verdammniskommandant Beliash', '', '', '', 23420),
(38765, 'deDE', 'Auftritt der Illidari: Die Shivarra', 'Speist den Toraktivator der Legion mit einer kraftvollen Seele und ruft damit Eure Truppen der Shivarra herbei.', 'Unsere Dämonenherrinnen der Shivarra warten im Schwarzen Tempel. Es ist riskant, sie hierher zu bringen... schließlich waren sie einmal Teil der Brennenden Legion. Trotzdem... sie gehören zu den unerbittlichsten Kämpfern unter Lord Illidans Kommando.\n\n<Jace sieht Euch lange wortlos an.>\n\nIch fürchte, jemand wird für ihre Herbeirufung das ultimative Opfer bringen müssen.', '', '', '', '', '', '', 23420),
(39050, 'deDE', 'Treffen mit der Königin', 'Schließt das Ritual ab und spioniert die Anführer der Legion aus.', 'Wir können einen Teil der Essenz verwenden, um mein Ritual zu beenden. Dann werdet Ihr die Anführer der Legion ausspionieren und ihre Pläne sehen können.\n\nBenutzt den Nethertiegel, um das Ritual abzuschließen.', '', '', '', '', '', '', 23420),
(40379, 'deDE', 'Auftritt der Illidari: Naga der Echsennarbe', 'Speist den Toraktivator der Legion mit einer Seele und öffnet damit einen Weg für Eure Truppen der Echsennarbe.', 'Das nächste Tor ist unten an der Geschmolzenen Küste. Unsere Truppen der Echsennarbe warten am Schwarzen Tempel auf der anderen Seite.\n\nUnsere Nagaverbündeten wären nicht zwingend meine erste Wahl gewesen, doch sie haben Lord Illidan die Treue geschworen. Wir werden sehen...\n\nWir müssen einen Weg finden, den Toraktivator mit Energie zu versorgen. Er benötigt Seelen, wie die anderen.\n\n<Allari die Seelenfresserin grinst, als Savis das sagt.>', '', '', '', '', '', '', 23420),
(39049, 'deDE', 'Augen auf das Ziel', 'Tötet Inquisitor Seelenschmerz und beansprucht seine Macht.', 'Dieser Dämon hat sich als gänzlich nutzlos erwiesen. Ich habe die Seelensense eingesetzt, doch bisher hat er sich meinem Verhör widersetzt. Wir werden sehen, wie lang er das durchhält.\n\nIch habe eine andere Idee...\n\nNicht weit vor uns ist ein anderer Inquisitor. Diese Dämonen sind verheerende Teufelswirker, die durch schwebende Augen sehen und dunkle Pakte schließen. Wenn Ihr Euch seine Macht aneignet, könnte Jace Düsterweber sie in seinem Ritual einsetzen, um die Legion auszuspionieren.\n\nEr wird sie nicht kampflos aufgeben, also entreißt sie seinen kalten, toten Klauen.', '', 'Stehlt seine Essenz und werdet mächtiger.', 'Inquisitor Seelenschmerz', '', '', '', 23420),
(38759, 'deDE', 'Lasst sie frei', 'Einige Eurer Dämonenjäger sind in ernster Gefahr. Findet den seelengeschliffenen Schlüssel und holt sie da raus.', 'Cyana ging hinunter zur Geschmolzenen Küste. Sie wollte ein paar Dämonenjägern helfen, die vor uns durch das Portal kamen. Doch dabei ist etwas schiefgelaufen.\n\nDiesem Inquisitor zufolge werden sie alle von einem bisher unbekannten Dämonen festgehalten, den man "Wärter" nennt. Dieses Ding saugt seinen Opfern die Seele aus. Ich weiß nicht, wie lange sie noch durchhalten können.', '', '', '', '', '', '', 23420),
(40378, 'deDE', 'Auftritt der Illidari: Aschenzungen', 'Beschwört Eure Truppen der Aschenzungen, indem Ihr das Legionstor aktiviert.', 'Lord Illidan hat Euch das Kommando anvertraut. Nun brauchen wir den Rest unserer Illidaritruppen.\n\nVor uns gibt es drei Tore der Brennenden Legion. Aktiviert sie und unsere Illidari können vom Schwarzen Tempel aus zu uns stoßen.\n\nWie die meisten Konstruktionen der Legion werden die Tore mit Seelen angetrieben. Für das erste sollte es bereits reichen, was denkt Ihr? Ebnen wir unseren Truppen der Aschenzungen den Weg.', '', '', '', '', '', '', 23420),
(39279, 'deDE', 'Angriff auf Mardum', '', '', '', '', '', '', '', '', 23420),
(43806, 'deDE', 'Die Schlacht um die Verheerte Küste', 'Trefft Euch mit der dritten Flotte.', 'Die dritte Flotte ist bereits vor uns ausgelaufen, $n. Dieser Greif wird Euch dorthin bringen.\n\nViel Glück da draußen. Die Legion wird es uns bestimmt nicht leicht machen. Hoffen wir nur, dass König Varian die Lage im Griff hat.', '', '', '', '', '', '', 23420),
(37654, 'deDE', 'Gesetze des Meeres', 'Sucht nach Looper Allen in der Nähe des Schiffes, das an der Ostküste von Mak''rana auf Grund gelaufen ist.', '$gWerter Herr:Werte Dame;, bitte gewährt mir einen Augenblick Eurer kostbaren Zeit. Meine Bergungskollegen und ich haben Euer Schiff bemerkt, das unglücklicherweise letzte Nacht im Sturm auf Grund gelaufen ist.\n\nTut mir sehr leid um die Crew.\n\n<Herr Fessel unterstreicht dies mit einer mitfühlenden Geste.>\n\nAber Gesetz ist Gesetz. Das Seeverkehrsrecht ist hier absolut eindeutig. Wenn Ihr trotzdem noch ein paar Sachen bergen wollt, wäret Ihr dann vielleicht so liebenswürdig, nach meinem Akquise-Scout Looper Ausschau zu halten? Er hätte schon vor einer Weile pünktlich zum Nachmittagstee zurück sein sollen.', '', '', '', '', '', '', 23420),
(38407, 'deDE', 'Flaschenpost', 'Bringt Okunas Nachricht zu Lady Irisse.', '<Ihr habt bei der Leiche eine Flaschenpost gefunden. Es ist ein Hilferuf von jemandem namens Okuna Langhauer. Okuna berichtet, dass er zusammen mit seinen Schiffskameraden von Seeriesen gefangen gehalten wird.\n\nVielleicht kann Euch ja Lady Irisse am Zerfallenen Palast zeigen, wo das ist, wenn Ihr ihr diesen Brief zeigt?>', '', '', '', '', '', '', 23420),
(44414, 'deDE', 'Teufelsbrut von Lothros', 'Bezwingt Lothros.', 'Diese Teufelsbrut nimmt einfach kein Ende! Ich kann sie nicht schnell genug niedermähen, um an den Schreckenslord im Mausoleum zu gelangen.\n\nMeister Illidan, werdet Ihr mich begleiten, damit wir Lothros gemeinsam zur Strecke bringen können?', '', '', 'Lothros', '', '', '', 23420),
(42985, 'deDE', 'Eine königliche Audienz', 'Meldet Euch bei Kriegshäuptling Sylvanas Windläufer in Unterstadt.', '$n,$B$Bwer nicht auf der Seite der Horde steht, stellt sich gegen sie. Als Champion habt Ihr solche Feinde zuhauf vernichtet.$B$BIch bewundere Eure Hingabe. Dank Euch werden wir zu ungeahnter Macht aufsteigen und die Allianz endgültig in die Schranken weisen.$B$BKommt so schnell wie möglich zu mir nach Unterstadt. Eure Siege verdienen eine angemessene Belohnung.$B$B- Kriegshäuptling Sylvanas Windläufer', '', '', 'Sylvanas Windläufer', '', '', '', 23420),
(38643, 'deDE', 'Ein Dorf in Gefahr', 'Sprecht mit Bürgermeister Heathrow in Bradenbach.', 'Der Leichnam von Darcy Heathrow liegt vor Euch, von Pfeilen durchbohrt und mit Stichwunden übersät.$b$bIn einer Hand umklammert sie eine Notiz, die sie mit letzter Kraft verfasst hat:$b$b"Die Toten marschieren nach Bradenbach.$b$bBitte helft uns!"', '', '', '', '', '', '', 23420),
(40122, 'deDE', 'Cenarius, Hüter des Hains', 'Sprecht mit Malfurion Sturmgrimm in Lorlathil in Val''sharah.', 'Ahh, Val''sharah.$b$bEs ist schon lange her, dass ich einen Fuß in diese heiligen Wälder gesetzt habe. Viel zu lange...$b$bUnsere Aufgabe hier ist einfach, $C. Wir müssen meinen Mentor, Cenarius, rufen und herausfinden, wo er die Tränen von Elune verwahrt. Sie werden Euch unserem Ziel näherbringen, Sargeras'' Grab zu versiegeln und die Legion aufzuhalten.$b$bAuf Euch warten schwere Prüfungen. Euer Aufenthalt hier ist lediglich eine Atempause von kurzer Dauer.$b$bKommt, das wird nicht viel Zeit in Anspruch nehmen.', '', '', '', '', '', '', 23420),
(38672, 'deDE', 'Ausbruch', 'Befreit Altruis den Leidenden und Kayn Sonnenzorn.', 'Wir können uns unseren Weg aus dem Verlies nicht ohne Hilfe freikämpfen.\n\nZwei Eurer Verbündeten werden in diesem Raum gefangen gehalten: Kayn Sonnenzorn und Altruis der Leidende.\n\nSchnell, befreit sie! Ihre Fähigkeiten sind von größtem Nutzen für uns.', '', '', '', '', '', '', 23420),
(44701, 'deDE', 'Sturmheim', 'Sprecht mit Nathanos Pestrufer in der Violetten Zitadelle in Dalaran.', 'Gut, dass wir Euch gefunden haben, $n!\n\nNathanos Pestrufer, Champion der Bansheekönigin selbst, wünscht, Euch in Dalaran zu sprechen. Es klang dringend. Er wartet in der Violetten Zitadelle auf Euch.\n\nBeeilt Euch!', '', '', 'Lord Grayson Schattenbruch', '', '', '', 23420),
(44137, 'deDE', 'Auf ins Getümmel', 'Sprecht mit dem kommandierenden Dämonenjäger am Verteidigungsposten der Illidari.', 'Die Brennende Legion hat hier einen Sieg errungen, aber die Illidari auch. Anscheinend können die Dämonenjäger dem Ansturm standhalten... wenn auch nur mit Müh und Not.\n\nKommt, setzen wir Eure Waffe sinnvoll ein. Ich werde unsere nächsten Schritte planen, während Ihr die Legion niederkämpft.', '', '', '', '', '', '', 23420),
(34398, 'deDE', 'Das Dunkle Portal', 'Sprecht mit Erzmagier Khadgar am Fuße des Dunklen Portals in den Verwüsteten Landen.', '$B|cFF660000             AN ALLE HELDEN$b                 ABENTEURER$b        VERTEIDIGER VON AZEROTH|r$b$bErzmagier Khadgar, Held des Zweiten Krieges, stellt ein Bündnis der stärksten Kämpfer von Azeroth zusammen, um durch das Dunkle Portal zu treten und zu verhindern, dass die Eiserne Horde in Azeroth einfällt.$b$bKämpfer, die unsterblichen Ruhm erlangen möchten, finden Khadgar am Fuße des Dunklen Portals in den Verwüsteten Landen.$b$bSicherheit nicht gewährleistet.', '', '', '', '', '', '', 23420),
(34209, 'deDE', 'Sicheres Geleit', 'Trefft Euch mit Cordana Teufelsang außerhalb von Throm''var auf den Frostwindklippen.', 'Es hat sich eine äußerst wichtige Angelegenheit ergeben. Während Ihr unterwegs wart, hat mich Erzmagier Khadgar kontaktiert.$B$BEr untersucht dämonische Einflüsse im Norden.$B$BDie Frostwölfe werden Khadgar nicht akzeptieren, da er ein Mensch ist. Auch eine Nachtelfe wie ich wird dort nicht mit offenen Armen empfangen werden... Euch hingegen könnte es gelingen, sie von unserer Sache zu überzeugen, wenn Ihr für den Erzmagier bürgt.$B$BIch werde außerhalb von Throm''var auf den Frostwindklippen auf Euch warten.', '', '', '', '', '', '', 23420),
(33543, 'deDE', 'Die Lachenden Schädel', 'Findet ein Stammesmitglied des Lachenden Schädels.', 'Ah, $p, gut. Ich brauche jemanden, der nicht meinem Klan angehört, um mit dem Lachenden Schädel zu verhandeln.$b$bWir brauchen ihre Hilfe, um uns in diesem Land zurechtzufinden, aber sie sind gefährlich und unberechenbar, ganz so wie Gorgrond selbst.$b$bFolgt dieser Straße und findet jemanden, der bereit ist, mit Euch zu reden.$b$bHoffentlich seid Ihr so neu und interessant für sie, dass sie zuerst reden, bevor sie zustechen.', '', '', '', '', '', '', 23420),
(35077, 'deDE', 'Entpilzung', 'Benutzt den Flammenwerfer QR58, um 20 Sporenpusteln zu zerstören.', 'Habt Ihr die Pilze draußen im Wasser gesehen? Sie wachsen schneller als ein Babyravasaurier aus Un''Goro, und wenn wir sie nicht unter Kontrolle bekommen, wird die Werkstatt genauso ruiniert wie unser letztes Dorf.$b$bOnkel Krixel hat mir immer gesagt, man soll die roten Pilze verbrennen, weil die für das schnelle Wachstum verantwortlich sind.$b$bIhr könntet uns helfen, indem Ihr einen Flammenwerfer nehmt und einige der Sporen im Watt da unten vernichtet.', '', '', '', '', '', '', 23420),
(34655, 'deDE', 'Die Schatten von Skettis', 'Sprecht mit Reshad in Skettis.', 'Mein Volk flüchtete scharenweise aus unserer Heimat. Ich verkaufe Glücksbringer, um die Reise einfacher zu machen.$B$BDoch wie ich sehe, seid Ihr nicht an Glückbringern interessiert... Nein, Ihr sucht nach Antworten.$B$BSkettis liegt direkt da vorne. Einst war es eine große Stadt, die Terokk als Zufluchtsort für die Unerwünschten errichtete. Doch jetzt ist es ein ruiniertes Elendsviertel.$B$BEin alter Geschichtenerzähler lebt dort – ein Schriftenbewahrer namens Reshad. Findet ihn und sagt ihm diese Worte: "Der Schatten wächst." Er weiß dann, dass Ihr $gein Freund:eine Freundin; seid.$B$BGeht jetzt. Eure Gegenwart ist schlecht fürs Geschäft.', '', '', '', '', '', '', 23420),
(33735, 'deDE', 'Mangelware', 'Sammelt $1oa Sprengladungen der Eisernen Horde ein.', 'Der Feind hat Sprengladungen am Strand da unten deponiert. Wir müssen sie bergen, damit sie nicht gegen uns verwendet werden.\n\nBerichten zufolge sind Kisten mit Sprengstoff an der Küste entlang verstreut.\n\nWährend Ihr danach sucht, werden Durotan und ich weiter in das Gebiet der Eisernen Horde vordringen.', '', '', '', '', '', '', 23420),
(34965, 'deDE', 'Audienz am Thron', 'Haltet mit den Zornen am Thron der Elemente Zwiesprache.', 'Bereits kurz nach meiner Ankunft in Nagrand trug der Wind die Stimme von Kalandrios an meine Ohren.$B$BEs ist eine große Ehre, zum Thron der Elemente gerufen zu werden, doch mein Herz litt unter der düsteren Vorahnung, worum es gehen würde.$B$BIn der Luft liegt eine Krankheit, die ich überall um uns herum spüre. Sie umfängt alles Leben hier... fesselnd... erstickend...$B$BKommt! Es wird Zeit, mit den Zornen in Kontakt zu treten und ihnen unsere Hilfe anzubieten. Ein unsichtbarer Schatten ist auf das Tal gefallen und womöglich ist es an uns, ihn zu entfernen.', '', '', '', '', '', '', 23420),
(34795, 'deDE', 'Die Macht des Kriegshymnenklans', 'Sprecht mit Thrall in Wor''var.', '<Der Offizier leidet unter starken Schmerzen. Es ist offenkundig, dass er nicht mehr lange durchhalten wird.>$B$BDer Kriegshymnenklan. Überraschungsangriff.$B$BAuf dem Weg zur... Brennenden Klinge.$B$B<Er hält kurz inne, bevor er fortfährt.>$B$BSie haben den... Bauplan...$B$B<Dies sind seine letzten Worte.>', '', '', '', '', '', '', 23420),
(33815, 'deDE', 'Das Lied von Frost und Feuer', 'Trefft Euch mit Durotan.', 'Ihr habt viele meiner Klanmitglieder aus den Sklavenpferchen von Tanaan gerettet. Ihre Befreiung und die Zerstörung des Dunklen Portals sind mir Beweis genug, dass Ihr $geiner:eine; von uns seid.$b$bAllerdings erachtet Euch unser Anführer, Durotan, immer noch als $gFremden:Fremde;. Lasst mich Euch ihm vorstellen.', '', '', '', '', '', '', 23420),
(40077, 'deDE', 'Die Invasion beginnt', 'Tötet $1oa Dämonen und tauscht das Banner der Legion aus, um den Grat der Verzweiflung einzunehmen.', 'Die Angriffstruppen unserer Dämonenjäger sind hier. Sie sind bereits im Gefecht gegen die Legion.\n\n$n, hisst voller Stolz unsere Flagge und lasst sie sehen, mit wem sie es zu tun haben. Ich will die Furcht in ihren Augen sehen.', '', '', '', '', '', '', 23420);


DELETE FROM `quest_objectives_locale` WHERE (`ID`=279980) OR (`ID`=281206) OR (`ID`=279956) OR (`ID`=280462) OR (`ID`=280461) OR (`ID`=280460) OR (`ID`=280459) OR (`ID`=280458) OR (`ID`=280435) OR (`ID`=281966) OR (`ID`=281103) OR (`ID`=280502) OR (`ID`=280501) OR (`ID`=280500) OR (`ID`=280006) OR (`ID`=280005) OR (`ID`=279993) OR (`ID`=278683) OR (`ID`=278682) OR (`ID`=278681) OR (`ID`=278680) OR (`ID`=279857) OR (`ID`=278669) OR (`ID`=279864) OR (`ID`=279863) OR (`ID`=279862) OR (`ID`=279861) OR (`ID`=279765) OR (`ID`=280354) OR (`ID`=279764) OR (`ID`=279763) OR (`ID`=278955) OR (`ID`=278473) OR (`ID`=278472) OR (`ID`=278458) OR (`ID`=280115) OR (`ID`=278937) OR (`ID`=280772) OR (`ID`=278948) OR (`ID`=280773) OR (`ID`=280771) OR (`ID`=280770) OR (`ID`=279707) OR (`ID`=278413) OR (`ID`=278929) OR (`ID`=280293) OR (`ID`=278928) OR (`ID`=280769) OR (`ID`=281030) OR (`ID`=281156) OR (`ID`=281158) OR (`ID`=280768) OR (`ID`=279929) OR (`ID`=279741) OR (`ID`=279930) OR (`ID`=279928) OR (`ID`=279937) OR (`ID`=279936) OR (`ID`=279935) OR (`ID`=279757) OR (`ID`=279289) OR (`ID`=279292) OR (`ID`=279288) OR (`ID`=285830) OR (`ID`=280339) OR (`ID`=286143) OR (`ID`=280526) OR (`ID`=278275) OR (`ID`=278274) OR (`ID`=272640) OR (`ID`=271524) OR (`ID`=273428) OR (`ID`=272992) OR (`ID`=273268) OR (`ID`=273269) OR (`ID`=272643) OR (`ID`=280292) OR (`ID`=280276);
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(279980, 'deDE', 38729, 0, 'Sargeritschlüsselstein aktiviert', 23420),
(281206, 'deDE', 38728, 2, 'Sucht einen Weg nach unten', 23420),
(279956, 'deDE', 39663, 0, 'Reist zur Teufelshammer', 23420),
(280462, 'deDE', 39516, 4, 'Mannethrel gelehrt', 23420),
(280461, 'deDE', 39516, 3, 'Kor''vas gelehrt', 23420),
(280460, 'deDE', 39516, 2, 'Kayn gelehrt', 23420),
(280459, 'deDE', 39516, 1, 'Cyana gelehrt', 23420),
(280458, 'deDE', 39516, 0, 'Allari gelehrt', 23420),
(280435, 'deDE', 40051, 0, 'Wählt zwischen Verwüstung und Rachsucht', 23420),
(281966, 'deDE', 40222, 1, 'Höhleneingangsklumpen', 23420),
(281103, 'deDE', 38819, 10, 'Stabilisator der Pfeiler des Kummers zerstört', 23420),
(280502, 'deDE', 38819, 9, 'Mo''arg', 23420),
(280501, 'deDE', 38819, 8, 'Schwache Gegner', 23420),
(280500, 'deDE', 38819, 7, 'Normale Gegner', 23420),
(280006, 'deDE', 38819, 6, 'Ersten Stabilisator der Verdammnisfestung zerstört', 23420),
(280005, 'deDE', 38819, 5, 'Stabilisator der Verdammnisfestung', 23420),
(279993, 'deDE', 38819, 3, 'Verletzter Dämonenjäger', 23420),
(278683, 'deDE', 38819, 2, 'Benannte Bosse', 23420),
(278682, 'deDE', 38819, 1, 'Ersten Seelenernter zerstört', 23420),
(278681, 'deDE', 38819, 0, 'Seelenernter zerstört', 23420),
(278680, 'deDE', 38819, -1, 'Kriegsfortschritt', 23420),
(279857, 'deDE', 38727, 1, 'Verwüster der Seelenmaschine', 23420),
(278669, 'deDE', 38727, 0, 'Verwüsterbanner der Seelenmaschine', 23420),
(279864, 'deDE', 38727, 5, 'Verwüster der Verderbnisschmiede', 23420),
(279863, 'deDE', 38727, 4, 'Verwüsterbanner der Verderbnisschmiede', 23420),
(279862, 'deDE', 38727, 3, 'Verwüster der Verdammnisfestung', 23420),
(279861, 'deDE', 38727, 2, 'Verwüsterbanner der Verdammnisfestung', 23420),
(279765, 'deDE', 39495, 0, 'Caza getötet und Macht übernommen', 23420),
(280354, 'deDE', 39495, 1, 'Erdrutsch', 23420),
(279764, 'deDE', 39262, 1, 'Wendet Euch zur Höhle und verwendet ''Geistersicht''', 23420),
(279763, 'deDE', 39262, 0, 'Sprecht mit Jace Düsterweber', 23420),
(278955, 'deDE', 38813, 2, 'Matronenmutter Malicia informiert', 23420),
(278473, 'deDE', 38813, 1, 'Lady S''theno informiert', 23420),
(278472, 'deDE', 38813, 0, 'Schlachtenfürst Gaardoun informiert', 23420),
(278458, 'deDE', 38766, 0, 'Beliash getötet und Macht übernommen', 23420),
(280115, 'deDE', 38765, 3, 'Erster beschworener Wächter', 23420),
(278937, 'deDE', 38765, 2, 'Streitkräfte der Shivarra', 23420),
(280772, 'deDE', 38765, 0, 'Opfer gebracht', 23420),
(278948, 'deDE', 39050, 0, 'Ritual abgeschlossen', 23420),
(280773, 'deDE', 40379, 2, 'Erster beschworener Wächter', 23420),
(280771, 'deDE', 40379, 1, 'Streitkräfte der Echsennarbe', 23420),
(280770, 'deDE', 40379, 0, 'Seele geopfert', 23420),
(279707, 'deDE', 39049, 2, 'Inquisitor Seelenschmerz getötet und Macht übernommen', 23420),
(278413, 'deDE', 38759, 1, 'Mannethrel Düsterstern befreit', 23420),
(278929, 'deDE', 38759, 2, 'Izal Weißmond befreit', 23420),
(280293, 'deDE', 38759, 4, 'Cyana Nachtgleve befreit', 23420),
(278928, 'deDE', 38759, 0, 'Belath Dämmerklinge befreit', 23420),
(280769, 'deDE', 40378, 1, 'Erster beschworener Wächter', 23420),
(281030, 'deDE', 40378, 2, 'Sucht Allari im Südosten', 23420),
(281156, 'deDE', 40378, 3, 'Nehmt Illidans Gabe an', 23420),
(281158, 'deDE', 40378, 4, 'Verstecktes Ziel: Seht Teufelssäbler', 23420),
(280768, 'deDE', 40378, 0, 'Streitkräfte der Aschenzungen', 23420),
(279929, 'deDE', 39279, 8, 'Erstes Legionsportal zerstört', 23420),
(279741, 'deDE', 39279, 5, 'Legionsportal', 23420),
(279930, 'deDE', 39279, 9, 'Ersten Teufelskondensator zerstört', 23420),
(279928, 'deDE', 39279, 7, 'Teufelskondensator zerstört', 23420),
(279937, 'deDE', 39279, 10, 'Legionskommunikator Nr. 3', 23420),
(279936, 'deDE', 39279, 2, 'Legionskommunikator Nr. 2', 23420),
(279935, 'deDE', 39279, 1, 'Legionskommunikator Nr. 1', 23420),
(279757, 'deDE', 39279, 6, 'Schläger der Mo''arg', 23420),
(279289, 'deDE', 39279, 0, 'Normale Gegner', 23420),
(279292, 'deDE', 39279, 3, 'Geringe Gegner', 23420),
(279288, 'deDE', 39279, -1, 'Greift die Legion im Tiefland an', 23420),
(285830, 'deDE', 43806, 0, 'Die Verheerte Küste angegriffen', 23420),
(280339, 'deDE', 40122, 0, 'Sprecht mit Malfurion', 23420),
(286143, 'deDE', 38672, 5, 'Altruis'' Ende', 23420),
(280526, 'deDE', 38672, 2, 'Kayns Ende', 23420),
(278275, 'deDE', 38672, 1, 'Kayn befreit', 23420),
(278274, 'deDE', 38672, 0, 'Altruis befreit', 23420),
(272640, 'deDE', 34398, 0, 'Sprecht mit Erzmagier Khadgar', 23420),
(271524, 'deDE', 33543, 0, 'Nähert Euch friedlich Todesgrimasse ', 23420),
(273428, 'deDE', 35077, 0, 'Sporenpusteln verbrannt', 23420),
(272992, 'deDE', 34655, 0, 'Reshad das Passwort genannt', 23420),
(273268, 'deDE', 34965, 0, 'Kommuniziert mit den Zornen', 23420),
(273269, 'deDE', 34965, 1, 'Sprecht mit Scharfseher Drek''Thar', 23420),
(272643, 'deDE', 33815, 0, 'Durotan vorgestellt', 23420),
(280292, 'deDE', 40077, 1, 'Ändert das Banner der Legion', 23420),
(280276, 'deDE', 40077, 0, 'Dämonen getötet', 23420);


DELETE FROM `gameobject_template_locale` WHERE ('entry'=252405 AND `locale`='deDE') OR ('entry'=245840 AND `locale`='deDE') OR ('entry'=245208 AND `locale`='deDE') OR ('entry'=245434 AND `locale`='deDE') OR ('entry'=244644 AND `locale`='deDE') OR ('entry'=245207 AND `locale`='deDE') OR ('entry'=247182 AND `locale`='deDE') OR ('entry'=245206 AND `locale`='deDE') OR ('entry'=244654 AND `locale`='deDE') OR ('entry'=244655 AND `locale`='deDE') OR ('entry'=244658 AND `locale`='deDE') OR ('entry'=244659 AND `locale`='deDE') OR ('entry'=245915 AND `locale`='deDE') OR ('entry'=244653 AND `locale`='deDE') OR ('entry'=244652 AND `locale`='deDE') OR ('entry'=244354 AND `locale`='deDE') OR ('entry'=244355 AND `locale`='deDE') OR ('entry'=244657 AND `locale`='deDE') OR ('entry'=244662 AND `locale`='deDE') OR ('entry'=244656 AND `locale`='deDE') OR ('entry'=244663 AND `locale`='deDE') OR ('entry'=244353 AND `locale`='deDE') OR ('entry'=245913 AND `locale`='deDE') OR ('entry'=244352 AND `locale`='deDE') OR ('entry'=245751 AND `locale`='deDE') OR ('entry'=245750 AND `locale`='deDE') OR ('entry'=245749 AND `locale`='deDE') OR ('entry'=245756 AND `locale`='deDE') OR ('entry'=245914 AND `locale`='deDE') OR ('entry'=245747 AND `locale`='deDE') OR ('entry'=245748 AND `locale`='deDE') OR ('entry'=245755 AND `locale`='deDE') OR ('entry'=245754 AND `locale`='deDE') OR ('entry'=245757 AND `locale`='deDE') OR ('entry'=245752 AND `locale`='deDE') OR ('entry'=245758 AND `locale`='deDE') OR ('entry'=245753 AND `locale`='deDE') OR ('entry'=245611 AND `locale`='deDE') OR ('entry'=245746 AND `locale`='deDE') OR ('entry'=245761 AND `locale`='deDE') OR ('entry'=245760 AND `locale`='deDE') OR ('entry'=245745 AND `locale`='deDE') OR ('entry'=245759 AND `locale`='deDE') OR ('entry'=245741 AND `locale`='deDE') OR ('entry'=245744 AND `locale`='deDE') OR ('entry'=245742 AND `locale`='deDE') OR ('entry'=245740 AND `locale`='deDE') OR ('entry'=245743 AND `locale`='deDE') OR ('entry'=245739 AND `locale`='deDE') OR ('entry'=245737 AND `locale`='deDE') OR ('entry'=245738 AND `locale`='deDE') OR ('entry'=244661 AND `locale`='deDE') OR ('entry'=244660 AND `locale`='deDE') OR ('entry'=244404 AND `locale`='deDE') OR ('entry'=243944 AND `locale`='deDE') OR ('entry'=243943 AND `locale`='deDE') OR ('entry'=243945 AND `locale`='deDE') OR ('entry'=244588 AND `locale`='deDE') OR ('entry'=244927 AND `locale`='deDE') OR ('entry'=244923 AND `locale`='deDE') OR ('entry'=244944 AND `locale`='deDE') OR ('entry'=244943 AND `locale`='deDE') OR ('entry'=247411 AND `locale`='deDE') OR ('entry'=243946 AND `locale`='deDE') OR ('entry'=244596 AND `locale`='deDE') OR ('entry'=244582 AND `locale`='deDE') OR ('entry'=253189 AND `locale`='deDE') OR ('entry'=245728 AND `locale`='deDE') OR ('entry'=244700 AND `locale`='deDE') OR ('entry'=244466 AND `locale`='deDE') OR ('entry'=245112 AND `locale`='deDE') OR ('entry'=246269 AND `locale`='deDE') OR ('entry'=244638 AND `locale`='deDE') OR ('entry'=243965 AND `locale`='deDE') OR ('entry'=246353 AND `locale`='deDE') OR ('entry'=245345 AND `locale`='deDE') OR ('entry'=243967 AND `locale`='deDE') OR ('entry'=245995 AND `locale`='deDE') OR ('entry'=246012 AND `locale`='deDE') OR ('entry'=245996 AND `locale`='deDE') OR ('entry'=244699 AND `locale`='deDE') OR ('entry'=244701 AND `locale`='deDE') OR ('entry'=246486 AND `locale`='deDE') OR ('entry'=246485 AND `locale`='deDE') OR ('entry'=243968 AND `locale`='deDE') OR ('entry'=246296 AND `locale`='deDE') OR ('entry'=244694 AND `locale`='deDE') OR ('entry'=244692 AND `locale`='deDE') OR ('entry'=254868 AND `locale`='deDE') OR ('entry'=254867 AND `locale`='deDE') OR ('entry'=254866 AND `locale`='deDE') OR ('entry'=254865 AND `locale`='deDE') OR ('entry'=245045 AND `locale`='deDE') OR ('entry'=245170 AND `locale`='deDE') OR ('entry'=246147 AND `locale`='deDE') OR ('entry'=246249 AND `locale`='deDE') OR ('entry'=245316 AND `locale`='deDE') OR ('entry'=244604 AND `locale`='deDE') OR ('entry'=246567 AND `locale`='deDE') OR ('entry'=241757 AND `locale`='deDE') OR ('entry'=246566 AND `locale`='deDE') OR ('entry'=244601 AND `locale`='deDE') OR ('entry'=243335 AND `locale`='deDE') OR ('entry'=243059 AND `locale`='deDE') OR ('entry'=244691 AND `locale`='deDE') OR ('entry'=241756 AND `locale`='deDE') OR ('entry'=244689 AND `locale`='deDE') OR ('entry'=244440 AND `locale`='deDE') OR ('entry'=242989 AND `locale`='deDE') OR ('entry'=244441 AND `locale`='deDE') OR ('entry'=242990 AND `locale`='deDE') OR ('entry'=243873 AND `locale`='deDE') OR ('entry'=244916 AND `locale`='deDE') OR ('entry'=242987 AND `locale`='deDE') OR ('entry'=259043 AND `locale`='deDE') OR ('entry'=250560 AND `locale`='deDE') OR ('entry'=244439 AND `locale`='deDE') OR ('entry'=241751 AND `locale`='deDE') OR ('entry'=246309 AND `locale`='deDE');
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(252405, 'deDE', 'Das Verlies der Wächterinnen', '', NULL, 23420),
(245840, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(245208, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(245434, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244644, 'deDE', 'Aufstieg der Wächterin', '', NULL, 23420),
(245207, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(247182, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(245206, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244654, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244655, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244658, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244659, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(245915, 'deDE', 'Die Richtkammer', '', NULL, 23420),
(244653, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244652, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244354, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244355, 'deDE', 'Die Befehlskammer', '', NULL, 23420),
(244657, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244662, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244656, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244663, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244353, 'deDE', 'Die Eiskammer', '', NULL, 23420),
(245913, 'deDE', 'Die Eiskammer', '', NULL, 23420),
(244352, 'deDE', 'Die Spiegelkammer', '', NULL, 23420),
(245751, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245750, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245749, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245756, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245914, 'deDE', 'Die Spiegelkammer', '', NULL, 23420),
(245747, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245748, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245755, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245754, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245757, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245752, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245758, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245753, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245611, 'deDE', 'Vereiste Brücke', '', NULL, 23420),
(245746, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245761, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245760, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245745, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245759, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245741, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245744, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245742, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245740, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245743, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245739, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245737, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(245738, 'deDE', 'Spiegelkachel', '', NULL, 23420),
(244661, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244660, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244404, 'deDE', 'Illidans Gemächer', '', NULL, 23420),
(243944, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(243943, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(243945, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244588, 'deDE', 'Zelle der Wächterinnen', '', NULL, 23420),
(244927, 'deDE', 'Kollisionswand', '', NULL, 23420),
(244923, 'deDE', 'Hebel', '', NULL, 23420),
(244944, 'deDE', 'Altruis'' Kriegsgleven', '', NULL, 23420),
(244943, 'deDE', 'Kayns Kriegsgleven', '', NULL, 23420),
(247411, 'deDE', 'Kollisionswand', '', NULL, 23420),
(243946, 'deDE', 'Tor der Wächterin', '', NULL, 23420),
(244596, 'deDE', 'Zelle der Wächterinnen', '', NULL, 23420),
(244582, 'deDE', 'Waffenständer der Dämonenjäger', '', NULL, 23420),
(253189, 'deDE', 'Kriegsgleven der Illidari', '', NULL, 23420),
(245728, 'deDE', 'Sargeritschlüsselstein', '', NULL, 23420),
(244700, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(244466, 'deDE', 'Teufelsportal', '', NULL, 23420),
(245112, 'deDE', 'Foliant der Dämonengeheimnisse', '', NULL, 23420),
(246269, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(244638, 'deDE', 'Brunnen der Seelen', '', NULL, 23420),
(243965, 'deDE', 'Banner der Illidari', '', NULL, 23420),
(246353, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(245345, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(243967, 'deDE', 'Banner der Illidari', '', NULL, 23420),
(245995, 'deDE', 'Pfeilerstabilisator', '', NULL, 23420),
(246012, 'deDE', 'Pfeilerstabilisator', '', NULL, 23420),
(245996, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(244699, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(244701, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(246486, 'deDE', 'Pfeiler des Kummers', '', NULL, 23420),
(246485, 'deDE', 'Pfeiler des Kummers', '', NULL, 23420),
(243968, 'deDE', 'Banner der Illidari', '', NULL, 23420),
(246296, 'deDE', 'Spinnennetz', '', NULL, 23420),
(244694, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(244692, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(254868, 'deDE', 'Hocker', '', NULL, 23420),
(254867, 'deDE', 'Hocker', '', NULL, 23420),
(254866, 'deDE', 'Amboss', '', NULL, 23420),
(254865, 'deDE', 'Schmiede', '', NULL, 23420),
(245045, 'deDE', 'Erdrutsch', '', NULL, 23420),
(245170, 'deDE', 'Dämonenzauberschutz', '', NULL, 23420),
(246147, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(246249, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(245316, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(244604, 'deDE', 'Pfeilerstabilisator', '', NULL, 23420),
(246567, 'deDE', 'Geist des Kummers B', '', NULL, 23420),
(241757, 'deDE', 'Toraktivator der Legion', '', NULL, 23420),
(246566, 'deDE', 'Geist des Kummers A', '', NULL, 23420),
(244601, 'deDE', 'Pfeilerstabilisator', '', NULL, 23420),
(243335, 'deDE', 'Nethertiegel', '', NULL, 23420),
(243059, 'deDE', 'Dämonenzauberschutz', '', NULL, 23420),
(244691, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(241756, 'deDE', 'Toraktivator der Legion', '', NULL, 23420),
(244689, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420),
(244440, 'deDE', 'Legionskommunikator', '', NULL, 23420),
(242989, 'deDE', 'Wärterkäfig', '', NULL, 23420),
(244441, 'deDE', 'Legionskommunikator', '', NULL, 23420),
(242990, 'deDE', 'Wärterkäfig', '', NULL, 23420),
(243873, 'deDE', 'Legionsportal', '', NULL, 23420),
(244916, 'deDE', 'Wärterkäfig', '', NULL, 23420),
(242987, 'deDE', 'Wärterkäfig', '', NULL, 23420),
(259043, 'deDE', 'Banner der Legion 02', '', NULL, 23420),
(250560, 'deDE', 'Banner der Legion', '', NULL, 23420),
(244439, 'deDE', 'Legionskommunikator', '', NULL, 23420),
(241751, 'deDE', 'Toraktivator der Legion', '', NULL, 23420),
(246309, 'deDE', 'Kleine Schatztruhe', '', NULL, 23420);


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text     xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_offer_reward` WHERE `ID` IN (38729 /*38729*/, 38728 /*38728*/, 39663 /*39663*/, 39516 /*39516*/, 40051 /*40051*/, 38819 /*38819*/, 38727 /*38727*/, 40222 /*40222*/, 38725 /*38725*/, 39495 /*39495*/, 39262 /*39262*/, 38813 /*38813*/, 38766 /*38766*/, 38765 /*38765*/, 39050 /*39050*/, 40379 /*40379*/, 39049 /*39049*/, 38759 /*38759*/, 40378 /*40378*/, 40077 /*40077*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(38729, 0, 0, 0, 0, 0, 0, 0, 0, '<Während Ihr die Macht des Sargeritschlüsselsteins beschwört, wird das Teufelsportal mit einer absonderlichen Energie überflutet.\n\nEs wird Zeit, triumphierend zum Schwarzen Tempel zurückzukehren und Lord Illidan dabei zu unterstützen, die Invasoren aus Azeroth und Shattrath zurückzuschlagen.>', 23420), -- 38729
(38728, 1, 0, 0, 0, 0, 0, 0, 0, 'Nun, da sich der Schlüsselstein in unserem Besitz befindet, gibt es nur noch eins zu tun...', 23420), -- 38728
(39663, 6, 0, 0, 0, 0, 0, 0, 0, 'Könnt Ihr ihre Macht spüren? Ihr werdet jeden Kämpfer brauchen.\n\nDennoch... kümmere ich mich jetzt besser darum, wie wir hier heil wieder herauskommen.', 23420), -- 39663
(39516, 1, 274, 0, 0, 0, 0, 0, 0, 'Ich gebe mir selbst die Schuld, $n. Mannethrel war zu geschwächt. In uns tobt eine nie endende Schlacht um die Kontrolle der Dämonenkraft, die wir alle in uns aufgenommen haben.\n\nVielleicht wäre es das Beste, die Unterweisungen für den Moment zu verschieben. Wir können uns keine weiteren Verluste erlauben.', 23420), -- 39516
(40051, 273, 0, 0, 0, 0, 0, 0, 0, 'Überraschend. Ich stimme zu, eine ausgezeichnete Wahl, $n.\n\nNun müsst Ihr den Rest von uns unterweisen.', 23420), -- 40051
(38819, 273, 6, 0, 0, 0, 0, 0, 0, 'Ich stimme absolut zu. Mir scheint, Ihr habt ihre Streitkräfte vollständig ausgelöscht.\n\nWas denkt Ihr? Ist es an der Zeit, zum Kommandoschiff zu fliegen und uns den Sargeritschlüsselstein zu holen?', 23420), -- 38819
(38727, 1, 0, 0, 0, 0, 0, 0, 0, 'Wahre Anführer sieht man selten und noch seltener sieht man sie an der Front.\n\nWir sind jetzt bereit für den Sturm auf das Befehlszentrum von Brutkönigin Tyranna.', 23420), -- 38727
(40222, 6, 0, 0, 0, 0, 0, 0, 0, 'Spürt Ihr die Macht, die dem Buch entströmt? Ich kann kaum erwarten, seine Geheimnisse zu ergründen.', 23420), -- 40222
(38725, 273, 0, 0, 0, 0, 0, 0, 0, '$n. Ich habe sie gefunden.', 23420), -- 38725
(39495, 25, 0, 0, 0, 0, 0, 0, 0, 'Ausgezeichnet. Ihr habt die Macht dieses Teufelslords absorbiert und seid noch stärker geworden.\n\nJetzt können wir uns ganz auf die Vernichtung der Brutkönigin und ihrer Legionskräfte konzentrieren.', 23420), -- 39495
(39262, 5, 0, 0, 0, 0, 0, 0, 0, 'Ich wusste, dass mich mein Gefühl nicht getrogen hat.', 23420), -- 39262
(38813, 273, 0, 0, 0, 0, 0, 0, 0, 'Unser Angriff hat begonnen.\n\nSchon bald werden wir siegreich an Lord Illidans Seite im Schwarzen Tempel stehen.', 23420), -- 38813
(38766, 273, 0, 0, 0, 0, 0, 0, 0, 'Die Flut von Dämonen hat sich bereits deutlich abgeschwächt.\n\nIch spüre die Energie der Verdammniswache. Ihr seid bereits mächtiger geworden.', 23420), -- 38766
(38765, 274, 1, 0, 0, 0, 0, 0, 0, 'Grausam. Doch Dämonenjägern sind Opfer nicht fremd. Wir haben alles gegeben, um zu werden, was wir heute sind.\n\nDie Legion wird fallen. Wir erwarten Eure Befehle, $n.', 23420), -- 38765
(39050, 1, 0, 0, 0, 0, 0, 0, 0, 'Das ist neu... Sie sieht wie eine Art Spinnendämon aus. Als ob die Legion auch noch Spinnen gebraucht hätte...', 23420), -- 39050
(40379, 0, 0, 0, 0, 0, 0, 0, 0, 'Ein grausames Tagwerk, doch wir haben alle bereits so viel geopfert, um dahin zu gelangen, wo wir jetzt sind.\n\nWir werden ALLES tun, was nötig ist, um die Brennende Legion zu besiegen. Einfach Alles.', 23420), -- 40379
(39049, 273, 0, 0, 0, 0, 0, 0, 0, 'Die Essenz eines Inquisitors, ausgezeichnet. Allari hatte recht.\n\nAlso dann, beenden wir mein Ritual und sehen wir, was die Anführer der Legion im Schilde führen.', 23420), -- 39049
(38759, 0, 0, 0, 0, 0, 0, 0, 0, 'Ein Wärter hat ihre Seelen vollständig ausgesaugt?\n\nDie Streitkräfte der Brennenden Legion an diesem Ort sind weit mächtiger und brutaler als alles, was wir bisher gesehen haben.', 23420), -- 38759
(40378, 0, 0, 0, 0, 0, 0, 0, 0, 'Sie sind eben vorbeigekommen. Nicht unsere furchterregendsten Kämpfer, doch sie haben sich früher bereits als nützlich erwiesen.', 23420), -- 40378
(40077, 1, 0, 0, 0, 0, 0, 0, 0, 'Das können sie nicht ignorieren.\n\nLasst uns beginnen, den Rest unserer Truppen herbeizurufen.', 23420); -- 40077


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text     xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_request_items` WHERE `ID` IN (38728 /*38728*/, 40051 /*40051*/, 40222 /*40222*/, 39262 /*39262*/, 38759 /*38759*/);
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(38728, 6, 0, 0, 0, '$n, seid Ihr bereit?', 23420), -- 38728
(40051, 6, 0, 0, 0, 'Habt Ihr eine Entscheidung getroffen?', 23420), -- 40051
(40222, 6, 0, 0, 0, 'Habt Ihr ihn?', 23420), -- 40222
(39262, 0, 0, 0, 0, '$n, nutzt Eure Geistersicht, um die Höhle abzusuchen.\n\nWir müssen wissen, ob sich darin magisch getarnte Truppen der Legion verbergen.', 23420), -- 39262
(38759, 6, 0, 0, 0, 'Einige unserer Dämonenjäger sind verschwunden. Habt Ihr sie an der Geschmolzenen Küste finden können?', 23420); -- 38759


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=18776 AND `id`=0 ) OR (`menu_id`=18435 AND `id`=0 ) OR (`menu_id`=18823 AND `id`=0 ) OR (`menu_id`=18435 AND `id`=1 ) OR (`menu_id`=18994 AND `id`=0 ) OR (`menu_id`=18937 AND `id`=0 ) OR (`menu_id`=18935 AND `id`=0 ) OR (`menu_id`=18936 AND `id`=0 ) OR (`menu_id`=19175 AND `id`=0 ) OR (`menu_id`=18447 AND `id`=0 ) OR (`menu_id`=17260 AND `id`=0 ) OR (`menu_id`=18434 AND `id`=0 ) OR (`menu_id`=19133 AND `id`=1 ) OR (`menu_id`=19133 AND `id`=0 ) OR (`menu_id`=19016 AND `id`=1 ) OR (`menu_id`=19016 AND `id`=0 ) OR (`menu_id`=18438 AND `id`=1 ) OR (`menu_id`=18438 AND `id`=0 ) OR (`menu_id`=19015 AND `id`=0 ) OR (`menu_id`=18864 AND `id`=0 ) OR (`menu_id`=18993 AND `id`=0 );
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(18776, 0, '', '', 'Weißmond, ich nehme mir eine Eurer Teufelsfledermäuse. Wir werden zu Ende bringen, weswegen wir hier sind.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18435, 0, '', '', 'Kayn, erzählt mir, was Ihr über Mardum wisst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18823, 0, '', '', 'Mannethrel, macht Euch bereit. Ich werde Euch mit der Macht der Legionsgeheimnisse erfüllen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18435, 1, '', '', 'Kayn, ich werde Euch beibringen, was ich über die dämonischen Geheimnisse weiß.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18994, 0, '', '', 'Noch etwas?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18937, 0, '', '', 'Seid Ihr bereit, die Geheimnisse der Legion zu erfahren, Kor''vas?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18935, 0, '', '', 'Allari, folgende Geheimnisse habe ich aufgedeckt.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18936, 0, '', '', 'Hört gut zu, Cyana. Ich sage Euch, was ich aus dem Folianten der Dämonengeheimnisse gelernt habe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19175, 0, '', '', 'Ja, Jace, ich werde die Höhle mit meiner Geistersicht untersuchen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18447, 0, '', '', 'Milady, zieht mit Euren Naga in Richtung Norden. Unterbrecht das Ritual, das die Dämonen an der Seelenmaschine abhalten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17260, 0, '', '', 'Schlachtenfürst, Eure Truppen werden die Verdammnisfestung im Südosten angreifen. Sorgt dafür, dass ihr riesiges Dämonenheer uns nicht in den Rücken fällt.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18434, 0, '', '', 'Matronenmutter, führt Eure Shivan über die Schlucht zur Verderbnisschmiede. Lasst unsere Feinde leiden.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19133, 1, '', '', 'Wartet einen Moment, Sevis. Ich überlege es mir noch einmal.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19133, 0, '', '', 'Ich bin mir sicher, Sevis. Ich muss Euch opfern, um das Tor zu aktivieren.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19016, 1, '', '', 'Ich möchte, dass Ihr mich opfert, um das Tor mit Macht zu erfüllen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19016, 0, '', '', 'Sevis, ich muss Euch opfern, um das letzte Tor zu aktivieren.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18438, 1, '', '', 'Was haltet Ihr von den Shivarra?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18438, 0, '', '', 'Jace, was ist dieser Nethertiegel?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19015, 0, '', '', 'Mystiker, danke für Euer Opfer.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18864, 0, '', '', 'Kayn, erzählt mir, was Ihr über Mardum wisst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18993, 0, '', '', 'Noch etwas?', '', '', '', '', '', '', '', '', '', '', '', '', '');


DELETE FROM `locales_creature_text` WHERE (`entry`=98497 AND `groupid`=0) OR  (`entry`=96877 AND `groupid`=0) OR  (`entry`=97303 AND `groupid`=0) OR  (`entry`=99045 AND `groupid`=0) OR  (`entry`=97459 AND `groupid`=0) OR  (`entry`=98484 AND `groupid`=0) OR  (`entry`=100549 AND `groupid`=0) OR  (`entry`=94654 AND `groupid`=0) OR  (`entry`=98486 AND `groupid`=0) OR  (`entry`=97059 AND `groupid`=0) OR  (`entry`=96654 AND `groupid`=0) OR  (`entry`=100543 AND `groupid`=0) OR  (`entry`=95226 AND `groupid`=0) OR  (`entry`=97058 AND `groupid`=0) OR  (`entry`=99919 AND `groupid`=0) OR  (`entry`=99916 AND `groupid`=0) OR  (`entry`=96436 AND `groupid`=0) OR  (`entry`=90247 AND `groupid`=0) OR  (`entry`=93112 AND `groupid`=0) OR  (`entry`=96655 AND `groupid`=0) OR  (`entry`=96279 AND `groupid`=0) OR  (`entry`=93230 AND `groupid`=0) OR  (`entry`=98459 AND `groupid`=0) OR  (`entry`=96653 AND `groupid`=0) OR  (`entry`=93221 AND `groupid`=0) OR  (`entry`=96499 AND `groupid`=0) OR  (`entry`=99917 AND `groupid`=0) OR  (`entry`=93802 AND `groupid`=0) OR  (`entry`=93127 AND `groupid`=0) OR  (`entry`=97244 AND `groupid`=0) OR  (`entry`=94400 AND `groupid`=0) OR  (`entry`=97676 AND `groupid`=0) OR  (`entry`=96884 AND `groupid`=0) OR  (`entry`=100982 AND `groupid`=0) OR  (`entry`=93105 AND `groupid`=0) OR  (`entry`=98986 AND `groupid`=0) OR  (`entry`=103432 AND `groupid`=0) OR  (`entry`=99915 AND `groupid`=0) OR  (`entry`=94435 AND `groupid`=0) OR  (`entry`=93716 AND `groupid`=0) OR  (`entry`=94410 AND `groupid`=0) OR  (`entry`=102714 AND `groupid`=0) OR  (`entry`=95046 AND `groupid`=0) OR  (`entry`=96494 AND `groupid`=0) OR  (`entry`=96888 AND `groupid`=0) OR  (`entry`=96652 AND `groupid`=0) OR  (`entry`=94377 AND `groupid`=0) OR  (`entry`=96420 AND `groupid`=0) OR  (`entry`=96441 AND `groupid`=0) OR  (`entry`=95048 AND `groupid`=0) OR  (`entry`=98229 AND `groupid`=0) OR  (`entry`=93759 AND `groupid`=0) OR  (`entry`=93693 AND `groupid`=0) OR  (`entry`=96277 AND `groupid`=0) OR  (`entry`=96280 AND `groupid`=0) OR  (`entry`=98711 AND `groupid`=0) OR  (`entry`=102724 AND `groupid`=0) OR  (`entry`=93117 AND `groupid`=0);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(98497, 0, 0, '', '', 'Ich werde Euch persönlich Tyranna übergeben.', '', '', '', '', '' ),
(96877, 0, 0, '', '', 'Ich bin hier, um Euch zu helfen.', '', '', '', '', '' ),
(97303, 0, 0, '', '', 'Schnell, $n. Aktiviert das Portal mit dem Schlüsselstein. Wir müssen zum Schwarzen Tempel zurück.', '', '', '', '', '' ),
(99045, 0, 0, '', '', 'Wie Ihr Euch auch entscheidet, es tut gut, die Macht der Legion gegen sie einzusetzen.', '', '', '', '', '' ),
(97459, 0, 0, '', '', '$n, welch eine Blamage...', '', '', '', '', '' ),
(98484, 0, 0, '', '', 'Erbärmlich.', '', '', '', '', '' ),
(100549, 0, 0, '', '', 'Ich bin nicht den ganzen Weg gekommen, um auf diesem Fels zu sterben.', '', '', '', '', '' ),
(94654, 0, 0, '', '', 'Die Legion erobert alles.', '', '', '', '', '' ),
(98486, 0, 0, '', '', 'Wir werden das Universum im Feuer läutern.', '', '', '', '', '' ),
(97059, 0, 0, '', '', 'Sucht Ihr Euren zerschlagenen Draenei?', '', '', '', '', '' ),
(96654, 0, 0, '', '', '$n, Ihr habt es geschafft!', '', '', '', '', '' ),
(100543, 0, 0, '', '', 'Wir werden uns den Schlüsselstein holen!', '', '', '', '', '' ),
(95226, 0, 0, '', '', 'Eure Seele gehört mir.', '', '', '', '', '' ),
(97058, 0, 0, '', '', 'Mein Schwarm wird sich an Eurer Seele laben!', '', '', '', '', '' ),
(99919, 0, 0, '', '', 'Ihr und ich gemeinsam, bis zum letzten Atemzug, $n.', '', '', '', '', '' ),
(99916, 0, 0, '', '', 'Hier wurden viele Seelen geopfert. Der Mechanismus funktioniert.', '', '', '', '', '' ),
(96436, 0, 0, '', '', '$n, würdet Ihr Eure Geistersicht am Höhleneingang benutzen? Irgendetwas stimmt da nicht.', '', '', '', '', '' ),
(90247, 0, 0, '', '', 'Ganz wie Ihr wünscht.', '', '', '', '', '' ),
(93112, 0, 0, '', '', 'Eindringlinge. Warnt die Brutmutter!', '', '', '', '', '' ),
(96655, 0, 0, '', '', 'Auf ihrem Weg über die Brücke hat Kor''vas eine Schneise der Verwüstung hinterlassen.', '', '', '', '', '' ),
(96279, 0, 0, '', '', 'Schuldig im Sinne der Anklage.', '', '', '', '', '' ),
(93230, 0, 0, '', '', 'Zu schwach... kann sie kaum... kontrollieren.', '', '', '', '', '' ),
(98459, 0, 0, '', '', 'Ich habe ein paar Vorschläge, wie wir vorgehen sollten.', '', '', '', '', '' ),
(96653, 0, 0, '', '', 'Euch allen viel Glück. Ich bringe den Rest von uns dort hoch, $n.', '', '', '', '', '' ),
(93221, 0, 0, '', '', 'Sie werden sterben.', '', '', '', '', '' ),
(96499, 0, 0, '', '', 'Der Teufelslord ist dort vorne.', '', '', '', '', '' ),
(99917, 0, 0, '', '', 'Beeilung, $n! Ihr habt nicht viel Zeit.', '', '', '', '', '' ),
(93802, 0, 0, '', '', 'Niemand widersteht meinem Kuss.', '', '', '', '', '' ),
(93127, 0, 0, '', '', 'Die Spinnenfestung, die Seelenmaschine und die Schmiede sind ihre Primärziele. Die Diener Illidans müssen siegreich sein.', '', '', '', '', '' ),
(97244, 0, 0, '', '', 'Nehmt den Schlüsselstein! Wir müssen über das Portal unten zum Schwarzen Tempel zurück.', '', '', '', '', '' ),
(94400, 0, 0, '', '', 'Ich werde Euch alle vernichten.', '', '', '', '', '' ),
(97676, 0, 0, '', '', 'Vorsicht, $n! In der Seelenmaschine befindet sich ein Schreckenslord.', '', '', '', '', '' ),
(96884, 0, 0, '', '', 'Die Echsennarbe steht zu ihrem Bündnis.', '', '', '', '', '' ),
(100982, 0, 0, '', '', 'Ich treffe Euch am Tor an der Geschmolzenen Küste.', '', '', '', '', '' ),
(93105, 0, 0, '', '', 'Ich sehe Eure Geheimnisse...', '', '', '', '', '' ),
(98986, 0, 0, '', '', 'Verschwindet. Der Foliant gehört mir!', '', '', '', '', '' ),
(103432, 0, 0, '', '', 'Für Brutkönigin Tyranna. Für die Legion!', '', '', '', '', '' ),
(99915, 0, 0, '', '', '$n, wir haben ein großes Problem.', '', '', '', '', '' ),
(94435, 0, 0, '', '', 'Wir kümmern uns darum. Vergesst nicht, dass der Schwarze Tempel angegriffen wird.', '', '', '', '', '' ),
(93716, 0, 0, '', '', 'Knusprige Knochen, saftiges Mark.', '', '', '', '', '' ),
(94410, 0, 0, '', '', '$n, hier herüber!', '', '', '', '', '' ),
(102714, 0, 0, '', '', 'Das Ritual wurde unterbrochen.', '', '', '', '', '' ),
(95046, 0, 0, '', '', 'Der Wille der Legion geschehe.', '', '', '', '', '' ),
(96494, 0, 0, '', '', 'Eure Teufelsmacht wird Euch nicht retten.', '', '', '', '', '' ),
(96888, 0, 0, '', '', 'Wir helfen Euch, die Legion zu vernichten.', '', '', '', '', '' ),
(96652, 0, 0, '', '', '$n, wartet... Ich kann die Macht nicht kontrollieren.', '', '', '', '', '' ),
(94377, 0, 0, '', '', 'Ich... lasse das... nicht zu!', '', '', '', '', '' ),
(96420, 0, 0, '', '', 'Ihre Zahl ist uns weit, weit überlegen. Können wir sie überhaupt besiegen?', '', '', '', '', '' ),
(96441, 0, 0, '', '', 'Meine Axt verfehlt nie ihr Ziel!', '', '', '', '', '' ),
(95048, 0, 0, '', '', 'Zerquetscht diese Insekten, Beliash.', '', '', '', '', '' ),
(98229, 0, 0, '', '', 'Aktiviert alle drei Portale und ruft den Rest unserer Streitmacht herbei.', '', '', '', '', '' ),
(93759, 0, 0, '', '', 'Ich spüre große Macht in Euch, $n. Habt Ihr die Essenz eines Dämonen gestohlen?', '', '', '', '', '' ),
(93693, 0, 0, '', '', 'Eine würdige Aufgabe für die Echsennarbe. Abgemacht.', '', '', '', '', '' ),
(96277, 0, 0, '', '', 'Dämonenjäger? Wie seid Ihr hierhergekommen?', '', '', '', '', '' ),
(96280, 0, 0, '', '', 'Fleisch! Mein Leibgericht!', '', '', '', '', '' ),
(98711, 0, 0, '', '', 'Die Wichtelmutter ist in der Höhle dort.', '', '', '', '', '' ),
(102724, 0, 0, '', '', 'Eure Welt wird gereinigt werden!', '', '', '', '', '' ),
(93117, 0, 0, '', '', 'Diese Schmerzen!', '', '', '', '', '' );


DELETE FROM `creature_template_locale` WHERE (`entry`=97069 /*97069*/ AND `locale`='deDE') OR (`entry`=96847 /*96847*/ AND `locale`='deDE') OR (`entry`=101648 /*101648*/ AND `locale`='deDE') OR (`entry`=92782 /*92782*/ AND `locale`='deDE') OR (`entry`=100168 /*100168*/ AND `locale`='deDE') OR (`entry`=103658 /*103658*/ AND `locale`='deDE') OR (`entry`=103655 /*103655*/ AND `locale`='deDE') OR (`entry`=92718 /*92718*/ AND `locale`='deDE') OR (`entry`=98714 /*98714*/ AND `locale`='deDE') OR (`entry`=98713 /*98713*/ AND `locale`='deDE') OR (`entry`=97965 /*97965*/ AND `locale`='deDE') OR (`entry`=97964 /*97964*/ AND `locale`='deDE') OR (`entry`=97303 /*97303*/ AND `locale`='deDE') OR (`entry`=100333 /*100333*/ AND `locale`='deDE') OR (`entry`=100334 /*100334*/ AND `locale`='deDE') OR (`entry`=98712 /*98712*/ AND `locale`='deDE') OR (`entry`=97244 /*97244*/ AND `locale`='deDE') OR (`entry`=97297 /*97297*/ AND `locale`='deDE') OR (`entry`=100243 /*100243*/ AND `locale`='deDE') OR (`entry`=100244 /*100244*/ AND `locale`='deDE') OR (`entry`=101760 /*101760*/ AND `locale`='deDE') OR (`entry`=97959 /*97959*/ AND `locale`='deDE') OR (`entry`=99045 /*99045*/ AND `locale`='deDE') OR (`entry`=101317 /*101317*/ AND `locale`='deDE') OR (`entry`=100548 /*100548*/ AND `locale`='deDE') OR (`entry`=100545 /*100545*/ AND `locale`='deDE') OR (`entry`=101597 /*101597*/ AND `locale`='deDE') OR (`entry`=97704 /*97704*/ AND `locale`='deDE') OR (`entry`=97769 /*97769*/ AND `locale`='deDE') OR (`entry`=97058 /*97058*/ AND `locale`='deDE') OR (`entry`=33765 /*33765*/ AND `locale`='deDE') OR (`entry`=97771 /*97771*/ AND `locale`='deDE') OR (`entry`=97772 /*97772*/ AND `locale`='deDE') OR (`entry`=97770 /*97770*/ AND `locale`='deDE') OR (`entry`=97881 /*97881*/ AND `locale`='deDE') OR (`entry`=96767 /*96767*/ AND `locale`='deDE') OR (`entry`=96279 /*96279*/ AND `locale`='deDE') OR (`entry`=93280 /*93280*/ AND `locale`='deDE') OR (`entry`=96561 /*96561*/ AND `locale`='deDE') OR (`entry`=99419 /*99419*/ AND `locale`='deDE') OR (`entry`=97370 /*97370*/ AND `locale`='deDE') OR (`entry`=103432 /*103432*/ AND `locale`='deDE') OR (`entry`=100257 /*100257*/ AND `locale`='deDE') OR (`entry`=98986 /*98986*/ AND `locale`='deDE') OR (`entry`=99423 /*99423*/ AND `locale`='deDE') OR (`entry`=98711 /*98711*/ AND `locale`='deDE') OR (`entry`=103429 /*103429*/ AND `locale`='deDE') OR (`entry`=99735 /*99735*/ AND `locale`='deDE') OR (`entry`=96280 /*96280*/ AND `locale`='deDE') OR (`entry`=98158 /*98158*/ AND `locale`='deDE') OR (`entry`=97607 /*97607*/ AND `locale`='deDE') OR (`entry`=97606 /*97606*/ AND `locale`='deDE') OR (`entry`=96563 /*96563*/ AND `locale`='deDE') OR (`entry`=97962 /*97962*/ AND `locale`='deDE') OR (`entry`=101318 /*101318*/ AND `locale`='deDE') OR (`entry`=93764 /*93764*/ AND `locale`='deDE') OR (`entry`=98165 /*98165*/ AND `locale`='deDE') OR (`entry`=101320 /*101320*/ AND `locale`='deDE') OR (`entry`=108409 /*108409*/ AND `locale`='deDE') OR (`entry`=108408 /*108408*/ AND `locale`='deDE') OR (`entry`=98160 /*98160*/ AND `locale`='deDE') OR (`entry`=101321 /*101321*/ AND `locale`='deDE') OR (`entry`=96402 /*96402*/ AND `locale`='deDE') OR (`entry`=96564 /*96564*/ AND `locale`='deDE') OR (`entry`=96277 /*96277*/ AND `locale`='deDE') OR (`entry`=100703 /*100703*/ AND `locale`='deDE') OR (`entry`=100717 /*100717*/ AND `locale`='deDE') OR (`entry`=97459 /*97459*/ AND `locale`='deDE') OR (`entry`=100543 /*100543*/ AND `locale`='deDE') OR (`entry`=97057 /*97057*/ AND `locale`='deDE') OR (`entry`=97382 /*97382*/ AND `locale`='deDE') OR (`entry`=100549 /*100549*/ AND `locale`='deDE') OR (`entry`=99787 /*99787*/ AND `locale`='deDE') OR (`entry`=101947 /*101947*/ AND `locale`='deDE') OR (`entry`=97059 /*97059*/ AND `locale`='deDE') OR (`entry`=96562 /*96562*/ AND `locale`='deDE') OR (`entry`=100504 /*100504*/ AND `locale`='deDE') OR (`entry`=96768 /*96768*/ AND `locale`='deDE') OR (`entry`=101781 /*101781*/ AND `locale`='deDE') OR (`entry`=97676 /*97676*/ AND `locale`='deDE') OR (`entry`=65183 /*65183*/ AND `locale`='deDE') OR (`entry`=97601 /*97601*/ AND `locale`='deDE') OR (`entry`=96499 /*96499*/ AND `locale`='deDE') OR (`entry`=96473 /*96473*/ AND `locale`='deDE') OR (`entry`=100061 /*100061*/ AND `locale`='deDE') OR (`entry`=97597 /*97597*/ AND `locale`='deDE') OR (`entry`=102726 /*102726*/ AND `locale`='deDE') OR (`entry`=96494 /*96494*/ AND `locale`='deDE') OR (`entry`=96441 /*96441*/ AND `locale`='deDE') OR (`entry`=97598 /*97598*/ AND `locale`='deDE') OR (`entry`=96493 /*96493*/ AND `locale`='deDE') OR (`entry`=97706 /*97706*/ AND `locale`='deDE') OR (`entry`=101397 /*101397*/ AND `locale`='deDE') OR (`entry`=101790 /*101790*/ AND `locale`='deDE') OR (`entry`=96503 /*96503*/ AND `locale`='deDE') OR (`entry`=101789 /*101789*/ AND `locale`='deDE') OR (`entry`=101788 /*101788*/ AND `locale`='deDE') OR (`entry`=101787 /*101787*/ AND `locale`='deDE') OR (`entry`=96504 /*96504*/ AND `locale`='deDE') OR (`entry`=96501 /*96501*/ AND `locale`='deDE') OR (`entry`=96500 /*96500*/ AND `locale`='deDE') OR (`entry`=96436 /*96436*/ AND `locale`='deDE') OR (`entry`=98157 /*98157*/ AND `locale`='deDE') OR (`entry`=99759 /*99759*/ AND `locale`='deDE') OR (`entry`=97971 /*97971*/ AND `locale`='deDE') OR (`entry`=97034 /*97034*/ AND `locale`='deDE') OR (`entry`=96502 /*96502*/ AND `locale`='deDE') OR (`entry`=102907 /*102907*/ AND `locale`='deDE') OR (`entry`=102906 /*102906*/ AND `locale`='deDE') OR (`entry`=102905 /*102905*/ AND `locale`='deDE') OR (`entry`=96653 /*96653*/ AND `locale`='deDE') OR (`entry`=95450 /*95450*/ AND `locale`='deDE') OR (`entry`=95449 /*95449*/ AND `locale`='deDE') OR (`entry`=95447 /*95447*/ AND `locale`='deDE') OR (`entry`=90247 /*90247*/ AND `locale`='deDE') OR (`entry`=96650 /*96650*/ AND `locale`='deDE') OR (`entry`=96732 /*96732*/ AND `locale`='deDE') OR (`entry`=94435 /*94435*/ AND `locale`='deDE') OR (`entry`=96252 /*96252*/ AND `locale`='deDE') OR (`entry`=93707 /*93707*/ AND `locale`='deDE') OR (`entry`=96253 /*96253*/ AND `locale`='deDE') OR (`entry`=96655 /*96655*/ AND `locale`='deDE') OR (`entry`=96420 /*96420*/ AND `locale`='deDE') OR (`entry`=105945 /*105945*/ AND `locale`='deDE') OR (`entry`=97604 /*97604*/ AND `locale`='deDE') OR (`entry`=97600 /*97600*/ AND `locale`='deDE') OR (`entry`=97599 /*97599*/ AND `locale`='deDE') OR (`entry`=102910 /*102910*/ AND `locale`='deDE') OR (`entry`=96652 /*96652*/ AND `locale`='deDE') OR (`entry`=113927 /*113927*/ AND `locale`='deDE') OR (`entry`=113924 /*113924*/ AND `locale`='deDE') OR (`entry`=102908 /*102908*/ AND `locale`='deDE') OR (`entry`=96278 /*96278*/ AND `locale`='deDE') OR (`entry`=93693 /*93693*/ AND `locale`='deDE') OR (`entry`=102724 /*102724*/ AND `locale`='deDE') OR (`entry`=97602 /*97602*/ AND `locale`='deDE') OR (`entry`=97014 /*97014*/ AND `locale`='deDE') OR (`entry`=96231 /*96231*/ AND `locale`='deDE') OR (`entry`=96230 /*96230*/ AND `locale`='deDE') OR (`entry`=96228 /*96228*/ AND `locale`='deDE') OR (`entry`=96930 /*96930*/ AND `locale`='deDE') OR (`entry`=96654 /*96654*/ AND `locale`='deDE') OR (`entry`=96276 /*96276*/ AND `locale`='deDE') OR (`entry`=96931 /*96931*/ AND `locale`='deDE') OR (`entry`=99352 /*99352*/ AND `locale`='deDE') OR (`entry`=97603 /*97603*/ AND `locale`='deDE') OR (`entry`=93716 /*93716*/ AND `locale`='deDE') OR (`entry`=93802 /*93802*/ AND `locale`='deDE') OR (`entry`=99915 /*99915*/ AND `locale`='deDE') OR (`entry`=98611 /*98611*/ AND `locale`='deDE') OR (`entry`=97634 /*97634*/ AND `locale`='deDE') OR (`entry`=97629 /*97629*/ AND `locale`='deDE') OR (`entry`=97624 /*97624*/ AND `locale`='deDE') OR (`entry`=95048 /*95048*/ AND `locale`='deDE') OR (`entry`=102714 /*102714*/ AND `locale`='deDE') OR (`entry`=95046 /*95046*/ AND `locale`='deDE') OR (`entry`=94654 /*94654*/ AND `locale`='deDE') OR (`entry`=23837 /*23837*/ AND `locale`='deDE') OR (`entry`=93759 /*93759*/ AND `locale`='deDE') OR (`entry`=95049 /*95049*/ AND `locale`='deDE') OR (`entry`=93117 /*93117*/ AND `locale`='deDE') OR (`entry`=93230 /*93230*/ AND `locale`='deDE') OR (`entry`=98907 /*98907*/ AND `locale`='deDE') OR (`entry`=98905 /*98905*/ AND `locale`='deDE') OR (`entry`=94651 /*94651*/ AND `locale`='deDE') OR (`entry`=99917 /*99917*/ AND `locale`='deDE') OR (`entry`=99914 /*99914*/ AND `locale`='deDE') OR (`entry`=93105 /*93105*/ AND `locale`='deDE') OR (`entry`=94400 /*94400*/ AND `locale`='deDE') OR (`entry`=96400 /*96400*/ AND `locale`='deDE') OR (`entry`=96731 /*96731*/ AND `locale`='deDE') OR (`entry`=99351 /*99351*/ AND `locale`='deDE') OR (`entry`=94377 /*94377*/ AND `locale`='deDE') OR (`entry`=94705 /*94705*/ AND `locale`='deDE') OR (`entry`=94704 /*94704*/ AND `locale`='deDE') OR (`entry`=101288 /*101288*/ AND `locale`='deDE') OR (`entry`=95226 /*95226*/ AND `locale`='deDE') OR (`entry`=96159 /*96159*/ AND `locale`='deDE') OR (`entry`=100982 /*100982*/ AND `locale`='deDE') OR (`entry`=105316 /*105316*/ AND `locale`='deDE') OR (`entry`=24021 /*24021*/ AND `locale`='deDE') OR (`entry`=93112 /*93112*/ AND `locale`='deDE') OR (`entry`=97592 /*97592*/ AND `locale`='deDE') OR (`entry`=93127 /*93127*/ AND `locale`='deDE') OR (`entry`=97142 /*97142*/ AND `locale`='deDE') OR (`entry`=93221 /*93221*/ AND `locale`='deDE') OR (`entry`=93115 /*93115*/ AND `locale`='deDE') OR (`entry`=94410 /*94410*/ AND `locale`='deDE') OR (`entry`=101518 /*101518*/ AND `locale`='deDE') OR (`entry`=100510 /*100510*/ AND `locale`='deDE') OR (`entry`=37490 /*37490*/ AND `locale`='deDE') OR (`entry`=99916 /*99916*/ AND `locale`='deDE') OR (`entry`=101704 /*101704*/ AND `locale`='deDE') OR (`entry`=98229 /*98229*/ AND `locale`='deDE') OR (`entry`=98354 /*98354*/ AND `locale`='deDE') OR (`entry`=98456 /*98456*/ AND `locale`='deDE') OR (`entry`=98457 /*98457*/ AND `locale`='deDE') OR (`entry`=93762 /*93762*/ AND `locale`='deDE') OR (`entry`=98460 /*98460*/ AND `locale`='deDE') OR (`entry`=98458 /*98458*/ AND `locale`='deDE') OR (`entry`=98459 /*98459*/ AND `locale`='deDE') OR (`entry`=98482 /*98482*/ AND `locale`='deDE') OR (`entry`=99919 /*99919*/ AND `locale`='deDE') OR (`entry`=99656 /*99656*/ AND `locale`='deDE') OR (`entry`=98497 /*98497*/ AND `locale`='deDE') OR (`entry`=98483 /*98483*/ AND `locale`='deDE') OR (`entry`=98486 /*98486*/ AND `locale`='deDE') OR (`entry`=98484 /*98484*/ AND `locale`='deDE') OR (`entry`=101748 /*101748*/ AND `locale`='deDE') OR (`entry`=98622 /*98622*/ AND `locale`='deDE') OR (`entry`=98621 /*98621*/ AND `locale`='deDE') OR (`entry`=98618 /*98618*/ AND `locale`='deDE') OR (`entry`=99918 /*99918*/ AND `locale`='deDE') OR (`entry`=98292 /*98292*/ AND `locale`='deDE') OR (`entry`=98290 /*98290*/ AND `locale`='deDE') OR (`entry`=98228 /*98228*/ AND `locale`='deDE') OR (`entry`=98227 /*98227*/ AND `locale`='deDE') OR (`entry`=93011 /*93011*/ AND `locale`='deDE') OR (`entry`=97712 /*97712*/ AND `locale`='deDE') OR (`entry`=97594 /*97594*/ AND `locale`='deDE') OR (`entry`=99218 /*99218*/ AND `locale`='deDE') OR (`entry`=100161 /*100161*/ AND `locale`='deDE') OR (`entry`=94492 /*94492*/ AND `locale`='deDE') OR (`entry`=98191 /*98191*/ AND `locale`='deDE') OR (`entry`=94744 /*94744*/ AND `locale`='deDE') OR (`entry`=94655 /*94655*/ AND `locale`='deDE') OR (`entry`=99650 /*99650*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(97069, 'deDE', 'Zornfürst Lekos', '', NULL, NULL, 23420), -- 97069
(96847, 'deDE', 'Drelanim Wisperwind', '', NULL, NULL, 23420), -- 96847
(101648, 'deDE', 'Verlieskakerlake', '', NULL, NULL, 23420), -- 101648
(92782, 'deDE', 'Wilder Teufelspirscher', '', NULL, NULL, 23420), -- 92782
(100168, 'deDE', 'Der Flügel der Jäger', '', NULL, NULL, 23420), -- 100168
(103658, 'deDE', 'Kayns Zelle', '', NULL, 'questinteract', 23420), -- 103658
(103655, 'deDE', 'Altruis'' Zelle', '', NULL, 'questinteract', 23420), -- 103655
(92718, 'deDE', 'Maiev Schattensang', '', NULL, NULL, 23420), -- 92718
(98714, 'deDE', 'Cyana Nachtgleve', '', 'Illidari', NULL, 23420), -- 98714
(98713, 'deDE', 'Kor''vas Blutdorn', '', 'Illidari', NULL, 23420), -- 98713
(97965, 'deDE', 'Allari die Seelenfresserin', '', 'Illidari', NULL, 23420), -- 97965
(97964, 'deDE', 'Jace Düsterweber', '', 'Illidari', NULL, 23420), -- 97964
(97303, 'deDE', 'Kayn Sonnenzorn', '', 'Illidari', NULL, 23420), -- 97303
(100333, 'deDE', 'Huschender Brutling', '', NULL, NULL, 23420), -- 100333
(100334, 'deDE', 'Tyrannas Brut', '', NULL, NULL, 23420), -- 100334
(98712, 'deDE', 'Kor''vas Blutdorn', '', 'Illidari', 'questinteract', 23420), -- 98712
(97244, 'deDE', 'Kayn Sonnenzorn', '', 'Illidari', 'questinteract', 23420), -- 97244
(97297, 'deDE', 'Cyana Nachtgleve', '', 'Illidari', NULL, 23420), -- 97297
(100243, 'deDE', 'Tückischer Kriecher', '', NULL, NULL, 23420), -- 100243
(100244, 'deDE', 'Jungfer der Aranasi', '', NULL, NULL, 23420), -- 100244
(97959, 'deDE', 'Jace Düsterweber', '', 'Illidari', 'questinteract', 23420), -- 97959
(99045, 'deDE', 'Kor''vas Blutdorn', '', 'Illidari', NULL, 23420), -- 99045
(101317, 'deDE', 'Illysanna Rabenkrone', '', 'Illidari', NULL, 23420), -- 101317
(100548, 'deDE', 'Dämonenjägerin', '', 'Illidari', 'questinteract', 23420), -- 100548
(100545, 'deDE', 'Dämonenjäger', '', 'Illidari', 'questinteract', 23420), -- 100545
(101597, 'deDE', 'Tirathon Saltheril', '', 'Illidari', NULL, 23420), -- 101597
(97704, 'deDE', 'Brunnen der Seelen', '', NULL, NULL, 23420), -- 97704
(97769, 'deDE', 'Henker von Mardum', '', NULL, NULL, 23420), -- 97769
(97058, 'deDE', 'Graf Perfidas', '', NULL, NULL, 23420), -- 97058
(33765, 'deDE', 'ELM General Purpose Bunny (scale x2)', '', NULL, NULL, 23420), -- 33765
(97771, 'deDE', 'Myrmidone der Echsennarbe', '', 'Illidans Diener', NULL, 23420), -- 97771
(97772, 'deDE', 'Meeresruferin der Echsennarbe', '', 'Illidans Dienerin', NULL, 23420), -- 97772
(97770, 'deDE', 'Harpunenkämpfer der Echsennarbe', '', 'Illidans Diener', NULL, 23420), -- 97770
(96767, 'deDE', 'Naga', '', NULL, NULL, 23420), -- 96767
(96279, 'deDE', 'Henker von Mardum', '', NULL, NULL, 23420), -- 96279
(93280, 'deDE', 'Gefangene Seele', '', NULL, NULL, 23420), -- 93280
(96561, 'deDE', 'Harpunenkämpfer der Echsennarbe', '', 'Illidans Diener', NULL, 23420), -- 96561
(99419, 'deDE', 'Baric Sturmläufer', '', 'Illidari', NULL, 23420), -- 99419
(97370, 'deDE', 'General Volroth', '', NULL, NULL, 23420), -- 97370
(103432, 'deDE', 'Zenturio der Königin', '', NULL, NULL, 23420), -- 103432
(100257, 'deDE', 'Teufelsgeysir', '', NULL, NULL, 23420), -- 100257
(98986, 'deDE', 'Fecundia', '', NULL, NULL, 23420), -- 98986
(99423, 'deDE', 'Zaria Schattenherz', '', 'Illidari', NULL, 23420), -- 99423
(98711, 'deDE', 'Kor''vas Blutdorn', '', 'Illidari', NULL, 23420), -- 98711
(99735, 'deDE', 'Gehörnter Diener', '', NULL, NULL, 23420), -- 99735
(96280, 'deDE', 'Instabiler Diener', '', NULL, NULL, 23420), -- 96280
(98158, 'deDE', 'Asha Rabenruf', '', 'Illidari', NULL, 23420), -- 98158
(97607, 'deDE', 'Priesterin des Deliriums', '', 'Illidans Dienerin', NULL, 23420), -- 97607
(97606, 'deDE', 'Priesterin der Lust', '', 'Illidans Dienerin', NULL, 23420), -- 97606
(96563, 'deDE', 'Priesterin der Lust', '', 'Illidans Dienerin', NULL, 23420), -- 96563
(97962, 'deDE', 'Allari die Seelenfresserin', '', 'Illidari', 'questinteract', 23420), -- 97962
(101318, 'deDE', 'Pfeiler des Kummers', '', NULL, NULL, 23420), -- 101318
(93764, 'deDE', 'Shivan', '', NULL, NULL, 23420), -- 93764
(98165, 'deDE', 'Cassiel Nachtdorn', '', 'Illidari', NULL, 23420), -- 98165
(101320, 'deDE', 'Pfeiler des Kummers', '', NULL, NULL, 23420), -- 101320
(108409, 'deDE', 'Champion des Zorns', '', NULL, NULL, 23420), -- 108409
(108408, 'deDE', 'Ariana Feuerherz', '', 'Illidari', NULL, 23420), -- 108408
(98160, 'deDE', 'Sirius Schwarzschwinge', '', 'Illidari', NULL, 23420), -- 98160
(101321, 'deDE', 'Pfeiler des Kummers', '', NULL, NULL, 23420), -- 101321
(96402, 'deDE', 'Bullige Schmiedenbestie', '', NULL, NULL, 23420), -- 96402
(96564, 'deDE', 'Priesterin des Deliriums', '', 'Illidans Dienerin', NULL, 23420), -- 96564
(96277, 'deDE', 'Zenturio der Königin', '', NULL, NULL, 23420), -- 96277
(100703, 'deDE', 'Zehrende Spinne', '', NULL, NULL, 23420), -- 100703
(100717, 'deDE', 'Spinnenei', '', NULL, NULL, 23420), -- 100717
(97459, 'deDE', 'Schlachtenfürst Gaardoun', '', 'Hauptmann der Aschenzungen', 'questinteract', 23420), -- 97459
(100543, 'deDE', 'Dämonenjägerin', '', 'Illidari', 'questinteract', 23420), -- 100543
(97057, 'deDE', 'Aufseher Brutarg', '', NULL, NULL, 23420), -- 97057
(97382, 'deDE', 'Seelenernter', '', NULL, 'questinteract', 23420), -- 97382
(100549, 'deDE', 'Dämonenjäger', '', 'Illidari', 'questinteract', 23420), -- 100549
(99787, 'deDE', 'Marius Teufelsbann', '', 'Illidari', NULL, 23420), -- 99787
(101947, 'deDE', 'Stabilisator der Verdammnisfestung', '', NULL, 'questinteract', 23420), -- 101947
(97059, 'deDE', 'König Voras', '', 'Brutkönigin Tyrannas Gefährte', NULL, 23420), -- 97059
(96562, 'deDE', 'Pirscher der Aschenzungen', '', 'Illidans Diener', NULL, 23420), -- 96562
(100504, 'deDE', 'Jungfer der Aranasi', '', NULL, NULL, 23420), -- 100504
(96768, 'deDE', 'Zerschlagener', '', NULL, NULL, 23420), -- 96768
(97676, 'deDE', 'Lady S''theno', '', 'Hauptmann der Echsennarbe', NULL, 23420), -- 97676
(65183, 'deDE', 'Geistheiler', '', NULL, NULL, 23420), -- 65183
(97601, 'deDE', 'Zerstörerin der Shivarra', '', 'Illidans Dienerin', NULL, 23420), -- 97601
(96499, 'deDE', 'Jace Düsterweber', '', 'Illidari', NULL, 23420), -- 96499
(96473, 'deDE', 'Zauberer der Eredar', '', NULL, NULL, 23420), -- 96473
(100061, 'deDE', 'Axe Throw Stalker', '', NULL, NULL, 23420), -- 100061
(97597, 'deDE', 'Zauberer der Eredar', '', NULL, NULL, 23420), -- 97597
(102726, 'deDE', 'Zauberin der Eredar', '', NULL, NULL, 23420), -- 102726
(96494, 'deDE', 'Schlächter der Teufelswache', '', NULL, NULL, 23420), -- 96494
(96441, 'deDE', 'Teufelslord Caza', '', NULL, NULL, 23420), -- 96441
(97598, 'deDE', 'Schlächter der Teufelswache', '', NULL, NULL, 23420), -- 97598
(96493, 'deDE', 'Schlächter der Teufelswache', '', NULL, NULL, 23420), -- 96493
(97706, 'deDE', 'Teufelsweber', '', NULL, NULL, 23420), -- 97706
(101397, 'deDE', 'Cailyn Bleichbann', '', 'Illidari', NULL, 23420), -- 101397
(101790, 'deDE', 'Dämonenjäger', '', 'Illidari', NULL, 23420), -- 101790
(96503, 'deDE', 'Meeresruferin der Echsennarbe', '', 'Illidans Dienerin', NULL, 23420), -- 96503
(101789, 'deDE', 'Dämonenjägerin', '', 'Illidari', NULL, 23420), -- 101789
(101788, 'deDE', 'Dämonenjäger', '', 'Illidari', NULL, 23420), -- 101788
(101787, 'deDE', 'Dämonenjägerin', '', 'Illidari', NULL, 23420), -- 101787
(96504, 'deDE', 'Zerstörerin der Shivarra', '', 'Illidans Dienerin', NULL, 23420), -- 96504
(96501, 'deDE', 'Mystiker der Aschenzungen', '', 'Illidans Diener', NULL, 23420), -- 96501
(96500, 'deDE', 'Krieger der Aschenzungen', '', 'Illidans Diener', NULL, 23420), -- 96500
(96436, 'deDE', 'Jace Düsterweber', '', 'Illidari', NULL, 23420), -- 96436
(98157, 'deDE', 'Lyana Düstergram', '', 'Illidari', NULL, 23420), -- 98157
(99759, 'deDE', 'Dämonischer Krabbler', '', NULL, NULL, 23420), -- 99759
(97971, 'deDE', 'Klon von Jace Düsterweber', '', 'Illidari', NULL, 23420), -- 97971
(97034, 'deDE', 'Champion des Zorns', '', NULL, NULL, 23420), -- 97034
(96502, 'deDE', 'Myrmidone der Echsennarbe', '', 'Illidans Diener', NULL, 23420), -- 96502
(102907, 'deDE', 'Krieger der Aschenzungen', '', 'Illidans Diener', NULL, 23420), -- 102907
(102906, 'deDE', 'Pirscher der Aschenzungen', '', 'Illidans Diener', NULL, 23420), -- 102906
(102905, 'deDE', 'Mystiker der Aschenzungen', '', 'Illidans Diener', NULL, 23420), -- 102905
(96653, 'deDE', 'Izal Weißmond', '', 'Flugmeisterin der Illidari', NULL, 23420), -- 96653
(95450, 'deDE', 'Pirscher der Aschenzungen', '', 'Illidans Diener', NULL, 23420), -- 95450
(95449, 'deDE', 'Mystiker der Aschenzungen', '', 'Illidans Diener', NULL, 23420), -- 95449
(95447, 'deDE', 'Krieger der Aschenzungen', '', 'Illidans Diener', NULL, 23420), -- 95447
(90247, 'deDE', 'Schlachtenfürst Gaardoun', '', 'Hauptmann der Aschenzungen', NULL, 23420), -- 90247
(96650, 'deDE', 'Falara Nachtweise', '', 'Versorgerin der Illidari', NULL, 23420), -- 96650
(96732, 'deDE', 'Verwüster der Legion', '', NULL, NULL, 23420), -- 96732
(94435, 'deDE', 'Matronenmutter Malicia', '', 'Hauptmann der Shivarra', NULL, 23420), -- 94435
(96252, 'deDE', 'Priesterin der Lust', '', 'Illidans Dienerin', NULL, 23420), -- 96252
(93707, 'deDE', 'Priesterin des Deliriums', '', 'Illidans Dienerin', NULL, 23420), -- 93707
(96253, 'deDE', 'Zerstörerin der Shivarra', '', 'Illidans Dienerin', NULL, 23420), -- 96253
(96655, 'deDE', 'Allari die Seelenfresserin', '', 'Illidari', NULL, 23420), -- 96655
(96420, 'deDE', 'Cyana Nachtgleve', '', 'Illidari', NULL, 23420), -- 96420
(105945, 'deDE', 'Sevis Leuchtflamme', '', 'Illidari', NULL, 23420), -- 105945
(97604, 'deDE', 'Zenturio der Königin', '', NULL, NULL, 23420), -- 97604
(97600, 'deDE', 'Dämonenjäger', '', 'Illidari', NULL, 23420), -- 97600
(97599, 'deDE', 'Dämonenjäger', '', 'Illidari', NULL, 23420), -- 97599
(102910, 'deDE', 'Myrmidone der Echsennarbe', '', 'Illidans Diener', NULL, 23420), -- 102910
(96652, 'deDE', 'Mannethrel Düsterstern', '', 'Illidari', NULL, 23420), -- 96652
(113927, 'deDE', 'Illidari Kilbride', '', 'Illidari', NULL, 23420), -- 113927
(113924, 'deDE', 'Illidari Starr', '', 'Illidari', NULL, 23420), -- 113924
(102908, 'deDE', 'Meeresruferin der Echsennarbe', '', 'Illidans Dienerin', NULL, 23420), -- 102908
(96278, 'deDE', 'Flammender Seelenpirscher', '', NULL, NULL, 23420), -- 96278
(93693, 'deDE', 'Lady S''theno', '', 'Hauptmann der Echsennarbe', NULL, 23420), -- 93693
(102724, 'deDE', 'Bösartige Seelenmeisterin', '', NULL, NULL, 23420), -- 102724
(97602, 'deDE', 'Flammender Seelenpirscher', '', NULL, NULL, 23420), -- 97602
(97014, 'deDE', 'Bösartiger Seelenmeister', '', NULL, NULL, 23420), -- 97014
(96231, 'deDE', 'Harpunenkämpfer der Echsennarbe', '', 'Illidans Diener', NULL, 23420), -- 96231
(96230, 'deDE', 'Myrmidone der Echsennarbe', '', 'Illidans Diener', NULL, 23420), -- 96230
(96228, 'deDE', 'Meeresruferin der Echsennarbe', '', 'Illidans Dienerin', NULL, 23420), -- 96228
(96930, 'deDE', 'Dämonenjäger', '', 'Illidari', NULL, 23420), -- 96930
(96654, 'deDE', 'Belath Dämmerklinge', '', 'Illidari', NULL, 23420), -- 96654
(96276, 'deDE', 'Klingenschwinge der Legion', '', NULL, NULL, 23420), -- 96276
(96931, 'deDE', 'Dämonenjäger', '', 'Illidari', NULL, 23420), -- 96931
(99352, 'deDE', 'Verwüstergeschoss der Legion', '', NULL, NULL, 23420), -- 99352
(97603, 'deDE', 'Verdammnismetzler', '', NULL, NULL, 23420), -- 97603
(93716, 'deDE', 'Verdammnismetzler', '', NULL, NULL, 23420), -- 93716
(93802, 'deDE', 'Brutkönigin Tyranna', '', 'Herrscherin von Mardum, dem Zerrütteten Abgrund', NULL, 23420), -- 93802
(99915, 'deDE', 'Sevis Leuchtflamme', '', 'Illidari', NULL, 23420), -- 99915
(98611, 'deDE', 'Vernichter der Verdammniswachen', '', NULL, NULL, 23420), -- 98611
(97634, 'deDE', 'Pfeiler des Kummers', '', NULL, NULL, 23420), -- 97634
(97629, 'deDE', 'Pfeiler des Kummers', '', NULL, NULL, 23420), -- 97629
(97624, 'deDE', 'Pfeiler des Kummers', '', NULL, NULL, 23420), -- 97624
(95048, 'deDE', 'Brutkönigin Tyranna', '', 'Herrscherin von Mardum, dem Zerrütteten Abgrund', NULL, 23420), -- 95048
(102714, 'deDE', 'Beschwörerin der Eredar', '', NULL, NULL, 23420), -- 102714
(95046, 'deDE', 'Beschwörer der Eredar', '', NULL, NULL, 23420), -- 95046
(94654, 'deDE', 'Vernichter der Verdammniswachen', '', NULL, NULL, 23420), -- 94654
(23837, 'deDE', 'ELM General Purpose Bunny', '', NULL, NULL, 23420), -- 23837
(93759, 'deDE', 'Jace Düsterweber', '', 'Illidari', NULL, 23420), -- 93759
(95049, 'deDE', 'Dämonenzauberschutz', '', NULL, NULL, 23420), -- 95049
(93117, 'deDE', 'Izal Weißmond', '', 'Illidari', NULL, 23420), -- 93117
(93230, 'deDE', 'Mannethrel Düsterstern', '', 'Illidari', NULL, 23420), -- 93230
(98907, 'deDE', 'Dämonische Halluzination', '', NULL, NULL, 23420), -- 98907
(98905, 'deDE', 'Dämonische Halluzination', '', NULL, NULL, 23420), -- 98905
(94651, 'deDE', 'Höllischer Wichtel', '', NULL, NULL, 23420), -- 94651
(99917, 'deDE', 'Sevis Leuchtflamme', '', 'Illidari', NULL, 23420), -- 99917
(99914, 'deDE', 'Mystiker der Aschenzungen', '', 'Illidans Diener', NULL, 23420), -- 99914
(93105, 'deDE', 'Inquisitor Seelenschmerz', '', NULL, NULL, 23420), -- 93105
(94400, 'deDE', 'Belath Dämmerklinge', '', 'Illidari', NULL, 23420), -- 94400
(96400, 'deDE', 'Schläger der Mo''arg', '', NULL, NULL, 23420), -- 96400
(96731, 'deDE', 'Verwüster der Legion', '', NULL, NULL, 23420), -- 96731
(99351, 'deDE', 'Verwüstergeschoss der Legion', '', NULL, NULL, 23420), -- 99351
(94377, 'deDE', 'Cyana Nachtgleve', '', 'Illidari', NULL, 23420), -- 94377
(94705, 'deDE', 'Dämonenjäger', '', 'Illidari', NULL, 23420), -- 94705
(94704, 'deDE', 'Dämonenjäger', '', 'Illidari', NULL, 23420), -- 94704
(101288, 'deDE', 'Abyssischer Basilisk', '', NULL, NULL, 23420), -- 101288
(95226, 'deDE', 'Wärter der Qual', '', NULL, NULL, 23420), -- 95226
(96159, 'deDE', 'Gigantische Höllenbestie', '', NULL, NULL, 23420), -- 96159
(100982, 'deDE', 'Sevis Leuchtflamme', '', 'Illidari', NULL, 23420), -- 100982
(105316, 'deDE', 'Verhörer Arev''naal', '', NULL, NULL, 23420), -- 105316
(24021, 'deDE', 'ELM General Purpose Bunny (scale x0.01)', '', NULL, NULL, 23420), -- 24021
(93112, 'deDE', 'Wachposten der Teufelswache', '', NULL, NULL, 23420), -- 93112
(97592, 'deDE', 'Wachposten der Teufelswache', '', NULL, NULL, 23420), -- 97592
(93127, 'deDE', 'Kayn Sonnenzorn', '', 'Illidari', NULL, 23420), -- 93127
(97142, 'deDE', 'Teufelskondensator', '', NULL, 'questinteract', 23420), -- 97142
(93221, 'deDE', 'Verdammniskommandant Beliash', '', NULL, NULL, 23420), -- 93221
(93115, 'deDE', 'Scheußlicher Teufelspirscher', '', NULL, NULL, 23420), -- 93115
(94410, 'deDE', 'Allari die Seelenfresserin', '', 'Illidari', NULL, 23420), -- 94410
(101518, 'deDE', 'Teufelssäbler', '', NULL, 'questinteract', 23420), -- 101518
(100510, 'deDE', 'Die Teufelshammer', '', NULL, NULL, 23420), -- 100510
(37490, 'deDE', 'ELM General Purpose Bunny Infinite', '', NULL, NULL, 23420), -- 37490
(99916, 'deDE', 'Sevis Leuchtflamme', '', 'Illidari', NULL, 23420), -- 99916
(101704, 'deDE', 'Mächtiger Teufelskristall', '', NULL, 'openhandglow', 23420), -- 101704
(98229, 'deDE', 'Kayn Sonnenzorn', '', 'Illidari', NULL, 23420), -- 98229
(98354, 'deDE', 'Kor''vas Blutdorn', '', 'Illidari', NULL, 23420), -- 98354
(98456, 'deDE', 'Allari die Seelenfresserin', '', 'Illidari', NULL, 23420), -- 98456
(98457, 'deDE', 'Cyana Nachtgleve', '', 'Illidari', NULL, 23420), -- 98457
(93762, 'deDE', 'Verwüster der Legion', '', NULL, NULL, 23420), -- 93762
(98460, 'deDE', 'Kor''vas Blutdorn', '', 'Illidari', NULL, 23420), -- 98460
(98458, 'deDE', 'Jace Düsterweber', '', 'Illidari', NULL, 23420), -- 98458
(98459, 'deDE', 'Kayn Sonnenzorn', '', 'Illidari', NULL, 23420), -- 98459
(98482, 'deDE', 'Scheußlicher Teufelspirscher', '', NULL, NULL, 23420), -- 98482
(99919, 'deDE', 'Sevis Leuchtflamme', '', 'Illidari', NULL, 23420), -- 99919
(99656, 'deDE', 'Roter Brutling', '', NULL, NULL, 23420), -- 99656
(98497, 'deDE', 'Wichtelmutter', '', NULL, NULL, 23420), -- 98497
(98483, 'deDE', 'Höllischer Wichtel', '', NULL, NULL, 23420), -- 98483
(98486, 'deDE', 'Zornkrieger', '', NULL, NULL, 23420), -- 98486
(98484, 'deDE', 'Schläger der Mo''arg', '', NULL, NULL, 23420), -- 98484
(101748, 'deDE', 'Teufelsgeysir', '', NULL, NULL, 23420), -- 101748
(98622, 'deDE', 'Schläger der Mo''arg', '', NULL, NULL, 23420), -- 98622
(98621, 'deDE', 'Wichtelmutter', '', NULL, NULL, 23420), -- 98621
(98618, 'deDE', 'Höllischer Wichtel', '', NULL, NULL, 23420), -- 98618
(99918, 'deDE', 'Sevis Leuchtflamme', '', 'Illidari', NULL, 23420), -- 99918
(98292, 'deDE', 'Kor''vas Blutdorn', '', 'Illidari', NULL, 23420), -- 98292
(98290, 'deDE', 'Cyana Nachtgleve', '', 'Illidari', NULL, 23420), -- 98290
(98228, 'deDE', 'Jace Düsterweber', '', 'Illidari', NULL, 23420), -- 98228
(98227, 'deDE', 'Allari die Seelenfresserin', '', 'Illidari', NULL, 23420), -- 98227
(93011, 'deDE', 'Kayn Sonnenzorn', '', 'Illidari', NULL, 23420), -- 93011
(97712, 'deDE', 'Zornkrieger', '', NULL, NULL, 23420), -- 97712
(97594, 'deDE', 'Scheußlicher Teufelspirscher', '', NULL, NULL, 23420), -- 97594
(99218, 'deDE', 'Verwüstergeschoss der Legion', '', NULL, NULL, 23420), -- 99218
(100161, 'deDE', 'Verwüster der Legion', '', NULL, NULL, 23420), -- 100161
(94492, 'deDE', 'Gigantische Höllenbestie', '', NULL, NULL, 23420), -- 94492
(98191, 'deDE', 'Befehlszentrum der Legion', '', NULL, NULL, 23420), -- 98191
(94744, 'deDE', 'Schreckliche Teufelsfledermaus', '', NULL, NULL, 23420), -- 94744
(94655, 'deDE', 'Seelensauger', '', NULL, NULL, 23420), -- 94655
(99650, 'deDE', 'Dornenklauenbrutling', '', NULL, NULL, 23420); -- 99650
