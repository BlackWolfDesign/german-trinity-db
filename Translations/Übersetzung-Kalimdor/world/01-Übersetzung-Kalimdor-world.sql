-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/24/2017 17:32:52


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=8726 AND `locale`='deDE') OR (`ID`=8672 AND `locale`='deDE') OR (`ID`=29741 AND `locale`='deDE') OR (`ID`=45738 AND `locale`='deDE') OR (`ID`=8686 AND `locale`='deDE') OR (`ID`=43294 AND `locale`='deDE') OR (`ID`=43244 AND `locale`='deDE') OR (`ID`=43293 AND `locale`='deDE') OR (`ID`=43243 AND `locale`='deDE') OR (`ID`=43242 AND `locale`='deDE') OR (`ID`=43245 AND `locale`='deDE') OR (`ID`=43287 AND `locale`='deDE') OR (`ID`=43297 AND `locale`='deDE') OR (`ID`=43285 AND `locale`='deDE') OR (`ID`=43286 AND `locale`='deDE') OR (`ID`=43296 AND `locale`='deDE') OR (`ID`=43288 AND `locale`='deDE') OR (`ID`=34774 AND `locale`='deDE') OR (`ID`=8717 AND `locale`='deDE') OR (`ID`=8680 AND `locale`='deDE') OR (`ID`=8670 AND `locale`='deDE') OR (`ID`=29742 AND `locale`='deDE') OR (`ID`=8724 AND `locale`='deDE') OR (`ID`=8684 AND `locale`='deDE') OR (`ID`=8671 AND `locale`='deDE') OR (`ID`=8681 AND `locale`='deDE') OR (`ID`=43283 AND `locale`='deDE') OR (`ID`=43291 AND `locale`='deDE') OR (`ID`=43292 AND `locale`='deDE') OR (`ID`=43284 AND `locale`='deDE') OR (`ID`=29740 AND `locale`='deDE') OR (`ID`=29177 AND `locale`='deDE') OR (`ID`=24759 AND `locale`='deDE') OR (`ID`=368 AND `locale`='deDE') OR (`ID`=24758 AND `locale`='deDE') OR (`ID`=8723 AND `locale`='deDE') OR (`ID`=8715 AND `locale`='deDE') OR (`ID`=8867 AND `locale`='deDE') OR (`ID`=8718 AND `locale`='deDE') OR (`ID`=8872 AND `locale`='deDE') OR (`ID`=8679 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(8726, 'deDE', 'Urahnin Prunkspeer', '', '', '', '', '', '', '', '', 23420),
(8672, 'deDE', 'Urahne Steinkeil', '', '', '', '', '', '', '', '', 23420),
(29741, 'deDE', 'Urahne Sekhemi', '', '', '', '', '', '', '', '', 23360),
(45738, 'deDE', 'Der Koloss von Ashi', '', '', '', '', '', '', '', '', 23360),
(8686, 'deDE', 'Urahne Hohenberg', '', '', '', '', '', '', '', '', 23360),
(43294, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23360),
(43244, 'deDE', 'Invasion: Tanaris', '', '', '', '', '', '', '', '', 23360),
(43293, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23360),
(43243, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23360),
(43242, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23420),
(43245, 'deDE', 'Invasion: Westfall', '', '', '', '', '', '', '', '', 23420),
(43287, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23420),
(43297, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23420),
(43285, 'deDE', 'Invasion: Vorgebirge des Hügellands', '', '', '', '', '', '', '', '', 23420),
(43286, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23420),
(43296, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23420),
(43288, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23420),
(34774, 'deDE', 'Blingtron 5000', '', '', '', '', '', '', '', '', 23360),
(8717, 'deDE', 'Urahne Mondwacht', '', '', '', '', '', '', '', '', 23360),
(8680, 'deDE', 'Urahnin Windtotem', '', '', '', '', '', '', '', '', 23360),
(8670, 'deDE', 'Urahnin Runentotem', '', '', '', '', '', '', '', '', 23360),
(29742, 'deDE', 'Urahne Menkhaf', '', '', '', '', '', '', '', '', 23360),
(8724, 'deDE', 'Urahnin Morgentau', '', '', '', '', '', '', '', '', 23420),
(8684, 'deDE', 'Urahne Traumdeuter', '', '', '', '', '', '', '', '', 23360),
(8671, 'deDE', 'Urahnin Rachtotem', '', '', '', '', '', '', '', '', 23360),
(8681, 'deDE', 'Urahne Donnerhorn', '', '', '', '', '', '', '', '', 23420),
(43283, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23420),
(43291, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23420),
(43292, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23420),
(43284, 'deDE', 'Invasion: Dun Morogh', '', '', '', '', '', '', '', '', 23420),
(29740, 'deDE', 'Urahnin Immerschatten', '', '', '', '', '', '', '', '', 23420),
(29177, 'deDE', 'Geflügelte Wachsamkeit', 'Legt eine Zwielichtfeuerlanze an und fliegt auf Avianas Wächter. Vernichtet 10 Zwielichttjoster, indem Ihr in sie hineinfliegt. Drückt die Taste ''Flattern'', damit Euer Reittier mit den Flügeln schlägt!', 'Die Agenten des Zwielichts setzen ihren Angriff innerhalb dieses Kessels in den Feuerlanden fort. Wir dürfen nicht in unserer Offensive nachlassen! Schwingt Euch in die Lüfte und vernichtet so viele der Tjoster, wie Ihr könnt!$b$bVergesst nicht, Euch zunächst mit einer Lanze auszurüsten.', '', '', '', '', '', '', 23420),
(24759, 'deDE', 'Die Grundlagen: Dinge schlagen', 'Zerstört 6 Tikiziele.', 'Alles klar, Mann. Fangen wir mit den Grundlagen an. Ich muss wissen, dass Ihr die Grundlagen der natürlichen Künste beherrscht.$B$BGeht und zerstört diese Tikiziele. Geht es langsam an. Natürlich. Ich beobachte Eure Form und dann sehen wir weiter.', '', '', '', '', '', '', 23360),
(368, 'deDE', 'Eine neue Seuche', 'Apotheker Johaan in der Stadt Brill benötigt 5 Finsterflossenschuppen von den Murlocs in Tirisfal.', 'Während Ihr unterwegs wart und Proben für mich gesammelt habt, haben meine Experimente ergeben, dass noch weitere Reagenzien benötigt werden, wenn sich diese Krankheit ordentlich ausbreiten soll. Ein paar unglückliche Opfer zu vergiften, ist eine Kleinigkeit. Eine ganze Welt erkranken zu lassen, erweist sich als etwas komplizierter.$b$bIch werde 5 Finsterflossenschuppen von den Murlocs in der Umgebung brauchen. Ihr findet die Kreaturen entlang der Küste im Norden oder Westen von hier.', '', '', '', '', '', '', 23360),
(24758, 'deDE', 'Aufstieg der Dunkelspeere', 'Trefft Nekali auf dem Ausbildungsgelände der Dunkelspeere.', 'Die Trolle des Dunkelspeerstamms waren lange Zeit Opfer. Ausgestoßene. Immer wieder mussten wir fliehen und unser Zuhause verlassen. Aber diesmal hat uns nicht die Flucht hierher getrieben.$B$BVol''jin bot uns Führung durch Weisheit, und das hat uns so lange am Leben erhalten. Nun bietet uns diese Weisheit Vorzeichen einer chaotischen Zukunft für die Horde und für Garrosh Höllschrei.$B$BNein, diesmal fliehen wir nicht. Diesmal kämpfen wir für ein Zuhause, das bestehen bleibt.$B$BEs ist Zeit, mit Eurer Ausbildung zu beginnen, $n. Sprecht mit Nekali im Osten.', '', '', '', '', '', '', 23360),
(8723, 'deDE', 'Urahne Nachtwind', '', '', '', '', '', '', '', '', 23420),
(8715, 'deDE', 'Urahne Messerblatt', '', '', '', '', '', '', '', '', 23420),
(8867, 'deDE', 'Mondfeuerwerk', 'Zündet 8 Raketen und 2 Raketenbündel und kehrt dann zu einer Botin des Mondfests in irgendeiner Hauptstadt zurück.', 'Die Druiden der Mondlichtung feiern jedes Jahr den großen Triumph unserer Stadt über ein uraltes Übel.$B$BWir ehren die Weisheit unserer Urahnen, nehmen an herrlichen Festgelagen teil und... schießen natürlich Feuerwerk ab!$B$BFür eine kleine Spende könnt Ihr bei unserem Verkäufer farbenfrohe Raketen erwerben. Versucht doch mal einen der hier herumstehenden Zünder, $n.', '', '', '', '', '', '', 23420),
(8718, 'deDE', 'Urahne Klingentanz', '', '', '', '', '', '', '', '', 23420),
(8872, 'deDE', 'Das Mondfest', 'Sprecht mit der Botin des Mondfests in der Enklave des Cenarius in Darnassus. Die Boten in den anderen Hauptstädten können Euch ebenfalls weiterhelfen.', 'Es ist mal wieder soweit, $n! Der Zirkel des Cenarius lädt alle Bewohner Azeroths dazu ein, gemeinsam mit uns das Mondfest zu feiern. Der Ort, an dem Ihr die örtliche Botin des Mondfests finden könnt, wird durch einen Kreis aus heiligem Mondlicht markiert. Sie vermag Euch ausführlicher über dieses Fest zu berichten.', '', '', '', '', '', '', 23420),
(8679, 'deDE', 'Urahne Grimmtotem', '', '', '', '', '', '', '', '', 23420);


SET NAMES 'utf8';
DELETE FROM `quest_objectives_locale` WHERE (`ID`=258788) OR (`ID`=258787);
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(258788, 'deDE', 8867, 1, 'Raketenbündel abgefeuert', 23420),
(258787, 'deDE', 8867, 0, 'Raketen abgefeuert', 23420);


DELETE FROM `gameobject_template_locale` WHERE (`entry`=247045 AND `locale`='deDE') OR (`entry`=247042 AND `locale`='deDE') OR (`entry`=247040 AND `locale`='deDE') OR (`entry`=153516 AND `locale`='deDE') OR (`entry`=207126 AND `locale`='deDE') OR (`entry`=175932 AND `locale`='deDE') OR (`entry`=175930 AND `locale`='deDE') OR (`entry`=205008 AND `locale`='deDE') OR (`entry`=207179 AND `locale`='deDE') OR (`entry`=175929 AND `locale`='deDE') OR (`entry`=175931 AND `locale`='deDE') OR (`entry`=179864 AND `locale`='deDE') OR (`entry`=179863 AND `locale`='deDE') OR (`entry`=175927 AND `locale`='deDE') OR (`entry`=176404 AND `locale`='deDE') OR (`entry`=179125 AND `locale`='deDE') OR (`entry`=176749 AND `locale`='deDE') OR (`entry`=176748 AND `locale`='deDE') OR (`entry`=187299 AND `locale`='deDE') OR (`entry`=207301 AND `locale`='deDE') OR (`entry`=207300 AND `locale`='deDE') OR (`entry`=207293 AND `locale`='deDE') OR (`entry`=208192 AND `locale`='deDE') OR (`entry`=176804 AND `locale`='deDE') OR (`entry`=176806 AND `locale`='deDE') OR (`entry`=176805 AND `locale`='deDE') OR (`entry`=176807 AND `locale`='deDE') OR (`entry`=176808 AND `locale`='deDE') OR (`entry`=208189 AND `locale`='deDE') OR (`entry`=175811 AND `locale`='deDE') OR (`entry`=207291 AND `locale`='deDE') OR (`entry`=207306 AND `locale`='deDE') OR (`entry`=175587 AND `locale`='deDE') OR (`entry`=207424 AND `locale`='deDE') OR (`entry`=175629 AND `locale`='deDE') OR (`entry`=175628 AND `locale`='deDE') OR (`entry`=175586 AND `locale`='deDE') OR (`entry`=207419 AND `locale`='deDE') OR (`entry`=207420 AND `locale`='deDE') OR (`entry`=207421 AND `locale`='deDE') OR (`entry`=207489 AND `locale`='deDE') OR (`entry`=207423 AND `locale`='deDE') OR (`entry`=176588 AND `locale`='deDE') OR (`entry`=207422 AND `locale`='deDE') OR (`entry`=175324 AND `locale`='deDE') OR (`entry`=206684 AND `locale`='deDE') OR (`entry`=206682 AND `locale`='deDE') OR (`entry`=207595 AND `locale`='deDE') OR (`entry`=207594 AND `locale`='deDE') OR (`entry`=206591 AND `locale`='deDE') OR (`entry`=205564 AND `locale`='deDE') OR (`entry`=207710 AND `locale`='deDE') OR (`entry`=206393 AND `locale`='deDE') OR (`entry`=202615 AND `locale`='deDE') OR (`entry`=207969 AND `locale`='deDE') OR (`entry`=207962 AND `locale`='deDE') OR (`entry`=207600 AND `locale`='deDE') OR (`entry`=207599 AND `locale`='deDE') OR (`entry`=207598 AND `locale`='deDE') OR (`entry`=214507 AND `locale`='deDE') OR (`entry`=206689 AND `locale`='deDE') OR (`entry`=206949 AND `locale`='deDE') OR (`entry`=206954 AND `locale`='deDE') OR (`entry`=206953 AND `locale`='deDE') OR (`entry`=206588 AND `locale`='deDE') OR (`entry`=206112 AND `locale`='deDE') OR (`entry`=207399 AND `locale`='deDE') OR (`entry`=207398 AND `locale`='deDE') OR (`entry`=206551 AND `locale`='deDE') OR (`entry`=206075 AND `locale`='deDE') OR (`entry`=206531 AND `locale`='deDE') OR (`entry`=206964 AND `locale`='deDE') OR (`entry`=207698 AND `locale`='deDE') OR (`entry`=207069 AND `locale`='deDE') OR (`entry`=206563 AND `locale`='deDE') OR (`entry`=205874 AND `locale`='deDE') OR (`entry`=205824 AND `locale`='deDE') OR (`entry`=205385 AND `locale`='deDE') OR (`entry`=205473 AND `locale`='deDE') OR (`entry`=206895 AND `locale`='deDE') OR (`entry`=206898 AND `locale`='deDE') OR (`entry`=206900 AND `locale`='deDE') OR (`entry`=206891 AND `locale`='deDE') OR (`entry`=206911 AND `locale`='deDE') OR (`entry`=206910 AND `locale`='deDE') OR (`entry`=206896 AND `locale`='deDE') OR (`entry`=207124 AND `locale`='deDE') OR (`entry`=206897 AND `locale`='deDE') OR (`entry`=180453 AND `locale`='deDE') OR (`entry`=208242 AND `locale`='deDE') OR (`entry`=205527 AND `locale`='deDE') OR (`entry`=207604 AND `locale`='deDE') OR (`entry`=207603 AND `locale`='deDE') OR (`entry`=206940 AND `locale`='deDE') OR (`entry`=207280 AND `locale`='deDE') OR (`entry`=206892 AND `locale`='deDE') OR (`entry`=206904 AND `locale`='deDE') OR (`entry`=206893 AND `locale`='deDE') OR (`entry`=206561 AND `locale`='deDE') OR (`entry`=206939 AND `locale`='deDE') OR (`entry`=206933 AND `locale`='deDE') OR (`entry`=206879 AND `locale`='deDE') OR (`entry`=206878 AND `locale`='deDE') OR (`entry`=206934 AND `locale`='deDE') OR (`entry`=206903 AND `locale`='deDE') OR (`entry`=206902 AND `locale`='deDE') OR (`entry`=180904 AND `locale`='deDE') OR (`entry`=180899 AND `locale`='deDE') OR (`entry`=180898 AND `locale`='deDE') OR (`entry`=206901 AND `locale`='deDE') OR (`entry`=206932 AND `locale`='deDE') OR (`entry`=206894 AND `locale`='deDE') OR (`entry`=206912 AND `locale`='deDE') OR (`entry`=207130 AND `locale`='deDE') OR (`entry`=207151 AND `locale`='deDE') OR (`entry`=207166 AND `locale`='deDE') OR (`entry`=206390 AND `locale`='deDE') OR (`entry`=208241 AND `locale`='deDE') OR (`entry`=202779 AND `locale`='deDE') OR (`entry`=205539 AND `locale`='deDE') OR (`entry`=195203 AND `locale`='deDE') OR (`entry`=141863 AND `locale`='deDE') OR (`entry`=141862 AND `locale`='deDE') OR (`entry`=142198 AND `locale`='deDE') OR (`entry`=204997 AND `locale`='deDE') OR (`entry`=201696 AND `locale`='deDE') OR (`entry`=142197 AND `locale`='deDE') OR (`entry`=202335 AND `locale`='deDE') OR (`entry`=202325 AND `locale`='deDE') OR (`entry`=202324 AND `locale`='deDE') OR (`entry`=202323 AND `locale`='deDE') OR (`entry`=202322 AND `locale`='deDE') OR (`entry`=202321 AND `locale`='deDE') OR (`entry`=202320 AND `locale`='deDE') OR (`entry`=48516 AND `locale`='deDE') OR (`entry`=201703 AND `locale`='deDE') OR (`entry`=202405 AND `locale`='deDE') OR (`entry`=3727 AND `locale`='deDE') OR (`entry`=3729 AND `locale`='deDE') OR (`entry`=23577 AND `locale`='deDE') OR (`entry`=201395 AND `locale`='deDE') OR (`entry`=201394 AND `locale`='deDE') OR (`entry`=201393 AND `locale`='deDE') OR (`entry`=201392 AND `locale`='deDE') OR (`entry`=201391 AND `locale`='deDE') OR (`entry`=201701 AND `locale`='deDE') OR (`entry`=201399 AND `locale`='deDE') OR (`entry`=202176 AND `locale`='deDE') OR (`entry`=202175 AND `locale`='deDE') OR (`entry`=202174 AND `locale`='deDE') OR (`entry`=202173 AND `locale`='deDE') OR (`entry`=202172 AND `locale`='deDE') OR (`entry`=201724 AND `locale`='deDE') OR (`entry`=201602 AND `locale`='deDE') OR (`entry`=182255 AND `locale`='deDE') OR (`entry`=201781 AND `locale`='deDE') OR (`entry`=195640 AND `locale`='deDE') OR (`entry`=177525 AND `locale`='deDE') OR (`entry`=201792 AND `locale`='deDE') OR (`entry`=195502 AND `locale`='deDE') OR (`entry`=195505 AND `locale`='deDE') OR (`entry`=195503 AND `locale`='deDE') OR (`entry`=195504 AND `locale`='deDE') OR (`entry`=202467 AND `locale`='deDE') OR (`entry`=204994 AND `locale`='deDE') OR (`entry`=204993 AND `locale`='deDE') OR (`entry`=201588 AND `locale`='deDE') OR (`entry`=204999 AND `locale`='deDE') OR (`entry`=201587 AND `locale`='deDE') OR (`entry`=202597 AND `locale`='deDE') OR (`entry`=202598 AND `locale`='deDE') OR (`entry`=201723 AND `locale`='deDE') OR (`entry`=202535 AND `locale`='deDE') OR (`entry`=201924 AND `locale`='deDE') OR (`entry`=201725 AND `locale`='deDE') OR (`entry`=3266 AND `locale`='deDE') OR (`entry`=202542 AND `locale`='deDE') OR (`entry`=141813 AND `locale`='deDE') OR (`entry`=201963 AND `locale`='deDE') OR (`entry`=201971 AND `locale`='deDE') OR (`entry`=202533 AND `locale`='deDE') OR (`entry`=201879 AND `locale`='deDE') OR (`entry`=201877 AND `locale`='deDE') OR (`entry`=202478 AND `locale`='deDE') OR (`entry`=202477 AND `locale`='deDE') OR (`entry`=201810 AND `locale`='deDE') OR (`entry`=202452 AND `locale`='deDE') OR (`entry`=91706 AND `locale`='deDE') OR (`entry`=201743 AND `locale`='deDE') OR (`entry`=204992 AND `locale`='deDE') OR (`entry`=204991 AND `locale`='deDE') OR (`entry`=201595 AND `locale`='deDE') OR (`entry`=201738 AND `locale`='deDE') OR (`entry`=201737 AND `locale`='deDE') OR (`entry`=204998 AND `locale`='deDE') OR (`entry`=202595 AND `locale`='deDE') OR (`entry`=203814 AND `locale`='deDE') OR (`entry`=204996 AND `locale`='deDE') OR (`entry`=202476 AND `locale`='deDE') OR (`entry`=204995 AND `locale`='deDE') OR (`entry`=200302 AND `locale`='deDE') OR (`entry`=200303 AND `locale`='deDE') OR (`entry`=246267 AND `locale`='deDE') OR (`entry`=180876 AND `locale`='deDE') OR (`entry`=180851 AND `locale`='deDE') OR (`entry`=246960 AND `locale`='deDE') OR (`entry`=246683 AND `locale`='deDE') OR (`entry`=178984 AND `locale`='deDE') OR (`entry`=177785 AND `locale`='deDE') OR (`entry`=177273 AND `locale`='deDE') OR (`entry`=218722 AND `locale`='deDE') OR (`entry`=195219 AND `locale`='deDE') OR (`entry`=180765 AND `locale`='deDE') OR (`entry`=195218 AND `locale`='deDE') OR (`entry`=180910 AND `locale`='deDE') OR (`entry`=180879 AND `locale`='deDE') OR (`entry`=180909 AND `locale`='deDE') OR (`entry`=190942 AND `locale`='deDE') OR (`entry`=185518 AND `locale`='deDE') OR (`entry`=185503 AND `locale`='deDE') OR (`entry`=185495 AND `locale`='deDE') OR (`entry`=185504 AND `locale`='deDE') OR (`entry`=185494 AND `locale`='deDE') OR (`entry`=185492 AND `locale`='deDE') OR (`entry`=180875 AND `locale`='deDE') OR (`entry`=180874 AND `locale`='deDE') OR (`entry`=180340 AND `locale`='deDE') OR (`entry`=180339 AND `locale`='deDE') OR (`entry`=180338 AND `locale`='deDE') OR (`entry`=247053 AND `locale`='deDE') OR (`entry`=247052 AND `locale`='deDE') OR (`entry`=175765 AND `locale`='deDE') OR (`entry`=195693 AND `locale`='deDE') OR (`entry`=175764 AND `locale`='deDE') OR (`entry`=204746 AND `locale`='deDE') OR (`entry`=175767 AND `locale`='deDE') OR (`entry`=175766 AND `locale`='deDE') OR (`entry`=203805 AND `locale`='deDE') OR (`entry`=239240 AND `locale`='deDE') OR (`entry`=207276 AND `locale`='deDE') OR (`entry`=207275 AND `locale`='deDE') OR (`entry`=207274 AND `locale`='deDE') OR (`entry`=207273 AND `locale`='deDE') OR (`entry`=207272 AND `locale`='deDE') OR (`entry`=207271 AND `locale`='deDE') OR (`entry`=207270 AND `locale`='deDE') OR (`entry`=199332 AND `locale`='deDE') OR (`entry`=200298 AND `locale`='deDE') OR (`entry`=207220 AND `locale`='deDE') OR (`entry`=207269 AND `locale`='deDE') OR (`entry`=207268 AND `locale`='deDE') OR (`entry`=207267 AND `locale`='deDE') OR (`entry`=207266 AND `locale`='deDE') OR (`entry`=207265 AND `locale`='deDE') OR (`entry`=207264 AND `locale`='deDE') OR (`entry`=204413 AND `locale`='deDE') OR (`entry`=204412 AND `locale`='deDE') OR (`entry`=204121 AND `locale`='deDE') OR (`entry`=204050 AND `locale`='deDE') OR (`entry`=204122 AND `locale`='deDE') OR (`entry`=204232 AND `locale`='deDE') OR (`entry`=204231 AND `locale`='deDE') OR (`entry`=204230 AND `locale`='deDE') OR (`entry`=204229 AND `locale`='deDE') OR (`entry`=197332 AND `locale`='deDE') OR (`entry`=195345 AND `locale`='deDE') OR (`entry`=195651 AND `locale`='deDE') OR (`entry`=195584 AND `locale`='deDE') OR (`entry`=195583 AND `locale`='deDE') OR (`entry`=195575 AND `locale`='deDE') OR (`entry`=195686 AND `locale`='deDE') OR (`entry`=195711 AND `locale`='deDE') OR (`entry`=196461 AND `locale`='deDE') OR (`entry`=196460 AND `locale`='deDE') OR (`entry`=196459 AND `locale`='deDE') OR (`entry`=196407 AND `locale`='deDE') OR (`entry`=195367 AND `locale`='deDE') OR (`entry`=204271 AND `locale`='deDE') OR (`entry`=204270 AND `locale`='deDE') OR (`entry`=204268 AND `locale`='deDE') OR (`entry`=204238 AND `locale`='deDE') OR (`entry`=204269 AND `locale`='deDE') OR (`entry`=195576 AND `locale`='deDE') OR (`entry`=196836 AND `locale`='deDE') OR (`entry`=204775 AND `locale`='deDE') OR (`entry`=143397 AND `locale`='deDE') OR (`entry`=195577 AND `locale`='deDE') OR (`entry`=195365 AND `locale`='deDE') OR (`entry`=195582 AND `locale`='deDE') OR (`entry`=20898 AND `locale`='deDE') OR (`entry`=195587 AND `locale`='deDE') OR (`entry`=142123 AND `locale`='deDE') OR (`entry`=196833 AND `locale`='deDE') OR (`entry`=142124 AND `locale`='deDE') OR (`entry`=196832 AND `locale`='deDE') OR (`entry`=199330 AND `locale`='deDE') OR (`entry`=201572 AND `locale`='deDE') OR (`entry`=196474 AND `locale`='deDE') OR (`entry`=195681 AND `locale`='deDE') OR (`entry`=204786 AND `locale`='deDE') OR (`entry`=196835 AND `locale`='deDE') OR (`entry`=88496 AND `locale`='deDE') OR (`entry`=204785 AND `locale`='deDE') OR (`entry`=207425 AND `locale`='deDE') OR (`entry`=204784 AND `locale`='deDE') OR (`entry`=201597 AND `locale`='deDE') OR (`entry`=88497 AND `locale`='deDE') OR (`entry`=196834 AND `locale`='deDE') OR (`entry`=205082 AND `locale`='deDE') OR (`entry`=20918 AND `locale`='deDE') OR (`entry`=201302 AND `locale`='deDE') OR (`entry`=204245 AND `locale`='deDE') OR (`entry`=150086 AND `locale`='deDE') OR (`entry`=201579 AND `locale`='deDE') OR (`entry`=195642 AND `locale`='deDE') OR (`entry`=195513 AND `locale`='deDE') OR (`entry`=250419 AND `locale`='deDE') OR (`entry`=195683 AND `locale`='deDE') OR (`entry`=195676 AND `locale`='deDE') OR (`entry`=250420 AND `locale`='deDE') OR (`entry`=195461 AND `locale`='deDE') OR (`entry`=250418 AND `locale`='deDE') OR (`entry`=142121 AND `locale`='deDE') OR (`entry`=204237 AND `locale`='deDE') OR (`entry`=204236 AND `locale`='deDE') OR (`entry`=204234 AND `locale`='deDE') OR (`entry`=204235 AND `locale`='deDE') OR (`entry`=201397 AND `locale`='deDE') OR (`entry`=250421 AND `locale`='deDE') OR (`entry`=250416 AND `locale`='deDE') OR (`entry`=196487 AND `locale`='deDE') OR (`entry`=196486 AND `locale`='deDE') OR (`entry`=204252 AND `locale`='deDE') OR (`entry`=204251 AND `locale`='deDE') OR (`entry`=204250 AND `locale`='deDE') OR (`entry`=204249 AND `locale`='deDE') OR (`entry`=204248 AND `locale`='deDE') OR (`entry`=195360 AND `locale`='deDE') OR (`entry`=199331 AND `locale`='deDE') OR (`entry`=199329 AND `locale`='deDE') OR (`entry`=199333 AND `locale`='deDE') OR (`entry`=204244 AND `locale`='deDE') OR (`entry`=201386 AND `locale`='deDE') OR (`entry`=204233 AND `locale`='deDE') OR (`entry`=201400 AND `locale`='deDE') OR (`entry`=152621 AND `locale`='deDE') OR (`entry`=195623 AND `locale`='deDE') OR (`entry`=141870 AND `locale`='deDE') OR (`entry`=196462 AND `locale`='deDE') OR (`entry`=152620 AND `locale`='deDE') OR (`entry`=152631 AND `locale`='deDE') OR (`entry`=152622 AND `locale`='deDE') OR (`entry`=195455 AND `locale`='deDE') OR (`entry`=195435 AND `locale`='deDE') OR (`entry`=195431 AND `locale`='deDE') OR (`entry`=204783 AND `locale`='deDE') OR (`entry`=197345 AND `locale`='deDE') OR (`entry`=197346 AND `locale`='deDE') OR (`entry`=197344 AND `locale`='deDE') OR (`entry`=196870 AND `locale`='deDE') OR (`entry`=184856 AND `locale`='deDE') OR (`entry`=177278 AND `locale`='deDE') OR (`entry`=176792 AND `locale`='deDE') OR (`entry`=201401 AND `locale`='deDE') OR (`entry`=207404 AND `locale`='deDE') OR (`entry`=195448 AND `locale`='deDE') OR (`entry`=195447 AND `locale`='deDE') OR (`entry`=195146 AND `locale`='deDE') OR (`entry`=3743 AND `locale`='deDE') OR (`entry`=3737 AND `locale`='deDE') OR (`entry`=24725 AND `locale`='deDE') OR (`entry`=152618 AND `locale`='deDE') OR (`entry`=24724 AND `locale`='deDE') OR (`entry`=24728 AND `locale`='deDE') OR (`entry`=3246 AND `locale`='deDE') OR (`entry`=24729 AND `locale`='deDE') OR (`entry`=3249 AND `locale`='deDE') OR (`entry`=24726 AND `locale`='deDE') OR (`entry`=195311 AND `locale`='deDE') OR (`entry`=24723 AND `locale`='deDE') OR (`entry`=195240 AND `locale`='deDE') OR (`entry`=18930 AND `locale`='deDE') OR (`entry`=195206 AND `locale`='deDE') OR (`entry`=3252 AND `locale`='deDE') OR (`entry`=195187 AND `locale`='deDE') OR (`entry`=3251 AND `locale`='deDE') OR (`entry`=195205 AND `locale`='deDE') OR (`entry`=178884 AND `locale`='deDE') OR (`entry`=180057 AND `locale`='deDE') OR (`entry`=140214 AND `locale`='deDE') OR (`entry`=220178 AND `locale`='deDE') OR (`entry`=220177 AND `locale`='deDE') OR (`entry`=201708 AND `locale`='deDE') OR (`entry`=219410 AND `locale`='deDE') OR (`entry`=203234 AND `locale`='deDE') OR (`entry`=220361 AND `locale`='deDE') OR (`entry`=3262 AND `locale`='deDE') OR (`entry`=23574 AND `locale`='deDE') OR (`entry`=3258 AND `locale`='deDE') OR (`entry`=23573 AND `locale`='deDE') OR (`entry`=23571 AND `locale`='deDE') OR (`entry`=23572 AND `locale`='deDE') OR (`entry`=3525 AND `locale`='deDE') OR (`entry`=3524 AND `locale`='deDE') OR (`entry`=3523 AND `locale`='deDE') OR (`entry`=3264 AND `locale`='deDE') OR (`entry`=220179 AND `locale`='deDE') OR (`entry`=220353 AND `locale`='deDE') OR (`entry`=3260 AND `locale`='deDE') OR (`entry`=3642 AND `locale`='deDE') OR (`entry`=141972 AND `locale`='deDE') OR (`entry`=180658 AND `locale`='deDE') OR (`entry`=3640 AND `locale`='deDE') OR (`entry`=3256 AND `locale`='deDE') OR (`entry`=23575 AND `locale`='deDE') OR (`entry`=3964 AND `locale`='deDE') OR (`entry`=3963 AND `locale`='deDE') OR (`entry`=20739 AND `locale`='deDE') OR (`entry`=20738 AND `locale`='deDE') OR (`entry`=195119 AND `locale`='deDE') OR (`entry`=202081 AND `locale`='deDE') OR (`entry`=208343 AND `locale`='deDE') OR (`entry`=177794 AND `locale`='deDE') OR (`entry`=3740 AND `locale`='deDE') OR (`entry`=220359 AND `locale`='deDE') OR (`entry`=161752 AND `locale`='deDE') OR (`entry`=61936 AND `locale`='deDE') OR (`entry`=61935 AND `locale`='deDE') OR (`entry`=4141 AND `locale`='deDE') OR (`entry`=4072 AND `locale`='deDE') OR (`entry`=129127 AND `locale`='deDE') OR (`entry`=201291 AND `locale`='deDE') OR (`entry`=208345 AND `locale`='deDE') OR (`entry`=206559 AND `locale`='deDE') OR (`entry`=201293 AND `locale`='deDE') OR (`entry`=126051 AND `locale`='deDE') OR (`entry`=126050 AND `locale`='deDE') OR (`entry`=4166 AND `locale`='deDE') OR (`entry`=106336 AND `locale`='deDE') OR (`entry`=106327 AND `locale`='deDE') OR (`entry`=149038 AND `locale`='deDE') OR (`entry`=143982 AND `locale`='deDE') OR (`entry`=106325 AND `locale`='deDE') OR (`entry`=165742 AND `locale`='deDE') OR (`entry`=165751 AND `locale`='deDE') OR (`entry`=165744 AND `locale`='deDE') OR (`entry`=165743 AND `locale`='deDE') OR (`entry`=165741 AND `locale`='deDE') OR (`entry`=165745 AND `locale`='deDE') OR (`entry`=165749 AND `locale`='deDE') OR (`entry`=165748 AND `locale`='deDE') OR (`entry`=165747 AND `locale`='deDE') OR (`entry`=165746 AND `locale`='deDE') OR (`entry`=165750 AND `locale`='deDE') OR (`entry`=165740 AND `locale`='deDE') OR (`entry`=204611 AND `locale`='deDE') OR (`entry`=202590 AND `locale`='deDE') OR (`entry`=195224 AND `locale`='deDE') OR (`entry`=23570 AND `locale`='deDE') OR (`entry`=58369 AND `locale`='deDE') OR (`entry`=195118 AND `locale`='deDE') OR (`entry`=175708 AND `locale`='deDE') OR (`entry`=203825 AND `locale`='deDE') OR (`entry`=174858 AND `locale`='deDE') OR (`entry`=169969 AND `locale`='deDE') OR (`entry`=169968 AND `locale`='deDE') OR (`entry`=174872 AND `locale`='deDE') OR (`entry`=143399 AND `locale`='deDE') OR (`entry`=143398 AND `locale`='deDE') OR (`entry`=107042 AND `locale`='deDE') OR (`entry`=107041 AND `locale`='deDE') OR (`entry`=107040 AND `locale`='deDE') OR (`entry`=107039 AND `locale`='deDE') OR (`entry`=107037 AND `locale`='deDE') OR (`entry`=106641 AND `locale`='deDE') OR (`entry`=106638 AND `locale`='deDE') OR (`entry`=144125 AND `locale`='deDE') OR (`entry`=3972 AND `locale`='deDE') OR (`entry`=169967 AND `locale`='deDE') OR (`entry`=68865 AND `locale`='deDE') OR (`entry`=21679 AND `locale`='deDE') OR (`entry`=21530 AND `locale`='deDE') OR (`entry`=169966 AND `locale`='deDE') OR (`entry`=21277 AND `locale`='deDE') OR (`entry`=174857 AND `locale`='deDE') OR (`entry`=220357 AND `locale`='deDE') OR (`entry`=106326 AND `locale`='deDE') OR (`entry`=106335 AND `locale`='deDE') OR (`entry`=123328 AND `locale`='deDE') OR (`entry`=195016 AND `locale`='deDE') OR (`entry`=126053 AND `locale`='deDE') OR (`entry`=126052 AND `locale`='deDE') OR (`entry`=195017 AND `locale`='deDE') OR (`entry`=195004 AND `locale`='deDE') OR (`entry`=195003 AND `locale`='deDE') OR (`entry`=195001 AND `locale`='deDE') OR (`entry`=20810 AND `locale`='deDE') OR (`entry`=105176 AND `locale`='deDE') OR (`entry`=3726 AND `locale`='deDE') OR (`entry`=3725 AND `locale`='deDE') OR (`entry`=58388 AND `locale`='deDE') OR (`entry`=58389 AND `locale`='deDE') OR (`entry`=203471 AND `locale`='deDE') OR (`entry`=5622 AND `locale`='deDE') OR (`entry`=5621 AND `locale`='deDE') OR (`entry`=5620 AND `locale`='deDE') OR (`entry`=5619 AND `locale`='deDE') OR (`entry`=195018 AND `locale`='deDE') OR (`entry`=3724 AND `locale`='deDE') OR (`entry`=149036 AND `locale`='deDE') OR (`entry`=220176 AND `locale`='deDE') OR (`entry`=24727 AND `locale`='deDE') OR (`entry`=195202 AND `locale`='deDE') OR (`entry`=123462 AND `locale`='deDE') OR (`entry`=123333 AND `locale`='deDE') OR (`entry`=195211 AND `locale`='deDE') OR (`entry`=160421 AND `locale`='deDE') OR (`entry`=160416 AND `locale`='deDE') OR (`entry`=160415 AND `locale`='deDE') OR (`entry`=160414 AND `locale`='deDE') OR (`entry`=160413 AND `locale`='deDE') OR (`entry`=160411 AND `locale`='deDE') OR (`entry`=195361 AND `locale`='deDE') OR (`entry`=206509 AND `locale`='deDE') OR (`entry`=204678 AND `locale`='deDE') OR (`entry`=202814 AND `locale`='deDE') OR (`entry`=202813 AND `locale`='deDE') OR (`entry`=202812 AND `locale`='deDE') OR (`entry`=202811 AND `locale`='deDE') OR (`entry`=207690 AND `locale`='deDE') OR (`entry`=207689 AND `locale`='deDE') OR (`entry`=207688 AND `locale`='deDE') OR (`entry`=207687 AND `locale`='deDE') OR (`entry`=207686 AND `locale`='deDE') OR (`entry`=206508 AND `locale`='deDE') OR (`entry`=204643 AND `locale`='deDE') OR (`entry`=204699 AND `locale`='deDE') OR (`entry`=207634 AND `locale`='deDE') OR (`entry`=204726 AND `locale`='deDE') OR (`entry`=50805 AND `locale`='deDE') OR (`entry`=204700 AND `locale`='deDE') OR (`entry`=206595 AND `locale`='deDE') OR (`entry`=207635 AND `locale`='deDE') OR (`entry`=50803 AND `locale`='deDE') OR (`entry`=205091 AND `locale`='deDE') OR (`entry`=50804 AND `locale`='deDE') OR (`entry`=206730 AND `locale`='deDE') OR (`entry`=204727 AND `locale`='deDE') OR (`entry`=204689 AND `locale`='deDE') OR (`entry`=3301 AND `locale`='deDE') OR (`entry`=205090 AND `locale`='deDE') OR (`entry`=204688 AND `locale`='deDE') OR (`entry`=204721 AND `locale`='deDE') OR (`entry`=206739 AND `locale`='deDE') OR (`entry`=206738 AND `locale`='deDE') OR (`entry`=206909 AND `locale`='deDE') OR (`entry`=204720 AND `locale`='deDE') OR (`entry`=204719 AND `locale`='deDE') OR (`entry`=206734 AND `locale`='deDE') OR (`entry`=207101 AND `locale`='deDE') OR (`entry`=207149 AND `locale`='deDE') OR (`entry`=207148 AND `locale`='deDE') OR (`entry`=207147 AND `locale`='deDE') OR (`entry`=207100 AND `locale`='deDE') OR (`entry`=207097 AND `locale`='deDE') OR (`entry`=204625 AND `locale`='deDE') OR (`entry`=204722 AND `locale`='deDE') OR (`entry`=207150 AND `locale`='deDE') OR (`entry`=207146 AND `locale`='deDE') OR (`entry`=206733 AND `locale`='deDE') OR (`entry`=204709 AND `locale`='deDE') OR (`entry`=204626 AND `locale`='deDE') OR (`entry`=204622 AND `locale`='deDE') OR (`entry`=206735 AND `locale`='deDE') OR (`entry`=204198 AND `locale`='deDE') OR (`entry`=204194 AND `locale`='deDE') OR (`entry`=206732 AND `locale`='deDE') OR (`entry`=204723 AND `locale`='deDE') OR (`entry`=204724 AND `locale`='deDE') OR (`entry`=204604 AND `locale`='deDE') OR (`entry`=204621 AND `locale`='deDE') OR (`entry`=204725 AND `locale`='deDE') OR (`entry`=204640 AND `locale`='deDE') OR (`entry`=204628 AND `locale`='deDE') OR (`entry`=204605 AND `locale`='deDE') OR (`entry`=237738 AND `locale`='deDE') OR (`entry`=204197 AND `locale`='deDE') OR (`entry`=197257 AND `locale`='deDE') OR (`entry`=206736 AND `locale`='deDE') OR (`entry`=204609 AND `locale`='deDE') OR (`entry`=206741 AND `locale`='deDE') OR (`entry`=206547 AND `locale`='deDE') OR (`entry`=197276 AND `locale`='deDE') OR (`entry`=197278 AND `locale`='deDE') OR (`entry`=197280 AND `locale`='deDE') OR (`entry`=206549 AND `locale`='deDE') OR (`entry`=197279 AND `locale`='deDE') OR (`entry`=202717 AND `locale`='deDE') OR (`entry`=175787 AND `locale`='deDE') OR (`entry`=175788 AND `locale`='deDE') OR (`entry`=204610 AND `locale`='deDE') OR (`entry`=204191 AND `locale`='deDE') OR (`entry`=204192 AND `locale`='deDE') OR (`entry`=204196 AND `locale`='deDE') OR (`entry`=204606 AND `locale`='deDE') OR (`entry`=204608 AND `locale`='deDE') OR (`entry`=204607 AND `locale`='deDE') OR (`entry`=197285 AND `locale`='deDE') OR (`entry`=197322 AND `locale`='deDE') OR (`entry`=206548 AND `locale`='deDE') OR (`entry`=197323 AND `locale`='deDE') OR (`entry`=206546 AND `locale`='deDE') OR (`entry`=204634 AND `locale`='deDE') OR (`entry`=206538 AND `locale`='deDE') OR (`entry`=206545 AND `locale`='deDE') OR (`entry`=206530 AND `locale`='deDE') OR (`entry`=206539 AND `locale`='deDE') OR (`entry`=204205 AND `locale`='deDE') OR (`entry`=197286 AND `locale`='deDE') OR (`entry`=197311 AND `locale`='deDE') OR (`entry`=197307 AND `locale`='deDE') OR (`entry`=204631 AND `locale`='deDE') OR (`entry`=204632 AND `locale`='deDE') OR (`entry`=204633 AND `locale`='deDE') OR (`entry`=204195 AND `locale`='deDE') OR (`entry`=204200 AND `locale`='deDE') OR (`entry`=206729 AND `locale`='deDE') OR (`entry`=180888 AND `locale`='deDE') OR (`entry`=197287 AND `locale`='deDE') OR (`entry`=197284 AND `locale`='deDE') OR (`entry`=197314 AND `locale`='deDE') OR (`entry`=206529 AND `locale`='deDE') OR (`entry`=105576 AND `locale`='deDE') OR (`entry`=197312 AND `locale`='deDE') OR (`entry`=210058 AND `locale`='deDE') OR (`entry`=207646 AND `locale`='deDE') OR (`entry`=206740 AND `locale`='deDE') OR (`entry`=197313 AND `locale`='deDE') OR (`entry`=197315 AND `locale`='deDE') OR (`entry`=197309 AND `locale`='deDE') OR (`entry`=202810 AND `locale`='deDE') OR (`entry`=204203 AND `locale`='deDE') OR (`entry`=204204 AND `locale`='deDE') OR (`entry`=197310 AND `locale`='deDE') OR (`entry`=197207 AND `locale`='deDE') OR (`entry`=197260 AND `locale`='deDE') OR (`entry`=237942 AND `locale`='deDE') OR (`entry`=197259 AND `locale`='deDE') OR (`entry`=204693 AND `locale`='deDE') OR (`entry`=208534 AND `locale`='deDE') OR (`entry`=202798 AND `locale`='deDE') OR (`entry`=204679 AND `locale`='deDE') OR (`entry`=208054 AND `locale`='deDE') OR (`entry`=197261 AND `locale`='deDE') OR (`entry`=204695 AND `locale`='deDE') OR (`entry`=202801 AND `locale`='deDE') OR (`entry`=202800 AND `locale`='deDE') OR (`entry`=202799 AND `locale`='deDE') OR (`entry`=207630 AND `locale`='deDE') OR (`entry`=202815 AND `locale`='deDE') OR (`entry`=202808 AND `locale`='deDE') OR (`entry`=186722 AND `locale`='deDE') OR (`entry`=204941 AND `locale`='deDE') OR (`entry`=207633 AND `locale`='deDE') OR (`entry`=208269 AND `locale`='deDE') OR (`entry`=202802 AND `locale`='deDE') OR (`entry`=208270 AND `locale`='deDE') OR (`entry`=204939 AND `locale`='deDE') OR (`entry`=204696 AND `locale`='deDE') OR (`entry`=204698 AND `locale`='deDE') OR (`entry`=204936 AND `locale`='deDE') OR (`entry`=207632 AND `locale`='deDE') OR (`entry`=204937 AND `locale`='deDE') OR (`entry`=204938 AND `locale`='deDE') OR (`entry`=202809 AND `locale`='deDE') OR (`entry`=204680 AND `locale`='deDE') OR (`entry`=204940 AND `locale`='deDE') OR (`entry`=204681 AND `locale`='deDE') OR (`entry`=202805 AND `locale`='deDE') OR (`entry`=202804 AND `locale`='deDE') OR (`entry`=202803 AND `locale`='deDE') OR (`entry`=206109 AND `locale`='deDE') OR (`entry`=204706 AND `locale`='deDE') OR (`entry`=204694 AND `locale`='deDE') OR (`entry`=202816 AND `locale`='deDE') OR (`entry`=202817 AND `locale`='deDE') OR (`entry`=204708 AND `locale`='deDE') OR (`entry`=207631 AND `locale`='deDE') OR (`entry`=204707 AND `locale`='deDE') OR (`entry`=204613 AND `locale`='deDE') OR (`entry`=204612 AND `locale`='deDE') OR (`entry`=204619 AND `locale`='deDE') OR (`entry`=204614 AND `locale`='deDE') OR (`entry`=204683 AND `locale`='deDE') OR (`entry`=205142 AND `locale`='deDE') OR (`entry`=204682 AND `locale`='deDE') OR (`entry`=195222 AND `locale`='deDE') OR (`entry`=180759 AND `locale`='deDE') OR (`entry`=180761 AND `locale`='deDE') OR (`entry`=205108 AND `locale`='deDE') OR (`entry`=204616 AND `locale`='deDE') OR (`entry`=205109 AND `locale`='deDE') OR (`entry`=207637 AND `locale`='deDE') OR (`entry`=204638 AND `locale`='deDE') OR (`entry`=180775 AND `locale`='deDE') OR (`entry`=207108 AND `locale`='deDE') OR (`entry`=180767 AND `locale`='deDE') OR (`entry`=180778 AND `locale`='deDE') OR (`entry`=204637 AND `locale`='deDE') OR (`entry`=235877 AND `locale`='deDE') OR (`entry`=207638 AND `locale`='deDE') OR (`entry`=204714 AND `locale`='deDE') OR (`entry`=204702 AND `locale`='deDE') OR (`entry`=204618 AND `locale`='deDE') OR (`entry`=204685 AND `locale`='deDE') OR (`entry`=207636 AND `locale`='deDE') OR (`entry`=204713 AND `locale`='deDE') OR (`entry`=204617 AND `locale`='deDE') OR (`entry`=206965 AND `locale`='deDE') OR (`entry`=204684 AND `locale`='deDE') OR (`entry`=204635 AND `locale`='deDE') OR (`entry`=204615 AND `locale`='deDE') OR (`entry`=204701 AND `locale`='deDE') OR (`entry`=180768 AND `locale`='deDE') OR (`entry`=204636 AND `locale`='deDE') OR (`entry`=207640 AND `locale`='deDE') OR (`entry`=204703 AND `locale`='deDE') OR (`entry`=215424 AND `locale`='deDE') OR (`entry`=204705 AND `locale`='deDE') OR (`entry`=204711 AND `locale`='deDE') OR (`entry`=207639 AND `locale`='deDE') OR (`entry`=206603 AND `locale`='deDE') OR (`entry`=204692 AND `locale`='deDE') OR (`entry`=204717 AND `locale`='deDE') OR (`entry`=204718 AND `locale`='deDE') OR (`entry`=206787 AND `locale`='deDE') OR (`entry`=207642 AND `locale`='deDE') OR (`entry`=204716 AND `locale`='deDE') OR (`entry`=204715 AND `locale`='deDE') OR (`entry`=207645 AND `locale`='deDE') OR (`entry`=207414 AND `locale`='deDE') OR (`entry`=195638 AND `locale`='deDE') OR (`entry`=204243 AND `locale`='deDE') OR (`entry`=204629 AND `locale`='deDE') OR (`entry`=204630 AND `locale`='deDE') OR (`entry`=207643 AND `locale`='deDE') OR (`entry`=207644 AND `locale`='deDE') OR (`entry`=204642 AND `locale`='deDE') OR (`entry`=204641 AND `locale`='deDE') OR (`entry`=209867 AND `locale`='deDE') OR (`entry`=204712 AND `locale`='deDE') OR (`entry`=204704 AND `locale`='deDE') OR (`entry`=207641 AND `locale`='deDE') OR (`entry`=207107 AND `locale`='deDE') OR (`entry`=204687 AND `locale`='deDE') OR (`entry`=204686 AND `locale`='deDE') OR (`entry`=204677 AND `locale`='deDE') OR (`entry`=203969 AND `locale`='deDE') OR (`entry`=208266 AND `locale`='deDE') OR (`entry`=207110 AND `locale`='deDE') OR (`entry`=208267 AND `locale`='deDE') OR (`entry`=207065 AND `locale`='deDE') OR (`entry`=208268 AND `locale`='deDE') OR (`entry`=207066 AND `locale`='deDE') OR (`entry`=206608 AND `locale`='deDE') OR (`entry`=206609 AND `locale`='deDE') OR (`entry`=206610 AND `locale`='deDE') OR (`entry`=206116 AND `locale`='deDE') OR (`entry`=207109 AND `locale`='deDE') OR (`entry`=195639 AND `locale`='deDE') OR (`entry`=196837 AND `locale`='deDE') OR (`entry`=204246 AND `locale`='deDE') OR (`entry`=20694 AND `locale`='deDE') OR (`entry`=204190 AND `locale`='deDE') OR (`entry`=31407 AND `locale`='deDE') OR (`entry`=203851 AND `locale`='deDE') OR (`entry`=204183 AND `locale`='deDE') OR (`entry`=204182 AND `locale`='deDE') OR (`entry`=204181 AND `locale`='deDE') OR (`entry`=206575 AND `locale`='deDE') OR (`entry`=206574 AND `locale`='deDE') OR (`entry`=204166 AND `locale`='deDE') OR (`entry`=204187 AND `locale`='deDE') OR (`entry`=204165 AND `locale`='deDE') OR (`entry`=204164 AND `locale`='deDE') OR (`entry`=252071 AND `locale`='deDE') OR (`entry`=203855 AND `locale`='deDE') OR (`entry`=55615 AND `locale`='deDE') OR (`entry`=203854 AND `locale`='deDE') OR (`entry`=203853 AND `locale`='deDE') OR (`entry`=203852 AND `locale`='deDE') OR (`entry`=3290 AND `locale`='deDE') OR (`entry`=204185 AND `locale`='deDE') OR (`entry`=204178 AND `locale`='deDE') OR (`entry`=204657 AND `locale`='deDE') OR (`entry`=204180 AND `locale`='deDE') OR (`entry`=204656 AND `locale`='deDE') OR (`entry`=204179 AND `locale`='deDE') OR (`entry`=204655 AND `locale`='deDE') OR (`entry`=204189 AND `locale`='deDE') OR (`entry`=204186 AND `locale`='deDE') OR (`entry`=204184 AND `locale`='deDE') OR (`entry`=204666 AND `locale`='deDE') OR (`entry`=204665 AND `locale`='deDE') OR (`entry`=204645 AND `locale`='deDE') OR (`entry`=206737 AND `locale`='deDE') OR (`entry`=204188 AND `locale`='deDE') OR (`entry`=204177 AND `locale`='deDE') OR (`entry`=204176 AND `locale`='deDE') OR (`entry`=204658 AND `locale`='deDE') OR (`entry`=204193 AND `locale`='deDE') OR (`entry`=204646 AND `locale`='deDE') OR (`entry`=204175 AND `locale`='deDE') OR (`entry`=223814 AND `locale`='deDE') OR (`entry`=223739 AND `locale`='deDE') OR (`entry`=204174 AND `locale`='deDE') OR (`entry`=204173 AND `locale`='deDE') OR (`entry`=204172 AND `locale`='deDE') OR (`entry`=58595 AND `locale`='deDE') OR (`entry`=204171 AND `locale`='deDE') OR (`entry`=204167 AND `locale`='deDE') OR (`entry`=204170 AND `locale`='deDE') OR (`entry`=204169 AND `locale`='deDE') OR (`entry`=207889 AND `locale`='deDE') OR (`entry`=204168 AND `locale`='deDE') OR (`entry`=196475 AND `locale`='deDE') OR (`entry`=204360 AND `locale`='deDE') OR (`entry`=176499 AND `locale`='deDE') OR (`entry`=31573 AND `locale`='deDE') OR (`entry`=31572 AND `locale`='deDE') OR (`entry`=31575 AND `locale`='deDE') OR (`entry`=143981 AND `locale`='deDE') OR (`entry`=31580 AND `locale`='deDE') OR (`entry`=201905 AND `locale`='deDE') OR (`entry`=202589 AND `locale`='deDE') OR (`entry`=203029 AND `locale`='deDE') OR (`entry`=203031 AND `locale`='deDE') OR (`entry`=203030 AND `locale`='deDE') OR (`entry`=202434 AND `locale`='deDE') OR (`entry`=201968 AND `locale`='deDE') OR (`entry`=205076 AND `locale`='deDE') OR (`entry`=202648 AND `locale`='deDE') OR (`entry`=3236 AND `locale`='deDE') OR (`entry`=202217 AND `locale`='deDE') OR (`entry`=202216 AND `locale`='deDE') OR (`entry`=202215 AND `locale`='deDE') OR (`entry`=202113 AND `locale`='deDE') OR (`entry`=31411 AND `locale`='deDE') OR (`entry`=18077 AND `locale`='deDE') OR (`entry`=207206 AND `locale`='deDE') OR (`entry`=104555 AND `locale`='deDE') OR (`entry`=204135 AND `locale`='deDE') OR (`entry`=203840 AND `locale`='deDE') OR (`entry`=31409 AND `locale`='deDE') OR (`entry`=101748 AND `locale`='deDE') OR (`entry`=31410 AND `locale`='deDE') OR (`entry`=203841 AND `locale`='deDE') OR (`entry`=203846 AND `locale`='deDE') OR (`entry`=203839 AND `locale`='deDE') OR (`entry`=203847 AND `locale`='deDE') OR (`entry`=203848 AND `locale`='deDE') OR (`entry`=203849 AND `locale`='deDE') OR (`entry`=203850 AND `locale`='deDE') OR (`entry`=31574 AND `locale`='deDE') OR (`entry`=18079 AND `locale`='deDE') OR (`entry`=18076 AND `locale`='deDE') OR (`entry`=18075 AND `locale`='deDE') OR (`entry`=221518 AND `locale`='deDE') OR (`entry`=221519 AND `locale`='deDE') OR (`entry`=221517 AND `locale`='deDE') OR (`entry`=221516 AND `locale`='deDE') OR (`entry`=3658 AND `locale`='deDE') OR (`entry`=207207 AND `locale`='deDE') OR (`entry`=31578 AND `locale`='deDE') OR (`entry`=186238 AND `locale`='deDE') OR (`entry`=55250 AND `locale`='deDE') OR (`entry`=31579 AND `locale`='deDE') OR (`entry`=31577 AND `locale`='deDE') OR (`entry`=174859 AND `locale`='deDE') OR (`entry`=3851 AND `locale`='deDE') OR (`entry`=3850 AND `locale`='deDE') OR (`entry`=3849 AND `locale`='deDE') OR (`entry`=3848 AND `locale`='deDE') OR (`entry`=3847 AND `locale`='deDE') OR (`entry`=203857 AND `locale`='deDE') OR (`entry`=203856 AND `locale`='deDE') OR (`entry`=202587 AND `locale`='deDE') OR (`entry`=18083 AND `locale`='deDE') OR (`entry`=12665 AND `locale`='deDE') OR (`entry`=31408 AND `locale`='deDE') OR (`entry`=178089 AND `locale`='deDE') OR (`entry`=178087 AND `locale`='deDE') OR (`entry`=203844 AND `locale`='deDE') OR (`entry`=203843 AND `locale`='deDE') OR (`entry`=203845 AND `locale`='deDE') OR (`entry`=203842 AND `locale`='deDE') OR (`entry`=3084 AND `locale`='deDE') OR (`entry`=202580 AND `locale`='deDE') OR (`entry`=101749 AND `locale`='deDE') OR (`entry`=63674 AND `locale`='deDE') OR (`entry`=3192 AND `locale`='deDE') OR (`entry`=3190 AND `locale`='deDE') OR (`entry`=202618 AND `locale`='deDE') OR (`entry`=202834 AND `locale`='deDE') OR (`entry`=202833 AND `locale`='deDE') OR (`entry`=202839 AND `locale`='deDE') OR (`entry`=202835 AND `locale`='deDE') OR (`entry`=3189 AND `locale`='deDE') OR (`entry`=171938 AND `locale`='deDE') OR (`entry`=175784 AND `locale`='deDE') OR (`entry`=181964 AND `locale`='deDE') OR (`entry`=182016 AND `locale`='deDE') OR (`entry`=185046 AND `locale`='deDE') OR (`entry`=185047 AND `locale`='deDE') OR (`entry`=181806 AND `locale`='deDE') OR (`entry`=182032 AND `locale`='deDE') OR (`entry`=181898 AND `locale`='deDE') OR (`entry`=185041 AND `locale`='deDE') OR (`entry`=182027 AND `locale`='deDE') OR (`entry`=185040 AND `locale`='deDE') OR (`entry`=182532 AND `locale`='deDE') OR (`entry`=182015 AND `locale`='deDE') OR (`entry`=185043 AND `locale`='deDE') OR (`entry`=182019 AND `locale`='deDE') OR (`entry`=182017 AND `locale`='deDE') OR (`entry`=185044 AND `locale`='deDE') OR (`entry`=185042 AND `locale`='deDE') OR (`entry`=185045 AND `locale`='deDE') OR (`entry`=181894 AND `locale`='deDE') OR (`entry`=181780 AND `locale`='deDE') OR (`entry`=184850 AND `locale`='deDE') OR (`entry`=181997 AND `locale`='deDE') OR (`entry`=181993 AND `locale`='deDE') OR (`entry`=181992 AND `locale`='deDE') OR (`entry`=181991 AND `locale`='deDE') OR (`entry`=181998 AND `locale`='deDE') OR (`entry`=181999 AND `locale`='deDE') OR (`entry`=182000 AND `locale`='deDE') OR (`entry`=181996 AND `locale`='deDE') OR (`entry`=182001 AND `locale`='deDE') OR (`entry`=181995 AND `locale`='deDE') OR (`entry`=181994 AND `locale`='deDE') OR (`entry`=75293 AND `locale`='deDE') OR (`entry`=181878 AND `locale`='deDE') OR (`entry`=181699 AND `locale`='deDE') OR (`entry`=2849 AND `locale`='deDE') OR (`entry`=182013 AND `locale`='deDE') OR (`entry`=185036 AND `locale`='deDE') OR (`entry`=185037 AND `locale`='deDE') OR (`entry`=185039 AND `locale`='deDE') OR (`entry`=185038 AND `locale`='deDE') OR (`entry`=185056 AND `locale`='deDE') OR (`entry`=181771 AND `locale`='deDE') OR (`entry`=181770 AND `locale`='deDE') OR (`entry`=181746 AND `locale`='deDE') OR (`entry`=181870 AND `locale`='deDE') OR (`entry`=181988 AND `locale`='deDE') OR (`entry`=181897 AND `locale`='deDE') OR (`entry`=19019 AND `locale`='deDE') OR (`entry`=182216 AND `locale`='deDE') OR (`entry`=3705 AND `locale`='deDE') OR (`entry`=182215 AND `locale`='deDE') OR (`entry`=182218 AND `locale`='deDE') OR (`entry`=182219 AND `locale`='deDE') OR (`entry`=182217 AND `locale`='deDE') OR (`entry`=182220 AND `locale`='deDE') OR (`entry`=182221 AND `locale`='deDE') OR (`entry`=181981 AND `locale`='deDE') OR (`entry`=181756 AND `locale`='deDE') OR (`entry`=181758 AND `locale`='deDE') OR (`entry`=182056 AND `locale`='deDE') OR (`entry`=182055 AND `locale`='deDE') OR (`entry`=181895 AND `locale`='deDE') OR (`entry`=181918 AND `locale`='deDE') OR (`entry`=182950 AND `locale`='deDE') OR (`entry`=181889 AND `locale`='deDE') OR (`entry`=181891 AND `locale`='deDE') OR (`entry`=181887 AND `locale`='deDE') OR (`entry`=181829 AND `locale`='deDE') OR (`entry`=181888 AND `locale`='deDE') OR (`entry`=181851 AND `locale`='deDE') OR (`entry`=182114 AND `locale`='deDE') OR (`entry`=181779 AND `locale`='deDE') OR (`entry`=2846 AND `locale`='deDE') OR (`entry`=181893 AND `locale`='deDE') OR (`entry`=181892 AND `locale`='deDE') OR (`entry`=19017 AND `locale`='deDE') OR (`entry`=3662 AND `locale`='deDE') OR (`entry`=202275 AND `locale`='deDE') OR (`entry`=181854 AND `locale`='deDE') OR (`entry`=207307 AND `locale`='deDE') OR (`entry`=205889 AND `locale`='deDE') OR (`entry`=205888 AND `locale`='deDE') OR (`entry`=205883 AND `locale`='deDE') OR (`entry`=205884 AND `locale`='deDE') OR (`entry`=207964 AND `locale`='deDE') OR (`entry`=207963 AND `locale`='deDE') OR (`entry`=206501 AND `locale`='deDE') OR (`entry`=206040 AND `locale`='deDE') OR (`entry`=206663 AND `locale`='deDE') OR (`entry`=206662 AND `locale`='deDE') OR (`entry`=205251 AND `locale`='deDE') OR (`entry`=205266 AND `locale`='deDE') OR (`entry`=205540 AND `locale`='deDE') OR (`entry`=206958 AND `locale`='deDE') OR (`entry`=205562 AND `locale`='deDE') OR (`entry`=214503 AND `locale`='deDE') OR (`entry`=207281 AND `locale`='deDE') OR (`entry`=206293 AND `locale`='deDE') OR (`entry`=206532 AND `locale`='deDE') OR (`entry`=206395 AND `locale`='deDE') OR (`entry`=206396 AND `locale`='deDE') OR (`entry`=206397 AND `locale`='deDE') OR (`entry`=207335 AND `locale`='deDE') OR (`entry`=207334 AND `locale`='deDE') OR (`entry`=206041 AND `locale`='deDE') OR (`entry`=207365 AND `locale`='deDE') OR (`entry`=206579 AND `locale`='deDE') OR (`entry`=206578 AND `locale`='deDE') OR (`entry`=207596 AND `locale`='deDE') OR (`entry`=205017 AND `locale`='deDE') OR (`entry`=207078 AND `locale`='deDE') OR (`entry`=202942 AND `locale`='deDE') OR (`entry`=202941 AND `locale`='deDE') OR (`entry`=174863 AND `locale`='deDE') OR (`entry`=160409 AND `locale`='deDE') OR (`entry`=160410 AND `locale`='deDE') OR (`entry`=202940 AND `locale`='deDE') OR (`entry`=203837 AND `locale`='deDE') OR (`entry`=202944 AND `locale`='deDE') OR (`entry`=207073 AND `locale`='deDE') OR (`entry`=203056 AND `locale`='deDE') OR (`entry`=203060 AND `locale`='deDE') OR (`entry`=70517 AND `locale`='deDE') OR (`entry`=177226 AND `locale`='deDE') OR (`entry`=177227 AND `locale`='deDE') OR (`entry`=142122 AND `locale`='deDE') OR (`entry`=148504 AND `locale`='deDE') OR (`entry`=144112 AND `locale`='deDE') OR (`entry`=175335 AND `locale`='deDE') OR (`entry`=202441 AND `locale`='deDE') OR (`entry`=203820 AND `locale`='deDE') OR (`entry`=204733 AND `locale`='deDE') OR (`entry`=207004 AND `locale`='deDE') OR (`entry`=207005 AND `locale`='deDE') OR (`entry`=207003 AND `locale`='deDE') OR (`entry`=207002 AND `locale`='deDE') OR (`entry`=178829 AND `locale`='deDE') OR (`entry`=205057 AND `locale`='deDE') OR (`entry`=202187 AND `locale`='deDE') OR (`entry`=202197 AND `locale`='deDE') OR (`entry`=202195 AND `locale`='deDE') OR (`entry`=202196 AND `locale`='deDE') OR (`entry`=203042 AND `locale`='deDE') OR (`entry`=175763 AND `locale`='deDE') OR (`entry`=141838 AND `locale`='deDE') OR (`entry`=195037 AND `locale`='deDE') OR (`entry`=175491 AND `locale`='deDE') OR (`entry`=175490 AND `locale`='deDE') OR (`entry`=35844 AND `locale`='deDE') OR (`entry`=149028 AND `locale`='deDE') OR (`entry`=149027 AND `locale`='deDE') OR (`entry`=149026 AND `locale`='deDE') OR (`entry`=175492 AND `locale`='deDE') OR (`entry`=149029 AND `locale`='deDE') OR (`entry`=202407 AND `locale`='deDE') OR (`entry`=202186 AND `locale`='deDE') OR (`entry`=202474 AND `locale`='deDE') OR (`entry`=142343 AND `locale`='deDE') OR (`entry`=141844 AND `locale`='deDE') OR (`entry`=202420 AND `locale`='deDE') OR (`entry`=144053 AND `locale`='deDE') OR (`entry`=202198 AND `locale`='deDE') OR (`entry`=184006 AND `locale`='deDE') OR (`entry`=186218 AND `locale`='deDE') OR (`entry`=182560 AND `locale`='deDE') OR (`entry`=214613 AND `locale`='deDE') OR (`entry`=214612 AND `locale`='deDE') OR (`entry`=201947 AND `locale`='deDE') OR (`entry`=203824 AND `locale`='deDE') OR (`entry`=148879 AND `locale`='deDE') OR (`entry`=160418 AND `locale`='deDE') OR (`entry`=186897 AND `locale`='deDE') OR (`entry`=186899 AND `locale`='deDE') OR (`entry`=160420 AND `locale`='deDE') OR (`entry`=160419 AND `locale`='deDE') OR (`entry`=186902 AND `locale`='deDE') OR (`entry`=186922 AND `locale`='deDE') OR (`entry`=186900 AND `locale`='deDE') OR (`entry`=186903 AND `locale`='deDE') OR (`entry`=186901 AND `locale`='deDE') OR (`entry`=186904 AND `locale`='deDE') OR (`entry`=186896 AND `locale`='deDE') OR (`entry`=201946 AND `locale`='deDE') OR (`entry`=201944 AND `locale`='deDE') OR (`entry`=201945 AND `locale`='deDE') OR (`entry`=142184 AND `locale`='deDE') OR (`entry`=148880 AND `locale`='deDE') OR (`entry`=203022 AND `locale`='deDE') OR (`entry`=203021 AND `locale`='deDE') OR (`entry`=202263 AND `locale`='deDE') OR (`entry`=203019 AND `locale`='deDE') OR (`entry`=148878 AND `locale`='deDE') OR (`entry`=148877 AND `locale`='deDE') OR (`entry`=179496 AND `locale`='deDE') OR (`entry`=148876 AND `locale`='deDE') OR (`entry`=184532 AND `locale`='deDE') OR (`entry`=184531 AND `locale`='deDE') OR (`entry`=184530 AND `locale`='deDE') OR (`entry`=181095 AND `locale`='deDE') OR (`entry`=203821 AND `locale`='deDE') OR (`entry`=246871 AND `locale`='deDE') OR (`entry`=187912 AND `locale`='deDE') OR (`entry`=214500 AND `locale`='deDE') OR (`entry`=214502 AND `locale`='deDE') OR (`entry`=207512 AND `locale`='deDE') OR (`entry`=202464 AND `locale`='deDE') OR (`entry`=202470 AND `locale`='deDE') OR (`entry`=206743 AND `locale`='deDE') OR (`entry`=206767 AND `locale`='deDE') OR (`entry`=206742 AND `locale`='deDE') OR (`entry`=202264 AND `locale`='deDE') OR (`entry`=164910 AND `locale`='deDE') OR (`entry`=202159 AND `locale`='deDE') OR (`entry`=202135 AND `locale`='deDE') OR (`entry`=174682 AND `locale`='deDE') OR (`entry`=202160 AND `locale`='deDE') OR (`entry`=202158 AND `locale`='deDE') OR (`entry`=204812 AND `locale`='deDE') OR (`entry`=201867 AND `locale`='deDE') OR (`entry`=201866 AND `locale`='deDE') OR (`entry`=266306 AND `locale`='deDE') OR (`entry`=201913 AND `locale`='deDE') OR (`entry`=161521 AND `locale`='deDE') OR (`entry`=148503 AND `locale`='deDE') OR (`entry`=161505 AND `locale`='deDE') OR (`entry`=202247 AND `locale`='deDE') OR (`entry`=161526 AND `locale`='deDE') OR (`entry`=169217 AND `locale`='deDE') OR (`entry`=202082 AND `locale`='deDE') OR (`entry`=142143 AND `locale`='deDE') OR (`entry`=142144 AND `locale`='deDE') OR (`entry`=201979 AND `locale`='deDE') OR (`entry`=201978 AND `locale`='deDE') OR (`entry`=201975 AND `locale`='deDE') OR (`entry`=164659 AND `locale`='deDE') OR (`entry`=164779 AND `locale`='deDE') OR (`entry`=164957 AND `locale`='deDE') OR (`entry`=164778 AND `locale`='deDE') OR (`entry`=201925 AND `locale`='deDE') OR (`entry`=166863 AND `locale`='deDE') OR (`entry`=195022 AND `locale`='deDE') OR (`entry`=164658 AND `locale`='deDE') OR (`entry`=2040 AND `locale`='deDE') OR (`entry`=123848 AND `locale`='deDE') OR (`entry`=202110 AND `locale`='deDE') OR (`entry`=161527 AND `locale`='deDE') OR (`entry`=164660 AND `locale`='deDE') OR (`entry`=164956 AND `locale`='deDE') OR (`entry`=164780 AND `locale`='deDE') OR (`entry`=164661 AND `locale`='deDE') OR (`entry`=164958 AND `locale`='deDE') OR (`entry`=180684 AND `locale`='deDE') OR (`entry`=164781 AND `locale`='deDE') OR (`entry`=209096 AND `locale`='deDE') OR (`entry`=249699 AND `locale`='deDE') OR (`entry`=204119 AND `locale`='deDE') OR (`entry`=204118 AND `locale`='deDE') OR (`entry`=209080 AND `locale`='deDE') OR (`entry`=204878 AND `locale`='deDE') OR (`entry`=209081 AND `locale`='deDE') OR (`entry`=202754 AND `locale`='deDE') OR (`entry`=202844 AND `locale`='deDE') OR (`entry`=202846 AND `locale`='deDE') OR (`entry`=202787 AND `locale`='deDE') OR (`entry`=206613 AND `locale`='deDE') OR (`entry`=202652 AND `locale`='deDE') OR (`entry`=207327 AND `locale`='deDE') OR (`entry`=203147 AND `locale`='deDE') OR (`entry`=204431 AND `locale`='deDE') OR (`entry`=203098 AND `locale`='deDE') OR (`entry`=203096 AND `locale`='deDE') OR (`entry`=203095 AND `locale`='deDE') OR (`entry`=203094 AND `locale`='deDE') OR (`entry`=203093 AND `locale`='deDE') OR (`entry`=203092 AND `locale`='deDE') OR (`entry`=208449 AND `locale`='deDE') OR (`entry`=208444 AND `locale`='deDE') OR (`entry`=208454 AND `locale`='deDE') OR (`entry`=203089 AND `locale`='deDE') OR (`entry`=202732 AND `locale`='deDE') OR (`entry`=203066 AND `locale`='deDE') OR (`entry`=204219 AND `locale`='deDE') OR (`entry`=202660 AND `locale`='deDE') OR (`entry`=203067 AND `locale`='deDE') OR (`entry`=202723 AND `locale`='deDE') OR (`entry`=202712 AND `locale`='deDE') OR (`entry`=202711 AND `locale`='deDE') OR (`entry`=204580 AND `locale`='deDE') OR (`entry`=202697 AND `locale`='deDE') OR (`entry`=202706 AND `locale`='deDE') OR (`entry`=204218 AND `locale`='deDE') OR (`entry`=204217 AND `locale`='deDE') OR (`entry`=204216 AND `locale`='deDE') OR (`entry`=202705 AND `locale`='deDE') OR (`entry`=202749 AND `locale`='deDE') OR (`entry`=202702 AND `locale`='deDE') OR (`entry`=202703 AND `locale`='deDE') OR (`entry`=202367 AND `locale`='deDE') OR (`entry`=202731 AND `locale`='deDE') OR (`entry`=202387 AND `locale`='deDE') OR (`entry`=208791 AND `locale`='deDE') OR (`entry`=208899 AND `locale`='deDE') OR (`entry`=202967 AND `locale`='deDE') OR (`entry`=208573 AND `locale`='deDE') OR (`entry`=208902 AND `locale`='deDE') OR (`entry`=208900 AND `locale`='deDE') OR (`entry`=202748 AND `locale`='deDE') OR (`entry`=194297 AND `locale`='deDE') OR (`entry`=208442 AND `locale`='deDE') OR (`entry`=206683 AND `locale`='deDE') OR (`entry`=208901 AND `locale`='deDE') OR (`entry`=208381 AND `locale`='deDE') OR (`entry`=202996 AND `locale`='deDE') OR (`entry`=204227 AND `locale`='deDE') OR (`entry`=204226 AND `locale`='deDE') OR (`entry`=204127 AND `locale`='deDE') OR (`entry`=204222 AND `locale`='deDE') OR (`entry`=204128 AND `locale`='deDE') OR (`entry`=202701 AND `locale`='deDE') OR (`entry`=203091 AND `locale`='deDE') OR (`entry`=204221 AND `locale`='deDE') OR (`entry`=204220 AND `locale`='deDE') OR (`entry`=204225 AND `locale`='deDE') OR (`entry`=203169 AND `locale`='deDE') OR (`entry`=203310 AND `locale`='deDE') OR (`entry`=203375 AND `locale`='deDE') OR (`entry`=203816 AND `locale`='deDE') OR (`entry`=203815 AND `locale`='deDE') OR (`entry`=203818 AND `locale`='deDE') OR (`entry`=203817 AND `locale`='deDE') OR (`entry`=203819 AND `locale`='deDE') OR (`entry`=202776 AND `locale`='deDE') OR (`entry`=209128 AND `locale`='deDE') OR (`entry`=207734 AND `locale`='deDE') OR (`entry`=203048 AND `locale`='deDE') OR (`entry`=207359 AND `locale`='deDE') OR (`entry`=207518 AND `locale`='deDE') OR (`entry`=203047 AND `locale`='deDE') OR (`entry`=203046 AND `locale`='deDE') OR (`entry`=203383 AND `locale`='deDE') OR (`entry`=202736 AND `locale`='deDE') OR (`entry`=194203 AND `locale`='deDE') OR (`entry`=195071 AND `locale`='deDE') OR (`entry`=207521 AND `locale`='deDE') OR (`entry`=1926 AND `locale`='deDE') OR (`entry`=1923 AND `locale`='deDE') OR (`entry`=195057 AND `locale`='deDE') OR (`entry`=194140 AND `locale`='deDE') OR (`entry`=1921 AND `locale`='deDE') OR (`entry`=194141 AND `locale`='deDE') OR (`entry`=194142 AND `locale`='deDE') OR (`entry`=61929 AND `locale`='deDE') OR (`entry`=194144 AND `locale`='deDE') OR (`entry`=194143 AND `locale`='deDE') OR (`entry`=194150 AND `locale`='deDE') OR (`entry`=194139 AND `locale`='deDE') OR (`entry`=194114 AND `locale`='deDE') OR (`entry`=194106 AND `locale`='deDE') OR (`entry`=16393 AND `locale`='deDE') OR (`entry`=208296 AND `locale`='deDE') OR (`entry`=195007 AND `locale`='deDE') OR (`entry`=17185 AND `locale`='deDE') OR (`entry`=194100 AND `locale`='deDE') OR (`entry`=195058 AND `locale`='deDE') OR (`entry`=195054 AND `locale`='deDE') OR (`entry`=204228 AND `locale`='deDE') OR (`entry`=195078 AND `locale`='deDE') OR (`entry`=175227 AND `locale`='deDE') OR (`entry`=195043 AND `locale`='deDE') OR (`entry`=195080 AND `locale`='deDE') OR (`entry`=195042 AND `locale`='deDE') OR (`entry`=195021 AND `locale`='deDE') OR (`entry`=176190 AND `locale`='deDE') OR (`entry`=175207 AND `locale`='deDE') OR (`entry`=17182 AND `locale`='deDE') OR (`entry`=194211 AND `locale`='deDE') OR (`entry`=194113 AND `locale`='deDE') OR (`entry`=103687 AND `locale`='deDE') OR (`entry`=92490 AND `locale`='deDE') OR (`entry`=92489 AND `locale`='deDE') OR (`entry`=194112 AND `locale`='deDE') OR (`entry`=194145 AND `locale`='deDE') OR (`entry`=194771 AND `locale`='deDE') OR (`entry`=194179 AND `locale`='deDE') OR (`entry`=91737 AND `locale`='deDE') OR (`entry`=204132 AND `locale`='deDE') OR (`entry`=204131 AND `locale`='deDE') OR (`entry`=204774 AND `locale`='deDE') OR (`entry`=17183 AND `locale`='deDE') OR (`entry`=181646 AND `locale`='deDE') OR (`entry`=194204 AND `locale`='deDE') OR (`entry`=194208 AND `locale`='deDE') OR (`entry`=194209 AND `locale`='deDE') OR (`entry`=194714 AND `locale`='deDE') OR (`entry`=194130 AND `locale`='deDE') OR (`entry`=194124 AND `locale`='deDE') OR (`entry`=194133 AND `locale`='deDE') OR (`entry`=194131 AND `locale`='deDE') OR (`entry`=194122 AND `locale`='deDE') OR (`entry`=194787 AND `locale`='deDE') OR (`entry`=176291 AND `locale`='deDE') OR (`entry`=176289 AND `locale`='deDE') OR (`entry`=194105 AND `locale`='deDE') OR (`entry`=194107 AND `locale`='deDE') OR (`entry`=176196 AND `locale`='deDE') OR (`entry`=180655 AND `locale`='deDE') OR (`entry`=194101 AND `locale`='deDE') OR (`entry`=204130 AND `locale`='deDE') OR (`entry`=194104 AND `locale`='deDE') OR (`entry`=194102 AND `locale`='deDE') OR (`entry`=194103 AND `locale`='deDE') OR (`entry`=194088 AND `locale`='deDE') OR (`entry`=13359 AND `locale`='deDE') OR (`entry`=207533 AND `locale`='deDE') OR (`entry`=194090 AND `locale`='deDE') OR (`entry`=194089 AND `locale`='deDE') OR (`entry`=175984 AND `locale`='deDE') OR (`entry`=207091 AND `locale`='deDE') OR (`entry`=207104 AND `locale`='deDE') OR (`entry`=204150 AND `locale`='deDE') OR (`entry`=204149 AND `locale`='deDE') OR (`entry`=204147 AND `locale`='deDE') OR (`entry`=204148 AND `locale`='deDE') OR (`entry`=204146 AND `locale`='deDE') OR (`entry`=204145 AND `locale`='deDE') OR (`entry`=204157 AND `locale`='deDE') OR (`entry`=176091 AND `locale`='deDE') OR (`entry`=204158 AND `locale`='deDE') OR (`entry`=3718 AND `locale`='deDE') OR (`entry`=3717 AND `locale`='deDE') OR (`entry`=3716 AND `locale`='deDE') OR (`entry`=204159 AND `locale`='deDE') OR (`entry`=204160 AND `locale`='deDE') OR (`entry`=206398 AND `locale`='deDE') OR (`entry`=204162 AND `locale`='deDE') OR (`entry`=204161 AND `locale`='deDE') OR (`entry`=207105 AND `locale`='deDE') OR (`entry`=207063 AND `locale`='deDE') OR (`entry`=204735 AND `locale`='deDE') OR (`entry`=206382 AND `locale`='deDE') OR (`entry`=206963 AND `locale`='deDE') OR (`entry`=206852 AND `locale`='deDE') OR (`entry`=177276 AND `locale`='deDE') OR (`entry`=142140 AND `locale`='deDE') OR (`entry`=175076 AND `locale`='deDE') OR (`entry`=175075 AND `locale`='deDE') OR (`entry`=176305 AND `locale`='deDE') OR (`entry`=176161 AND `locale`='deDE') OR (`entry`=176306 AND `locale`='deDE') OR (`entry`=176158 AND `locale`='deDE') OR (`entry`=208190 AND `locale`='deDE') OR (`entry`=177275 AND `locale`='deDE') OR (`entry`=176184 AND `locale`='deDE') OR (`entry`=176160 AND `locale`='deDE') OR (`entry`=176159 AND `locale`='deDE') OR (`entry`=204144 AND `locale`='deDE') OR (`entry`=204156 AND `locale`='deDE') OR (`entry`=204155 AND `locale`='deDE') OR (`entry`=204154 AND `locale`='deDE') OR (`entry`=176326 AND `locale`='deDE') OR (`entry`=206706 AND `locale`='deDE') OR (`entry`=206764 AND `locale`='deDE') OR (`entry`=206381 AND `locale`='deDE') OR (`entry`=204142 AND `locale`='deDE') OR (`entry`=3636 AND `locale`='deDE') OR (`entry`=204140 AND `locale`='deDE') OR (`entry`=204139 AND `locale`='deDE') OR (`entry`=204136 AND `locale`='deDE') OR (`entry`=3637 AND `locale`='deDE') OR (`entry`=204143 AND `locale`='deDE') OR (`entry`=204138 AND `locale`='deDE') OR (`entry`=207476 AND `locale`='deDE') OR (`entry`=204141 AND `locale`='deDE') OR (`entry`=204137 AND `locale`='deDE') OR (`entry`=206585 AND `locale`='deDE') OR (`entry`=204738 AND `locale`='deDE') OR (`entry`=204737 AND `locale`='deDE') OR (`entry`=204736 AND `locale`='deDE') OR (`entry`=204734 AND `locale`='deDE') OR (`entry`=206656 AND `locale`='deDE') OR (`entry`=181928 AND `locale`='deDE') OR (`entry`=181286 AND `locale`='deDE') OR (`entry`=181489 AND `locale`='deDE') OR (`entry`=181488 AND `locale`='deDE') OR (`entry`=181493 AND `locale`='deDE') OR (`entry`=181490 AND `locale`='deDE') OR (`entry`=181954 AND `locale`='deDE') OR (`entry`=181491 AND `locale`='deDE') OR (`entry`=181494 AND `locale`='deDE') OR (`entry`=181283 AND `locale`='deDE') OR (`entry`=181487 AND `locale`='deDE') OR (`entry`=181480 AND `locale`='deDE') OR (`entry`=181483 AND `locale`='deDE') OR (`entry`=181486 AND `locale`='deDE') OR (`entry`=181481 AND `locale`='deDE') OR (`entry`=181485 AND `locale`='deDE') OR (`entry`=181484 AND `locale`='deDE') OR (`entry`=181433 AND `locale`='deDE') OR (`entry`=181396 AND `locale`='deDE') OR (`entry`=202585 AND `locale`='deDE') OR (`entry`=184076 AND `locale`='deDE') OR (`entry`=181370 AND `locale`='deDE') OR (`entry`=181652 AND `locale`='deDE') OR (`entry`=181650 AND `locale`='deDE') OR (`entry`=181651 AND `locale`='deDE') OR (`entry`=182127 AND `locale`='deDE') OR (`entry`=182070 AND `locale`='deDE') OR (`entry`=181933 AND `locale`='deDE') OR (`entry`=181932 AND `locale`='deDE') OR (`entry`=184284 AND `locale`='deDE') OR (`entry`=181790 AND `locale`='deDE') OR (`entry`=182278 AND `locale`='deDE') OR (`entry`=182279 AND `locale`='deDE') OR (`entry`=181805 AND `locale`='deDE') OR (`entry`=182948 AND `locale`='deDE') OR (`entry`=181654 AND `locale`='deDE') OR (`entry`=183275 AND `locale`='deDE') OR (`entry`=183276 AND `locale`='deDE') OR (`entry`=181672 AND `locale`='deDE') OR (`entry`=183274 AND `locale`='deDE') OR (`entry`=183277 AND `locale`='deDE') OR (`entry`=181694 AND `locale`='deDE') OR (`entry`=181616 AND `locale`='deDE') OR (`entry`=181675 AND `locale`='deDE') OR (`entry`=181674 AND `locale`='deDE') OR (`entry`=181920 AND `locale`='deDE') OR (`entry`=181808 AND `locale`='deDE') OR (`entry`=181807 AND `locale`='deDE') OR (`entry`=181696 AND `locale`='deDE') OR (`entry`=181712 AND `locale`='deDE') OR (`entry`=181809 AND `locale`='deDE') OR (`entry`=181716 AND `locale`='deDE') OR (`entry`=181715 AND `locale`='deDE') OR (`entry`=181711 AND `locale`='deDE') OR (`entry`=181697 AND `locale`='deDE') OR (`entry`=181683 AND `locale`='deDE') OR (`entry`=181789 AND `locale`='deDE') OR (`entry`=181788 AND `locale`='deDE') OR (`entry`=181787 AND `locale`='deDE') OR (`entry`=175850 AND `locale`='deDE') OR (`entry`=175846 AND `locale`='deDE') OR (`entry`=175845 AND `locale`='deDE') OR (`entry`=3719 AND `locale`='deDE') OR (`entry`=181773 AND `locale`='deDE') OR (`entry`=181745 AND `locale`='deDE') OR (`entry`=181775 AND `locale`='deDE') OR (`entry`=181748 AND `locale`='deDE') OR (`entry`=175847 AND `locale`='deDE') OR (`entry`=175844 AND `locale`='deDE') OR (`entry`=175849 AND `locale`='deDE') OR (`entry`=175848 AND `locale`='deDE') OR (`entry`=2844 AND `locale`='deDE') OR (`entry`=182012 AND `locale`='deDE') OR (`entry`=181757 AND `locale`='deDE') OR (`entry`=181644 AND `locale`='deDE') OR (`entry`=182949 AND `locale`='deDE') OR (`entry`=183872 AND `locale`='deDE') OR (`entry`=183873 AND `locale`='deDE') OR (`entry`=183874 AND `locale`='deDE') OR (`entry`=183800 AND `locale`='deDE') OR (`entry`=183799 AND `locale`='deDE') OR (`entry`=183859 AND `locale`='deDE') OR (`entry`=182200 AND `locale`='deDE') OR (`entry`=183861 AND `locale`='deDE') OR (`entry`=183858 AND `locale`='deDE') OR (`entry`=180700 AND `locale`='deDE') OR (`entry`=204879 AND `locale`='deDE') OR (`entry`=180038 AND `locale`='deDE') OR (`entry`=183871 AND `locale`='deDE') OR (`entry`=183869 AND `locale`='deDE') OR (`entry`=183868 AND `locale`='deDE') OR (`entry`=183866 AND `locale`='deDE') OR (`entry`=184923 AND `locale`='deDE') OR (`entry`=184922 AND `locale`='deDE') OR (`entry`=183878 AND `locale`='deDE') OR (`entry`=183863 AND `locale`='deDE') OR (`entry`=183793 AND `locale`='deDE') OR (`entry`=183867 AND `locale`='deDE') OR (`entry`=183862 AND `locale`='deDE') OR (`entry`=183870 AND `locale`='deDE') OR (`entry`=183864 AND `locale`='deDE') OR (`entry`=183856 AND `locale`='deDE') OR (`entry`=183855 AND `locale`='deDE') OR (`entry`=183891 AND `locale`='deDE') OR (`entry`=183890 AND `locale`='deDE') OR (`entry`=187291 AND `locale`='deDE') OR (`entry`=183875 AND `locale`='deDE') OR (`entry`=207322 AND `locale`='deDE') OR (`entry`=183860 AND `locale`='deDE') OR (`entry`=183857 AND `locale`='deDE') OR (`entry`=182253 AND `locale`='deDE') OR (`entry`=182251 AND `locale`='deDE') OR (`entry`=181849 AND `locale`='deDE') OR (`entry`=181732 AND `locale`='deDE') OR (`entry`=181731 AND `locale`='deDE') OR (`entry`=181733 AND `locale`='deDE') OR (`entry`=181726 AND `locale`='deDE') OR (`entry`=181730 AND `locale`='deDE') OR (`entry`=181727 AND `locale`='deDE') OR (`entry`=181725 AND `locale`='deDE') OR (`entry`=181729 AND `locale`='deDE') OR (`entry`=181728 AND `locale`='deDE') OR (`entry`=181723 AND `locale`='deDE') OR (`entry`=181724 AND `locale`='deDE') OR (`entry`=181714 AND `locale`='deDE') OR (`entry`=181721 AND `locale`='deDE') OR (`entry`=181722 AND `locale`='deDE') OR (`entry`=181720 AND `locale`='deDE') OR (`entry`=194497 AND `locale`='deDE') OR (`entry`=183889 AND `locale`='deDE') OR (`entry`=182252 AND `locale`='deDE') OR (`entry`=183865 AND `locale`='deDE') OR (`entry`=183893 AND `locale`='deDE') OR (`entry`=207996 AND `locale`='deDE') OR (`entry`=208595 AND `locale`='deDE') OR (`entry`=208591 AND `locale`='deDE') OR (`entry`=20991 AND `locale`='deDE') OR (`entry`=20990 AND `locale`='deDE') OR (`entry`=195074 AND `locale`='deDE') OR (`entry`=20989 AND `locale`='deDE') OR (`entry`=207346 AND `locale`='deDE') OR (`entry`=4406 AND `locale`='deDE') OR (`entry`=19549 AND `locale`='deDE') OR (`entry`=32879 AND `locale`='deDE') OR (`entry`=202592 AND `locale`='deDE') OR (`entry`=7923 AND `locale`='deDE') OR (`entry`=6751 AND `locale`='deDE') OR (`entry`=177272 AND `locale`='deDE') OR (`entry`=142109 AND `locale`='deDE') OR (`entry`=32880 AND `locale`='deDE') OR (`entry`=19550 AND `locale`='deDE') OR (`entry`=1988 AND `locale`='deDE') OR (`entry`=1987 AND `locale`='deDE') OR (`entry`=126158 AND `locale`='deDE') OR (`entry`=1989 AND `locale`='deDE') OR (`entry`=1990 AND `locale`='deDE') OR (`entry`=2000 AND `locale`='deDE') OR (`entry`=1997 AND `locale`='deDE') OR (`entry`=1986 AND `locale`='deDE') OR (`entry`=2739 AND `locale`='deDE') OR (`entry`=2742 AND `locale`='deDE') OR (`entry`=2740 AND `locale`='deDE') OR (`entry`=2741 AND `locale`='deDE') OR (`entry`=19551 AND `locale`='deDE') OR (`entry`=4608 AND `locale`='deDE') OR (`entry`=1999 AND `locale`='deDE') OR (`entry`=1998 AND `locale`='deDE') OR (`entry`=195175 AND `locale`='deDE') OR (`entry`=195174 AND `locale`='deDE') OR (`entry`=195176 AND `locale`='deDE') OR (`entry`=195177 AND `locale`='deDE') OR (`entry`=195178 AND `locale`='deDE') OR (`entry`=1992 AND `locale`='deDE') OR (`entry`=195179 AND `locale`='deDE') OR (`entry`=1991 AND `locale`='deDE') OR (`entry`=208046 AND `locale`='deDE') OR (`entry`=1673 AND `locale`='deDE') OR (`entry`=19552 AND `locale`='deDE') OR (`entry`=6752 AND `locale`='deDE') OR (`entry`=92696 AND `locale`='deDE') OR (`entry`=92544 AND `locale`='deDE') OR (`entry`=92540 AND `locale`='deDE') OR (`entry`=92539 AND `locale`='deDE') OR (`entry`=92527 AND `locale`='deDE') OR (`entry`=92528 AND `locale`='deDE') OR (`entry`=195529 AND `locale`='deDE') OR (`entry`=1685 AND `locale`='deDE') OR (`entry`=195530 AND `locale`='deDE') OR (`entry`=92525 AND `locale`='deDE') OR (`entry`=92526 AND `locale`='deDE') OR (`entry`=91672 AND `locale`='deDE') OR (`entry`=175725 AND `locale`='deDE') OR (`entry`=21581 AND `locale`='deDE') OR (`entry`=92530 AND `locale`='deDE') OR (`entry`=92529 AND `locale`='deDE') OR (`entry`=92535 AND `locale`='deDE') OR (`entry`=92534 AND `locale`='deDE') OR (`entry`=92533 AND `locale`='deDE') OR (`entry`=92536 AND `locale`='deDE') OR (`entry`=148423 AND `locale`='deDE') OR (`entry`=188123 AND `locale`='deDE') OR (`entry`=92700 AND `locale`='deDE') OR (`entry`=92699 AND `locale`='deDE') OR (`entry`=92524 AND `locale`='deDE') OR (`entry`=208825 AND `locale`='deDE') OR (`entry`=92694 AND `locale`='deDE') OR (`entry`=92693 AND `locale`='deDE') OR (`entry`=92541 AND `locale`='deDE') OR (`entry`=92542 AND `locale`='deDE') OR (`entry`=92695 AND `locale`='deDE') OR (`entry`=92543 AND `locale`='deDE') OR (`entry`=195528 AND `locale`='deDE') OR (`entry`=175759 AND `locale`='deDE') OR (`entry`=92552 AND `locale`='deDE') OR (`entry`=95449 AND `locale`='deDE') OR (`entry`=146096 AND `locale`='deDE') OR (`entry`=19877 AND `locale`='deDE') OR (`entry`=175730 AND `locale`='deDE') OR (`entry`=92537 AND `locale`='deDE') OR (`entry`=92538 AND `locale`='deDE') OR (`entry`=175760 AND `locale`='deDE') OR (`entry`=175727 AND `locale`='deDE') OR (`entry`=180870 AND `locale`='deDE') OR (`entry`=91673 AND `locale`='deDE') OR (`entry`=175731 AND `locale`='deDE') OR (`entry`=92551 AND `locale`='deDE') OR (`entry`=92549 AND `locale`='deDE') OR (`entry`=92550 AND `locale`='deDE') OR (`entry`=92548 AND `locale`='deDE') OR (`entry`=92547 AND `locale`='deDE') OR (`entry`=92546 AND `locale`='deDE') OR (`entry`=92545 AND `locale`='deDE') OR (`entry`=92532 AND `locale`='deDE') OR (`entry`=92531 AND `locale`='deDE') OR (`entry`=208830 AND `locale`='deDE') OR (`entry`=208829 AND `locale`='deDE') OR (`entry`=138498 AND `locale`='deDE') OR (`entry`=207995 AND `locale`='deDE') OR (`entry`=208817 AND `locale`='deDE') OR (`entry`=208824 AND `locale`='deDE') OR (`entry`=208818 AND `locale`='deDE') OR (`entry`=208819 AND `locale`='deDE') OR (`entry`=240625 AND `locale`='deDE') OR (`entry`=207321 AND `locale`='deDE') OR (`entry`=142110 AND `locale`='deDE') OR (`entry`=208815 AND `locale`='deDE') OR (`entry`=208814 AND `locale`='deDE') OR (`entry`=187337 AND `locale`='deDE') OR (`entry`=187296 AND `locale`='deDE') OR (`entry`=178147 AND `locale`='deDE') OR (`entry`=203217 AND `locale`='deDE') OR (`entry`=50831 AND `locale`='deDE') OR (`entry`=50830 AND `locale`='deDE') OR (`entry`=2335 AND `locale`='deDE') OR (`entry`=2334 AND `locale`='deDE') OR (`entry`=176787 AND `locale`='deDE') OR (`entry`=92426 AND `locale`='deDE') OR (`entry`=92427 AND `locale`='deDE') OR (`entry`=177288 AND `locale`='deDE') OR (`entry`=203041 AND `locale`='deDE') OR (`entry`=204797 AND `locale`='deDE') OR (`entry`=2336 AND `locale`='deDE') OR (`entry`=206765 AND `locale`='deDE') OR (`entry`=204798 AND `locale`='deDE') OR (`entry`=204804 AND `locale`='deDE') OR (`entry`=178105 AND `locale`='deDE') OR (`entry`=203253 AND `locale`='deDE') OR (`entry`=204800 AND `locale`='deDE') OR (`entry`=203280 AND `locale`='deDE') OR (`entry`=203291 AND `locale`='deDE') OR (`entry`=204805 AND `locale`='deDE') OR (`entry`=203279 AND `locale`='deDE') OR (`entry`=203290 AND `locale`='deDE') OR (`entry`=203235 AND `locale`='deDE') OR (`entry`=203446 AND `locale`='deDE') OR (`entry`=142342 AND `locale`='deDE') OR (`entry`=203413 AND `locale`='deDE') OR (`entry`=194471 AND `locale`='deDE') OR (`entry`=194472 AND `locale`='deDE') OR (`entry`=203166 AND `locale`='deDE') OR (`entry`=203159 AND `locale`='deDE') OR (`entry`=203155 AND `locale`='deDE') OR (`entry`=203165 AND `locale`='deDE') OR (`entry`=203163 AND `locale`='deDE') OR (`entry`=203158 AND `locale`='deDE') OR (`entry`=203160 AND `locale`='deDE') OR (`entry`=203176 AND `locale`='deDE') OR (`entry`=203161 AND `locale`='deDE') OR (`entry`=203239 AND `locale`='deDE') OR (`entry`=203443 AND `locale`='deDE') OR (`entry`=194802 AND `locale`='deDE') OR (`entry`=203429 AND `locale`='deDE') OR (`entry`=194473 AND `locale`='deDE') OR (`entry`=203162 AND `locale`='deDE') OR (`entry`=203154 AND `locale`='deDE') OR (`entry`=203174 AND `locale`='deDE') OR (`entry`=203167 AND `locale`='deDE') OR (`entry`=203148 AND `locale`='deDE') OR (`entry`=203171 AND `locale`='deDE') OR (`entry`=203149 AND `locale`='deDE') OR (`entry`=19602 AND `locale`='deDE') OR (`entry`=203157 AND `locale`='deDE') OR (`entry`=203156 AND `locale`='deDE') OR (`entry`=203164 AND `locale`='deDE') OR (`entry`=195220 AND `locale`='deDE') OR (`entry`=19603 AND `locale`='deDE') OR (`entry`=195217 AND `locale`='deDE') OR (`entry`=194474 AND `locale`='deDE') OR (`entry`=194475 AND `locale`='deDE') OR (`entry`=194477 AND `locale`='deDE') OR (`entry`=194476 AND `locale`='deDE') OR (`entry`=92424 AND `locale`='deDE') OR (`entry`=204799 AND `locale`='deDE') OR (`entry`=203186 AND `locale`='deDE') OR (`entry`=203141 AND `locale`='deDE') OR (`entry`=203146 AND `locale`='deDE') OR (`entry`=203145 AND `locale`='deDE') OR (`entry`=203175 AND `locale`='deDE') OR (`entry`=176506 AND `locale`='deDE') OR (`entry`=176509 AND `locale`='deDE') OR (`entry`=176508 AND `locale`='deDE') OR (`entry`=176507 AND `locale`='deDE') OR (`entry`=143983 AND `locale`='deDE') OR (`entry`=207474 AND `locale`='deDE') OR (`entry`=203214 AND `locale`='deDE') OR (`entry`=203216 AND `locale`='deDE') OR (`entry`=203215 AND `locale`='deDE') OR (`entry`=203431 AND `locale`='deDE') OR (`entry`=203294 AND `locale`='deDE') OR (`entry`=203449 AND `locale`='deDE') OR (`entry`=203448 AND `locale`='deDE') OR (`entry`=202572 AND `locale`='deDE') OR (`entry`=105173 AND `locale`='deDE') OR (`entry`=194438 AND `locale`='deDE') OR (`entry`=203377 AND `locale`='deDE') OR (`entry`=203376 AND `locale`='deDE') OR (`entry`=203374 AND `locale`='deDE') OR (`entry`=207522 AND `locale`='deDE') OR (`entry`=208095 AND `locale`='deDE') OR (`entry`=1621 AND `locale`='deDE') OR (`entry`=3730 AND `locale`='deDE') OR (`entry`=177280 AND `locale`='deDE') OR (`entry`=1731 AND `locale`='deDE') OR (`entry`=203384 AND `locale`='deDE') OR (`entry`=1620 AND `locale`='deDE') OR (`entry`=11899 AND `locale`='deDE') OR (`entry`=1732 AND `locale`='deDE') OR (`entry`=11898 AND `locale`='deDE') OR (`entry`=180549 AND `locale`='deDE') OR (`entry`=180439 AND `locale`='deDE') OR (`entry`=180564 AND `locale`='deDE') OR (`entry`=180559 AND `locale`='deDE') OR (`entry`=180438 AND `locale`='deDE') OR (`entry`=180554 AND `locale`='deDE') OR (`entry`=180436 AND `locale`='deDE') OR (`entry`=180583 AND `locale`='deDE') OR (`entry`=180501 AND `locale`='deDE') OR (`entry`=180529 AND `locale`='deDE') OR (`entry`=180441 AND `locale`='deDE') OR (`entry`=180534 AND `locale`='deDE') OR (`entry`=180440 AND `locale`='deDE') OR (`entry`=180539 AND `locale`='deDE') OR (`entry`=180544 AND `locale`='deDE') OR (`entry`=180503 AND `locale`='deDE') OR (`entry`=180442 AND `locale`='deDE') OR (`entry`=180455 AND `locale`='deDE') OR (`entry`=181635 AND `locale`='deDE') OR (`entry`=207853 AND `locale`='deDE') OR (`entry`=181618 AND `locale`='deDE') OR (`entry`=181603 AND `locale`='deDE') OR (`entry`=207513 AND `locale`='deDE') OR (`entry`=180454 AND `locale`='deDE') OR (`entry`=180474 AND `locale`='deDE') OR (`entry`=180480 AND `locale`='deDE') OR (`entry`=180477 AND `locale`='deDE') OR (`entry`=180475 AND `locale`='deDE') OR (`entry`=180473 AND `locale`='deDE') OR (`entry`=180479 AND `locale`='deDE') OR (`entry`=180478 AND `locale`='deDE') OR (`entry`=180476 AND `locale`='deDE') OR (`entry`=180444 AND `locale`='deDE') OR (`entry`=180443 AND `locale`='deDE') OR (`entry`=179344 AND `locale`='deDE') OR (`entry`=176589 AND `locale`='deDE') OR (`entry`=180915 AND `locale`='deDE') OR (`entry`=180914 AND `locale`='deDE') OR (`entry`=180451 AND `locale`='deDE') OR (`entry`=180448 AND `locale`='deDE') OR (`entry`=180913 AND `locale`='deDE') OR (`entry`=207566 AND `locale`='deDE') OR (`entry`=178553 AND `locale`='deDE') OR (`entry`=179565 AND `locale`='deDE') OR (`entry`=2047 AND `locale`='deDE') OR (`entry`=176583 AND `locale`='deDE') OR (`entry`=181634 AND `locale`='deDE') OR (`entry`=181633 AND `locale`='deDE') OR (`entry`=206915 AND `locale`='deDE') OR (`entry`=180518 AND `locale`='deDE') OR (`entry`=206931 AND `locale`='deDE') OR (`entry`=206917 AND `locale`='deDE') OR (`entry`=206916 AND `locale`='deDE') OR (`entry`=206930 AND `locale`='deDE') OR (`entry`=206582 AND `locale`='deDE') OR (`entry`=206918 AND `locale`='deDE') OR (`entry`=206920 AND `locale`='deDE') OR (`entry`=206919 AND `locale`='deDE') OR (`entry`=181619 AND `locale`='deDE') OR (`entry`=181617 AND `locale`='deDE') OR (`entry`=176584 AND `locale`='deDE') OR (`entry`=206929 AND `locale`='deDE') OR (`entry`=206928 AND `locale`='deDE') OR (`entry`=180466 AND `locale`='deDE') OR (`entry`=180461 AND `locale`='deDE') OR (`entry`=176586 AND `locale`='deDE') OR (`entry`=206927 AND `locale`='deDE') OR (`entry`=206926 AND `locale`='deDE') OR (`entry`=206925 AND `locale`='deDE') OR (`entry`=206924 AND `locale`='deDE') OR (`entry`=206922 AND `locale`='deDE') OR (`entry`=206921 AND `locale`='deDE') OR (`entry`=206581 AND `locale`='deDE') OR (`entry`=206923 AND `locale`='deDE') OR (`entry`=180502 AND `locale`='deDE') OR (`entry`=180456 AND `locale`='deDE') OR (`entry`=206914 AND `locale`='deDE') OR (`entry`=206913 AND `locale`='deDE') OR (`entry`=142142 AND `locale`='deDE') OR (`entry`=153402 AND `locale`='deDE') OR (`entry`=153400 AND `locale`='deDE') OR (`entry`=153399 AND `locale`='deDE') OR (`entry`=180760 AND `locale`='deDE') OR (`entry`=177266 AND `locale`='deDE') OR (`entry`=177265 AND `locale`='deDE') OR (`entry`=3286 AND `locale`='deDE') OR (`entry`=177269 AND `locale`='deDE') OR (`entry`=50469 AND `locale`='deDE') OR (`entry`=50468 AND `locale`='deDE') OR (`entry`=3296 AND `locale`='deDE') OR (`entry`=50449 AND `locale`='deDE') OR (`entry`=50448 AND `locale`='deDE') OR (`entry`=50447 AND `locale`='deDE') OR (`entry`=50446 AND `locale`='deDE') OR (`entry`=50445 AND `locale`='deDE') OR (`entry`=50450 AND `locale`='deDE') OR (`entry`=207323 AND `locale`='deDE') OR (`entry`=187295 AND `locale`='deDE') OR (`entry`=177270 AND `locale`='deDE') OR (`entry`=143985 AND `locale`='deDE') OR (`entry`=3298 AND `locale`='deDE') OR (`entry`=152583 AND `locale`='deDE') OR (`entry`=160426 AND `locale`='deDE') OR (`entry`=3315 AND `locale`='deDE') OR (`entry`=3303 AND `locale`='deDE') OR (`entry`=180776 AND `locale`='deDE') OR (`entry`=177268 AND `locale`='deDE') OR (`entry`=178571 AND `locale`='deDE') OR (`entry`=177267 AND `locale`='deDE') OR (`entry`=180880 AND `locale`='deDE') OR (`entry`=180883 AND `locale`='deDE') OR (`entry`=180882 AND `locale`='deDE') OR (`entry`=180881 AND `locale`='deDE') OR (`entry`=180878 AND `locale`='deDE') OR (`entry`=180873 AND `locale`='deDE') OR (`entry`=180872 AND `locale`='deDE') OR (`entry`=180868 AND `locale`='deDE') OR (`entry`=180869 AND `locale`='deDE') OR (`entry`=74730 AND `locale`='deDE') OR (`entry`=177209 AND `locale`='deDE') OR (`entry`=177207 AND `locale`='deDE') OR (`entry`=177204 AND `locale`='deDE') OR (`entry`=74728 AND `locale`='deDE') OR (`entry`=74727 AND `locale`='deDE') OR (`entry`=177208 AND `locale`='deDE') OR (`entry`=177205 AND `locale`='deDE') OR (`entry`=74729 AND `locale`='deDE') OR (`entry`=2909 AND `locale`='deDE') OR (`entry`=57708 AND `locale`='deDE') OR (`entry`=208867 AND `locale`='deDE') OR (`entry`=208889 AND `locale`='deDE') OR (`entry`=2911 AND `locale`='deDE') OR (`entry`=2904 AND `locale`='deDE') OR (`entry`=3223 AND `locale`='deDE') OR (`entry`=3222 AND `locale`='deDE') OR (`entry`=142118 AND `locale`='deDE') OR (`entry`=23879 AND `locale`='deDE') OR (`entry`=2732 AND `locale`='deDE') OR (`entry`=2913 AND `locale`='deDE') OR (`entry`=2910 AND `locale`='deDE') OR (`entry`=3311 AND `locale`='deDE') OR (`entry`=21680 AND `locale`='deDE') OR (`entry`=186264 AND `locale`='deDE') OR (`entry`=50549 AND `locale`='deDE') OR (`entry`=50548 AND `locale`='deDE') OR (`entry`=50547 AND `locale`='deDE') OR (`entry`=15069 AND `locale`='deDE') OR (`entry`=74445 AND `locale`='deDE') OR (`entry`=143984 AND `locale`='deDE') OR (`entry`=74443 AND `locale`='deDE') OR (`entry`=74444 AND `locale`='deDE') OR (`entry`=74442 AND `locale`='deDE') OR (`entry`=74441 AND `locale`='deDE') OR (`entry`=74440 AND `locale`='deDE') OR (`entry`=15068 AND `locale`='deDE') OR (`entry`=3656 AND `locale`='deDE') OR (`entry`=74439 AND `locale`='deDE') OR (`entry`=18035 AND `locale`='deDE') OR (`entry`=2914 AND `locale`='deDE') OR (`entry`=201942 AND `locale`='deDE') OR (`entry`=201717 AND `locale`='deDE') OR (`entry`=202588 AND `locale`='deDE') OR (`entry`=204597 AND `locale`='deDE') OR (`entry`=204595 AND `locale`='deDE') OR (`entry`=202112 AND `locale`='deDE') OR (`entry`=3076 AND `locale`='deDE') OR (`entry`=204596 AND `locale`='deDE') OR (`entry`=180056 AND `locale`='deDE') OR (`entry`=3310 AND `locale`='deDE') OR (`entry`=208890 AND `locale`='deDE') OR (`entry`=207472 AND `locale`='deDE') OR (`entry`=195142 AND `locale`='deDE') OR (`entry`=208880 AND `locale`='deDE') OR (`entry`=181273 AND `locale`='deDE') OR (`entry`=208875 AND `locale`='deDE') OR (`entry`=208878 AND `locale`='deDE') OR (`entry`=208887 AND `locale`='deDE') OR (`entry`=208888 AND `locale`='deDE') OR (`entry`=204598 AND `locale`='deDE') OR (`entry`=201697 AND `locale`='deDE') OR (`entry`=3308 AND `locale`='deDE') OR (`entry`=2908 AND `locale`='deDE') OR (`entry`=186287 AND `locale`='deDE') OR (`entry`=1618 AND `locale`='deDE') OR (`entry`=50910 AND `locale`='deDE') OR (`entry`=2912 AND `locale`='deDE') OR (`entry`=1617 AND `locale`='deDE') OR (`entry`=142131 AND `locale`='deDE') OR (`entry`=1619 AND `locale`='deDE') OR (`entry`=144063 AND `locale`='deDE') OR (`entry`=142188 AND `locale`='deDE') OR (`entry`=202222 AND `locale`='deDE') OR (`entry`=202085 AND `locale`='deDE') OR (`entry`=142091 AND `locale`='deDE') OR (`entry`=142090 AND `locale`='deDE') OR (`entry`=202606 AND `locale`='deDE') OR (`entry`=142185 AND `locale`='deDE') OR (`entry`=142186 AND `locale`='deDE') OR (`entry`=142187 AND `locale`='deDE') OR (`entry`=206589 AND `locale`='deDE') OR (`entry`=204272 AND `locale`='deDE') OR (`entry`=206590 AND `locale`='deDE') OR (`entry`=142073 AND `locale`='deDE') OR (`entry`=177706 AND `locale`='deDE') OR (`entry`=177444 AND `locale`='deDE') OR (`entry`=202221 AND `locale`='deDE') OR (`entry`=202115 AND `locale`='deDE') OR (`entry`=202114 AND `locale`='deDE') OR (`entry`=180685 AND `locale`='deDE') OR (`entry`=142179 AND `locale`='deDE') OR (`entry`=175732 AND `locale`='deDE') OR (`entry`=204989 AND `locale`='deDE') OR (`entry`=204990 AND `locale`='deDE') OR (`entry`=202107 AND `locale`='deDE') OR (`entry`=164909 AND `locale`='deDE') OR (`entry`=208203 AND `locale`='deDE') OR (`entry`=208322 AND `locale`='deDE') OR (`entry`=208324 AND `locale`='deDE') OR (`entry`=208323 AND `locale`='deDE') OR (`entry`=208201 AND `locale`='deDE') OR (`entry`=208202 AND `locale`='deDE') OR (`entry`=179510 AND `locale`='deDE') OR (`entry`=207535 AND `locale`='deDE') OR (`entry`=206751 AND `locale`='deDE') OR (`entry`=179508 AND `locale`='deDE') OR (`entry`=206753 AND `locale`='deDE') OR (`entry`=206750 AND `locale`='deDE') OR (`entry`=206747 AND `locale`='deDE') OR (`entry`=179507 AND `locale`='deDE') OR (`entry`=206752 AND `locale`='deDE') OR (`entry`=206749 AND `locale`='deDE') OR (`entry`=206748 AND `locale`='deDE') OR (`entry`=178224 AND `locale`='deDE') OR (`entry`=204984 AND `locale`='deDE') OR (`entry`=204983 AND `locale`='deDE') OR (`entry`=204982 AND `locale`='deDE') OR (`entry`=204985 AND `locale`='deDE') OR (`entry`=153126 AND `locale`='deDE') OR (`entry`=50989 AND `locale`='deDE') OR (`entry`=50988 AND `locale`='deDE') OR (`entry`=153125 AND `locale`='deDE') OR (`entry`=153124 AND `locale`='deDE') OR (`entry`=148841 AND `locale`='deDE') OR (`entry`=50987 AND `locale`='deDE') OR (`entry`=50986 AND `locale`='deDE') OR (`entry`=148840 AND `locale`='deDE') OR (`entry`=176278 AND `locale`='deDE') OR (`entry`=176318 AND `locale`='deDE') OR (`entry`=204981 AND `locale`='deDE') OR (`entry`=204980 AND `locale`='deDE') OR (`entry`=184396 AND `locale`='deDE') OR (`entry`=184395 AND `locale`='deDE') OR (`entry`=202164 AND `locale`='deDE') OR (`entry`=176283 AND `locale`='deDE') OR (`entry`=176284 AND `locale`='deDE') OR (`entry`=178826 AND `locale`='deDE') OR (`entry`=176285 AND `locale`='deDE') OR (`entry`=176286 AND `locale`='deDE') OR (`entry`=176281 AND `locale`='deDE') OR (`entry`=176279 AND `locale`='deDE') OR (`entry`=176280 AND `locale`='deDE') OR (`entry`=176288 AND `locale`='deDE') OR (`entry`=176287 AND `locale`='deDE') OR (`entry`=110236 AND `locale`='deDE') OR (`entry`=164953 AND `locale`='deDE') OR (`entry`=110235 AND `locale`='deDE') OR (`entry`=153581 AND `locale`='deDE') OR (`entry`=40301 AND `locale`='deDE') OR (`entry`=206614 AND `locale`='deDE') OR (`entry`=41185 AND `locale`='deDE') OR (`entry`=153580 AND `locale`='deDE') OR (`entry`=142195 AND `locale`='deDE') OR (`entry`=110234 AND `locale`='deDE') OR (`entry`=41186 AND `locale`='deDE') OR (`entry`=41187 AND `locale`='deDE') OR (`entry`=41192 AND `locale`='deDE') OR (`entry`=41191 AND `locale`='deDE') OR (`entry`=41189 AND `locale`='deDE') OR (`entry`=41190 AND `locale`='deDE') OR (`entry`=41188 AND `locale`='deDE') OR (`entry`=203134 AND `locale`='deDE') OR (`entry`=202793 AND `locale`='deDE') OR (`entry`=164954 AND `locale`='deDE') OR (`entry`=153584 AND `locale`='deDE') OR (`entry`=41193 AND `locale`='deDE') OR (`entry`=110231 AND `locale`='deDE') OR (`entry`=110232 AND `locale`='deDE') OR (`entry`=143986 AND `locale`='deDE') OR (`entry`=41194 AND `locale`='deDE') OR (`entry`=110233 AND `locale`='deDE') OR (`entry`=40298 AND `locale`='deDE') OR (`entry`=153579 AND `locale`='deDE') OR (`entry`=153576 AND `locale`='deDE') OR (`entry`=179513 AND `locale`='deDE') OR (`entry`=141611 AND `locale`='deDE') OR (`entry`=143980 AND `locale`='deDE') OR (`entry`=153577 AND `locale`='deDE') OR (`entry`=141610 AND `locale`='deDE') OR (`entry`=144050 AND `locale`='deDE') OR (`entry`=49487 AND `locale`='deDE') OR (`entry`=49486 AND `locale`='deDE') OR (`entry`=49485 AND `locale`='deDE') OR (`entry`=153582 AND `locale`='deDE') OR (`entry`=38030 AND `locale`='deDE') OR (`entry`=38029 AND `locale`='deDE') OR (`entry`=38028 AND `locale`='deDE') OR (`entry`=40197 AND `locale`='deDE') OR (`entry`=141069 AND `locale`='deDE') OR (`entry`=177192 AND `locale`='deDE') OR (`entry`=177189 AND `locale`='deDE') OR (`entry`=177188 AND `locale`='deDE') OR (`entry`=1733 AND `locale`='deDE') OR (`entry`=179469 AND `locale`='deDE') OR (`entry`=2043 AND `locale`='deDE') OR (`entry`=2042 AND `locale`='deDE') OR (`entry`=3722 AND `locale`='deDE') OR (`entry`=19016 AND `locale`='deDE') OR (`entry`=203020 AND `locale`='deDE') OR (`entry`=194620 AND `locale`='deDE') OR (`entry`=194619 AND `locale`='deDE') OR (`entry`=195024 AND `locale`='deDE') OR (`entry`=178247 AND `locale`='deDE') OR (`entry`=206854 AND `locale`='deDE') OR (`entry`=191369 AND `locale`='deDE') OR (`entry`=207534 AND `locale`='deDE') OR (`entry`=195012 AND `locale`='deDE') OR (`entry`=179489 AND `locale`='deDE') OR (`entry`=178186 AND `locale`='deDE') OR (`entry`=178184 AND `locale`='deDE') OR (`entry`=178828 AND `locale`='deDE') OR (`entry`=17783 AND `locale`='deDE') OR (`entry`=17284 AND `locale`='deDE') OR (`entry`=194202 AND `locale`='deDE') OR (`entry`=17282 AND `locale`='deDE') OR (`entry`=93192 AND `locale`='deDE') OR (`entry`=194296 AND `locale`='deDE') OR (`entry`=194651 AND `locale`='deDE') OR (`entry`=20960 AND `locale`='deDE') OR (`entry`=178144 AND `locale`='deDE') OR (`entry`=20961 AND `locale`='deDE') OR (`entry`=194311 AND `locale`='deDE') OR (`entry`=194310 AND `locale`='deDE') OR (`entry`=194309 AND `locale`='deDE') OR (`entry`=20963 AND `locale`='deDE') OR (`entry`=176999 AND `locale`='deDE') OR (`entry`=176998 AND `locale`='deDE') OR (`entry`=20962 AND `locale`='deDE') OR (`entry`=194779 AND `locale`='deDE') OR (`entry`=195079 AND `locale`='deDE') OR (`entry`=3723 AND `locale`='deDE') OR (`entry`=194803 AND `locale`='deDE') OR (`entry`=175104 AND `locale`='deDE') OR (`entry`=206838 AND `locale`='deDE') OR (`entry`=142117 AND `locale`='deDE') OR (`entry`=175179 AND `locale`='deDE') OR (`entry`=194778 AND `locale`='deDE') OR (`entry`=194777 AND `locale`='deDE') OR (`entry`=175180 AND `locale`='deDE') OR (`entry`=175181 AND `locale`='deDE') OR (`entry`=195077 AND `locale`='deDE') OR (`entry`=208790 AND `locale`='deDE') OR (`entry`=206837 AND `locale`='deDE') OR (`entry`=194809 AND `locale`='deDE') OR (`entry`=194175 AND `locale`='deDE') OR (`entry`=3220 AND `locale`='deDE') OR (`entry`=18643 AND `locale`='deDE') OR (`entry`=18645 AND `locale`='deDE') OR (`entry`=18644 AND `locale`='deDE') OR (`entry`=20806 AND `locale`='deDE') OR (`entry`=18596 AND `locale`='deDE') OR (`entry`=195138 AND `locale`='deDE') OR (`entry`=195136 AND `locale`='deDE') OR (`entry`=195135 AND `locale`='deDE') OR (`entry`=19015 AND `locale`='deDE') OR (`entry`=195134 AND `locale`='deDE') OR (`entry`=240617 AND `locale`='deDE') OR (`entry`=19024 AND `locale`='deDE') OR (`entry`=177277 AND `locale`='deDE') OR (`entry`=195005 AND `locale`='deDE') OR (`entry`=195002 AND `locale`='deDE') OR (`entry`=195110 AND `locale`='deDE') OR (`entry`=195111 AND `locale`='deDE') OR (`entry`=178146 AND `locale`='deDE') OR (`entry`=203823 AND `locale`='deDE') OR (`entry`=203822 AND `locale`='deDE') OR (`entry`=204875 AND `locale`='deDE') OR (`entry`=178864 AND `locale`='deDE') OR (`entry`=194566 AND `locale`='deDE') OR (`entry`=194263 AND `locale`='deDE') OR (`entry`=177225 AND `locale`='deDE') OR (`entry`=20724 AND `locale`='deDE') OR (`entry`=20725 AND `locale`='deDE') OR (`entry`=194550 AND `locale`='deDE') OR (`entry`=194549 AND `locale`='deDE') OR (`entry`=194990 AND `locale`='deDE') OR (`entry`=176784 AND `locale`='deDE') OR (`entry`=21004 AND `locale`='deDE') OR (`entry`=194493 AND `locale`='deDE') OR (`entry`=194615 AND `locale`='deDE') OR (`entry`=194616 AND `locale`='deDE') OR (`entry`=177194 AND `locale`='deDE') OR (`entry`=181913 AND `locale`='deDE') OR (`entry`=181681 AND `locale`='deDE') OR (`entry`=177195 AND `locale`='deDE') OR (`entry`=177197 AND `locale`='deDE') OR (`entry`=6288 AND `locale`='deDE') OR (`entry`=178195 AND `locale`='deDE') OR (`entry`=20964 AND `locale`='deDE') OR (`entry`=194465 AND `locale`='deDE') OR (`entry`=194464 AND `locale`='deDE') OR (`entry`=177196 AND `locale`='deDE') OR (`entry`=181916 AND `locale`='deDE') OR (`entry`=180758 AND `locale`='deDE') OR (`entry`=175285 AND `locale`='deDE') OR (`entry`=175286 AND `locale`='deDE') OR (`entry`=175284 AND `locale`='deDE') OR (`entry`=140113 AND `locale`='deDE') OR (`entry`=181690 AND `locale`='deDE') OR (`entry`=194458 AND `locale`='deDE') OR (`entry`=194617 AND `locale`='deDE') OR (`entry`=6292 AND `locale`='deDE') OR (`entry`=6291 AND `locale`='deDE') OR (`entry`=140112 AND `locale`='deDE') OR (`entry`=140111 AND `locale`='deDE') OR (`entry`=140110 AND `locale`='deDE') OR (`entry`=140109 AND `locale`='deDE') OR (`entry`=6290 AND `locale`='deDE') OR (`entry`=6289 AND `locale`='deDE') OR (`entry`=194613 AND `locale`='deDE') OR (`entry`=203460 AND `locale`='deDE') OR (`entry`=194997 AND `locale`='deDE') OR (`entry`=140105 AND `locale`='deDE') OR (`entry`=181686 AND `locale`='deDE') OR (`entry`=194482 AND `locale`='deDE') OR (`entry`=19904 AND `locale`='deDE') OR (`entry`=185321 AND `locale`='deDE') OR (`entry`=174728 AND `locale`='deDE') OR (`entry`=20727 AND `locale`='deDE') OR (`entry`=20359 AND `locale`='deDE') OR (`entry`=214538 AND `locale`='deDE') OR (`entry`=186332 AND `locale`='deDE') OR (`entry`=19905 AND `locale`='deDE') OR (`entry`=19906 AND `locale`='deDE') OR (`entry`=186330 AND `locale`='deDE') OR (`entry`=186329 AND `locale`='deDE') OR (`entry`=186463 AND `locale`='deDE') OR (`entry`=186230 AND `locale`='deDE') OR (`entry`=186232 AND `locale`='deDE') OR (`entry`=186231 AND `locale`='deDE') OR (`entry`=186426 AND `locale`='deDE') OR (`entry`=186233 AND `locale`='deDE') OR (`entry`=2555 AND `locale`='deDE') OR (`entry`=22234 AND `locale`='deDE') OR (`entry`=20992 AND `locale`='deDE') OR (`entry`=21127 AND `locale`='deDE') OR (`entry`=186441 AND `locale`='deDE') OR (`entry`=186450 AND `locale`='deDE') OR (`entry`=186629 AND `locale`='deDE') OR (`entry`=205332 AND `locale`='deDE') OR (`entry`=186631 AND `locale`='deDE') OR (`entry`=186630 AND `locale`='deDE') OR (`entry`=20849 AND `locale`='deDE') OR (`entry`=21459 AND `locale`='deDE') OR (`entry`=21042 AND `locale`='deDE') OR (`entry`=187273 AND `locale`='deDE') OR (`entry`=187272 AND `locale`='deDE') OR (`entry`=20968 AND `locale`='deDE') OR (`entry`=205267 AND `locale`='deDE') OR (`entry`=202596 AND `locale`='deDE') OR (`entry`=20966 AND `locale`='deDE') OR (`entry`=20965 AND `locale`='deDE') OR (`entry`=181626 AND `locale`='deDE') OR (`entry`=186418 AND `locale`='deDE') OR (`entry`=186301 AND `locale`='deDE') OR (`entry`=20985 AND `locale`='deDE') OR (`entry`=187252 AND `locale`='deDE') OR (`entry`=186423 AND `locale`='deDE') OR (`entry`=20982 AND `locale`='deDE') OR (`entry`=266102 AND `locale`='deDE') OR (`entry`=266114 AND `locale`='deDE') OR (`entry`=266107 AND `locale`='deDE') OR (`entry`=266116 AND `locale`='deDE') OR (`entry`=266115 AND `locale`='deDE') OR (`entry`=266113 AND `locale`='deDE') OR (`entry`=266108 AND `locale`='deDE') OR (`entry`=266109 AND `locale`='deDE') OR (`entry`=179085 AND `locale`='deDE') OR (`entry`=202300 AND `locale`='deDE') OR (`entry`=202298 AND `locale`='deDE') OR (`entry`=20829 AND `locale`='deDE') OR (`entry`=207475 AND `locale`='deDE') OR (`entry`=20830 AND `locale`='deDE') OR (`entry`=20831 AND `locale`='deDE') OR (`entry`=186283 AND `locale`='deDE') OR (`entry`=186278 AND `locale`='deDE') OR (`entry`=186273 AND `locale`='deDE') OR (`entry`=179086 AND `locale`='deDE') OR (`entry`=186272 AND `locale`='deDE') OR (`entry`=186243 AND `locale`='deDE') OR (`entry`=208948 AND `locale`='deDE') OR (`entry`=142095 AND `locale`='deDE') OR (`entry`=186322 AND `locale`='deDE') OR (`entry`=186266 AND `locale`='deDE') OR (`entry`=20925 AND `locale`='deDE'); 
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(247045, 'deDE', 'Zen''tabras Moosbett', '', NULL, 23420),
(247042, 'deDE', 'Zen''tabras Vorräte', '', NULL, 23420),
(247040, 'deDE', 'Zen''tabras Zelt', '', NULL, 23420),
(153516, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(207126, 'deDE', 'Waffenstapel der Totenwaldfelle', '', NULL, 23420),
(175932, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175930, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(205008, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(207179, 'deDE', 'Kessel', '', NULL, 23420),
(175929, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175931, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(179864, 'deDE', 'Amboss', '', NULL, 23420),
(179863, 'deDE', 'Schmiede', '', NULL, 23420),
(175927, 'deDE', 'Malyfous'' Katalog', '', NULL, 23420),
(176404, 'deDE', 'Briefkasten', '', NULL, 23420),
(179125, 'deDE', 'Izzys Besitztümer', '', NULL, 23420),
(176749, 'deDE', 'Bank', '', NULL, 23420),
(176748, 'deDE', 'Bank', '', NULL, 23420),
(187299, 'deDE', 'Gildentresor', '', NULL, 23420),
(207301, 'deDE', 'Antike Urne', '', NULL, 23420),
(207300, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(207293, 'deDE', 'Eiswindeier', '', NULL, 23420),
(208192, 'deDE', 'Schneebedeckter Bau', '', NULL, 23420),
(176804, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(176806, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(176805, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176807, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(176808, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(208189, 'deDE', 'Räucherfleisch', '', NULL, 23420),
(175811, 'deDE', 'Herd', '', NULL, 23420),
(207291, 'deDE', 'Echo Drei', '', NULL, 23420),
(207306, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175587, 'deDE', 'Beschädigte Kiste', '', NULL, 23420),
(207424, 'deDE', 'Verlassene Forschungsobjekte', '', NULL, 23420),
(175629, 'deDE', 'Jarons Ladung', '', NULL, 23420),
(175628, 'deDE', 'Jarons Ladung', '', NULL, 23420),
(175586, 'deDE', 'Jarons Wagen', '', NULL, 23420),
(207419, 'deDE', 'Braune Feder', '', NULL, 23420),
(207420, 'deDE', 'Braune Feder', '', NULL, 23420),
(207421, 'deDE', 'Klauentotem einer Eulenbestie', '', NULL, 23420),
(207489, 'deDE', 'Stabile Schatzkiste', '', NULL, 23420),
(207423, 'deDE', 'Lebenstotem einer Eulenbestie', '', NULL, 23420),
(176588, 'deDE', 'Eiskappe', '', NULL, 23420),
(207422, 'deDE', 'Mondtotem einer Eulenbestie', '', NULL, 23420),
(175324, 'deDE', 'Frosthagelsplitter', '', NULL, 23420),
(206684, 'deDE', 'Heuballen', '', NULL, 23420),
(206682, 'deDE', 'Heuballen', '', NULL, 23420),
(207595, 'deDE', 'Fackel', '', NULL, 23420),
(207594, 'deDE', 'Fackel', '', NULL, 23420),
(206591, 'deDE', 'Rüstung der Neferset', '', NULL, 23420),
(205564, 'deDE', 'Versammlungsstein', '', NULL, 23420),
(207710, 'deDE', 'Fokus alter Waffen der Tol''vir', '', NULL, 23420),
(206393, 'deDE', 'Uralte Tol''virwaffen', '', NULL, 23420),
(202615, 'deDE', 'Sandfalle', '', NULL, 23420),
(207969, 'deDE', 'Doodad_Uldum_Titan_Micro_Sun_Beam03', '', NULL, 23420),
(207962, 'deDE', 'Doodad_Uldum_Micro_Laserbeams01', '', NULL, 23420),
(207600, 'deDE', 'Feuergrube', '', NULL, 23420),
(207599, 'deDE', 'Amboss', '', NULL, 23420),
(207598, 'deDE', 'Amboss', '', NULL, 23420),
(214507, 'deDE', 'Instance Portal (Party + Heroic)', '', NULL, 23420),
(206689, 'deDE', 'Fackel', '', NULL, 23420),
(206949, 'deDE', 'Käfig', '', NULL, 23420),
(206954, 'deDE', 'Käfig', '', NULL, 23420),
(206953, 'deDE', 'Käfig', '', NULL, 23420),
(206588, 'deDE', 'Rüstung der Neferset', '', NULL, 23420),
(206112, 'deDE', 'Krokodileier', '', NULL, 23420),
(207399, 'deDE', 'Fackel', '', NULL, 23420),
(207398, 'deDE', 'Fackel', '', NULL, 23420),
(206551, 'deDE', 'Linse der Sonne', '', NULL, 23420),
(206075, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206531, 'deDE', 'Truhe des Obelisken der Sonne', '', NULL, 23420),
(206964, 'deDE', 'Obelisk des Sonnenstrahls', '', NULL, 23420),
(207698, 'deDE', 'Titanenwächter', '', NULL, 23420),
(207069, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206563, 'deDE', 'Gut erhaltener Götze', '', NULL, 23420),
(205874, 'deDE', 'Sandbedeckte Hieroglyphen', '', NULL, 23420),
(205824, 'deDE', 'Heiliges Gefäß', '', NULL, 23420),
(205385, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(205473, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(206895, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206898, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206900, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206891, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206911, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206910, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206896, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(207124, 'deDE', 'Geplünderter Beutesack', '', NULL, 23420),
(206897, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(180453, 'deDE', 'Glyphenverzierter Kristall des Regalschwarms', '', NULL, 23420),
(208242, 'deDE', 'Briefkasten', '', NULL, 23420),
(205527, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(207604, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207603, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206940, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(207280, 'deDE', 'Titanenschalttafel', '', NULL, 23420),
(206892, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206904, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206893, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206561, 'deDE', 'Tempelkristall von Uldum', '', NULL, 23420),
(206939, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206933, 'deDE', 'Fackel', '', NULL, 23420),
(206879, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(206878, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(206934, 'deDE', 'Fackel', '', NULL, 23420),
(206903, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206902, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180904, 'deDE', 'Uralte Tür', '', NULL, 23420),
(180899, 'deDE', 'AQROOT', '', NULL, 23420),
(180898, 'deDE', 'AQRUNE', '', NULL, 23420),
(206901, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206932, 'deDE', 'Fackel', '', NULL, 23420),
(206894, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206912, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(207130, 'deDE', 'Titanenwächter', '', NULL, 23420),
(207151, 'deDE', 'Titanenwächter', '', NULL, 23420),
(207166, 'deDE', 'Titanenschalttafel', '', NULL, 23420),
(206390, 'deDE', 'Gleiterei', '', NULL, 23420),
(208241, 'deDE', 'Briefkasten', '', NULL, 23420),
(202779, 'deDE', 'Ein Schwarm Schwarzbauchmatschflosser', '', NULL, 23420),
(205539, 'deDE', 'Gepeinigter Grabräuber', '', NULL, 23420),
(195203, 'deDE', 'Munitionsvorrat von Theramore', '', NULL, 23420),
(141863, 'deDE', 'Kanone', '', NULL, 23420),
(141862, 'deDE', 'Kanone', '', NULL, 23420),
(142198, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204997, 'deDE', 'Amboss', '', NULL, 23420),
(201696, 'deDE', 'Schmiede', '', NULL, 23420),
(142197, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(202335, 'deDE', 'Paxtons Feldkanone', '', NULL, 23420),
(202325, 'deDE', 'Schrauben und Zahnräder', '', NULL, 23420),
(202324, 'deDE', 'Zerbrochene Flaschen', '', NULL, 23420),
(202323, 'deDE', 'Kanonier Whessans Kegelpreise', '', NULL, 23420),
(202322, 'deDE', 'Musketenkugeln', '', NULL, 23420),
(202321, 'deDE', 'Besteckset', '', NULL, 23420),
(202320, 'deDE', 'Kiste mit Nägeln', '', NULL, 23420),
(48516, 'deDE', 'Hauptkontrollprogramm', '', NULL, 23420),
(201703, 'deDE', 'Frischer Kadaver', '', NULL, 23420),
(202405, 'deDE', 'Vorratskiste aus Nordwacht', '', NULL, 23420),
(3727, 'deDE', 'Maguskönigskraut', '', NULL, 23420),
(3729, 'deDE', 'Wilddornrose', '', NULL, 23420),
(23577, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(201395, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201394, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201393, 'deDE', 'Schmiede', '', NULL, 23420),
(201392, 'deDE', 'Amboss', '', NULL, 23420),
(201391, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201701, 'deDE', 'Konfiszierte Waffen', '', NULL, 23420),
(201399, 'deDE', 'Kessel', '', NULL, 23420),
(202176, 'deDE', 'Briefkasten', '', NULL, 23420),
(202175, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(202174, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(202173, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(202172, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(201724, 'deDE', 'Borstennackenkäfig', '', NULL, 23420),
(201602, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(182255, 'deDE', 'Wyvernhorst', '', NULL, 23420),
(201781, 'deDE', 'Feldbanner', '', NULL, 23420),
(195640, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(177525, 'deDE', 'Mondkinstein', '', NULL, 23420),
(201792, 'deDE', 'Belagerungsmaschine der Nordwacht', '', NULL, 23420),
(195502, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195505, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(195503, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(195504, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202467, 'deDE', 'Geheiminformationen aus Taurajo', '', NULL, 23420),
(204994, 'deDE', 'Amboss', '', NULL, 23420),
(204993, 'deDE', 'Schmiede', '', NULL, 23420),
(201588, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204999, 'deDE', 'Briefkasten', '', NULL, 23420),
(201587, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(202597, 'deDE', 'Kawummofass', '', NULL, 23420),
(202598, 'deDE', 'Großer fieser Auslöser', '', NULL, 23420),
(201723, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(202535, 'deDE', 'Doppelzopfs Banner', '', NULL, 23420),
(201924, 'deDE', 'Eberschädel', '', NULL, 23420),
(201725, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(3266, 'deDE', 'Kessel', '', NULL, 23420),
(202542, 'deDE', 'Doppelzopfs Werkzeuge', '', NULL, 23420),
(141813, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201963, 'deDE', 'Zwergische Artilleriezahnräder', '', NULL, 23420),
(201971, 'deDE', 'Plan der Burg Bael''dun', '', NULL, 23420),
(202533, 'deDE', 'Artefakt aus Bael Modan', '', NULL, 23420),
(201879, 'deDE', 'Kriegsnarbenbanner - Horde', '', NULL, 23420),
(201877, 'deDE', 'Kriegsnarbenflaggenmast', '', NULL, 23420),
(202478, 'deDE', 'Belagerungsmaschinenschrott', '', NULL, 23420),
(202477, 'deDE', 'Belagerungsmaschinenschrott', '', NULL, 23420),
(201810, 'deDE', 'Feldbanner', '', NULL, 23420),
(202452, 'deDE', 'Winziger Tisch', '', NULL, 23420),
(91706, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201743, 'deDE', 'Feldbanner', '', NULL, 23420),
(204992, 'deDE', 'Kessel', '', NULL, 23420),
(204991, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201595, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201738, 'deDE', 'Knospende Blüte', '', NULL, 23420),
(201737, 'deDE', 'Knospende Blüte', '', NULL, 23420),
(204998, 'deDE', 'Amboss', '', NULL, 23420),
(202595, 'deDE', 'Höhleneinsturz von Frazzelcraz', '', NULL, 23420),
(203814, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204996, 'deDE', 'Schmiede', '', NULL, 23420),
(202476, 'deDE', 'Hawthornes Tisch', '', NULL, 23420),
(204995, 'deDE', 'Amboss', '', NULL, 23420),
(200302, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(200303, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(246267, 'deDE', 'Jadefeder', '', NULL, 23420),
(180876, 'deDE', 'Elunes Segen FALLE FREUNDLICH', '', NULL, 23420),
(180851, 'deDE', 'Feuerwerksrakete, Typ 1 Rot', '', NULL, 23420),
(246960, 'deDE', 'Fischhaufen', '', NULL, 23420),
(246683, 'deDE', 'Fischhaufen', '', NULL, 23420),
(178984, 'deDE', 'Schmuckstückaura', '', NULL, 23420),
(177785, 'deDE', 'Schmuckstückbehälter', '', NULL, 23420),
(177273, 'deDE', 'Mondbrunnen', '', NULL, 23420),
(218722, 'deDE', 'Emi Brunet', '', NULL, 23420),
(195219, 'deDE', 'Briefkasten', '', NULL, 23420),
(180765, 'deDE', 'Laterne', '', NULL, 23420),
(195218, 'deDE', 'Briefkasten', '', NULL, 23420),
(180910, 'deDE', 'Sternensangs Rolle', '', NULL, 23420),
(180879, 'deDE', 'ElvenWoodenTable', '', NULL, 23420),
(180909, 'deDE', 'Roter Glücksumschlag', '', NULL, 23420),
(190942, 'deDE', 'Schwarzes Tor', '', NULL, 23420),
(185518, 'deDE', 'Leuchten des Traumfängers', '', NULL, 23420),
(185503, 'deDE', 'Hügeltruhe', '', NULL, 23420),
(185495, 'deDE', 'Blume des Smaragdgrünen Traums', '', NULL, 23420),
(185504, 'deDE', 'Traumfänger der Mondlichtung', '', NULL, 23420),
(185494, 'deDE', 'Blume des Smaragdgrünen Traums', '', NULL, 23420),
(185492, 'deDE', 'Blume des Smaragdgrünen Traums', '', NULL, 23420),
(180875, 'deDE', 'Boss Fight Altar', '', NULL, 23420),
(180874, 'deDE', 'Zünder für Raketenbündel', '', NULL, 23420),
(180340, 'deDE', 'Kerze 03', '', NULL, 23420),
(180339, 'deDE', 'Kerze 02', '', NULL, 23420),
(180338, 'deDE', 'Kerze 01', '', NULL, 23420),
(247053, 'deDE', 'Teppich', '', NULL, 23420),
(247052, 'deDE', 'Kissen', '', NULL, 23420),
(175765, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195693, 'deDE', 'Vaterstein', '', NULL, 23420),
(175764, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204746, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(175767, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175766, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(203805, 'deDE', 'Stuhl', '', NULL, 23420),
(239240, 'deDE', 'Ofen', '', NULL, 23420),
(207276, 'deDE', 'Stuhl', '', NULL, 23420),
(207275, 'deDE', 'Stuhl', '', NULL, 23420),
(207274, 'deDE', 'Stuhl', '', NULL, 23420),
(207273, 'deDE', 'Stuhl', '', NULL, 23420),
(207272, 'deDE', 'Stuhl', '', NULL, 23420),
(207271, 'deDE', 'Stuhl', '', NULL, 23420),
(207270, 'deDE', 'Stuhl', '', NULL, 23420),
(199332, 'deDE', 'Tafel der Hochgeborenen', '', NULL, 23420),
(200298, 'deDE', 'Herz von Arkkoroc', '', NULL, 23420),
(207220, 'deDE', 'Briefkasten', '', NULL, 23420),
(207269, 'deDE', 'Stuhl', '', NULL, 23420),
(207268, 'deDE', 'Stuhl', '', NULL, 23420),
(207267, 'deDE', 'Stuhl', '', NULL, 23420),
(207266, 'deDE', 'Stuhl', '', NULL, 23420),
(207265, 'deDE', 'Stuhl', '', NULL, 23420),
(207264, 'deDE', 'Stuhl', '', NULL, 23420),
(204413, 'deDE', 'Amboss', '', NULL, 23420),
(204412, 'deDE', 'Schmiede', '', NULL, 23420),
(204121, 'deDE', 'Amboss', '', NULL, 23420),
(204050, 'deDE', 'Blaupausen der Feindbrecher', '', NULL, 23420),
(204122, 'deDE', 'Schmiede', '', NULL, 23420),
(204232, 'deDE', 'Grill', '', NULL, 23420),
(204231, 'deDE', 'Amboss', '', NULL, 23420),
(204230, 'deDE', 'Schmiede', '', NULL, 23420),
(204229, 'deDE', 'Briefkasten', '', NULL, 23420),
(197332, 'deDE', 'Käfig des Schattenhammers', '', NULL, 23420),
(195345, 'deDE', 'Frostrune', '', NULL, 23420),
(195651, 'deDE', 'Essenz des Eises', '', NULL, 23420),
(195584, 'deDE', 'Gestohlenes Handbuch', '', NULL, 23420),
(195583, 'deDE', 'Gestohlenes Handbuch', '', NULL, 23420),
(195575, 'deDE', 'Gestohlenes Handbuch', '', NULL, 23420),
(195686, 'deDE', 'Kahpheepflanze', '', NULL, 23420),
(195711, 'deDE', 'Stein der brühenden Wasserlords', '', NULL, 23420),
(196461, 'deDE', 'Schattenportalstein', '', NULL, 23420),
(196460, 'deDE', 'Feuerportalstein', '', NULL, 23420),
(196459, 'deDE', 'Frostportalstein', '', NULL, 23420),
(196407, 'deDE', 'Latente Energie', '', NULL, 23420),
(195367, 'deDE', 'Latente Energie', '', NULL, 23420),
(204271, 'deDE', 'Grill', '', NULL, 23420),
(204270, 'deDE', 'Grill', '', NULL, 23420),
(204268, 'deDE', 'Amboss', '', NULL, 23420),
(204238, 'deDE', 'Briefkasten', '', NULL, 23420),
(204269, 'deDE', 'Schmiede', '', NULL, 23420),
(195576, 'deDE', 'Gestohlenes Handbuch', '', NULL, 23420),
(196836, 'deDE', 'Haggrums Rauchgrube', '', NULL, 23420),
(204775, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(143397, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195577, 'deDE', 'Gestohlenes Handbuch', '', NULL, 23420),
(195365, 'deDE', 'Energieleitung', '', NULL, 23420),
(195582, 'deDE', 'Gestohlenes Handbuch', '', NULL, 23420),
(20898, 'deDE', 'Brennender Knochenhaufen', '', NULL, 23420),
(195587, 'deDE', 'Lebendiger Zornthymian', '', NULL, 23420),
(142123, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(196833, 'deDE', 'Unterer Seherstein', '', NULL, 23420),
(142124, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(196832, 'deDE', 'Oberer Seherstein', '', NULL, 23420),
(199330, 'deDE', 'Tafel der Hochgeborenen', '', NULL, 23420),
(201572, 'deDE', 'BGG', '', NULL, 23420),
(196474, 'deDE', 'Ehrgeizweite', '', NULL, 23420),
(195681, 'deDE', 'Schwindender Verstand', '', NULL, 23420),
(204786, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(196835, 'deDE', 'Wichtige Dokumente', '', NULL, 23420),
(88496, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204785, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(207425, 'deDE', 'Portal nach Bärenkopf', '', NULL, 23420),
(204784, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201597, 'deDE', 'Berühren um zu Beginnen', '', NULL, 23420),
(88497, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(196834, 'deDE', 'Strauchwurzelgebräu', '', NULL, 23420),
(205082, 'deDE', 'Kleines Feuer', '', NULL, 23420),
(20918, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201302, 'deDE', 'Gebietsanspruch', '', NULL, 23420),
(204245, 'deDE', 'Doodad_Goblin_elevator01', '', NULL, 23420),
(150086, 'deDE', 'Behelfsmäßige Heli-Plattform', '', NULL, 23420),
(201579, 'deDE', 'Schlüsselsteinscherbe', '', NULL, 23420),
(195642, 'deDE', 'Kraftstein der Naga', '', NULL, 23420),
(195513, 'deDE', 'Uralter Steinkasten', '', NULL, 23420),
(250419, 'deDE', 'Fluxeindämmungsventilhebel', '', NULL, 23420),
(195683, 'deDE', 'Reaktorkontrollpult', '', NULL, 23420),
(195676, 'deDE', 'Geheimer Laborquäkkasten', '', NULL, 23420),
(250420, 'deDE', 'Hydro-Synchro-Verteiler', '', NULL, 23420),
(195461, 'deDE', 'Feldmesserpfahl', '', NULL, 23420),
(250418, 'deDE', 'Thoriumverteilerhebel', '', NULL, 23420),
(142121, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204237, 'deDE', 'Grill', '', NULL, 23420),
(204236, 'deDE', 'Grill', '', NULL, 23420),
(204234, 'deDE', 'Schmiede', '', NULL, 23420),
(204235, 'deDE', 'Amboss', '', NULL, 23420),
(201397, 'deDE', 'Notlukenauslöser', '', NULL, 23420),
(250421, 'deDE', 'Antimateriespeicherlader', '', NULL, 23420),
(250416, 'deDE', 'Tagebuch eines Praktikanten', '', NULL, 23420),
(196487, 'deDE', 'Boden des Käfigs für Versuchsobjekte', '', NULL, 23420),
(196486, 'deDE', 'Käfig für Versuchsobjekte', '', NULL, 23420),
(204252, 'deDE', 'Briefkasten', '', NULL, 23420),
(204251, 'deDE', 'Amboss', '', NULL, 23420),
(204250, 'deDE', 'Schmiede', '', NULL, 23420),
(204249, 'deDE', 'Grill', '', NULL, 23420),
(204248, 'deDE', 'Grill', '', NULL, 23420),
(195360, 'deDE', 'Landmine', '', NULL, 23420),
(199331, 'deDE', 'Tafel der Hochgeborenen', '', NULL, 23420),
(199329, 'deDE', 'Tafel der Hochgeborenen', '', NULL, 23420),
(199333, 'deDE', 'Runenstein der Grollflossen', '', NULL, 23420),
(204244, 'deDE', 'Doodad_Goblin_elevator01', '', NULL, 23420),
(201386, 'deDE', 'Strandgrundstück', '', NULL, 23420),
(204233, 'deDE', 'Briefkasten', '', NULL, 23420),
(201400, 'deDE', 'Tourismusbroschüre des Geheimlabors', '', NULL, 23420),
(152621, 'deDE', 'Azsharitformation', '', NULL, 23420),
(195623, 'deDE', 'Goblinmörsergranate', '', NULL, 23420),
(141870, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(196462, 'deDE', 'Abführmittel in Riesengröße', '', NULL, 23420),
(152620, 'deDE', 'Azsharitformation', '', NULL, 23420),
(152631, 'deDE', 'Azsharitformation', '', NULL, 23420),
(152622, 'deDE', 'Azsharitformation', '', NULL, 23420),
(195455, 'deDE', 'Uralter Schutthaufen', '', NULL, 23420),
(195435, 'deDE', 'Waffenkiste', '', NULL, 23420),
(195431, 'deDE', 'Hauptquartierfunkgerät', '', NULL, 23420),
(204783, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(197345, 'deDE', 'Detonationsladung 2', '', NULL, 23420),
(197346, 'deDE', 'Detonationsladung 3', '', NULL, 23420),
(197344, 'deDE', 'Detonationsladung 1', '', NULL, 23420),
(196870, 'deDE', 'Goblindetonator', '', NULL, 23420),
(184856, 'deDE', 'Kasten C', '', NULL, 23420),
(177278, 'deDE', 'Mondbrunnen', '', NULL, 23420),
(176792, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201401, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(207404, 'deDE', 'Donais'' Gold', '', NULL, 23420),
(195448, 'deDE', 'Eisenblock', '', NULL, 23420),
(195447, 'deDE', 'Eisenstapel', '', NULL, 23420),
(195146, 'deDE', 'Donnerkopfgipfel', '', NULL, 23420),
(3743, 'deDE', 'Felsspaltenpflanze', '', NULL, 23420),
(3737, 'deDE', 'Blubbernder Felsspalt', '', NULL, 23420),
(24725, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(152618, 'deDE', 'Beute der Kolkar', '', NULL, 23420),
(24724, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(24728, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3246, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(24729, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3249, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(24726, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(195311, 'deDE', 'Sonnenschuppenbrutstätte', '', NULL, 23420),
(24723, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(195240, 'deDE', 'Geheiminformationen der Zentauren', '', NULL, 23420),
(18930, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195206, 'deDE', 'Seltsame Piratenlandmarke', '', NULL, 23420),
(3252, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195187, 'deDE', 'Handschellen von Theramore', '', NULL, 23420),
(3251, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195205, 'deDE', 'Hängender Piratenkopf', '', NULL, 23420),
(178884, 'deDE', 'Versammlungsstein', '', NULL, 23420),
(180057, 'deDE', 'Blubbernder Felsspalt', '', NULL, 23420),
(140214, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(220178, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(220177, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(201708, 'deDE', 'Zusammengerolltes Seil', '', NULL, 23420),
(219410, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(203234, 'deDE', 'Höhleneinsturz der Ausufernden Tiefen', '', NULL, 23420),
(220361, 'deDE', 'Holz der Kor''kron', '', NULL, 23420),
(3262, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(23574, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3258, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(23573, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(23571, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(23572, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3525, 'deDE', 'Der Altar des Feuers', '', NULL, 23420),
(3524, 'deDE', 'Die Dämonensaat', '', NULL, 23420),
(3523, 'deDE', 'TEST Circle of Flame', '', NULL, 23420),
(3264, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(220179, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(220353, 'deDE', 'Kor''kron-Fleischgestell', '', NULL, 23420),
(3260, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(3642, 'deDE', 'Beute der Kolkar', '', NULL, 23420),
(141972, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(180658, 'deDE', 'Ein Schwarm Deviatfische', '', NULL, 23420),
(3640, 'deDE', 'Potenter Pilz', '', NULL, 23420),
(3256, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(23575, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3964, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(3963, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(20739, 'deDE', 'Amboss', '', NULL, 23420),
(20738, 'deDE', 'Schmiede', '', NULL, 23420),
(195119, 'deDE', 'Einfaches Denkmal', '', NULL, 23420),
(202081, 'deDE', 'Takks Nest', '', NULL, 23420),
(208343, 'deDE', 'Schlickhaufen', '', NULL, 23420),
(177794, 'deDE', 'Merkwürdige Schließkassette', '', NULL, 23420),
(3740, 'deDE', 'Blubbernder Felsspalt', '', NULL, 23420),
(220359, 'deDE', 'Ölfass der Kor''kron', '', NULL, 23420),
(161752, 'deDE', 'Werkzeugkiste', '', NULL, 23420),
(61936, 'deDE', 'Treibstoffzufuhrventil', '', NULL, 23420),
(61935, 'deDE', 'Regelventil', '', NULL, 23420),
(4141, 'deDE', 'Steuerkonsole', '', NULL, 23420),
(4072, 'deDE', 'Hauptsteuerventil', '', NULL, 23420),
(129127, 'deDE', 'Gallywix'' Schließkassette', '', NULL, 23420),
(201291, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(208345, 'deDE', 'Schlammiges Spurbeispiel', '', NULL, 23420),
(206559, 'deDE', 'Schmiede', '', NULL, 23420),
(201293, 'deDE', 'Amboss', '', NULL, 23420),
(126051, 'deDE', 'Bank', '', NULL, 23420),
(126050, 'deDE', 'Bank', '', NULL, 23420),
(4166, 'deDE', 'Alchemistische Gerätschaften', '', NULL, 23420),
(106336, 'deDE', 'Loderndes Feuer', '', NULL, 23420),
(106327, 'deDE', 'Loderndes Feuer', '', NULL, 23420),
(149038, 'deDE', 'Tischkocher', '', NULL, 23420),
(143982, 'deDE', 'Briefkasten', '', NULL, 23420),
(106325, 'deDE', 'Loderndes Feuer', '', NULL, 23420),
(165742, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(165751, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(165744, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(165743, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(165741, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(165745, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(165749, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(165748, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(165747, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(165746, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(165750, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(165740, 'deDE', 'Kunstlose Kohlenpfanne', '', NULL, 23420),
(204611, 'deDE', 'Feuer', '', NULL, 23420),
(202590, 'deDE', 'Amboss', '', NULL, 23420),
(195224, 'deDE', 'Gestohlene Silberschließkassette', '', NULL, 23420),
(23570, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(58369, 'deDE', 'Gestohlene Eisentruhe', '', NULL, 23420),
(195118, 'deDE', 'Gestohlener Kornsack', '', NULL, 23420),
(175708, 'deDE', 'Vorratskiste des Wegekreuzes', '', NULL, 23420),
(203825, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(174858, 'deDE', 'Eisenzars Importwaffen', '', NULL, 23420),
(169969, 'deDE', 'Schmiede', '', NULL, 23420),
(169968, 'deDE', 'Amboss', '', NULL, 23420),
(174872, 'deDE', 'Herd', '', NULL, 23420),
(143399, 'deDE', 'Taverne Zum geborstenen Kiel', '', NULL, 23420),
(143398, 'deDE', 'Herd', '', NULL, 23420),
(107042, 'deDE', 'Holzstuhl', '', NULL, 23420),
(107041, 'deDE', 'Holzstuhl', '', NULL, 23420),
(107040, 'deDE', 'Holzstuhl', '', NULL, 23420),
(107039, 'deDE', 'Holzstuhl', '', NULL, 23420),
(107037, 'deDE', 'Holzstuhl', '', NULL, 23420),
(106641, 'deDE', 'Holzstuhl', '', NULL, 23420),
(106638, 'deDE', 'Holzstuhl', '', NULL, 23420),
(144125, 'deDE', 'Briefkasten', '', NULL, 23420),
(3972, 'deDE', 'GESUCHT', '', NULL, 23420),
(169967, 'deDE', 'Schmied', '', NULL, 23420),
(68865, 'deDE', 'Schnüffelnasenleitstecken', '', NULL, 23420),
(21679, 'deDE', 'Schmiede', '', NULL, 23420),
(21530, 'deDE', 'Schnüffelnasenbesitzeranleitung', '', NULL, 23420),
(169966, 'deDE', 'Amboss', '', NULL, 23420),
(21277, 'deDE', 'Kiste mit Löchern', '', NULL, 23420),
(174857, 'deDE', 'Jazziks Gemischtwaren', '', NULL, 23420),
(220357, 'deDE', 'Steinkiste der Kor''kron', '', NULL, 23420),
(106326, 'deDE', 'Loderndes Feuer', '', NULL, 23420),
(106335, 'deDE', 'Loderndes Feuer', '', NULL, 23420),
(123328, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195016, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(126053, 'deDE', 'Bank', '', NULL, 23420),
(126052, 'deDE', 'Bank', '', NULL, 23420),
(195017, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(195004, 'deDE', 'Wolfsketten', '', NULL, 23420),
(195003, 'deDE', 'Wolfsketten', '', NULL, 23420),
(195001, 'deDE', 'Wolfsketten', '', NULL, 23420),
(20810, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(105176, 'deDE', 'Geldkassette der Venture Co.', '', NULL, 23420),
(3726, 'deDE', 'Erdwurzel', '', NULL, 23420),
(3725, 'deDE', 'Silberblatt', '', NULL, 23420),
(58388, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(58389, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203471, 'deDE', 'Goblinboot', '', NULL, 23420),
(5622, 'deDE', 'Lila Aura, kurzer Pfeiler, Maßstab 0,6', '', NULL, 23420),
(5621, 'deDE', 'Fehlerhafte Kraftsteine', '', NULL, 23420),
(5620, 'deDE', 'Fehlerhafte Kraftsteine', '', NULL, 23420),
(5619, 'deDE', 'Fehlerhafter Kraftstein', '', NULL, 23420),
(195018, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3724, 'deDE', 'Friedensblume', '', NULL, 23420),
(149036, 'deDE', 'Marvons Truhe', '', NULL, 23420),
(220176, 'deDE', 'Goblinteleporter', '', NULL, 23420),
(24727, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(195202, 'deDE', 'Dünnster Teil des Rumpfs', '', NULL, 23420),
(123462, 'deDE', 'Das Juwel der Südlichen Meere', '', NULL, 23420),
(123333, 'deDE', 'Geldkassette des Bukaniers', '', NULL, 23420),
(195211, 'deDE', 'Gewehrständer der Südmeerfreibeuter', '', NULL, 23420),
(160421, 'deDE', 'Kessel', '', NULL, 23420),
(160416, 'deDE', 'Stuhl', '', NULL, 23420),
(160415, 'deDE', 'Stuhl', '', NULL, 23420),
(160414, 'deDE', 'Stuhl', '', NULL, 23420),
(160413, 'deDE', 'Stuhl', '', NULL, 23420),
(160411, 'deDE', 'Herd', '', NULL, 23420),
(195361, 'deDE', 'Stapel azsharischen Holzes', '', NULL, 23420),
(206509, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204678, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202814, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(202813, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202812, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202811, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207690, 'deDE', 'Portal nach Vashj''ir', '', NULL, 23420),
(207689, 'deDE', 'Portal nach Tiefenheim', '', NULL, 23420),
(207688, 'deDE', 'Portal zum Hyjal', '', NULL, 23420),
(207687, 'deDE', 'Portal nach Uldum', '', NULL, 23420),
(207686, 'deDE', 'Portal ins Schattenhochland', '', NULL, 23420),
(206508, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204643, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204699, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207634, 'deDE', 'Briefkasten', '', NULL, 23420),
(204726, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(50805, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(204700, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206595, 'deDE', 'Portal nach Tol Barad', '', NULL, 23420),
(207635, 'deDE', 'Briefkasten', '', NULL, 23420),
(50803, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(205091, 'deDE', 'Gildentresor', '', NULL, 23420),
(50804, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(206730, 'deDE', 'Briefkasten', '', NULL, 23420),
(204727, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204689, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3301, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(205090, 'deDE', 'Briefkasten', '', NULL, 23420),
(204688, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204721, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206739, 'deDE', 'Schmiede', '', NULL, 23420),
(206738, 'deDE', 'Amboss', '', NULL, 23420),
(206909, 'deDE', 'Banner (Scale 1.5)', '', NULL, 23420),
(204720, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204719, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206734, 'deDE', 'Briefkasten', '', NULL, 23420),
(207101, 'deDE', 'Stuhl', '', NULL, 23420),
(207149, 'deDE', 'Barbiersessel', '', NULL, 23420),
(207148, 'deDE', 'Barbiersessel', '', NULL, 23420),
(207147, 'deDE', 'Barbiersessel', '', NULL, 23420),
(207100, 'deDE', 'Stuhl', '', NULL, 23420),
(207097, 'deDE', 'Stuhl', '', NULL, 23420),
(204625, 'deDE', 'Fackel', '', NULL, 23420),
(204722, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207150, 'deDE', 'Barbiersessel', '', NULL, 23420),
(207146, 'deDE', 'Barbiersessel', '', NULL, 23420),
(206733, 'deDE', 'Briefkasten', '', NULL, 23420),
(204709, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204626, 'deDE', 'Fackel', '', NULL, 23420),
(204622, 'deDE', 'Fackel', '', NULL, 23420),
(206735, 'deDE', 'Briefkasten', '', NULL, 23420),
(204198, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204194, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206732, 'deDE', 'Briefkasten', '', NULL, 23420),
(204723, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204724, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204604, 'deDE', 'Fackel', '', NULL, 23420),
(204621, 'deDE', 'Fackel', '', NULL, 23420),
(204725, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204640, 'deDE', 'Fackel', '', NULL, 23420),
(204628, 'deDE', 'Fackel', '', NULL, 23420),
(204605, 'deDE', 'Fackel', '', NULL, 23420),
(237738, 'deDE', 'Portal nach Ashran', '', NULL, 23420),
(204197, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(197257, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206736, 'deDE', 'Briefkasten', '', NULL, 23420),
(204609, 'deDE', 'Fackel', '', NULL, 23420),
(206741, 'deDE', 'Briefkasten', '', NULL, 23420),
(206547, 'deDE', 'Rekkuls Gifte', '', NULL, 23420),
(197276, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(197278, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(197280, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206549, 'deDE', 'Bruderschaft der Schattenläufer', '', NULL, 23420),
(197279, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202717, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(175787, 'deDE', 'Ungezierter Pflock', '', NULL, 23420),
(175788, 'deDE', 'Ungezierter Pflock', '', NULL, 23420),
(204610, 'deDE', 'Thron von Grommash', '', NULL, 23420),
(204191, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204192, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204196, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204606, 'deDE', 'Fackel', '', NULL, 23420),
(204608, 'deDE', 'Fackel', '', NULL, 23420),
(204607, 'deDE', 'Fackel', '', NULL, 23420),
(197285, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(197322, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206548, 'deDE', 'Eisenholz - Stäbe und Zauberstäbe', '', NULL, 23420),
(197323, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206546, 'deDE', 'Die Arkane Enklave', '', NULL, 23420),
(204634, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206538, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206545, 'deDE', 'Die langsame Klinge', '', NULL, 23420),
(206530, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206539, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204205, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(197286, 'deDE', 'Schattengrundreagenzien', '', NULL, 23420),
(197311, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(197307, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204631, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204632, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204633, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204195, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204200, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206729, 'deDE', 'Briefkasten', '', NULL, 23420),
(180888, 'deDE', 'Tisch', '', NULL, 23420),
(197287, 'deDE', 'Dunkelerde', '', NULL, 23420),
(197284, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(197314, 'deDE', 'Dunkelfeuerenklave', '', NULL, 23420),
(206529, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(105576, 'deDE', 'Beschwörungskreis', '', NULL, 23420),
(197312, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(210058, 'deDE', 'Schmiede', '', NULL, 23420),
(207646, 'deDE', 'Briefkasten', '', NULL, 23420),
(206740, 'deDE', 'Briefkasten', '', NULL, 23420),
(197313, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(197315, 'deDE', 'Versammlungsstein', '', NULL, 23420),
(197309, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202810, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204203, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204204, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(197310, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(197207, 'deDE', 'Briefkasten', '', NULL, 23420),
(197260, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(237942, 'deDE', 'Gravierte Steinplatte', '', NULL, 23420),
(197259, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204693, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(208534, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(202798, 'deDE', 'Rauchfleischgestell', '', NULL, 23420),
(204679, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(208054, 'deDE', 'Briefkasten', '', NULL, 23420),
(197261, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204695, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202801, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(202800, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202799, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(207630, 'deDE', 'Briefkasten', '', NULL, 23420),
(202815, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(202808, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(186722, 'deDE', 'Gelbe Stoffpuppe', '', NULL, 23420),
(204941, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(207633, 'deDE', 'Briefkasten', '', NULL, 23420),
(208269, 'deDE', 'Schmiede', '', NULL, 23420),
(202802, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(208270, 'deDE', 'Amboss', '', NULL, 23420),
(204939, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204696, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204698, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204936, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207632, 'deDE', 'Briefkasten', '', NULL, 23420),
(204937, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204938, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(202809, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204680, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204940, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204681, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202805, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202804, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202803, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206109, 'deDE', 'Auf Befehl des Kriegshäuptlings', '', NULL, 23420),
(204706, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204694, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202816, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202817, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204708, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207631, 'deDE', 'Briefkasten', '', NULL, 23420),
(204707, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204613, 'deDE', 'Feuer', '', NULL, 23420),
(204612, 'deDE', 'Feuer', '', NULL, 23420),
(204619, 'deDE', 'Feuer', '', NULL, 23420),
(204614, 'deDE', 'Feuer', '', NULL, 23420),
(204683, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(205142, 'deDE', 'Stuhl', '', NULL, 23420),
(204682, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(195222, 'deDE', 'Bühne', '', NULL, 23420),
(180759, 'deDE', 'Heldenporträt eines Orcs', '', NULL, 23420),
(180761, 'deDE', 'Heldenporträt eines Tauren', '', NULL, 23420),
(205108, 'deDE', 'Gildentresor', '', NULL, 23420),
(204616, 'deDE', 'Fackel', '', NULL, 23420),
(205109, 'deDE', 'Gildentresor', '', NULL, 23420),
(207637, 'deDE', 'Briefkasten', '', NULL, 23420),
(204638, 'deDE', 'Feuer', '', NULL, 23420),
(180775, 'deDE', 'Banner', '', NULL, 23420),
(207108, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180767, 'deDE', 'Laterne', '', NULL, 23420),
(180778, 'deDE', 'Banner', '', NULL, 23420),
(204637, 'deDE', 'Feuer', '', NULL, 23420),
(235877, 'deDE', 'Portal zu den Verwüsteten Landen', '', NULL, 23420),
(207638, 'deDE', 'Briefkasten', '', NULL, 23420),
(204714, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204702, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204618, 'deDE', 'Feuer', '', NULL, 23420),
(204685, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207636, 'deDE', 'Briefkasten', '', NULL, 23420),
(204713, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204617, 'deDE', 'Fackel', '', NULL, 23420),
(206965, 'deDE', 'Banner (Vergrößerung 1,5)', '', NULL, 23420),
(204684, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204635, 'deDE', 'Feuer', '', NULL, 23420),
(204615, 'deDE', 'Feuer', '', NULL, 23420),
(204701, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180768, 'deDE', 'Laterne', '', NULL, 23420),
(204636, 'deDE', 'Feuer', '', NULL, 23420),
(207640, 'deDE', 'Briefkasten', '', NULL, 23420),
(204703, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(215424, 'deDE', 'Portal nach Honigtau', '', NULL, 23420),
(204705, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204711, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207639, 'deDE', 'Briefkasten', '', NULL, 23420),
(206603, 'deDE', 'Gildenkiste', '', NULL, 23420),
(204692, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204717, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204718, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206787, 'deDE', 'Lantern (Scale 2)', '', NULL, 23420),
(207642, 'deDE', 'Briefkasten', '', NULL, 23420),
(204716, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204715, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207645, 'deDE', 'Briefkasten', '', NULL, 23420),
(207414, 'deDE', 'Wasser der Weitsicht', '', NULL, 23420),
(195638, 'deDE', 'Goblinmörser', '', NULL, 23420),
(204243, 'deDE', 'Doodad_Goblin_elevator01', '', NULL, 23420),
(204629, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204630, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207643, 'deDE', 'Briefkasten', '', NULL, 23420),
(207644, 'deDE', 'Briefkasten', '', NULL, 23420),
(204642, 'deDE', 'Feuer', '', NULL, 23420),
(204641, 'deDE', 'Feuer', '', NULL, 23420),
(209867, 'deDE', 'Schmiede', '', NULL, 23420),
(204712, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204704, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207641, 'deDE', 'Briefkasten', '', NULL, 23420),
(207107, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204687, 'deDE', 'Schmiede', '', NULL, 23420),
(204686, 'deDE', 'Schmiede', '', NULL, 23420),
(204677, 'deDE', 'Amboss', '', NULL, 23420),
(203969, 'deDE', 'Kaktusfeige', '', NULL, 23420),
(208266, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207110, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(208267, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207065, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(208268, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207066, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206608, 'deDE', 'Aufzug', '', NULL, 23420),
(206609, 'deDE', 'Aufzug', '', NULL, 23420),
(206610, 'deDE', 'Aufzug', '', NULL, 23420),
(206116, 'deDE', 'Auf Befehl des Kriegshäuptlings', '', NULL, 23420),
(207109, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(195639, 'deDE', 'Goblinmörser', '', NULL, 23420),
(196837, 'deDE', 'Doodad_Goblin_PoolElevator01', '', NULL, 23420),
(204246, 'deDE', 'Doodad_Goblin_elevator01', '', NULL, 23420),
(20694, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204190, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(31407, 'deDE', 'Loderndes Freudenfeuer', '', NULL, 23420),
(203851, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204183, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204182, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204181, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206575, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206574, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204166, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204187, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204165, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204164, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(252071, 'deDE', 'Fels', '', NULL, 23420),
(203855, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(55615, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(203854, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203853, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203852, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(3290, 'deDE', 'Gestohlener Vorratssack', '', NULL, 23420),
(204185, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204178, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204657, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204180, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204656, 'deDE', 'Feuer', '', NULL, 23420),
(204179, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204655, 'deDE', 'Feuergrube', '', NULL, 23420),
(204189, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204186, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204184, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204666, 'deDE', 'Wandfackel', '', NULL, 23420),
(204665, 'deDE', 'Wandfackel', '', NULL, 23420),
(204645, 'deDE', 'Feuer', '', NULL, 23420),
(206737, 'deDE', 'Briefkasten', '', NULL, 23420),
(204188, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204177, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204176, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204658, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204193, 'deDE', 'Feuer', '', NULL, 23420),
(204646, 'deDE', 'Feuer', '', NULL, 23420),
(204175, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(223814, 'deDE', 'Doodad_OGRaid_FrontGate_Back', '', NULL, 23420),
(223739, 'deDE', 'Das Tor von Orgrimmar', '', NULL, 23420),
(204174, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204173, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204172, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(58595, 'deDE', 'Geheimlager der Brennenden Klinge', '', NULL, 23420),
(204171, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204167, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204170, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204169, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207889, 'deDE', 'Doodad_Goblin_elevator01', '', NULL, 23420),
(204168, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(196475, 'deDE', 'Geheimlaborarchive', '', NULL, 23420),
(204360, 'deDE', 'Monströse Muschel', '', NULL, 23420),
(176499, 'deDE', 'Portal nach Orgrimmar', '', NULL, 23420),
(31573, 'deDE', 'Mittelgroße Kohlenpfanne', '', NULL, 23420),
(31572, 'deDE', 'Mittelgroße Kohlenpfanne', '', NULL, 23420),
(31575, 'deDE', 'Große Kohlenpfanne', '', NULL, 23420),
(143981, 'deDE', 'Briefkasten', '', NULL, 23420),
(31580, 'deDE', 'Blubbernder Kessel', '', NULL, 23420),
(201905, 'deDE', 'Blutkralleneier', '', NULL, 23420),
(202589, 'deDE', 'Briefkasten', '', NULL, 23420),
(203029, 'deDE', 'Kessel', '', NULL, 23420),
(203031, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(203030, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(202434, 'deDE', 'Visionskohlenbecken', '', NULL, 23420),
(201968, 'deDE', 'Dunkelspeerkäfig', '', NULL, 23420),
(205076, 'deDE', 'Entzauberte Tikimaske', '', NULL, 23420),
(202648, 'deDE', 'Schatz von Kul Tiras', '', NULL, 23420),
(3236, 'deDE', 'Gnomenwerkzeugkasten', '', NULL, 23420),
(202217, 'deDE', 'Nagabanner', '', NULL, 23420),
(202216, 'deDE', 'Nagabanner', '', NULL, 23420),
(202215, 'deDE', 'Nagabanner', '', NULL, 23420),
(202113, 'deDE', 'Gallschuppenflagge', '', NULL, 23420),
(31411, 'deDE', 'Tosendes Freudenfeuer', '', NULL, 23420),
(18077, 'deDE', 'Rauchständer', '', NULL, 23420),
(207206, 'deDE', 'Briefkasten', '', NULL, 23420),
(104555, 'deDE', 'Amboss', '', NULL, 23420),
(204135, 'deDE', 'Kessel', '', NULL, 23420),
(203840, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(31409, 'deDE', 'Tosendes Freudenfeuer', '', NULL, 23420),
(101748, 'deDE', 'Schamanenschrein', '', NULL, 23420),
(31410, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(203841, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203846, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203839, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203847, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203848, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203849, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203850, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(31574, 'deDE', 'Rauchständer', '', NULL, 23420),
(18079, 'deDE', 'Blubbernder Kessel', '', NULL, 23420),
(18076, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(18075, 'deDE', 'Kochtisch', '', NULL, 23420),
(221518, 'deDE', 'Stuhl', '', NULL, 23420),
(221519, 'deDE', 'Stuhl', '', NULL, 23420),
(221517, 'deDE', 'Stuhl', '', NULL, 23420),
(221516, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3658, 'deDE', 'Wasserfass', '', NULL, 23420),
(207207, 'deDE', 'Magisches Feuer', '', NULL, 23420),
(31578, 'deDE', 'Große Kohlenpfanne', '', NULL, 23420),
(186238, 'deDE', 'Zeppelin, Horde (Windmacht)', '', NULL, 23420),
(55250, 'deDE', 'Große Kohlenpfanne', '', NULL, 23420),
(31579, 'deDE', 'Große Kohlenpfanne', '', NULL, 23420),
(31577, 'deDE', 'Große Kohlenpfanne', '', NULL, 23420),
(174859, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(3851, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(3850, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(3849, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(3848, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(3847, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(203857, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203856, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202587, 'deDE', 'Briefkasten', '', NULL, 23420),
(18083, 'deDE', 'Blubbernder Kessel', '', NULL, 23420),
(12665, 'deDE', 'Kochtisch', '', NULL, 23420),
(31408, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(178089, 'deDE', 'Thazz''rils Hackenaura', '', NULL, 23420),
(178087, 'deDE', 'Thazz''rils Hacke', '', NULL, 23420),
(203844, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203843, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203845, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203842, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(3084, 'deDE', 'Feuer', '', NULL, 23420),
(202580, 'deDE', 'Höhlenkäfig', '', NULL, 23420),
(101749, 'deDE', 'Schamanenschrein', '', NULL, 23420),
(63674, 'deDE', 'Schamanenschrein', '', NULL, 23420),
(3192, 'deDE', 'Angriffsplan: Orgrimmar', '', NULL, 23420),
(3190, 'deDE', 'Angriffsplan: Sen''jin', '', NULL, 23420),
(202618, 'deDE', 'Kessel', '', NULL, 23420),
(202834, 'deDE', 'Stroh eines Fledermausschlafplatzes aus Sen''jin', '', NULL, 23420),
(202833, 'deDE', 'Fledermaustotem aus Sen''jin', '', NULL, 23420),
(202839, 'deDE', 'Zaunpfahl eines Fledermausschlafplatzes aus Sen''jin', '', NULL, 23420),
(202835, 'deDE', 'Zaun eines Fledermausschlafplatzes aus Sen''jin', '', NULL, 23420),
(3189, 'deDE', 'Angriffsplan: Tal der Prüfungen', '', NULL, 23420),
(171938, 'deDE', 'Kaktusapfel', '', NULL, 23420),
(175784, 'deDE', 'Holzstoß', '', NULL, 23420),
(181964, 'deDE', 'Statue der Königin Azshara', '', NULL, 23420),
(182016, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185046, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185047, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(181806, 'deDE', 'Beschwörungsrune', '', NULL, 23420),
(182032, 'deDE', 'Galaens Tagebuch', '', NULL, 23420),
(181898, 'deDE', 'Heilvorräte', '', NULL, 23420),
(185041, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(182027, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185040, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(182532, 'deDE', 'Glyphen vom Monument der Nazzivus', '', NULL, 23420),
(182015, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185043, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(182019, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(182017, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185044, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185042, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185045, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(181894, 'deDE', 'Teufelszapfenfungus', '', NULL, 23420),
(181780, 'deDE', 'Veränderter Blutmythoskristall', '', NULL, 23420),
(184850, 'deDE', 'Portalaufseher der Sonnenfalken', '', NULL, 23420),
(181997, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(181993, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(181992, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(181991, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(181998, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(181999, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(182000, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(181996, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(182001, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(181995, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(181994, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(75293, 'deDE', 'Große ramponierte Truhe', '', NULL, 23420),
(181878, 'deDE', 'Fass mit Unrat', '', NULL, 23420),
(181699, 'deDE', 'Fass mit Unrat', '', NULL, 23420),
(2849, 'deDE', 'Ramponierte Truhe', '', NULL, 23420),
(182013, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185036, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185037, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185039, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185038, 'deDE', 'Dunkle Kohlenpfanne', '', NULL, 23420),
(185056, 'deDE', 'Axxarienkristall', '', NULL, 23420),
(181771, 'deDE', 'Verderbter Kristall', '', NULL, 23420),
(181770, 'deDE', 'Verderbter Kristall', '', NULL, 23420),
(181746, 'deDE', 'Kloppers Ausrüstung', '', NULL, 23420),
(181870, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181988, 'deDE', 'Ewig brennender Scheiterhaufen', '', NULL, 23420),
(181897, 'deDE', 'Yseras Träne', '', NULL, 23420),
(19019, 'deDE', 'Kasten mit verschiedenen Ersatzteilen', '', NULL, 23420),
(182216, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3705, 'deDE', 'Fass mit Milch', '', NULL, 23420),
(182215, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(182218, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(182219, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(182217, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(182220, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(182221, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181981, 'deDE', 'Drachenknochen', '', NULL, 23420),
(181756, 'deDE', 'Zerfleddertes altes Buch', '', NULL, 23420),
(181758, 'deDE', 'Erdhaufen', '', NULL, 23420),
(182056, 'deDE', 'Schmiede', '', NULL, 23420),
(182055, 'deDE', 'Amboss', '', NULL, 23420),
(181895, 'deDE', 'Transdimensionale Schiffsreparatur für Stümper', '', NULL, 23420),
(181918, 'deDE', 'Kochtopf', '', NULL, 23420),
(182950, 'deDE', 'Briefkasten', '', NULL, 23420),
(181889, 'deDE', 'Steckbrief', '', NULL, 23420),
(181891, 'deDE', 'Blutpilz', '', NULL, 23420),
(181887, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181829, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181888, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181851, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(182114, 'deDE', 'Verderbte Kristallaura', '', NULL, 23420),
(181779, 'deDE', 'Kristall der Absturzstelle', '', NULL, 23420),
(2846, 'deDE', 'Ramponierte Truhe', '', NULL, 23420),
(181893, 'deDE', 'Ruinöser Birkensporling', '', NULL, 23420),
(181892, 'deDE', 'Wasserstinkmorchel', '', NULL, 23420),
(19017, 'deDE', 'Riesenmuschel', '', NULL, 23420),
(3662, 'deDE', 'Nahrungsmittelkiste', '', NULL, 23420),
(202275, 'deDE', 'Zornschuppenbrunnen', '', NULL, 23420),
(181854, 'deDE', 'Sandbirne', '', NULL, 23420),
(207307, 'deDE', 'Versammlungsstein', '', NULL, 23420),
(205889, 'deDE', 'Wächterkopf', '', NULL, 23420),
(205888, 'deDE', 'Staubwolke', '', NULL, 23420),
(205883, 'deDE', 'Statuentrümmer 00', '', NULL, 23420),
(205884, 'deDE', 'Wächterkopf', '', NULL, 23420),
(207964, 'deDE', 'Doodad_Uldum_Micro_Laserbeams02', '', NULL, 23420),
(207963, 'deDE', 'Doodad_Uldum_Micro_Laserbeams01', '', NULL, 23420),
(206501, 'deDE', 'Kollabierender Stern', '', NULL, 23420),
(206040, 'deDE', 'Obelisk der Sterne STRAHL', '', NULL, 23420),
(206663, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206662, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(205251, 'deDE', 'Verschnörkeltes Tafelfragment', '', NULL, 23420),
(205266, 'deDE', 'Aufwendige Scheibe', '', NULL, 23420),
(205540, 'deDE', 'Verfallenes Skelett', '', NULL, 23420),
(206958, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(205562, 'deDE', 'Versammlungsstein', '', NULL, 23420),
(214503, 'deDE', 'Instance Portal (Party + Heroic)', '', NULL, 23420),
(207281, 'deDE', 'Atulhets Aufzeichnungsfragment', '', NULL, 23420),
(206293, 'deDE', 'A.I.D.A.-Terminal', '', NULL, 23420),
(206532, 'deDE', 'Wartungswerkzeug', '', NULL, 23420),
(206395, 'deDE', 'Erster Sprengsatz', '', NULL, 23420),
(206396, 'deDE', 'Zweiter Sprengsatz', '', NULL, 23420),
(206397, 'deDE', 'Dritter Sprengsatz', '', NULL, 23420),
(207335, 'deDE', 'Fackel', '', NULL, 23420),
(207334, 'deDE', 'Fackel', '', NULL, 23420),
(206041, 'deDE', 'Amboss', '', NULL, 23420),
(207365, 'deDE', 'Fackel', '', NULL, 23420),
(206579, 'deDE', 'Wedel der Neferset', '', NULL, 23420),
(206578, 'deDE', 'Falle platzieren', '', NULL, 23420),
(207596, 'deDE', 'Fackel', '', NULL, 23420),
(205017, 'deDE', 'Kessel', '', NULL, 23420),
(207078, 'deDE', 'Ajamons Portal nach Südmeerstützpunkt', '', NULL, 23420),
(202942, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(202941, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(174863, 'deDE', 'Herd', '', NULL, 23420),
(160409, 'deDE', 'Stuhl', '', NULL, 23420),
(160410, 'deDE', 'Stuhl', '', NULL, 23420),
(202940, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(203837, 'deDE', 'Kessel', '', NULL, 23420),
(202944, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(207073, 'deDE', 'Ajamons Portal nach Tirths Heimsuchung', '', NULL, 23420),
(203056, 'deDE', 'G_Firework02Blue - Normal, Quest 1 & 2 Phases (scale x2)', '', NULL, 23420),
(203060, 'deDE', 'G_Firework02Yellow - Normal, Quest 1 & 2 Phases (scale x2)', '', NULL, 23420),
(70517, 'deDE', 'Laternenmast', '', NULL, 23420),
(177226, 'deDE', 'Buch ''Böse Wahrsagerei für Dummies''', '', NULL, 23420),
(177227, 'deDE', 'Couch', '', NULL, 23420),
(142122, 'deDE', 'Steckbrief', '', NULL, 23420),
(148504, 'deDE', 'Ein verdächtiger Grabstein', '', NULL, 23420),
(144112, 'deDE', 'Briefkasten', '', NULL, 23420),
(175335, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(202441, 'deDE', 'Jang''thraze der Beschützer', '', NULL, 23420),
(203820, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204733, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(207004, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207005, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207003, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(207002, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(178829, 'deDE', 'Versammlungsstein', '', NULL, 23420),
(205057, 'deDE', 'Flaches Grab', '', NULL, 23420),
(202187, 'deDE', 'Hebel zur Beobachtung von Blutblüten', '', NULL, 23420),
(202197, 'deDE', 'Hebel zur Beobachtung von Dimetrodons', '', NULL, 23420),
(202195, 'deDE', 'Hebel zur Beobachtung von Pterrordax', '', NULL, 23420),
(202196, 'deDE', 'Hebel zur Beobachtung von Gorillas', '', NULL, 23420),
(203042, 'deDE', 'Amboss', '', NULL, 23420),
(175763, 'deDE', 'Alter Hass - Die Kolonialisierung von Kalimdor', '', NULL, 23420),
(141838, 'deDE', 'Schmiede', '', NULL, 23420),
(195037, 'deDE', 'Silithidenei', '', NULL, 23420),
(175491, 'deDE', 'Ogerdreck 1', '', NULL, 23420),
(175490, 'deDE', 'Ogerdreck 2', '', NULL, 23420),
(35844, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(149028, 'deDE', 'Fleischgestell', '', NULL, 23420),
(149027, 'deDE', 'Fleischgestell', '', NULL, 23420),
(149026, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(175492, 'deDE', 'Ogerdreck 3', '', NULL, 23420),
(149029, 'deDE', 'Kessel', '', NULL, 23420),
(202407, 'deDE', 'Sandscharrers Kiste', '', NULL, 23420),
(202186, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(202474, 'deDE', 'Vorsintflutliche Kiste', '', NULL, 23420),
(142343, 'deDE', 'Podest von Uldum', '', NULL, 23420),
(141844, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(202420, 'deDE', 'Uralte Hieroglyphen', '', NULL, 23420),
(144053, 'deDE', 'Kieselschliffs Feldmesserausstattung', '', NULL, 23420),
(202198, 'deDE', 'Kiste des Dampfdruckkartells', '', NULL, 23420),
(184006, 'deDE', 'Temporalgefängnis', '', NULL, 23420),
(186218, 'deDE', 'Mondbrunnen', '', NULL, 23420),
(182560, 'deDE', 'Versammlungsstein', '', NULL, 23420),
(214613, 'deDE', 'Instance Portal (Raid 4 Difficulties)', '', NULL, 23420),
(214612, 'deDE', 'Instance Portal (Party + Heroic)', '', NULL, 23420),
(201947, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(203824, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(148879, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(160418, 'deDE', 'Stuhl', '', NULL, 23420),
(186897, 'deDE', 'Bank', '', NULL, 23420),
(186899, 'deDE', 'Bank', '', NULL, 23420),
(160420, 'deDE', 'Herd', '', NULL, 23420),
(160419, 'deDE', 'Stuhl', '', NULL, 23420),
(186902, 'deDE', 'Bank', '', NULL, 23420),
(186922, 'deDE', 'Bank', '', NULL, 23420),
(186900, 'deDE', 'Bank', '', NULL, 23420),
(186903, 'deDE', 'Bank', '', NULL, 23420),
(186901, 'deDE', 'Bank', '', NULL, 23420),
(186904, 'deDE', 'Bank', '', NULL, 23420),
(186896, 'deDE', 'Bank', '', NULL, 23420),
(201946, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201944, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(201945, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(142184, 'deDE', 'Truhe des Kapitäns', '', NULL, 23420),
(148880, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(203022, 'deDE', 'Piratenbeute', '', NULL, 23420),
(203021, 'deDE', 'Piratenbeute', '', NULL, 23420),
(202263, 'deDE', 'Piratenbeute', '', NULL, 23420),
(203019, 'deDE', 'Piratenbeute', '', NULL, 23420),
(148878, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(148877, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(179496, 'deDE', 'Verbeulte Schließkiste', '', NULL, 23420),
(148876, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(184532, 'deDE', 'Stuhl', '', NULL, 23420),
(184531, 'deDE', 'Stuhl', '', NULL, 23420),
(184530, 'deDE', 'Stuhl', '', NULL, 23420),
(181095, 'deDE', 'Wahrsagerei für Dummies', '', NULL, 23420),
(203821, 'deDE', 'Kessel', '', NULL, 23420),
(246871, 'deDE', 'Winkeys Taucherhelm', '', NULL, 23420),
(187912, 'deDE', 'Tor', '', NULL, 23420),
(214500, 'deDE', 'Instance Portal (Party + Heroic)', '', NULL, 23420),
(214502, 'deDE', 'Instance Portal (Party + Heroic)', '', NULL, 23420),
(207512, 'deDE', 'Seidene Schatzkiste', '', NULL, 23420),
(202464, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(202470, 'deDE', 'Bilgewässer Schließkiste', '', NULL, 23420),
(206743, 'deDE', 'Käfig des Schattenhammers', '', NULL, 23420),
(206767, 'deDE', 'Steuergerät für Magathas Fesseln', '', NULL, 23420),
(206742, 'deDE', 'Käfig des Schattenhammers', '', NULL, 23420),
(202264, 'deDE', 'Ringos Sack', '', NULL, 23420),
(164910, 'deDE', 'Verschnörkelte Truhe', '', NULL, 23420),
(202159, 'deDE', 'Weggeworfene Vorräte', '', NULL, 23420),
(202135, 'deDE', 'Dadangas Grab', '', NULL, 23420),
(174682, 'deDE', 'Vorsicht, Pterrordax!', '', NULL, 23420),
(202160, 'deDE', 'Weggeworfene Vorräte', '', NULL, 23420),
(202158, 'deDE', 'Weggeworfene Vorräte', '', NULL, 23420),
(204812, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(201867, 'deDE', 'Amboss', '', NULL, 23420),
(201866, 'deDE', 'Schmiede', '', NULL, 23420),
(266306, 'deDE', 'Schmiede', '', NULL, 23420),
(201913, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(161521, 'deDE', 'Forscherausrüstung', '', NULL, 23420),
(148503, 'deDE', 'Heiße Stelle des Feuersäulengrats', '', NULL, 23420),
(161505, 'deDE', 'Ein havariertes Floß', '', NULL, 23420),
(202247, 'deDE', 'Maximillians Lagerfeuer', '', NULL, 23420),
(161526, 'deDE', 'Kiste mit Lebensmitteln', '', NULL, 23420),
(169217, 'deDE', 'Flacher Fels von Un''Goro', '', NULL, 23420),
(202082, 'deDE', 'Nest der Ravasaurusmatriarchin', '', NULL, 23420),
(142143, 'deDE', 'Blindkraut', '', NULL, 23420),
(142144, 'deDE', 'Geisterpilz', '', NULL, 23420),
(201979, 'deDE', 'Kokosnuss von Un''Goro', '', NULL, 23420),
(201978, 'deDE', 'Fester Stein', '', NULL, 23420),
(201975, 'deDE', 'Teerblüte', '', NULL, 23420),
(164659, 'deDE', 'Grüner Machtkristall', '', NULL, 23420),
(164779, 'deDE', 'Grüner Machtkristall', '', NULL, 23420),
(164957, 'deDE', 'Östlicher Kristallpylon', '', NULL, 23420),
(164778, 'deDE', 'Blauer Machtkristall', '', NULL, 23420),
(201925, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(166863, 'deDE', 'Frisches Dreschadongerippe', '', NULL, 23420),
(195022, 'deDE', 'Gifthautravasaurusei', '', NULL, 23420),
(164658, 'deDE', 'Blauer Machtkristall', '', NULL, 23420),
(2040, 'deDE', 'Mithrilvorkommen', '', NULL, 23420),
(123848, 'deDE', 'Schlammbedeckte Thoriumader', '', NULL, 23420),
(202110, 'deDE', 'Gewaltiger Dinosaurierknochen', '', NULL, 23420),
(161527, 'deDE', 'Dinosaurierknochen', '', NULL, 23420),
(164660, 'deDE', 'Roter Machtkristall', '', NULL, 23420),
(164956, 'deDE', 'Westlicher Kristallpylon', '', NULL, 23420),
(164780, 'deDE', 'Roter Machtkristall', '', NULL, 23420),
(164661, 'deDE', 'Gelber Machtkristall', '', NULL, 23420),
(164958, 'deDE', 'Blutblütensprössling', '', NULL, 23420),
(180684, 'deDE', 'Ein Schwarm großer Weisenfische', '', NULL, 23420),
(164781, 'deDE', 'Gelber Machtkristall', '', NULL, 23420),
(209096, 'deDE', 'Der Schwur', '', NULL, 23420),
(249699, 'deDE', 'Götze der Wildnis', '', NULL, 23420),
(204119, 'deDE', 'Schmiede', '', NULL, 23420),
(204118, 'deDE', 'Amboss', '', NULL, 23420),
(209080, 'deDE', 'Portal nach Sturmwind', '', NULL, 23420),
(204878, 'deDE', 'Briefkasten', '', NULL, 23420),
(209081, 'deDE', 'Portal nach Orgrimmar', '', NULL, 23420),
(202754, 'deDE', 'Wacholderbeeren', '', NULL, 23420),
(202844, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202846, 'deDE', 'Verkohlte Stabfragmente', '', NULL, 23420),
(202787, 'deDE', 'Käfig des Schattenhammers', '', NULL, 23420),
(206613, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202652, 'deDE', 'Versorgungsgüter des Schattenhammers', '', NULL, 23420),
(207327, 'deDE', 'Finkles Maulwurfmaschine', '', NULL, 23420),
(203147, 'deDE', 'Avianas Grabkreis', '', NULL, 23420),
(204431, 'deDE', 'Briefkasten', '', NULL, 23420),
(203098, 'deDE', 'Schmiede der Feuerlande', '', NULL, 23420),
(203096, 'deDE', 'Portalrune', '', NULL, 23420),
(203095, 'deDE', 'Portalrune', '', NULL, 23420),
(203094, 'deDE', 'Portalrune', '', NULL, 23420),
(203093, 'deDE', 'Portalrune', '', NULL, 23420),
(203092, 'deDE', 'Portalrune', '', NULL, 23420),
(208449, 'deDE', 'Baum', '', NULL, 23420),
(208444, 'deDE', 'Baum', '', NULL, 23420),
(208454, 'deDE', 'Baum', '', NULL, 23420),
(203089, 'deDE', 'Ramponierte Steintruhe', '', NULL, 23420),
(202732, 'deDE', 'Die Kondensation von Elektragräuel', '', NULL, 23420),
(203066, 'deDE', 'Waffenkiste des Schattenhammers', '', NULL, 23420),
(204219, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202660, 'deDE', 'Lycanthoths Altar', '', NULL, 23420),
(203067, 'deDE', 'Amboss des Schattenhammers', '', NULL, 23420),
(202723, 'deDE', 'Das Portal des Manipulierers', '', NULL, 23420),
(202712, 'deDE', 'Die Zwielichtapokryphen', '', NULL, 23420),
(202711, 'deDE', 'Podium des Schattenhammers', '', NULL, 23420),
(204580, 'deDE', 'Gar''gols persönliche Schatzkiste', '', NULL, 23420),
(202697, 'deDE', 'Auge des Zwielichts', '', NULL, 23420),
(202706, 'deDE', 'Kessel des Schattenhammers', '', NULL, 23420),
(204218, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204217, 'deDE', 'Schmiede', '', NULL, 23420),
(204216, 'deDE', 'Amboss', '', NULL, 23420),
(202705, 'deDE', 'Schwarzfeuerglut', '', NULL, 23420),
(202749, 'deDE', 'Azsharas Schleier', '', NULL, 23420),
(202702, 'deDE', 'Steinblüte', '', NULL, 23420),
(202703, 'deDE', 'Bitterblüte', '', NULL, 23420),
(202367, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(202731, 'deDE', 'Blitzkanal', '', NULL, 23420),
(202387, 'deDE', 'Briefkasten', '', NULL, 23420),
(208791, 'deDE', 'Kleiner Grabstein', '', NULL, 23420),
(208899, 'deDE', 'Feuerportal', '', NULL, 23420),
(202967, 'deDE', 'Waffenständer des Schattenhammers', '', NULL, 23420),
(208573, 'deDE', 'Tisch', '', NULL, 23420),
(208902, 'deDE', 'Feuerportal', '', NULL, 23420),
(208900, 'deDE', 'Portal in die Feuerlande', '', NULL, 23420),
(202748, 'deDE', 'Sturmwinde', '', NULL, 23420),
(194297, 'deDE', 'Low Poly Fire - Medium (with animations)', '', NULL, 23420),
(208442, 'deDE', 'Blauknollenwinde', '', NULL, 23420),
(206683, 'deDE', 'Cosmetic Object - Fire Med', '', NULL, 23420),
(208901, 'deDE', 'Feuerportal', '', NULL, 23420),
(208381, 'deDE', 'Aschehaufen', '', NULL, 23420),
(202996, 'deDE', 'Initiationspodium', '', NULL, 23420),
(204227, 'deDE', 'Kessel', '', NULL, 23420),
(204226, 'deDE', 'Kessel', '', NULL, 23420),
(204127, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204222, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204128, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(202701, 'deDE', 'Plumpskloversteck', '', NULL, 23420),
(203091, 'deDE', 'Plumpsklo der Oger', '', NULL, 23420),
(204221, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204220, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204225, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203169, 'deDE', 'Blaithes Schlafplatz', '', NULL, 23420),
(203310, 'deDE', 'Pfeil des Aufsehers', '', NULL, 23420),
(203375, 'deDE', 'Nemesiskristall', '', NULL, 23420),
(203816, 'deDE', 'Brennende Trümmer', '', NULL, 23420),
(203815, 'deDE', 'Brennende Trümmer', '', NULL, 23420),
(203818, 'deDE', 'Brennende Trümmer', '', NULL, 23420),
(203817, 'deDE', 'Brennende Trümmer', '', NULL, 23420),
(203819, 'deDE', 'Brennende Trümmer', '', NULL, 23420),
(202776, 'deDE', 'Ein Schwarm Bergforellen', '', NULL, 23420),
(209128, 'deDE', 'Versammlungsstein der Feuerlande', '', NULL, 23420),
(207734, 'deDE', 'Feuerteich', '', NULL, 23420),
(203048, 'deDE', 'Kodex des Aszendenten', '', NULL, 23420),
(207359, 'deDE', 'Reines Zwielichtei', '', NULL, 23420),
(207518, 'deDE', 'Seidene Schatzkiste', '', NULL, 23420),
(203047, 'deDE', 'Die brennenden Litaneien', '', NULL, 23420),
(203046, 'deDE', 'Foliant der Flamme', '', NULL, 23420),
(203383, 'deDE', 'Lavaspritzer', '', NULL, 23420),
(202736, 'deDE', 'Obsidiumvorkommen', '', NULL, 23420),
(194203, 'deDE', 'Entzündete Wildfeuerflasche', '', NULL, 23420),
(195071, 'deDE', 'Alptraumportal', '', NULL, 23420),
(207521, 'deDE', 'Schatzkiste aus Ahornholz', '', NULL, 23420),
(1926, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(1923, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195057, 'deDE', 'Verschlingendes Artefakt', '', NULL, 23420),
(194140, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(1921, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194141, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194142, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(61929, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(194144, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194143, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194150, 'deDE', 'Jadefeuerkohlenpfanne', '', NULL, 23420),
(194139, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194114, 'deDE', 'Bärenpodest', '', NULL, 23420),
(194106, 'deDE', 'Uralte Bärenstatue', '', NULL, 23420),
(16393, 'deDE', 'Uralte Flamme', '', NULL, 23420),
(208296, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195007, 'deDE', 'Feder eines getöteten Wildekin', '', NULL, 23420),
(17185, 'deDE', 'Dröhnkiste 525', '', NULL, 23420),
(194100, 'deDE', 'Bärenklau', '', NULL, 23420),
(195058, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195054, 'deDE', 'Schlammverkrustete uralte Scheibe', '', NULL, 23420),
(204228, 'deDE', 'Horn der Urtume', '', NULL, 23420),
(195078, 'deDE', 'Portal nach Azshara', '', NULL, 23420),
(175227, 'deDE', 'Gestrandete Meereskreatur', '', NULL, 23420),
(195043, 'deDE', 'Bauplatz der Grauflossen', '', NULL, 23420),
(195080, 'deDE', 'Schwimmende Trümmer der Grauflossen', '', NULL, 23420),
(195042, 'deDE', 'Trümmer der Grauflossen', '', NULL, 23420),
(195021, 'deDE', 'Glimmermuschel', '', NULL, 23420),
(176190, 'deDE', 'Gestrandete Meeresschildkröte', '', NULL, 23420),
(175207, 'deDE', 'Gestrandete Meereskreatur', '', NULL, 23420),
(17182, 'deDE', 'Dröhnkiste 827', '', NULL, 23420),
(194211, 'deDE', 'Kugel und Kette', '', NULL, 23420),
(194113, 'deDE', 'Shadowmoon Rune 2 (scale x2.00)', '', NULL, 23420),
(103687, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(92490, 'deDE', 'Schmiede', '', NULL, 23420),
(92489, 'deDE', 'Amboss', '', NULL, 23420),
(194112, 'deDE', 'Schattenmondrune 2', '', NULL, 23420),
(194145, 'deDE', 'Aetherions Ritualkugel', '', NULL, 23420),
(194771, 'deDE', 'Rauchwerk des Hainhüters', '', NULL, 23420),
(194179, 'deDE', 'Die letzte Flamme von Bashal''Aran', '', NULL, 23420),
(91737, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204132, 'deDE', 'Schmiede', '', NULL, 23420),
(204131, 'deDE', 'Amboss', '', NULL, 23420),
(204774, 'deDE', 'Briefkasten', '', NULL, 23420),
(17183, 'deDE', 'Dröhnkiste 411', '', NULL, 23420),
(181646, 'deDE', 'Exodar', '', NULL, 23420),
(194204, 'deDE', 'Schattenhammerpläne', '', NULL, 23420),
(194208, 'deDE', 'Rauchender Fliegenpilz', '', NULL, 23420),
(194209, 'deDE', 'Rauchender Fliegenpilz', '', NULL, 23420),
(194714, 'deDE', 'Widerliche Werkbank', '', NULL, 23420),
(194130, 'deDE', 'Boden des sicheren Bärenkäfigs', '', NULL, 23420),
(194124, 'deDE', 'Sicherer Bärenkäfig', '', NULL, 23420),
(194133, 'deDE', 'Sicherer Nachtrattenkäfig', '', NULL, 23420),
(194131, 'deDE', 'Boden des sicheren Nachtrattenkäfigs', '', NULL, 23420),
(194122, 'deDE', 'Dröhnkiste 723', '', NULL, 23420),
(194787, 'deDE', 'Versengtes Buch', '', NULL, 23420),
(176291, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176289, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(194105, 'deDE', 'Dröhnkiste 413', '', NULL, 23420),
(194107, 'deDE', 'Verkrustete Muschel', '', NULL, 23420),
(176196, 'deDE', 'Gestrandete Meeresschildkröte', '', NULL, 23420),
(180655, 'deDE', 'Schwimmende Trümmer', '', NULL, 23420),
(194101, 'deDE', 'Splitterspeerkäfig', '', NULL, 23420),
(204130, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(194104, 'deDE', 'Vorratskisten der Splitterspeere', '', NULL, 23420),
(194102, 'deDE', 'Vorratskisten der Splitterspeere', '', NULL, 23420),
(194103, 'deDE', 'Vorratskisten der Splitterspeere', '', NULL, 23420),
(194088, 'deDE', 'Hochgeborenenrelikt', '', NULL, 23420),
(13359, 'deDE', 'Katzenstatuette', '', NULL, 23420),
(207533, 'deDE', 'Schatzkiste aus Runensteinen', '', NULL, 23420),
(194090, 'deDE', 'Hochgeborenenrelikt', '', NULL, 23420),
(194089, 'deDE', 'Hochgeborenenrelikt', '', NULL, 23420),
(175984, 'deDE', 'Kessel', '', NULL, 23420),
(207091, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(207104, 'deDE', 'Hauptkontrollpumpe', '', NULL, 23420),
(204150, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204149, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204147, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204148, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204146, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204145, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204157, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176091, 'deDE', 'Kessel der Totenwaldfelle', '', NULL, 23420),
(204158, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3718, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(3717, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(3716, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204159, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204160, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206398, 'deDE', 'Briefkasten', '', NULL, 23420),
(204162, 'deDE', 'Amboss', '', NULL, 23420),
(204161, 'deDE', 'Schmiede', '', NULL, 23420),
(207105, 'deDE', 'Eisenwaldsprengstoff', '', NULL, 23420),
(207063, 'deDE', 'Stab des Sonnenlichts', '', NULL, 23420),
(204735, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206382, 'deDE', 'Briefkasten', '', NULL, 23420),
(206963, 'deDE', 'Reiche Eisenwalderde', '', NULL, 23420),
(206852, 'deDE', 'Höllenbestienüberreste', '', NULL, 23420),
(177276, 'deDE', 'Mondbrunnen', '', NULL, 23420),
(142140, 'deDE', 'Lila Lotus', '', NULL, 23420),
(175076, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(175075, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(176305, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(176161, 'deDE', 'Kohlenpfanne des Hasses', '', NULL, 23420),
(176306, 'deDE', 'Käfig', '', NULL, 23420),
(176158, 'deDE', 'Kohlenpfanne des Schmerzes', '', NULL, 23420),
(208190, 'deDE', 'Busch', '', NULL, 23420),
(177275, 'deDE', 'Mondbrunnen', '', NULL, 23420),
(176184, 'deDE', 'der verderbte Mondbrunnen von Jaedenar', '', NULL, 23420),
(176160, 'deDE', 'Kohlenpfanne des Leidens', '', NULL, 23420),
(176159, 'deDE', 'Kohlenpfanne der Bosheit', '', NULL, 23420),
(204144, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204156, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204155, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204154, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(176326, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206706, 'deDE', 'Klageveilchen', '', NULL, 23420),
(206764, 'deDE', 'Smaragdgrüne Schimmerkappe', '', NULL, 23420),
(206381, 'deDE', 'Briefkasten', '', NULL, 23420),
(204142, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3636, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204140, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204139, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204136, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(3637, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204143, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204138, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(207476, 'deDE', 'Silberbeschlagene Schatzkiste', '', NULL, 23420),
(204141, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204137, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(206585, 'deDE', 'Totem von Ruumbo', '', NULL, 23420),
(204738, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204737, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204736, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204734, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206656, 'deDE', 'Mal des Tichondrius', '', NULL, 23420),
(181928, 'deDE', 'Käfig der Prinzessin der Tannenruhfeste', '', NULL, 23420),
(181286, 'deDE', 'Holografischer Emitter', '', NULL, 23420),
(181489, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181488, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181493, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181490, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181954, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181491, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181494, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181283, 'deDE', 'Emitterersatzteile', '', NULL, 23420),
(181487, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181480, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181483, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181486, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181481, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181485, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181484, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181433, 'deDE', 'Strahlender Energiekristall', '', NULL, 23420),
(181396, 'deDE', 'Teppich', '', NULL, 23420),
(202585, 'deDE', 'Briefkasten', '', NULL, 23420),
(184076, 'deDE', 'Kochtopf', '', NULL, 23420),
(181370, 'deDE', 'Käfig für unberechenbare Mutanten', '', NULL, 23420),
(181652, 'deDE', 'Feuerriss', '', NULL, 23420),
(181650, 'deDE', 'Wasserriss', '', NULL, 23420),
(181651, 'deDE', 'Luftriss', '', NULL, 23420),
(182127, 'deDE', 'Verderbte Blume', '', NULL, 23420),
(182070, 'deDE', 'Roter Kristall', '', NULL, 23420),
(181933, 'deDE', 'Kochtopf', '', NULL, 23420),
(181932, 'deDE', 'Lodernde Kohlenpfanne', '', NULL, 23420),
(184284, 'deDE', 'Fackel des Am''mentals', '', NULL, 23420),
(181790, 'deDE', 'Kochtopf', '', NULL, 23420),
(182278, 'deDE', 'Schmiede', '', NULL, 23420),
(182279, 'deDE', 'Amboss', '', NULL, 23420),
(181805, 'deDE', 'Holografischer Emitter', '', NULL, 23420),
(182948, 'deDE', 'Briefkasten', '', NULL, 23420),
(181654, 'deDE', 'Holz', '', NULL, 23420),
(183275, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(183276, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181672, 'deDE', 'Weidenmann', '', NULL, 23420),
(183274, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(183277, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181694, 'deDE', 'Nagaflagge', '', NULL, 23420),
(181616, 'deDE', 'Ein Schwarm Schnapper', '', NULL, 23420),
(181675, 'deDE', 'Nautischer Kompass', '', NULL, 23420),
(181674, 'deDE', 'Nautische Karte', '', NULL, 23420),
(181920, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181808, 'deDE', 'Seemuschelschale', '', NULL, 23420),
(181807, 'deDE', 'Seemuschelschale', '', NULL, 23420),
(181696, 'deDE', 'Ausgehöhlter Stamm', '', NULL, 23420),
(181712, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181809, 'deDE', 'Seemuschelschale', '', NULL, 23420),
(181716, 'deDE', 'Schmiede', '', NULL, 23420),
(181715, 'deDE', 'Amboss', '', NULL, 23420),
(181711, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181697, 'deDE', 'Laubhaufen', '', NULL, 23420),
(181683, 'deDE', 'Uraltes Relikt', '', NULL, 23420),
(181789, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181788, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181787, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175850, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175846, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175845, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(3719, 'deDE', 'Nahrungsmittelkiste', '', NULL, 23420),
(181773, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181745, 'deDE', 'Wildekinwaffe', '', NULL, 23420),
(181775, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(181748, 'deDE', 'Blutkristall', '', NULL, 23420),
(175847, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175844, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175849, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175848, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(2844, 'deDE', 'Ramponierte Truhe', '', NULL, 23420),
(182012, 'deDE', 'Behelfsmäßiges Gefängnis', '', NULL, 23420),
(181757, 'deDE', 'Tannenruhgetreide', '', NULL, 23420),
(181644, 'deDE', 'Azurlöwenmäulchen', '', NULL, 23420),
(182949, 'deDE', 'Briefkasten', '', NULL, 23420),
(183872, 'deDE', 'Angeln', '', NULL, 23420),
(183873, 'deDE', 'Reagenzien', '', NULL, 23420),
(183874, 'deDE', 'Juwelierskunst', '', NULL, 23420),
(183800, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(183799, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(183859, 'deDE', 'Kochkunst', '', NULL, 23420),
(182200, 'deDE', 'Kochtopf', '', NULL, 23420),
(183861, 'deDE', 'Gasthaus', '', NULL, 23420),
(183858, 'deDE', 'Briefkasten', '', NULL, 23420),
(180700, 'deDE', 'Heuballen 1', '', NULL, 23420),
(204879, 'deDE', 'Hippogryphenhorst', '', NULL, 23420),
(180038, 'deDE', 'Heuballen 02', '', NULL, 23420),
(183871, 'deDE', 'Stumpfe Waffen', '', NULL, 23420),
(183869, 'deDE', 'Klingenwaffen', '', NULL, 23420),
(183868, 'deDE', 'Plattenrüstung & Schilde', '', NULL, 23420),
(183866, 'deDE', 'Sanktum der Jäger', '', NULL, 23420),
(184923, 'deDE', 'Schmiede', '', NULL, 23420),
(184922, 'deDE', 'Schmiede', '', NULL, 23420),
(183878, 'deDE', 'Amboss', '', NULL, 23420),
(183863, 'deDE', 'Ingenieurskunst', '', NULL, 23420),
(183793, 'deDE', 'Amboss', '', NULL, 23420),
(183867, 'deDE', 'Waffenring', '', NULL, 23420),
(183862, 'deDE', 'Bergbau & Schmiedekunst', '', NULL, 23420),
(183870, 'deDE', 'Lederverarbeitung & Kürschnerei', '', NULL, 23420),
(183864, 'deDE', 'Schneiderei', '', NULL, 23420),
(183856, 'deDE', 'Briefkasten', '', NULL, 23420),
(183855, 'deDE', 'Auktionshaus', '', NULL, 23420),
(183891, 'deDE', 'Sanktum der Verteidiger', '', NULL, 23420),
(183890, 'deDE', 'Alchemie & Kräuterkunde', '', NULL, 23420),
(187291, 'deDE', 'Gildentresor', '', NULL, 23420),
(183875, 'deDE', 'Verzauberkunst & Inschriftenkunde', '', NULL, 23420),
(207322, 'deDE', 'Heldenaufruf', '', NULL, 23420),
(183860, 'deDE', 'Bank', '', NULL, 23420),
(183857, 'deDE', 'Briefkasten', '', NULL, 23420),
(182253, 'deDE', 'Händlertreppe', '', NULL, 23420),
(182251, 'deDE', 'Die Kristallhalle', '', NULL, 23420),
(181849, 'deDE', 'Felshetzerkäfig', '', NULL, 23420),
(181732, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181731, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181733, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181726, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181730, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181727, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181725, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181729, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181728, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181723, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181724, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181714, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181721, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181722, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(181720, 'deDE', 'Käfig der Sichelklauen', '', NULL, 23420),
(194497, 'deDE', 'Banner der Exodar', '', NULL, 23420),
(183889, 'deDE', 'Sanktum der Anachoreten', '', NULL, 23420),
(182252, 'deDE', 'Die Halle des Lichts', '', NULL, 23420),
(183865, 'deDE', 'Gildenmeister & Wappenröcke', '', NULL, 23420),
(183893, 'deDE', 'Halle der Mystiker', '', NULL, 23420),
(207996, 'deDE', 'Portal nach Darnassus', '', NULL, 23420),
(208595, 'deDE', 'Ghost State - 2.00', '', NULL, 23420),
(208591, 'deDE', 'Heimgesuchte Kriegstrommel', '', NULL, 23420),
(20991, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(20990, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195074, 'deDE', 'Melithars gestohlene Taschen', '', NULL, 23420),
(20989, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(207346, 'deDE', 'Mondblütenlilie', '', NULL, 23420),
(4406, 'deDE', 'Waldwebereier', '', NULL, 23420),
(19549, 'deDE', 'Mondbrunnen des Laubschattentals', '', NULL, 23420),
(32879, 'deDE', 'Kessel', '', NULL, 23420),
(202592, 'deDE', 'Briefkasten', '', NULL, 23420),
(7923, 'deDE', 'Denalans Blumentopf', '', NULL, 23420),
(6751, 'deDE', 'Sonderbare fruchtbeladene Pflanze', '', NULL, 23420),
(177272, 'deDE', 'Mondbrunnen', '', NULL, 23420),
(142109, 'deDE', 'Briefkasten', '', NULL, 23420),
(32880, 'deDE', 'Kessel', '', NULL, 23420),
(19550, 'deDE', 'Mondbrunnen von Sternenhauch', '', NULL, 23420),
(1988, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(1987, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(126158, 'deDE', 'Tallonkais Kommode', '', NULL, 23420),
(1989, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(1990, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(2000, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(1997, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(1986, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(2739, 'deDE', 'Truhe der schwarzen Feder', '', NULL, 23420),
(2742, 'deDE', 'Truhe des Nistens', '', NULL, 23420),
(2740, 'deDE', 'Truhe der Rabenkralle', '', NULL, 23420),
(2741, 'deDE', 'Truhe des Himmels', '', NULL, 23420),
(19551, 'deDE', 'Mondbrunnen der Teiche von Arlithrien', '', NULL, 23420),
(4608, 'deDE', 'Bäumlingsspross', '', NULL, 23420),
(1999, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(1998, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195175, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195174, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195176, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195177, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195178, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(1992, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195179, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(1991, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(208046, 'deDE', 'Teufelszapfen', '', NULL, 23420),
(1673, 'deDE', 'Teufelszapfen', '', NULL, 23420),
(19552, 'deDE', 'Mondbrunnen der Lichtung des Orakels', '', NULL, 23420),
(6752, 'deDE', 'Sonderbare wedelbestückte Pflanze', '', NULL, 23420),
(92696, 'deDE', 'Wurfwaffen', '', NULL, 23420),
(92544, 'deDE', 'Pfeilmacher', '', NULL, 23420),
(92540, 'deDE', 'Waffen', '', NULL, 23420),
(92539, 'deDE', 'Waffen', '', NULL, 23420),
(92527, 'deDE', 'Taschen', '', NULL, 23420),
(92528, 'deDE', 'Gemischtwaren', '', NULL, 23420),
(195529, 'deDE', 'Briefkasten', '', NULL, 23420),
(1685, 'deDE', 'Schmiede', '', NULL, 23420),
(195530, 'deDE', 'Briefkasten', '', NULL, 23420),
(92525, 'deDE', 'Argentumdämmerung', '', NULL, 23420),
(92526, 'deDE', 'Alchemie', '', NULL, 23420),
(91672, 'deDE', 'Holzstuhl', '', NULL, 23420),
(175725, 'deDE', 'Die Alten Götter und die Ordnung von Azeroth', '', NULL, 23420),
(21581, 'deDE', 'Folgen des Zweiten Krieges', '', NULL, 23420),
(92530, 'deDE', 'Verzauberkunst', '', NULL, 23420),
(92529, 'deDE', 'Verzauberkunst', '', NULL, 23420),
(92535, 'deDE', 'Allgemeines Handwerk', '', NULL, 23420),
(92534, 'deDE', 'Schneiderei', '', NULL, 23420),
(92533, 'deDE', 'Lederverarbeitung', '', NULL, 23420),
(92536, 'deDE', 'Allgemeines Handwerk', '', NULL, 23420),
(148423, 'deDE', 'Gasthaus', '', NULL, 23420),
(188123, 'deDE', 'Briefkasten', '', NULL, 23420),
(92700, 'deDE', 'Schneiderei', '', NULL, 23420),
(92699, 'deDE', 'Lederverarbeitung', '', NULL, 23420),
(92524, 'deDE', 'Gilde', '', NULL, 23420),
(208825, 'deDE', 'Schrein der Vorfahren', '', NULL, 23420),
(92694, 'deDE', 'Taschen', '', NULL, 23420),
(92693, 'deDE', 'Gemischtwaren', '', NULL, 23420),
(92541, 'deDE', 'Waffen', '', NULL, 23420),
(92542, 'deDE', 'Waffen', '', NULL, 23420),
(92695, 'deDE', 'Pfeilmacher', '', NULL, 23420),
(92543, 'deDE', 'Wurfwaffen', '', NULL, 23420),
(195528, 'deDE', 'Briefkasten', '', NULL, 23420),
(175759, 'deDE', 'Der aufstrebende Verräter', '', NULL, 23420),
(92552, 'deDE', 'Zweihändige Waffen', '', NULL, 23420),
(95449, 'deDE', 'Stoffrüstung', '', NULL, 23420),
(146096, 'deDE', 'Schildwachenbaracke', '', NULL, 23420),
(19877, 'deDE', 'Velindes Spind', '', NULL, 23420),
(175730, 'deDE', 'Der Weltenbaum und der Smaragdgrüne Traum', '', NULL, 23420),
(92537, 'deDE', 'Erste Hilfe', '', NULL, 23420),
(92538, 'deDE', 'Kochkunst', '', NULL, 23420),
(175760, 'deDE', 'Aufstieg der Blutelfen', '', NULL, 23420),
(175727, 'deDE', 'Der Krieg der Ahnen', '', NULL, 23420),
(180870, 'deDE', 'Knallfrösche', '', NULL, 23420),
(91673, 'deDE', 'Herd', '', NULL, 23420),
(175731, 'deDE', 'Die Verbannung der Hochelfen', '', NULL, 23420),
(92551, 'deDE', 'Zweihändige Waffen', '', NULL, 23420),
(92549, 'deDE', 'Stoffrüstung', '', NULL, 23420),
(92550, 'deDE', 'Lederrüstung', '', NULL, 23420),
(92548, 'deDE', 'Kettenrüstungen', '', NULL, 23420),
(92547, 'deDE', 'Schilde', '', NULL, 23420),
(92546, 'deDE', 'Schilde', '', NULL, 23420),
(92545, 'deDE', 'Kettenrüstungen', '', NULL, 23420),
(92532, 'deDE', 'Stäbe', '', NULL, 23420),
(92531, 'deDE', 'Roben', '', NULL, 23420),
(208830, 'deDE', 'Reishäufchen', '', NULL, 23420),
(208829, 'deDE', 'Reiskorb', '', NULL, 23420),
(138498, 'deDE', 'Brunnen des Tempels des Mondes', '', NULL, 23420),
(207995, 'deDE', 'Portal zur Exodar', '', NULL, 23420),
(208817, 'deDE', 'Gabentisch', '', NULL, 23420),
(208824, 'deDE', 'Schreinaura', '', NULL, 23420),
(208818, 'deDE', 'Gesegnete Reiskuchen', '', NULL, 23420),
(208819, 'deDE', 'Gesegnete Reiskuchen', '', NULL, 23420),
(240625, 'deDE', 'Re­li­qui­ar der Hohepriesterin', '', NULL, 23420),
(207321, 'deDE', 'Heldenaufruf', '', NULL, 23420),
(142110, 'deDE', 'Briefkasten', '', NULL, 23420),
(208815, 'deDE', 'Vergrabener Krug', '', NULL, 23420),
(208814, 'deDE', 'Vergrabener Kimchikrug', '', NULL, 23420),
(187337, 'deDE', 'Gildentresor', '', NULL, 23420),
(187296, 'deDE', 'Gildentresor', '', NULL, 23420),
(178147, 'deDE', 'Ruul Schneehuf, Käfig', '', NULL, 23420),
(203217, 'deDE', 'Gefängnis der Hochgeborenen', '', NULL, 23420),
(50831, 'deDE', 'Schmiede', '', NULL, 23420),
(50830, 'deDE', 'Steinamboss', '', NULL, 23420),
(2335, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(2334, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(176787, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(92426, 'deDE', 'Eingelegter Schlick', '', NULL, 23420),
(92427, 'deDE', 'Rauchfleischgestell', '', NULL, 23420),
(177288, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(203041, 'deDE', 'Kessel', '', NULL, 23420),
(204797, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(2336, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(206765, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204798, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204804, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(178105, 'deDE', 'Resonitkristall', '', NULL, 23420),
(203253, 'deDE', 'Kalimdoradlernest', '', NULL, 23420),
(204800, 'deDE', 'Kessel', '', NULL, 23420),
(203280, 'deDE', 'Waffenkiste der Allianz', '', NULL, 23420),
(203291, 'deDE', 'Schmiede', '', NULL, 23420),
(204805, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203279, 'deDE', 'Waffenkiste der Allianz', '', NULL, 23420),
(203290, 'deDE', 'Amboss', '', NULL, 23420),
(203235, 'deDE', 'Detonationsgerät', '', NULL, 23420),
(203446, 'deDE', 'Banner der Horde', '', NULL, 23420),
(142342, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(203413, 'deDE', 'Krom''gar "Elfentöter"', '', NULL, 23420),
(194471, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194472, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(203166, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203159, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203155, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203165, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203163, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203158, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203160, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203176, 'deDE', 'Mechanisiertes Eis', '', NULL, 23420),
(203161, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203239, 'deDE', 'Stützbalkenmarkierung', '', NULL, 23420),
(203443, 'deDE', 'Ersatzteil', '', NULL, 23420),
(194802, 'deDE', 'Amboss', '', NULL, 23420),
(203429, 'deDE', 'Briefkasten', '', NULL, 23420),
(194473, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(203162, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203154, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203174, 'deDE', 'Mechanisiertes Feuer', '', NULL, 23420),
(203167, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203148, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203171, 'deDE', 'Jägerin Illionas Käfig', '', NULL, 23420),
(203149, 'deDE', 'Hordenkäfigsbasis', '', NULL, 23420),
(19602, 'deDE', 'Konstruktionspläne der Venture Co.', '', NULL, 23420),
(203157, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203156, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(203164, 'deDE', 'Hordenkäfig', '', NULL, 23420),
(195220, 'deDE', 'Stonetalon Fire (Large)', '', NULL, 23420),
(19603, 'deDE', 'Dokumente der Venture Co.', '', NULL, 23420),
(195217, 'deDE', 'Feuer des Steinkrallengebirges', '', NULL, 23420),
(194474, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194475, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194477, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194476, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(92424, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204799, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(203186, 'deDE', 'BETRETEN VERBOTEN!', '', NULL, 23420),
(203141, 'deDE', 'Briefkasten', '', NULL, 23420),
(203146, 'deDE', 'Schmiede', '', NULL, 23420),
(203145, 'deDE', 'Amboss', '', NULL, 23420),
(203175, 'deDE', 'Mechanisierte Luft', '', NULL, 23420),
(176506, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(176509, 'deDE', 'Schmiede', '', NULL, 23420),
(176508, 'deDE', 'Amboss', '', NULL, 23420),
(176507, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(143983, 'deDE', 'Briefkasten', '', NULL, 23420),
(207474, 'deDE', 'Silberbeschlagene Schatzkiste', '', NULL, 23420),
(203214, 'deDE', 'Relikt von Eldre''thar', '', NULL, 23420),
(203216, 'deDE', 'Relikt von Eldre''thar', '', NULL, 23420),
(203215, 'deDE', 'Relikt von Eldre''thar', '', NULL, 23420),
(203431, 'deDE', 'Zelt der Nordwacht', '', NULL, 23420),
(203294, 'deDE', 'Logbuch von Krom''gar', '', NULL, 23420),
(203449, 'deDE', 'Kotfliegen', '', NULL, 23420),
(203448, 'deDE', 'Dampfender Wyvernkothaufen', '', NULL, 23420),
(202572, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(105173, 'deDE', 'Kessel', '', NULL, 23420),
(194438, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(203377, 'deDE', 'Schmiede', '', NULL, 23420),
(203376, 'deDE', 'Amboss', '', NULL, 23420),
(203374, 'deDE', 'Zeichenbuch der Urahnin Sareth''na', '', NULL, 23420),
(207522, 'deDE', 'Schatzkiste aus Ahornholz', '', NULL, 23420),
(208095, 'deDE', 'Briefkasten', '', NULL, 23420),
(1621, 'deDE', 'Wilddornrose', '', NULL, 23420),
(3730, 'deDE', 'Beulengras', '', NULL, 23420),
(177280, 'deDE', 'Mondbrunnen', '', NULL, 23420),
(1731, 'deDE', 'Kupferader', '', NULL, 23420),
(203384, 'deDE', 'Seldarrias Ei', '', NULL, 23420),
(1620, 'deDE', 'Maguskönigskraut', '', NULL, 23420),
(11899, 'deDE', 'Mesafahrstuhl', '', NULL, 23420),
(1732, 'deDE', 'Zinnader', '', NULL, 23420),
(11898, 'deDE', 'Mesafahrstuhl', '', NULL, 23420),
(180549, 'deDE', 'Geringer Windstein', '', NULL, 23420),
(180439, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(180564, 'deDE', 'Geringer Windstein', '', NULL, 23420),
(180559, 'deDE', 'Großer Windstein', '', NULL, 23420),
(180438, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(180554, 'deDE', 'Windstein', '', NULL, 23420),
(180436, 'deDE', 'Zwielichtschrifttafelfragment', '', NULL, 23420),
(180583, 'deDE', 'Zwielichtschrifttafelfragment', '', NULL, 23420),
(180501, 'deDE', 'Zwielichtschrifttafelfragment', '', NULL, 23420),
(180529, 'deDE', 'Geringer Windstein', '', NULL, 23420),
(180441, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(180534, 'deDE', 'Windstein', '', NULL, 23420),
(180440, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(180539, 'deDE', 'Großer Windstein', '', NULL, 23420),
(180544, 'deDE', 'Geringer Windstein', '', NULL, 23420),
(180503, 'deDE', 'Sandiges Kochbuch', '', NULL, 23420),
(180442, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(180455, 'deDE', 'Glyphenverzierter Kristall des Zoraschwarms', '', NULL, 23420),
(181635, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(207853, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(181618, 'deDE', 'ToWoW - Flaggezähler, Allianz', '', NULL, 23420),
(181603, 'deDE', 'Sillithusflaggenmast, Allianz', '', NULL, 23420),
(207513, 'deDE', 'Seidene Schatzkiste', '', NULL, 23420),
(180454, 'deDE', 'Glyphenverzierter Kristall des Ashischwarms', '', NULL, 23420),
(180474, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180480, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180477, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180475, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180473, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180479, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180478, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180476, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180444, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(180443, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(179344, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176589, 'deDE', 'Schwarzer Lotus', '', NULL, 23420),
(180915, 'deDE', 'Kochkohlenpfanne', '', NULL, 23420),
(180914, 'deDE', 'Amboss', '', NULL, 23420),
(180451, 'deDE', 'Briefkasten', '', NULL, 23420),
(180448, 'deDE', 'Steckbrief: Totenstachel', '', NULL, 23420),
(180913, 'deDE', 'Schmiede', '', NULL, 23420),
(207566, 'deDE', 'Portal zu den Verwüsteten Landen', '', NULL, 23420),
(178553, 'deDE', 'Kapsel des Ashischwarms', '', NULL, 23420),
(179565, 'deDE', 'Staubiger Reliquienschrein', '', NULL, 23420),
(2047, 'deDE', 'Echtsilbervorkommen', '', NULL, 23420),
(176583, 'deDE', 'Goldener Sansam', '', NULL, 23420),
(181634, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(181633, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(206915, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180518, 'deDE', 'Geringer Windstein', '', NULL, 23420),
(206931, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206917, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206916, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206930, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206582, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(206918, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206920, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206919, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(181619, 'deDE', 'ToWoW - Flaggezähler, Horde', '', NULL, 23420),
(181617, 'deDE', 'Sillithusflaggenmast, Horde', '', NULL, 23420),
(176584, 'deDE', 'Traumblatt', '', NULL, 23420),
(206929, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206928, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180466, 'deDE', 'Großer Windstein', '', NULL, 23420),
(180461, 'deDE', 'Windstein', '', NULL, 23420),
(176586, 'deDE', 'Bergsilbersalbei', '', NULL, 23420),
(206927, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206926, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206925, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206924, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206922, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206921, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206581, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(206923, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(180502, 'deDE', 'Kultistenfalle', '', NULL, 23420),
(180456, 'deDE', 'Geringer Windstein', '', NULL, 23420),
(206914, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206913, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(142142, 'deDE', 'Sonnengras', '', NULL, 23420),
(153402, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(153400, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(153399, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(180760, 'deDE', 'Heldenporträt eines Verlassenen', '', NULL, 23420),
(177266, 'deDE', 'Brücke zur Anhöhe der Ältesten', '', NULL, 23420),
(177265, 'deDE', 'Brücke zur Anhöhe der Geister', '', NULL, 23420),
(3286, 'deDE', 'Loderndes Feuer', '', NULL, 23420),
(177269, 'deDE', 'Brücke zur Anhöhe der Geister', '', NULL, 23420),
(50469, 'deDE', 'Schmiede von Donnerfels', '', NULL, 23420),
(50468, 'deDE', 'Amboss', '', NULL, 23420),
(3296, 'deDE', 'Versengendes Feuer', '', NULL, 23420),
(50449, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(50448, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(50447, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(50446, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(50445, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(50450, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(207323, 'deDE', 'Auf Befehl des Kriegshäuptlings', '', NULL, 23420),
(187295, 'deDE', 'Gildentresor', '', NULL, 23420),
(177270, 'deDE', 'Brücke zur Anhöhe der Jäger', '', NULL, 23420),
(143985, 'deDE', 'Briefkasten', '', NULL, 23420),
(3298, 'deDE', 'Loderndes Feuer', '', NULL, 23420),
(152583, 'deDE', 'Auktion_Nodus', '', NULL, 23420),
(160426, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(3315, 'deDE', 'Siedender Kessel', '', NULL, 23420),
(3303, 'deDE', 'Warmes Feuer', '', NULL, 23420),
(180776, 'deDE', 'Banner', '', NULL, 23420),
(177268, 'deDE', 'Brücke zur Anhöhe der Jäger', '', NULL, 23420),
(178571, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(177267, 'deDE', 'Brücke zur Anhöhe der Ältesten', '', NULL, 23420),
(180880, 'deDE', 'AmmoBoxBlue', '', NULL, 23420),
(180883, 'deDE', 'AmmoBoxRedBlock', '', NULL, 23420),
(180882, 'deDE', 'AmmoBoxRed', '', NULL, 23420),
(180881, 'deDE', 'AmmoBoxBlueBlock', '', NULL, 23420),
(180878, 'deDE', 'GunShopFireworksBarrel', '', NULL, 23420),
(180873, 'deDE', 'Knallfrösche', '', NULL, 23420),
(180872, 'deDE', 'Knallfrösche', '', NULL, 23420),
(180868, 'deDE', 'Zünder für Feuerwerk', '', NULL, 23420),
(180869, 'deDE', 'Zünder für Raketenbündel', '', NULL, 23420),
(74730, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(177209, 'deDE', 'Hass der Zentauren', '', NULL, 23420),
(177207, 'deDE', 'Nebel der Dämmerung', '', NULL, 23420),
(177204, 'deDE', 'Trauer der Erdenmutter', '', NULL, 23420),
(74728, 'deDE', 'Rauchfleischgestell', '', NULL, 23420),
(74727, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(177208, 'deDE', 'Der weiße Hirsch und der Mond', '', NULL, 23420),
(177205, 'deDE', 'Waldlord und die ersten Druiden', '', NULL, 23420),
(74729, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(2909, 'deDE', 'Wasserpumpe der Wildmähnen', '', NULL, 23420),
(57708, 'deDE', 'Laternenmast', '', NULL, 23420),
(208867, 'deDE', 'Glänzende Steine', '', NULL, 23420),
(208889, 'deDE', 'Frischgefangener Fisch', '', NULL, 23420),
(2911, 'deDE', 'Brunnen der Sturmbullen', '', NULL, 23420),
(2904, 'deDE', 'Brunnensäubernde Aura', '', NULL, 23420),
(3223, 'deDE', 'Schmiede', '', NULL, 23420),
(3222, 'deDE', 'Amboss', '', NULL, 23420),
(142118, 'deDE', 'Blubbernder Kessel', '', NULL, 23420),
(23879, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(2732, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(2913, 'deDE', 'Brunnen der Winterhufe', '', NULL, 23420),
(2910, 'deDE', 'Quellstein', '', NULL, 23420),
(3311, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(21680, 'deDE', 'Duellflagge', '', NULL, 23420),
(186264, 'deDE', 'Verzweifelt gesucht: Kyle', '', NULL, 23420),
(50549, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(50548, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(50547, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(15069, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(74445, 'deDE', 'Rauchfleischgestell', '', NULL, 23420),
(143984, 'deDE', 'Briefkasten', '', NULL, 23420),
(74443, 'deDE', 'Rauchfleischgestell', '', NULL, 23420),
(74444, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(74442, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(74441, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(74440, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(15068, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(3656, 'deDE', 'Feuer', '', NULL, 23420),
(74439, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(18035, 'deDE', 'Stammesfeuer', '', NULL, 23420),
(2914, 'deDE', 'Stammesfeuer', '', NULL, 23420),
(201942, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(201717, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(202588, 'deDE', 'Briefkasten', '', NULL, 23420),
(204597, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204595, 'deDE', 'Kessel', '', NULL, 23420),
(202112, 'deDE', 'Stacheleberkäfig', '', NULL, 23420),
(3076, 'deDE', 'Schmutzbefleckte Karte', '', NULL, 23420),
(204596, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(180056, 'deDE', 'Geheimnisvoller Baumstumpf', '', NULL, 23420),
(3310, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(208890, 'deDE', 'Frischgejagtes Geflügel', '', NULL, 23420),
(207472, 'deDE', 'Silberbeschlagene Schatzkiste', '', NULL, 23420),
(195142, 'deDE', 'Portal zur Höllenfeuerhalbinsel', '', NULL, 23420),
(208880, 'deDE', 'Maiskörner', '', NULL, 23420),
(181273, 'deDE', 'Beschwörungskreis', '', NULL, 23420),
(208875, 'deDE', 'Kienapfel von Mulgore', '', NULL, 23420),
(208878, 'deDE', '"Magischer" Pilz', '', NULL, 23420),
(208887, 'deDE', 'Süßkartoffeln von Mulgore', '', NULL, 23420),
(208888, 'deDE', 'Saftige Gewürzkräuter', '', NULL, 23420),
(204598, 'deDE', 'Ofen', '', NULL, 23420),
(201697, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3308, 'deDE', 'Feuer', '', NULL, 23420),
(2908, 'deDE', 'Versiegelte Vorratskiste', '', NULL, 23420),
(186287, 'deDE', 'Käfig der Schwarzhufe', '', NULL, 23420),
(1618, 'deDE', 'Friedensblume', '', NULL, 23420),
(50910, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(2912, 'deDE', 'Bernsteinkorn', '', NULL, 23420),
(1617, 'deDE', 'Silberblatt', '', NULL, 23420),
(142131, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(1619, 'deDE', 'Erdwurzel', '', NULL, 23420),
(144063, 'deDE', 'Monolith von Equinex', '', NULL, 23420),
(142188, 'deDE', 'Flamme von Samha', '', NULL, 23420),
(202222, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(202085, 'deDE', 'Amboss', '', NULL, 23420),
(142091, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(142090, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(202606, 'deDE', 'Steinträne', '', NULL, 23420),
(142185, 'deDE', 'Flamme von Byltan', '', NULL, 23420),
(142186, 'deDE', 'Flamme von Lahassa', '', NULL, 23420),
(142187, 'deDE', 'Flamme von Imbel', '', NULL, 23420),
(206589, 'deDE', 'Vermessungsgerät (gelb)', '', NULL, 23420),
(204272, 'deDE', 'Vermessungsgerät (grün)', '', NULL, 23420),
(206590, 'deDE', 'Vermessungsgerät (rot)', '', NULL, 23420),
(142073, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(177706, 'deDE', 'Melizzas Käfig', '', NULL, 23420),
(177444, 'deDE', 'Steintür', '', NULL, 23420),
(202221, 'deDE', 'Briefkasten', '', NULL, 23420),
(202115, 'deDE', 'Amboss', '', NULL, 23420),
(202114, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(180685, 'deDE', 'Schwimmende Wrackteile', '', NULL, 23420),
(142179, 'deDE', 'Pavillon von Solarsal', '', NULL, 23420),
(175732, 'deDE', 'Die Schildwachen und die lange Wacht', '', NULL, 23420),
(204989, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(204990, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(202107, 'deDE', 'Briefkasten', '', NULL, 23420),
(164909, 'deDE', 'Havariertes Ruderboot', '', NULL, 23420),
(208203, 'deDE', 'Leere Kaja''Cola-Dose', '', NULL, 23420),
(208322, 'deDE', 'Barbecue', '', NULL, 23420),
(208324, 'deDE', 'Barbecue', '', NULL, 23420),
(208323, 'deDE', 'Barbecue', '', NULL, 23420),
(208201, 'deDE', 'Leere Kaja''Cola-Dose', '', NULL, 23420),
(208202, 'deDE', 'Leere Kaja''Cola-Dose', '', NULL, 23420),
(179510, 'deDE', 'Fackel', '', NULL, 23420),
(207535, 'deDE', 'Schatzkiste aus Runensteinen', '', NULL, 23420),
(206751, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(179508, 'deDE', 'Fackel', '', NULL, 23420),
(206753, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206750, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206747, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(179507, 'deDE', 'Fackel', '', NULL, 23420),
(206752, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206749, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(206748, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(178224, 'deDE', 'Düsterteich', '', NULL, 23420),
(204984, 'deDE', 'Schmiede', '', NULL, 23420),
(204983, 'deDE', 'Amboss', '', NULL, 23420),
(204982, 'deDE', 'Kessel', '', NULL, 23420),
(204985, 'deDE', 'Briefkasten', '', NULL, 23420),
(153126, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(50989, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(50988, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(153125, 'deDE', 'Kessel', '', NULL, 23420),
(153124, 'deDE', 'Kessel', '', NULL, 23420),
(148841, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(50987, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(50986, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(148840, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176278, 'deDE', 'Kessel', '', NULL, 23420),
(176318, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(204981, 'deDE', 'Schmiede', '', NULL, 23420),
(204980, 'deDE', 'Amboss', '', NULL, 23420),
(184396, 'deDE', 'Feuer', '', NULL, 23420),
(184395, 'deDE', 'Feuer', '', NULL, 23420),
(202164, 'deDE', 'Kessel', '', NULL, 23420),
(176283, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176284, 'deDE', 'Kessel', '', NULL, 23420),
(178826, 'deDE', 'Versammlungsstein', '', NULL, 23420),
(176285, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176286, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176281, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176279, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176280, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176288, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(176287, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(110236, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(164953, 'deDE', 'Große Lederrucksäcke', '', NULL, 23420),
(110235, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(153581, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(40301, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(206614, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(41185, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(153580, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(142195, 'deDE', 'Schlachtplan der Waldpfoten', '', NULL, 23420),
(110234, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(41186, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(41187, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(41192, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(41191, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(41189, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(41190, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(41188, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(203134, 'deDE', 'Leeres Podest', '', NULL, 23420),
(202793, 'deDE', 'Lockere Erde', '', NULL, 23420),
(164954, 'deDE', 'Kapsel der Zukk''ash', '', NULL, 23420),
(153584, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(41193, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(110231, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(110232, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(143986, 'deDE', 'Briefkasten', '', NULL, 23420),
(41194, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(110233, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(40298, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(153579, 'deDE', 'Kessel', '', NULL, 23420),
(153576, 'deDE', 'Kessel', '', NULL, 23420),
(179513, 'deDE', 'Fackel', '', NULL, 23420),
(141611, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(143980, 'deDE', 'Gordunnischriftrolle', '', NULL, 23420),
(153577, 'deDE', 'Kessel', '', NULL, 23420),
(141610, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(144050, 'deDE', 'Gordunnifalle', '', NULL, 23420),
(49487, 'deDE', 'Brennende Glutspäne', '', NULL, 23420),
(49486, 'deDE', 'Warmes Feuer', '', NULL, 23420),
(49485, 'deDE', 'Warmes Feuer', '', NULL, 23420),
(153582, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(38030, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(38029, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(38028, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(40197, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(141069, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(177192, 'deDE', 'Tür', '', NULL, 23420),
(177189, 'deDE', 'Tür', '', NULL, 23420),
(177188, 'deDE', 'Tür', '', NULL, 23420),
(1733, 'deDE', 'Silberader', '', NULL, 23420),
(179469, 'deDE', 'Arenatür', '', NULL, 23420),
(2043, 'deDE', 'Khadgars Schnurrbart', '', NULL, 23420),
(2042, 'deDE', 'Blassblatt', '', NULL, 23420),
(3722, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(19016, 'deDE', 'Mit Sternenstaub bedeckter Busch', '', NULL, 23420),
(203020, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(194620, 'deDE', 'Briefkasten', '', NULL, 23420),
(194619, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(195024, 'deDE', 'Leuchtfeuer', '', NULL, 23420),
(178247, 'deDE', 'Nagakohlenpfanne', '', NULL, 23420),
(206854, 'deDE', 'Stratholme Fire Large', '', NULL, 23420),
(191369, 'deDE', 'Cosmetic Object - Fire Large', '', NULL, 23420),
(207534, 'deDE', 'Schatzkiste aus Runensteinen', '', NULL, 23420),
(195012, 'deDE', 'Versunkenes Altmetall', '', NULL, 23420),
(179489, 'deDE', 'Durchnässte Schließkiste', '', NULL, 23420),
(178186, 'deDE', 'Saphir von Aku''mai', '', NULL, 23420),
(178184, 'deDE', 'Saphir von Aku''mai', '', NULL, 23420),
(178828, 'deDE', 'Versammlungsstein', '', NULL, 23420),
(17783, 'deDE', 'Antike Statuette', '', NULL, 23420),
(17284, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194202, 'deDE', 'Wildfeuertrank', '', NULL, 23420),
(17282, 'deDE', 'Bathranshaar', '', NULL, 23420),
(93192, 'deDE', 'Herzholz', '', NULL, 23420),
(194296, 'deDE', 'Low Poly Fire - Small (with animations)', '', NULL, 23420),
(194651, 'deDE', 'Licht von Elune', '', NULL, 23420),
(20960, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(178144, 'deDE', 'Trolltruhe', '', NULL, 23420),
(20961, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194311, 'deDE', 'Roter Mondstein', '', NULL, 23420),
(194310, 'deDE', 'Grüner Mondstein', '', NULL, 23420),
(194309, 'deDE', 'Blauer Mondstein', '', NULL, 23420),
(20963, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(176999, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(176998, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(20962, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194779, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(195079, 'deDE', 'Liegengebliebener Wagen', '', NULL, 23420),
(3723, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194803, 'deDE', 'Briefkasten', '', NULL, 23420),
(175104, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(206838, 'deDE', 'Night Elf Hero Portrait (Scale 2)', '', NULL, 23420),
(142117, 'deDE', 'Briefkasten', '', NULL, 23420),
(175179, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194778, 'deDE', 'Amboss', '', NULL, 23420),
(194777, 'deDE', 'Schmiede', '', NULL, 23420),
(175180, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175181, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(195077, 'deDE', 'Mondberührter Ton', '', NULL, 23420),
(208790, 'deDE', 'Nachtelfisches Grab', '', NULL, 23420),
(206837, 'deDE', 'Banner (Scale 2)', '', NULL, 23420),
(194809, 'deDE', 'Briefkasten', '', NULL, 23420),
(194175, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(3220, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(18643, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(18645, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(18644, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(20806, 'deDE', 'Mondbrunnen des Eschentals', '', NULL, 23420),
(18596, 'deDE', 'Knisterndes Lagerfeuer', '', NULL, 23420),
(195138, 'deDE', 'Kupferbeschlag', '', NULL, 23420),
(195136, 'deDE', 'Bronzerad', '', NULL, 23420),
(195135, 'deDE', 'Klemmbolzen', '', NULL, 23420),
(19015, 'deDE', 'Elunes Träne', '', NULL, 23420),
(195134, 'deDE', 'Die Bombe', '', NULL, 23420),
(240617, 'deDE', 'Beutel der verlorenen Schildwache', '', NULL, 23420),
(19024, 'deDE', 'Verborgener Schrein', '', NULL, 23420),
(177277, 'deDE', 'Mondbrunnen', '', NULL, 23420),
(195005, 'deDE', 'Lava', '', NULL, 23420),
(195002, 'deDE', 'Lavafelsspalt', '', NULL, 23420),
(195110, 'deDE', 'Licht von Elune', '', NULL, 23420),
(195111, 'deDE', 'Osos Truhe', '', NULL, 23420),
(178146, 'deDE', 'Gurdas Schredder', '', NULL, 23420),
(203823, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(203822, 'deDE', 'Kohlenpfanne', '', NULL, 23420),
(204875, 'deDE', 'Schutthaufen', '', NULL, 23420),
(178864, 'deDE', 'Briefkasten', '', NULL, 23420),
(194566, 'deDE', 'Frisches Geröll', '', NULL, 23420),
(194263, 'deDE', 'Brauchbarer Pfeil', '', NULL, 23420),
(177225, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(20724, 'deDE', 'Säule von Dor''Danil', '', NULL, 23420),
(20725, 'deDE', 'Das Vermächtnis der Aspekte', '', NULL, 23420),
(194550, 'deDE', 'Das Herz des Waldes', '', NULL, 23420),
(194549, 'deDE', 'Das Herz des Waldes', '', NULL, 23420),
(194990, 'deDE', 'Alliance Breadcrumb', '', NULL, 23420),
(176784, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(21004, 'deDE', 'Denkmal für Grom Höllschrei', '', NULL, 23420),
(194493, 'deDE', 'Dämonentor', '', NULL, 23420),
(194615, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194616, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(177194, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181913, 'deDE', 'Elunes Kelch', '', NULL, 23420),
(181681, 'deDE', 'Elunes Kelch', '', NULL, 23420),
(177195, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(177197, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(6288, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(178195, 'deDE', 'Öl des Kriegshymnenklans', '', NULL, 23420),
(20964, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(194465, 'deDE', 'Rituelle Rune', '', NULL, 23420),
(194464, 'deDE', 'Ritualedelstein', '', NULL, 23420),
(177196, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181916, 'deDE', 'Verdorbenes Satyrnaarholz', '', NULL, 23420),
(180758, 'deDE', 'Heldenporträt eines Nachtelfen', '', NULL, 23420),
(175285, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175286, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(175284, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(140113, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(181690, 'deDE', 'Fruchtbarer Erdhaufen', '', NULL, 23420),
(194458, 'deDE', 'Teufelsfeuer', '', NULL, 23420),
(194617, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(6292, 'deDE', 'Schwelende Kohlenpfanne', '', NULL, 23420),
(6291, 'deDE', 'Schwelende Kohlenpfanne', '', NULL, 23420),
(140112, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(140111, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(140110, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(140109, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(6290, 'deDE', 'Schwelende Lohe', '', NULL, 23420),
(6289, 'deDE', 'Schwelende Lohe', '', NULL, 23420),
(194613, 'deDE', 'Schwelende Kohlenpfanne', '', NULL, 23420),
(203460, 'deDE', 'Feuerbrandfalle', '', NULL, 23420),
(194997, 'deDE', 'Dorniger Blutkelch', '', NULL, 23420),
(140105, 'deDE', 'Wilde Lohe', '', NULL, 23420),
(181686, 'deDE', 'Holzstapel', '', NULL, 23420),
(194482, 'deDE', 'Sprengstoff der Horde', '', NULL, 23420),
(19904, 'deDE', 'Mok''Morokks Schnupftabak', '', NULL, 23420),
(185321, 'deDE', 'Versammlungsstein', '', NULL, 23420),
(174728, 'deDE', 'Beschädigte Kiste', '', NULL, 23420),
(20727, 'deDE', 'Gizmoriumtransportkiste', '', NULL, 23420),
(20359, 'deDE', 'Ei von Onyxia', '', NULL, 23420),
(214538, 'deDE', 'Instance Portal (Raid 10/25 No Heroic)', '', NULL, 23420),
(186332, 'deDE', 'Überreste eines Ogers', '', NULL, 23420),
(19905, 'deDE', 'Mok''Morokks Grog', '', NULL, 23420),
(19906, 'deDE', 'Mok''Morokks Geldkassette', '', NULL, 23420),
(186330, 'deDE', 'Banner der Steinbrecher', '', NULL, 23420),
(186329, 'deDE', 'Klanbanner der Steinbrecher', '', NULL, 23420),
(186463, 'deDE', 'Drachenwinde', '', NULL, 23420),
(186230, 'deDE', 'Briefkasten', '', NULL, 23420),
(186232, 'deDE', 'Amboss', '', NULL, 23420),
(186231, 'deDE', 'Schmiede', '', NULL, 23420),
(186426, 'deDE', 'Steckbrief', '', NULL, 23420),
(186233, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(2555, 'deDE', 'Modrige Rolle', '', NULL, 23420),
(22234, 'deDE', 'Signalfackel', '', NULL, 23420),
(20992, 'deDE', 'Schwarzer Schild', '', NULL, 23420),
(21127, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(186441, 'deDE', 'Kraftkernfragment', '', NULL, 23420),
(186450, 'deDE', 'Zeppelinfracht', '', NULL, 23420),
(186629, 'deDE', 'Briefkasten', '', NULL, 23420),
(205332, 'deDE', 'Steckbrief', '', NULL, 23420),
(186631, 'deDE', 'Amboss', '', NULL, 23420),
(186630, 'deDE', 'Schmiede', '', NULL, 23420),
(20849, 'deDE', 'Freudenfeuer', '', NULL, 23420),
(21459, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(21042, 'deDE', 'Abzeichen der Wache von Theramore', '', NULL, 23420),
(187273, 'deDE', 'Verdächtiger Hufabdruck', '', NULL, 23420),
(187272, 'deDE', 'Gewöhnlicher Hufabdruck', '', NULL, 23420),
(20968, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(205267, 'deDE', 'Graunebelei', '', NULL, 23420),
(202596, 'deDE', 'Sprengsatz von Frazzelcraz', '', NULL, 23420),
(20966, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(20965, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(181626, 'deDE', 'Verformte Kisten', '', NULL, 23420),
(186418, 'deDE', 'Mordants Geschenk', '', NULL, 23420),
(186301, 'deDE', 'Waffen der Schwarzhufe', '', NULL, 23420),
(20985, 'deDE', 'Lockere Erde', '', NULL, 23420),
(187252, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(186423, 'deDE', 'Hexenfluch', '', NULL, 23420),
(20982, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(266102, 'deDE', 'Gnomenmaschine', '', NULL, 23420),
(266114, 'deDE', 'Gnomenmaschine', '', NULL, 23420),
(266107, 'deDE', 'Gnomenmaschine', '', NULL, 23420),
(266116, 'deDE', 'Gnomenmaschine', '', NULL, 23420),
(266115, 'deDE', 'Gnomenmaschine', '', NULL, 23420),
(266113, 'deDE', 'Gnomenmaschine', '', NULL, 23420),
(266108, 'deDE', 'Gnomenmaschine', '', NULL, 23420),
(266109, 'deDE', 'Gnomenmaschine', '', NULL, 23420),
(179085, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(202300, 'deDE', 'Strickleiter', '', NULL, 23420),
(202298, 'deDE', 'Strickleiter', '', NULL, 23420),
(20829, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(207475, 'deDE', 'Silberbeschlagene Schatzkiste', '', NULL, 23420),
(20830, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(20831, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(186283, 'deDE', 'Zeppelintrümmer', '', NULL, 23420),
(186278, 'deDE', 'Brennende Wrackteile', '', NULL, 23420),
(186273, 'deDE', 'Beschädigte Tauchausrüstung', '', NULL, 23420),
(179086, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(186272, 'deDE', 'Werkzeugsatz', '', NULL, 23420),
(186243, 'deDE', 'Lagerfeuer', '', NULL, 23420),
(208948, 'deDE', 'Gildentresor', '', NULL, 23420),
(142095, 'deDE', 'Briefkasten', '', NULL, 23420),
(186322, 'deDE', 'Monument der Familie Hyal', '', NULL, 23420),
(186266, 'deDE', 'Propagandamaterial der Deserteure', '', NULL, 23420),
(20925, 'deDE', 'Schließkiste des Kapitäns', '', NULL, 23420);


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text     xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_offer_reward` WHERE `ID` IN (34774 /*34774*/, 8717 /*8717*/, 8680 /*8680*/, 8670 /*8670*/, 29742 /*29742*/, 8724 /*8724*/, 8684 /*8684*/, 8671 /*8671*/, 8681 /*8681*/, 29740 /*29740*/, 29177 /*29177*/, 24758 /*24758*/, 8723 /*8723*/, 8715 /*8715*/, 8872 /*8872*/, 8718 /*8718*/, 8679 /*8679*/, 8726 /*8726*/, 8672 /*8672*/, 29741 /*29741*/, 8686 /*8686*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(34774, 0, 0, 0, 0, 0, 0, 0, 0, 'Ihr werdet dieses Geschenk annehmen und es wird Euch gefallen. Solltet Ihr das nicht tun, werden automatisch meine organischen Einäscherungswaffen aktiviert. Danke.', 23360), -- 34774
(8717, 1, 0, 0, 0, 0, 0, 0, 0, 'Der Himmel, mit seinen unzähligen Sternen, bietet die Antworten auf alle irdenen Fragen, $n. Mag es sein, dass wenn der Weise und Glückliche zu ihnen aufblickt, er die tiefere Wahrheit findet?', 23360), -- 8717
(8680, 1, 0, 0, 0, 0, 0, 0, 0, 'Ich spüre das Feuer des Lebens in Euch, $C. Ich akzeptiere Eure Ehrerbietung! Nehmt dies als Zeichen meiner Dankbarkeit...', 23360), -- 8680
(8670, 1, 0, 0, 0, 0, 0, 0, 0, 'Ich spüre das Feuer des Lebens in Euch, $C. Ich akzeptiere Eure Ehrerbietung! Nehmt dies als Zeichen meiner Dankbarkeit...', 23360), -- 8670
(29742, 1, 0, 0, 0, 0, 0, 0, 0, 'Alle Tol''vir waren einst wie ich. Nun ist mein Volk aus Fleisch und Blut und braucht die Weisheit der Zeitalter dringender als je zuvor.', 23360), -- 29742
(8724, 1, 0, 0, 0, 0, 0, 0, 0, 'Der Himmel, mit seinen unzähligen Sternen, bietet die Antworten auf alle irdenen Fragen, $n. Mag es sein, dass wenn der Weise und Glückliche zu ihnen aufblickt, er die tiefere Wahrheit findet?', 23420), -- 8724
(8684, 1, 0, 0, 0, 0, 0, 0, 0, 'Ich spüre das Feuer des Lebens in Euch, $C. Ich akzeptiere Eure Ehrerbietung! Nehmt dies als Zeichen meiner Dankbarkeit...', 23360), -- 8684
(8671, 1, 0, 0, 0, 0, 0, 0, 0, 'Ich spüre das Feuer des Lebens in Euch, $C. Ich akzeptiere Eure Ehrerbietung! Nehmt dies als Zeichen meiner Dankbarkeit...', 23360), -- 8671
(8681, 1, 0, 0, 0, 0, 0, 0, 0, 'Ich spüre das Feuer des Lebens in Euch, $C. Ich akzeptiere Eure Ehrerbietung! Nehmt dies als Zeichen meiner Dankbarkeit...', 23420), -- 8681
(29740, 1, 0, 0, 0, 0, 0, 0, 0, 'Im endlosen Glitzern der Sterne verbergen sich die Antworten auf alle irdischen Mysterien, $n. Darf man dann nicht annehmen, dass der Weise und der Glückspilz beim Blick nach oben die Wahrheit entdecken könnten?', 23420), -- 29740
(29177, 1, 2, 0, 0, 0, 0, 0, 0, 'Gut gemacht, $C. Ich hoffe, Ihr vergesst uns hier in der Brutstätte nicht.', 23420), -- 29177
(24758, 6, 5, 0, 0, 0, 60, 0, 0, 'Noch so ein Jungblut, wie?$B$BDafür seht Ihr aber ganz schön erfahren aus. Weise Augen. Vielleicht wird aus Euch noch $gein echter:eine echte:c; $C.$B$BLasst uns keine Zeit mehr verschwenden.', 23360), -- 24758
(8723, 1, 0, 0, 0, 0, 0, 0, 0, 'Der Himmel, mit seinen unzähligen Sternen, bietet die Antworten auf alle irdenen Fragen, $n. Mag es sein, dass wenn der Weise und Glückliche zu ihnen aufblickt, er die tiefere Wahrheit findet?', 23420), -- 8723
(8715, 1, 0, 0, 0, 0, 0, 0, 0, 'Der Himmel, mit seinen unzähligen Sternen, bietet die Antworten auf alle irdenen Fragen, $n. Mag es sein, dass wenn der Weise und Glückliche zu ihnen aufblickt, er die tiefere Wahrheit findet?', 23420), -- 8715
(8872, 0, 0, 0, 0, 0, 0, 0, 0, 'Seid gegrüßt, $n. Seid Ihr gekommen, um an den Festlichkeiten teilzunehmen?', 23420), -- 8872
(8718, 1, 0, 0, 0, 0, 0, 0, 0, 'Der Himmel, mit seinen unzähligen Sternen, bietet die Antworten auf alle irdenen Fragen, $n. Mag es sein, dass wenn der Weise und Glückliche zu ihnen aufblickt, er die tiefere Wahrheit findet?', 23420), -- 8718
(8679, 1, 0, 0, 0, 0, 0, 0, 0, 'Ich spüre das Feuer des Lebens in Euch, $C. Ich akzeptiere Eure Ehrerbietung! Nehmt dies als Zeichen meiner Dankbarkeit...', 23420), -- 8679
(8726, 1, 0, 0, 0, 0, 0, 0, 0, 'Der Himmel, mit seinen unzähligen Sternen, bietet die Antworten auf alle irdenen Fragen, $n. Mag es sein, dass wenn der Weise und Glückliche zu ihnen aufblickt, er die tiefere Wahrheit findet?', 23420), -- 8726
(8672, 1, 0, 0, 0, 0, 0, 0, 0, 'Ich spüre das Feuer des Lebens in Euch, $C. Ich akzeptiere Eure Ehrerbietung! Nehmt dies als Zeichen meiner Dankbarkeit...', 23420), -- 8672
(29741, 1, 0, 0, 0, 0, 0, 0, 0, 'Alle Tol''vir waren einst wie ich. Nun besteht mein Volk aus Fleisch und Blut und benötigt die Weisheit der Zeitalter mehr als je zuvor.', 23360), -- 29741
(8686, 1, 0, 0, 0, 0, 0, 0, 0, 'Ich spüre das Feuer des Lebens in Euch, $C. Ich akzeptiere Eure Ehrerbietung! Nehmt dies als Zeichen meiner Dankbarkeit...', 23360); -- 8686


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text     xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_greeting` WHERE (`ID`=40109 AND `Type`=0) OR (`ID`=48459 AND `Type`=0) OR (`ID`=48349 AND `Type`=0) OR (`ID`=48339 AND `Type`=0);
INSERT INTO `quest_greeting` (`ID`, `Type`, `GreetEmoteType`, `GreetEmoteDelay`, `Greeting`, `VerifiedBuild`) VALUES
(40109, 0, 0, 0, '', 23360), -- 40109
(48459, 0, 396, 0, 'Es ist wundervoll, diesen Wald wachsen und gedeihen zu sehen.$B$BJedes geläuterte bisschen bereitet mir so viel Freude.', 23420), -- 48459
(48349, 0, 1, 0, 'Die Jagd nach Dämonen belohnt einen ganz anders als die Jagd nach großen Tieren. Das weiß nicht jeder zu schätzen.', 23420), -- 48349
(48339, 0, 1, 0, 'Grüße, $C! Seid Ihr gekommen, um den Wald zu läutern?', 23420); -- 48339


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=9821 AND `id`=2) OR (`menu_id`=9821 AND `id`=1) OR (`menu_id`=11814 AND `id`=1) OR (`menu_id`=11814 AND `id`=0) OR (`menu_id`=4140 AND `id`=0) OR (`menu_id`=12460 AND `id`=0) OR (`menu_id`=83 AND `id`=0) OR (`menu_id`=7475 AND `id`=0) OR (`menu_id`=7476 AND `id`=0) OR (`menu_id`=7477 AND `id`=0) OR (`menu_id`=7478 AND `id`=0) OR (`menu_id`=7455 AND `id`=0) OR (`menu_id`=7833 AND `id`=0) OR (`menu_id`=7459 AND `id`=0) OR (`menu_id`=8243 AND `id`=0) OR (`menu_id`=12433 AND `id`=0) OR (`menu_id`=6944 AND `id`=0) OR (`menu_id`=7469 AND `id`=0) OR (`menu_id`=7788 AND `id`=14) OR (`menu_id`=7788 AND `id`=13) OR (`menu_id`=7788 AND `id`=12) OR (`menu_id`=7788 AND `id`=11) OR (`menu_id`=7788 AND `id`=10) OR (`menu_id`=7788 AND `id`=9) OR (`menu_id`=7788 AND `id`=8) OR (`menu_id`=7788 AND `id`=7) OR (`menu_id`=7788 AND `id`=6) OR (`menu_id`=7788 AND `id`=5) OR (`menu_id`=7788 AND `id`=4) OR (`menu_id`=7788 AND `id`=3) OR (`menu_id`=7788 AND `id`=2) OR (`menu_id`=7788 AND `id`=1) OR (`menu_id`=7788 AND `id`=0) OR (`menu_id`=7777 AND `id`=9) OR (`menu_id`=7777 AND `id`=8) OR (`menu_id`=7777 AND `id`=7) OR (`menu_id`=7777 AND `id`=6) OR (`menu_id`=7777 AND `id`=5) OR (`menu_id`=7777 AND `id`=4) OR (`menu_id`=7777 AND `id`=3) OR (`menu_id`=7777 AND `id`=2) OR (`menu_id`=7777 AND `id`=1) OR (`menu_id`=7777 AND `id`=0) OR (`menu_id`=7787 AND `id`=7) OR (`menu_id`=7787 AND `id`=6) OR (`menu_id`=7787 AND `id`=5) OR (`menu_id`=7787 AND `id`=4) OR (`menu_id`=7787 AND `id`=3) OR (`menu_id`=7787 AND `id`=2) OR (`menu_id`=7787 AND `id`=1) OR (`menu_id`=7787 AND `id`=0) OR (`menu_id`=2343 AND `id`=9) OR (`menu_id`=2343 AND `id`=8) OR (`menu_id`=2343 AND `id`=7) OR (`menu_id`=2343 AND `id`=6) OR (`menu_id`=2343 AND `id`=5) OR (`menu_id`=2343 AND `id`=4) OR (`menu_id`=2343 AND `id`=3) OR (`menu_id`=2343 AND `id`=2) OR (`menu_id`=2343 AND `id`=1) OR (`menu_id`=2343 AND `id`=0) OR (`menu_id`=2352 AND `id`=9) OR (`menu_id`=2352 AND `id`=8) OR (`menu_id`=2352 AND `id`=7) OR (`menu_id`=2352 AND `id`=6) OR (`menu_id`=2352 AND `id`=5) OR (`menu_id`=2352 AND `id`=4) OR (`menu_id`=2352 AND `id`=3) OR (`menu_id`=2352 AND `id`=2) OR (`menu_id`=2352 AND `id`=1) OR (`menu_id`=2352 AND `id`=0) OR (`menu_id`=2351 AND `id`=14) OR (`menu_id`=2351 AND `id`=13) OR (`menu_id`=2351 AND `id`=12) OR (`menu_id`=2351 AND `id`=11) OR (`menu_id`=2351 AND `id`=10) OR (`menu_id`=2351 AND `id`=9) OR (`menu_id`=2351 AND `id`=8) OR (`menu_id`=2351 AND `id`=7) OR (`menu_id`=2351 AND `id`=6) OR (`menu_id`=2351 AND `id`=5) OR (`menu_id`=2351 AND `id`=4) OR (`menu_id`=2351 AND `id`=3) OR (`menu_id`=2351 AND `id`=2) OR (`menu_id`=2351 AND `id`=1) OR (`menu_id`=2351 AND `id`=0) OR (`menu_id`=11778 AND `id`=0) OR (`menu_id`=14047 AND `id`=8) OR (`menu_id`=14042 AND `id`=8) OR (`menu_id`=6586 AND `id`=0) OR (`menu_id`=6587 AND `id`=0) OR (`menu_id`=6588 AND `id`=0) OR (`menu_id`=6526 AND `id`=0) OR (`menu_id`=6528 AND `id`=0) OR (`menu_id`=1581 AND `id`=1) OR (`menu_id`=1581 AND `id`=0) OR (`menu_id`=8077 AND `id`=0) OR (`menu_id`=11071 AND `id`=0) OR (`menu_id`=12844 AND `id`=1) OR (`menu_id`=12844 AND `id`=0) OR (`menu_id`=12843 AND `id`=1) OR (`menu_id`=12843 AND `id`=0) OR (`menu_id`=4355 AND `id`=0) OR (`menu_id`=4004 AND `id`=1) OR (`menu_id`=12846 AND `id`=1) OR (`menu_id`=12846 AND `id`=0) OR (`menu_id`=10362 AND `id`=1) OR (`menu_id`=10362 AND `id`=0) OR (`menu_id`=12842 AND `id`=1) OR (`menu_id`=12842 AND `id`=0) OR (`menu_id`=4122 AND `id`=0) OR (`menu_id`=12850 AND `id`=1) OR (`menu_id`=12850 AND `id`=0) OR (`menu_id`=12848 AND `id`=2) OR (`menu_id`=12848 AND `id`=0) OR (`menu_id`=12847 AND `id`=1) OR (`menu_id`=12847 AND `id`=0) OR (`menu_id`=12852 AND `id`=1) OR (`menu_id`=12852 AND `id`=0) OR (`menu_id`=342 AND `id`=1) OR (`menu_id`=342 AND `id`=0) OR (`menu_id`=1161 AND `id`=0) OR (`menu_id`=14297 AND `id`=8) OR (`menu_id`=12517 AND `id`=1) OR (`menu_id`=12865 AND `id`=15) OR (`menu_id`=12865 AND `id`=14) OR (`menu_id`=12865 AND `id`=13) OR (`menu_id`=12865 AND `id`=12) OR (`menu_id`=12865 AND `id`=11) OR (`menu_id`=12865 AND `id`=10) OR (`menu_id`=12865 AND `id`=9) OR (`menu_id`=12865 AND `id`=8) OR (`menu_id`=12865 AND `id`=7) OR (`menu_id`=12865 AND `id`=6) OR (`menu_id`=12865 AND `id`=5) OR (`menu_id`=12865 AND `id`=4) OR (`menu_id`=12865 AND `id`=3) OR (`menu_id`=12865 AND `id`=2) OR (`menu_id`=12865 AND `id`=1) OR (`menu_id`=12865 AND `id`=0) OR (`menu_id`=8851 AND `id`=3) OR (`menu_id`=8851 AND `id`=2) OR (`menu_id`=8851 AND `id`=1) OR (`menu_id`=12855 AND `id`=7) OR (`menu_id`=12855 AND `id`=6) OR (`menu_id`=12855 AND `id`=5) OR (`menu_id`=12855 AND `id`=4) OR (`menu_id`=12855 AND `id`=3) OR (`menu_id`=12855 AND `id`=2) OR (`menu_id`=12855 AND `id`=1) OR (`menu_id`=12855 AND `id`=0) OR (`menu_id`=12591 AND `id`=0) OR (`menu_id`=12499 AND `id`=1) OR (`menu_id`=12499 AND `id`=0) OR (`menu_id`=9868 AND `id`=1) OR (`menu_id`=9868 AND `id`=0) OR (`menu_id`=11115 AND `id`=1) OR (`menu_id`=11115 AND `id`=0) OR (`menu_id`=11121 AND `id`=1) OR (`menu_id`=11121 AND `id`=0) OR (`menu_id`=11119 AND `id`=1) OR (`menu_id`=11119 AND `id`=0) OR (`menu_id`=11118 AND `id`=1) OR (`menu_id`=11118 AND `id`=0) OR (`menu_id`=11122 AND `id`=0) OR (`menu_id`=11120 AND `id`=0) OR (`menu_id`=11117 AND `id`=0) OR (`menu_id`=11116 AND `id`=0) OR (`menu_id`=11113 AND `id`=0) OR (`menu_id`=11128 AND `id`=0) OR (`menu_id`=10941 AND `id`=1) OR (`menu_id`=11150 AND `id`=0) OR (`menu_id`=11149 AND `id`=0) OR (`menu_id`=11138 AND `id`=0) OR (`menu_id`=11460 AND `id`=0) OR (`menu_id`=11731 AND `id`=0) OR (`menu_id`=1301 AND `id`=0) OR (`menu_id`=7524 AND `id`=0) OR (`menu_id`=900 AND `id`=0) OR (`menu_id`=8124 AND `id`=0) OR (`menu_id`=2890 AND `id`=1) OR (`menu_id`=2890 AND `id`=0) OR (`menu_id`=4746 AND `id`=0) OR (`menu_id`=4152 AND `id`=0) OR (`menu_id`=2242 AND `id`=0) OR (`menu_id`=1142 AND `id`=0) OR (`menu_id`=11402 AND `id`=1) OR (`menu_id`=11402 AND `id`=0) OR (`menu_id`=8072 AND `id`=0) OR (`menu_id`=8057 AND `id`=0) OR (`menu_id`=8058 AND `id`=1) OR (`menu_id`=8058 AND `id`=0) OR (`menu_id`=8059 AND `id`=0) OR (`menu_id`=7712 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(9821, 2, '', '', 'Ich würde gern meine Kampfhaustiere heilen und wiederbeleben.', '', '', '', '', '', '', '', 'Es wird eine kleine Gebühr für die medizinische Hilfe erhoben.', '', '', '', '', ''),
(9821, 1, '', '', 'Ich suche nach einem verlorenen Gefährten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11814, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11814, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4140, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12460, 0, '', '', 'Wohin kann ich fliegen?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(83, 0, '', '', 'Bringt mich ins Leben zurück.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7475, 0, '', '', 'Also, warum seid Ihr immer noch hier?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7476, 0, '', '', 'Bis was?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7477, 0, '', '', 'Wer oder was genau ist Ysera und wie hat man Euch gesegnet?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7478, 0, '', '', 'Warum leidet Ihr?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7455, 0, '', '', 'Ich möchte mehr über Erste Hilfe lernen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7833, 0, '', '', 'Unterweist mich im Bergbau.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7459, 0, '', '', 'Unterweist mich in der Kräuterkunde.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8243, 0, '', '', 'Ich sehe mich nur mal um.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12433, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6944, 0, '', '', 'Wohin kann ich fliegen?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7469, 0, '', '', 'Ich möchte gerne mit einem Hippogryphen reisen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 14, '', '', 'Kochkunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 13, '', '', 'Schneiderei', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 12, '', '', 'Kürschnerei', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 11, '', '', 'Bergbau', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 10, '', '', 'Lederverarbeitung', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 9, '', '', 'Juwelierskunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 8, '', '', 'Inschriftenkunde', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 7, '', '', 'Kräuterkunde', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 6, '', '', 'Angeln', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 5, '', '', 'Erste Hilfe', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 4, '', '', 'Ingenieurskunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 3, '', '', 'Verzauberkunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 2, '', '', 'Schmiedekunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 1, '', '', 'Archäologie', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7788, 0, '', '', 'Alchemie', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7777, 9, '', '', 'Berufsausbilder', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7777, 8, '', '', 'Klassenausbilder', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7777, 7, '', '', 'Kampfmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7777, 6, '', '', 'Stallmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7777, 5, '', '', 'Briefkasten', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7777, 4, '', '', 'Gasthaus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7777, 3, '', '', 'Gildenmeister & -händler', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7777, 2, '', '', 'Hippogryphenmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7777, 1, '', '', 'Bank', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7777, 0, '', '', 'Auktionshaus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7787, 7, '', '', 'Mönch', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7787, 6, '', '', 'Krieger', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7787, 5, '', '', 'Schamane', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7787, 4, '', '', 'Priester', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7787, 3, '', '', 'Paladin', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7787, 2, '', '', 'Magier', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7787, 1, '', '', 'Jäger', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7787, 0, '', '', 'Druide', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2343, 9, '', '', 'Mönch', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2343, 8, '', '', 'Krieger', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2343, 7, '', '', 'Hexenmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2343, 6, '', '', 'Schamane', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2343, 5, '', '', 'Schurke', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2343, 4, '', '', 'Priester', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2343, 3, '', '', 'Paladin', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2343, 2, '', '', 'Magier', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2343, 1, '', '', 'Jäger', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2343, 0, '', '', 'Druide', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2352, 9, '', '', 'Berufsausbilder', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2352, 8, '', '', 'Klassenausbilder', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2352, 7, '', '', 'Kampfmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2352, 6, '', '', 'Stallmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2352, 5, '', '', 'Briefkasten', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2352, 4, '', '', 'Gasthaus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2352, 3, '', '', 'Gildenmeister & -händler', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2352, 2, '', '', 'Hippogryphenmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2352, 1, '', '', 'Bank', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2352, 0, '', '', 'Auktionshaus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 14, '', '', 'Schneiderei', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 13, '', '', 'Kürschnerei', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 12, '', '', 'Bergbau', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 11, '', '', 'Lederverarbeitung', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 10, '', '', 'Juwelierskunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 9, '', '', 'Inschriftenkunde', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 8, '', '', 'Kräuterkunde', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 7, '', '', 'Angeln', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 6, '', '', 'Erste Hilfe', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 5, '', '', 'Ingenieurskunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 4, '', '', 'Verzauberkunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 3, '', '', 'Kochkunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 2, '', '', 'Schmiedekunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 1, '', '', 'Archäologie', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2351, 0, '', '', 'Alchemie', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11778, 0, '', '', 'Zeigt mir, was Ihr verkauft.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14047, 8, '', '', 'Betretet die Feuerprobe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14042, 8, '', '', 'Betretet die Feuerprobe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6586, 0, '', '', 'Und was sagt Ihr dazu?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6587, 0, '', '', 'Was sagen sie?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6588, 0, '', '', 'Woher wisst Ihr das?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6526, 0, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6528, 0, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1581, 1, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1581, 0, '', '', 'Ich sehe mich nur mal um.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8077, 0, '', '', 'Ich möchte das Schlachtfeld betreten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11071, 0, '', '', 'Kapitän, ich muss zurück zur Feste Nordwacht.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12844, 1, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12844, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12843, 1, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12843, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4355, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4004, 1, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12846, 1, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12846, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10362, 1, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10362, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12842, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12842, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4122, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12850, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12850, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12848, 2, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12848, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12847, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12847, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12852, 1, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12852, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(342, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(342, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1161, 0, '', '', 'Was wisst Ihr über das Gasthaus "Zur Süßen Ruh"?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14297, 8, '', '', 'Wie kann ich Euch helfen?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12517, 1, '', '', 'Wie kann ich meine Talente zurücksetzen?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 15, '', '', 'Schneiderei', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 14, '', '', 'Kürschnerei', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 13, '', '', 'Reiten', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 12, '', '', 'Bergbau', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 11, '', '', 'Lederverarbeitung', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 10, '', '', 'Juwelierskunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 9, '', '', 'Inschriftenkunde', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 8, '', '', 'Kräuterkunde', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 7, '', '', 'Angeln', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 6, '', '', 'Erste Hilfe', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 5, '', '', 'Ingenieurskunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 4, '', '', 'Verzauberkunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 3, '', '', 'Kochen', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 2, '', '', 'Schmiedekunst', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 1, '', '', 'Archäologie', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12865, 0, '', '', 'Alchemie', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8851, 3, '', '', 'Berufsausbilder', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8851, 2, '', '', 'Klassenausbilder', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8851, 1, '', '', 'Bank', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12855, 7, '', '', 'Krieger', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12855, 6, '', '', 'Hexenmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12855, 5, '', '', 'Schamane', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12855, 4, '', '', 'Schurke', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12855, 3, '', '', 'Priester', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12855, 2, '', '', 'Paladin', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12855, 1, '', '', 'Magier', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12855, 0, '', '', 'Jäger', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12591, 0, '', '', 'Ich möchte mir Eure Waren anschauen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12499, 1, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12499, 0, '', '', 'Ich würde gerne Eure Waren durchstöbern, Yasmin.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9868, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9868, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11115, 1, '', '', 'Die kenne ich schon. Erzählt mir eine andere...', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11115, 0, '', '', 'Das klingt ernst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11121, 1, '', '', 'Die kenne ich schon. Erzählt mir eine andere...', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11121, 0, '', '', 'Das ist nicht gut.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11119, 1, '', '', 'Die kenne ich schon. Erzählt mir eine andere...', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11119, 0, '', '', 'Ein Hinterteil?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11118, 1, '', '', 'Die kenne ich schon. Erzählt mir eine andere...', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11118, 0, '', '', 'ROTZ heulen?!', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11122, 0, '', '', 'Erzählt mir noch eine Geschichte, Schwarzer Peter!', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11120, 0, '', '', 'Erzählt mir noch eine Geschichte, Schwarzer Peter!', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11117, 0, '', '', 'Erzählt mir noch eine Geschichte, Schwarzer Peter!', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11116, 0, '', '', 'Erzählt mir noch eine Geschichte, Schwarzer Peter!', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11113, 0, '', '', 'Erzählt mir eine Geschichte, Schwarzer Peter!', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11128, 0, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10941, 1, '', '', 'Was ist hier geschehen?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11150, 0, '', '', 'Wieso habt Ihr das getan?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11149, 0, '', '', 'Wir haben die Siedlung geplündert?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11138, 0, '', '', 'Erzählt mir, was in Taurajo geschehen ist.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11460, 0, '', '', 'Wie ich hörte, verkauft Ihr Anconahühner?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11731, 0, '', '', 'Bringt mir ein Noggenfogger!', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1301, 0, '', '', 'Ich möchte Eure Waren sehen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7524, 0, '', '', 'Ich sehe mich nur mal um.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(900, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8124, 0, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2890, 1, '', '', 'Ich sehe mich nur mal um.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2890, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4746, 0, '', '', 'Ich möchte mich mal umsehen, Dirge.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4152, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2242, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(1142, 0, '', '', 'Erzählt mir mehr, Trenton.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11402, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11402, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8072, 0, '', '', 'Bringt mich bitte zum Hort des Meisters.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8057, 0, '', '', 'Käse, was? Ist es vielleicht... alter Käse?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8058, 1, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8058, 0, '', '', 'Ich brauche ein paar Reagenzien und Gifte, Lady.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8059, 0, '', '', 'Ich möchte meine Ausrüstung reparieren lassen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7712, 0, '', '', 'Ich sehe mich nur mal um.', '', '', '', '', '', '', '', '', '', '', '', '', '');


DELETE FROM `points_of_interest_locale` WHERE (`ID`=263 AND `locale`='deDE') OR (`ID`=196 AND `locale`='deDE') OR (`ID`=195 AND `locale`='deDE') OR (`ID`=194 AND `locale`='deDE') OR (`ID`=193 AND `locale`='deDE') OR (`ID`=192 AND `locale`='deDE') OR (`ID`=191 AND `locale`='deDE') OR (`ID`=190 AND `locale`='deDE') OR (`ID`=500 AND `locale`='deDE') OR (`ID`=101 AND `locale`='deDE') OR (`ID`=501 AND `locale`='deDE') OR (`ID`=502 AND `locale`='deDE') OR (`ID`=100 AND `locale`='deDE') OR (`ID`=267 AND `locale`='deDE') OR (`ID`=266 AND `locale`='deDE') OR (`ID`=265 AND `locale`='deDE') OR (`ID`=99 AND `locale`='deDE') OR (`ID`=98 AND `locale`='deDE') OR (`ID`=110 AND `locale`='deDE') OR (`ID`=109 AND `locale`='deDE') OR (`ID`=503 AND `locale`='deDE') OR (`ID`=108 AND `locale`='deDE') OR (`ID`=504 AND `locale`='deDE') OR (`ID`=107 AND `locale`='deDE') OR (`ID`=268 AND `locale`='deDE') OR (`ID`=106 AND `locale`='deDE') OR (`ID`=105 AND `locale`='deDE') OR (`ID`=505 AND `locale`='deDE') OR (`ID`=104 AND `locale`='deDE') OR (`ID`=103 AND `locale`='deDE') OR (`ID`=462 AND `locale`='deDE') OR (`ID`=506 AND `locale`='deDE') OR (`ID`=102 AND `locale`='deDE') OR (`ID`=97 AND `locale`='deDE') OR (`ID`=95 AND `locale`='deDE') OR (`ID`=94 AND `locale`='deDE') OR (`ID`=93 AND `locale`='deDE') OR (`ID`=92 AND `locale`='deDE') OR (`ID`=91 AND `locale`='deDE') OR (`ID`=90 AND `locale`='deDE') OR (`ID`=89 AND `locale`='deDE') OR (`ID`=469 AND `locale`='deDE');
INSERT INTO `points_of_interest_locale` (`ID`, `locale`, `Name`, `VerifiedBuild`) VALUES
(263, 'deDE', 'Exodar, Kampfmeister', 23420),
(196, 'deDE', 'Exodar, Stallmeister', 23420),
(195, 'deDE', 'Exodar, Briefkasten', 23420),
(194, 'deDE', 'Exodar, Gasthaus', 23420),
(193, 'deDE', 'Exodar, Gildenmeister', 23420),
(192, 'deDE', 'Exodar, Hippogryphenmeister', 23420),
(191, 'deDE', 'Exodar, Bank', 23420),
(190, 'deDE', 'Exodar, Auktionator', 23420),
(500, 'deDE', 'Mönchslehrer von Darnassus', 23420),
(101, 'deDE', 'Die Terrasse der Krieger', 23420),
(501, 'deDE', 'Hexenmeisterlehrer von Darnassus', 23420),
(502, 'deDE', 'Schamanenlehrer von Darnassus', 23420),
(100, 'deDE', 'Schurkenlehrer von Darnassus', 23420),
(267, 'deDE', 'Tempel des Mondes', 23420),
(266, 'deDE', 'Paladinlehrer von Darnassus', 23420),
(265, 'deDE', 'Magierlehrer von Darnassus', 23420),
(99, 'deDE', 'Jägerlehrer von Darnassus', 23420),
(98, 'deDE', 'Druidenlehrer von Darnassus', 23420),
(110, 'deDE', 'Schneiderei von Darnassus', 23420),
(109, 'deDE', 'Kürschnerei von Darnassus', 23420),
(503, 'deDE', 'Bergbau von Darnassus', 23420),
(108, 'deDE', 'Lederverarbeitung von Darnassus', 23420),
(504, 'deDE', 'Darnassus, Juwelierskunst', 23420),
(107, 'deDE', 'Inschriften von Darnassus', 23420),
(268, 'deDE', 'Kräuterkunde von Darnassus', 23420),
(106, 'deDE', 'Angler von Darnassus', 23420),
(105, 'deDE', 'Lehrer für Erste Hilfe von Darnassus', 23420),
(505, 'deDE', 'Ingenieurskunst von Darnassus', 23420),
(104, 'deDE', 'Verzauberkunst von Darnassus', 23420),
(103, 'deDE', 'Kochkunst von Darnassus', 23420),
(462, 'deDE', 'Schmiedekunst von Darnassus', 23420),
(506, 'deDE', 'Archäologie von Darnassus', 23420),
(102, 'deDE', 'Alchemie von Darnassus', 23420),
(97, 'deDE', 'Kampfmeister von Darnassus', 23420),
(95, 'deDE', 'Alassin', 23420),
(94, 'deDE', 'Briefkasten von Darnassus', 23420),
(93, 'deDE', 'Gasthaus von Darnassus', 23420),
(92, 'deDE', 'Gildenmeister von Darnassus', 23420),
(91, 'deDE', 'Hippogryphenmeister von Darnassus', 23420),
(90, 'deDE', 'Bank von Darnassus', 23420),
(89, 'deDE', 'Auktionshaus Darnassus', 23420),
(469, 'deDE', 'Bankier von Theramore', 23360);


INSERT IGNORE INTO `points_of_interest` (`ID`, `PositionX`, `PositionY`, `Icon`, `Flags`, `Importance`, `Name`, `VerifiedBuild`) VALUES
(263, -4000.54, -11372.1, 7, 99, 0, 'Exodar, Kampfmeister', 23420),
(196, -3792.05, -11703.7, 7, 99, 0, 'Exodar, Stallmeister', 23420),
(195, -3913.49, -11607.6, 7, 99, 0, 'Exodar, Briefkasten', 23420),
(194, -3761.47, -11696.2, 7, 99, 0, 'Exodar, Gasthaus', 23420),
(193, -4099.8, -11630, 7, 99, 0, 'Exodar, Gildenmeister', 23420),
(192, -3865.608, -11640.78, 7, 99, 0, 'Exodar, Hippogryphenmeister', 23420),
(191, -3918.91, -11550.1, 7, 99, 0, 'Exodar, Bank', 23420),
(190, -4020.99, -11733.5, 7, 99, 0, 'Exodar, Auktionator', 23420),
(500, 9845.986, 2460.844, 7, 99, 0, 'Mönchslehrer von Darnassus', 23420),
(101, 9951.91, 2280.39, 7, 99, 0, 'Die Terrasse der Krieger', 23420),
(501, 10300.74, 2418.637, 7, 99, 0, 'Hexenmeisterlehrer von Darnassus', 23420),
(502, 9654.393, 2510.8, 7, 99, 0, 'Schamanenlehrer von Darnassus', 23420),
(100, 10122, 2599.13, 7, 99, 0, 'Schurkenlehrer von Darnassus', 23420),
(267, 9659.13, 2524.89, 7, 99, 0, 'Tempel des Mondes', 23420),
(266, 9657.61, 2513.117, 7, 99, 0, 'Paladinlehrer von Darnassus', 23420),
(265, 9638.622, 2607.958, 7, 99, 0, 'Magierlehrer von Darnassus', 23420),
(99, 10177.3, 2511.1, 7, 99, 0, 'Jägerlehrer von Darnassus', 23420),
(98, 10186, 2570.47, 7, 99, 0, 'Druidenlehrer von Darnassus', 23420),
(110, 10079.7, 2268.2, 7, 99, 0, 'Schneiderei von Darnassus', 23420),
(109, 10081.4, 2257.19, 7, 99, 0, 'Kürschnerei von Darnassus', 23420),
(503, 10124.69, 2415.28, 7, 99, 0, 'Bergbau von Darnassus', 23420),
(108, 10086.6, 2255.77, 7, 99, 0, 'Lederverarbeitung von Darnassus', 23420),
(504, 10147.66, 2353.578, 7, 99, 0, 'Darnassus, Juwelierskunst', 23420),
(107, 10131.8, 2323.74, 7, 99, 0, 'Inschriften von Darnassus', 23420),
(268, 9757.18, 2430.17, 7, 99, 0, 'Kräuterkunde von Darnassus', 23420),
(106, 9836.21, 2432.18, 7, 99, 0, 'Angler von Darnassus', 23420),
(105, 10150.1, 2390.44, 7, 99, 0, 'Lehrer für Erste Hilfe von Darnassus', 23420),
(505, 10127.96, 2422.82, 7, 99, 0, 'Ingenieurskunst von Darnassus', 23420),
(104, 10146.1, 2313.43, 7, 99, 0, 'Verzauberkunst von Darnassus', 23420),
(103, 10088.6, 2419.22, 7, 99, 0, 'Kochkunst von Darnassus', 23420),
(462, 9924.486, 2311.766, 7, 99, 0, 'Schmiedekunst von Darnassus', 23420),
(506, 9608.43, 2531.16, 7, 99, 0, 'Archäologie von Darnassus', 23420),
(102, 10075.9, 2356.76, 7, 99, 0, 'Alchemie von Darnassus', 23420),
(97, 9982.61, 2319.79, 7, 99, 0, 'Kampfmeister von Darnassus', 23420),
(95, 10167.2, 2522.67, 7, 99, 0, 'Alassin', 23420),
(94, 9942.18, 2495.49, 7, 99, 0, 'Briefkasten von Darnassus', 23420),
(93, 10133.3, 2222.52, 7, 99, 0, 'Gasthaus von Darnassus', 23420),
(92, 10076.4, 2199.59, 7, 99, 0, 'Gildenmeister von Darnassus', 23420),
(91, 9972.819, 2623.863, 7, 99, 0, 'Hippogryphenmeister von Darnassus', 23420),
(90, 9938.46, 2512.35, 7, 99, 0, 'Bank von Darnassus', 23420),
(89, 9862.37, 2339.19, 7, 99, 0, 'Auktionshaus Darnassus', 23420),
(469, -3721.332, -4538.389, 7, 99, 0, 'Bankier von Theramore', 23360);


DELETE FROM `locales_creature_text` WHERE (`entry`=40904 AND `groupid`=0) OR (`entry`=11440 AND `groupid`=0) OR (`entry`=37910 AND `groupid`=0) OR (`entry`=36989 AND `groupid`=0) OR (`entry`=34946 AND `groupid`=0) OR (`entry`=40719 AND `groupid`=0) OR (`entry`=23602 AND `groupid`=0) OR (`entry`=71188 AND `groupid`=0) OR (`entry`=38749 AND `groupid`=0) OR (`entry`=75685 AND `groupid`=0) OR (`entry`=40132 AND `groupid`=0) OR (`entry`=39034 AND `groupid`=0) OR (`entry`=66422 AND `groupid`=0) OR (`entry`=23843 AND `groupid`=0) OR (`entry`=18896 AND `groupid`=0) OR (`entry`=41131 AND `groupid`=0) OR (`entry`=41232 AND `groupid`=0) OR (`entry`=37988 AND `groupid`=0) OR (`entry`=39965 AND `groupid`=0) OR (`entry`=39952 AND `groupid`=0) OR (`entry`=17528 AND `groupid`=0) OR (`entry`=23579 AND `groupid`=0) OR (`entry`=34258 AND `groupid`=0) OR (`entry`=41563 AND `groupid`=0) OR (`entry`=34509 AND `groupid`=0) OR (`entry`=3448 AND `groupid`=0) OR (`entry`=4792 AND `groupid`=0) OR (`entry`=44722 AND `groupid`=0) OR (`entry`=9877 AND `groupid`=0) OR (`entry`=40578 AND `groupid`=0) OR (`entry`=34295 AND `groupid`=0) OR (`entry`=17702 AND `groupid`=0) OR (`entry`=3942 AND `groupid`=0) OR (`entry`=2979 AND `groupid`=0) OR (`entry`=34503 AND `groupid`=0) OR (`entry`=2951 AND `groupid`=0) OR (`entry`=66452 AND `groupid`=0) OR (`entry`=6115 AND `groupid`=0) OR (`entry`=37951 AND `groupid`=0) OR (`entry`=3429 AND `groupid`=0) OR (`entry`=39270 AND `groupid`=0) OR (`entry`=16483 AND `groupid`=0) OR (`entry`=23995 AND `groupid`=0) OR (`entry`=34937 AND `groupid`=0) OR (`entry`=45015 AND `groupid`=0) OR (`entry`=2616 AND `groupid`=0) OR (`entry`=47621 AND `groupid`=0) OR (`entry`=34939 AND `groupid`=0) OR (`entry`=3486 AND `groupid`=0) OR (`entry`=19173 AND `groupid`=0) OR (`entry`=3168 AND `groupid`=0) OR (`entry`=3055 AND `groupid`=0) OR (`entry`=34933 AND `groupid`=0) OR (`entry`=36385 AND `groupid`=0) OR (`entry`=33057 AND `groupid`=0) OR (`entry`=43949 AND `groupid`=0) OR (`entry`=5240 AND `groupid`=0) OR (`entry`=33232 AND `groupid`=0);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(40904, 0, 0, '', '', 'Blind durch dunkle Mine laufen mit großem Fels im Arm. Viel besser als letze Arbeit.', '', '', '', '', ''),
(11440, 0, 0, '', '', 'Fühl mich gar nicht gut…', '', '', '', '', ''),
(37910, 0, 0, '', '', 'Makogg ', '', '', '', '', ''),
(36989, 0, 0, '', '', '%s ruft um Hilfe!', '', '', '', '', ''),
(34946, 0, 0, '', '', 'Herein, herein! Einer nach dem anderen.', '', '', '', '', ''),
(40719, 0, 0, '', '', 'Tägliche Welle:', '', '', '', '', ''),
(23602, 0, 0, '', '', 'Lasst uns zum Gasthaus gehen, wenn Ihr Dienstschluss habt. Ich gebe eine Runde aus und wir können uns über die gute alte Zeit unterhalten.', '', '', '', '', ''),
(71188, 0, 0, '', '', 'Wir werden dieses Land säubern!', '', '', '', '', ''),
(38749, 0, 0, '', '', 'Ich habe eine Überraschung für Euch, Ihr Hunde!', '', '', '', '', ''),
(75685, 0, 0, '', '', 'Kag kil  ', '', '', '', '', ''),
(40132, 0, 0, '', '', 'Dieser eine Moment reicht einem Dämon, um Verwüstung anzurichten. Die Brennende Legion hat uns seit Jahrtausenden im Blick, ständig auf einen noch so winzigen Riss wartend, um hindurchzuschlüpfen, oder auf die kleinste Regung, um aus ihr zu flüstern.', '', '', '', '', ''),
(39034, 0, 0, '', '', 'Die Zartbesaiteten unter uns sollten jetzt ihre Augen bedecken! Auf dem Weg zum Ring seht Ihr nun einen Schrecken sondergleichen! Gefangen in den dunklen Gängen des Schwarmbaus der Centipaar! Ich präsentiere den rasenden Tod: SARINEXX!', '', '', '', '', ''),
(66422, 0, 0, '', '', 'Hallöchen, Schätzchen, ich bin schon ganz aufgeregt und bereit für den Kampf!', '', '', '', '', ''),
(23843, 0, 0, '', '', 'Jarl?', '', '', '', '', ''),
(18896, 0, 0, '', '', 'Funktion: Geheimpolizei, Verhörmeister', '', '', '', '', ''),
(41131, 0, 0, '', '', 'Ich hätte das Meer aufhalten können, hättet Ihr nicht unablässig gegackert!', '', '', '', '', ''),
(41232, 0, 0, '', '', 'Ihr werdet Eure Taten bereuen!', '', '', '', '', ''),
(37988, 0, 0, '', '', 'Ja, Mann. Lasst uns ein paar Tikizielschädel knacken!', '', '', '', '', ''),
(39965, 0, 0, '', '', 'Raaar!!! Ich zerschmettern $R!', '', '', '', '', ''),
(39952, 0, 0, '', '', 'Ich zerschmettern! Du sterben!', '', '', '', '', ''),
(17528, 0, 0, '', '', 'Narren! Schwächlinge! Tzerak hat keine Verwendung für Euch!', '', '', '', '', ''),
(23579, 0, 0, '', '', 'Ihr habt gesagt, dass Brogg hier bleiben kann! Ihr habt versprochen, dass Ihr Brogg bei der Rückeroberung des Horts der Steinbrecher helft!', '', '', '', '', ''),
(34258, 0, 0, '', '', 'Macht schneller, Ihr Peons! ...Zwingt mich nicht, zum Stock zu greifen.', '', '', '', '', ''),
(41563, 0, 0, '', '', 'Werdet eins mit dem Feuer!', '', '', '', '', ''),
(34509, 0, 0, '', '', 'Gi ruk ag zil''nok tov''osh Lok Zugas Ruk golar Thok zug Revas kagg ul''gammathar Thok osh''kazil kil Thok kazreth maza aaz aaz''no maka ', '', '', '', '', ''),
(3448, 0, 0, '', '', 'Makogg ', '', '', '', '', ''),
(4792, 0, 0, '', '', 'Wenn ich es nicht essen kann, mag ich es nicht.', '', '', '', '', ''),
(44722, 0, 0, '', '', 'Konntet Ihr es voraussehen, Narr?', '', '', '', '', ''),
(9877, 0, 0, '', '', 'Ich lass nicht zu, dass $gein erbärmlicher:eine erbärmliche:r; $R mir mein Geburtsrecht aberkennt!', '', '', '', '', ''),
(40578, 0, 0, '', '', 'Mit der Lanze bewaffnen', '', '', '', '', ''),
(34295, 0, 0, '', '', 'Der Tod steigt von unten auf und bricht von oben herab! Es gibt kein Entkommen!', '', '', '', '', ''),
(17702, 0, 0, '', '', 'Stellt Euch dem Zorn der Sichelklauen!', '', '', '', '', ''),
(3942, 0, 0, '', '', 'Ihr werdet es so nett und warm mit uns hier unten haben, und das für immer, schönes Wesen!', '', '', '', '', ''),
(2979, 0, 0, '', '', 'Fragt Euch selbst, $n: Ist es zum Wohle der Venture Company?', '', '', '', '', ''),
(34503, 0, 0, '', '', '%s versucht zu flüchten!', '', '', '', '', ''),
(2951, 0, 0, '', '', 'Grrr... Frischfleisch!', '', '', '', '', ''),
(66452, 0, 0, '', '', 'Wie ich sehe, trainiert Ihr Kampfhaustiere, $R. Wollt Ihr einmal Euer Können mit dem eines Grimmtotems messen?', '', '', '', '', ''),
(6115, 0, 0, '', '', '%s gerät in Raserei!', '', '', '', '', ''),
(37951, 0, 0, '', '', '$n, Zuni, Euer Trainer ist im Ausbildungsgelände im Osten. Macht den Dunkelspeeren Ehre!', '', '', '', '', ''),
(3429, 0, 0, '', '', 'Makogg ', '', '', '', '', ''),
(39270, 0, 0, '', '', 'O regas ruk ', '', '', '', '', ''),
(16483, 0, 0, '', '', 'Zekul zar il gul Gular me teamanar zekul ', '', '', '', '', ''),
(23995, 0, 0, '', '', 'Wir haben schon oft genug darüber gesprochen, Brogg. Ihr könnt nicht in diesem Gebäude bleiben. Sicher, es bietet Euch genug Platz, doch es passt sonst niemand mehr hinein!', '', '', '', '', ''),
(34937, 0, 0, '', '', 'Meine Freunde, meine Kollegen. Entspannt Euch.', '', '', '', '', ''),
(45015, 0, 0, '', '', 'Kag kil  ', '', '', '', '', ''),
(2616, 0, 0, '', '', 'Wo zur Scherbenwelt sind diese Ladungen? Seit Tagen lassen sie mich warten!', '', '', '', '', ''),
(47621, 0, 0, '', '', 'Ihr da, hier rüber! Kommt, tötet meinen Gefängniswärter!', '', '', '', '', ''),
(34939, 0, 0, '', '', 'Auf Anweisung der Königin müssen alle Besitztümer untersucht werden, bevor Einlass in die Stadt gewährt wird.', '', '', '', '', ''),
(3486, 0, 0, '', '', 'Makogg ', '', '', '', '', ''),
(19173, 0, 0, '', '', 'Die Druiden von Nachthafen veranstalten zu Ehren des Mondfests eine große Freudenfeier auf der Mondlichtung.', '', '', '', '', ''),
(3168, 0, 0, '', '', 'Gul''rok Zil''nok ', '', '', '', '', ''),
(3055, 0, 0, '', '', 'Ish taisha nokee  Eche nokee tawaporah ', '', '', '', '', ''),
(34933, 0, 0, '', '', 'Ich habe gehört, Lord Xavius ist heute zu seinem Turm in Eschental aufgebrochen.', '', '', '', '', ''),
(36385, 0, 0, '', '', 'Hier drüben!', '', '', '', '', ''),
(33057, 0, 0, '', '', 'Eindringlinge! Gut, ich fing schon an, mich zu langweilen...', '', '', '', '', ''),
(43949, 0, 0, '', '', 'Gul''rok Zil''nok ', '', '', '', '', ''),
(5240, 0, 0, '', '', 'Ich werde Euch vernichten!', '', '', '', '', ''),
(33232, 0, 0, '', '', 'Kommt ins oberste Geschoss des Gasthauses! Sie haben mich in Ketten gelegt!', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=11553 /*11553*/ AND `locale`='deDE') OR (`entry`=48461 /*48461*/ AND `locale`='deDE') OR (`entry`=15395 /*15395*/ AND `locale`='deDE') OR (`entry`=10017 /*10017*/ AND `locale`='deDE') OR (`entry`=7158 /*7158*/ AND `locale`='deDE') OR (`entry`=7157 /*7157*/ AND `locale`='deDE') OR (`entry`=10016 /*10016*/ AND `locale`='deDE') OR (`entry`=7156 /*7156*/ AND `locale`='deDE') OR (`entry`=11558 /*11558*/ AND `locale`='deDE') OR (`entry`=11555 /*11555*/ AND `locale`='deDE') OR (`entry`=11557 /*11557*/ AND `locale`='deDE') OR (`entry`=10738 /*10738*/ AND `locale`='deDE') OR (`entry`=48765 /*48765*/ AND `locale`='deDE') OR (`entry`=10916 /*10916*/ AND `locale`='deDE') OR (`entry`=10307 /*10307*/ AND `locale`='deDE') OR (`entry`=9298 /*9298*/ AND `locale`='deDE') OR (`entry`=11516 /*11516*/ AND `locale`='deDE') OR (`entry`=11552 /*11552*/ AND `locale`='deDE') OR (`entry`=11556 /*11556*/ AND `locale`='deDE') OR (`entry`=7441 /*7441*/ AND `locale`='deDE') OR (`entry`=7440 /*7440*/ AND `locale`='deDE') OR (`entry`=7442 /*7442*/ AND `locale`='deDE') OR (`entry`=48767 /*48767*/ AND `locale`='deDE') OR (`entry`=48768 /*48768*/ AND `locale`='deDE') OR (`entry`=10920 /*10920*/ AND `locale`='deDE') OR (`entry`=48665 /*48665*/ AND `locale`='deDE') OR (`entry`=48664 /*48664*/ AND `locale`='deDE') OR (`entry`=48658 /*48658*/ AND `locale`='deDE') OR (`entry`=7524 /*7524*/ AND `locale`='deDE') OR (`entry`=7523 /*7523*/ AND `locale`='deDE') OR (`entry`=15606 /*15606*/ AND `locale`='deDE') OR (`entry`=48740 /*48740*/ AND `locale`='deDE') OR (`entry`=49217 /*49217*/ AND `locale`='deDE') OR (`entry`=11138 /*11138*/ AND `locale`='deDE') OR (`entry`=11757 /*11757*/ AND `locale`='deDE') OR (`entry`=11183 /*11183*/ AND `locale`='deDE') OR (`entry`=11182 /*11182*/ AND `locale`='deDE') OR (`entry`=48952 /*48952*/ AND `locale`='deDE') OR (`entry`=48670 /*48670*/ AND `locale`='deDE') OR (`entry`=48960 /*48960*/ AND `locale`='deDE') OR (`entry`=11186 /*11186*/ AND `locale`='deDE') OR (`entry`=11184 /*11184*/ AND `locale`='deDE') OR (`entry`=11754 /*11754*/ AND `locale`='deDE') OR (`entry`=11546 /*11546*/ AND `locale`='deDE') OR (`entry`=10978 /*10978*/ AND `locale`='deDE') OR (`entry`=10468 /*10468*/ AND `locale`='deDE') OR (`entry`=52831 /*52831*/ AND `locale`='deDE') OR (`entry`=52830 /*52830*/ AND `locale`='deDE') OR (`entry`=11187 /*11187*/ AND `locale`='deDE') OR (`entry`=62119 /*62119*/ AND `locale`='deDE') OR (`entry`=16416 /*16416*/ AND `locale`='deDE') OR (`entry`=11118 /*11118*/ AND `locale`='deDE') OR (`entry`=50366 /*50366*/ AND `locale`='deDE') OR (`entry`=11755 /*11755*/ AND `locale`='deDE') OR (`entry`=11753 /*11753*/ AND `locale`='deDE') OR (`entry`=11185 /*11185*/ AND `locale`='deDE') OR (`entry`=10637 /*10637*/ AND `locale`='deDE') OR (`entry`=15574 /*15574*/ AND `locale`='deDE') OR (`entry`=48965 /*48965*/ AND `locale`='deDE') OR (`entry`=14742 /*14742*/ AND `locale`='deDE') OR (`entry`=11189 /*11189*/ AND `locale`='deDE') OR (`entry`=11188 /*11188*/ AND `locale`='deDE') OR (`entry`=13917 /*13917*/ AND `locale`='deDE') OR (`entry`=11193 /*11193*/ AND `locale`='deDE') OR (`entry`=11192 /*11192*/ AND `locale`='deDE') OR (`entry`=11191 /*11191*/ AND `locale`='deDE') OR (`entry`=11190 /*11190*/ AND `locale`='deDE') OR (`entry`=9857 /*9857*/ AND `locale`='deDE') OR (`entry`=49773 /*49773*/ AND `locale`='deDE') OR (`entry`=12150 /*12150*/ AND `locale`='deDE') OR (`entry`=11119 /*11119*/ AND `locale`='deDE') OR (`entry`=10305 /*10305*/ AND `locale`='deDE') OR (`entry`=5198 /*5198*/ AND `locale`='deDE') OR (`entry`=4779 /*4779*/ AND `locale`='deDE') OR (`entry`=20102 /*20102*/ AND `locale`='deDE') OR (`entry`=11139 /*11139*/ AND `locale`='deDE') OR (`entry`=48918 /*48918*/ AND `locale`='deDE') OR (`entry`=50044 /*50044*/ AND `locale`='deDE') OR (`entry`=7443 /*7443*/ AND `locale`='deDE') OR (`entry`=49346 /*49346*/ AND `locale`='deDE') OR (`entry`=2084 /*2084*/ AND `locale`='deDE') OR (`entry`=49402 /*49402*/ AND `locale`='deDE') OR (`entry`=49347 /*49347*/ AND `locale`='deDE') OR (`entry`=11079 /*11079*/ AND `locale`='deDE') OR (`entry`=3779 /*3779*/ AND `locale`='deDE') OR (`entry`=48622 /*48622*/ AND `locale`='deDE') OR (`entry`=2303 /*2303*/ AND `locale`='deDE') OR (`entry`=10619 /*10619*/ AND `locale`='deDE') OR (`entry`=10618 /*10618*/ AND `locale`='deDE') OR (`entry`=10737 /*10737*/ AND `locale`='deDE') OR (`entry`=7434 /*7434*/ AND `locale`='deDE') OR (`entry`=7430 /*7430*/ AND `locale`='deDE') OR (`entry`=51681 /*51681*/ AND `locale`='deDE') OR (`entry`=50094 /*50094*/ AND `locale`='deDE') OR (`entry`=50092 /*50092*/ AND `locale`='deDE') OR (`entry`=49537 /*49537*/ AND `locale`='deDE') OR (`entry`=49436 /*49436*/ AND `locale`='deDE') OR (`entry`=49396 /*49396*/ AND `locale`='deDE') OR (`entry`=7431 /*7431*/ AND `locale`='deDE') OR (`entry`=7433 /*7433*/ AND `locale`='deDE') OR (`entry`=7432 /*7432*/ AND `locale`='deDE') OR (`entry`=11751 /*11751*/ AND `locale`='deDE') OR (`entry`=11718 /*11718*/ AND `locale`='deDE') OR (`entry`=7448 /*7448*/ AND `locale`='deDE') OR (`entry`=7444 /*7444*/ AND `locale`='deDE') OR (`entry`=7455 /*7455*/ AND `locale`='deDE') OR (`entry`=51711 /*51711*/ AND `locale`='deDE') OR (`entry`=49565 /*49565*/ AND `locale`='deDE') OR (`entry`=7451 /*7451*/ AND `locale`='deDE') OR (`entry`=11696 /*11696*/ AND `locale`='deDE') OR (`entry`=7456 /*7456*/ AND `locale`='deDE') OR (`entry`=50995 /*50995*/ AND `locale`='deDE') OR (`entry`=7450 /*7450*/ AND `locale`='deDE') OR (`entry`=48723 /*48723*/ AND `locale`='deDE') OR (`entry`=48722 /*48722*/ AND `locale`='deDE') OR (`entry`=49177 /*49177*/ AND `locale`='deDE') OR (`entry`=49235 /*49235*/ AND `locale`='deDE') OR (`entry`=49178 /*49178*/ AND `locale`='deDE') OR (`entry`=7439 /*7439*/ AND `locale`='deDE') OR (`entry`=49233 /*49233*/ AND `locale`='deDE') OR (`entry`=7460 /*7460*/ AND `locale`='deDE') OR (`entry`=7438 /*7438*/ AND `locale`='deDE') OR (`entry`=49772 /*49772*/ AND `locale`='deDE') OR (`entry`=68839 /*68839*/ AND `locale`='deDE') OR (`entry`=7458 /*7458*/ AND `locale`='deDE') OR (`entry`=7459 /*7459*/ AND `locale`='deDE') OR (`entry`=66470 /*66470*/ AND `locale`='deDE') OR (`entry`=66469 /*66469*/ AND `locale`='deDE') OR (`entry`=66468 /*66468*/ AND `locale`='deDE') OR (`entry`=66466 /*66466*/ AND `locale`='deDE') OR (`entry`=50422 /*50422*/ AND `locale`='deDE') OR (`entry`=50129 /*50129*/ AND `locale`='deDE') OR (`entry`=50126 /*50126*/ AND `locale`='deDE') OR (`entry`=10929 /*10929*/ AND `locale`='deDE') OR (`entry`=10301 /*10301*/ AND `locale`='deDE') OR (`entry`=50291 /*50291*/ AND `locale`='deDE') OR (`entry`=49161 /*49161*/ AND `locale`='deDE') OR (`entry`=16015 /*16015*/ AND `locale`='deDE') OR (`entry`=10918 /*10918*/ AND `locale`='deDE') OR (`entry`=62435 /*62435*/ AND `locale`='deDE') OR (`entry`=50251 /*50251*/ AND `locale`='deDE') OR (`entry`=32261 /*32261*/ AND `locale`='deDE') OR (`entry`=50250 /*50250*/ AND `locale`='deDE') OR (`entry`=10198 /*10198*/ AND `locale`='deDE') OR (`entry`=50258 /*50258*/ AND `locale`='deDE') OR (`entry`=10807 /*10807*/ AND `locale`='deDE') OR (`entry`=50263 /*50263*/ AND `locale`='deDE') OR (`entry`=50280 /*50280*/ AND `locale`='deDE') OR (`entry`=61690 /*61690*/ AND `locale`='deDE') OR (`entry`=50281 /*50281*/ AND `locale`='deDE') OR (`entry`=7454 /*7454*/ AND `locale`='deDE') OR (`entry`=50282 /*50282*/ AND `locale`='deDE') OR (`entry`=7453 /*7453*/ AND `locale`='deDE') OR (`entry`=7452 /*7452*/ AND `locale`='deDE') OR (`entry`=7429 /*7429*/ AND `locale`='deDE') OR (`entry`=47285 /*47285*/ AND `locale`='deDE') OR (`entry`=51759 /*51759*/ AND `locale`='deDE') OR (`entry`=48028 /*48028*/ AND `locale`='deDE') OR (`entry`=48011 /*48011*/ AND `locale`='deDE') OR (`entry`=46334 /*46334*/ AND `locale`='deDE') OR (`entry`=46884 /*46884*/ AND `locale`='deDE') OR (`entry`=47699 /*47699*/ AND `locale`='deDE') OR (`entry`=48040 /*48040*/ AND `locale`='deDE') OR (`entry`=48041 /*48041*/ AND `locale`='deDE') OR (`entry`=48043 /*48043*/ AND `locale`='deDE') OR (`entry`=47318 /*47318*/ AND `locale`='deDE') OR (`entry`=49148 /*49148*/ AND `locale`='deDE') OR (`entry`=48861 /*48861*/ AND `locale`='deDE') OR (`entry`=48860 /*48860*/ AND `locale`='deDE') OR (`entry`=48856 /*48856*/ AND `locale`='deDE') OR (`entry`=49156 /*49156*/ AND `locale`='deDE') OR (`entry`=48853 /*48853*/ AND `locale`='deDE') OR (`entry`=66824 /*66824*/ AND `locale`='deDE') OR (`entry`=66814 /*66814*/ AND `locale`='deDE') OR (`entry`=66813 /*66813*/ AND `locale`='deDE') OR (`entry`=66812 /*66812*/ AND `locale`='deDE') OR (`entry`=47062 /*47062*/ AND `locale`='deDE') OR (`entry`=46135 /*46135*/ AND `locale`='deDE') OR (`entry`=50065 /*50065*/ AND `locale`='deDE') OR (`entry`=50066 /*50066*/ AND `locale`='deDE') OR (`entry`=46340 /*46340*/ AND `locale`='deDE') OR (`entry`=48267 /*48267*/ AND `locale`='deDE') OR (`entry`=35374 /*35374*/ AND `locale`='deDE') OR (`entry`=47567 /*47567*/ AND `locale`='deDE') OR (`entry`=46317 /*46317*/ AND `locale`='deDE') OR (`entry`=46496 /*46496*/ AND `locale`='deDE') OR (`entry`=40350 /*40350*/ AND `locale`='deDE') OR (`entry`=51676 /*51676*/ AND `locale`='deDE') OR (`entry`=46315 /*46315*/ AND `locale`='deDE') OR (`entry`=47813 /*47813*/ AND `locale`='deDE') OR (`entry`=47730 /*47730*/ AND `locale`='deDE') OR (`entry`=47742 /*47742*/ AND `locale`='deDE') OR (`entry`=47741 /*47741*/ AND `locale`='deDE') OR (`entry`=47762 /*47762*/ AND `locale`='deDE') OR (`entry`=47810 /*47810*/ AND `locale`='deDE') OR (`entry`=47760 /*47760*/ AND `locale`='deDE') OR (`entry`=47753 /*47753*/ AND `locale`='deDE') OR (`entry`=47801 /*47801*/ AND `locale`='deDE') OR (`entry`=47738 /*47738*/ AND `locale`='deDE') OR (`entry`=47726 /*47726*/ AND `locale`='deDE') OR (`entry`=47722 /*47722*/ AND `locale`='deDE') OR (`entry`=47729 /*47729*/ AND `locale`='deDE') OR (`entry`=47725 /*47725*/ AND `locale`='deDE') OR (`entry`=47727 /*47727*/ AND `locale`='deDE') OR (`entry`=49345 /*49345*/ AND `locale`='deDE') OR (`entry`=48550 /*48550*/ AND `locale`='deDE') OR (`entry`=49244 /*49244*/ AND `locale`='deDE') OR (`entry`=49242 /*49242*/ AND `locale`='deDE') OR (`entry`=46877 /*46877*/ AND `locale`='deDE') OR (`entry`=46880 /*46880*/ AND `locale`='deDE') OR (`entry`=46879 /*46879*/ AND `locale`='deDE') OR (`entry`=46878 /*46878*/ AND `locale`='deDE') OR (`entry`=46875 /*46875*/ AND `locale`='deDE') OR (`entry`=46872 /*46872*/ AND `locale`='deDE') OR (`entry`=48237 /*48237*/ AND `locale`='deDE') OR (`entry`=48514 /*48514*/ AND `locale`='deDE') OR (`entry`=48512 /*48512*/ AND `locale`='deDE') OR (`entry`=48548 /*48548*/ AND `locale`='deDE') OR (`entry`=48564 /*48564*/ AND `locale`='deDE') OR (`entry`=47032 /*47032*/ AND `locale`='deDE') OR (`entry`=47037 /*47037*/ AND `locale`='deDE') OR (`entry`=49415 /*49415*/ AND `locale`='deDE') OR (`entry`=49414 /*49414*/ AND `locale`='deDE') OR (`entry`=50401 /*50401*/ AND `locale`='deDE') OR (`entry`=47122 /*47122*/ AND `locale`='deDE') OR (`entry`=48606 /*48606*/ AND `locale`='deDE') OR (`entry`=49411 /*49411*/ AND `locale`='deDE') OR (`entry`=46979 /*46979*/ AND `locale`='deDE') OR (`entry`=46993 /*46993*/ AND `locale`='deDE') OR (`entry`=47190 /*47190*/ AND `locale`='deDE') OR (`entry`=47201 /*47201*/ AND `locale`='deDE') OR (`entry`=48239 /*48239*/ AND `locale`='deDE') OR (`entry`=47202 /*47202*/ AND `locale`='deDE') OR (`entry`=48874 /*48874*/ AND `locale`='deDE') OR (`entry`=48857 /*48857*/ AND `locale`='deDE') OR (`entry`=51648 /*51648*/ AND `locale`='deDE') OR (`entry`=48868 /*48868*/ AND `locale`='deDE') OR (`entry`=47645 /*47645*/ AND `locale`='deDE') OR (`entry`=48168 /*48168*/ AND `locale`='deDE') OR (`entry`=43718 /*43718*/ AND `locale`='deDE') OR (`entry`=48012 /*48012*/ AND `locale`='deDE') OR (`entry`=46134 /*46134*/ AND `locale`='deDE') OR (`entry`=46333 /*46333*/ AND `locale`='deDE') OR (`entry`=47720 /*47720*/ AND `locale`='deDE') OR (`entry`=45353 /*45353*/ AND `locale`='deDE') OR (`entry`=46136 /*46136*/ AND `locale`='deDE') OR (`entry`=46024 /*46024*/ AND `locale`='deDE') OR (`entry`=46041 /*46041*/ AND `locale`='deDE') OR (`entry`=47219 /*47219*/ AND `locale`='deDE') OR (`entry`=47213 /*47213*/ AND `locale`='deDE') OR (`entry`=55210 /*55210*/ AND `locale`='deDE') OR (`entry`=47207 /*47207*/ AND `locale`='deDE') OR (`entry`=47216 /*47216*/ AND `locale`='deDE') OR (`entry`=47220 /*47220*/ AND `locale`='deDE') OR (`entry`=47227 /*47227*/ AND `locale`='deDE') OR (`entry`=47981 /*47981*/ AND `locale`='deDE') OR (`entry`=47978 /*47978*/ AND `locale`='deDE') OR (`entry`=47980 /*47980*/ AND `locale`='deDE') OR (`entry`=45772 /*45772*/ AND `locale`='deDE') OR (`entry`=45799 /*45799*/ AND `locale`='deDE') OR (`entry`=46087 /*46087*/ AND `locale`='deDE') OR (`entry`=46042 /*46042*/ AND `locale`='deDE') OR (`entry`=45716 /*45716*/ AND `locale`='deDE') OR (`entry`=45715 /*45715*/ AND `locale`='deDE') OR (`entry`=45755 /*45755*/ AND `locale`='deDE') OR (`entry`=47982 /*47982*/ AND `locale`='deDE') OR (`entry`=47670 /*47670*/ AND `locale`='deDE') OR (`entry`=47159 /*47159*/ AND `locale`='deDE') OR (`entry`=47520 /*47520*/ AND `locale`='deDE') OR (`entry`=49525 /*49525*/ AND `locale`='deDE') OR (`entry`=48959 /*48959*/ AND `locale`='deDE') OR (`entry`=49528 /*49528*/ AND `locale`='deDE') OR (`entry`=48273 /*48273*/ AND `locale`='deDE') OR (`entry`=47707 /*47707*/ AND `locale`='deDE') OR (`entry`=47516 /*47516*/ AND `locale`='deDE') OR (`entry`=47515 /*47515*/ AND `locale`='deDE') OR (`entry`=47703 /*47703*/ AND `locale`='deDE') OR (`entry`=47706 /*47706*/ AND `locale`='deDE') OR (`entry`=47705 /*47705*/ AND `locale`='deDE') OR (`entry`=47704 /*47704*/ AND `locale`='deDE') OR (`entry`=47702 /*47702*/ AND `locale`='deDE') OR (`entry`=47708 /*47708*/ AND `locale`='deDE') OR (`entry`=47701 /*47701*/ AND `locale`='deDE') OR (`entry`=47519 /*47519*/ AND `locale`='deDE') OR (`entry`=47193 /*47193*/ AND `locale`='deDE') OR (`entry`=51672 /*51672*/ AND `locale`='deDE') OR (`entry`=47732 /*47732*/ AND `locale`='deDE') OR (`entry`=47455 /*47455*/ AND `locale`='deDE') OR (`entry`=47452 /*47452*/ AND `locale`='deDE') OR (`entry`=47772 /*47772*/ AND `locale`='deDE') OR (`entry`=47546 /*47546*/ AND `locale`='deDE') OR (`entry`=47292 /*47292*/ AND `locale`='deDE') OR (`entry`=47690 /*47690*/ AND `locale`='deDE') OR (`entry`=46003 /*46003*/ AND `locale`='deDE') OR (`entry`=48518 /*48518*/ AND `locale`='deDE') OR (`entry`=48668 /*48668*/ AND `locale`='deDE') OR (`entry`=48534 /*48534*/ AND `locale`='deDE') OR (`entry`=47716 /*47716*/ AND `locale`='deDE') OR (`entry`=47715 /*47715*/ AND `locale`='deDE') OR (`entry`=47755 /*47755*/ AND `locale`='deDE') OR (`entry`=48205 /*48205*/ AND `locale`='deDE') OR (`entry`=48204 /*48204*/ AND `locale`='deDE') OR (`entry`=48374 /*48374*/ AND `locale`='deDE') OR (`entry`=48214 /*48214*/ AND `locale`='deDE') OR (`entry`=47709 /*47709*/ AND `locale`='deDE') OR (`entry`=51713 /*51713*/ AND `locale`='deDE') OR (`entry`=46871 /*46871*/ AND `locale`='deDE') OR (`entry`=48644 /*48644*/ AND `locale`='deDE') OR (`entry`=48694 /*48694*/ AND `locale`='deDE') OR (`entry`=11732 /*11732*/ AND `locale`='deDE') OR (`entry`=11731 /*11731*/ AND `locale`='deDE') OR (`entry`=15215 /*15215*/ AND `locale`='deDE') OR (`entry`=11734 /*11734*/ AND `locale`='deDE') OR (`entry`=11733 /*11733*/ AND `locale`='deDE') OR (`entry`=49408 /*49408*/ AND `locale`='deDE') OR (`entry`=49406 /*49406*/ AND `locale`='deDE') OR (`entry`=49410 /*49410*/ AND `locale`='deDE') OR (`entry`=49409 /*49409*/ AND `locale`='deDE') OR (`entry`=48274 /*48274*/ AND `locale`='deDE') OR (`entry`=48203 /*48203*/ AND `locale`='deDE') OR (`entry`=50470 /*50470*/ AND `locale`='deDE') OR (`entry`=50939 /*50939*/ AND `locale`='deDE') OR (`entry`=51675 /*51675*/ AND `locale`='deDE') OR (`entry`=51217 /*51217*/ AND `locale`='deDE') OR (`entry`=62894 /*62894*/ AND `locale`='deDE') OR (`entry`=51673 /*51673*/ AND `locale`='deDE') OR (`entry`=62896 /*62896*/ AND `locale`='deDE') OR (`entry`=49197 /*49197*/ AND `locale`='deDE') OR (`entry`=47803 /*47803*/ AND `locale`='deDE') OR (`entry`=47283 /*47283*/ AND `locale`='deDE') OR (`entry`=51753 /*51753*/ AND `locale`='deDE') OR (`entry`=51193 /*51193*/ AND `locale`='deDE') OR (`entry`=47223 /*47223*/ AND `locale`='deDE') OR (`entry`=47224 /*47224*/ AND `locale`='deDE') OR (`entry`=62186 /*62186*/ AND `locale`='deDE') OR (`entry`=47048 /*47048*/ AND `locale`='deDE') OR (`entry`=49832 /*49832*/ AND `locale`='deDE') OR (`entry`=51674 /*51674*/ AND `locale`='deDE') OR (`entry`=51671 /*51671*/ AND `locale`='deDE') OR (`entry`=62899 /*62899*/ AND `locale`='deDE') OR (`entry`=47221 /*47221*/ AND `locale`='deDE') OR (`entry`=47222 /*47222*/ AND `locale`='deDE') OR (`entry`=48631 /*48631*/ AND `locale`='deDE') OR (`entry`=48629 /*48629*/ AND `locale`='deDE') OR (`entry`=45859 /*45859*/ AND `locale`='deDE') OR (`entry`=62523 /*62523*/ AND `locale`='deDE') OR (`entry`=49835 /*49835*/ AND `locale`='deDE') OR (`entry`=49727 /*49727*/ AND `locale`='deDE') OR (`entry`=45905 /*45905*/ AND `locale`='deDE') OR (`entry`=48334 /*48334*/ AND `locale`='deDE') OR (`entry`=62895 /*62895*/ AND `locale`='deDE') OR (`entry`=45302 /*45302*/ AND `locale`='deDE') OR (`entry`=45321 /*45321*/ AND `locale`='deDE') OR (`entry`=62892 /*62892*/ AND `locale`='deDE') OR (`entry`=48858 /*48858*/ AND `locale`='deDE') OR (`entry`=46868 /*46868*/ AND `locale`='deDE') OR (`entry`=47656 /*47656*/ AND `locale`='deDE') OR (`entry`=62893 /*62893*/ AND `locale`='deDE') OR (`entry`=51712 /*51712*/ AND `locale`='deDE') OR (`entry`=3386 /*3386*/ AND `locale`='deDE') OR (`entry`=3385 /*3385*/ AND `locale`='deDE') OR (`entry`=38621 /*38621*/ AND `locale`='deDE') OR (`entry`=39322 /*39322*/ AND `locale`='deDE') OR (`entry`=38697 /*38697*/ AND `locale`='deDE') OR (`entry`=38619 /*38619*/ AND `locale`='deDE') OR (`entry`=44307 /*44307*/ AND `locale`='deDE') OR (`entry`=44283 /*44283*/ AND `locale`='deDE') OR (`entry`=44348 /*44348*/ AND `locale`='deDE') OR (`entry`=44268 /*44268*/ AND `locale`='deDE') OR (`entry`=39212 /*39212*/ AND `locale`='deDE') OR (`entry`=44303 /*44303*/ AND `locale`='deDE') OR (`entry`=38804 /*38804*/ AND `locale`='deDE') OR (`entry`=38620 /*38620*/ AND `locale`='deDE') OR (`entry`=3454 /*3454*/ AND `locale`='deDE') OR (`entry`=38801 /*38801*/ AND `locale`='deDE') OR (`entry`=38800 /*38800*/ AND `locale`='deDE') OR (`entry`=3465 /*3465*/ AND `locale`='deDE') OR (`entry`=38754 /*38754*/ AND `locale`='deDE') OR (`entry`=3455 /*3455*/ AND `locale`='deDE') OR (`entry`=6020 /*6020*/ AND `locale`='deDE') OR (`entry`=38747 /*38747*/ AND `locale`='deDE') OR (`entry`=38818 /*38818*/ AND `locale`='deDE') OR (`entry`=38805 /*38805*/ AND `locale`='deDE') OR (`entry`=38624 /*38624*/ AND `locale`='deDE') OR (`entry`=38659 /*38659*/ AND `locale`='deDE') OR (`entry`=38658 /*38658*/ AND `locale`='deDE') OR (`entry`=38878 /*38878*/ AND `locale`='deDE') OR (`entry`=3256 /*3256*/ AND `locale`='deDE') OR (`entry`=3255 /*3255*/ AND `locale`='deDE') OR (`entry`=3234 /*3234*/ AND `locale`='deDE') OR (`entry`=3461 /*3461*/ AND `locale`='deDE') OR (`entry`=3275 /*3275*/ AND `locale`='deDE') OR (`entry`=9523 /*9523*/ AND `locale`='deDE') OR (`entry`=3425 /*3425*/ AND `locale`='deDE') OR (`entry`=34641 /*34641*/ AND `locale`='deDE') OR (`entry`=71457 /*71457*/ AND `locale`='deDE') OR (`entry`=71384 /*71384*/ AND `locale`='deDE') OR (`entry`=3246 /*3246*/ AND `locale`='deDE') OR (`entry`=3272 /*3272*/ AND `locale`='deDE') OR (`entry`=3273 /*3273*/ AND `locale`='deDE') OR (`entry`=38380 /*38380*/ AND `locale`='deDE') OR (`entry`=38382 /*38382*/ AND `locale`='deDE') OR (`entry`=44299 /*44299*/ AND `locale`='deDE') OR (`entry`=44219 /*44219*/ AND `locale`='deDE') OR (`entry`=39210 /*39210*/ AND `locale`='deDE') OR (`entry`=44346 /*44346*/ AND `locale`='deDE') OR (`entry`=38383 /*38383*/ AND `locale`='deDE') OR (`entry`=38379 /*38379*/ AND `locale`='deDE') OR (`entry`=44294 /*44294*/ AND `locale`='deDE') OR (`entry`=44277 /*44277*/ AND `locale`='deDE') OR (`entry`=34848 /*34848*/ AND `locale`='deDE') OR (`entry`=37160 /*37160*/ AND `locale`='deDE') OR (`entry`=37180 /*37180*/ AND `locale`='deDE') OR (`entry`=34855 /*34855*/ AND `locale`='deDE') OR (`entry`=37216 /*37216*/ AND `locale`='deDE') OR (`entry`=51857 /*51857*/ AND `locale`='deDE') OR (`entry`=44349 /*44349*/ AND `locale`='deDE') OR (`entry`=44270 /*44270*/ AND `locale`='deDE') OR (`entry`=37154 /*37154*/ AND `locale`='deDE') OR (`entry`=4049 /*4049*/ AND `locale`='deDE') OR (`entry`=37138 /*37138*/ AND `locale`='deDE') OR (`entry`=44297 /*44297*/ AND `locale`='deDE') OR (`entry`=39340 /*39340*/ AND `locale`='deDE') OR (`entry`=44285 /*44285*/ AND `locale`='deDE') OR (`entry`=37204 /*37204*/ AND `locale`='deDE') OR (`entry`=62131 /*62131*/ AND `locale`='deDE') OR (`entry`=62129 /*62129*/ AND `locale`='deDE') OR (`entry`=37166 /*37166*/ AND `locale`='deDE') OR (`entry`=37165 /*37165*/ AND `locale`='deDE') OR (`entry`=51856 /*51856*/ AND `locale`='deDE') OR (`entry`=44286 /*44286*/ AND `locale`='deDE') OR (`entry`=9983 /*9983*/ AND `locale`='deDE') OR (`entry`=3810 /*3810*/ AND `locale`='deDE') OR (`entry`=11857 /*11857*/ AND `locale`='deDE') OR (`entry`=7714 /*7714*/ AND `locale`='deDE') OR (`entry`=37517 /*37517*/ AND `locale`='deDE') OR (`entry`=37516 /*37516*/ AND `locale`='deDE') OR (`entry`=3705 /*3705*/ AND `locale`='deDE') OR (`entry`=37515 /*37515*/ AND `locale`='deDE') OR (`entry`=44296 /*44296*/ AND `locale`='deDE') OR (`entry`=37220 /*37220*/ AND `locale`='deDE') OR (`entry`=3426 /*3426*/ AND `locale`='deDE') OR (`entry`=37170 /*37170*/ AND `locale`='deDE') OR (`entry`=37161 /*37161*/ AND `locale`='deDE') OR (`entry`=38314 /*38314*/ AND `locale`='deDE') OR (`entry`=37570 /*37570*/ AND `locale`='deDE') OR (`entry`=5829 /*5829*/ AND `locale`='deDE') OR (`entry`=37548 /*37548*/ AND `locale`='deDE') OR (`entry`=5864 /*5864*/ AND `locale`='deDE') OR (`entry`=37487 /*37487*/ AND `locale`='deDE') OR (`entry`=38554 /*38554*/ AND `locale`='deDE') OR (`entry`=5863 /*5863*/ AND `locale`='deDE') OR (`entry`=3704 /*3704*/ AND `locale`='deDE') OR (`entry`=37717 /*37717*/ AND `locale`='deDE') OR (`entry`=37679 /*37679*/ AND `locale`='deDE') OR (`entry`=10380 /*10380*/ AND `locale`='deDE') OR (`entry`=3418 /*3418*/ AND `locale`='deDE') OR (`entry`=3387 /*3387*/ AND `locale`='deDE') OR (`entry`=52060 /*52060*/ AND `locale`='deDE') OR (`entry`=15588 /*15588*/ AND `locale`='deDE') OR (`entry`=3433 /*3433*/ AND `locale`='deDE') OR (`entry`=34346 /*34346*/ AND `locale`='deDE') OR (`entry`=3261 /*3261*/ AND `locale`='deDE') OR (`entry`=37706 /*37706*/ AND `locale`='deDE') OR (`entry`=38327 /*38327*/ AND `locale`='deDE') OR (`entry`=10378 /*10378*/ AND `locale`='deDE') OR (`entry`=5944 /*5944*/ AND `locale`='deDE') OR (`entry`=3703 /*3703*/ AND `locale`='deDE') OR (`entry`=6387 /*6387*/ AND `locale`='deDE') OR (`entry`=8016 /*8016*/ AND `locale`='deDE') OR (`entry`=37743 /*37743*/ AND `locale`='deDE') OR (`entry`=51851 /*51851*/ AND `locale`='deDE') OR (`entry`=39330 /*39330*/ AND `locale`='deDE') OR (`entry`=37909 /*37909*/ AND `locale`='deDE') OR (`entry`=44287 /*44287*/ AND `locale`='deDE') OR (`entry`=44300 /*44300*/ AND `locale`='deDE') OR (`entry`=38033 /*38033*/ AND `locale`='deDE') OR (`entry`=44354 /*44354*/ AND `locale`='deDE') OR (`entry`=37910 /*37910*/ AND `locale`='deDE') OR (`entry`=37811 /*37811*/ AND `locale`='deDE') OR (`entry`=37866 /*37866*/ AND `locale`='deDE') OR (`entry`=44276 /*44276*/ AND `locale`='deDE') OR (`entry`=5859 /*5859*/ AND `locale`='deDE') OR (`entry`=37940 /*37940*/ AND `locale`='deDE') OR (`entry`=37661 /*37661*/ AND `locale`='deDE') OR (`entry`=66429 /*66429*/ AND `locale`='deDE') OR (`entry`=66428 /*66428*/ AND `locale`='deDE') OR (`entry`=66427 /*66427*/ AND `locale`='deDE') OR (`entry`=66422 /*66422*/ AND `locale`='deDE') OR (`entry`=34527 /*34527*/ AND `locale`='deDE') OR (`entry`=39282 /*39282*/ AND `locale`='deDE') OR (`entry`=39281 /*39281*/ AND `locale`='deDE') OR (`entry`=39279 /*39279*/ AND `locale`='deDE') OR (`entry`=39280 /*39280*/ AND `locale`='deDE') OR (`entry`=5899 /*5899*/ AND `locale`='deDE') OR (`entry`=39154 /*39154*/ AND `locale`='deDE') OR (`entry`=39118 /*39118*/ AND `locale`='deDE') OR (`entry`=37560 /*37560*/ AND `locale`='deDE') OR (`entry`=37931 /*37931*/ AND `locale`='deDE') OR (`entry`=37933 /*37933*/ AND `locale`='deDE') OR (`entry`=37847 /*37847*/ AND `locale`='deDE') OR (`entry`=37660 /*37660*/ AND `locale`='deDE') OR (`entry`=37836 /*37836*/ AND `locale`='deDE') OR (`entry`=37835 /*37835*/ AND `locale`='deDE') OR (`entry`=37817 /*37817*/ AND `locale`='deDE') OR (`entry`=37834 /*37834*/ AND `locale`='deDE') OR (`entry`=37812 /*37812*/ AND `locale`='deDE') OR (`entry`=37559 /*37559*/ AND `locale`='deDE') OR (`entry`=39156 /*39156*/ AND `locale`='deDE') OR (`entry`=39155 /*39155*/ AND `locale`='deDE') OR (`entry`=5847 /*5847*/ AND `locale`='deDE') OR (`entry`=5849 /*5849*/ AND `locale`='deDE') OR (`entry`=39174 /*39174*/ AND `locale`='deDE') OR (`entry`=5851 /*5851*/ AND `locale`='deDE') OR (`entry`=38109 /*38109*/ AND `locale`='deDE') OR (`entry`=38119 /*38119*/ AND `locale`='deDE') OR (`entry`=38118 /*38118*/ AND `locale`='deDE') OR (`entry`=38127 /*38127*/ AND `locale`='deDE') OR (`entry`=38190 /*38190*/ AND `locale`='deDE') OR (`entry`=38115 /*38115*/ AND `locale`='deDE') OR (`entry`=38140 /*38140*/ AND `locale`='deDE') OR (`entry`=3375 /*3375*/ AND `locale`='deDE') OR (`entry`=3374 /*3374*/ AND `locale`='deDE') OR (`entry`=38183 /*38183*/ AND `locale`='deDE') OR (`entry`=38215 /*38215*/ AND `locale`='deDE') OR (`entry`=3378 /*3378*/ AND `locale`='deDE') OR (`entry`=39153 /*39153*/ AND `locale`='deDE') OR (`entry`=3444 /*3444*/ AND `locale`='deDE') OR (`entry`=3377 /*3377*/ AND `locale`='deDE') OR (`entry`=5848 /*5848*/ AND `locale`='deDE') OR (`entry`=3376 /*3376*/ AND `locale`='deDE') OR (`entry`=38290 /*38290*/ AND `locale`='deDE') OR (`entry`=3341 /*3341*/ AND `locale`='deDE') OR (`entry`=39697 /*39697*/ AND `locale`='deDE') OR (`entry`=37553 /*37553*/ AND `locale`='deDE') OR (`entry`=37908 /*37908*/ AND `locale`='deDE') OR (`entry`=44305 /*44305*/ AND `locale`='deDE') OR (`entry`=37924 /*37924*/ AND `locale`='deDE') OR (`entry`=38070 /*38070*/ AND `locale`='deDE') OR (`entry`=39009 /*39009*/ AND `locale`='deDE') OR (`entry`=38323 /*38323*/ AND `locale`='deDE') OR (`entry`=38986 /*38986*/ AND `locale`='deDE') OR (`entry`=39024 /*39024*/ AND `locale`='deDE') OR (`entry`=39003 /*39003*/ AND `locale`='deDE') OR (`entry`=37669 /*37669*/ AND `locale`='deDE') OR (`entry`=37585 /*37585*/ AND `locale`='deDE') OR (`entry`=44302 /*44302*/ AND `locale`='deDE') OR (`entry`=39005 /*39005*/ AND `locale`='deDE') OR (`entry`=6247 /*6247*/ AND `locale`='deDE') OR (`entry`=6244 /*6244*/ AND `locale`='deDE') OR (`entry`=37219 /*37219*/ AND `locale`='deDE') OR (`entry`=37092 /*37092*/ AND `locale`='deDE') OR (`entry`=37091 /*37091*/ AND `locale`='deDE') OR (`entry`=37090 /*37090*/ AND `locale`='deDE') OR (`entry`=37199 /*37199*/ AND `locale`='deDE') OR (`entry`=37082 /*37082*/ AND `locale`='deDE') OR (`entry`=37088 /*37088*/ AND `locale`='deDE') OR (`entry`=37093 /*37093*/ AND `locale`='deDE') OR (`entry`=37083 /*37083*/ AND `locale`='deDE') OR (`entry`=38873 /*38873*/ AND `locale`='deDE') OR (`entry`=38876 /*38876*/ AND `locale`='deDE') OR (`entry`=38875 /*38875*/ AND `locale`='deDE') OR (`entry`=38871 /*38871*/ AND `locale`='deDE') OR (`entry`=37084 /*37084*/ AND `locale`='deDE') OR (`entry`=62127 /*62127*/ AND `locale`='deDE') OR (`entry`=19444 /*19444*/ AND `locale`='deDE') OR (`entry`=34640 /*34640*/ AND `locale`='deDE') OR (`entry`=49725 /*49725*/ AND `locale`='deDE') OR (`entry`=37738 /*37738*/ AND `locale`='deDE') OR (`entry`=37086 /*37086*/ AND `locale`='deDE') OR (`entry`=38941 /*38941*/ AND `locale`='deDE') OR (`entry`=38940 /*38940*/ AND `locale`='deDE') OR (`entry`=37511 /*37511*/ AND `locale`='deDE') OR (`entry`=44279 /*44279*/ AND `locale`='deDE') OR (`entry`=39025 /*39025*/ AND `locale`='deDE') OR (`entry`=38884 /*38884*/ AND `locale`='deDE') OR (`entry`=37085 /*37085*/ AND `locale`='deDE') OR (`entry`=62130 /*62130*/ AND `locale`='deDE') OR (`entry`=3240 /*3240*/ AND `locale`='deDE') OR (`entry`=37206 /*37206*/ AND `locale`='deDE') OR (`entry`=38015 /*38015*/ AND `locale`='deDE') OR (`entry`=37659 /*37659*/ AND `locale`='deDE') OR (`entry`=37207 /*37207*/ AND `locale`='deDE') OR (`entry`=37971 /*37971*/ AND `locale`='deDE') OR (`entry`=37922 /*37922*/ AND `locale`='deDE') OR (`entry`=44301 /*44301*/ AND `locale`='deDE') OR (`entry`=44280 /*44280*/ AND `locale`='deDE') OR (`entry`=44304 /*44304*/ AND `locale`='deDE') OR (`entry`=39211 /*39211*/ AND `locale`='deDE') OR (`entry`=39124 /*39124*/ AND `locale`='deDE') OR (`entry`=39094 /*39094*/ AND `locale`='deDE') OR (`entry`=39085 /*39085*/ AND `locale`='deDE') OR (`entry`=39084 /*39084*/ AND `locale`='deDE') OR (`entry`=39083 /*39083*/ AND `locale`='deDE') OR (`entry`=39006 /*39006*/ AND `locale`='deDE') OR (`entry`=44347 /*44347*/ AND `locale`='deDE') OR (`entry`=44267 /*44267*/ AND `locale`='deDE') OR (`entry`=39129 /*39129*/ AND `locale`='deDE') OR (`entry`=38055 /*38055*/ AND `locale`='deDE') OR (`entry`=37926 /*37926*/ AND `locale`='deDE') OR (`entry`=37923 /*37923*/ AND `locale`='deDE') OR (`entry`=39136 /*39136*/ AND `locale`='deDE') OR (`entry`=37974 /*37974*/ AND `locale`='deDE') OR (`entry`=39139 /*39139*/ AND `locale`='deDE') OR (`entry`=37925 /*37925*/ AND `locale`='deDE') OR (`entry`=3248 /*3248*/ AND `locale`='deDE') OR (`entry`=37978 /*37978*/ AND `locale`='deDE') OR (`entry`=37977 /*37977*/ AND `locale`='deDE') OR (`entry`=5832 /*5832*/ AND `locale`='deDE') OR (`entry`=37208 /*37208*/ AND `locale`='deDE') OR (`entry`=37555 /*37555*/ AND `locale`='deDE') OR (`entry`=37557 /*37557*/ AND `locale`='deDE') OR (`entry`=43742 /*43742*/ AND `locale`='deDE') OR (`entry`=33913 /*33913*/ AND `locale`='deDE') OR (`entry`=33178 /*33178*/ AND `locale`='deDE') OR (`entry`=32969 /*32969*/ AND `locale`='deDE') OR (`entry`=33071 /*33071*/ AND `locale`='deDE') OR (`entry`=32862 /*32862*/ AND `locale`='deDE') OR (`entry`=32858 /*32858*/ AND `locale`='deDE') OR (`entry`=33115 /*33115*/ AND `locale`='deDE') OR (`entry`=11956 /*11956*/ AND `locale`='deDE') OR (`entry`=11799 /*11799*/ AND `locale`='deDE') OR (`entry`=102432 /*102432*/ AND `locale`='deDE') OR (`entry`=11832 /*11832*/ AND `locale`='deDE') OR (`entry`=101627 /*101627*/ AND `locale`='deDE') OR (`entry`=12740 /*12740*/ AND `locale`='deDE') OR (`entry`=15719 /*15719*/ AND `locale`='deDE') OR (`entry`=15694 /*15694*/ AND `locale`='deDE') OR (`entry`=15906 /*15906*/ AND `locale`='deDE') OR (`entry`=66416 /*66416*/ AND `locale`='deDE') OR (`entry`=66417 /*66417*/ AND `locale`='deDE') OR (`entry`=66414 /*66414*/ AND `locale`='deDE') OR (`entry`=66412 /*66412*/ AND `locale`='deDE') OR (`entry`=15907 /*15907*/ AND `locale`='deDE') OR (`entry`=15905 /*15905*/ AND `locale`='deDE') OR (`entry`=15908 /*15908*/ AND `locale`='deDE') OR (`entry`=11801 /*11801*/ AND `locale`='deDE') OR (`entry`=12024 /*12024*/ AND `locale`='deDE') OR (`entry`=12025 /*12025*/ AND `locale`='deDE') OR (`entry`=11798 /*11798*/ AND `locale`='deDE') OR (`entry`=11797 /*11797*/ AND `locale`='deDE') OR (`entry`=39865 /*39865*/ AND `locale`='deDE') OR (`entry`=11800 /*11800*/ AND `locale`='deDE') OR (`entry`=70164 /*70164*/ AND `locale`='deDE') OR (`entry`=70163 /*70163*/ AND `locale`='deDE') OR (`entry`=12026 /*12026*/ AND `locale`='deDE') OR (`entry`=11939 /*11939*/ AND `locale`='deDE') OR (`entry`=12021 /*12021*/ AND `locale`='deDE') OR (`entry`=11796 /*11796*/ AND `locale`='deDE') OR (`entry`=12022 /*12022*/ AND `locale`='deDE') OR (`entry`=39140 /*39140*/ AND `locale`='deDE') OR (`entry`=13476 /*13476*/ AND `locale`='deDE') OR (`entry`=12019 /*12019*/ AND `locale`='deDE') OR (`entry`=11795 /*11795*/ AND `locale`='deDE') OR (`entry`=7940 /*7940*/ AND `locale`='deDE') OR (`entry`=4184 /*4184*/ AND `locale`='deDE') OR (`entry`=53563 /*53563*/ AND `locale`='deDE') OR (`entry`=12023 /*12023*/ AND `locale`='deDE') OR (`entry`=11802 /*11802*/ AND `locale`='deDE') OR (`entry`=12042 /*12042*/ AND `locale`='deDE') OR (`entry`=15864 /*15864*/ AND `locale`='deDE') OR (`entry`=15909 /*15909*/ AND `locale`='deDE') OR (`entry`=15961 /*15961*/ AND `locale`='deDE') OR (`entry`=11716 /*11716*/ AND `locale`='deDE') OR (`entry`=12029 /*12029*/ AND `locale`='deDE') OR (`entry`=11822 /*11822*/ AND `locale`='deDE') OR (`entry`=22889 /*22889*/ AND `locale`='deDE') OR (`entry`=22902 /*22902*/ AND `locale`='deDE') OR (`entry`=22837 /*22837*/ AND `locale`='deDE') OR (`entry`=22835 /*22835*/ AND `locale`='deDE') OR (`entry`=103421 /*103421*/ AND `locale`='deDE') OR (`entry`=15467 /*15467*/ AND `locale`='deDE') OR (`entry`=15466 /*15466*/ AND `locale`='deDE') OR (`entry`=11957 /*11957*/ AND `locale`='deDE') OR (`entry`=49735 /*49735*/ AND `locale`='deDE') OR (`entry`=10897 /*10897*/ AND `locale`='deDE') OR (`entry`=34886 /*34886*/ AND `locale`='deDE') OR (`entry`=34982 /*34982*/ AND `locale`='deDE') OR (`entry`=32639 /*32639*/ AND `locale`='deDE') OR (`entry`=32638 /*32638*/ AND `locale`='deDE') OR (`entry`=51840 /*51840*/ AND `locale`='deDE') OR (`entry`=18241 /*18241*/ AND `locale`='deDE') OR (`entry`=62177 /*62177*/ AND `locale`='deDE') OR (`entry`=62312 /*62312*/ AND `locale`='deDE') OR (`entry`=36591 /*36591*/ AND `locale`='deDE') OR (`entry`=34212 /*34212*/ AND `locale`='deDE') OR (`entry`=3820 /*3820*/ AND `locale`='deDE') OR (`entry`=34195 /*34195*/ AND `locale`='deDE') OR (`entry`=3817 /*3817*/ AND `locale`='deDE') OR (`entry`=50043 /*50043*/ AND `locale`='deDE') OR (`entry`=52093 /*52093*/ AND `locale`='deDE') OR (`entry`=48343 /*48343*/ AND `locale`='deDE') OR (`entry`=48342 /*48342*/ AND `locale`='deDE') OR (`entry`=48341 /*48341*/ AND `locale`='deDE') OR (`entry`=48340 /*48340*/ AND `locale`='deDE') OR (`entry`=50045 /*50045*/ AND `locale`='deDE') OR (`entry`=52092 /*52092*/ AND `locale`='deDE') OR (`entry`=52039 /*52039*/ AND `locale`='deDE') OR (`entry`=52018 /*52018*/ AND `locale`='deDE') OR (`entry`=113940 /*113940*/ AND `locale`='deDE') OR (`entry`=36822 /*36822*/ AND `locale`='deDE') OR (`entry`=6194 /*6194*/ AND `locale`='deDE') OR (`entry`=37143 /*37143*/ AND `locale`='deDE') OR (`entry`=36873 /*36873*/ AND `locale`='deDE') OR (`entry`=49900 /*49900*/ AND `locale`='deDE') OR (`entry`=49901 /*49901*/ AND `locale`='deDE') OR (`entry`=49896 /*49896*/ AND `locale`='deDE') OR (`entry`=43773 /*43773*/ AND `locale`='deDE') OR (`entry`=42804 /*42804*/ AND `locale`='deDE') OR (`entry`=36988 /*36988*/ AND `locale`='deDE') OR (`entry`=49909 /*49909*/ AND `locale`='deDE') OR (`entry`=49902 /*49902*/ AND `locale`='deDE') OR (`entry`=49894 /*49894*/ AND `locale`='deDE') OR (`entry`=49885 /*49885*/ AND `locale`='deDE') OR (`entry`=49882 /*49882*/ AND `locale`='deDE') OR (`entry`=49879 /*49879*/ AND `locale`='deDE') OR (`entry`=43774 /*43774*/ AND `locale`='deDE') OR (`entry`=43771 /*43771*/ AND `locale`='deDE') OR (`entry`=51509 /*51509*/ AND `locale`='deDE') OR (`entry`=42647 /*42647*/ AND `locale`='deDE') OR (`entry`=49884 /*49884*/ AND `locale`='deDE') OR (`entry`=42641 /*42641*/ AND `locale`='deDE') OR (`entry`=42640 /*42640*/ AND `locale`='deDE') OR (`entry`=36959 /*36959*/ AND `locale`='deDE') OR (`entry`=37009 /*37009*/ AND `locale`='deDE') OR (`entry`=70165 /*70165*/ AND `locale`='deDE') OR (`entry`=36975 /*36975*/ AND `locale`='deDE') OR (`entry`=36999 /*36999*/ AND `locale`='deDE') OR (`entry`=37010 /*37010*/ AND `locale`='deDE') OR (`entry`=37139 /*37139*/ AND `locale`='deDE') OR (`entry`=36972 /*36972*/ AND `locale`='deDE') OR (`entry`=36787 /*36787*/ AND `locale`='deDE') OR (`entry`=49880 /*49880*/ AND `locale`='deDE') OR (`entry`=36956 /*36956*/ AND `locale`='deDE') OR (`entry`=36917 /*36917*/ AND `locale`='deDE') OR (`entry`=49881 /*49881*/ AND `locale`='deDE') OR (`entry`=43304 /*43304*/ AND `locale`='deDE') OR (`entry`=50302 /*50302*/ AND `locale`='deDE') OR (`entry`=50301 /*50301*/ AND `locale`='deDE') OR (`entry`=49892 /*49892*/ AND `locale`='deDE') OR (`entry`=43301 /*43301*/ AND `locale`='deDE') OR (`entry`=49891 /*49891*/ AND `locale`='deDE') OR (`entry`=49890 /*49890*/ AND `locale`='deDE') OR (`entry`=35867 /*35867*/ AND `locale`='deDE') OR (`entry`=49878 /*49878*/ AND `locale`='deDE') OR (`entry`=42643 /*42643*/ AND `locale`='deDE') OR (`entry`=37142 /*37142*/ AND `locale`='deDE') OR (`entry`=8610 /*8610*/ AND `locale`='deDE') OR (`entry`=42836 /*42836*/ AND `locale`='deDE') OR (`entry`=43772 /*43772*/ AND `locale`='deDE') OR (`entry`=42786 /*42786*/ AND `locale`='deDE') OR (`entry`=49883 /*49883*/ AND `locale`='deDE') OR (`entry`=49875 /*49875*/ AND `locale`='deDE') OR (`entry`=36974 /*36974*/ AND `locale`='deDE') OR (`entry`=36973 /*36973*/ AND `locale`='deDE') OR (`entry`=49895 /*49895*/ AND `locale`='deDE') OR (`entry`=51142 /*51142*/ AND `locale`='deDE') OR (`entry`=42777 /*42777*/ AND `locale`='deDE') OR (`entry`=42644 /*42644*/ AND `locale`='deDE') OR (`entry`=49887 /*49887*/ AND `locale`='deDE') OR (`entry`=49876 /*49876*/ AND `locale`='deDE') OR (`entry`=36976 /*36976*/ AND `locale`='deDE') OR (`entry`=49888 /*49888*/ AND `locale`='deDE') OR (`entry`=49886 /*49886*/ AND `locale`='deDE') OR (`entry`=37141 /*37141*/ AND `locale`='deDE') OR (`entry`=37140 /*37140*/ AND `locale`='deDE') OR (`entry`=43776 /*43776*/ AND `locale`='deDE') OR (`entry`=42646 /*42646*/ AND `locale`='deDE') OR (`entry`=43330 /*43330*/ AND `locale`='deDE') OR (`entry`=43328 /*43328*/ AND `locale`='deDE') OR (`entry`=37064 /*37064*/ AND `locale`='deDE') OR (`entry`=37061 /*37061*/ AND `locale`='deDE') OR (`entry`=43710 /*43710*/ AND `locale`='deDE') OR (`entry`=36649 /*36649*/ AND `locale`='deDE') OR (`entry`=49861 /*49861*/ AND `locale`='deDE') OR (`entry`=36638 /*36638*/ AND `locale`='deDE') OR (`entry`=36637 /*36637*/ AND `locale`='deDE') OR (`entry`=36722 /*36722*/ AND `locale`='deDE') OR (`entry`=36336 /*36336*/ AND `locale`='deDE') OR (`entry`=36364 /*36364*/ AND `locale`='deDE') OR (`entry`=35064 /*35064*/ AND `locale`='deDE') OR (`entry`=36636 /*36636*/ AND `locale`='deDE') OR (`entry`=35065 /*35065*/ AND `locale`='deDE') OR (`entry`=35142 /*35142*/ AND `locale`='deDE') OR (`entry`=36131 /*36131*/ AND `locale`='deDE') OR (`entry`=6372 /*6372*/ AND `locale`='deDE') OR (`entry`=36639 /*36639*/ AND `locale`='deDE') OR (`entry`=36640 /*36640*/ AND `locale`='deDE') OR (`entry`=36334 /*36334*/ AND `locale`='deDE') OR (`entry`=36363 /*36363*/ AND `locale`='deDE') OR (`entry`=36408 /*36408*/ AND `locale`='deDE') OR (`entry`=36361 /*36361*/ AND `locale`='deDE') OR (`entry`=37900 /*37900*/ AND `locale`='deDE') OR (`entry`=36367 /*36367*/ AND `locale`='deDE') OR (`entry`=36365 /*36365*/ AND `locale`='deDE') OR (`entry`=36366 /*36366*/ AND `locale`='deDE') OR (`entry`=35171 /*35171*/ AND `locale`='deDE') OR (`entry`=43709 /*43709*/ AND `locale`='deDE') OR (`entry`=36210 /*36210*/ AND `locale`='deDE') OR (`entry`=36596 /*36596*/ AND `locale`='deDE') OR (`entry`=8586 /*8586*/ AND `locale`='deDE') OR (`entry`=35648 /*35648*/ AND `locale`='deDE') OR (`entry`=35754 /*35754*/ AND `locale`='deDE') OR (`entry`=35756 /*35756*/ AND `locale`='deDE') OR (`entry`=35755 /*35755*/ AND `locale`='deDE') OR (`entry`=35187 /*35187*/ AND `locale`='deDE') OR (`entry`=35759 /*35759*/ AND `locale`='deDE') OR (`entry`=6202 /*6202*/ AND `locale`='deDE') OR (`entry`=6200 /*6200*/ AND `locale`='deDE') OR (`entry`=6201 /*6201*/ AND `locale`='deDE') OR (`entry`=36599 /*36599*/ AND `locale`='deDE') OR (`entry`=36594 /*36594*/ AND `locale`='deDE') OR (`entry`=36592 /*36592*/ AND `locale`='deDE') OR (`entry`=36593 /*36593*/ AND `locale`='deDE') OR (`entry`=36611 /*36611*/ AND `locale`='deDE') OR (`entry`=36987 /*36987*/ AND `locale`='deDE') OR (`entry`=8408 /*8408*/ AND `locale`='deDE') OR (`entry`=43711 /*43711*/ AND `locale`='deDE') OR (`entry`=36370 /*36370*/ AND `locale`='deDE') OR (`entry`=36376 /*36376*/ AND `locale`='deDE') OR (`entry`=36372 /*36372*/ AND `locale`='deDE') OR (`entry`=36371 /*36371*/ AND `locale`='deDE') OR (`entry`=36375 /*36375*/ AND `locale`='deDE') OR (`entry`=36377 /*36377*/ AND `locale`='deDE') OR (`entry`=37152 /*37152*/ AND `locale`='deDE') OR (`entry`=36373 /*36373*/ AND `locale`='deDE') OR (`entry`=36374 /*36374*/ AND `locale`='deDE') OR (`entry`=50299 /*50299*/ AND `locale`='deDE') OR (`entry`=6651 /*6651*/ AND `locale`='deDE') OR (`entry`=36618 /*36618*/ AND `locale`='deDE') OR (`entry`=36614 /*36614*/ AND `locale`='deDE') OR (`entry`=36012 /*36012*/ AND `locale`='deDE') OR (`entry`=36015 /*36015*/ AND `locale`='deDE') OR (`entry`=36013 /*36013*/ AND `locale`='deDE') OR (`entry`=36843 /*36843*/ AND `locale`='deDE') OR (`entry`=36800 /*36800*/ AND `locale`='deDE') OR (`entry`=36795 /*36795*/ AND `locale`='deDE') OR (`entry`=36951 /*36951*/ AND `locale`='deDE') OR (`entry`=36952 /*36952*/ AND `locale`='deDE') OR (`entry`=13896 /*13896*/ AND `locale`='deDE') OR (`entry`=36921 /*36921*/ AND `locale`='deDE') OR (`entry`=36920 /*36920*/ AND `locale`='deDE') OR (`entry`=36953 /*36953*/ AND `locale`='deDE') OR (`entry`=6649 /*6649*/ AND `locale`='deDE') OR (`entry`=6195 /*6195*/ AND `locale`='deDE') OR (`entry`=37740 /*37740*/ AND `locale`='deDE') OR (`entry`=35881 /*35881*/ AND `locale`='deDE') OR (`entry`=35968 /*35968*/ AND `locale`='deDE') OR (`entry`=15600 /*15600*/ AND `locale`='deDE') OR (`entry`=35892 /*35892*/ AND `locale`='deDE') OR (`entry`=35880 /*35880*/ AND `locale`='deDE') OR (`entry`=6650 /*6650*/ AND `locale`='deDE') OR (`entry`=6350 /*6350*/ AND `locale`='deDE') OR (`entry`=6370 /*6370*/ AND `locale`='deDE') OR (`entry`=36125 /*36125*/ AND `locale`='deDE') OR (`entry`=36958 /*36958*/ AND `locale`='deDE') OR (`entry`=36541 /*36541*/ AND `locale`='deDE') OR (`entry`=36527 /*36527*/ AND `locale`='deDE') OR (`entry`=36500 /*36500*/ AND `locale`='deDE') OR (`entry`=35484 /*35484*/ AND `locale`='deDE') OR (`entry`=107464 /*107464*/ AND `locale`='deDE') OR (`entry`=35833 /*35833*/ AND `locale`='deDE') OR (`entry`=35832 /*35832*/ AND `locale`='deDE') OR (`entry`=35831 /*35831*/ AND `locale`='deDE') OR (`entry`=35829 /*35829*/ AND `locale`='deDE') OR (`entry`=35817 /*35817*/ AND `locale`='deDE') OR (`entry`=37015 /*37015*/ AND `locale`='deDE') OR (`entry`=37005 /*37005*/ AND `locale`='deDE') OR (`entry`=43705 /*43705*/ AND `locale`='deDE') OR (`entry`=36379 /*36379*/ AND `locale`='deDE') OR (`entry`=35657 /*35657*/ AND `locale`='deDE') OR (`entry`=36157 /*36157*/ AND `locale`='deDE') OR (`entry`=36156 /*36156*/ AND `locale`='deDE') OR (`entry`=36472 /*36472*/ AND `locale`='deDE') OR (`entry`=36146 /*36146*/ AND `locale`='deDE') OR (`entry`=36509 /*36509*/ AND `locale`='deDE') OR (`entry`=49774 /*49774*/ AND `locale`='deDE') OR (`entry`=43708 /*43708*/ AND `locale`='deDE') OR (`entry`=35091 /*35091*/ AND `locale`='deDE') OR (`entry`=35085 /*35085*/ AND `locale`='deDE') OR (`entry`=35526 /*35526*/ AND `locale`='deDE') OR (`entry`=35088 /*35088*/ AND `locale`='deDE') OR (`entry`=35087 /*35087*/ AND `locale`='deDE') OR (`entry`=36752 /*36752*/ AND `locale`='deDE') OR (`entry`=6193 /*6193*/ AND `locale`='deDE') OR (`entry`=36756 /*36756*/ AND `locale`='deDE') OR (`entry`=36748 /*36748*/ AND `locale`='deDE') OR (`entry`=36768 /*36768*/ AND `locale`='deDE') OR (`entry`=36749 /*36749*/ AND `locale`='deDE') OR (`entry`=36746 /*36746*/ AND `locale`='deDE') OR (`entry`=36744 /*36744*/ AND `locale`='deDE') OR (`entry`=6196 /*6196*/ AND `locale`='deDE') OR (`entry`=7885 /*7885*/ AND `locale`='deDE') OR (`entry`=36868 /*36868*/ AND `locale`='deDE') OR (`entry`=36922 /*36922*/ AND `locale`='deDE') OR (`entry`=36932 /*36932*/ AND `locale`='deDE') OR (`entry`=36925 /*36925*/ AND `locale`='deDE') OR (`entry`=37741 /*37741*/ AND `locale`='deDE') OR (`entry`=6352 /*6352*/ AND `locale`='deDE') OR (`entry`=7886 /*7886*/ AND `locale`='deDE') OR (`entry`=6190 /*6190*/ AND `locale`='deDE') OR (`entry`=36437 /*36437*/ AND `locale`='deDE') OR (`entry`=62120 /*62120*/ AND `locale`='deDE') OR (`entry`=36077 /*36077*/ AND `locale`='deDE') OR (`entry`=36126 /*36126*/ AND `locale`='deDE') OR (`entry`=36297 /*36297*/ AND `locale`='deDE') OR (`entry`=36025 /*36025*/ AND `locale`='deDE') OR (`entry`=36061 /*36061*/ AND `locale`='deDE') OR (`entry`=36147 /*36147*/ AND `locale`='deDE') OR (`entry`=36989 /*36989*/ AND `locale`='deDE') OR (`entry`=62121 /*62121*/ AND `locale`='deDE') OR (`entry`=36407 /*36407*/ AND `locale`='deDE') OR (`entry`=36385 /*36385*/ AND `locale`='deDE') OR (`entry`=36384 /*36384*/ AND `locale`='deDE') OR (`entry`=35466 /*35466*/ AND `locale`='deDE') OR (`entry`=43244 /*43244*/ AND `locale`='deDE') OR (`entry`=36304 /*36304*/ AND `locale`='deDE') OR (`entry`=43217 /*43217*/ AND `locale`='deDE') OR (`entry`=36785 /*36785*/ AND `locale`='deDE') OR (`entry`=36662 /*36662*/ AND `locale`='deDE') OR (`entry`=36887 /*36887*/ AND `locale`='deDE') OR (`entry`=36884 /*36884*/ AND `locale`='deDE') OR (`entry`=36918 /*36918*/ AND `locale`='deDE') OR (`entry`=36720 /*36720*/ AND `locale`='deDE') OR (`entry`=36688 /*36688*/ AND `locale`='deDE') OR (`entry`=36687 /*36687*/ AND `locale`='deDE') OR (`entry`=36680 /*36680*/ AND `locale`='deDE') OR (`entry`=36755 /*36755*/ AND `locale`='deDE') OR (`entry`=36754 /*36754*/ AND `locale`='deDE') OR (`entry`=36753 /*36753*/ AND `locale`='deDE') OR (`entry`=36718 /*36718*/ AND `locale`='deDE') OR (`entry`=36716 /*36716*/ AND `locale`='deDE') OR (`entry`=36654 /*36654*/ AND `locale`='deDE') OR (`entry`=36729 /*36729*/ AND `locale`='deDE') OR (`entry`=36738 /*36738*/ AND `locale`='deDE') OR (`entry`=36707 /*36707*/ AND `locale`='deDE') OR (`entry`=36815 /*36815*/ AND `locale`='deDE') OR (`entry`=36903 /*36903*/ AND `locale`='deDE') OR (`entry`=36902 /*36902*/ AND `locale`='deDE') OR (`entry`=36900 /*36900*/ AND `locale`='deDE') OR (`entry`=36730 /*36730*/ AND `locale`='deDE') OR (`entry`=36728 /*36728*/ AND `locale`='deDE') OR (`entry`=36919 /*36919*/ AND `locale`='deDE') OR (`entry`=6377 /*6377*/ AND `locale`='deDE') OR (`entry`=37002 /*37002*/ AND `locale`='deDE') OR (`entry`=8660 /*8660*/ AND `locale`='deDE') OR (`entry`=8764 /*8764*/ AND `locale`='deDE') OR (`entry`=8761 /*8761*/ AND `locale`='deDE') OR (`entry`=6375 /*6375*/ AND `locale`='deDE') OR (`entry`=36660 /*36660*/ AND `locale`='deDE') OR (`entry`=36849 /*36849*/ AND `locale`='deDE') OR (`entry`=36914 /*36914*/ AND `locale`='deDE') OR (`entry`=36816 /*36816*/ AND `locale`='deDE') OR (`entry`=36852 /*36852*/ AND `locale`='deDE') OR (`entry`=36850 /*36850*/ AND `locale`='deDE') OR (`entry`=36890 /*36890*/ AND `locale`='deDE') OR (`entry`=36665 /*36665*/ AND `locale`='deDE') OR (`entry`=36673 /*36673*/ AND `locale`='deDE') OR (`entry`=36936 /*36936*/ AND `locale`='deDE') OR (`entry`=35312 /*35312*/ AND `locale`='deDE') OR (`entry`=35095 /*35095*/ AND `locale`='deDE') OR (`entry`=35296 /*35296*/ AND `locale`='deDE') OR (`entry`=35245 /*35245*/ AND `locale`='deDE') OR (`entry`=35257 /*35257*/ AND `locale`='deDE') OR (`entry`=35111 /*35111*/ AND `locale`='deDE') OR (`entry`=34626 /*34626*/ AND `locale`='deDE') OR (`entry`=34638 /*34638*/ AND `locale`='deDE') OR (`entry`=34634 /*34634*/ AND `locale`='deDE') OR (`entry`=34846 /*34846*/ AND `locale`='deDE') OR (`entry`=5841 /*5841*/ AND `locale`='deDE') OR (`entry`=5838 /*5838*/ AND `locale`='deDE') OR (`entry`=3397 /*3397*/ AND `locale`='deDE') OR (`entry`=4316 /*4316*/ AND `locale`='deDE') OR (`entry`=3274 /*3274*/ AND `locale`='deDE') OR (`entry`=34967 /*34967*/ AND `locale`='deDE') OR (`entry`=38820 /*38820*/ AND `locale`='deDE') OR (`entry`=34729 /*34729*/ AND `locale`='deDE') OR (`entry`=3467 /*3467*/ AND `locale`='deDE') OR (`entry`=34727 /*34727*/ AND `locale`='deDE') OR (`entry`=34752 /*34752*/ AND `locale`='deDE') OR (`entry`=4975 /*4975*/ AND `locale`='deDE') OR (`entry`=3641 /*3641*/ AND `locale`='deDE') OR (`entry`=3655 /*3655*/ AND `locale`='deDE') OR (`entry`=3638 /*3638*/ AND `locale`='deDE') OR (`entry`=3634 /*3634*/ AND `locale`='deDE') OR (`entry`=20797 /*20797*/ AND `locale`='deDE') OR (`entry`=3630 /*3630*/ AND `locale`='deDE') OR (`entry`=3631 /*3631*/ AND `locale`='deDE') OR (`entry`=3835 /*3835*/ AND `locale`='deDE') OR (`entry`=3633 /*3633*/ AND `locale`='deDE') OR (`entry`=3632 /*3632*/ AND `locale`='deDE') OR (`entry`=44170 /*44170*/ AND `locale`='deDE') OR (`entry`=3396 /*3396*/ AND `locale`='deDE') OR (`entry`=3398 /*3398*/ AND `locale`='deDE') OR (`entry`=3254 /*3254*/ AND `locale`='deDE') OR (`entry`=4005 /*4005*/ AND `locale`='deDE') OR (`entry`=12034 /*12034*/ AND `locale`='deDE') OR (`entry`=37167 /*37167*/ AND `locale`='deDE') OR (`entry`=37157 /*37157*/ AND `locale`='deDE') OR (`entry`=37136 /*37136*/ AND `locale`='deDE') OR (`entry`=40995 /*40995*/ AND `locale`='deDE') OR (`entry`=14781 /*14781*/ AND `locale`='deDE') OR (`entry`=14754 /*14754*/ AND `locale`='deDE') OR (`entry`=14718 /*14718*/ AND `locale`='deDE') OR (`entry`=19910 /*19910*/ AND `locale`='deDE') OR (`entry`=14717 /*14717*/ AND `locale`='deDE') OR (`entry`=71001 /*71001*/ AND `locale`='deDE') OR (`entry`=70999 /*70999*/ AND `locale`='deDE') OR (`entry`=70997 /*70997*/ AND `locale`='deDE') OR (`entry`=34841 /*34841*/ AND `locale`='deDE') OR (`entry`=34613 /*34613*/ AND `locale`='deDE') OR (`entry`=3470 /*3470*/ AND `locale`='deDE') OR (`entry`=52312 /*52312*/ AND `locale`='deDE') OR (`entry`=3986 /*3986*/ AND `locale`='deDE') OR (`entry`=60736 /*60736*/ AND `locale`='deDE') OR (`entry`=52526 /*52526*/ AND `locale`='deDE') OR (`entry`=52460 /*52460*/ AND `locale`='deDE') OR (`entry`=52456 /*52456*/ AND `locale`='deDE') OR (`entry`=52448 /*52448*/ AND `locale`='deDE') OR (`entry`=52457 /*52457*/ AND `locale`='deDE') OR (`entry`=14892 /*14892*/ AND `locale`='deDE') OR (`entry`=14857 /*14857*/ AND `locale`='deDE') OR (`entry`=71011 /*71011*/ AND `locale`='deDE') OR (`entry`=73590 /*73590*/ AND `locale`='deDE') OR (`entry`=71010 /*71010*/ AND `locale`='deDE') OR (`entry`=3236 /*3236*/ AND `locale`='deDE') OR (`entry`=71012 /*71012*/ AND `locale`='deDE') OR (`entry`=3389 /*3389*/ AND `locale`='deDE') OR (`entry`=9990 /*9990*/ AND `locale`='deDE') OR (`entry`=5837 /*5837*/ AND `locale`='deDE') OR (`entry`=3394 /*3394*/ AND `locale`='deDE') OR (`entry`=3449 /*3449*/ AND `locale`='deDE') OR (`entry`=43955 /*43955*/ AND `locale`='deDE') OR (`entry`=52196 /*52196*/ AND `locale`='deDE') OR (`entry`=5830 /*5830*/ AND `locale`='deDE') OR (`entry`=3452 /*3452*/ AND `locale`='deDE') OR (`entry`=3280 /*3280*/ AND `locale`='deDE') OR (`entry`=3279 /*3279*/ AND `locale`='deDE') OR (`entry`=3278 /*3278*/ AND `locale`='deDE') OR (`entry`=3682 /*3682*/ AND `locale`='deDE') OR (`entry`=3277 /*3277*/ AND `locale`='deDE') OR (`entry`=3276 /*3276*/ AND `locale`='deDE') OR (`entry`=3380 /*3380*/ AND `locale`='deDE') OR (`entry`=5865 /*5865*/ AND `locale`='deDE') OR (`entry`=43951 /*43951*/ AND `locale`='deDE') OR (`entry`=43946 /*43946*/ AND `locale`='deDE') OR (`entry`=14873 /*14873*/ AND `locale`='deDE') OR (`entry`=43988 /*43988*/ AND `locale`='deDE') OR (`entry`=34509 /*34509*/ AND `locale`='deDE') OR (`entry`=52207 /*52207*/ AND `locale`='deDE') OR (`entry`=52227 /*52227*/ AND `locale`='deDE') OR (`entry`=71006 /*71006*/ AND `locale`='deDE') OR (`entry`=5842 /*5842*/ AND `locale`='deDE') OR (`entry`=116079 /*116079*/ AND `locale`='deDE') OR (`entry`=116078 /*116078*/ AND `locale`='deDE') OR (`entry`=116077 /*116077*/ AND `locale`='deDE') OR (`entry`=115286 /*115286*/ AND `locale`='deDE') OR (`entry`=50870 /*50870*/ AND `locale`='deDE') OR (`entry`=3439 /*3439*/ AND `locale`='deDE') OR (`entry`=5836 /*5836*/ AND `locale`='deDE') OR (`entry`=3295 /*3295*/ AND `locale`='deDE') OR (`entry`=52357 /*52357*/ AND `locale`='deDE') OR (`entry`=52356 /*52356*/ AND `locale`='deDE') OR (`entry`=5835 /*5835*/ AND `locale`='deDE') OR (`entry`=3445 /*3445*/ AND `locale`='deDE') OR (`entry`=71005 /*71005*/ AND `locale`='deDE') OR (`entry`=14909 /*14909*/ AND `locale`='deDE') OR (`entry`=14908 /*14908*/ AND `locale`='deDE') OR (`entry`=52192 /*52192*/ AND `locale`='deDE') OR (`entry`=9316 /*9316*/ AND `locale`='deDE') OR (`entry`=3379 /*3379*/ AND `locale`='deDE') OR (`entry`=34647 /*34647*/ AND `locale`='deDE') OR (`entry`=52338 /*52338*/ AND `locale`='deDE') OR (`entry`=3471 /*3471*/ AND `locale`='deDE') OR (`entry`=3285 /*3285*/ AND `locale`='deDE') OR (`entry`=71002 /*71002*/ AND `locale`='deDE') OR (`entry`=71000 /*71000*/ AND `locale`='deDE') OR (`entry`=4127 /*4127*/ AND `locale`='deDE') OR (`entry`=7310 /*7310*/ AND `locale`='deDE') OR (`entry`=7307 /*7307*/ AND `locale`='deDE') OR (`entry`=7288 /*7288*/ AND `locale`='deDE') OR (`entry`=7287 /*7287*/ AND `locale`='deDE') OR (`entry`=7067 /*7067*/ AND `locale`='deDE') OR (`entry`=7308 /*7308*/ AND `locale`='deDE') OR (`entry`=7233 /*7233*/ AND `locale`='deDE') OR (`entry`=3282 /*3282*/ AND `locale`='deDE') OR (`entry`=52171 /*52171*/ AND `locale`='deDE') OR (`entry`=3284 /*3284*/ AND `locale`='deDE') OR (`entry`=43982 /*43982*/ AND `locale`='deDE') OR (`entry`=40558 /*40558*/ AND `locale`='deDE') OR (`entry`=3442 /*3442*/ AND `locale`='deDE') OR (`entry`=43945 /*43945*/ AND `locale`='deDE') OR (`entry`=34698 /*34698*/ AND `locale`='deDE') OR (`entry`=43957 /*43957*/ AND `locale`='deDE') OR (`entry`=34674 /*34674*/ AND `locale`='deDE') OR (`entry`=43953 /*43953*/ AND `locale`='deDE') OR (`entry`=44167 /*44167*/ AND `locale`='deDE') OR (`entry`=34730 /*34730*/ AND `locale`='deDE') OR (`entry`=34723 /*34723*/ AND `locale`='deDE') OR (`entry`=34721 /*34721*/ AND `locale`='deDE') OR (`entry`=34719 /*34719*/ AND `locale`='deDE') OR (`entry`=34718 /*34718*/ AND `locale`='deDE') OR (`entry`=34717 /*34717*/ AND `locale`='deDE') OR (`entry`=34715 /*34715*/ AND `locale`='deDE') OR (`entry`=3084 /*3084*/ AND `locale`='deDE') OR (`entry`=34513 /*34513*/ AND `locale`='deDE') OR (`entry`=5907 /*5907*/ AND `locale`='deDE') OR (`entry`=14894 /*14894*/ AND `locale`='deDE') OR (`entry`=14874 /*14874*/ AND `locale`='deDE') OR (`entry`=14872 /*14872*/ AND `locale`='deDE') OR (`entry`=10685 /*10685*/ AND `locale`='deDE') OR (`entry`=3432 /*3432*/ AND `locale`='deDE') OR (`entry`=34504 /*34504*/ AND `locale`='deDE') OR (`entry`=34563 /*34563*/ AND `locale`='deDE') OR (`entry`=34560 /*34560*/ AND `locale`='deDE') OR (`entry`=34578 /*34578*/ AND `locale`='deDE') OR (`entry`=14893 /*14893*/ AND `locale`='deDE') OR (`entry`=3487 /*3487*/ AND `locale`='deDE') OR (`entry`=3486 /*3486*/ AND `locale`='deDE') OR (`entry`=3488 /*3488*/ AND `locale`='deDE') OR (`entry`=3478 /*3478*/ AND `locale`='deDE') OR (`entry`=3477 /*3477*/ AND `locale`='deDE') OR (`entry`=3483 /*3483*/ AND `locale`='deDE') OR (`entry`=3479 /*3479*/ AND `locale`='deDE') OR (`entry`=3428 /*3428*/ AND `locale`='deDE') OR (`entry`=15597 /*15597*/ AND `locale`='deDE') OR (`entry`=3615 /*3615*/ AND `locale`='deDE') OR (`entry`=3490 /*3490*/ AND `locale`='deDE') OR (`entry`=3431 /*3431*/ AND `locale`='deDE') OR (`entry`=3429 /*3429*/ AND `locale`='deDE') OR (`entry`=3390 /*3390*/ AND `locale`='deDE') OR (`entry`=3485 /*3485*/ AND `locale`='deDE') OR (`entry`=3484 /*3484*/ AND `locale`='deDE') OR (`entry`=50033 /*50033*/ AND `locale`='deDE') OR (`entry`=9981 /*9981*/ AND `locale`='deDE') OR (`entry`=5871 /*5871*/ AND `locale`='deDE') OR (`entry`=5774 /*5774*/ AND `locale`='deDE') OR (`entry`=3934 /*3934*/ AND `locale`='deDE') OR (`entry`=3482 /*3482*/ AND `locale`='deDE') OR (`entry`=3481 /*3481*/ AND `locale`='deDE') OR (`entry`=3448 /*3448*/ AND `locale`='deDE') OR (`entry`=18739 /*18739*/ AND `locale`='deDE') OR (`entry`=34828 /*34828*/ AND `locale`='deDE') OR (`entry`=44164 /*44164*/ AND `locale`='deDE') OR (`entry`=34547 /*34547*/ AND `locale`='deDE') OR (`entry`=3271 /*3271*/ AND `locale`='deDE') OR (`entry`=34543 /*34543*/ AND `locale`='deDE') OR (`entry`=34544 /*34544*/ AND `locale`='deDE') OR (`entry`=3438 /*3438*/ AND `locale`='deDE') OR (`entry`=34829 /*34829*/ AND `locale`='deDE') OR (`entry`=44165 /*44165*/ AND `locale`='deDE') OR (`entry`=3241 /*3241*/ AND `locale`='deDE') OR (`entry`=5828 /*5828*/ AND `locale`='deDE') OR (`entry`=3416 /*3416*/ AND `locale`='deDE') OR (`entry`=5831 /*5831*/ AND `locale`='deDE') OR (`entry`=34576 /*34576*/ AND `locale`='deDE') OR (`entry`=71211 /*71211*/ AND `locale`='deDE') OR (`entry`=3245 /*3245*/ AND `locale`='deDE') OR (`entry`=34759 /*34759*/ AND `locale`='deDE') OR (`entry`=34754 /*34754*/ AND `locale`='deDE') OR (`entry`=34656 /*34656*/ AND `locale`='deDE') OR (`entry`=34651 /*34651*/ AND `locale`='deDE') OR (`entry`=6791 /*6791*/ AND `locale`='deDE') OR (`entry`=6253 /*6253*/ AND `locale`='deDE') OR (`entry`=6252 /*6252*/ AND `locale`='deDE') OR (`entry`=23536 /*23536*/ AND `locale`='deDE') OR (`entry`=23535 /*23535*/ AND `locale`='deDE') OR (`entry`=23534 /*23534*/ AND `locale`='deDE') OR (`entry`=6254 /*6254*/ AND `locale`='deDE') OR (`entry`=6266 /*6266*/ AND `locale`='deDE') OR (`entry`=6251 /*6251*/ AND `locale`='deDE') OR (`entry`=7166 /*7166*/ AND `locale`='deDE') OR (`entry`=7161 /*7161*/ AND `locale`='deDE') OR (`entry`=8738 /*8738*/ AND `locale`='deDE') OR (`entry`=3391 /*3391*/ AND `locale`='deDE') OR (`entry`=3495 /*3495*/ AND `locale`='deDE') OR (`entry`=3494 /*3494*/ AND `locale`='deDE') OR (`entry`=15582 /*15582*/ AND `locale`='deDE') OR (`entry`=10063 /*10063*/ AND `locale`='deDE') OR (`entry`=8119 /*8119*/ AND `locale`='deDE') OR (`entry`=3496 /*3496*/ AND `locale`='deDE') OR (`entry`=3572 /*3572*/ AND `locale`='deDE') OR (`entry`=3339 /*3339*/ AND `locale`='deDE') OR (`entry`=3493 /*3493*/ AND `locale`='deDE') OR (`entry`=3491 /*3491*/ AND `locale`='deDE') OR (`entry`=3446 /*3446*/ AND `locale`='deDE') OR (`entry`=3492 /*3492*/ AND `locale`='deDE') OR (`entry`=3499 /*3499*/ AND `locale`='deDE') OR (`entry`=3292 /*3292*/ AND `locale`='deDE') OR (`entry`=3498 /*3498*/ AND `locale`='deDE') OR (`entry`=3464 /*3464*/ AND `locale`='deDE') OR (`entry`=16418 /*16418*/ AND `locale`='deDE') OR (`entry`=71007 /*71007*/ AND `locale`='deDE') OR (`entry`=71207 /*71207*/ AND `locale`='deDE') OR (`entry`=71188 /*71188*/ AND `locale`='deDE') OR (`entry`=71187 /*71187*/ AND `locale`='deDE') OR (`entry`=14859 /*14859*/ AND `locale`='deDE') OR (`entry`=71009 /*71009*/ AND `locale`='deDE') OR (`entry`=8306 /*8306*/ AND `locale`='deDE') OR (`entry`=3443 /*3443*/ AND `locale`='deDE') OR (`entry`=14850 /*14850*/ AND `locale`='deDE') OR (`entry`=8307 /*8307*/ AND `locale`='deDE') OR (`entry`=50028 /*50028*/ AND `locale`='deDE') OR (`entry`=3338 /*3338*/ AND `locale`='deDE') OR (`entry`=50029 /*50029*/ AND `locale`='deDE') OR (`entry`=50032 /*50032*/ AND `locale`='deDE') OR (`entry`=50027 /*50027*/ AND `locale`='deDE') OR (`entry`=3659 /*3659*/ AND `locale`='deDE') OR (`entry`=3658 /*3658*/ AND `locale`='deDE') OR (`entry`=3480 /*3480*/ AND `locale`='deDE') OR (`entry`=3489 /*3489*/ AND `locale`='deDE') OR (`entry`=34503 /*34503*/ AND `locale`='deDE') OR (`entry`=3269 /*3269*/ AND `locale`='deDE') OR (`entry`=34545 /*34545*/ AND `locale`='deDE') OR (`entry`=3268 /*3268*/ AND `locale`='deDE') OR (`entry`=3266 /*3266*/ AND `locale`='deDE') OR (`entry`=34287 /*34287*/ AND `locale`='deDE') OR (`entry`=34285 /*34285*/ AND `locale`='deDE') OR (`entry`=14901 /*14901*/ AND `locale`='deDE') OR (`entry`=34284 /*34284*/ AND `locale`='deDE') OR (`entry`=5810 /*5810*/ AND `locale`='deDE') OR (`entry`=43956 /*43956*/ AND `locale`='deDE') OR (`entry`=51914 /*51914*/ AND `locale`='deDE') OR (`entry`=43949 /*43949*/ AND `locale`='deDE') OR (`entry`=34258 /*34258*/ AND `locale`='deDE') OR (`entry`=9336 /*9336*/ AND `locale`='deDE') OR (`entry`=3286 /*3286*/ AND `locale`='deDE') OR (`entry`=3283 /*3283*/ AND `locale`='deDE') OR (`entry`=3296 /*3296*/ AND `locale`='deDE') OR (`entry`=51346 /*51346*/ AND `locale`='deDE') OR (`entry`=74228 /*74228*/ AND `locale`='deDE') OR (`entry`=39596 /*39596*/ AND `locale`='deDE') OR (`entry`=39595 /*39595*/ AND `locale`='deDE') OR (`entry`=39385 /*39385*/ AND `locale`='deDE') OR (`entry`=3127 /*3127*/ AND `locale`='deDE') OR (`entry`=3126 /*3126*/ AND `locale`='deDE') OR (`entry`=39324 /*39324*/ AND `locale`='deDE') OR (`entry`=34259 /*34259*/ AND `locale`='deDE') OR (`entry`=3337 /*3337*/ AND `locale`='deDE') OR (`entry`=34261 /*34261*/ AND `locale`='deDE') OR (`entry`=3521 /*3521*/ AND `locale`='deDE') OR (`entry`=3123 /*3123*/ AND `locale`='deDE') OR (`entry`=44166 /*44166*/ AND `locale`='deDE') OR (`entry`=34280 /*34280*/ AND `locale`='deDE') OR (`entry`=3122 /*3122*/ AND `locale`='deDE') OR (`entry`=3267 /*3267*/ AND `locale`='deDE') OR (`entry`=39337 /*39337*/ AND `locale`='deDE') OR (`entry`=3100 /*3100*/ AND `locale`='deDE') OR (`entry`=39452 /*39452*/ AND `locale`='deDE') OR (`entry`=3113 /*3113*/ AND `locale`='deDE') OR (`entry`=3114 /*3114*/ AND `locale`='deDE') OR (`entry`=3265 /*3265*/ AND `locale`='deDE') OR (`entry`=3244 /*3244*/ AND `locale`='deDE') OR (`entry`=44057 /*44057*/ AND `locale`='deDE') OR (`entry`=3110 /*3110*/ AND `locale`='deDE') OR (`entry`=5435 /*5435*/ AND `locale`='deDE') OR (`entry`=16227 /*16227*/ AND `locale`='deDE') OR (`entry`=44058 /*44058*/ AND `locale`='deDE') OR (`entry`=3497 /*3497*/ AND `locale`='deDE') OR (`entry`=3665 /*3665*/ AND `locale`='deDE') OR (`entry`=8496 /*8496*/ AND `locale`='deDE') OR (`entry`=9558 /*9558*/ AND `locale`='deDE') OR (`entry`=3502 /*3502*/ AND `locale`='deDE') OR (`entry`=3453 /*3453*/ AND `locale`='deDE') OR (`entry`=5629 /*5629*/ AND `locale`='deDE') OR (`entry`=5901 /*5901*/ AND `locale`='deDE') OR (`entry`=3388 /*3388*/ AND `locale`='deDE') OR (`entry`=34707 /*34707*/ AND `locale`='deDE') OR (`entry`=3382 /*3382*/ AND `locale`='deDE') OR (`entry`=6240 /*6240*/ AND `locale`='deDE') OR (`entry`=44168 /*44168*/ AND `locale`='deDE') OR (`entry`=34804 /*34804*/ AND `locale`='deDE') OR (`entry`=34750 /*34750*/ AND `locale`='deDE') OR (`entry`=34749 /*34749*/ AND `locale`='deDE') OR (`entry`=3476 /*3476*/ AND `locale`='deDE') OR (`entry`=25297 /*25297*/ AND `locale`='deDE') OR (`entry`=34747 /*34747*/ AND `locale`='deDE') OR (`entry`=3384 /*3384*/ AND `locale`='deDE') OR (`entry`=3383 /*3383*/ AND `locale`='deDE') OR (`entry`=34706 /*34706*/ AND `locale`='deDE') OR (`entry`=4955 /*4955*/ AND `locale`='deDE') OR (`entry`=34733 /*34733*/ AND `locale`='deDE') OR (`entry`=35198 /*35198*/ AND `locale`='deDE') OR (`entry`=35096 /*35096*/ AND `locale`='deDE') OR (`entry`=11046 /*11046*/ AND `locale`='deDE') OR (`entry`=8659 /*8659*/ AND `locale`='deDE') OR (`entry`=3348 /*3348*/ AND `locale`='deDE') OR (`entry`=3347 /*3347*/ AND `locale`='deDE') OR (`entry`=58199 /*58199*/ AND `locale`='deDE') OR (`entry`=51230 /*51230*/ AND `locale`='deDE') OR (`entry`=45244 /*45244*/ AND `locale`='deDE') OR (`entry`=44780 /*44780*/ AND `locale`='deDE') OR (`entry`=47336 /*47336*/ AND `locale`='deDE') OR (`entry`=47321 /*47321*/ AND `locale`='deDE') OR (`entry`=47335 /*47335*/ AND `locale`='deDE') OR (`entry`=54113 /*54113*/ AND `locale`='deDE') OR (`entry`=44779 /*44779*/ AND `locale`='deDE') OR (`entry`=44770 /*44770*/ AND `locale`='deDE') OR (`entry`=3403 /*3403*/ AND `locale`='deDE') OR (`entry`=13417 /*13417*/ AND `locale`='deDE') OR (`entry`=5892 /*5892*/ AND `locale`='deDE') OR (`entry`=44785 /*44785*/ AND `locale`='deDE') OR (`entry`=44745 /*44745*/ AND `locale`='deDE') OR (`entry`=44726 /*44726*/ AND `locale`='deDE') OR (`entry`=44740 /*44740*/ AND `locale`='deDE') OR (`entry`=44735 /*44735*/ AND `locale`='deDE') OR (`entry`=45230 /*45230*/ AND `locale`='deDE') OR (`entry`=44788 /*44788*/ AND `locale`='deDE') OR (`entry`=19176 /*19176*/ AND `locale`='deDE') OR (`entry`=44787 /*44787*/ AND `locale`='deDE') OR (`entry`=44782 /*44782*/ AND `locale`='deDE') OR (`entry`=15188 /*15188*/ AND `locale`='deDE') OR (`entry`=44783 /*44783*/ AND `locale`='deDE') OR (`entry`=3322 /*3322*/ AND `locale`='deDE') OR (`entry`=44781 /*44781*/ AND `locale`='deDE') OR (`entry`=44743 /*44743*/ AND `locale`='deDE') OR (`entry`=44723 /*44723*/ AND `locale`='deDE') OR (`entry`=44725 /*44725*/ AND `locale`='deDE') OR (`entry`=69978 /*69978*/ AND `locale`='deDE') OR (`entry`=69977 /*69977*/ AND `locale`='deDE') OR (`entry`=12791 /*12791*/ AND `locale`='deDE') OR (`entry`=12790 /*12790*/ AND `locale`='deDE') OR (`entry`=29143 /*29143*/ AND `locale`='deDE') OR (`entry`=73151 /*73151*/ AND `locale`='deDE') OR (`entry`=12798 /*12798*/ AND `locale`='deDE') OR (`entry`=12796 /*12796*/ AND `locale`='deDE') OR (`entry`=10880 /*10880*/ AND `locale`='deDE') OR (`entry`=5910 /*5910*/ AND `locale`='deDE') OR (`entry`=3314 /*3314*/ AND `locale`='deDE') OR (`entry`=49622 /*49622*/ AND `locale`='deDE') OR (`entry`=4047 /*4047*/ AND `locale`='deDE') OR (`entry`=3144 /*3144*/ AND `locale`='deDE') OR (`entry`=45339 /*45339*/ AND `locale`='deDE') OR (`entry`=46142 /*46142*/ AND `locale`='deDE') OR (`entry`=3334 /*3334*/ AND `locale`='deDE') OR (`entry`=44338 /*44338*/ AND `locale`='deDE') OR (`entry`=88543 /*88543*/ AND `locale`='deDE') OR (`entry`=23128 /*23128*/ AND `locale`='deDE') OR (`entry`=35068 /*35068*/ AND `locale`='deDE') OR (`entry`=46555 /*46555*/ AND `locale`='deDE') OR (`entry`=58155 /*58155*/ AND `locale`='deDE') OR (`entry`=46556 /*46556*/ AND `locale`='deDE') OR (`entry`=52034 /*52034*/ AND `locale`='deDE') OR (`entry`=44160 /*44160*/ AND `locale`='deDE') OR (`entry`=34765 /*34765*/ AND `locale`='deDE') OR (`entry`=3335 /*3335*/ AND `locale`='deDE') OR (`entry`=5816 /*5816*/ AND `locale`='deDE') OR (`entry`=3330 /*3330*/ AND `locale`='deDE') OR (`entry`=89175 /*89175*/ AND `locale`='deDE') OR (`entry`=3189 /*3189*/ AND `locale`='deDE') OR (`entry`=3328 /*3328*/ AND `locale`='deDE') OR (`entry`=3327 /*3327*/ AND `locale`='deDE') OR (`entry`=47233 /*47233*/ AND `locale`='deDE') OR (`entry`=3323 /*3323*/ AND `locale`='deDE') OR (`entry`=45337 /*45337*/ AND `locale`='deDE') OR (`entry`=17098 /*17098*/ AND `locale`='deDE') OR (`entry`=46140 /*46140*/ AND `locale`='deDE') OR (`entry`=47571 /*47571*/ AND `locale`='deDE') OR (`entry`=26537 /*26537*/ AND `locale`='deDE') OR (`entry`=47253 /*47253*/ AND `locale`='deDE') OR (`entry`=3331 /*3331*/ AND `locale`='deDE') OR (`entry`=20490 /*20490*/ AND `locale`='deDE') OR (`entry`=20489 /*20489*/ AND `locale`='deDE') OR (`entry`=20491 /*20491*/ AND `locale`='deDE') OR (`entry`=5909 /*5909*/ AND `locale`='deDE') OR (`entry`=47254 /*47254*/ AND `locale`='deDE') OR (`entry`=88706 /*88706*/ AND `locale`='deDE') OR (`entry`=5875 /*5875*/ AND `locale`='deDE') OR (`entry`=88704 /*88704*/ AND `locale`='deDE') OR (`entry`=88705 /*88705*/ AND `locale`='deDE') OR (`entry`=47248 /*47248*/ AND `locale`='deDE') OR (`entry`=47246 /*47246*/ AND `locale`='deDE') OR (`entry`=47247 /*47247*/ AND `locale`='deDE') OR (`entry`=14375 /*14375*/ AND `locale`='deDE') OR (`entry`=20486 /*20486*/ AND `locale`='deDE') OR (`entry`=20488 /*20488*/ AND `locale`='deDE') OR (`entry`=20493 /*20493*/ AND `locale`='deDE') OR (`entry`=20492 /*20492*/ AND `locale`='deDE') OR (`entry`=47897 /*47897*/ AND `locale`='deDE') OR (`entry`=44948 /*44948*/ AND `locale`='deDE') OR (`entry`=44918 /*44918*/ AND `locale`='deDE') OR (`entry`=19177 /*19177*/ AND `locale`='deDE') OR (`entry`=14720 /*14720*/ AND `locale`='deDE') OR (`entry`=3329 /*3329*/ AND `locale`='deDE') OR (`entry`=5639 /*5639*/ AND `locale`='deDE') OR (`entry`=3216 /*3216*/ AND `locale`='deDE') OR (`entry`=62194 /*62194*/ AND `locale`='deDE') OR (`entry`=62198 /*62198*/ AND `locale`='deDE') OR (`entry`=44919 /*44919*/ AND `locale`='deDE') OR (`entry`=3310 /*3310*/ AND `locale`='deDE') OR (`entry`=50477 /*50477*/ AND `locale`='deDE') OR (`entry`=50323 /*50323*/ AND `locale`='deDE') OR (`entry`=3346 /*3346*/ AND `locale`='deDE') OR (`entry`=46181 /*46181*/ AND `locale`='deDE') OR (`entry`=23635 /*23635*/ AND `locale`='deDE') OR (`entry`=50488 /*50488*/ AND `locale`='deDE') OR (`entry`=15891 /*15891*/ AND `locale`='deDE') OR (`entry`=15579 /*15579*/ AND `locale`='deDE') OR (`entry`=44158 /*44158*/ AND `locale`='deDE') OR (`entry`=43062 /*43062*/ AND `locale`='deDE') OR (`entry`=9564 /*9564*/ AND `locale`='deDE') OR (`entry`=11066 /*11066*/ AND `locale`='deDE') OR (`entry`=3345 /*3345*/ AND `locale`='deDE') OR (`entry`=42506 /*42506*/ AND `locale`='deDE') OR (`entry`=49737 /*49737*/ AND `locale`='deDE') OR (`entry`=63626 /*63626*/ AND `locale`='deDE') OR (`entry`=12136 /*12136*/ AND `locale`='deDE') OR (`entry`=52810 /*52810*/ AND `locale`='deDE') OR (`entry`=52812 /*52812*/ AND `locale`='deDE') OR (`entry`=52809 /*52809*/ AND `locale`='deDE') OR (`entry`=3369 /*3369*/ AND `locale`='deDE') OR (`entry`=46709 /*46709*/ AND `locale`='deDE') OR (`entry`=3368 /*3368*/ AND `locale`='deDE') OR (`entry`=46708 /*46708*/ AND `locale`='deDE') OR (`entry`=46742 /*46742*/ AND `locale`='deDE') OR (`entry`=46716 /*46716*/ AND `locale`='deDE') OR (`entry`=7010 /*7010*/ AND `locale`='deDE') OR (`entry`=49131 /*49131*/ AND `locale`='deDE') OR (`entry`=3315 /*3315*/ AND `locale`='deDE') OR (`entry`=14499 /*14499*/ AND `locale`='deDE') OR (`entry`=2857 /*2857*/ AND `locale`='deDE') OR (`entry`=14498 /*14498*/ AND `locale`='deDE') OR (`entry`=3412 /*3412*/ AND `locale`='deDE') OR (`entry`=62199 /*62199*/ AND `locale`='deDE') OR (`entry`=62200 /*62200*/ AND `locale`='deDE') OR (`entry`=11017 /*11017*/ AND `locale`='deDE') OR (`entry`=3413 /*3413*/ AND `locale`='deDE') OR (`entry`=14451 /*14451*/ AND `locale`='deDE') OR (`entry`=52032 /*52032*/ AND `locale`='deDE') OR (`entry`=46741 /*46741*/ AND `locale`='deDE') OR (`entry`=46718 /*46718*/ AND `locale`='deDE') OR (`entry`=3372 /*3372*/ AND `locale`='deDE') OR (`entry`=3371 /*3371*/ AND `locale`='deDE') OR (`entry`=3367 /*3367*/ AND `locale`='deDE') OR (`entry`=3351 /*3351*/ AND `locale`='deDE') OR (`entry`=3350 /*3350*/ AND `locale`='deDE') OR (`entry`=3364 /*3364*/ AND `locale`='deDE') OR (`entry`=3363 /*3363*/ AND `locale`='deDE') OR (`entry`=3317 /*3317*/ AND `locale`='deDE') OR (`entry`=2855 /*2855*/ AND `locale`='deDE') OR (`entry`=3321 /*3321*/ AND `locale`='deDE') OR (`entry`=6986 /*6986*/ AND `locale`='deDE') OR (`entry`=6987 /*6987*/ AND `locale`='deDE') OR (`entry`=3366 /*3366*/ AND `locale`='deDE') OR (`entry`=3225 /*3225*/ AND `locale`='deDE') OR (`entry`=5811 /*5811*/ AND `locale`='deDE') OR (`entry`=3365 /*3365*/ AND `locale`='deDE') OR (`entry`=7088 /*7088*/ AND `locale`='deDE') OR (`entry`=3316 /*3316*/ AND `locale`='deDE') OR (`entry`=62196 /*62196*/ AND `locale`='deDE') OR (`entry`=62197 /*62197*/ AND `locale`='deDE') OR (`entry`=42709 /*42709*/ AND `locale`='deDE') OR (`entry`=4485 /*4485*/ AND `locale`='deDE') OR (`entry`=42638 /*42638*/ AND `locale`='deDE') OR (`entry`=46619 /*46619*/ AND `locale`='deDE') OR (`entry`=46620 /*46620*/ AND `locale`='deDE') OR (`entry`=46618 /*46618*/ AND `locale`='deDE') OR (`entry`=46622 /*46622*/ AND `locale`='deDE') OR (`entry`=46621 /*46621*/ AND `locale`='deDE') OR (`entry`=46642 /*46642*/ AND `locale`='deDE') OR (`entry`=19175 /*19175*/ AND `locale`='deDE') OR (`entry`=3359 /*3359*/ AND `locale`='deDE') OR (`entry`=47663 /*47663*/ AND `locale`='deDE') OR (`entry`=72559 /*72559*/ AND `locale`='deDE') OR (`entry`=62193 /*62193*/ AND `locale`='deDE') OR (`entry`=16076 /*16076*/ AND `locale`='deDE') OR (`entry`=16069 /*16069*/ AND `locale`='deDE') OR (`entry`=66022 /*66022*/ AND `locale`='deDE') OR (`entry`=70301 /*70301*/ AND `locale`='deDE') OR (`entry`=12351 /*12351*/ AND `locale`='deDE') OR (`entry`=356 /*356*/ AND `locale`='deDE') OR (`entry`=14540 /*14540*/ AND `locale`='deDE') OR (`entry`=14541 /*14541*/ AND `locale`='deDE') OR (`entry`=3332 /*3332*/ AND `locale`='deDE') OR (`entry`=3333 /*3333*/ AND `locale`='deDE') OR (`entry`=66437 /*66437*/ AND `locale`='deDE') OR (`entry`=12353 /*12353*/ AND `locale`='deDE') OR (`entry`=64105 /*64105*/ AND `locale`='deDE') OR (`entry`=5195 /*5195*/ AND `locale`='deDE') OR (`entry`=43239 /*43239*/ AND `locale`='deDE') OR (`entry`=3362 /*3362*/ AND `locale`='deDE') OR (`entry`=4752 /*4752*/ AND `locale`='deDE') OR (`entry`=47808 /*47808*/ AND `locale`='deDE') OR (`entry`=15186 /*15186*/ AND `locale`='deDE') OR (`entry`=69333 /*69333*/ AND `locale`='deDE') OR (`entry`=7951 /*7951*/ AND `locale`='deDE') OR (`entry`=68869 /*68869*/ AND `locale`='deDE') OR (`entry`=62445 /*62445*/ AND `locale`='deDE') OR (`entry`=65008 /*65008*/ AND `locale`='deDE') OR (`entry`=16869 /*16869*/ AND `locale`='deDE') OR (`entry`=16868 /*16868*/ AND `locale`='deDE') OR (`entry`=2756 /*2756*/ AND `locale`='deDE') OR (`entry`=5034 /*5034*/ AND `locale`='deDE') OR (`entry`=5029 /*5029*/ AND `locale`='deDE') OR (`entry`=47809 /*47809*/ AND `locale`='deDE') OR (`entry`=31768 /*31768*/ AND `locale`='deDE') OR (`entry`=31769 /*31769*/ AND `locale`='deDE') OR (`entry`=47764 /*47764*/ AND `locale`='deDE') OR (`entry`=31757 /*31757*/ AND `locale`='deDE') OR (`entry`=31756 /*31756*/ AND `locale`='deDE') OR (`entry`=47771 /*47771*/ AND `locale`='deDE') OR (`entry`=31758 /*31758*/ AND `locale`='deDE') OR (`entry`=46639 /*46639*/ AND `locale`='deDE') OR (`entry`=46637 /*46637*/ AND `locale`='deDE') OR (`entry`=46640 /*46640*/ AND `locale`='deDE') OR (`entry`=31755 /*31755*/ AND `locale`='deDE') OR (`entry`=14539 /*14539*/ AND `locale`='deDE') OR (`entry`=46638 /*46638*/ AND `locale`='deDE') OR (`entry`=114840 /*114840*/ AND `locale`='deDE') OR (`entry`=47818 /*47818*/ AND `locale`='deDE') OR (`entry`=47817 /*47817*/ AND `locale`='deDE') OR (`entry`=47815 /*47815*/ AND `locale`='deDE') OR (`entry`=47788 /*47788*/ AND `locale`='deDE') OR (`entry`=47767 /*47767*/ AND `locale`='deDE') OR (`entry`=3352 /*3352*/ AND `locale`='deDE') OR (`entry`=35162 /*35162*/ AND `locale`='deDE') OR (`entry`=35086 /*35086*/ AND `locale`='deDE') OR (`entry`=8576 /*8576*/ AND `locale`='deDE') OR (`entry`=42672 /*42672*/ AND `locale`='deDE') OR (`entry`=42673 /*42673*/ AND `locale`='deDE') OR (`entry`=42671 /*42671*/ AND `locale`='deDE') OR (`entry`=42650 /*42650*/ AND `locale`='deDE') OR (`entry`=57922 /*57922*/ AND `locale`='deDE') OR (`entry`=50482 /*50482*/ AND `locale`='deDE') OR (`entry`=46675 /*46675*/ AND `locale`='deDE') OR (`entry`=3358 /*3358*/ AND `locale`='deDE') OR (`entry`=3357 /*3357*/ AND `locale`='deDE') OR (`entry`=11178 /*11178*/ AND `locale`='deDE') OR (`entry`=11177 /*11177*/ AND `locale`='deDE') OR (`entry`=11176 /*11176*/ AND `locale`='deDE') OR (`entry`=10266 /*10266*/ AND `locale`='deDE') OR (`entry`=7793 /*7793*/ AND `locale`='deDE') OR (`entry`=7792 /*7792*/ AND `locale`='deDE') OR (`entry`=5812 /*5812*/ AND `locale`='deDE') OR (`entry`=4043 /*4043*/ AND `locale`='deDE') OR (`entry`=3409 /*3409*/ AND `locale`='deDE') OR (`entry`=3356 /*3356*/ AND `locale`='deDE') OR (`entry`=3355 /*3355*/ AND `locale`='deDE') OR (`entry`=1383 /*1383*/ AND `locale`='deDE') OR (`entry`=42548 /*42548*/ AND `locale`='deDE') OR (`entry`=7790 /*7790*/ AND `locale`='deDE') OR (`entry`=7231 /*7231*/ AND `locale`='deDE') OR (`entry`=7230 /*7230*/ AND `locale`='deDE') OR (`entry`=3361 /*3361*/ AND `locale`='deDE') OR (`entry`=3360 /*3360*/ AND `locale`='deDE') OR (`entry`=13842 /*13842*/ AND `locale`='deDE') OR (`entry`=46667 /*46667*/ AND `locale`='deDE') OR (`entry`=3354 /*3354*/ AND `locale`='deDE') OR (`entry`=3353 /*3353*/ AND `locale`='deDE') OR (`entry`=35364 /*35364*/ AND `locale`='deDE') OR (`entry`=34955 /*34955*/ AND `locale`='deDE') OR (`entry`=49573 /*49573*/ AND `locale`='deDE') OR (`entry`=3199 /*3199*/ AND `locale`='deDE') OR (`entry`=3196 /*3196*/ AND `locale`='deDE') OR (`entry`=3195 /*3195*/ AND `locale`='deDE') OR (`entry`=3117 /*3117*/ AND `locale`='deDE') OR (`entry`=39604 /*39604*/ AND `locale`='deDE') OR (`entry`=3208 /*3208*/ AND `locale`='deDE') OR (`entry`=3118 /*3118*/ AND `locale`='deDE') OR (`entry`=3203 /*3203*/ AND `locale`='deDE') OR (`entry`=66145 /*66145*/ AND `locale`='deDE') OR (`entry`=66144 /*66144*/ AND `locale`='deDE') OR (`entry`=66126 /*66126*/ AND `locale`='deDE') OR (`entry`=39603 /*39603*/ AND `locale`='deDE') OR (`entry`=40971 /*40971*/ AND `locale`='deDE') OR (`entry`=40970 /*40970*/ AND `locale`='deDE') OR (`entry`=3035 /*3035*/ AND `locale`='deDE') OR (`entry`=44599 /*44599*/ AND `locale`='deDE') OR (`entry`=3293 /*3293*/ AND `locale`='deDE') OR (`entry`=3116 /*3116*/ AND `locale`='deDE') OR (`entry`=3115 /*3115*/ AND `locale`='deDE') OR (`entry`=3193 /*3193*/ AND `locale`='deDE') OR (`entry`=39325 /*39325*/ AND `locale`='deDE') OR (`entry`=43331 /*43331*/ AND `locale`='deDE') OR (`entry`=39464 /*39464*/ AND `locale`='deDE') OR (`entry`=40949 /*40949*/ AND `locale`='deDE') OR (`entry`=40948 /*40948*/ AND `locale`='deDE') OR (`entry`=39380 /*39380*/ AND `locale`='deDE') OR (`entry`=39379 /*39379*/ AND `locale`='deDE') OR (`entry`=42504 /*42504*/ AND `locale`='deDE') OR (`entry`=42859 /*42859*/ AND `locale`='deDE') OR (`entry`=71100 /*71100*/ AND `locale`='deDE') OR (`entry`=4311 /*4311*/ AND `locale`='deDE') OR (`entry`=3204 /*3204*/ AND `locale`='deDE') OR (`entry`=40891 /*40891*/ AND `locale`='deDE') OR (`entry`=3198 /*3198*/ AND `locale`='deDE') OR (`entry`=3197 /*3197*/ AND `locale`='deDE') OR (`entry`=88750 /*88750*/ AND `locale`='deDE') OR (`entry`=86884 /*86884*/ AND `locale`='deDE') OR (`entry`=41621 /*41621*/ AND `locale`='deDE') OR (`entry`=40893 /*40893*/ AND `locale`='deDE') OR (`entry`=3108 /*3108*/ AND `locale`='deDE') OR (`entry`=39353 /*39353*/ AND `locale`='deDE') OR (`entry`=39590 /*39590*/ AND `locale`='deDE') OR (`entry`=39351 /*39351*/ AND `locale`='deDE') OR (`entry`=39519 /*39519*/ AND `locale`='deDE') OR (`entry`=39352 /*39352*/ AND `locale`='deDE') OR (`entry`=39323 /*39323*/ AND `locale`='deDE') OR (`entry`=11943 /*11943*/ AND `locale`='deDE') OR (`entry`=6928 /*6928*/ AND `locale`='deDE') OR (`entry`=6787 /*6787*/ AND `locale`='deDE') OR (`entry`=3175 /*3175*/ AND `locale`='deDE') OR (`entry`=3174 /*3174*/ AND `locale`='deDE') OR (`entry`=3167 /*3167*/ AND `locale`='deDE') OR (`entry`=3163 /*3163*/ AND `locale`='deDE') OR (`entry`=11025 /*11025*/ AND `locale`='deDE') OR (`entry`=63063 /*63063*/ AND `locale`='deDE') OR (`entry`=63061 /*63061*/ AND `locale`='deDE') OR (`entry`=3166 /*3166*/ AND `locale`='deDE') OR (`entry`=3165 /*3165*/ AND `locale`='deDE') OR (`entry`=44380 /*44380*/ AND `locale`='deDE') OR (`entry`=9987 /*9987*/ AND `locale`='deDE') OR (`entry`=3294 /*3294*/ AND `locale`='deDE') OR (`entry`=3172 /*3172*/ AND `locale`='deDE') OR (`entry`=37912 /*37912*/ AND `locale`='deDE') OR (`entry`=39270 /*39270*/ AND `locale`='deDE') OR (`entry`=6027 /*6027*/ AND `locale`='deDE') OR (`entry`=39272 /*39272*/ AND `locale`='deDE') OR (`entry`=39072 /*39072*/ AND `locale`='deDE') OR (`entry`=37989 /*37989*/ AND `locale`='deDE') OR (`entry`=39157 /*39157*/ AND `locale`='deDE') OR (`entry`=37987 /*37987*/ AND `locale`='deDE') OR (`entry`=37969 /*37969*/ AND `locale`='deDE') OR (`entry`=37961 /*37961*/ AND `locale`='deDE') OR (`entry`=38282 /*38282*/ AND `locale`='deDE') OR (`entry`=38247 /*38247*/ AND `locale`='deDE') OR (`entry`=38245 /*38245*/ AND `locale`='deDE') OR (`entry`=38280 /*38280*/ AND `locale`='deDE') OR (`entry`=38278 /*38278*/ AND `locale`='deDE') OR (`entry`=38243 /*38243*/ AND `locale`='deDE') OR (`entry`=47057 /*47057*/ AND `locale`='deDE') OR (`entry`=38279 /*38279*/ AND `locale`='deDE') OR (`entry`=38246 /*38246*/ AND `locale`='deDE') OR (`entry`=38966 /*38966*/ AND `locale`='deDE') OR (`entry`=90113 /*90113*/ AND `locale`='deDE') OR (`entry`=39033 /*39033*/ AND `locale`='deDE') OR (`entry`=39027 /*39027*/ AND `locale`='deDE') OR (`entry`=39008 /*39008*/ AND `locale`='deDE') OR (`entry`=39007 /*39007*/ AND `locale`='deDE') OR (`entry`=38989 /*38989*/ AND `locale`='deDE') OR (`entry`=39031 /*39031*/ AND `locale`='deDE') OR (`entry`=39032 /*39032*/ AND `locale`='deDE') OR (`entry`=38005 /*38005*/ AND `locale`='deDE') OR (`entry`=37960 /*37960*/ AND `locale`='deDE') OR (`entry`=38988 /*38988*/ AND `locale`='deDE') OR (`entry`=38990 /*38990*/ AND `locale`='deDE') OR (`entry`=38987 /*38987*/ AND `locale`='deDE') OR (`entry`=37956 /*37956*/ AND `locale`='deDE') OR (`entry`=37951 /*37951*/ AND `locale`='deDE') OR (`entry`=38037 /*38037*/ AND `locale`='deDE') OR (`entry`=63310 /*63310*/ AND `locale`='deDE') OR (`entry`=63309 /*63309*/ AND `locale`='deDE') OR (`entry`=38281 /*38281*/ AND `locale`='deDE') OR (`entry`=39062 /*39062*/ AND `locale`='deDE') OR (`entry`=38272 /*38272*/ AND `locale`='deDE') OR (`entry`=38268 /*38268*/ AND `locale`='deDE') OR (`entry`=38244 /*38244*/ AND `locale`='deDE') OR (`entry`=38242 /*38242*/ AND `locale`='deDE') OR (`entry`=38142 /*38142*/ AND `locale`='deDE') OR (`entry`=42619 /*42619*/ AND `locale`='deDE') OR (`entry`=42618 /*42618*/ AND `locale`='deDE') OR (`entry`=38038 /*38038*/ AND `locale`='deDE') OR (`entry`=38046 /*38046*/ AND `locale`='deDE') OR (`entry`=38141 /*38141*/ AND `locale`='deDE') OR (`entry`=39269 /*39269*/ AND `locale`='deDE') OR (`entry`=39267 /*39267*/ AND `locale`='deDE') OR (`entry`=37911 /*37911*/ AND `locale`='deDE') OR (`entry`=3129 /*3129*/ AND `locale`='deDE') OR (`entry`=38442 /*38442*/ AND `locale`='deDE') OR (`entry`=38326 /*38326*/ AND `locale`='deDE') OR (`entry`=38324 /*38324*/ AND `locale`='deDE') OR (`entry`=38440 /*38440*/ AND `locale`='deDE') OR (`entry`=38217 /*38217*/ AND `locale`='deDE') OR (`entry`=38437 /*38437*/ AND `locale`='deDE') OR (`entry`=38306 /*38306*/ AND `locale`='deDE') OR (`entry`=38423 /*38423*/ AND `locale`='deDE') OR (`entry`=38003 /*38003*/ AND `locale`='deDE') OR (`entry`=38560 /*38560*/ AND `locale`='deDE') OR (`entry`=38301 /*38301*/ AND `locale`='deDE') OR (`entry`=38300 /*38300*/ AND `locale`='deDE') OR (`entry`=3107 /*3107*/ AND `locale`='deDE') OR (`entry`=12350 /*12350*/ AND `locale`='deDE') OR (`entry`=10676 /*10676*/ AND `locale`='deDE') OR (`entry`=14543 /*14543*/ AND `locale`='deDE') OR (`entry`=12349 /*12349*/ AND `locale`='deDE') OR (`entry`=41142 /*41142*/ AND `locale`='deDE') OR (`entry`=40222 /*40222*/ AND `locale`='deDE') OR (`entry`=3933 /*3933*/ AND `locale`='deDE') OR (`entry`=3184 /*3184*/ AND `locale`='deDE') OR (`entry`=50004 /*50004*/ AND `locale`='deDE') OR (`entry`=50015 /*50015*/ AND `locale`='deDE') OR (`entry`=10369 /*10369*/ AND `locale`='deDE') OR (`entry`=3186 /*3186*/ AND `locale`='deDE') OR (`entry`=49998 /*49998*/ AND `locale`='deDE') OR (`entry`=62114 /*62114*/ AND `locale`='deDE') OR (`entry`=3194 /*3194*/ AND `locale`='deDE') OR (`entry`=14545 /*14545*/ AND `locale`='deDE') OR (`entry`=14544 /*14544*/ AND `locale`='deDE') OR (`entry`=12346 /*12346*/ AND `locale`='deDE') OR (`entry`=7953 /*7953*/ AND `locale`='deDE') OR (`entry`=7952 /*7952*/ AND `locale`='deDE') OR (`entry`=3304 /*3304*/ AND `locale`='deDE') OR (`entry`=3188 /*3188*/ AND `locale`='deDE') OR (`entry`=3140 /*3140*/ AND `locale`='deDE') OR (`entry`=3187 /*3187*/ AND `locale`='deDE') OR (`entry`=51913 /*51913*/ AND `locale`='deDE') OR (`entry`=50002 /*50002*/ AND `locale`='deDE') OR (`entry`=50001 /*50001*/ AND `locale`='deDE') OR (`entry`=6408 /*6408*/ AND `locale`='deDE') OR (`entry`=10578 /*10578*/ AND `locale`='deDE') OR (`entry`=3185 /*3185*/ AND `locale`='deDE') OR (`entry`=49997 /*49997*/ AND `locale`='deDE') OR (`entry`=50011 /*50011*/ AND `locale`='deDE') OR (`entry`=5942 /*5942*/ AND `locale`='deDE') OR (`entry`=5941 /*5941*/ AND `locale`='deDE') OR (`entry`=3297 /*3297*/ AND `locale`='deDE') OR (`entry`=39268 /*39268*/ AND `locale`='deDE') OR (`entry`=5824 /*5824*/ AND `locale`='deDE') OR (`entry`=3111 /*3111*/ AND `locale`='deDE') OR (`entry`=75687 /*75687*/ AND `locale`='deDE') OR (`entry`=3881 /*3881*/ AND `locale`='deDE') OR (`entry`=3191 /*3191*/ AND `locale`='deDE') OR (`entry`=3112 /*3112*/ AND `locale`='deDE') OR (`entry`=3336 /*3336*/ AND `locale`='deDE') OR (`entry`=5880 /*5880*/ AND `locale`='deDE') OR (`entry`=3171 /*3171*/ AND `locale`='deDE') OR (`entry`=5943 /*5943*/ AND `locale`='deDE') OR (`entry`=47418 /*47418*/ AND `locale`='deDE') OR (`entry`=3139 /*3139*/ AND `locale`='deDE') OR (`entry`=3168 /*3168*/ AND `locale`='deDE') OR (`entry`=3164 /*3164*/ AND `locale`='deDE') OR (`entry`=3170 /*3170*/ AND `locale`='deDE') OR (`entry`=3620 /*3620*/ AND `locale`='deDE') OR (`entry`=3142 /*3142*/ AND `locale`='deDE') OR (`entry`=3173 /*3173*/ AND `locale`='deDE') OR (`entry`=3169 /*3169*/ AND `locale`='deDE') OR (`entry`=12430 /*12430*/ AND `locale`='deDE') OR (`entry`=3706 /*3706*/ AND `locale`='deDE') OR (`entry`=75686 /*75686*/ AND `locale`='deDE') OR (`entry`=15572 /*15572*/ AND `locale`='deDE') OR (`entry`=39423 /*39423*/ AND `locale`='deDE') OR (`entry`=45015 /*45015*/ AND `locale`='deDE') OR (`entry`=75685 /*75685*/ AND `locale`='deDE') OR (`entry`=41140 /*41140*/ AND `locale`='deDE') OR (`entry`=3125 /*3125*/ AND `locale`='deDE') OR (`entry`=11378 /*11378*/ AND `locale`='deDE') OR (`entry`=3145 /*3145*/ AND `locale`='deDE') OR (`entry`=51787 /*51787*/ AND `locale`='deDE') OR (`entry`=39400 /*39400*/ AND `locale`='deDE') OR (`entry`=39399 /*39399*/ AND `locale`='deDE') OR (`entry`=39408 /*39408*/ AND `locale`='deDE') OR (`entry`=12776 /*12776*/ AND `locale`='deDE') OR (`entry`=9796 /*9796*/ AND `locale`='deDE') OR (`entry`=3882 /*3882*/ AND `locale`='deDE') OR (`entry`=3161 /*3161*/ AND `locale`='deDE') OR (`entry`=3160 /*3160*/ AND `locale`='deDE') OR (`entry`=3159 /*3159*/ AND `locale`='deDE') OR (`entry`=3158 /*3158*/ AND `locale`='deDE') OR (`entry`=3156 /*3156*/ AND `locale`='deDE') OR (`entry`=3281 /*3281*/ AND `locale`='deDE') OR (`entry`=3287 /*3287*/ AND `locale`='deDE') OR (`entry`=39326 /*39326*/ AND `locale`='deDE') OR (`entry`=3183 /*3183*/ AND `locale`='deDE') OR (`entry`=3102 /*3102*/ AND `locale`='deDE') OR (`entry`=3101 /*3101*/ AND `locale`='deDE') OR (`entry`=3098 /*3098*/ AND `locale`='deDE') OR (`entry`=10176 /*10176*/ AND `locale`='deDE') OR (`entry`=3143 /*3143*/ AND `locale`='deDE') OR (`entry`=44820 /*44820*/ AND `locale`='deDE') OR (`entry`=63296 /*63296*/ AND `locale`='deDE') OR (`entry`=39214 /*39214*/ AND `locale`='deDE') OR (`entry`=39206 /*39206*/ AND `locale`='deDE') OR (`entry`=5887 /*5887*/ AND `locale`='deDE') OR (`entry`=3157 /*3157*/ AND `locale`='deDE') OR (`entry`=3155 /*3155*/ AND `locale`='deDE') OR (`entry`=3153 /*3153*/ AND `locale`='deDE') OR (`entry`=39215 /*39215*/ AND `locale`='deDE') OR (`entry`=3124 /*3124*/ AND `locale`='deDE') OR (`entry`=5952 /*5952*/ AND `locale`='deDE') OR (`entry`=39224 /*39224*/ AND `locale`='deDE') OR (`entry`=49837 /*49837*/ AND `locale`='deDE') OR (`entry`=5890 /*5890*/ AND `locale`='deDE') OR (`entry`=39261 /*39261*/ AND `locale`='deDE') OR (`entry`=3099 /*3099*/ AND `locale`='deDE') OR (`entry`=62116 /*62116*/ AND `locale`='deDE') OR (`entry`=49743 /*49743*/ AND `locale`='deDE') OR (`entry`=39251 /*39251*/ AND `locale`='deDE') OR (`entry`=39249 /*39249*/ AND `locale`='deDE') OR (`entry`=39245 /*39245*/ AND `locale`='deDE') OR (`entry`=62115 /*62115*/ AND `locale`='deDE') OR (`entry`=39255 /*39255*/ AND `locale`='deDE') OR (`entry`=3106 /*3106*/ AND `locale`='deDE') OR (`entry`=39004 /*39004*/ AND `locale`='deDE') OR (`entry`=39260 /*39260*/ AND `locale`='deDE') OR (`entry`=39317 /*39317*/ AND `locale`='deDE') OR (`entry`=10556 /*10556*/ AND `locale`='deDE') OR (`entry`=5823 /*5823*/ AND `locale`='deDE') OR (`entry`=34431 /*34431*/ AND `locale`='deDE') OR (`entry`=34438 /*34438*/ AND `locale`='deDE') OR (`entry`=51795 /*51795*/ AND `locale`='deDE') OR (`entry`=17529 /*17529*/ AND `locale`='deDE') OR (`entry`=22060 /*22060*/ AND `locale`='deDE') OR (`entry`=17508 /*17508*/ AND `locale`='deDE') OR (`entry`=17606 /*17606*/ AND `locale`='deDE') OR (`entry`=17850 /*17850*/ AND `locale`='deDE') OR (`entry`=17496 /*17496*/ AND `locale`='deDE') OR (`entry`=17327 /*17327*/ AND `locale`='deDE') OR (`entry`=17325 /*17325*/ AND `locale`='deDE') OR (`entry`=17326 /*17326*/ AND `locale`='deDE') OR (`entry`=17528 /*17528*/ AND `locale`='deDE') OR (`entry`=17337 /*17337*/ AND `locale`='deDE') OR (`entry`=17339 /*17339*/ AND `locale`='deDE') OR (`entry`=17338 /*17338*/ AND `locale`='deDE') OR (`entry`=17664 /*17664*/ AND `locale`='deDE') OR (`entry`=17887 /*17887*/ AND `locale`='deDE') OR (`entry`=17886 /*17886*/ AND `locale`='deDE') OR (`entry`=17678 /*17678*/ AND `locale`='deDE') OR (`entry`=17979 /*17979*/ AND `locale`='deDE') OR (`entry`=17686 /*17686*/ AND `locale`='deDE') OR (`entry`=17683 /*17683*/ AND `locale`='deDE') OR (`entry`=17680 /*17680*/ AND `locale`='deDE') OR (`entry`=17610 /*17610*/ AND `locale`='deDE') OR (`entry`=17609 /*17609*/ AND `locale`='deDE') OR (`entry`=17607 /*17607*/ AND `locale`='deDE') OR (`entry`=17608 /*17608*/ AND `locale`='deDE') OR (`entry`=61828 /*61828*/ AND `locale`='deDE') OR (`entry`=17353 /*17353*/ AND `locale`='deDE') OR (`entry`=17346 /*17346*/ AND `locale`='deDE') OR (`entry`=17522 /*17522*/ AND `locale`='deDE') OR (`entry`=17523 /*17523*/ AND `locale`='deDE') OR (`entry`=17358 /*17358*/ AND `locale`='deDE') OR (`entry`=17927 /*17927*/ AND `locale`='deDE') OR (`entry`=17926 /*17926*/ AND `locale`='deDE') OR (`entry`=17986 /*17986*/ AND `locale`='deDE') OR (`entry`=17982 /*17982*/ AND `locale`='deDE') OR (`entry`=17527 /*17527*/ AND `locale`='deDE') OR (`entry`=17340 /*17340*/ AND `locale`='deDE') OR (`entry`=17342 /*17342*/ AND `locale`='deDE') OR (`entry`=17341 /*17341*/ AND `locale`='deDE') OR (`entry`=17494 /*17494*/ AND `locale`='deDE') OR (`entry`=17661 /*17661*/ AND `locale`='deDE') OR (`entry`=17421 /*17421*/ AND `locale`='deDE') OR (`entry`=17329 /*17329*/ AND `locale`='deDE') OR (`entry`=17713 /*17713*/ AND `locale`='deDE') OR (`entry`=17712 /*17712*/ AND `locale`='deDE') OR (`entry`=17592 /*17592*/ AND `locale`='deDE') OR (`entry`=17674 /*17674*/ AND `locale`='deDE') OR (`entry`=17588 /*17588*/ AND `locale`='deDE') OR (`entry`=17589 /*17589*/ AND `locale`='deDE') OR (`entry`=17328 /*17328*/ AND `locale`='deDE') OR (`entry`=17330 /*17330*/ AND `locale`='deDE') OR (`entry`=17550 /*17550*/ AND `locale`='deDE') OR (`entry`=17344 /*17344*/ AND `locale`='deDE') OR (`entry`=17348 /*17348*/ AND `locale`='deDE') OR (`entry`=17350 /*17350*/ AND `locale`='deDE') OR (`entry`=17604 /*17604*/ AND `locale`='deDE') OR (`entry`=17323 /*17323*/ AND `locale`='deDE') OR (`entry`=17324 /*17324*/ AND `locale`='deDE') OR (`entry`=17322 /*17322*/ AND `locale`='deDE') OR (`entry`=17704 /*17704*/ AND `locale`='deDE') OR (`entry`=17600 /*17600*/ AND `locale`='deDE') OR (`entry`=18020 /*18020*/ AND `locale`='deDE') OR (`entry`=18021 /*18021*/ AND `locale`='deDE') OR (`entry`=17554 /*17554*/ AND `locale`='deDE') OR (`entry`=17658 /*17658*/ AND `locale`='deDE') OR (`entry`=17423 /*17423*/ AND `locale`='deDE') OR (`entry`=17988 /*17988*/ AND `locale`='deDE') OR (`entry`=18804 /*18804*/ AND `locale`='deDE') OR (`entry`=49961 /*49961*/ AND `locale`='deDE') OR (`entry`=49959 /*49959*/ AND `locale`='deDE') OR (`entry`=17974 /*17974*/ AND `locale`='deDE') OR (`entry`=17424 /*17424*/ AND `locale`='deDE') OR (`entry`=18026 /*18026*/ AND `locale`='deDE') OR (`entry`=17824 /*17824*/ AND `locale`='deDE') OR (`entry`=17883 /*17883*/ AND `locale`='deDE') OR (`entry`=17825 /*17825*/ AND `locale`='deDE') OR (`entry`=17843 /*17843*/ AND `locale`='deDE') OR (`entry`=17844 /*17844*/ AND `locale`='deDE') OR (`entry`=17684 /*17684*/ AND `locale`='deDE') OR (`entry`=17663 /*17663*/ AND `locale`='deDE') OR (`entry`=17703 /*17703*/ AND `locale`='deDE') OR (`entry`=18803 /*18803*/ AND `locale`='deDE') OR (`entry`=18427 /*18427*/ AND `locale`='deDE') OR (`entry`=18030 /*18030*/ AND `locale`='deDE') OR (`entry`=17676 /*17676*/ AND `locale`='deDE') OR (`entry`=17667 /*17667*/ AND `locale`='deDE') OR (`entry`=49964 /*49964*/ AND `locale`='deDE') OR (`entry`=49962 /*49962*/ AND `locale`='deDE') OR (`entry`=49957 /*49957*/ AND `locale`='deDE') OR (`entry`=17434 /*17434*/ AND `locale`='deDE') OR (`entry`=17642 /*17642*/ AND `locale`='deDE') OR (`entry`=18811 /*18811*/ AND `locale`='deDE') OR (`entry`=18028 /*18028*/ AND `locale`='deDE') OR (`entry`=17433 /*17433*/ AND `locale`='deDE') OR (`entry`=18029 /*18029*/ AND `locale`='deDE') OR (`entry`=18025 /*18025*/ AND `locale`='deDE') OR (`entry`=17549 /*17549*/ AND `locale`='deDE') OR (`entry`=17553 /*17553*/ AND `locale`='deDE') OR (`entry`=18023 /*18023*/ AND `locale`='deDE') OR (`entry`=49966 /*49966*/ AND `locale`='deDE') OR (`entry`=17666 /*17666*/ AND `locale`='deDE') OR (`entry`=10780 /*10780*/ AND `locale`='deDE') OR (`entry`=17682 /*17682*/ AND `locale`='deDE') OR (`entry`=17701 /*17701*/ AND `locale`='deDE') OR (`entry`=17702 /*17702*/ AND `locale`='deDE') OR (`entry`=17542 /*17542*/ AND `locale`='deDE') OR (`entry`=17321 /*17321*/ AND `locale`='deDE') OR (`entry`=17320 /*17320*/ AND `locale`='deDE') OR (`entry`=17659 /*17659*/ AND `locale`='deDE') OR (`entry`=21145 /*21145*/ AND `locale`='deDE') OR (`entry`=17649 /*17649*/ AND `locale`='deDE') OR (`entry`=17586 /*17586*/ AND `locale`='deDE') OR (`entry`=17599 /*17599*/ AND `locale`='deDE') OR (`entry`=17352 /*17352*/ AND `locale`='deDE') OR (`entry`=18173 /*18173*/ AND `locale`='deDE') OR (`entry`=17673 /*17673*/ AND `locale`='deDE') OR (`entry`=62051 /*62051*/ AND `locale`='deDE') OR (`entry`=17347 /*17347*/ AND `locale`='deDE') OR (`entry`=17343 /*17343*/ AND `locale`='deDE') OR (`entry`=17334 /*17334*/ AND `locale`='deDE') OR (`entry`=17336 /*17336*/ AND `locale`='deDE') OR (`entry`=10779 /*10779*/ AND `locale`='deDE') OR (`entry`=17333 /*17333*/ AND `locale`='deDE') OR (`entry`=17331 /*17331*/ AND `locale`='deDE') OR (`entry`=61827 /*61827*/ AND `locale`='deDE') OR (`entry`=17349 /*17349*/ AND `locale`='deDE') OR (`entry`=17345 /*17345*/ AND `locale`='deDE') OR (`entry`=17525 /*17525*/ AND `locale`='deDE') OR (`entry`=39020 /*39020*/ AND `locale`='deDE') OR (`entry`=46128 /*46128*/ AND `locale`='deDE') OR (`entry`=46126 /*46126*/ AND `locale`='deDE') OR (`entry`=46127 /*46127*/ AND `locale`='deDE') OR (`entry`=46129 /*46129*/ AND `locale`='deDE') OR (`entry`=55211 /*55211*/ AND `locale`='deDE') OR (`entry`=45875 /*45875*/ AND `locale`='deDE') OR (`entry`=45874 /*45874*/ AND `locale`='deDE') OR (`entry`=46196 /*46196*/ AND `locale`='deDE') OR (`entry`=46197 /*46197*/ AND `locale`='deDE') OR (`entry`=46195 /*46195*/ AND `locale`='deDE') OR (`entry`=46198 /*46198*/ AND `locale`='deDE') OR (`entry`=45204 /*45204*/ AND `locale`='deDE') OR (`entry`=45205 /*45205*/ AND `locale`='deDE') OR (`entry`=45202 /*45202*/ AND `locale`='deDE') OR (`entry`=45190 /*45190*/ AND `locale`='deDE') OR (`entry`=49228 /*49228*/ AND `locale`='deDE') OR (`entry`=49224 /*49224*/ AND `locale`='deDE') OR (`entry`=48761 /*48761*/ AND `locale`='deDE') OR (`entry`=48701 /*48701*/ AND `locale`='deDE') OR (`entry`=46335 /*46335*/ AND `locale`='deDE') OR (`entry`=46883 /*46883*/ AND `locale`='deDE') OR (`entry`=46336 /*46336*/ AND `locale`='deDE') OR (`entry`=48697 /*48697*/ AND `locale`='deDE') OR (`entry`=48501 /*48501*/ AND `locale`='deDE') OR (`entry`=44417 /*44417*/ AND `locale`='deDE') OR (`entry`=40560 /*40560*/ AND `locale`='deDE') OR (`entry`=62953 /*62953*/ AND `locale`='deDE') OR (`entry`=44598 /*44598*/ AND `locale`='deDE') OR (`entry`=40527 /*40527*/ AND `locale`='deDE') OR (`entry`=5431 /*5431*/ AND `locale`='deDE') OR (`entry`=12124 /*12124*/ AND `locale`='deDE') OR (`entry`=48626 /*48626*/ AND `locale`='deDE') OR (`entry`=48627 /*48627*/ AND `locale`='deDE') OR (`entry`=48625 /*48625*/ AND `locale`='deDE') OR (`entry`=49214 /*49214*/ AND `locale`='deDE') OR (`entry`=46362 /*46362*/ AND `locale`='deDE') OR (`entry`=46750 /*46750*/ AND `locale`='deDE') OR (`entry`=46361 /*46361*/ AND `locale`='deDE') OR (`entry`=46715 /*46715*/ AND `locale`='deDE') OR (`entry`=47014 /*47014*/ AND `locale`='deDE') OR (`entry`=46888 /*46888*/ AND `locale`='deDE') OR (`entry`=46617 /*46617*/ AND `locale`='deDE') OR (`entry`=46592 /*46592*/ AND `locale`='deDE') OR (`entry`=46590 /*46590*/ AND `locale`='deDE') OR (`entry`=46587 /*46587*/ AND `locale`='deDE') OR (`entry`=51760 /*51760*/ AND `locale`='deDE') OR (`entry`=46462 /*46462*/ AND `locale`='deDE') OR (`entry`=46425 /*46425*/ AND `locale`='deDE') OR (`entry`=46311 /*46311*/ AND `locale`='deDE') OR (`entry`=48263 /*48263*/ AND `locale`='deDE') OR (`entry`=47306 /*47306*/ AND `locale`='deDE') OR (`entry`=50748 /*50748*/ AND `locale`='deDE') OR (`entry`=41135 /*41135*/ AND `locale`='deDE') OR (`entry`=41131 /*41131*/ AND `locale`='deDE') OR (`entry`=7394 /*7394*/ AND `locale`='deDE') OR (`entry`=46206 /*46206*/ AND `locale`='deDE') OR (`entry`=40958 /*40958*/ AND `locale`='deDE') OR (`entry`=40888 /*40888*/ AND `locale`='deDE') OR (`entry`=40959 /*40959*/ AND `locale`='deDE') OR (`entry`=40885 /*40885*/ AND `locale`='deDE') OR (`entry`=2110 /*2110*/ AND `locale`='deDE') OR (`entry`=41060 /*41060*/ AND `locale`='deDE') OR (`entry`=48128 /*48128*/ AND `locale`='deDE') OR (`entry`=44722 /*44722*/ AND `locale`='deDE') OR (`entry`=15520 /*15520*/ AND `locale`='deDE') OR (`entry`=38571 /*38571*/ AND `locale`='deDE') OR (`entry`=40583 /*40583*/ AND `locale`='deDE') OR (`entry`=21448 /*21448*/ AND `locale`='deDE') OR (`entry`=40604 /*40604*/ AND `locale`='deDE') OR (`entry`=7733 /*7733*/ AND `locale`='deDE') OR (`entry`=9985 /*9985*/ AND `locale`='deDE') OR (`entry`=14743 /*14743*/ AND `locale`='deDE') OR (`entry`=40508 /*40508*/ AND `locale`='deDE') OR (`entry`=39178 /*39178*/ AND `locale`='deDE') OR (`entry`=16014 /*16014*/ AND `locale`='deDE') OR (`entry`=8126 /*8126*/ AND `locale`='deDE') OR (`entry`=43964 /*43964*/ AND `locale`='deDE') OR (`entry`=40589 /*40589*/ AND `locale`='deDE') OR (`entry`=40588 /*40588*/ AND `locale`='deDE') OR (`entry`=39159 /*39159*/ AND `locale`='deDE') OR (`entry`=8124 /*8124*/ AND `locale`='deDE') OR (`entry`=7799 /*7799*/ AND `locale`='deDE') OR (`entry`=7824 /*7824*/ AND `locale`='deDE') OR (`entry`=39034 /*39034*/ AND `locale`='deDE') OR (`entry`=69323 /*69323*/ AND `locale`='deDE') OR (`entry`=69322 /*69322*/ AND `locale`='deDE') OR (`entry`=40216 /*40216*/ AND `locale`='deDE') OR (`entry`=20278 /*20278*/ AND `locale`='deDE') OR (`entry`=19860 /*19860*/ AND `locale`='deDE') OR (`entry`=5594 /*5594*/ AND `locale`='deDE') OR (`entry`=8883 /*8883*/ AND `locale`='deDE') OR (`entry`=4708 /*4708*/ AND `locale`='deDE') OR (`entry`=28126 /*28126*/ AND `locale`='deDE') OR (`entry`=8882 /*8882*/ AND `locale`='deDE') OR (`entry`=16417 /*16417*/ AND `locale`='deDE') OR (`entry`=8884 /*8884*/ AND `locale`='deDE') OR (`entry`=40581 /*40581*/ AND `locale`='deDE') OR (`entry`=44398 /*44398*/ AND `locale`='deDE') OR (`entry`=40582 /*40582*/ AND `locale`='deDE') OR (`entry`=40580 /*40580*/ AND `locale`='deDE') OR (`entry`=38927 /*38927*/ AND `locale`='deDE') OR (`entry`=7804 /*7804*/ AND `locale`='deDE') OR (`entry`=7270 /*7270*/ AND `locale`='deDE') OR (`entry`=5649 /*5649*/ AND `locale`='deDE') OR (`entry`=5645 /*5645*/ AND `locale`='deDE') OR (`entry`=5647 /*5647*/ AND `locale`='deDE') OR (`entry`=38909 /*38909*/ AND `locale`='deDE') OR (`entry`=5646 /*5646*/ AND `locale`='deDE') OR (`entry`=38502 /*38502*/ AND `locale`='deDE') OR (`entry`=5459 /*5459*/ AND `locale`='deDE') OR (`entry`=40717 /*40717*/ AND `locale`='deDE') OR (`entry`=40666 /*40666*/ AND `locale`='deDE') OR (`entry`=40656 /*40656*/ AND `locale`='deDE') OR (`entry`=39186 /*39186*/ AND `locale`='deDE') OR (`entry`=40572 /*40572*/ AND `locale`='deDE') OR (`entry`=8131 /*8131*/ AND `locale`='deDE') OR (`entry`=8129 /*8129*/ AND `locale`='deDE') OR (`entry`=6568 /*6568*/ AND `locale`='deDE') OR (`entry`=15586 /*15586*/ AND `locale`='deDE') OR (`entry`=11756 /*11756*/ AND `locale`='deDE') OR (`entry`=8128 /*8128*/ AND `locale`='deDE') OR (`entry`=7823 /*7823*/ AND `locale`='deDE') OR (`entry`=8736 /*8736*/ AND `locale`='deDE') OR (`entry`=8661 /*8661*/ AND `locale`='deDE') OR (`entry`=43418 /*43418*/ AND `locale`='deDE') OR (`entry`=40806 /*40806*/ AND `locale`='deDE') OR (`entry`=38535 /*38535*/ AND `locale`='deDE') OR (`entry`=38534 /*38534*/ AND `locale`='deDE') OR (`entry`=38532 /*38532*/ AND `locale`='deDE') OR (`entry`=14567 /*14567*/ AND `locale`='deDE') OR (`entry`=9460 /*9460*/ AND `locale`='deDE') OR (`entry`=5411 /*5411*/ AND `locale`='deDE') OR (`entry`=5426 /*5426*/ AND `locale`='deDE') OR (`entry`=5458 /*5458*/ AND `locale`='deDE') OR (`entry`=5455 /*5455*/ AND `locale`='deDE') OR (`entry`=5460 /*5460*/ AND `locale`='deDE') OR (`entry`=48185 /*48185*/ AND `locale`='deDE') OR (`entry`=40648 /*40648*/ AND `locale`='deDE') OR (`entry`=41730 /*41730*/ AND `locale`='deDE') OR (`entry`=47044 /*47044*/ AND `locale`='deDE') OR (`entry`=47043 /*47043*/ AND `locale`='deDE') OR (`entry`=47042 /*47042*/ AND `locale`='deDE') OR (`entry`=62257 /*62257*/ AND `locale`='deDE') OR (`entry`=5473 /*5473*/ AND `locale`='deDE') OR (`entry`=38880 /*38880*/ AND `locale`='deDE') OR (`entry`=5475 /*5475*/ AND `locale`='deDE') OR (`entry`=5471 /*5471*/ AND `locale`='deDE') OR (`entry`=5474 /*5474*/ AND `locale`='deDE') OR (`entry`=5472 /*5472*/ AND `locale`='deDE') OR (`entry`=38856 /*38856*/ AND `locale`='deDE') OR (`entry`=38849 /*38849*/ AND `locale`='deDE') OR (`entry`=38847 /*38847*/ AND `locale`='deDE') OR (`entry`=38916 /*38916*/ AND `locale`='deDE') OR (`entry`=15573 /*15573*/ AND `locale`='deDE') OR (`entry`=40754 /*40754*/ AND `locale`='deDE') OR (`entry`=44374 /*44374*/ AND `locale`='deDE') OR (`entry`=41215 /*41215*/ AND `locale`='deDE') OR (`entry`=40747 /*40747*/ AND `locale`='deDE') OR (`entry`=43980 /*43980*/ AND `locale`='deDE') OR (`entry`=43972 /*43972*/ AND `locale`='deDE') OR (`entry`=38922 /*38922*/ AND `locale`='deDE') OR (`entry`=38914 /*38914*/ AND `locale`='deDE') OR (`entry`=39185 /*39185*/ AND `locale`='deDE') OR (`entry`=5430 /*5430*/ AND `locale`='deDE') OR (`entry`=40662 /*40662*/ AND `locale`='deDE') OR (`entry`=5419 /*5419*/ AND `locale`='deDE') OR (`entry`=40827 /*40827*/ AND `locale`='deDE') OR (`entry`=40815 /*40815*/ AND `locale`='deDE') OR (`entry`=39191 /*39191*/ AND `locale`='deDE') OR (`entry`=38578 /*38578*/ AND `locale`='deDE') OR (`entry`=40826 /*40826*/ AND `locale`='deDE') OR (`entry`=40109 /*40109*/ AND `locale`='deDE') OR (`entry`=38997 /*38997*/ AND `locale`='deDE') OR (`entry`=38998 /*38998*/ AND `locale`='deDE') OR (`entry`=44694 /*44694*/ AND `locale`='deDE') OR (`entry`=44869 /*44869*/ AND `locale`='deDE') OR (`entry`=44546 /*44546*/ AND `locale`='deDE') OR (`entry`=44750 /*44750*/ AND `locale`='deDE') OR (`entry`=44613 /*44613*/ AND `locale`='deDE') OR (`entry`=38714 /*38714*/ AND `locale`='deDE') OR (`entry`=20025 /*20025*/ AND `locale`='deDE') OR (`entry`=38742 /*38742*/ AND `locale`='deDE') OR (`entry`=38706 /*38706*/ AND `locale`='deDE') OR (`entry`=41214 /*41214*/ AND `locale`='deDE') OR (`entry`=19959 /*19959*/ AND `locale`='deDE') OR (`entry`=19933 /*19933*/ AND `locale`='deDE') OR (`entry`=19932 /*19932*/ AND `locale`='deDE') OR (`entry`=20131 /*20131*/ AND `locale`='deDE') OR (`entry`=20130 /*20130*/ AND `locale`='deDE') OR (`entry`=19936 /*19936*/ AND `locale`='deDE') OR (`entry`=19935 /*19935*/ AND `locale`='deDE') OR (`entry`=18542 /*18542*/ AND `locale`='deDE') OR (`entry`=20142 /*20142*/ AND `locale`='deDE') OR (`entry`=8198 /*8198*/ AND `locale`='deDE') OR (`entry`=80675 /*80675*/ AND `locale`='deDE') OR (`entry`=8197 /*8197*/ AND `locale`='deDE') OR (`entry`=22872 /*22872*/ AND `locale`='deDE') OR (`entry`=44587 /*44587*/ AND `locale`='deDE') OR (`entry`=21643 /*21643*/ AND `locale`='deDE') OR (`entry`=20082 /*20082*/ AND `locale`='deDE') OR (`entry`=20081 /*20081*/ AND `locale`='deDE') OR (`entry`=20080 /*20080*/ AND `locale`='deDE') OR (`entry`=68820 /*68820*/ AND `locale`='deDE') OR (`entry`=40657 /*40657*/ AND `locale`='deDE') OR (`entry`=38750 /*38750*/ AND `locale`='deDE') OR (`entry`=44759 /*44759*/ AND `locale`='deDE') OR (`entry`=38749 /*38749*/ AND `locale`='deDE') OR (`entry`=44763 /*44763*/ AND `locale`='deDE') OR (`entry`=44762 /*44762*/ AND `locale`='deDE') OR (`entry`=38660 /*38660*/ AND `locale`='deDE') OR (`entry`=40635 /*40635*/ AND `locale`='deDE') OR (`entry`=40632 /*40632*/ AND `locale`='deDE') OR (`entry`=38662 /*38662*/ AND `locale`='deDE') OR (`entry`=38650 /*38650*/ AND `locale`='deDE') OR (`entry`=44861 /*44861*/ AND `locale`='deDE') OR (`entry`=40593 /*40593*/ AND `locale`='deDE') OR (`entry`=40636 /*40636*/ AND `locale`='deDE') OR (`entry`=38649 /*38649*/ AND `locale`='deDE') OR (`entry`=38665 /*38665*/ AND `locale`='deDE') OR (`entry`=7855 /*7855*/ AND `locale`='deDE') OR (`entry`=7858 /*7858*/ AND `locale`='deDE') OR (`entry`=38646 /*38646*/ AND `locale`='deDE') OR (`entry`=38648 /*38648*/ AND `locale`='deDE') OR (`entry`=38823 /*38823*/ AND `locale`='deDE') OR (`entry`=38824 /*38824*/ AND `locale`='deDE') OR (`entry`=38719 /*38719*/ AND `locale`='deDE') OR (`entry`=14123 /*14123*/ AND `locale`='deDE') OR (`entry`=40764 /*40764*/ AND `locale`='deDE') OR (`entry`=39022 /*39022*/ AND `locale`='deDE') OR (`entry`=8667 /*8667*/ AND `locale`='deDE') OR (`entry`=20069 /*20069*/ AND `locale`='deDE') OR (`entry`=5465 /*5465*/ AND `locale`='deDE') OR (`entry`=15192 /*15192*/ AND `locale`='deDE') OR (`entry`=20053 /*20053*/ AND `locale`='deDE') OR (`entry`=20055 /*20055*/ AND `locale`='deDE') OR (`entry`=20054 /*20054*/ AND `locale`='deDE') OR (`entry`=44573 /*44573*/ AND `locale`='deDE') OR (`entry`=40528 /*40528*/ AND `locale`='deDE') OR (`entry`=11811 /*11811*/ AND `locale`='deDE') OR (`entry`=38994 /*38994*/ AND `locale`='deDE') OR (`entry`=38993 /*38993*/ AND `locale`='deDE') OR (`entry`=44873 /*44873*/ AND `locale`='deDE') OR (`entry`=49836 /*49836*/ AND `locale`='deDE') OR (`entry`=44611 /*44611*/ AND `locale`='deDE') OR (`entry`=44612 /*44612*/ AND `locale`='deDE') OR (`entry`=9397 /*9397*/ AND `locale`='deDE') OR (`entry`=62256 /*62256*/ AND `locale`='deDE') OR (`entry`=44594 /*44594*/ AND `locale`='deDE') OR (`entry`=44714 /*44714*/ AND `locale`='deDE') OR (`entry`=7770 /*7770*/ AND `locale`='deDE') OR (`entry`=44568 /*44568*/ AND `locale`='deDE') OR (`entry`=19934 /*19934*/ AND `locale`='deDE') OR (`entry`=29867 /*29867*/ AND `locale`='deDE') OR (`entry`=29865 /*29865*/ AND `locale`='deDE') OR (`entry`=44569 /*44569*/ AND `locale`='deDE') OR (`entry`=29873 /*29873*/ AND `locale`='deDE') OR (`entry`=29868 /*29868*/ AND `locale`='deDE') OR (`entry`=29866 /*29866*/ AND `locale`='deDE') OR (`entry`=19951 /*19951*/ AND `locale`='deDE') OR (`entry`=19918 /*19918*/ AND `locale`='deDE') OR (`entry`=5427 /*5427*/ AND `locale`='deDE') OR (`entry`=20027 /*20027*/ AND `locale`='deDE') OR (`entry`=5420 /*5420*/ AND `locale`='deDE') OR (`entry`=20026 /*20026*/ AND `locale`='deDE') OR (`entry`=19950 /*19950*/ AND `locale`='deDE') OR (`entry`=7784 /*7784*/ AND `locale`='deDE') OR (`entry`=5450 /*5450*/ AND `locale`='deDE') OR (`entry`=39081 /*39081*/ AND `locale`='deDE') OR (`entry`=38992 /*38992*/ AND `locale`='deDE') OR (`entry`=8205 /*8205*/ AND `locale`='deDE') OR (`entry`=5454 /*5454*/ AND `locale`='deDE') OR (`entry`=62258 /*62258*/ AND `locale`='deDE') OR (`entry`=5451 /*5451*/ AND `locale`='deDE') OR (`entry`=5452 /*5452*/ AND `locale`='deDE') OR (`entry`=44595 /*44595*/ AND `locale`='deDE') OR (`entry`=39061 /*39061*/ AND `locale`='deDE') OR (`entry`=44710 /*44710*/ AND `locale`='deDE') OR (`entry`=47583 /*47583*/ AND `locale`='deDE') OR (`entry`=61169 /*61169*/ AND `locale`='deDE') OR (`entry`=4076 /*4076*/ AND `locale`='deDE') OR (`entry`=48130 /*48130*/ AND `locale`='deDE') OR (`entry`=9999 /*9999*/ AND `locale`='deDE') OR (`entry`=9376 /*9376*/ AND `locale`='deDE') OR (`entry`=10541 /*10541*/ AND `locale`='deDE') OR (`entry`=28092 /*28092*/ AND `locale`='deDE') OR (`entry`=9118 /*9118*/ AND `locale`='deDE') OR (`entry`=10583 /*10583*/ AND `locale`='deDE') OR (`entry`=38269 /*38269*/ AND `locale`='deDE') OR (`entry`=38488 /*38488*/ AND `locale`='deDE') OR (`entry`=10302 /*10302*/ AND `locale`='deDE') OR (`entry`=9119 /*9119*/ AND `locale`='deDE') OR (`entry`=38270 /*38270*/ AND `locale`='deDE') OR (`entry`=12959 /*12959*/ AND `locale`='deDE') OR (`entry`=10977 /*10977*/ AND `locale`='deDE') OR (`entry`=9997 /*9997*/ AND `locale`='deDE') OR (`entry`=9271 /*9271*/ AND `locale`='deDE') OR (`entry`=9270 /*9270*/ AND `locale`='deDE') OR (`entry`=3000 /*3000*/ AND `locale`='deDE') OR (`entry`=38354 /*38354*/ AND `locale`='deDE') OR (`entry`=9117 /*9117*/ AND `locale`='deDE') OR (`entry`=61328 /*61328*/ AND `locale`='deDE') OR (`entry`=6520 /*6520*/ AND `locale`='deDE') OR (`entry`=6521 /*6521*/ AND `locale`='deDE') OR (`entry`=62364 /*62364*/ AND `locale`='deDE') OR (`entry`=50478 /*50478*/ AND `locale`='deDE') OR (`entry`=9699 /*9699*/ AND `locale`='deDE') OR (`entry`=6557 /*6557*/ AND `locale`='deDE') OR (`entry`=38276 /*38276*/ AND `locale`='deDE') OR (`entry`=115923 /*115923*/ AND `locale`='deDE') OR (`entry`=38561 /*38561*/ AND `locale`='deDE') OR (`entry`=38277 /*38277*/ AND `locale`='deDE') OR (`entry`=38275 /*38275*/ AND `locale`='deDE') OR (`entry`=9998 /*9998*/ AND `locale`='deDE') OR (`entry`=9618 /*9618*/ AND `locale`='deDE') OR (`entry`=39175 /*39175*/ AND `locale`='deDE') OR (`entry`=9683 /*9683*/ AND `locale`='deDE') OR (`entry`=6498 /*6498*/ AND `locale`='deDE') OR (`entry`=38307 /*38307*/ AND `locale`='deDE') OR (`entry`=15583 /*15583*/ AND `locale`='deDE') OR (`entry`=9163 /*9163*/ AND `locale`='deDE') OR (`entry`=9166 /*9166*/ AND `locale`='deDE') OR (`entry`=38373 /*38373*/ AND `locale`='deDE') OR (`entry`=38237 /*38237*/ AND `locale`='deDE') OR (`entry`=9272 /*9272*/ AND `locale`='deDE') OR (`entry`=61317 /*61317*/ AND `locale`='deDE') OR (`entry`=61384 /*61384*/ AND `locale`='deDE') OR (`entry`=6519 /*6519*/ AND `locale`='deDE') OR (`entry`=6518 /*6518*/ AND `locale`='deDE') OR (`entry`=9622 /*9622*/ AND `locale`='deDE') OR (`entry`=6516 /*6516*/ AND `locale`='deDE') OR (`entry`=48972 /*48972*/ AND `locale`='deDE') OR (`entry`=6514 /*6514*/ AND `locale`='deDE') OR (`entry`=9623 /*9623*/ AND `locale`='deDE') OR (`entry`=6513 /*6513*/ AND `locale`='deDE') OR (`entry`=6527 /*6527*/ AND `locale`='deDE') OR (`entry`=6517 /*6517*/ AND `locale`='deDE') OR (`entry`=98222 /*98222*/ AND `locale`='deDE') OR (`entry`=6500 /*6500*/ AND `locale`='deDE') OR (`entry`=38263 /*38263*/ AND `locale`='deDE') OR (`entry`=38205 /*38205*/ AND `locale`='deDE') OR (`entry`=38214 /*38214*/ AND `locale`='deDE') OR (`entry`=6507 /*6507*/ AND `locale`='deDE') OR (`entry`=6508 /*6508*/ AND `locale`='deDE') OR (`entry`=9162 /*9162*/ AND `locale`='deDE') OR (`entry`=34158 /*34158*/ AND `locale`='deDE') OR (`entry`=11701 /*11701*/ AND `locale`='deDE') OR (`entry`=38274 /*38274*/ AND `locale`='deDE') OR (`entry`=38203 /*38203*/ AND `locale`='deDE') OR (`entry`=9619 /*9619*/ AND `locale`='deDE') OR (`entry`=62370 /*62370*/ AND `locale`='deDE') OR (`entry`=45439 /*45439*/ AND `locale`='deDE') OR (`entry`=6506 /*6506*/ AND `locale`='deDE') OR (`entry`=6505 /*6505*/ AND `locale`='deDE') OR (`entry`=6554 /*6554*/ AND `locale`='deDE') OR (`entry`=62373 /*62373*/ AND `locale`='deDE') OR (`entry`=38305 /*38305*/ AND `locale`='deDE') OR (`entry`=6552 /*6552*/ AND `locale`='deDE') OR (`entry`=6555 /*6555*/ AND `locale`='deDE') OR (`entry`=6553 /*6553*/ AND `locale`='deDE') OR (`entry`=6551 /*6551*/ AND `locale`='deDE') OR (`entry`=38329 /*38329*/ AND `locale`='deDE') OR (`entry`=38346 /*38346*/ AND `locale`='deDE') OR (`entry`=6503 /*6503*/ AND `locale`='deDE') OR (`entry`=6509 /*6509*/ AND `locale`='deDE') OR (`entry`=6504 /*6504*/ AND `locale`='deDE') OR (`entry`=62375 /*62375*/ AND `locale`='deDE') OR (`entry`=6502 /*6502*/ AND `locale`='deDE') OR (`entry`=6501 /*6501*/ AND `locale`='deDE') OR (`entry`=6511 /*6511*/ AND `locale`='deDE') OR (`entry`=38254 /*38254*/ AND `locale`='deDE') OR (`entry`=9164 /*9164*/ AND `locale`='deDE') OR (`entry`=6559 /*6559*/ AND `locale`='deDE') OR (`entry`=6510 /*6510*/ AND `locale`='deDE') OR (`entry`=61318 /*61318*/ AND `locale`='deDE') OR (`entry`=6512 /*6512*/ AND `locale`='deDE') OR (`entry`=9167 /*9167*/ AND `locale`='deDE') OR (`entry`=61313 /*61313*/ AND `locale`='deDE') OR (`entry`=49734 /*49734*/ AND `locale`='deDE') OR (`entry`=9600 /*9600*/ AND `locale`='deDE') OR (`entry`=6560 /*6560*/ AND `locale`='deDE') OR (`entry`=49722 /*49722*/ AND `locale`='deDE') OR (`entry`=48935 /*48935*/ AND `locale`='deDE') OR (`entry`=7554 /*7554*/ AND `locale`='deDE') OR (`entry`=7428 /*7428*/ AND `locale`='deDE') OR (`entry`=7446 /*7446*/ AND `locale`='deDE') OR (`entry`=41861 /*41861*/ AND `locale`='deDE') OR (`entry`=106250 /*106250*/ AND `locale`='deDE') OR (`entry`=43410 /*43410*/ AND `locale`='deDE') OR (`entry`=40833 /*40833*/ AND `locale`='deDE') OR (`entry`=40254 /*40254*/ AND `locale`='deDE') OR (`entry`=55227 /*55227*/ AND `locale`='deDE') OR (`entry`=40278 /*40278*/ AND `locale`='deDE') OR (`entry`=39925 /*39925*/ AND `locale`='deDE') OR (`entry`=50314 /*50314*/ AND `locale`='deDE') OR (`entry`=43411 /*43411*/ AND `locale`='deDE') OR (`entry`=43408 /*43408*/ AND `locale`='deDE') OR (`entry`=40289 /*40289*/ AND `locale`='deDE') OR (`entry`=71304 /*71304*/ AND `locale`='deDE') OR (`entry`=40843 /*40843*/ AND `locale`='deDE') OR (`entry`=43427 /*43427*/ AND `locale`='deDE') OR (`entry`=66819 /*66819*/ AND `locale`='deDE') OR (`entry`=66808 /*66808*/ AND `locale`='deDE') OR (`entry`=66807 /*66807*/ AND `locale`='deDE') OR (`entry`=66806 /*66806*/ AND `locale`='deDE') OR (`entry`=106303 /*106303*/ AND `locale`='deDE') OR (`entry`=47264 /*47264*/ AND `locale`='deDE') OR (`entry`=47263 /*47263*/ AND `locale`='deDE') OR (`entry`=59113 /*59113*/ AND `locale`='deDE') OR (`entry`=40229 /*40229*/ AND `locale`='deDE') OR (`entry`=39921 /*39921*/ AND `locale`='deDE') OR (`entry`=62888 /*62888*/ AND `locale`='deDE') OR (`entry`=40140 /*40140*/ AND `locale`='deDE') OR (`entry`=40139 /*40139*/ AND `locale`='deDE') OR (`entry`=40882 /*40882*/ AND `locale`='deDE') OR (`entry`=62884 /*62884*/ AND `locale`='deDE') OR (`entry`=20725 /*20725*/ AND `locale`='deDE') OR (`entry`=38951 /*38951*/ AND `locale`='deDE') OR (`entry`=38934 /*38934*/ AND `locale`='deDE') OR (`entry`=39919 /*39919*/ AND `locale`='deDE') OR (`entry`=36286 /*36286*/ AND `locale`='deDE') OR (`entry`=39869 /*39869*/ AND `locale`='deDE') OR (`entry`=39857 /*39857*/ AND `locale`='deDE') OR (`entry`=49445 /*49445*/ AND `locale`='deDE') OR (`entry`=49444 /*49444*/ AND `locale`='deDE') OR (`entry`=40149 /*40149*/ AND `locale`='deDE') OR (`entry`=40147 /*40147*/ AND `locale`='deDE') OR (`entry`=38902 /*38902*/ AND `locale`='deDE') OR (`entry`=40150 /*40150*/ AND `locale`='deDE') OR (`entry`=40148 /*40148*/ AND `locale`='deDE') OR (`entry`=38952 /*38952*/ AND `locale`='deDE') OR (`entry`=38896 /*38896*/ AND `locale`='deDE') OR (`entry`=40096 /*40096*/ AND `locale`='deDE') OR (`entry`=40123 /*40123*/ AND `locale`='deDE') OR (`entry`=62118 /*62118*/ AND `locale`='deDE') OR (`entry`=38926 /*38926*/ AND `locale`='deDE') OR (`entry`=39429 /*39429*/ AND `locale`='deDE') OR (`entry`=39427 /*39427*/ AND `locale`='deDE') OR (`entry`=38913 /*38913*/ AND `locale`='deDE') OR (`entry`=39437 /*39437*/ AND `locale`='deDE') OR (`entry`=41308 /*41308*/ AND `locale`='deDE') OR (`entry`=50080 /*50080*/ AND `locale`='deDE') OR (`entry`=50070 /*50070*/ AND `locale`='deDE') OR (`entry`=42660 /*42660*/ AND `locale`='deDE') OR (`entry`=42657 /*42657*/ AND `locale`='deDE') OR (`entry`=50082 /*50082*/ AND `locale`='deDE') OR (`entry`=54313 /*54313*/ AND `locale`='deDE') OR (`entry`=50079 /*50079*/ AND `locale`='deDE') OR (`entry`=50081 /*50081*/ AND `locale`='deDE') OR (`entry`=50068 /*50068*/ AND `locale`='deDE') OR (`entry`=50071 /*50071*/ AND `locale`='deDE') OR (`entry`=41006 /*41006*/ AND `locale`='deDE') OR (`entry`=50069 /*50069*/ AND `locale`='deDE') OR (`entry`=50084 /*50084*/ AND `locale`='deDE') OR (`entry`=50083 /*50083*/ AND `locale`='deDE') OR (`entry`=42659 /*42659*/ AND `locale`='deDE') OR (`entry`=42663 /*42663*/ AND `locale`='deDE') OR (`entry`=42658 /*42658*/ AND `locale`='deDE') OR (`entry`=42664 /*42664*/ AND `locale`='deDE') OR (`entry`=40845 /*40845*/ AND `locale`='deDE') OR (`entry`=40844 /*40844*/ AND `locale`='deDE') OR (`entry`=40834 /*40834*/ AND `locale`='deDE') OR (`entry`=40837 /*40837*/ AND `locale`='deDE') OR (`entry`=39846 /*39846*/ AND `locale`='deDE') OR (`entry`=40841 /*40841*/ AND `locale`='deDE') OR (`entry`=40838 /*40838*/ AND `locale`='deDE') OR (`entry`=39600 /*39600*/ AND `locale`='deDE') OR (`entry`=39438 /*39438*/ AND `locale`='deDE') OR (`entry`=39431 /*39431*/ AND `locale`='deDE') OR (`entry`=50419 /*50419*/ AND `locale`='deDE') OR (`entry`=40868 /*40868*/ AND `locale`='deDE') OR (`entry`=39737 /*39737*/ AND `locale`='deDE') OR (`entry`=39736 /*39736*/ AND `locale`='deDE') OR (`entry`=39730 /*39730*/ AND `locale`='deDE') OR (`entry`=39738 /*39738*/ AND `locale`='deDE') OR (`entry`=39789 /*39789*/ AND `locale`='deDE') OR (`entry`=39640 /*39640*/ AND `locale`='deDE') OR (`entry`=39646 /*39646*/ AND `locale`='deDE') OR (`entry`=62887 /*62887*/ AND `locale`='deDE') OR (`entry`=39643 /*39643*/ AND `locale`='deDE') OR (`entry`=39436 /*39436*/ AND `locale`='deDE') OR (`entry`=39642 /*39642*/ AND `locale`='deDE') OR (`entry`=43548 /*43548*/ AND `locale`='deDE') OR (`entry`=39432 /*39432*/ AND `locale`='deDE') OR (`entry`=39435 /*39435*/ AND `locale`='deDE') OR (`entry`=39433 /*39433*/ AND `locale`='deDE') OR (`entry`=39434 /*39434*/ AND `locale`='deDE') OR (`entry`=39644 /*39644*/ AND `locale`='deDE') OR (`entry`=43547 /*43547*/ AND `locale`='deDE') OR (`entry`=39637 /*39637*/ AND `locale`='deDE') OR (`entry`=39588 /*39588*/ AND `locale`='deDE') OR (`entry`=39859 /*39859*/ AND `locale`='deDE') OR (`entry`=39844 /*39844*/ AND `locale`='deDE') OR (`entry`=39843 /*39843*/ AND `locale`='deDE') OR (`entry`=53781 /*53781*/ AND `locale`='deDE') OR (`entry`=53783 /*53783*/ AND `locale`='deDE') OR (`entry`=53780 /*53780*/ AND `locale`='deDE') OR (`entry`=53782 /*53782*/ AND `locale`='deDE') OR (`entry`=53779 /*53779*/ AND `locale`='deDE') OR (`entry`=52688 /*52688*/ AND `locale`='deDE') OR (`entry`=52676 /*52676*/ AND `locale`='deDE') OR (`entry`=52682 /*52682*/ AND `locale`='deDE') OR (`entry`=53841 /*53841*/ AND `locale`='deDE') OR (`entry`=53842 /*53842*/ AND `locale`='deDE') OR (`entry`=53840 /*53840*/ AND `locale`='deDE') OR (`entry`=44775 /*44775*/ AND `locale`='deDE') OR (`entry`=52816 /*52816*/ AND `locale`='deDE') OR (`entry`=52594 /*52594*/ AND `locale`='deDE') OR (`entry`=54362 /*54362*/ AND `locale`='deDE') OR (`entry`=52795 /*52795*/ AND `locale`='deDE') OR (`entry`=52595 /*52595*/ AND `locale`='deDE') OR (`entry`=52596 /*52596*/ AND `locale`='deDE') OR (`entry`=53844 /*53844*/ AND `locale`='deDE') OR (`entry`=40757 /*40757*/ AND `locale`='deDE') OR (`entry`=40650 /*40650*/ AND `locale`='deDE') OR (`entry`=40660 /*40660*/ AND `locale`='deDE') OR (`entry`=40708 /*40708*/ AND `locale`='deDE') OR (`entry`=40578 /*40578*/ AND `locale`='deDE') OR (`entry`=40723 /*40723*/ AND `locale`='deDE') OR (`entry`=40720 /*40720*/ AND `locale`='deDE') OR (`entry`=41200 /*41200*/ AND `locale`='deDE') OR (`entry`=46464 /*46464*/ AND `locale`='deDE') OR (`entry`=52425 /*52425*/ AND `locale`='deDE') OR (`entry`=52906 /*52906*/ AND `locale`='deDE') OR (`entry`=53075 /*53075*/ AND `locale`='deDE') OR (`entry`=52844 /*52844*/ AND `locale`='deDE') OR (`entry`=52670 /*52670*/ AND `locale`='deDE') OR (`entry`=52669 /*52669*/ AND `locale`='deDE') OR (`entry`=55224 /*55224*/ AND `locale`='deDE') OR (`entry`=54393 /*54393*/ AND `locale`='deDE') OR (`entry`=52177 /*52177*/ AND `locale`='deDE') OR (`entry`=52547 /*52547*/ AND `locale`='deDE') OR (`entry`=53073 /*53073*/ AND `locale`='deDE') OR (`entry`=52986 /*52986*/ AND `locale`='deDE') OR (`entry`=52937 /*52937*/ AND `locale`='deDE') OR (`entry`=52932 /*52932*/ AND `locale`='deDE') OR (`entry`=52671 /*52671*/ AND `locale`='deDE') OR (`entry`=52219 /*52219*/ AND `locale`='deDE') OR (`entry`=53076 /*53076*/ AND `locale`='deDE') OR (`entry`=54320 /*54320*/ AND `locale`='deDE') OR (`entry`=53267 /*53267*/ AND `locale`='deDE') OR (`entry`=52289 /*52289*/ AND `locale`='deDE') OR (`entry`=52300 /*52300*/ AND `locale`='deDE') OR (`entry`=52195 /*52195*/ AND `locale`='deDE') OR (`entry`=52176 /*52176*/ AND `locale`='deDE') OR (`entry`=54251 /*54251*/ AND `locale`='deDE') OR (`entry`=52791 /*52791*/ AND `locale`='deDE') OR (`entry`=52794 /*52794*/ AND `locale`='deDE') OR (`entry`=53805 /*53805*/ AND `locale`='deDE') OR (`entry`=53823 /*53823*/ AND `locale`='deDE') OR (`entry`=41507 /*41507*/ AND `locale`='deDE') OR (`entry`=38915 /*38915*/ AND `locale`='deDE') OR (`entry`=36198 /*36198*/ AND `locale`='deDE') OR (`entry`=62885 /*62885*/ AND `locale`='deDE') OR (`entry`=40563 /*40563*/ AND `locale`='deDE') OR (`entry`=41504 /*41504*/ AND `locale`='deDE') OR (`entry`=41497 /*41497*/ AND `locale`='deDE') OR (`entry`=41492 /*41492*/ AND `locale`='deDE') OR (`entry`=41509 /*41509*/ AND `locale`='deDE') OR (`entry`=41499 /*41499*/ AND `locale`='deDE') OR (`entry`=41614 /*41614*/ AND `locale`='deDE') OR (`entry`=41557 /*41557*/ AND `locale`='deDE') OR (`entry`=41498 /*41498*/ AND `locale`='deDE') OR (`entry`=41563 /*41563*/ AND `locale`='deDE') OR (`entry`=41565 /*41565*/ AND `locale`='deDE') OR (`entry`=41502 /*41502*/ AND `locale`='deDE') OR (`entry`=41500 /*41500*/ AND `locale`='deDE') OR (`entry`=40564 /*40564*/ AND `locale`='deDE') OR (`entry`=40562 /*40562*/ AND `locale`='deDE') OR (`entry`=40573 /*40573*/ AND `locale`='deDE') OR (`entry`=40575 /*40575*/ AND `locale`='deDE') OR (`entry`=40780 /*40780*/ AND `locale`='deDE') OR (`entry`=41026 /*41026*/ AND `locale`='deDE') OR (`entry`=40928 /*40928*/ AND `locale`='deDE') OR (`entry`=40772 /*40772*/ AND `locale`='deDE') OR (`entry`=40934 /*40934*/ AND `locale`='deDE') OR (`entry`=40931 /*40931*/ AND `locale`='deDE') OR (`entry`=40687 /*40687*/ AND `locale`='deDE') OR (`entry`=50485 /*50485*/ AND `locale`='deDE') OR (`entry`=40463 /*40463*/ AND `locale`='deDE') OR (`entry`=40709 /*40709*/ AND `locale`='deDE') OR (`entry`=40814 /*40814*/ AND `locale`='deDE') OR (`entry`=65223 /*65223*/ AND `locale`='deDE') OR (`entry`=40755 /*40755*/ AND `locale`='deDE') OR (`entry`=40767 /*40767*/ AND `locale`='deDE') OR (`entry`=40713 /*40713*/ AND `locale`='deDE') OR (`entry`=62886 /*62886*/ AND `locale`='deDE') OR (`entry`=34398 /*34398*/ AND `locale`='deDE') OR (`entry`=33166 /*33166*/ AND `locale`='deDE') OR (`entry`=34399 /*34399*/ AND `locale`='deDE') OR (`entry`=33057 /*33057*/ AND `locale`='deDE') OR (`entry`=3823 /*3823*/ AND `locale`='deDE') OR (`entry`=32996 /*32996*/ AND `locale`='deDE') OR (`entry`=32997 /*32997*/ AND `locale`='deDE') OR (`entry`=34334 /*34334*/ AND `locale`='deDE') OR (`entry`=33043 /*33043*/ AND `locale`='deDE') OR (`entry`=3816 /*3816*/ AND `locale`='deDE') OR (`entry`=33044 /*33044*/ AND `locale`='deDE') OR (`entry`=32968 /*32968*/ AND `locale`='deDE') OR (`entry`=43425 /*43425*/ AND `locale`='deDE') OR (`entry`=34404 /*34404*/ AND `locale`='deDE') OR (`entry`=34403 /*34403*/ AND `locale`='deDE') OR (`entry`=34402 /*34402*/ AND `locale`='deDE') OR (`entry`=33250 /*33250*/ AND `locale`='deDE') OR (`entry`=33072 /*33072*/ AND `locale`='deDE') OR (`entry`=34392 /*34392*/ AND `locale`='deDE') OR (`entry`=33253 /*33253*/ AND `locale`='deDE') OR (`entry`=34301 /*34301*/ AND `locale`='deDE') OR (`entry`=33058 /*33058*/ AND `locale`='deDE') OR (`entry`=2192 /*2192*/ AND `locale`='deDE') OR (`entry`=34326 /*34326*/ AND `locale`='deDE') OR (`entry`=34427 /*34427*/ AND `locale`='deDE') OR (`entry`=33112 /*33112*/ AND `locale`='deDE') OR (`entry`=2184 /*2184*/ AND `locale`='deDE') OR (`entry`=34299 /*34299*/ AND `locale`='deDE') OR (`entry`=34316 /*34316*/ AND `locale`='deDE') OR (`entry`=33091 /*33091*/ AND `locale`='deDE') OR (`entry`=34385 /*34385*/ AND `locale`='deDE') OR (`entry`=33119 /*33119*/ AND `locale`='deDE') OR (`entry`=33107 /*33107*/ AND `locale`='deDE') OR (`entry`=3694 /*3694*/ AND `locale`='deDE') OR (`entry`=32987 /*32987*/ AND `locale`='deDE') OR (`entry`=4194 /*4194*/ AND `locale`='deDE') OR (`entry`=4190 /*4190*/ AND `locale`='deDE') OR (`entry`=33231 /*33231*/ AND `locale`='deDE') OR (`entry`=34304 /*34304*/ AND `locale`='deDE') OR (`entry`=33083 /*33083*/ AND `locale`='deDE') OR (`entry`=34302 /*34302*/ AND `locale`='deDE') OR (`entry`=32967 /*32967*/ AND `locale`='deDE') OR (`entry`=34406 /*34406*/ AND `locale`='deDE') OR (`entry`=34413 /*34413*/ AND `locale`='deDE') OR (`entry`=34405 /*34405*/ AND `locale`='deDE') OR (`entry`=34417 /*34417*/ AND `locale`='deDE') OR (`entry`=34396 /*34396*/ AND `locale`='deDE') OR (`entry`=62250 /*62250*/ AND `locale`='deDE') OR (`entry`=2165 /*2165*/ AND `locale`='deDE') OR (`entry`=34318 /*34318*/ AND `locale`='deDE') OR (`entry`=34306 /*34306*/ AND `locale`='deDE') OR (`entry`=2071 /*2071*/ AND `locale`='deDE') OR (`entry`=33117 /*33117*/ AND `locale`='deDE') OR (`entry`=33084 /*33084*/ AND `locale`='deDE') OR (`entry`=34282 /*34282*/ AND `locale`='deDE') OR (`entry`=34293 /*34293*/ AND `locale`='deDE') OR (`entry`=2321 /*2321*/ AND `locale`='deDE') OR (`entry`=2172 /*2172*/ AND `locale`='deDE') OR (`entry`=2237 /*2237*/ AND `locale`='deDE') OR (`entry`=34342 /*34342*/ AND `locale`='deDE') OR (`entry`=34340 /*34340*/ AND `locale`='deDE') OR (`entry`=34343 /*34343*/ AND `locale`='deDE') OR (`entry`=34339 /*34339*/ AND `locale`='deDE') OR (`entry`=2207 /*2207*/ AND `locale`='deDE') OR (`entry`=34423 /*34423*/ AND `locale`='deDE') OR (`entry`=34414 /*34414*/ AND `locale`='deDE') OR (`entry`=24042 /*24042*/ AND `locale`='deDE') OR (`entry`=34415 /*34415*/ AND `locale`='deDE') OR (`entry`=33079 /*33079*/ AND `locale`='deDE') OR (`entry`=34356 /*34356*/ AND `locale`='deDE') OR (`entry`=34350 /*34350*/ AND `locale`='deDE') OR (`entry`=2206 /*2206*/ AND `locale`='deDE') OR (`entry`=2233 /*2233*/ AND `locale`='deDE') OR (`entry`=34351 /*34351*/ AND `locale`='deDE') OR (`entry`=32999 /*32999*/ AND `locale`='deDE') OR (`entry`=33035 /*33035*/ AND `locale`='deDE') OR (`entry`=6887 /*6887*/ AND `locale`='deDE') OR (`entry`=33232 /*33232*/ AND `locale`='deDE') OR (`entry`=33039 /*33039*/ AND `locale`='deDE') OR (`entry`=33037 /*33037*/ AND `locale`='deDE') OR (`entry`=32989 /*32989*/ AND `locale`='deDE') OR (`entry`=33047 /*33047*/ AND `locale`='deDE') OR (`entry`=33045 /*33045*/ AND `locale`='deDE') OR (`entry`=33041 /*33041*/ AND `locale`='deDE') OR (`entry`=33111 /*33111*/ AND `locale`='deDE') OR (`entry`=33040 /*33040*/ AND `locale`='deDE') OR (`entry`=32986 /*32986*/ AND `locale`='deDE') OR (`entry`=33106 /*33106*/ AND `locale`='deDE') OR (`entry`=33053 /*33053*/ AND `locale`='deDE') OR (`entry`=32990 /*32990*/ AND `locale`='deDE') OR (`entry`=33001 /*33001*/ AND `locale`='deDE') OR (`entry`=33033 /*33033*/ AND `locale`='deDE') OR (`entry`=32988 /*32988*/ AND `locale`='deDE') OR (`entry`=32985 /*32985*/ AND `locale`='deDE') OR (`entry`=34315 /*34315*/ AND `locale`='deDE') OR (`entry`=33126 /*33126*/ AND `locale`='deDE') OR (`entry`=33048 /*33048*/ AND `locale`='deDE') OR (`entry`=2175 /*2175*/ AND `locale`='deDE') OR (`entry`=33177 /*33177*/ AND `locale`='deDE') OR (`entry`=32932 /*32932*/ AND `locale`='deDE') OR (`entry`=3841 /*3841*/ AND `locale`='deDE') OR (`entry`=32974 /*32974*/ AND `locale`='deDE') OR (`entry`=32973 /*32973*/ AND `locale`='deDE') OR (`entry`=32972 /*32972*/ AND `locale`='deDE') OR (`entry`=43431 /*43431*/ AND `locale`='deDE') OR (`entry`=43420 /*43420*/ AND `locale`='deDE') OR (`entry`=32978 /*32978*/ AND `locale`='deDE') OR (`entry`=32977 /*32977*/ AND `locale`='deDE') OR (`entry`=32971 /*32971*/ AND `locale`='deDE') OR (`entry`=51997 /*51997*/ AND `locale`='deDE') OR (`entry`=43436 /*43436*/ AND `locale`='deDE') OR (`entry`=43428 /*43428*/ AND `locale`='deDE') OR (`entry`=11037 /*11037*/ AND `locale`='deDE') OR (`entry`=49923 /*49923*/ AND `locale`='deDE') OR (`entry`=43439 /*43439*/ AND `locale`='deDE') OR (`entry`=43429 /*43429*/ AND `locale`='deDE') OR (`entry`=43424 /*43424*/ AND `locale`='deDE') OR (`entry`=32979 /*32979*/ AND `locale`='deDE') OR (`entry`=63084 /*63084*/ AND `locale`='deDE') OR (`entry`=63083 /*63083*/ AND `locale`='deDE') OR (`entry`=49968 /*49968*/ AND `locale`='deDE') OR (`entry`=49963 /*49963*/ AND `locale`='deDE') OR (`entry`=49940 /*49940*/ AND `locale`='deDE') OR (`entry`=49939 /*49939*/ AND `locale`='deDE') OR (`entry`=49927 /*49927*/ AND `locale`='deDE') OR (`entry`=32912 /*32912*/ AND `locale`='deDE') OR (`entry`=10085 /*10085*/ AND `locale`='deDE') OR (`entry`=4187 /*4187*/ AND `locale`='deDE') OR (`entry`=49942 /*49942*/ AND `locale`='deDE') OR (`entry`=15601 /*15601*/ AND `locale`='deDE') OR (`entry`=33864 /*33864*/ AND `locale`='deDE') OR (`entry`=32928 /*32928*/ AND `locale`='deDE') OR (`entry`=33127 /*33127*/ AND `locale`='deDE') OR (`entry`=32899 /*32899*/ AND `locale`='deDE') OR (`entry`=32890 /*32890*/ AND `locale`='deDE') OR (`entry`=32888 /*32888*/ AND `locale`='deDE') OR (`entry`=33296 /*33296*/ AND `locale`='deDE') OR (`entry`=33181 /*33181*/ AND `locale`='deDE') OR (`entry`=33207 /*33207*/ AND `locale`='deDE') OR (`entry`=33206 /*33206*/ AND `locale`='deDE') OR (`entry`=33180 /*33180*/ AND `locale`='deDE') OR (`entry`=33978 /*33978*/ AND `locale`='deDE') OR (`entry`=33179 /*33179*/ AND `locale`='deDE') OR (`entry`=33997 /*33997*/ AND `locale`='deDE') OR (`entry`=33020 /*33020*/ AND `locale`='deDE') OR (`entry`=33981 /*33981*/ AND `locale`='deDE') OR (`entry`=33980 /*33980*/ AND `locale`='deDE') OR (`entry`=33022 /*33022*/ AND `locale`='deDE') OR (`entry`=33024 /*33024*/ AND `locale`='deDE') OR (`entry`=33023 /*33023*/ AND `locale`='deDE') OR (`entry`=33021 /*33021*/ AND `locale`='deDE') OR (`entry`=32868 /*32868*/ AND `locale`='deDE') OR (`entry`=48764 /*48764*/ AND `locale`='deDE') OR (`entry`=48763 /*48763*/ AND `locale`='deDE') OR (`entry`=48650 /*48650*/ AND `locale`='deDE') OR (`entry`=48648 /*48648*/ AND `locale`='deDE') OR (`entry`=33905 /*33905*/ AND `locale`='deDE') OR (`entry`=33903 /*33903*/ AND `locale`='deDE') OR (`entry`=33884 /*33884*/ AND `locale`='deDE') OR (`entry`=34033 /*34033*/ AND `locale`='deDE') OR (`entry`=34231 /*34231*/ AND `locale`='deDE') OR (`entry`=32869 /*32869*/ AND `locale`='deDE') OR (`entry`=32860 /*32860*/ AND `locale`='deDE') OR (`entry`=34103 /*34103*/ AND `locale`='deDE') OR (`entry`=32966 /*32966*/ AND `locale`='deDE') OR (`entry`=32965 /*32965*/ AND `locale`='deDE') OR (`entry`=32963 /*32963*/ AND `locale`='deDE') OR (`entry`=43419 /*43419*/ AND `locale`='deDE') OR (`entry`=34041 /*34041*/ AND `locale`='deDE') OR (`entry`=33313 /*33313*/ AND `locale`='deDE') OR (`entry`=34030 /*34030*/ AND `locale`='deDE') OR (`entry`=34046 /*34046*/ AND `locale`='deDE') OR (`entry`=33009 /*33009*/ AND `locale`='deDE') OR (`entry`=32975 /*32975*/ AND `locale`='deDE') OR (`entry`=34056 /*34056*/ AND `locale`='deDE') OR (`entry`=33175 /*33175*/ AND `locale`='deDE') OR (`entry`=32936 /*32936*/ AND `locale`='deDE') OR (`entry`=33311 /*33311*/ AND `locale`='deDE') OR (`entry`=32935 /*32935*/ AND `locale`='deDE') OR (`entry`=62246 /*62246*/ AND `locale`='deDE') OR (`entry`=7015 /*7015*/ AND `locale`='deDE') OR (`entry`=33277 /*33277*/ AND `locale`='deDE') OR (`entry`=33262 /*33262*/ AND `locale`='deDE') OR (`entry`=64375 /*64375*/ AND `locale`='deDE') OR (`entry`=33345 /*33345*/ AND `locale`='deDE') OR (`entry`=32964 /*32964*/ AND `locale`='deDE') OR (`entry`=33055 /*33055*/ AND `locale`='deDE') OR (`entry`=32970 /*32970*/ AND `locale`='deDE') OR (`entry`=34248 /*34248*/ AND `locale`='deDE') OR (`entry`=33056 /*33056*/ AND `locale`='deDE') OR (`entry`=32859 /*32859*/ AND `locale`='deDE') OR (`entry`=32863 /*32863*/ AND `locale`='deDE') OR (`entry`=32861 /*32861*/ AND `locale`='deDE') OR (`entry`=34321 /*34321*/ AND `locale`='deDE') OR (`entry`=34309 /*34309*/ AND `locale`='deDE') OR (`entry`=33176 /*33176*/ AND `locale`='deDE') OR (`entry`=33359 /*33359*/ AND `locale`='deDE') OR (`entry`=32855 /*32855*/ AND `locale`='deDE') OR (`entry`=48024 /*48024*/ AND `locale`='deDE') OR (`entry`=48025 /*48025*/ AND `locale`='deDE') OR (`entry`=47679 /*47679*/ AND `locale`='deDE') OR (`entry`=50833 /*50833*/ AND `locale`='deDE') OR (`entry`=50724 /*50724*/ AND `locale`='deDE') OR (`entry`=48333 /*48333*/ AND `locale`='deDE') OR (`entry`=48493 /*48493*/ AND `locale`='deDE') OR (`entry`=47923 /*47923*/ AND `locale`='deDE') OR (`entry`=48007 /*48007*/ AND `locale`='deDE') OR (`entry`=51513 /*51513*/ AND `locale`='deDE') OR (`entry`=48236 /*48236*/ AND `locale`='deDE') OR (`entry`=48235 /*48235*/ AND `locale`='deDE') OR (`entry`=48127 /*48127*/ AND `locale`='deDE') OR (`entry`=43085 /*43085*/ AND `locale`='deDE') OR (`entry`=48332 /*48332*/ AND `locale`='deDE') OR (`entry`=48228 /*48228*/ AND `locale`='deDE') OR (`entry`=48238 /*48238*/ AND `locale`='deDE') OR (`entry`=48310 /*48310*/ AND `locale`='deDE') OR (`entry`=7104 /*7104*/ AND `locale`='deDE') OR (`entry`=50864 /*50864*/ AND `locale`='deDE') OR (`entry`=47556 /*47556*/ AND `locale`='deDE') OR (`entry`=48453 /*48453*/ AND `locale`='deDE') OR (`entry`=48452 /*48452*/ AND `locale`='deDE') OR (`entry`=48553 /*48553*/ AND `locale`='deDE') OR (`entry`=48492 /*48492*/ AND `locale`='deDE') OR (`entry`=48258 /*48258*/ AND `locale`='deDE') OR (`entry`=26043 /*26043*/ AND `locale`='deDE') OR (`entry`=543 /*543*/ AND `locale`='deDE') OR (`entry`=48552 /*48552*/ AND `locale`='deDE') OR (`entry`=47931 /*47931*/ AND `locale`='deDE') OR (`entry`=48555 /*48555*/ AND `locale`='deDE') OR (`entry`=15315 /*15315*/ AND `locale`='deDE') OR (`entry`=2803 /*2803*/ AND `locale`='deDE') OR (`entry`=12578 /*12578*/ AND `locale`='deDE') OR (`entry`=11181 /*11181*/ AND `locale`='deDE') OR (`entry`=5501 /*5501*/ AND `locale`='deDE') OR (`entry`=48556 /*48556*/ AND `locale`='deDE') OR (`entry`=48551 /*48551*/ AND `locale`='deDE') OR (`entry`=7100 /*7100*/ AND `locale`='deDE') OR (`entry`=14343 /*14343*/ AND `locale`='deDE') OR (`entry`=48259 /*48259*/ AND `locale`='deDE') OR (`entry`=48454 /*48454*/ AND `locale`='deDE') OR (`entry`=7149 /*7149*/ AND `locale`='deDE') OR (`entry`=7139 /*7139*/ AND `locale`='deDE') OR (`entry`=48077 /*48077*/ AND `locale`='deDE') OR (`entry`=48587 /*48587*/ AND `locale`='deDE') OR (`entry`=48581 /*48581*/ AND `locale`='deDE') OR (`entry`=48580 /*48580*/ AND `locale`='deDE') OR (`entry`=48456 /*48456*/ AND `locale`='deDE') OR (`entry`=48459 /*48459*/ AND `locale`='deDE') OR (`entry`=43073 /*43073*/ AND `locale`='deDE') OR (`entry`=48577 /*48577*/ AND `locale`='deDE') OR (`entry`=48574 /*48574*/ AND `locale`='deDE') OR (`entry`=48573 /*48573*/ AND `locale`='deDE') OR (`entry`=47843 /*47843*/ AND `locale`='deDE') OR (`entry`=48349 /*48349*/ AND `locale`='deDE') OR (`entry`=48339 /*48339*/ AND `locale`='deDE') OR (`entry`=48126 /*48126*/ AND `locale`='deDE') OR (`entry`=48491 /*48491*/ AND `locale`='deDE') OR (`entry`=48469 /*48469*/ AND `locale`='deDE') OR (`entry`=48216 /*48216*/ AND `locale`='deDE') OR (`entry`=47844 /*47844*/ AND `locale`='deDE') OR (`entry`=47842 /*47842*/ AND `locale`='deDE') OR (`entry`=48215 /*48215*/ AND `locale`='deDE') OR (`entry`=47747 /*47747*/ AND `locale`='deDE') OR (`entry`=48038 /*48038*/ AND `locale`='deDE') OR (`entry`=50777 /*50777*/ AND `locale`='deDE') OR (`entry`=48455 /*48455*/ AND `locale`='deDE') OR (`entry`=48457 /*48457*/ AND `locale`='deDE') OR (`entry`=48344 /*48344*/ AND `locale`='deDE') OR (`entry`=62317 /*62317*/ AND `locale`='deDE') OR (`entry`=7136 /*7136*/ AND `locale`='deDE') OR (`entry`=9879 /*9879*/ AND `locale`='deDE') OR (`entry`=14345 /*14345*/ AND `locale`='deDE') OR (`entry`=9454 /*9454*/ AND `locale`='deDE') OR (`entry`=107596 /*107596*/ AND `locale`='deDE') OR (`entry`=9517 /*9517*/ AND `locale`='deDE') OR (`entry`=9516 /*9516*/ AND `locale`='deDE') OR (`entry`=48240 /*48240*/ AND `locale`='deDE') OR (`entry`=48023 /*48023*/ AND `locale`='deDE') OR (`entry`=9877 /*9877*/ AND `locale`='deDE') OR (`entry`=50362 /*50362*/ AND `locale`='deDE') OR (`entry`=51664 /*51664*/ AND `locale`='deDE') OR (`entry`=7093 /*7093*/ AND `locale`='deDE') OR (`entry`=9861 /*9861*/ AND `locale`='deDE') OR (`entry`=9860 /*9860*/ AND `locale`='deDE') OR (`entry`=15603 /*15603*/ AND `locale`='deDE') OR (`entry`=47687 /*47687*/ AND `locale`='deDE') OR (`entry`=9862 /*9862*/ AND `locale`='deDE') OR (`entry`=7125 /*7125*/ AND `locale`='deDE') OR (`entry`=7110 /*7110*/ AND `locale`='deDE') OR (`entry`=47392 /*47392*/ AND `locale`='deDE') OR (`entry`=7106 /*7106*/ AND `locale`='deDE') OR (`entry`=14344 /*14344*/ AND `locale`='deDE') OR (`entry`=7113 /*7113*/ AND `locale`='deDE') OR (`entry`=7112 /*7112*/ AND `locale`='deDE') OR (`entry`=7115 /*7115*/ AND `locale`='deDE') OR (`entry`=66447 /*66447*/ AND `locale`='deDE') OR (`entry`=66445 /*66445*/ AND `locale`='deDE') OR (`entry`=66444 /*66444*/ AND `locale`='deDE') OR (`entry`=66443 /*66443*/ AND `locale`='deDE') OR (`entry`=66442 /*66442*/ AND `locale`='deDE') OR (`entry`=47675 /*47675*/ AND `locale`='deDE') OR (`entry`=7126 /*7126*/ AND `locale`='deDE') OR (`entry`=7120 /*7120*/ AND `locale`='deDE') OR (`entry`=7118 /*7118*/ AND `locale`='deDE') OR (`entry`=51025 /*51025*/ AND `locale`='deDE') OR (`entry`=7114 /*7114*/ AND `locale`='deDE') OR (`entry`=47696 /*47696*/ AND `locale`='deDE') OR (`entry`=47692 /*47692*/ AND `locale`='deDE') OR (`entry`=7092 /*7092*/ AND `locale`='deDE') OR (`entry`=48599 /*48599*/ AND `locale`='deDE') OR (`entry`=47617 /*47617*/ AND `locale`='deDE') OR (`entry`=43079 /*43079*/ AND `locale`='deDE') OR (`entry`=11019 /*11019*/ AND `locale`='deDE') OR (`entry`=10922 /*10922*/ AND `locale`='deDE') OR (`entry`=8958 /*8958*/ AND `locale`='deDE') OR (`entry`=7099 /*7099*/ AND `locale`='deDE') OR (`entry`=8960 /*8960*/ AND `locale`='deDE') OR (`entry`=47339 /*47339*/ AND `locale`='deDE') OR (`entry`=14339 /*14339*/ AND `locale`='deDE') OR (`entry`=10923 /*10923*/ AND `locale`='deDE') OR (`entry`=10921 /*10921*/ AND `locale`='deDE') OR (`entry`=48608 /*48608*/ AND `locale`='deDE') OR (`entry`=48607 /*48607*/ AND `locale`='deDE') OR (`entry`=22931 /*22931*/ AND `locale`='deDE') OR (`entry`=11554 /*11554*/ AND `locale`='deDE') OR (`entry`=10924 /*10924*/ AND `locale`='deDE') OR (`entry`=3792 /*3792*/ AND `locale`='deDE') OR (`entry`=3919 /*3919*/ AND `locale`='deDE') OR (`entry`=14342 /*14342*/ AND `locale`='deDE') OR (`entry`=62314 /*62314*/ AND `locale`='deDE') OR (`entry`=7154 /*7154*/ AND `locale`='deDE') OR (`entry`=7153 /*7153*/ AND `locale`='deDE') OR (`entry`=7155 /*7155*/ AND `locale`='deDE') OR (`entry`=47308 /*47308*/ AND `locale`='deDE') OR (`entry`=14340 /*14340*/ AND `locale`='deDE') OR (`entry`=50003 /*50003*/ AND `locale`='deDE') OR (`entry`=7109 /*7109*/ AND `locale`='deDE') OR (`entry`=7105 /*7105*/ AND `locale`='deDE') OR (`entry`=47369 /*47369*/ AND `locale`='deDE') OR (`entry`=9116 /*9116*/ AND `locale`='deDE') OR (`entry`=47341 /*47341*/ AND `locale`='deDE') OR (`entry`=62315 /*62315*/ AND `locale`='deDE') OR (`entry`=17425 /*17425*/ AND `locale`='deDE') OR (`entry`=63335 /*63335*/ AND `locale`='deDE') OR (`entry`=17071 /*17071*/ AND `locale`='deDE') OR (`entry`=16920 /*16920*/ AND `locale`='deDE') OR (`entry`=16919 /*16919*/ AND `locale`='deDE') OR (`entry`=16500 /*16500*/ AND `locale`='deDE') OR (`entry`=16535 /*16535*/ AND `locale`='deDE') OR (`entry`=16503 /*16503*/ AND `locale`='deDE') OR (`entry`=16499 /*16499*/ AND `locale`='deDE') OR (`entry`=16502 /*16502*/ AND `locale`='deDE') OR (`entry`=16537 /*16537*/ AND `locale`='deDE') OR (`entry`=16518 /*16518*/ AND `locale`='deDE') OR (`entry`=20239 /*20239*/ AND `locale`='deDE') OR (`entry`=16922 /*16922*/ AND `locale`='deDE') OR (`entry`=17181 /*17181*/ AND `locale`='deDE') OR (`entry`=17182 /*17182*/ AND `locale`='deDE') OR (`entry`=17180 /*17180*/ AND `locale`='deDE') OR (`entry`=17179 /*17179*/ AND `locale`='deDE') OR (`entry`=17087 /*17087*/ AND `locale`='deDE') OR (`entry`=16475 /*16475*/ AND `locale`='deDE') OR (`entry`=16971 /*16971*/ AND `locale`='deDE') OR (`entry`=16918 /*16918*/ AND `locale`='deDE') OR (`entry`=16501 /*16501*/ AND `locale`='deDE') OR (`entry`=16917 /*16917*/ AND `locale`='deDE') OR (`entry`=44703 /*44703*/ AND `locale`='deDE') OR (`entry`=20233 /*20233*/ AND `locale`='deDE') OR (`entry`=20227 /*20227*/ AND `locale`='deDE') OR (`entry`=16514 /*16514*/ AND `locale`='deDE') OR (`entry`=17073 /*17073*/ AND `locale`='deDE') OR (`entry`=16477 /*16477*/ AND `locale`='deDE') OR (`entry`=16517 /*16517*/ AND `locale`='deDE') OR (`entry`=17947 /*17947*/ AND `locale`='deDE') OR (`entry`=16522 /*16522*/ AND `locale`='deDE') OR (`entry`=16546 /*16546*/ AND `locale`='deDE') OR (`entry`=16521 /*16521*/ AND `locale`='deDE') OR (`entry`=16516 /*16516*/ AND `locale`='deDE') OR (`entry`=16483 /*16483*/ AND `locale`='deDE') OR (`entry`=16520 /*16520*/ AND `locale`='deDE') OR (`entry`=16921 /*16921*/ AND `locale`='deDE') OR (`entry`=16554 /*16554*/ AND `locale`='deDE') OR (`entry`=18810 /*18810*/ AND `locale`='deDE') OR (`entry`=17116 /*17116*/ AND `locale`='deDE') OR (`entry`=17214 /*17214*/ AND `locale`='deDE') OR (`entry`=17117 /*17117*/ AND `locale`='deDE') OR (`entry`=17983 /*17983*/ AND `locale`='deDE') OR (`entry`=17215 /*17215*/ AND `locale`='deDE') OR (`entry`=17488 /*17488*/ AND `locale`='deDE') OR (`entry`=17114 /*17114*/ AND `locale`='deDE') OR (`entry`=17360 /*17360*/ AND `locale`='deDE') OR (`entry`=17232 /*17232*/ AND `locale`='deDE') OR (`entry`=17222 /*17222*/ AND `locale`='deDE') OR (`entry`=17212 /*17212*/ AND `locale`='deDE') OR (`entry`=16551 /*16551*/ AND `locale`='deDE') OR (`entry`=16476 /*16476*/ AND `locale`='deDE') OR (`entry`=17480 /*17480*/ AND `locale`='deDE') OR (`entry`=17482 /*17482*/ AND `locale`='deDE') OR (`entry`=16553 /*16553*/ AND `locale`='deDE') OR (`entry`=17485 /*17485*/ AND `locale`='deDE') OR (`entry`=17483 /*17483*/ AND `locale`='deDE') OR (`entry`=17481 /*17481*/ AND `locale`='deDE') OR (`entry`=17228 /*17228*/ AND `locale`='deDE') OR (`entry`=43991 /*43991*/ AND `locale`='deDE') OR (`entry`=17101 /*17101*/ AND `locale`='deDE') OR (`entry`=17243 /*17243*/ AND `locale`='deDE') OR (`entry`=17311 /*17311*/ AND `locale`='deDE') OR (`entry`=17186 /*17186*/ AND `locale`='deDE') OR (`entry`=17187 /*17187*/ AND `locale`='deDE') OR (`entry`=17312 /*17312*/ AND `locale`='deDE') OR (`entry`=17188 /*17188*/ AND `locale`='deDE') OR (`entry`=21376 /*21376*/ AND `locale`='deDE') OR (`entry`=17614 /*17614*/ AND `locale`='deDE') OR (`entry`=16721 /*16721*/ AND `locale`='deDE') OR (`entry`=17274 /*17274*/ AND `locale`='deDE') OR (`entry`=17205 /*17205*/ AND `locale`='deDE') OR (`entry`=17361 /*17361*/ AND `locale`='deDE') OR (`entry`=17551 /*17551*/ AND `locale`='deDE') OR (`entry`=17110 /*17110*/ AND `locale`='deDE') OR (`entry`=17484 /*17484*/ AND `locale`='deDE') OR (`entry`=63078 /*63078*/ AND `locale`='deDE') OR (`entry`=63077 /*63077*/ AND `locale`='deDE') OR (`entry`=47431 /*47431*/ AND `locale`='deDE') OR (`entry`=17929 /*17929*/ AND `locale`='deDE') OR (`entry`=17486 /*17486*/ AND `locale`='deDE') OR (`entry`=17930 /*17930*/ AND `locale`='deDE') OR (`entry`=17279 /*17279*/ AND `locale`='deDE') OR (`entry`=17278 /*17278*/ AND `locale`='deDE') OR (`entry`=17242 /*17242*/ AND `locale`='deDE') OR (`entry`=17241 /*17241*/ AND `locale`='deDE') OR (`entry`=17240 /*17240*/ AND `locale`='deDE') OR (`entry`=17285 /*17285*/ AND `locale`='deDE') OR (`entry`=17246 /*17246*/ AND `locale`='deDE') OR (`entry`=17245 /*17245*/ AND `locale`='deDE') OR (`entry`=17489 /*17489*/ AND `locale`='deDE') OR (`entry`=17487 /*17487*/ AND `locale`='deDE') OR (`entry`=16225 /*16225*/ AND `locale`='deDE') OR (`entry`=620 /*620*/ AND `locale`='deDE') OR (`entry`=17244 /*17244*/ AND `locale`='deDE') OR (`entry`=17247 /*17247*/ AND `locale`='deDE') OR (`entry`=17587 /*17587*/ AND `locale`='deDE') OR (`entry`=17196 /*17196*/ AND `locale`='deDE') OR (`entry`=17372 /*17372*/ AND `locale`='deDE') OR (`entry`=17217 /*17217*/ AND `locale`='deDE') OR (`entry`=17298 /*17298*/ AND `locale`='deDE') OR (`entry`=17195 /*17195*/ AND `locale`='deDE') OR (`entry`=17193 /*17193*/ AND `locale`='deDE') OR (`entry`=17194 /*17194*/ AND `locale`='deDE') OR (`entry`=17364 /*17364*/ AND `locale`='deDE') OR (`entry`=17375 /*17375*/ AND `locale`='deDE') OR (`entry`=17183 /*17183*/ AND `locale`='deDE') OR (`entry`=17184 /*17184*/ AND `locale`='deDE') OR (`entry`=17185 /*17185*/ AND `locale`='deDE') OR (`entry`=18038 /*18038*/ AND `locale`='deDE') OR (`entry`=17453 /*17453*/ AND `locale`='deDE') OR (`entry`=17452 /*17452*/ AND `locale`='deDE') OR (`entry`=17448 /*17448*/ AND `locale`='deDE') OR (`entry`=17490 /*17490*/ AND `locale`='deDE') OR (`entry`=17438 /*17438*/ AND `locale`='deDE') OR (`entry`=17437 /*17437*/ AND `locale`='deDE') OR (`entry`=17444 /*17444*/ AND `locale`='deDE') OR (`entry`=17440 /*17440*/ AND `locale`='deDE') OR (`entry`=17441 /*17441*/ AND `locale`='deDE') OR (`entry`=17439 /*17439*/ AND `locale`='deDE') OR (`entry`=17445 /*17445*/ AND `locale`='deDE') OR (`entry`=17442 /*17442*/ AND `locale`='deDE') OR (`entry`=17446 /*17446*/ AND `locale`='deDE') OR (`entry`=17443 /*17443*/ AND `locale`='deDE') OR (`entry`=17432 /*17432*/ AND `locale`='deDE') OR (`entry`=61255 /*61255*/ AND `locale`='deDE') OR (`entry`=17272 /*17272*/ AND `locale`='deDE') OR (`entry`=17556 /*17556*/ AND `locale`='deDE') OR (`entry`=17199 /*17199*/ AND `locale`='deDE') OR (`entry`=17495 /*17495*/ AND `locale`='deDE') OR (`entry`=17189 /*17189*/ AND `locale`='deDE') OR (`entry`=17467 /*17467*/ AND `locale`='deDE') OR (`entry`=17192 /*17192*/ AND `locale`='deDE') OR (`entry`=17191 /*17191*/ AND `locale`='deDE') OR (`entry`=17475 /*17475*/ AND `locale`='deDE') OR (`entry`=17190 /*17190*/ AND `locale`='deDE') OR (`entry`=17374 /*17374*/ AND `locale`='deDE') OR (`entry`=17203 /*17203*/ AND `locale`='deDE') OR (`entry`=18815 /*18815*/ AND `locale`='deDE') OR (`entry`=20914 /*20914*/ AND `locale`='deDE') OR (`entry`=20850 /*20850*/ AND `locale`='deDE') OR (`entry`=20848 /*20848*/ AND `locale`='deDE') OR (`entry`=20847 /*20847*/ AND `locale`='deDE') OR (`entry`=20846 /*20846*/ AND `locale`='deDE') OR (`entry`=17584 /*17584*/ AND `locale`='deDE') OR (`entry`=19658 /*19658*/ AND `locale`='deDE') OR (`entry`=20849 /*20849*/ AND `locale`='deDE') OR (`entry`=17530 /*17530*/ AND `locale`='deDE') OR (`entry`=17197 /*17197*/ AND `locale`='deDE') OR (`entry`=16739 /*16739*/ AND `locale`='deDE') OR (`entry`=17520 /*17520*/ AND `locale`='deDE') OR (`entry`=21019 /*21019*/ AND `locale`='deDE') OR (`entry`=21018 /*21018*/ AND `locale`='deDE') OR (`entry`=21010 /*21010*/ AND `locale`='deDE') OR (`entry`=21008 /*21008*/ AND `locale`='deDE') OR (`entry`=18919 /*18919*/ AND `locale`='deDE') OR (`entry`=18918 /*18918*/ AND `locale`='deDE') OR (`entry`=18917 /*18917*/ AND `locale`='deDE') OR (`entry`=22905 /*22905*/ AND `locale`='deDE') OR (`entry`=18916 /*18916*/ AND `locale`='deDE') OR (`entry`=18915 /*18915*/ AND `locale`='deDE') OR (`entry`=17204 /*17204*/ AND `locale`='deDE') OR (`entry`=16708 /*16708*/ AND `locale`='deDE') OR (`entry`=16774 /*16774*/ AND `locale`='deDE') OR (`entry`=17519 /*17519*/ AND `locale`='deDE') OR (`entry`=17219 /*17219*/ AND `locale`='deDE') OR (`entry`=16757 /*16757*/ AND `locale`='deDE') OR (`entry`=17435 /*17435*/ AND `locale`='deDE') OR (`entry`=17436 /*17436*/ AND `locale`='deDE') OR (`entry`=62050 /*62050*/ AND `locale`='deDE') OR (`entry`=18813 /*18813*/ AND `locale`='deDE') OR (`entry`=18812 /*18812*/ AND `locale`='deDE') OR (`entry`=16731 /*16731*/ AND `locale`='deDE') OR (`entry`=30732 /*30732*/ AND `locale`='deDE') OR (`entry`=30716 /*30716*/ AND `locale`='deDE') OR (`entry`=19778 /*19778*/ AND `locale`='deDE') OR (`entry`=18985 /*18985*/ AND `locale`='deDE') OR (`entry`=17431 /*17431*/ AND `locale`='deDE') OR (`entry`=18892 /*18892*/ AND `locale`='deDE') OR (`entry`=17512 /*17512*/ AND `locale`='deDE') OR (`entry`=16727 /*16727*/ AND `locale`='deDE') OR (`entry`=16718 /*16718*/ AND `locale`='deDE') OR (`entry`=16719 /*16719*/ AND `locale`='deDE') OR (`entry`=16764 /*16764*/ AND `locale`='deDE') OR (`entry`=17555 /*17555*/ AND `locale`='deDE') OR (`entry`=50306 /*50306*/ AND `locale`='deDE') OR (`entry`=18921 /*18921*/ AND `locale`='deDE') OR (`entry`=18924 /*18924*/ AND `locale`='deDE') OR (`entry`=19073 /*19073*/ AND `locale`='deDE') OR (`entry`=17200 /*17200*/ AND `locale`='deDE') OR (`entry`=16765 /*16765*/ AND `locale`='deDE') OR (`entry`=16747 /*16747*/ AND `locale`='deDE') OR (`entry`=16716 /*16716*/ AND `locale`='deDE') OR (`entry`=16714 /*16714*/ AND `locale`='deDE') OR (`entry`=16762 /*16762*/ AND `locale`='deDE') OR (`entry`=16753 /*16753*/ AND `locale`='deDE') OR (`entry`=16750 /*16750*/ AND `locale`='deDE') OR (`entry`=16745 /*16745*/ AND `locale`='deDE') OR (`entry`=16728 /*16728*/ AND `locale`='deDE') OR (`entry`=16712 /*16712*/ AND `locale`='deDE') OR (`entry`=17505 /*17505*/ AND `locale`='deDE') OR (`entry`=16735 /*16735*/ AND `locale`='deDE') OR (`entry`=17122 /*17122*/ AND `locale`='deDE') OR (`entry`=16738 /*16738*/ AND `locale`='deDE') OR (`entry`=16715 /*16715*/ AND `locale`='deDE') OR (`entry`=16752 /*16752*/ AND `locale`='deDE') OR (`entry`=16751 /*16751*/ AND `locale`='deDE') OR (`entry`=16743 /*16743*/ AND `locale`='deDE') OR (`entry`=16740 /*16740*/ AND `locale`='deDE') OR (`entry`=16726 /*16726*/ AND `locale`='deDE') OR (`entry`=16724 /*16724*/ AND `locale`='deDE') OR (`entry`=16713 /*16713*/ AND `locale`='deDE') OR (`entry`=16657 /*16657*/ AND `locale`='deDE') OR (`entry`=20121 /*20121*/ AND `locale`='deDE') OR (`entry`=17504 /*17504*/ AND `locale`='deDE') OR (`entry`=16771 /*16771*/ AND `locale`='deDE') OR (`entry`=62464 /*62464*/ AND `locale`='deDE') OR (`entry`=17373 /*17373*/ AND `locale`='deDE') OR (`entry`=17120 /*17120*/ AND `locale`='deDE') OR (`entry`=16748 /*16748*/ AND `locale`='deDE') OR (`entry`=16763 /*16763*/ AND `locale`='deDE') OR (`entry`=16767 /*16767*/ AND `locale`='deDE') OR (`entry`=16746 /*16746*/ AND `locale`='deDE') OR (`entry`=16729 /*16729*/ AND `locale`='deDE') OR (`entry`=18349 /*18349*/ AND `locale`='deDE') OR (`entry`=16707 /*16707*/ AND `locale`='deDE') OR (`entry`=18348 /*18348*/ AND `locale`='deDE') OR (`entry`=22851 /*22851*/ AND `locale`='deDE') OR (`entry`=17538 /*17538*/ AND `locale`='deDE') OR (`entry`=16761 /*16761*/ AND `locale`='deDE') OR (`entry`=17121 /*17121*/ AND `locale`='deDE') OR (`entry`=17509 /*17509*/ AND `locale`='deDE') OR (`entry`=17216 /*17216*/ AND `locale`='deDE') OR (`entry`=18800 /*18800*/ AND `locale`='deDE') OR (`entry`=34987 /*34987*/ AND `locale`='deDE') OR (`entry`=16736 /*16736*/ AND `locale`='deDE') OR (`entry`=16723 /*16723*/ AND `locale`='deDE') OR (`entry`=16705 /*16705*/ AND `locale`='deDE') OR (`entry`=16741 /*16741*/ AND `locale`='deDE') OR (`entry`=34986 /*34986*/ AND `locale`='deDE') OR (`entry`=47570 /*47570*/ AND `locale`='deDE') OR (`entry`=18903 /*18903*/ AND `locale`='deDE') OR (`entry`=18902 /*18902*/ AND `locale`='deDE') OR (`entry`=18901 /*18901*/ AND `locale`='deDE') OR (`entry`=18900 /*18900*/ AND `locale`='deDE') OR (`entry`=18899 /*18899*/ AND `locale`='deDE') OR (`entry`=20674 /*20674*/ AND `locale`='deDE') OR (`entry`=18814 /*18814*/ AND `locale`='deDE') OR (`entry`=18896 /*18896*/ AND `locale`='deDE') OR (`entry`=47586 /*47586*/ AND `locale`='deDE') OR (`entry`=16742 /*16742*/ AND `locale`='deDE') OR (`entry`=16725 /*16725*/ AND `locale`='deDE') OR (`entry`=16722 /*16722*/ AND `locale`='deDE') OR (`entry`=17511 /*17511*/ AND `locale`='deDE') OR (`entry`=17510 /*17510*/ AND `locale`='deDE') OR (`entry`=16756 /*16756*/ AND `locale`='deDE') OR (`entry`=18350 /*18350*/ AND `locale`='deDE') OR (`entry`=17773 /*17773*/ AND `locale`='deDE') OR (`entry`=16710 /*16710*/ AND `locale`='deDE') OR (`entry`=20722 /*20722*/ AND `locale`='deDE') OR (`entry`=19171 /*19171*/ AND `locale`='deDE') OR (`entry`=16768 /*16768*/ AND `locale`='deDE') OR (`entry`=16732 /*16732*/ AND `locale`='deDE') OR (`entry`=16709 /*16709*/ AND `locale`='deDE') OR (`entry`=17201 /*17201*/ AND `locale`='deDE') OR (`entry`=17202 /*17202*/ AND `locale`='deDE') OR (`entry`=17468 /*17468*/ AND `locale`='deDE') OR (`entry`=18955 /*18955*/ AND `locale`='deDE') OR (`entry`=51501 /*51501*/ AND `locale`='deDE') OR (`entry`=16766 /*16766*/ AND `locale`='deDE') OR (`entry`=16734 /*16734*/ AND `locale`='deDE') OR (`entry`=16733 /*16733*/ AND `locale`='deDE') OR (`entry`=16706 /*16706*/ AND `locale`='deDE') OR (`entry`=17514 /*17514*/ AND `locale`='deDE') OR (`entry`=16755 /*16755*/ AND `locale`='deDE') OR (`entry`=17513 /*17513*/ AND `locale`='deDE') OR (`entry`=16749 /*16749*/ AND `locale`='deDE') OR (`entry`=16632 /*16632*/ AND `locale`='deDE') OR (`entry`=14379 /*14379*/ AND `locale`='deDE') OR (`entry`=10604 /*10604*/ AND `locale`='deDE') OR (`entry`=6091 /*6091*/ AND `locale`='deDE') OR (`entry`=3587 /*3587*/ AND `locale`='deDE') OR (`entry`=3589 /*3589*/ AND `locale`='deDE') OR (`entry`=3588 /*3588*/ AND `locale`='deDE') OR (`entry`=34757 /*34757*/ AND `locale`='deDE') OR (`entry`=34756 /*34756*/ AND `locale`='deDE') OR (`entry`=3514 /*3514*/ AND `locale`='deDE') OR (`entry`=3597 /*3597*/ AND `locale`='deDE') OR (`entry`=49479 /*49479*/ AND `locale`='deDE') OR (`entry`=8584 /*8584*/ AND `locale`='deDE') OR (`entry`=7690 /*7690*/ AND `locale`='deDE') OR (`entry`=43006 /*43006*/ AND `locale`='deDE') OR (`entry`=3596 /*3596*/ AND `locale`='deDE') OR (`entry`=3595 /*3595*/ AND `locale`='deDE') OR (`entry`=3594 /*3594*/ AND `locale`='deDE') OR (`entry`=3593 /*3593*/ AND `locale`='deDE') OR (`entry`=3591 /*3591*/ AND `locale`='deDE') OR (`entry`=3590 /*3590*/ AND `locale`='deDE') OR (`entry`=63331 /*63331*/ AND `locale`='deDE') OR (`entry`=44617 /*44617*/ AND `locale`='deDE') OR (`entry`=3592 /*3592*/ AND `locale`='deDE') OR (`entry`=2082 /*2082*/ AND `locale`='deDE') OR (`entry`=8583 /*8583*/ AND `locale`='deDE') OR (`entry`=2079 /*2079*/ AND `locale`='deDE') OR (`entry`=2077 /*2077*/ AND `locale`='deDE') OR (`entry`=1988 /*1988*/ AND `locale`='deDE') OR (`entry`=1994 /*1994*/ AND `locale`='deDE') OR (`entry`=49598 /*49598*/ AND `locale`='deDE') OR (`entry`=1986 /*1986*/ AND `locale`='deDE') OR (`entry`=1985 /*1985*/ AND `locale`='deDE') OR (`entry`=2032 /*2032*/ AND `locale`='deDE') OR (`entry`=1989 /*1989*/ AND `locale`='deDE') OR (`entry`=44614 /*44614*/ AND `locale`='deDE') OR (`entry`=1984 /*1984*/ AND `locale`='deDE') OR (`entry`=2031 /*2031*/ AND `locale`='deDE') OR (`entry`=6780 /*6780*/ AND `locale`='deDE') OR (`entry`=12160 /*12160*/ AND `locale`='deDE') OR (`entry`=2038 /*2038*/ AND `locale`='deDE') OR (`entry`=2004 /*2004*/ AND `locale`='deDE') OR (`entry`=2005 /*2005*/ AND `locale`='deDE') OR (`entry`=2002 /*2002*/ AND `locale`='deDE') OR (`entry`=14432 /*14432*/ AND `locale`='deDE') OR (`entry`=2003 /*2003*/ AND `locale`='deDE') OR (`entry`=3535 /*3535*/ AND `locale`='deDE') OR (`entry`=6094 /*6094*/ AND `locale`='deDE') OR (`entry`=4266 /*4266*/ AND `locale`='deDE') OR (`entry`=3614 /*3614*/ AND `locale`='deDE') OR (`entry`=3608 /*3608*/ AND `locale`='deDE') OR (`entry`=3600 /*3600*/ AND `locale`='deDE') OR (`entry`=3567 /*3567*/ AND `locale`='deDE') OR (`entry`=12429 /*12429*/ AND `locale`='deDE') OR (`entry`=3515 /*3515*/ AND `locale`='deDE') OR (`entry`=40553 /*40553*/ AND `locale`='deDE') OR (`entry`=2083 /*2083*/ AND `locale`='deDE') OR (`entry`=65097 /*65097*/ AND `locale`='deDE') OR (`entry`=63070 /*63070*/ AND `locale`='deDE') OR (`entry`=6781 /*6781*/ AND `locale`='deDE') OR (`entry`=6736 /*6736*/ AND `locale`='deDE') OR (`entry`=3602 /*3602*/ AND `locale`='deDE') OR (`entry`=3610 /*3610*/ AND `locale`='deDE') OR (`entry`=34761 /*34761*/ AND `locale`='deDE') OR (`entry`=2081 /*2081*/ AND `locale`='deDE') OR (`entry`=2078 /*2078*/ AND `locale`='deDE') OR (`entry`=3611 /*3611*/ AND `locale`='deDE') OR (`entry`=3571 /*3571*/ AND `locale`='deDE') OR (`entry`=43005 /*43005*/ AND `locale`='deDE') OR (`entry`=3598 /*3598*/ AND `locale`='deDE') OR (`entry`=11942 /*11942*/ AND `locale`='deDE') OR (`entry`=3613 /*3613*/ AND `locale`='deDE') OR (`entry`=6286 /*6286*/ AND `locale`='deDE') OR (`entry`=3599 /*3599*/ AND `locale`='deDE') OR (`entry`=3609 /*3609*/ AND `locale`='deDE') OR (`entry`=3612 /*3612*/ AND `locale`='deDE') OR (`entry`=47420 /*47420*/ AND `locale`='deDE') OR (`entry`=4265 /*4265*/ AND `locale`='deDE') OR (`entry`=10051 /*10051*/ AND `locale`='deDE') OR (`entry`=3601 /*3601*/ AND `locale`='deDE') OR (`entry`=15595 /*15595*/ AND `locale`='deDE') OR (`entry`=3306 /*3306*/ AND `locale`='deDE') OR (`entry`=3604 /*3604*/ AND `locale`='deDE') OR (`entry`=3603 /*3603*/ AND `locale`='deDE') OR (`entry`=2150 /*2150*/ AND `locale`='deDE') OR (`entry`=7234 /*7234*/ AND `locale`='deDE') OR (`entry`=7235 /*7235*/ AND `locale`='deDE') OR (`entry`=14428 /*14428*/ AND `locale`='deDE') OR (`entry`=2107 /*2107*/ AND `locale`='deDE') OR (`entry`=2006 /*2006*/ AND `locale`='deDE') OR (`entry`=2007 /*2007*/ AND `locale`='deDE') OR (`entry`=2022 /*2022*/ AND `locale`='deDE') OR (`entry`=14430 /*14430*/ AND `locale`='deDE') OR (`entry`=2008 /*2008*/ AND `locale`='deDE') OR (`entry`=2042 /*2042*/ AND `locale`='deDE') OR (`entry`=10606 /*10606*/ AND `locale`='deDE') OR (`entry`=2151 /*2151*/ AND `locale`='deDE') OR (`entry`=7317 /*7317*/ AND `locale`='deDE') OR (`entry`=7318 /*7318*/ AND `locale`='deDE') OR (`entry`=2025 /*2025*/ AND `locale`='deDE') OR (`entry`=2009 /*2009*/ AND `locale`='deDE') OR (`entry`=2010 /*2010*/ AND `locale`='deDE') OR (`entry`=34521 /*34521*/ AND `locale`='deDE') OR (`entry`=34642 /*34642*/ AND `locale`='deDE') OR (`entry`=34517 /*34517*/ AND `locale`='deDE') OR (`entry`=34524 /*34524*/ AND `locale`='deDE') OR (`entry`=34522 /*34522*/ AND `locale`='deDE') OR (`entry`=34530 /*34530*/ AND `locale`='deDE') OR (`entry`=34525 /*34525*/ AND `locale`='deDE') OR (`entry`=1992 /*1992*/ AND `locale`='deDE') OR (`entry`=2043 /*2043*/ AND `locale`='deDE') OR (`entry`=2011 /*2011*/ AND `locale`='deDE') OR (`entry`=1995 /*1995*/ AND `locale`='deDE') OR (`entry`=1998 /*1998*/ AND `locale`='deDE') OR (`entry`=2166 /*2166*/ AND `locale`='deDE') OR (`entry`=7313 /*7313*/ AND `locale`='deDE') OR (`entry`=3519 /*3519*/ AND `locale`='deDE') OR (`entry`=34575 /*34575*/ AND `locale`='deDE') OR (`entry`=3517 /*3517*/ AND `locale`='deDE') OR (`entry`=2080 /*2080*/ AND `locale`='deDE') OR (`entry`=44027 /*44027*/ AND `locale`='deDE') OR (`entry`=6287 /*6287*/ AND `locale`='deDE') OR (`entry`=3605 /*3605*/ AND `locale`='deDE') OR (`entry`=2027 /*2027*/ AND `locale`='deDE') OR (`entry`=2029 /*2029*/ AND `locale`='deDE') OR (`entry`=2030 /*2030*/ AND `locale`='deDE') OR (`entry`=2001 /*2001*/ AND `locale`='deDE') OR (`entry`=7319 /*7319*/ AND `locale`='deDE') OR (`entry`=2034 /*2034*/ AND `locale`='deDE') OR (`entry`=61165 /*61165*/ AND `locale`='deDE') OR (`entry`=3568 /*3568*/ AND `locale`='deDE') OR (`entry`=2021 /*2021*/ AND `locale`='deDE') OR (`entry`=2019 /*2019*/ AND `locale`='deDE') OR (`entry`=2020 /*2020*/ AND `locale`='deDE') OR (`entry`=2000 /*2000*/ AND `locale`='deDE') OR (`entry`=44030 /*44030*/ AND `locale`='deDE') OR (`entry`=3606 /*3606*/ AND `locale`='deDE') OR (`entry`=14431 /*14431*/ AND `locale`='deDE') OR (`entry`=2018 /*2018*/ AND `locale`='deDE') OR (`entry`=2015 /*2015*/ AND `locale`='deDE') OR (`entry`=2033 /*2033*/ AND `locale`='deDE') OR (`entry`=2017 /*2017*/ AND `locale`='deDE') OR (`entry`=890 /*890*/ AND `locale`='deDE') OR (`entry`=15624 /*15624*/ AND `locale`='deDE') OR (`entry`=1997 /*1997*/ AND `locale`='deDE') OR (`entry`=1999 /*1999*/ AND `locale`='deDE') OR (`entry`=2155 /*2155*/ AND `locale`='deDE') OR (`entry`=48624 /*48624*/ AND `locale`='deDE') OR (`entry`=1996 /*1996*/ AND `locale`='deDE') OR (`entry`=62242 /*62242*/ AND `locale`='deDE') OR (`entry`=4235 /*4235*/ AND `locale`='deDE') OR (`entry`=4173 /*4173*/ AND `locale`='deDE') OR (`entry`=4233 /*4233*/ AND `locale`='deDE') OR (`entry`=4232 /*4232*/ AND `locale`='deDE') OR (`entry`=4231 /*4231*/ AND `locale`='deDE') OR (`entry`=4171 /*4171*/ AND `locale`='deDE') OR (`entry`=14380 /*14380*/ AND `locale`='deDE') OR (`entry`=4230 /*4230*/ AND `locale`='deDE') OR (`entry`=4170 /*4170*/ AND `locale`='deDE') OR (`entry`=15678 /*15678*/ AND `locale`='deDE') OR (`entry`=8723 /*8723*/ AND `locale`='deDE') OR (`entry`=15679 /*15679*/ AND `locale`='deDE') OR (`entry`=8669 /*8669*/ AND `locale`='deDE') OR (`entry`=4164 /*4164*/ AND `locale`='deDE') OR (`entry`=6142 /*6142*/ AND `locale`='deDE') OR (`entry`=52641 /*52641*/ AND `locale`='deDE') OR (`entry`=52640 /*52640*/ AND `locale`='deDE') OR (`entry`=4203 /*4203*/ AND `locale`='deDE') OR (`entry`=4089 /*4089*/ AND `locale`='deDE') OR (`entry`=8026 /*8026*/ AND `locale`='deDE') OR (`entry`=34989 /*34989*/ AND `locale`='deDE') OR (`entry`=4088 /*4088*/ AND `locale`='deDE') OR (`entry`=4160 /*4160*/ AND `locale`='deDE') OR (`entry`=34988 /*34988*/ AND `locale`='deDE') OR (`entry`=11041 /*11041*/ AND `locale`='deDE') OR (`entry`=4784 /*4784*/ AND `locale`='deDE') OR (`entry`=7315 /*7315*/ AND `locale`='deDE') OR (`entry`=4786 /*4786*/ AND `locale`='deDE') OR (`entry`=4087 /*4087*/ AND `locale`='deDE') OR (`entry`=4226 /*4226*/ AND `locale`='deDE') OR (`entry`=11042 /*11042*/ AND `locale`='deDE') OR (`entry`=4783 /*4783*/ AND `locale`='deDE') OR (`entry`=52645 /*52645*/ AND `locale`='deDE') OR (`entry`=52644 /*52644*/ AND `locale`='deDE') OR (`entry`=47584 /*47584*/ AND `locale`='deDE') OR (`entry`=30731 /*30731*/ AND `locale`='deDE') OR (`entry`=30715 /*30715*/ AND `locale`='deDE') OR (`entry`=11070 /*11070*/ AND `locale`='deDE') OR (`entry`=4228 /*4228*/ AND `locale`='deDE') OR (`entry`=4213 /*4213*/ AND `locale`='deDE') OR (`entry`=4229 /*4229*/ AND `locale`='deDE') OR (`entry`=4159 /*4159*/ AND `locale`='deDE') OR (`entry`=4225 /*4225*/ AND `locale`='deDE') OR (`entry`=11050 /*11050*/ AND `locale`='deDE') OR (`entry`=6292 /*6292*/ AND `locale`='deDE') OR (`entry`=6034 /*6034*/ AND `locale`='deDE') OR (`entry`=11083 /*11083*/ AND `locale`='deDE') OR (`entry`=11081 /*11081*/ AND `locale`='deDE') OR (`entry`=6735 /*6735*/ AND `locale`='deDE') OR (`entry`=4212 /*4212*/ AND `locale`='deDE') OR (`entry`=4168 /*4168*/ AND `locale`='deDE') OR (`entry`=5047 /*5047*/ AND `locale`='deDE') OR (`entry`=4161 /*4161*/ AND `locale`='deDE') OR (`entry`=51504 /*51504*/ AND `locale`='deDE') OR (`entry`=5191 /*5191*/ AND `locale`='deDE') OR (`entry`=3469 /*3469*/ AND `locale`='deDE') OR (`entry`=8665 /*8665*/ AND `locale`='deDE') OR (`entry`=7555 /*7555*/ AND `locale`='deDE') OR (`entry`=7553 /*7553*/ AND `locale`='deDE') OR (`entry`=4241 /*4241*/ AND `locale`='deDE') OR (`entry`=4181 /*4181*/ AND `locale`='deDE') OR (`entry`=48623 /*48623*/ AND `locale`='deDE') OR (`entry`=4169 /*4169*/ AND `locale`='deDE') OR (`entry`=11709 /*11709*/ AND `locale`='deDE') OR (`entry`=4521 /*4521*/ AND `locale`='deDE') OR (`entry`=4180 /*4180*/ AND `locale`='deDE') OR (`entry`=4175 /*4175*/ AND `locale`='deDE') OR (`entry`=48735 /*48735*/ AND `locale`='deDE') OR (`entry`=50506 /*50506*/ AND `locale`='deDE') OR (`entry`=50502 /*50502*/ AND `locale`='deDE') OR (`entry`=50499 /*50499*/ AND `locale`='deDE') OR (`entry`=50498 /*50498*/ AND `locale`='deDE') OR (`entry`=36479 /*36479*/ AND `locale`='deDE') OR (`entry`=50518 /*50518*/ AND `locale`='deDE') OR (`entry`=50519 /*50519*/ AND `locale`='deDE') OR (`entry`=50520 /*50520*/ AND `locale`='deDE') OR (`entry`=50507 /*50507*/ AND `locale`='deDE') OR (`entry`=50516 /*50516*/ AND `locale`='deDE') OR (`entry`=50517 /*50517*/ AND `locale`='deDE') OR (`entry`=50521 /*50521*/ AND `locale`='deDE') OR (`entry`=50509 /*50509*/ AND `locale`='deDE') OR (`entry`=50505 /*50505*/ AND `locale`='deDE') OR (`entry`=50510 /*50510*/ AND `locale`='deDE') OR (`entry`=50508 /*50508*/ AND `locale`='deDE') OR (`entry`=50513 /*50513*/ AND `locale`='deDE') OR (`entry`=50501 /*50501*/ AND `locale`='deDE') OR (`entry`=50504 /*50504*/ AND `locale`='deDE') OR (`entry`=50500 /*50500*/ AND `locale`='deDE') OR (`entry`=50497 /*50497*/ AND `locale`='deDE') OR (`entry`=55285 /*55285*/ AND `locale`='deDE') OR (`entry`=4211 /*4211*/ AND `locale`='deDE') OR (`entry`=3468 /*3468*/ AND `locale`='deDE') OR (`entry`=55272 /*55272*/ AND `locale`='deDE') OR (`entry`=52642 /*52642*/ AND `locale`='deDE') OR (`entry`=52643 /*52643*/ AND `locale`='deDE') OR (`entry`=55273 /*55273*/ AND `locale`='deDE') OR (`entry`=52637 /*52637*/ AND `locale`='deDE') OR (`entry`=52636 /*52636*/ AND `locale`='deDE') OR (`entry`=4243 /*4243*/ AND `locale`='deDE') OR (`entry`=4138 /*4138*/ AND `locale`='deDE') OR (`entry`=9047 /*9047*/ AND `locale`='deDE') OR (`entry`=4244 /*4244*/ AND `locale`='deDE') OR (`entry`=4205 /*4205*/ AND `locale`='deDE') OR (`entry`=10089 /*10089*/ AND `locale`='deDE') OR (`entry`=4219 /*4219*/ AND `locale`='deDE') OR (`entry`=4218 /*4218*/ AND `locale`='deDE') OR (`entry`=7296 /*7296*/ AND `locale`='deDE') OR (`entry`=4217 /*4217*/ AND `locale`='deDE') OR (`entry`=4220 /*4220*/ AND `locale`='deDE') OR (`entry`=4146 /*4146*/ AND `locale`='deDE') OR (`entry`=10056 /*10056*/ AND `locale`='deDE') OR (`entry`=15598 /*15598*/ AND `locale`='deDE') OR (`entry`=3561 /*3561*/ AND `locale`='deDE') OR (`entry`=2796 /*2796*/ AND `locale`='deDE') OR (`entry`=4210 /*4210*/ AND `locale`='deDE') OR (`entry`=4223 /*4223*/ AND `locale`='deDE') OR (`entry`=14602 /*14602*/ AND `locale`='deDE') OR (`entry`=35168 /*35168*/ AND `locale`='deDE') OR (`entry`=12358 /*12358*/ AND `locale`='deDE') OR (`entry`=14556 /*14556*/ AND `locale`='deDE') OR (`entry`=12360 /*12360*/ AND `locale`='deDE') OR (`entry`=4730 /*4730*/ AND `locale`='deDE') OR (`entry`=4167 /*4167*/ AND `locale`='deDE') OR (`entry`=4753 /*4753*/ AND `locale`='deDE') OR (`entry`=14555 /*14555*/ AND `locale`='deDE') OR (`entry`=12359 /*12359*/ AND `locale`='deDE') OR (`entry`=4215 /*4215*/ AND `locale`='deDE') OR (`entry`=4163 /*4163*/ AND `locale`='deDE') OR (`entry`=5782 /*5782*/ AND `locale`='deDE') OR (`entry`=4423 /*4423*/ AND `locale`='deDE') OR (`entry`=4214 /*4214*/ AND `locale`='deDE') OR (`entry`=4156 /*4156*/ AND `locale`='deDE') OR (`entry`=4216 /*4216*/ AND `locale`='deDE') OR (`entry`=10878 /*10878*/ AND `locale`='deDE') OR (`entry`=4240 /*4240*/ AND `locale`='deDE') OR (`entry`=4177 /*4177*/ AND `locale`='deDE') OR (`entry`=2041 /*2041*/ AND `locale`='deDE') OR (`entry`=4236 /*4236*/ AND `locale`='deDE') OR (`entry`=4234 /*4234*/ AND `locale`='deDE') OR (`entry`=4172 /*4172*/ AND `locale`='deDE') OR (`entry`=4242 /*4242*/ AND `locale`='deDE') OR (`entry`=36506 /*36506*/ AND `locale`='deDE') OR (`entry`=47569 /*47569*/ AND `locale`='deDE') OR (`entry`=8396 /*8396*/ AND `locale`='deDE') OR (`entry`=4092 /*4092*/ AND `locale`='deDE') OR (`entry`=50715 /*50715*/ AND `locale`='deDE') OR (`entry`=50714 /*50714*/ AND `locale`='deDE') OR (`entry`=50690 /*50690*/ AND `locale`='deDE') OR (`entry`=2912 /*2912*/ AND `locale`='deDE') OR (`entry`=7740 /*7740*/ AND `locale`='deDE') OR (`entry`=61757 /*61757*/ AND `locale`='deDE') OR (`entry`=11401 /*11401*/ AND `locale`='deDE') OR (`entry`=52292 /*52292*/ AND `locale`='deDE') OR (`entry`=4091 /*4091*/ AND `locale`='deDE') OR (`entry`=35281 /*35281*/ AND `locale`='deDE') OR (`entry`=4204 /*4204*/ AND `locale`='deDE') OR (`entry`=43845 /*43845*/ AND `locale`='deDE') OR (`entry`=42936 /*42936*/ AND `locale`='deDE') OR (`entry`=17105 /*17105*/ AND `locale`='deDE') OR (`entry`=4165 /*4165*/ AND `locale`='deDE') OR (`entry`=4090 /*4090*/ AND `locale`='deDE') OR (`entry`=54116 /*54116*/ AND `locale`='deDE') OR (`entry`=7999 /*7999*/ AND `locale`='deDE') OR (`entry`=4221 /*4221*/ AND `locale`='deDE') OR (`entry`=62450 /*62450*/ AND `locale`='deDE') OR (`entry`=4222 /*4222*/ AND `locale`='deDE') OR (`entry`=15892 /*15892*/ AND `locale`='deDE') OR (`entry`=19173 /*19173*/ AND `locale`='deDE') OR (`entry`=14378 /*14378*/ AND `locale`='deDE') OR (`entry`=51371 /*51371*/ AND `locale`='deDE') OR (`entry`=50307 /*50307*/ AND `locale`='deDE') OR (`entry`=50305 /*50305*/ AND `locale`='deDE') OR (`entry`=40552 /*40552*/ AND `locale`='deDE') OR (`entry`=11700 /*11700*/ AND `locale`='deDE') OR (`entry`=7316 /*7316*/ AND `locale`='deDE') OR (`entry`=4209 /*4209*/ AND `locale`='deDE') OR (`entry`=4208 /*4208*/ AND `locale`='deDE') OR (`entry`=4155 /*4155*/ AND `locale`='deDE') OR (`entry`=3681 /*3681*/ AND `locale`='deDE') OR (`entry`=3562 /*3562*/ AND `locale`='deDE') OR (`entry`=10118 /*10118*/ AND `locale`='deDE') OR (`entry`=3607 /*3607*/ AND `locale`='deDE') OR (`entry`=7907 /*7907*/ AND `locale`='deDE') OR (`entry`=34295 /*34295*/ AND `locale`='deDE') OR (`entry`=3818 /*3818*/ AND `locale`='deDE') OR (`entry`=39254 /*39254*/ AND `locale`='deDE') OR (`entry`=11074 /*11074*/ AND `locale`='deDE') OR (`entry`=11914 /*11914*/ AND `locale`='deDE') OR (`entry`=11858 /*11858*/ AND `locale`='deDE') OR (`entry`=3411 /*3411*/ AND `locale`='deDE') OR (`entry`=4197 /*4197*/ AND `locale`='deDE') OR (`entry`=42023 /*42023*/ AND `locale`='deDE') OR (`entry`=12816 /*12816*/ AND `locale`='deDE') OR (`entry`=3995 /*3995*/ AND `locale`='deDE') OR (`entry`=35139 /*35139*/ AND `locale`='deDE') OR (`entry`=41370 /*41370*/ AND `locale`='deDE') OR (`entry`=50825 /*50825*/ AND `locale`='deDE') OR (`entry`=51062 /*51062*/ AND `locale`='deDE') OR (`entry`=41407 /*41407*/ AND `locale`='deDE') OR (`entry`=41350 /*41350*/ AND `locale`='deDE') OR (`entry`=11913 /*11913*/ AND `locale`='deDE') OR (`entry`=11910 /*11910*/ AND `locale`='deDE') OR (`entry`=41360 /*41360*/ AND `locale`='deDE') OR (`entry`=11912 /*11912*/ AND `locale`='deDE') OR (`entry`=11911 /*11911*/ AND `locale`='deDE') OR (`entry`=41230 /*41230*/ AND `locale`='deDE') OR (`entry`=11821 /*11821*/ AND `locale`='deDE') OR (`entry`=11915 /*11915*/ AND `locale`='deDE') OR (`entry`=4008 /*4008*/ AND `locale`='deDE') OR (`entry`=50759 /*50759*/ AND `locale`='deDE') OR (`entry`=61327 /*61327*/ AND `locale`='deDE') OR (`entry`=14881 /*14881*/ AND `locale`='deDE') OR (`entry`=62191 /*62191*/ AND `locale`='deDE') OR (`entry`=66335 /*66335*/ AND `locale`='deDE') OR (`entry`=66334 /*66334*/ AND `locale`='deDE') OR (`entry`=66333 /*66333*/ AND `locale`='deDE') OR (`entry`=66137 /*66137*/ AND `locale`='deDE') OR (`entry`=41186 /*41186*/ AND `locale`='deDE') OR (`entry`=41316 /*41316*/ AND `locale`='deDE') OR (`entry`=41311 /*41311*/ AND `locale`='deDE') OR (`entry`=41351 /*41351*/ AND `locale`='deDE') OR (`entry`=35136 /*35136*/ AND `locale`='deDE') OR (`entry`=41397 /*41397*/ AND `locale`='deDE') OR (`entry`=41278 /*41278*/ AND `locale`='deDE') OR (`entry`=41229 /*41229*/ AND `locale`='deDE') OR (`entry`=41286 /*41286*/ AND `locale`='deDE') OR (`entry`=41275 /*41275*/ AND `locale`='deDE') OR (`entry`=41274 /*41274*/ AND `locale`='deDE') OR (`entry`=42026 /*42026*/ AND `locale`='deDE') OR (`entry`=41361 /*41361*/ AND `locale`='deDE') OR (`entry`=41408 /*41408*/ AND `locale`='deDE') OR (`entry`=34843 /*34843*/ AND `locale`='deDE') OR (`entry`=38636 /*38636*/ AND `locale`='deDE') OR (`entry`=41282 /*41282*/ AND `locale`='deDE') OR (`entry`=41277 /*41277*/ AND `locale`='deDE') OR (`entry`=41276 /*41276*/ AND `locale`='deDE') OR (`entry`=41291 /*41291*/ AND `locale`='deDE') OR (`entry`=41318 /*41318*/ AND `locale`='deDE') OR (`entry`=41314 /*41314*/ AND `locale`='deDE') OR (`entry`=41315 /*41315*/ AND `locale`='deDE') OR (`entry`=41317 /*41317*/ AND `locale`='deDE') OR (`entry`=41283 /*41283*/ AND `locale`='deDE') OR (`entry`=41992 /*41992*/ AND `locale`='deDE') OR (`entry`=41991 /*41991*/ AND `locale`='deDE') OR (`entry`=41990 /*41990*/ AND `locale`='deDE') OR (`entry`=4489 /*4489*/ AND `locale`='deDE') OR (`entry`=51820 /*51820*/ AND `locale`='deDE') OR (`entry`=41854 /*41854*/ AND `locale`='deDE') OR (`entry`=43002 /*43002*/ AND `locale`='deDE') OR (`entry`=41676 /*41676*/ AND `locale`='deDE') OR (`entry`=41675 /*41675*/ AND `locale`='deDE') OR (`entry`=41674 /*41674*/ AND `locale`='deDE') OR (`entry`=40926 /*40926*/ AND `locale`='deDE') OR (`entry`=39257 /*39257*/ AND `locale`='deDE') OR (`entry`=39256 /*39256*/ AND `locale`='deDE') OR (`entry`=40993 /*40993*/ AND `locale`='deDE') OR (`entry`=34341 /*34341*/ AND `locale`='deDE') OR (`entry`=32254 /*32254*/ AND `locale`='deDE') OR (`entry`=41673 /*41673*/ AND `locale`='deDE') OR (`entry`=41064 /*41064*/ AND `locale`='deDE') OR (`entry`=40957 /*40957*/ AND `locale`='deDE') OR (`entry`=34353 /*34353*/ AND `locale`='deDE') OR (`entry`=34345 /*34345*/ AND `locale`='deDE') OR (`entry`=34834 /*34834*/ AND `locale`='deDE') OR (`entry`=34795 /*34795*/ AND `locale`='deDE') OR (`entry`=40901 /*40901*/ AND `locale`='deDE') OR (`entry`=41865 /*41865*/ AND `locale`='deDE') OR (`entry`=41864 /*41864*/ AND `locale`='deDE') OR (`entry`=40881 /*40881*/ AND `locale`='deDE') OR (`entry`=51827 /*51827*/ AND `locale`='deDE') OR (`entry`=41993 /*41993*/ AND `locale`='deDE') OR (`entry`=41989 /*41989*/ AND `locale`='deDE') OR (`entry`=41892 /*41892*/ AND `locale`='deDE') OR (`entry`=41890 /*41890*/ AND `locale`='deDE') OR (`entry`=40909 /*40909*/ AND `locale`='deDE') OR (`entry`=40907 /*40907*/ AND `locale`='deDE') OR (`entry`=41893 /*41893*/ AND `locale`='deDE') OR (`entry`=41891 /*41891*/ AND `locale`='deDE') OR (`entry`=42028 /*42028*/ AND `locale`='deDE') OR (`entry`=35140 /*35140*/ AND `locale`='deDE') OR (`entry`=41023 /*41023*/ AND `locale`='deDE') OR (`entry`=41677 /*41677*/ AND `locale`='deDE') OR (`entry`=40902 /*40902*/ AND `locale`='deDE') OR (`entry`=40903 /*40903*/ AND `locale`='deDE') OR (`entry`=41071 /*41071*/ AND `locale`='deDE') OR (`entry`=41062 /*41062*/ AND `locale`='deDE') OR (`entry`=41070 /*41070*/ AND `locale`='deDE') OR (`entry`=40973 /*40973*/ AND `locale`='deDE') OR (`entry`=40879 /*40879*/ AND `locale`='deDE') OR (`entry`=40984 /*40984*/ AND `locale`='deDE') OR (`entry`=40988 /*40988*/ AND `locale`='deDE') OR (`entry`=40904 /*40904*/ AND `locale`='deDE') OR (`entry`=40905 /*40905*/ AND `locale`='deDE') OR (`entry`=34832 /*34832*/ AND `locale`='deDE') OR (`entry`=40942 /*40942*/ AND `locale`='deDE') OR (`entry`=41937 /*41937*/ AND `locale`='deDE') OR (`entry`=41936 /*41936*/ AND `locale`='deDE') OR (`entry`=41935 /*41935*/ AND `locale`='deDE') OR (`entry`=42029 /*42029*/ AND `locale`='deDE') OR (`entry`=41987 /*41987*/ AND `locale`='deDE') OR (`entry`=41894 /*41894*/ AND `locale`='deDE') OR (`entry`=35204 /*35204*/ AND `locale`='deDE') OR (`entry`=35333 /*35333*/ AND `locale`='deDE') OR (`entry`=35137 /*35137*/ AND `locale`='deDE') OR (`entry`=40910 /*40910*/ AND `locale`='deDE') OR (`entry`=40908 /*40908*/ AND `locale`='deDE') OR (`entry`=40915 /*40915*/ AND `locale`='deDE') OR (`entry`=35203 /*35203*/ AND `locale`='deDE') OR (`entry`=35205 /*35205*/ AND `locale`='deDE') OR (`entry`=40900 /*40900*/ AND `locale`='deDE') OR (`entry`=41054 /*41054*/ AND `locale`='deDE') OR (`entry`=40899 /*40899*/ AND `locale`='deDE') OR (`entry`=43021 /*43021*/ AND `locale`='deDE') OR (`entry`=41053 /*41053*/ AND `locale`='deDE') OR (`entry`=40898 /*40898*/ AND `locale`='deDE') OR (`entry`=35334 /*35334*/ AND `locale`='deDE') OR (`entry`=41047 /*41047*/ AND `locale`='deDE') OR (`entry`=40897 /*40897*/ AND `locale`='deDE') OR (`entry`=40896 /*40896*/ AND `locale`='deDE') OR (`entry`=41039 /*41039*/ AND `locale`='deDE') OR (`entry`=50343 /*50343*/ AND `locale`='deDE') OR (`entry`=34866 /*34866*/ AND `locale`='deDE') OR (`entry`=4006 /*4006*/ AND `locale`='deDE') OR (`entry`=41185 /*41185*/ AND `locale`='deDE') OR (`entry`=4007 /*4007*/ AND `locale`='deDE') OR (`entry`=50812 /*50812*/ AND `locale`='deDE') OR (`entry`=12043 /*12043*/ AND `locale`='deDE') OR (`entry`=9549 /*9549*/ AND `locale`='deDE') OR (`entry`=5870 /*5870*/ AND `locale`='deDE') OR (`entry`=4312 /*4312*/ AND `locale`='deDE') OR (`entry`=4082 /*4082*/ AND `locale`='deDE') OR (`entry`=7731 /*7731*/ AND `locale`='deDE') OR (`entry`=10048 /*10048*/ AND `locale`='deDE') OR (`entry`=4083 /*4083*/ AND `locale`='deDE') OR (`entry`=50884 /*50884*/ AND `locale`='deDE') OR (`entry`=34946 /*34946*/ AND `locale`='deDE') OR (`entry`=34933 /*34933*/ AND `locale`='deDE') OR (`entry`=34934 /*34934*/ AND `locale`='deDE') OR (`entry`=34941 /*34941*/ AND `locale`='deDE') OR (`entry`=34940 /*34940*/ AND `locale`='deDE') OR (`entry`=34945 /*34945*/ AND `locale`='deDE') OR (`entry`=34939 /*34939*/ AND `locale`='deDE') OR (`entry`=34938 /*34938*/ AND `locale`='deDE') OR (`entry`=34937 /*34937*/ AND `locale`='deDE') OR (`entry`=34932 /*34932*/ AND `locale`='deDE') OR (`entry`=34931 /*34931*/ AND `locale`='deDE') OR (`entry`=42035 /*42035*/ AND `locale`='deDE') OR (`entry`=42033 /*42033*/ AND `locale`='deDE') OR (`entry`=42032 /*42032*/ AND `locale`='deDE') OR (`entry`=41434 /*41434*/ AND `locale`='deDE') OR (`entry`=41246 /*41246*/ AND `locale`='deDE') OR (`entry`=4014 /*4014*/ AND `locale`='deDE') OR (`entry`=4012 /*4012*/ AND `locale`='deDE') OR (`entry`=41441 /*41441*/ AND `locale`='deDE') OR (`entry`=41233 /*41233*/ AND `locale`='deDE') OR (`entry`=41432 /*41432*/ AND `locale`='deDE') OR (`entry`=41240 /*41240*/ AND `locale`='deDE') OR (`entry`=41437 /*41437*/ AND `locale`='deDE') OR (`entry`=40906 /*40906*/ AND `locale`='deDE') OR (`entry`=41475 /*41475*/ AND `locale`='deDE') OR (`entry`=5928 /*5928*/ AND `locale`='deDE') OR (`entry`=34818 /*34818*/ AND `locale`='deDE') OR (`entry`=34817 /*34817*/ AND `locale`='deDE') OR (`entry`=34807 /*34807*/ AND `locale`='deDE') OR (`entry`=35141 /*35141*/ AND `locale`='deDE') OR (`entry`=34803 /*34803*/ AND `locale`='deDE') OR (`entry`=42050 /*42050*/ AND `locale`='deDE') OR (`entry`=42047 /*42047*/ AND `locale`='deDE') OR (`entry`=42039 /*42039*/ AND `locale`='deDE') OR (`entry`=41870 /*41870*/ AND `locale`='deDE') OR (`entry`=42040 /*42040*/ AND `locale`='deDE') OR (`entry`=34805 /*34805*/ AND `locale`='deDE') OR (`entry`=34809 /*34809*/ AND `locale`='deDE') OR (`entry`=34811 /*34811*/ AND `locale`='deDE') OR (`entry`=41490 /*41490*/ AND `locale`='deDE') OR (`entry`=41493 /*41493*/ AND `locale`='deDE') OR (`entry`=43017 /*43017*/ AND `locale`='deDE') OR (`entry`=41491 /*41491*/ AND `locale`='deDE') OR (`entry`=41489 /*41489*/ AND `locale`='deDE') OR (`entry`=41488 /*41488*/ AND `locale`='deDE') OR (`entry`=41486 /*41486*/ AND `locale`='deDE') OR (`entry`=4057 /*4057*/ AND `locale`='deDE') OR (`entry`=4051 /*4051*/ AND `locale`='deDE') OR (`entry`=4053 /*4053*/ AND `locale`='deDE') OR (`entry`=4409 /*4409*/ AND `locale`='deDE') OR (`entry`=4052 /*4052*/ AND `locale`='deDE') OR (`entry`=4050 /*4050*/ AND `locale`='deDE') OR (`entry`=4061 /*4061*/ AND `locale`='deDE') OR (`entry`=5915 /*5915*/ AND `locale`='deDE') OR (`entry`=4067 /*4067*/ AND `locale`='deDE') OR (`entry`=34923 /*34923*/ AND `locale`='deDE') OR (`entry`=34969 /*34969*/ AND `locale`='deDE') OR (`entry`=4018 /*4018*/ AND `locale`='deDE') OR (`entry`=4019 /*4019*/ AND `locale`='deDE') OR (`entry`=61677 /*61677*/ AND `locale`='deDE') OR (`entry`=4017 /*4017*/ AND `locale`='deDE') OR (`entry`=34961 /*34961*/ AND `locale`='deDE') OR (`entry`=4021 /*4021*/ AND `locale`='deDE') OR (`entry`=4020 /*4020*/ AND `locale`='deDE') OR (`entry`=4407 /*4407*/ AND `locale`='deDE') OR (`entry`=34917 /*34917*/ AND `locale`='deDE') OR (`entry`=34968 /*34968*/ AND `locale`='deDE') OR (`entry`=41485 /*41485*/ AND `locale`='deDE') OR (`entry`=41482 /*41482*/ AND `locale`='deDE') OR (`entry`=41019 /*41019*/ AND `locale`='deDE') OR (`entry`=41418 /*41418*/ AND `locale`='deDE') OR (`entry`=41417 /*41417*/ AND `locale`='deDE') OR (`entry`=48687 /*48687*/ AND `locale`='deDE') OR (`entry`=44177 /*44177*/ AND `locale`='deDE') OR (`entry`=43019 /*43019*/ AND `locale`='deDE') OR (`entry`=35138 /*35138*/ AND `locale`='deDE') OR (`entry`=41638 /*41638*/ AND `locale`='deDE') OR (`entry`=41637 /*41637*/ AND `locale`='deDE') OR (`entry`=41635 /*41635*/ AND `locale`='deDE') OR (`entry`=41664 /*41664*/ AND `locale`='deDE') OR (`entry`=41487 /*41487*/ AND `locale`='deDE') OR (`entry`=41651 /*41651*/ AND `locale`='deDE') OR (`entry`=41627 /*41627*/ AND `locale`='deDE') OR (`entry`=35152 /*35152*/ AND `locale`='deDE') OR (`entry`=35173 /*35173*/ AND `locale`='deDE') OR (`entry`=35154 /*35154*/ AND `locale`='deDE') OR (`entry`=62189 /*62189*/ AND `locale`='deDE') OR (`entry`=35153 /*35153*/ AND `locale`='deDE') OR (`entry`=42101 /*42101*/ AND `locale`='deDE') OR (`entry`=42091 /*42091*/ AND `locale`='deDE') OR (`entry`=50895 /*50895*/ AND `locale`='deDE') OR (`entry`=23409 /*23409*/ AND `locale`='deDE') OR (`entry`=42038 /*42038*/ AND `locale`='deDE') OR (`entry`=41988 /*41988*/ AND `locale`='deDE') OR (`entry`=35160 /*35160*/ AND `locale`='deDE') OR (`entry`=41528 /*41528*/ AND `locale`='deDE') OR (`entry`=35159 /*35159*/ AND `locale`='deDE') OR (`entry`=35174 /*35174*/ AND `locale`='deDE') OR (`entry`=35161 /*35161*/ AND `locale`='deDE') OR (`entry`=35158 /*35158*/ AND `locale`='deDE') OR (`entry`=35151 /*35151*/ AND `locale`='deDE') OR (`entry`=62190 /*62190*/ AND `locale`='deDE') OR (`entry`=34894 /*34894*/ AND `locale`='deDE') OR (`entry`=49724 /*49724*/ AND `locale`='deDE') OR (`entry`=34896 /*34896*/ AND `locale`='deDE') OR (`entry`=34911 /*34911*/ AND `locale`='deDE') OR (`entry`=34898 /*34898*/ AND `locale`='deDE') OR (`entry`=34897 /*34897*/ AND `locale`='deDE') OR (`entry`=49779 /*49779*/ AND `locale`='deDE') OR (`entry`=35163 /*35163*/ AND `locale`='deDE') OR (`entry`=35150 /*35150*/ AND `locale`='deDE') OR (`entry`=7730 /*7730*/ AND `locale`='deDE') OR (`entry`=31890 /*31890*/ AND `locale`='deDE') OR (`entry`=5930 /*5930*/ AND `locale`='deDE') OR (`entry`=15169 /*15169*/ AND `locale`='deDE') OR (`entry`=11804 /*11804*/ AND `locale`='deDE') OR (`entry`=15200 /*15200*/ AND `locale`='deDE') OR (`entry`=14478 /*14478*/ AND `locale`='deDE') OR (`entry`=14347 /*14347*/ AND `locale`='deDE') OR (`entry`=15570 /*15570*/ AND `locale`='deDE') OR (`entry`=62184 /*62184*/ AND `locale`='deDE') OR (`entry`=50481 /*50481*/ AND `locale`='deDE') OR (`entry`=14474 /*14474*/ AND `locale`='deDE') OR (`entry`=50897 /*50897*/ AND `locale`='deDE') OR (`entry`=15610 /*15610*/ AND `locale`='deDE') OR (`entry`=11726 /*11726*/ AND `locale`='deDE') OR (`entry`=50742 /*50742*/ AND `locale`='deDE') OR (`entry`=50745 /*50745*/ AND `locale`='deDE') OR (`entry`=11727 /*11727*/ AND `locale`='deDE') OR (`entry`=11725 /*11725*/ AND `locale`='deDE') OR (`entry`=11729 /*11729*/ AND `locale`='deDE') OR (`entry`=11728 /*11728*/ AND `locale`='deDE') OR (`entry`=15444 /*15444*/ AND `locale`='deDE') OR (`entry`=15443 /*15443*/ AND `locale`='deDE') OR (`entry`=17765 /*17765*/ AND `locale`='deDE') OR (`entry`=17090 /*17090*/ AND `locale`='deDE') OR (`entry`=17080 /*17080*/ AND `locale`='deDE') OR (`entry`=17068 /*17068*/ AND `locale`='deDE') OR (`entry`=15903 /*15903*/ AND `locale`='deDE') OR (`entry`=15442 /*15442*/ AND `locale`='deDE') OR (`entry`=15441 /*15441*/ AND `locale`='deDE') OR (`entry`=11723 /*11723*/ AND `locale`='deDE') OR (`entry`=51375 /*51375*/ AND `locale`='deDE') OR (`entry`=15611 /*15611*/ AND `locale`='deDE') OR (`entry`=14475 /*14475*/ AND `locale`='deDE') OR (`entry`=11721 /*11721*/ AND `locale`='deDE') OR (`entry`=50744 /*50744*/ AND `locale`='deDE') OR (`entry`=50370 /*50370*/ AND `locale`='deDE') OR (`entry`=51752 /*51752*/ AND `locale`='deDE') OR (`entry`=6491 /*6491*/ AND `locale`='deDE') OR (`entry`=15202 /*15202*/ AND `locale`='deDE') OR (`entry`=15201 /*15201*/ AND `locale`='deDE') OR (`entry`=15213 /*15213*/ AND `locale`='deDE') OR (`entry`=11744 /*11744*/ AND `locale`='deDE') OR (`entry`=14476 /*14476*/ AND `locale`='deDE') OR (`entry`=15542 /*15542*/ AND `locale`='deDE') OR (`entry`=15541 /*15541*/ AND `locale`='deDE') OR (`entry`=13220 /*13220*/ AND `locale`='deDE') OR (`entry`=12956 /*12956*/ AND `locale`='deDE') OR (`entry`=11805 /*11805*/ AND `locale`='deDE') OR (`entry`=11722 /*11722*/ AND `locale`='deDE') OR (`entry`=15182 /*15182*/ AND `locale`='deDE') OR (`entry`=17082 /*17082*/ AND `locale`='deDE') OR (`entry`=15177 /*15177*/ AND `locale`='deDE') OR (`entry`=50588 /*50588*/ AND `locale`='deDE') OR (`entry`=15181 /*15181*/ AND `locale`='deDE') OR (`entry`=50587 /*50587*/ AND `locale`='deDE') OR (`entry`=15179 /*15179*/ AND `locale`='deDE') OR (`entry`=15540 /*15540*/ AND `locale`='deDE') OR (`entry`=17081 /*17081*/ AND `locale`='deDE') OR (`entry`=15722 /*15722*/ AND `locale`='deDE') OR (`entry`=15180 /*15180*/ AND `locale`='deDE') OR (`entry`=15178 /*15178*/ AND `locale`='deDE') OR (`entry`=15175 /*15175*/ AND `locale`='deDE') OR (`entry`=17074 /*17074*/ AND `locale`='deDE') OR (`entry`=15306 /*15306*/ AND `locale`='deDE') OR (`entry`=15270 /*15270*/ AND `locale`='deDE') OR (`entry`=15599 /*15599*/ AND `locale`='deDE') OR (`entry`=15183 /*15183*/ AND `locale`='deDE') OR (`entry`=15191 /*15191*/ AND `locale`='deDE') OR (`entry`=15176 /*15176*/ AND `locale`='deDE') OR (`entry`=15282 /*15282*/ AND `locale`='deDE') OR (`entry`=15190 /*15190*/ AND `locale`='deDE') OR (`entry`=15189 /*15189*/ AND `locale`='deDE') OR (`entry`=15614 /*15614*/ AND `locale`='deDE') OR (`entry`=16543 /*16543*/ AND `locale`='deDE') OR (`entry`=11740 /*11740*/ AND `locale`='deDE') OR (`entry`=11738 /*11738*/ AND `locale`='deDE') OR (`entry`=11735 /*11735*/ AND `locale`='deDE') OR (`entry`=16091 /*16091*/ AND `locale`='deDE') OR (`entry`=15419 /*15419*/ AND `locale`='deDE') OR (`entry`=15184 /*15184*/ AND `locale`='deDE') OR (`entry`=15174 /*15174*/ AND `locale`='deDE') OR (`entry`=11698 /*11698*/ AND `locale`='deDE') OR (`entry`=15293 /*15293*/ AND `locale`='deDE') OR (`entry`=12199 /*12199*/ AND `locale`='deDE') OR (`entry`=11724 /*11724*/ AND `locale`='deDE') OR (`entry`=61441 /*61441*/ AND `locale`='deDE') OR (`entry`=11746 /*11746*/ AND `locale`='deDE') OR (`entry`=12179 /*12179*/ AND `locale`='deDE') OR (`entry`=12178 /*12178*/ AND `locale`='deDE') OR (`entry`=15194 /*15194*/ AND `locale`='deDE') OR (`entry`=50743 /*50743*/ AND `locale`='deDE') OR (`entry`=15609 /*15609*/ AND `locale`='deDE') OR (`entry`=11730 /*11730*/ AND `locale`='deDE') OR (`entry`=14473 /*14473*/ AND `locale`='deDE') OR (`entry`=15545 /*15545*/ AND `locale`='deDE') OR (`entry`=11736 /*11736*/ AND `locale`='deDE') OR (`entry`=11741 /*11741*/ AND `locale`='deDE') OR (`entry`=15616 /*15616*/ AND `locale`='deDE') OR (`entry`=15613 /*15613*/ AND `locale`='deDE') OR (`entry`=15612 /*15612*/ AND `locale`='deDE') OR (`entry`=18199 /*18199*/ AND `locale`='deDE') OR (`entry`=15617 /*15617*/ AND `locale`='deDE') OR (`entry`=15615 /*15615*/ AND `locale`='deDE') OR (`entry`=17766 /*17766*/ AND `locale`='deDE') OR (`entry`=17079 /*17079*/ AND `locale`='deDE') OR (`entry`=17070 /*17070*/ AND `locale`='deDE') OR (`entry`=61319 /*61319*/ AND `locale`='deDE') OR (`entry`=11745 /*11745*/ AND `locale`='deDE') OR (`entry`=15308 /*15308*/ AND `locale`='deDE') OR (`entry`=61326 /*61326*/ AND `locale`='deDE') OR (`entry`=49840 /*49840*/ AND `locale`='deDE') OR (`entry`=11803 /*11803*/ AND `locale`='deDE') OR (`entry`=11882 /*11882*/ AND `locale`='deDE') OR (`entry`=11883 /*11883*/ AND `locale`='deDE') OR (`entry`=11881 /*11881*/ AND `locale`='deDE') OR (`entry`=11880 /*11880*/ AND `locale`='deDE') OR (`entry`=15693 /*15693*/ AND `locale`='deDE') OR (`entry`=15500 /*15500*/ AND `locale`='deDE') OR (`entry`=15499 /*15499*/ AND `locale`='deDE') OR (`entry`=15498 /*15498*/ AND `locale`='deDE') OR (`entry`=5785 /*5785*/ AND `locale`='deDE') OR (`entry`=3051 /*3051*/ AND `locale`='deDE') OR (`entry`=43613 /*43613*/ AND `locale`='deDE') OR (`entry`=34766 /*34766*/ AND `locale`='deDE') OR (`entry`=3419 /*3419*/ AND `locale`='deDE') OR (`entry`=3049 /*3049*/ AND `locale`='deDE') OR (`entry`=51640 /*51640*/ AND `locale`='deDE') OR (`entry`=3045 /*3045*/ AND `locale`='deDE') OR (`entry`=5906 /*5906*/ AND `locale`='deDE') OR (`entry`=30724 /*30724*/ AND `locale`='deDE') OR (`entry`=30709 /*30709*/ AND `locale`='deDE') OR (`entry`=5543 /*5543*/ AND `locale`='deDE') OR (`entry`=3447 /*3447*/ AND `locale`='deDE') OR (`entry`=3046 /*3046*/ AND `locale`='deDE') OR (`entry`=2798 /*2798*/ AND `locale`='deDE') OR (`entry`=3016 /*3016*/ AND `locale`='deDE') OR (`entry`=3009 /*3009*/ AND `locale`='deDE') OR (`entry`=3010 /*3010*/ AND `locale`='deDE') OR (`entry`=96470 /*96470*/ AND `locale`='deDE') OR (`entry`=11047 /*11047*/ AND `locale`='deDE') OR (`entry`=3014 /*3014*/ AND `locale`='deDE') OR (`entry`=47589 /*47589*/ AND `locale`='deDE') OR (`entry`=11071 /*11071*/ AND `locale`='deDE') OR (`entry`=3012 /*3012*/ AND `locale`='deDE') OR (`entry`=3011 /*3011*/ AND `locale`='deDE') OR (`entry`=11084 /*11084*/ AND `locale`='deDE') OR (`entry`=11051 /*11051*/ AND `locale`='deDE') OR (`entry`=7089 /*7089*/ AND `locale`='deDE') OR (`entry`=3093 /*3093*/ AND `locale`='deDE') OR (`entry`=3050 /*3050*/ AND `locale`='deDE') OR (`entry`=3007 /*3007*/ AND `locale`='deDE') OR (`entry`=3008 /*3008*/ AND `locale`='deDE') OR (`entry`=3092 /*3092*/ AND `locale`='deDE') OR (`entry`=3004 /*3004*/ AND `locale`='deDE') OR (`entry`=3005 /*3005*/ AND `locale`='deDE') OR (`entry`=3095 /*3095*/ AND `locale`='deDE') OR (`entry`=3978 /*3978*/ AND `locale`='deDE') OR (`entry`=52658 /*52658*/ AND `locale`='deDE') OR (`entry`=52657 /*52657*/ AND `locale`='deDE') OR (`entry`=3002 /*3002*/ AND `locale`='deDE') OR (`entry`=8722 /*8722*/ AND `locale`='deDE') OR (`entry`=3001 /*3001*/ AND `locale`='deDE') OR (`entry`=8674 /*8674*/ AND `locale`='deDE') OR (`entry`=52651 /*52651*/ AND `locale`='deDE') OR (`entry`=52655 /*52655*/ AND `locale`='deDE') OR (`entry`=51503 /*51503*/ AND `locale`='deDE') OR (`entry`=10278 /*10278*/ AND `locale`='deDE') OR (`entry`=8361 /*8361*/ AND `locale`='deDE') OR (`entry`=6410 /*6410*/ AND `locale`='deDE') OR (`entry`=5189 /*5189*/ AND `locale`='deDE') OR (`entry`=5054 /*5054*/ AND `locale`='deDE') OR (`entry`=3003 /*3003*/ AND `locale`='deDE') OR (`entry`=2999 /*2999*/ AND `locale`='deDE') OR (`entry`=2998 /*2998*/ AND `locale`='deDE') OR (`entry`=8362 /*8362*/ AND `locale`='deDE') OR (`entry`=8364 /*8364*/ AND `locale`='deDE') OR (`entry`=8363 /*8363*/ AND `locale`='deDE') OR (`entry`=2997 /*2997*/ AND `locale`='deDE') OR (`entry`=3042 /*3042*/ AND `locale`='deDE') OR (`entry`=3039 /*3039*/ AND `locale`='deDE') OR (`entry`=31144 /*31144*/ AND `locale`='deDE') OR (`entry`=10086 /*10086*/ AND `locale`='deDE') OR (`entry`=3043 /*3043*/ AND `locale`='deDE') OR (`entry`=3041 /*3041*/ AND `locale`='deDE') OR (`entry`=3040 /*3040*/ AND `locale`='deDE') OR (`entry`=3038 /*3038*/ AND `locale`='deDE') OR (`entry`=32667 /*32667*/ AND `locale`='deDE') OR (`entry`=32666 /*32666*/ AND `locale`='deDE') OR (`entry`=31146 /*31146*/ AND `locale`='deDE') OR (`entry`=6393 /*6393*/ AND `locale`='deDE') OR (`entry`=4943 /*4943*/ AND `locale`='deDE') OR (`entry`=34978 /*34978*/ AND `locale`='deDE') OR (`entry`=34976 /*34976*/ AND `locale`='deDE') OR (`entry`=5599 /*5599*/ AND `locale`='deDE') OR (`entry`=49750 /*49750*/ AND `locale`='deDE') OR (`entry`=14441 /*14441*/ AND `locale`='deDE') OR (`entry`=10881 /*10881*/ AND `locale`='deDE') OR (`entry`=6746 /*6746*/ AND `locale`='deDE') OR (`entry`=10054 /*10054*/ AND `locale`='deDE') OR (`entry`=3013 /*3013*/ AND `locale`='deDE') OR (`entry`=3017 /*3017*/ AND `locale`='deDE') OR (`entry`=8360 /*8360*/ AND `locale`='deDE') OR (`entry`=8358 /*8358*/ AND `locale`='deDE') OR (`entry`=8359 /*8359*/ AND `locale`='deDE') OR (`entry`=3015 /*3015*/ AND `locale`='deDE') OR (`entry`=50483 /*50483*/ AND `locale`='deDE') OR (`entry`=2995 /*2995*/ AND `locale`='deDE') OR (`entry`=8357 /*8357*/ AND `locale`='deDE') OR (`entry`=8356 /*8356*/ AND `locale`='deDE') OR (`entry`=2996 /*2996*/ AND `locale`='deDE') OR (`entry`=3022 /*3022*/ AND `locale`='deDE') OR (`entry`=3026 /*3026*/ AND `locale`='deDE') OR (`entry`=8398 /*8398*/ AND `locale`='deDE') OR (`entry`=3027 /*3027*/ AND `locale`='deDE') OR (`entry`=3023 /*3023*/ AND `locale`='deDE') OR (`entry`=3021 /*3021*/ AND `locale`='deDE') OR (`entry`=3020 /*3020*/ AND `locale`='deDE') OR (`entry`=14440 /*14440*/ AND `locale`='deDE') OR (`entry`=12383 /*12383*/ AND `locale`='deDE') OR (`entry`=8401 /*8401*/ AND `locale`='deDE') OR (`entry`=3019 /*3019*/ AND `locale`='deDE') OR (`entry`=3018 /*3018*/ AND `locale`='deDE') OR (`entry`=15164 /*15164*/ AND `locale`='deDE') OR (`entry`=3025 /*3025*/ AND `locale`='deDE') OR (`entry`=4721 /*4721*/ AND `locale`='deDE') OR (`entry`=3024 /*3024*/ AND `locale`='deDE') OR (`entry`=3029 /*3029*/ AND `locale`='deDE') OR (`entry`=3083 /*3083*/ AND `locale`='deDE') OR (`entry`=3028 /*3028*/ AND `locale`='deDE') OR (`entry`=14442 /*14442*/ AND `locale`='deDE') OR (`entry`=3037 /*3037*/ AND `locale`='deDE') OR (`entry`=9087 /*9087*/ AND `locale`='deDE') OR (`entry`=15898 /*15898*/ AND `locale`='deDE') OR (`entry`=15895 /*15895*/ AND `locale`='deDE') OR (`entry`=15893 /*15893*/ AND `locale`='deDE') OR (`entry`=15894 /*15894*/ AND `locale`='deDE') OR (`entry`=15897 /*15897*/ AND `locale`='deDE') OR (`entry`=47572 /*47572*/ AND `locale`='deDE') OR (`entry`=15580 /*15580*/ AND `locale`='deDE') OR (`entry`=43870 /*43870*/ AND `locale`='deDE') OR (`entry`=5770 /*5770*/ AND `locale`='deDE') OR (`entry`=3036 /*3036*/ AND `locale`='deDE') OR (`entry`=5769 /*5769*/ AND `locale`='deDE') OR (`entry`=3034 /*3034*/ AND `locale`='deDE') OR (`entry`=3033 /*3033*/ AND `locale`='deDE') OR (`entry`=43796 /*43796*/ AND `locale`='deDE') OR (`entry`=43004 /*43004*/ AND `locale`='deDE') OR (`entry`=9076 /*9076*/ AND `locale`='deDE') OR (`entry`=36832 /*36832*/ AND `locale`='deDE') OR (`entry`=37024 /*37024*/ AND `locale`='deDE') OR (`entry`=36648 /*36648*/ AND `locale`='deDE') OR (`entry`=51143 /*51143*/ AND `locale`='deDE') OR (`entry`=3441 /*3441*/ AND `locale`='deDE') OR (`entry`=43795 /*43795*/ AND `locale`='deDE') OR (`entry`=43001 /*43001*/ AND `locale`='deDE') OR (`entry`=8664 /*8664*/ AND `locale`='deDE') OR (`entry`=51372 /*51372*/ AND `locale`='deDE') OR (`entry`=36828 /*36828*/ AND `locale`='deDE') OR (`entry`=36827 /*36827*/ AND `locale`='deDE') OR (`entry`=43975 /*43975*/ AND `locale`='deDE') OR (`entry`=43974 /*43974*/ AND `locale`='deDE') OR (`entry`=43973 /*43973*/ AND `locale`='deDE') OR (`entry`=3068 /*3068*/ AND `locale`='deDE') OR (`entry`=5787 /*5787*/ AND `locale`='deDE') OR (`entry`=36825 /*36825*/ AND `locale`='deDE') OR (`entry`=5786 /*5786*/ AND `locale`='deDE') OR (`entry`=2973 /*2973*/ AND `locale`='deDE') OR (`entry`=51515 /*51515*/ AND `locale`='deDE') OR (`entry`=23618 /*23618*/ AND `locale`='deDE') OR (`entry`=15575 /*15575*/ AND `locale`='deDE') OR (`entry`=3219 /*3219*/ AND `locale`='deDE') OR (`entry`=12355 /*12355*/ AND `locale`='deDE') OR (`entry`=34155 /*34155*/ AND `locale`='deDE') OR (`entry`=63068 /*63068*/ AND `locale`='deDE') OR (`entry`=63067 /*63067*/ AND `locale`='deDE') OR (`entry`=49996 /*49996*/ AND `locale`='deDE') OR (`entry`=3215 /*3215*/ AND `locale`='deDE') OR (`entry`=51637 /*51637*/ AND `locale`='deDE') OR (`entry`=3064 /*3064*/ AND `locale`='deDE') OR (`entry`=23616 /*23616*/ AND `locale`='deDE') OR (`entry`=3218 /*3218*/ AND `locale`='deDE') OR (`entry`=43015 /*43015*/ AND `locale`='deDE') OR (`entry`=43013 /*43013*/ AND `locale`='deDE') OR (`entry`=2947 /*2947*/ AND `locale`='deDE') OR (`entry`=10600 /*10600*/ AND `locale`='deDE') OR (`entry`=3217 /*3217*/ AND `locale`='deDE') OR (`entry`=10721 /*10721*/ AND `locale`='deDE') OR (`entry`=10599 /*10599*/ AND `locale`='deDE') OR (`entry`=3063 /*3063*/ AND `locale`='deDE') OR (`entry`=3221 /*3221*/ AND `locale`='deDE') OR (`entry`=2948 /*2948*/ AND `locale`='deDE') OR (`entry`=3223 /*3223*/ AND `locale`='deDE') OR (`entry`=3884 /*3884*/ AND `locale`='deDE') OR (`entry`=2985 /*2985*/ AND `locale`='deDE') OR (`entry`=5938 /*5938*/ AND `locale`='deDE') OR (`entry`=36644 /*36644*/ AND `locale`='deDE') OR (`entry`=11944 /*11944*/ AND `locale`='deDE') OR (`entry`=6747 /*6747*/ AND `locale`='deDE') OR (`entry`=5939 /*5939*/ AND `locale`='deDE') OR (`entry`=3222 /*3222*/ AND `locale`='deDE') OR (`entry`=10050 /*10050*/ AND `locale`='deDE') OR (`entry`=6776 /*6776*/ AND `locale`='deDE') OR (`entry`=40809 /*40809*/ AND `locale`='deDE') OR (`entry`=14549 /*14549*/ AND `locale`='deDE') OR (`entry`=14542 /*14542*/ AND `locale`='deDE') OR (`entry`=3690 /*3690*/ AND `locale`='deDE') OR (`entry`=12354 /*12354*/ AND `locale`='deDE') OR (`entry`=14550 /*14550*/ AND `locale`='deDE') OR (`entry`=11407 /*11407*/ AND `locale`='deDE') OR (`entry`=3685 /*3685*/ AND `locale`='deDE') OR (`entry`=3080 /*3080*/ AND `locale`='deDE') OR (`entry`=3079 /*3079*/ AND `locale`='deDE') OR (`entry`=3078 /*3078*/ AND `locale`='deDE') OR (`entry`=3077 /*3077*/ AND `locale`='deDE') OR (`entry`=3067 /*3067*/ AND `locale`='deDE') OR (`entry`=3224 /*3224*/ AND `locale`='deDE') OR (`entry`=53561 /*53561*/ AND `locale`='deDE') OR (`entry`=47419 /*47419*/ AND `locale`='deDE') OR (`entry`=6290 /*6290*/ AND `locale`='deDE') OR (`entry`=5940 /*5940*/ AND `locale`='deDE') OR (`entry`=3076 /*3076*/ AND `locale`='deDE') OR (`entry`=3069 /*3069*/ AND `locale`='deDE') OR (`entry`=3055 /*3055*/ AND `locale`='deDE') OR (`entry`=3212 /*3212*/ AND `locale`='deDE') OR (`entry`=3081 /*3081*/ AND `locale`='deDE') OR (`entry`=3688 /*3688*/ AND `locale`='deDE') OR (`entry`=3065 /*3065*/ AND `locale`='deDE') OR (`entry`=3054 /*3054*/ AND `locale`='deDE') OR (`entry`=36803 /*36803*/ AND `locale`='deDE') OR (`entry`=43599 /*43599*/ AND `locale`='deDE') OR (`entry`=44927 /*44927*/ AND `locale`='deDE') OR (`entry`=36694 /*36694*/ AND `locale`='deDE') OR (`entry`=106 /*106*/ AND `locale`='deDE') OR (`entry`=2980 /*2980*/ AND `locale`='deDE') OR (`entry`=3213 /*3213*/ AND `locale`='deDE') OR (`entry`=3214 /*3214*/ AND `locale`='deDE') OR (`entry`=3883 /*3883*/ AND `locale`='deDE') OR (`entry`=3210 /*3210*/ AND `locale`='deDE') OR (`entry`=3075 /*3075*/ AND `locale`='deDE') OR (`entry`=3073 /*3073*/ AND `locale`='deDE') OR (`entry`=3209 /*3209*/ AND `locale`='deDE') OR (`entry`=3074 /*3074*/ AND `locale`='deDE') OR (`entry`=63327 /*63327*/ AND `locale`='deDE') OR (`entry`=38438 /*38438*/ AND `locale`='deDE') OR (`entry`=37737 /*37737*/ AND `locale`='deDE') OR (`entry`=37724 /*37724*/ AND `locale`='deDE') OR (`entry`=3059 /*3059*/ AND `locale`='deDE') OR (`entry`=2981 /*2981*/ AND `locale`='deDE') OR (`entry`=89713 /*89713*/ AND `locale`='deDE') OR (`entry`=3211 /*3211*/ AND `locale`='deDE') OR (`entry`=3062 /*3062*/ AND `locale`='deDE') OR (`entry`=3061 /*3061*/ AND `locale`='deDE') OR (`entry`=3060 /*3060*/ AND `locale`='deDE') OR (`entry`=45199 /*45199*/ AND `locale`='deDE') OR (`entry`=44848 /*44848*/ AND `locale`='deDE') OR (`entry`=3072 /*3072*/ AND `locale`='deDE') OR (`entry`=38345 /*38345*/ AND `locale`='deDE') OR (`entry`=36943 /*36943*/ AND `locale`='deDE') OR (`entry`=2966 /*2966*/ AND `locale`='deDE') OR (`entry`=2952 /*2952*/ AND `locale`='deDE') OR (`entry`=36727 /*36727*/ AND `locale`='deDE') OR (`entry`=36708 /*36708*/ AND `locale`='deDE') OR (`entry`=36697 /*36697*/ AND `locale`='deDE') OR (`entry`=37156 /*37156*/ AND `locale`='deDE') OR (`entry`=37155 /*37155*/ AND `locale`='deDE') OR (`entry`=36696 /*36696*/ AND `locale`='deDE') OR (`entry`=43720 /*43720*/ AND `locale`='deDE') OR (`entry`=36712 /*36712*/ AND `locale`='deDE') OR (`entry`=36942 /*36942*/ AND `locale`='deDE') OR (`entry`=7975 /*7975*/ AND `locale`='deDE') OR (`entry`=2955 /*2955*/ AND `locale`='deDE') OR (`entry`=2951 /*2951*/ AND `locale`='deDE') OR (`entry`=2950 /*2950*/ AND `locale`='deDE') OR (`entry`=2949 /*2949*/ AND `locale`='deDE') OR (`entry`=2962 /*2962*/ AND `locale`='deDE') OR (`entry`=2963 /*2963*/ AND `locale`='deDE') OR (`entry`=62462 /*62462*/ AND `locale`='deDE') OR (`entry`=51639 /*51639*/ AND `locale`='deDE') OR (`entry`=51638 /*51638*/ AND `locale`='deDE') OR (`entry`=3044 /*3044*/ AND `locale`='deDE') OR (`entry`=5957 /*5957*/ AND `locale`='deDE') OR (`entry`=3047 /*3047*/ AND `locale`='deDE') OR (`entry`=43892 /*43892*/ AND `locale`='deDE') OR (`entry`=43883 /*43883*/ AND `locale`='deDE') OR (`entry`=3048 /*3048*/ AND `locale`='deDE') OR (`entry`=43881 /*43881*/ AND `locale`='deDE') OR (`entry`=37185 /*37185*/ AND `locale`='deDE') OR (`entry`=37058 /*37058*/ AND `locale`='deDE') OR (`entry`=36931 /*36931*/ AND `locale`='deDE') OR (`entry`=37175 /*37175*/ AND `locale`='deDE') OR (`entry`=37178 /*37178*/ AND `locale`='deDE') OR (`entry`=2965 /*2965*/ AND `locale`='deDE') OR (`entry`=3233 /*3233*/ AND `locale`='deDE') OR (`entry`=3052 /*3052*/ AND `locale`='deDE') OR (`entry`=10636 /*10636*/ AND `locale`='deDE') OR (`entry`=2987 /*2987*/ AND `locale`='deDE') OR (`entry`=5807 /*5807*/ AND `locale`='deDE') OR (`entry`=2964 /*2964*/ AND `locale`='deDE') OR (`entry`=2977 /*2977*/ AND `locale`='deDE') OR (`entry`=2976 /*2976*/ AND `locale`='deDE') OR (`entry`=2988 /*2988*/ AND `locale`='deDE') OR (`entry`=2958 /*2958*/ AND `locale`='deDE') OR (`entry`=2970 /*2970*/ AND `locale`='deDE') OR (`entry`=2972 /*2972*/ AND `locale`='deDE') OR (`entry`=2974 /*2974*/ AND `locale`='deDE') OR (`entry`=2979 /*2979*/ AND `locale`='deDE') OR (`entry`=2978 /*2978*/ AND `locale`='deDE') OR (`entry`=2969 /*2969*/ AND `locale`='deDE') OR (`entry`=2956 /*2956*/ AND `locale`='deDE') OR (`entry`=2959 /*2959*/ AND `locale`='deDE') OR (`entry`=6271 /*6271*/ AND `locale`='deDE') OR (`entry`=2971 /*2971*/ AND `locale`='deDE') OR (`entry`=61143 /*61143*/ AND `locale`='deDE') OR (`entry`=2957 /*2957*/ AND `locale`='deDE') OR (`entry`=3566 /*3566*/ AND `locale`='deDE') OR (`entry`=62176 /*62176*/ AND `locale`='deDE') OR (`entry`=2960 /*2960*/ AND `locale`='deDE') OR (`entry`=34281 /*34281*/ AND `locale`='deDE') OR (`entry`=2994 /*2994*/ AND `locale`='deDE') OR (`entry`=3232 /*3232*/ AND `locale`='deDE') OR (`entry`=40474 /*40474*/ AND `locale`='deDE') OR (`entry`=44386 /*44386*/ AND `locale`='deDE') OR (`entry`=44384 /*44384*/ AND `locale`='deDE') OR (`entry`=40473 /*40473*/ AND `locale`='deDE') OR (`entry`=40467 /*40467*/ AND `locale`='deDE') OR (`entry`=39377 /*39377*/ AND `locale`='deDE') OR (`entry`=7776 /*7776*/ AND `locale`='deDE') OR (`entry`=14227 /*14227*/ AND `locale`='deDE') OR (`entry`=7775 /*7775*/ AND `locale`='deDE') OR (`entry`=5349 /*5349*/ AND `locale`='deDE') OR (`entry`=40972 /*40972*/ AND `locale`='deDE') OR (`entry`=39339 /*39339*/ AND `locale`='deDE') OR (`entry`=5366 /*5366*/ AND `locale`='deDE') OR (`entry`=8075 /*8075*/ AND `locale`='deDE') OR (`entry`=5362 /*5362*/ AND `locale`='deDE') OR (`entry`=50532 /*50532*/ AND `locale`='deDE') OR (`entry`=50544 /*50544*/ AND `locale`='deDE') OR (`entry`=50542 /*50542*/ AND `locale`='deDE') OR (`entry`=50538 /*50538*/ AND `locale`='deDE') OR (`entry`=50537 /*50537*/ AND `locale`='deDE') OR (`entry`=50536 /*50536*/ AND `locale`='deDE') OR (`entry`=50533 /*50533*/ AND `locale`='deDE') OR (`entry`=50529 /*50529*/ AND `locale`='deDE') OR (`entry`=50541 /*50541*/ AND `locale`='deDE') OR (`entry`=50539 /*50539*/ AND `locale`='deDE') OR (`entry`=50535 /*50535*/ AND `locale`='deDE') OR (`entry`=50534 /*50534*/ AND `locale`='deDE') OR (`entry`=50531 /*50531*/ AND `locale`='deDE') OR (`entry`=50530 /*50530*/ AND `locale`='deDE') OR (`entry`=50543 /*50543*/ AND `locale`='deDE') OR (`entry`=39394 /*39394*/ AND `locale`='deDE') OR (`entry`=39834 /*39834*/ AND `locale`='deDE') OR (`entry`=40966 /*40966*/ AND `locale`='deDE') OR (`entry`=5345 /*5345*/ AND `locale`='deDE') OR (`entry`=7772 /*7772*/ AND `locale`='deDE') OR (`entry`=7773 /*7773*/ AND `locale`='deDE') OR (`entry`=39395 /*39395*/ AND `locale`='deDE') OR (`entry`=39384 /*39384*/ AND `locale`='deDE') OR (`entry`=40032 /*40032*/ AND `locale`='deDE') OR (`entry`=40969 /*40969*/ AND `locale`='deDE') OR (`entry`=40968 /*40968*/ AND `locale`='deDE') OR (`entry`=40967 /*40967*/ AND `locale`='deDE') OR (`entry`=40035 /*40035*/ AND `locale`='deDE') OR (`entry`=44382 /*44382*/ AND `locale`='deDE') OR (`entry`=40224 /*40224*/ AND `locale`='deDE') OR (`entry`=40194 /*40194*/ AND `locale`='deDE') OR (`entry`=7941 /*7941*/ AND `locale`='deDE') OR (`entry`=39677 /*39677*/ AND `locale`='deDE') OR (`entry`=40051 /*40051*/ AND `locale`='deDE') OR (`entry`=5343 /*5343*/ AND `locale`='deDE') OR (`entry`=11717 /*11717*/ AND `locale`='deDE') OR (`entry`=39723 /*39723*/ AND `locale`='deDE') OR (`entry`=11825 /*11825*/ AND `locale`='deDE') OR (`entry`=11824 /*11824*/ AND `locale`='deDE') OR (`entry`=39733 /*39733*/ AND `locale`='deDE') OR (`entry`=44400 /*44400*/ AND `locale`='deDE') OR (`entry`=39728 /*39728*/ AND `locale`='deDE') OR (`entry`=5327 /*5327*/ AND `locale`='deDE') OR (`entry`=5328 /*5328*/ AND `locale`='deDE') OR (`entry`=40362 /*40362*/ AND `locale`='deDE') OR (`entry`=40226 /*40226*/ AND `locale`='deDE') OR (`entry`=7946 /*7946*/ AND `locale`='deDE') OR (`entry`=7945 /*7945*/ AND `locale`='deDE') OR (`entry`=7943 /*7943*/ AND `locale`='deDE') OR (`entry`=3700 /*3700*/ AND `locale`='deDE') OR (`entry`=37162 /*37162*/ AND `locale`='deDE') OR (`entry`=10293 /*10293*/ AND `locale`='deDE') OR (`entry`=7942 /*7942*/ AND `locale`='deDE') OR (`entry`=7947 /*7947*/ AND `locale`='deDE') OR (`entry`=39314 /*39314*/ AND `locale`='deDE') OR (`entry`=40036 /*40036*/ AND `locale`='deDE') OR (`entry`=96485 /*96485*/ AND `locale`='deDE') OR (`entry`=8157 /*8157*/ AND `locale`='deDE') OR (`entry`=7949 /*7949*/ AND `locale`='deDE') OR (`entry`=7948 /*7948*/ AND `locale`='deDE') OR (`entry`=44397 /*44397*/ AND `locale`='deDE') OR (`entry`=44391 /*44391*/ AND `locale`='deDE') OR (`entry`=41383 /*41383*/ AND `locale`='deDE') OR (`entry`=10059 /*10059*/ AND `locale`='deDE') OR (`entry`=7939 /*7939*/ AND `locale`='deDE') OR (`entry`=14637 /*14637*/ AND `locale`='deDE') OR (`entry`=40360 /*40360*/ AND `locale`='deDE') OR (`entry`=3936 /*3936*/ AND `locale`='deDE') OR (`entry`=5307 /*5307*/ AND `locale`='deDE') OR (`entry`=5462 /*5462*/ AND `locale`='deDE') OR (`entry`=5461 /*5461*/ AND `locale`='deDE') OR (`entry`=5359 /*5359*/ AND `locale`='deDE') OR (`entry`=54533 /*54533*/ AND `locale`='deDE') OR (`entry`=52562 /*52562*/ AND `locale`='deDE') OR (`entry`=51943 /*51943*/ AND `locale`='deDE') OR (`entry`=51935 /*51935*/ AND `locale`='deDE') OR (`entry`=51946 /*51946*/ AND `locale`='deDE') OR (`entry`=52008 /*52008*/ AND `locale`='deDE') OR (`entry`=51942 /*51942*/ AND `locale`='deDE') OR (`entry`=51944 /*51944*/ AND `locale`='deDE') OR (`entry`=51936 /*51936*/ AND `locale`='deDE') OR (`entry`=51734 /*51734*/ AND `locale`='deDE') OR (`entry`=51733 /*51733*/ AND `locale`='deDE') OR (`entry`=51717 /*51717*/ AND `locale`='deDE') OR (`entry`=51735 /*51735*/ AND `locale`='deDE') OR (`entry`=51758 /*51758*/ AND `locale`='deDE') OR (`entry`=51755 /*51755*/ AND `locale`='deDE') OR (`entry`=51726 /*51726*/ AND `locale`='deDE') OR (`entry`=51756 /*51756*/ AND `locale`='deDE') OR (`entry`=51719 /*51719*/ AND `locale`='deDE') OR (`entry`=51718 /*51718*/ AND `locale`='deDE') OR (`entry`=54228 /*54228*/ AND `locale`='deDE') OR (`entry`=51757 /*51757*/ AND `locale`='deDE') OR (`entry`=51865 /*51865*/ AND `locale`='deDE') OR (`entry`=53418 /*53418*/ AND `locale`='deDE') OR (`entry`=51722 /*51722*/ AND `locale`='deDE') OR (`entry`=51945 /*51945*/ AND `locale`='deDE') OR (`entry`=51941 /*51941*/ AND `locale`='deDE') OR (`entry`=51750 /*51750*/ AND `locale`='deDE') OR (`entry`=52057 /*52057*/ AND `locale`='deDE') OR (`entry`=51714 /*51714*/ AND `locale`='deDE') OR (`entry`=32780 /*32780*/ AND `locale`='deDE') OR (`entry`=43896 /*43896*/ AND `locale`='deDE') OR (`entry`=15587 /*15587*/ AND `locale`='deDE') OR (`entry`=11442 /*11442*/ AND `locale`='deDE') OR (`entry`=5346 /*5346*/ AND `locale`='deDE') OR (`entry`=68805 /*68805*/ AND `locale`='deDE') OR (`entry`=7807 /*7807*/ AND `locale`='deDE') OR (`entry`=92462 /*92462*/ AND `locale`='deDE') OR (`entry`=39896 /*39896*/ AND `locale`='deDE') OR (`entry`=40052 /*40052*/ AND `locale`='deDE') OR (`entry`=44381 /*44381*/ AND `locale`='deDE') OR (`entry`=40493 /*40493*/ AND `locale`='deDE') OR (`entry`=39653 /*39653*/ AND `locale`='deDE') OR (`entry`=41580 /*41580*/ AND `locale`='deDE') OR (`entry`=40496 /*40496*/ AND `locale`='deDE') OR (`entry`=40078 /*40078*/ AND `locale`='deDE') OR (`entry`=40497 /*40497*/ AND `locale`='deDE') OR (`entry`=40132 /*40132*/ AND `locale`='deDE') OR (`entry`=37522 /*37522*/ AND `locale`='deDE') OR (`entry`=5347 /*5347*/ AND `locale`='deDE') OR (`entry`=5304 /*5304*/ AND `locale`='deDE') OR (`entry`=5300 /*5300*/ AND `locale`='deDE') OR (`entry`=5240 /*5240*/ AND `locale`='deDE') OR (`entry`=5234 /*5234*/ AND `locale`='deDE') OR (`entry`=5236 /*5236*/ AND `locale`='deDE') OR (`entry`=66364 /*66364*/ AND `locale`='deDE') OR (`entry`=66361 /*66361*/ AND `locale`='deDE') OR (`entry`=66363 /*66363*/ AND `locale`='deDE') OR (`entry`=66352 /*66352*/ AND `locale`='deDE') OR (`entry`=40079 /*40079*/ AND `locale`='deDE') OR (`entry`=41605 /*41605*/ AND `locale`='deDE') OR (`entry`=44377 /*44377*/ AND `locale`='deDE') OR (`entry`=44379 /*44379*/ AND `locale`='deDE') OR (`entry`=44378 /*44378*/ AND `locale`='deDE') OR (`entry`=40498 /*40498*/ AND `locale`='deDE') OR (`entry`=39840 /*39840*/ AND `locale`='deDE') OR (`entry`=44376 /*44376*/ AND `locale`='deDE') OR (`entry`=39656 /*39656*/ AND `locale`='deDE') OR (`entry`=39889 /*39889*/ AND `locale`='deDE') OR (`entry`=39894 /*39894*/ AND `locale`='deDE') OR (`entry`=39657 /*39657*/ AND `locale`='deDE') OR (`entry`=7854 /*7854*/ AND `locale`='deDE') OR (`entry`=12418 /*12418*/ AND `locale`='deDE') OR (`entry`=62395 /*62395*/ AND `locale`='deDE') OR (`entry`=11443 /*11443*/ AND `locale`='deDE') OR (`entry`=11440 /*11440*/ AND `locale`='deDE') OR (`entry`=40193 /*40193*/ AND `locale`='deDE') OR (`entry`=40367 /*40367*/ AND `locale`='deDE') OR (`entry`=40369 /*40369*/ AND `locale`='deDE') OR (`entry`=39725 /*39725*/ AND `locale`='deDE') OR (`entry`=44385 /*44385*/ AND `locale`='deDE') OR (`entry`=40914 /*40914*/ AND `locale`='deDE') OR (`entry`=40913 /*40913*/ AND `locale`='deDE') OR (`entry`=14661 /*14661*/ AND `locale`='deDE') OR (`entry`=5350 /*5350*/ AND `locale`='deDE') OR (`entry`=5244 /*5244*/ AND `locale`='deDE') OR (`entry`=5245 /*5245*/ AND `locale`='deDE') OR (`entry`=5246 /*5246*/ AND `locale`='deDE') OR (`entry`=40129 /*40129*/ AND `locale`='deDE') OR (`entry`=40059 /*40059*/ AND `locale`='deDE') OR (`entry`=5247 /*5247*/ AND `locale`='deDE') OR (`entry`=11447 /*11447*/ AND `locale`='deDE') OR (`entry`=40026 /*40026*/ AND `locale`='deDE') OR (`entry`=5255 /*5255*/ AND `locale`='deDE') OR (`entry`=5258 /*5258*/ AND `locale`='deDE') OR (`entry`=7957 /*7957*/ AND `locale`='deDE') OR (`entry`=7956 /*7956*/ AND `locale`='deDE') OR (`entry`=7997 /*7997*/ AND `locale`='deDE') OR (`entry`=7727 /*7727*/ AND `locale`='deDE') OR (`entry`=7725 /*7725*/ AND `locale`='deDE') OR (`entry`=5354 /*5354*/ AND `locale`='deDE') OR (`entry`=51920 /*51920*/ AND `locale`='deDE') OR (`entry`=8142 /*8142*/ AND `locale`='deDE') OR (`entry`=8115 /*8115*/ AND `locale`='deDE') OR (`entry`=14373 /*14373*/ AND `locale`='deDE') OR (`entry`=9986 /*9986*/ AND `locale`='deDE') OR (`entry`=11752 /*11752*/ AND `locale`='deDE') OR (`entry`=4544 /*4544*/ AND `locale`='deDE') OR (`entry`=11098 /*11098*/ AND `locale`='deDE') OR (`entry`=39893 /*39893*/ AND `locale`='deDE') OR (`entry`=39847 /*39847*/ AND `locale`='deDE') OR (`entry`=8144 /*8144*/ AND `locale`='deDE') OR (`entry`=5390 /*5390*/ AND `locale`='deDE') OR (`entry`=8159 /*8159*/ AND `locale`='deDE') OR (`entry`=8145 /*8145*/ AND `locale`='deDE') OR (`entry`=7875 /*7875*/ AND `locale`='deDE') OR (`entry`=5278 /*5278*/ AND `locale`='deDE') OR (`entry`=7726 /*7726*/ AND `locale`='deDE') OR (`entry`=39958 /*39958*/ AND `locale`='deDE') OR (`entry`=39957 /*39957*/ AND `locale`='deDE') OR (`entry`=15581 /*15581*/ AND `locale`='deDE') OR (`entry`=36208 /*36208*/ AND `locale`='deDE') OR (`entry`=14355 /*14355*/ AND `locale`='deDE') OR (`entry`=39965 /*39965*/ AND `locale`='deDE') OR (`entry`=39952 /*39952*/ AND `locale`='deDE') OR (`entry`=11498 /*11498*/ AND `locale`='deDE') OR (`entry`=11818 /*11818*/ AND `locale`='deDE') OR (`entry`=8158 /*8158*/ AND `locale`='deDE') OR (`entry`=8146 /*8146*/ AND `locale`='deDE') OR (`entry`=8143 /*8143*/ AND `locale`='deDE') OR (`entry`=39898 /*39898*/ AND `locale`='deDE') OR (`entry`=3500 /*3500*/ AND `locale`='deDE') OR (`entry`=7737 /*7737*/ AND `locale`='deDE') OR (`entry`=5260 /*5260*/ AND `locale`='deDE') OR (`entry`=9548 /*9548*/ AND `locale`='deDE') OR (`entry`=14395 /*14395*/ AND `locale`='deDE') OR (`entry`=11497 /*11497*/ AND `locale`='deDE') OR (`entry`=5253 /*5253*/ AND `locale`='deDE') OR (`entry`=5249 /*5249*/ AND `locale`='deDE') OR (`entry`=61081 /*61081*/ AND `locale`='deDE') OR (`entry`=5356 /*5356*/ AND `locale`='deDE') OR (`entry`=40332 /*40332*/ AND `locale`='deDE') OR (`entry`=8147 /*8147*/ AND `locale`='deDE') OR (`entry`=883 /*883*/ AND `locale`='deDE') OR (`entry`=5254 /*5254*/ AND `locale`='deDE') OR (`entry`=5251 /*5251*/ AND `locale`='deDE') OR (`entry`=5268 /*5268*/ AND `locale`='deDE') OR (`entry`=7584 /*7584*/ AND `locale`='deDE') OR (`entry`=5286 /*5286*/ AND `locale`='deDE') OR (`entry`=61080 /*61080*/ AND `locale`='deDE') OR (`entry`=39946 /*39946*/ AND `locale`='deDE') OR (`entry`=2914 /*2914*/ AND `locale`='deDE') OR (`entry`=61142 /*61142*/ AND `locale`='deDE') OR (`entry`=40168 /*40168*/ AND `locale`='deDE') OR (`entry`=12921 /*12921*/ AND `locale`='deDE') OR (`entry`=34618 /*34618*/ AND `locale`='deDE') OR (`entry`=3891 /*3891*/ AND `locale`='deDE') OR (`entry`=33443 /*33443*/ AND `locale`='deDE') OR (`entry`=33419 /*33419*/ AND `locale`='deDE') OR (`entry`=24734 /*24734*/ AND `locale`='deDE') OR (`entry`=24739 /*24739*/ AND `locale`='deDE') OR (`entry`=24737 /*24737*/ AND `locale`='deDE') OR (`entry`=24736 /*24736*/ AND `locale`='deDE') OR (`entry`=24735 /*24735*/ AND `locale`='deDE') OR (`entry`=24740 /*24740*/ AND `locale`='deDE') OR (`entry`=24738 /*24738*/ AND `locale`='deDE') OR (`entry`=3774 /*3774*/ AND `locale`='deDE') OR (`entry`=3772 /*3772*/ AND `locale`='deDE') OR (`entry`=10644 /*10644*/ AND `locale`='deDE') OR (`entry`=66331 /*66331*/ AND `locale`='deDE') OR (`entry`=66330 /*66330*/ AND `locale`='deDE') OR (`entry`=66329 /*66329*/ AND `locale`='deDE') OR (`entry`=66136 /*66136*/ AND `locale`='deDE') OR (`entry`=43606 /*43606*/ AND `locale`='deDE') OR (`entry`=43621 /*43621*/ AND `locale`='deDE') OR (`entry`=43619 /*43619*/ AND `locale`='deDE') OR (`entry`=43617 /*43617*/ AND `locale`='deDE') OR (`entry`=12717 /*12717*/ AND `locale`='deDE') OR (`entry`=12863 /*12863*/ AND `locale`='deDE') OR (`entry`=12719 /*12719*/ AND `locale`='deDE') OR (`entry`=34122 /*34122*/ AND `locale`='deDE') OR (`entry`=34303 /*34303*/ AND `locale`='deDE') OR (`entry`=43615 /*43615*/ AND `locale`='deDE') OR (`entry`=11901 /*11901*/ AND `locale`='deDE') OR (`entry`=12962 /*12962*/ AND `locale`='deDE') OR (`entry`=12961 /*12961*/ AND `locale`='deDE') OR (`entry`=34329 /*34329*/ AND `locale`='deDE') OR (`entry`=3711 /*3711*/ AND `locale`='deDE') OR (`entry`=3944 /*3944*/ AND `locale`='deDE') OR (`entry`=3943 /*3943*/ AND `locale`='deDE') OR (`entry`=3721 /*3721*/ AND `locale`='deDE') OR (`entry`=10559 /*10559*/ AND `locale`='deDE') OR (`entry`=4802 /*4802*/ AND `locale`='deDE') OR (`entry`=3814 /*3814*/ AND `locale`='deDE') OR (`entry`=3812 /*3812*/ AND `locale`='deDE') OR (`entry`=62313 /*62313*/ AND `locale`='deDE') OR (`entry`=3713 /*3713*/ AND `locale`='deDE') OR (`entry`=3717 /*3717*/ AND `locale`='deDE') OR (`entry`=34374 /*34374*/ AND `locale`='deDE') OR (`entry`=33265 /*33265*/ AND `locale`='deDE') OR (`entry`=3846 /*3846*/ AND `locale`='deDE') OR (`entry`=3845 /*3845*/ AND `locale`='deDE') OR (`entry`=33209 /*33209*/ AND `locale`='deDE') OR (`entry`=33208 /*33208*/ AND `locale`='deDE') OR (`entry`=33204 /*33204*/ AND `locale`='deDE') OR (`entry`=33187 /*33187*/ AND `locale`='deDE') OR (`entry`=33182 /*33182*/ AND `locale`='deDE') OR (`entry`=33183 /*33183*/ AND `locale`='deDE') OR (`entry`=3734 /*3734*/ AND `locale`='deDE') OR (`entry`=3733 /*3733*/ AND `locale`='deDE') OR (`entry`=3730 /*3730*/ AND `locale`='deDE') OR (`entry`=3728 /*3728*/ AND `locale`='deDE') OR (`entry`=3727 /*3727*/ AND `locale`='deDE') OR (`entry`=33389 /*33389*/ AND `locale`='deDE') OR (`entry`=33390 /*33390*/ AND `locale`='deDE') OR (`entry`=3725 /*3725*/ AND `locale`='deDE') OR (`entry`=33445 /*33445*/ AND `locale`='deDE') OR (`entry`=11219 /*11219*/ AND `locale`='deDE') OR (`entry`=33381 /*33381*/ AND `locale`='deDE') OR (`entry`=33276 /*33276*/ AND `locale`='deDE') OR (`entry`=3847 /*3847*/ AND `locale`='deDE') OR (`entry`=33375 /*33375*/ AND `locale`='deDE') OR (`entry`=33278 /*33278*/ AND `locale`='deDE') OR (`entry`=11806 /*11806*/ AND `locale`='deDE') OR (`entry`=33348 /*33348*/ AND `locale`='deDE') OR (`entry`=33444 /*33444*/ AND `locale`='deDE') OR (`entry`=34608 /*34608*/ AND `locale`='deDE') OR (`entry`=33347 /*33347*/ AND `locale`='deDE') OR (`entry`=33356 /*33356*/ AND `locale`='deDE') OR (`entry`=33334 /*33334*/ AND `locale`='deDE') OR (`entry`=33283 /*33283*/ AND `locale`='deDE') OR (`entry`=33407 /*33407*/ AND `locale`='deDE') OR (`entry`=3987 /*3987*/ AND `locale`='deDE') OR (`entry`=12818 /*12818*/ AND `locale`='deDE') OR (`entry`=3922 /*3922*/ AND `locale`='deDE') OR (`entry`=34251 /*34251*/ AND `locale`='deDE') OR (`entry`=10639 /*10639*/ AND `locale`='deDE') OR (`entry`=12757 /*12757*/ AND `locale`='deDE') OR (`entry`=34429 /*34429*/ AND `locale`='deDE') OR (`entry`=34424 /*34424*/ AND `locale`='deDE') OR (`entry`=3921 /*3921*/ AND `locale`='deDE') OR (`entry`=3925 /*3925*/ AND `locale`='deDE') OR (`entry`=3926 /*3926*/ AND `locale`='deDE') OR (`entry`=3924 /*3924*/ AND `locale`='deDE') OR (`entry`=43630 /*43630*/ AND `locale`='deDE') OR (`entry`=43624 /*43624*/ AND `locale`='deDE') OR (`entry`=34510 /*34510*/ AND `locale`='deDE') OR (`entry`=12721 /*12721*/ AND `locale`='deDE') OR (`entry`=34359 /*34359*/ AND `locale`='deDE') OR (`entry`=11137 /*11137*/ AND `locale`='deDE') OR (`entry`=3967 /*3967*/ AND `locale`='deDE') OR (`entry`=3954 /*3954*/ AND `locale`='deDE') OR (`entry`=6738 /*6738*/ AND `locale`='deDE') OR (`entry`=3959 /*3959*/ AND `locale`='deDE') OR (`entry`=3915 /*3915*/ AND `locale`='deDE') OR (`entry`=10052 /*10052*/ AND `locale`='deDE') OR (`entry`=3961 /*3961*/ AND `locale`='deDE') OR (`entry`=3691 /*3691*/ AND `locale`='deDE') OR (`entry`=3888 /*3888*/ AND `locale`='deDE') OR (`entry`=34239 /*34239*/ AND `locale`='deDE') OR (`entry`=3969 /*3969*/ AND `locale`='deDE') OR (`entry`=51770 /*51770*/ AND `locale`='deDE') OR (`entry`=33512 /*33512*/ AND `locale`='deDE') OR (`entry`=3996 /*3996*/ AND `locale`='deDE') OR (`entry`=3962 /*3962*/ AND `locale`='deDE') OR (`entry`=17106 /*17106*/ AND `locale`='deDE') OR (`entry`=15605 /*15605*/ AND `locale`='deDE') OR (`entry`=4079 /*4079*/ AND `locale`='deDE') OR (`entry`=3958 /*3958*/ AND `locale`='deDE') OR (`entry`=3953 /*3953*/ AND `locale`='deDE') OR (`entry`=3952 /*3952*/ AND `locale`='deDE') OR (`entry`=33454 /*33454*/ AND `locale`='deDE') OR (`entry`=3970 /*3970*/ AND `locale`='deDE') OR (`entry`=33451 /*33451*/ AND `locale`='deDE') OR (`entry`=4267 /*4267*/ AND `locale`='deDE') OR (`entry`=43625 /*43625*/ AND `locale`='deDE') OR (`entry`=34395 /*34395*/ AND `locale`='deDE') OR (`entry`=43653 /*43653*/ AND `locale`='deDE') OR (`entry`=34420 /*34420*/ AND `locale`='deDE') OR (`entry`=34419 /*34419*/ AND `locale`='deDE') OR (`entry`=3894 /*3894*/ AND `locale`='deDE') OR (`entry`=3892 /*3892*/ AND `locale`='deDE') OR (`entry`=34132 /*34132*/ AND `locale`='deDE') OR (`entry`=34123 /*34123*/ AND `locale`='deDE') OR (`entry`=6087 /*6087*/ AND `locale`='deDE') OR (`entry`=34160 /*34160*/ AND `locale`='deDE') OR (`entry`=34163 /*34163*/ AND `locale`='deDE') OR (`entry`=12678 /*12678*/ AND `locale`='deDE') OR (`entry`=3781 /*3781*/ AND `locale`='deDE') OR (`entry`=34378 /*34378*/ AND `locale`='deDE') OR (`entry`=3951 /*3951*/ AND `locale`='deDE') OR (`entry`=34354 /*34354*/ AND `locale`='deDE') OR (`entry`=40895 /*40895*/ AND `locale`='deDE') OR (`entry`=40894 /*40894*/ AND `locale`='deDE') OR (`entry`=3885 /*3885*/ AND `locale`='deDE') OR (`entry`=3783 /*3783*/ AND `locale`='deDE') OR (`entry`=10642 /*10642*/ AND `locale`='deDE') OR (`entry`=43063 /*43063*/ AND `locale`='deDE') OR (`entry`=34559 /*34559*/ AND `locale`='deDE') OR (`entry`=3963 /*3963*/ AND `locale`='deDE') OR (`entry`=34569 /*34569*/ AND `locale`='deDE') OR (`entry`=3964 /*3964*/ AND `locale`='deDE') OR (`entry`=41680 /*41680*/ AND `locale`='deDE') OR (`entry`=41679 /*41679*/ AND `locale`='deDE') OR (`entry`=41678 /*41678*/ AND `locale`='deDE') OR (`entry`=41681 /*41681*/ AND `locale`='deDE') OR (`entry`=34943 /*34943*/ AND `locale`='deDE') OR (`entry`=3965 /*3965*/ AND `locale`='deDE') OR (`entry`=41744 /*41744*/ AND `locale`='deDE') OR (`entry`=41743 /*41743*/ AND `locale`='deDE') OR (`entry`=6288 /*6288*/ AND `locale`='deDE') OR (`entry`=4320 /*4320*/ AND `locale`='deDE') OR (`entry`=3960 /*3960*/ AND `locale`='deDE') OR (`entry`=34617 /*34617*/ AND `locale`='deDE') OR (`entry`=3956 /*3956*/ AND `locale`='deDE') OR (`entry`=12696 /*12696*/ AND `locale`='deDE') OR (`entry`=43633 /*43633*/ AND `locale`='deDE') OR (`entry`=43634 /*43634*/ AND `locale`='deDE') OR (`entry`=3955 /*3955*/ AND `locale`='deDE') OR (`entry`=43568 /*43568*/ AND `locale`='deDE') OR (`entry`=52000 /*52000*/ AND `locale`='deDE') OR (`entry`=43646 /*43646*/ AND `locale`='deDE') OR (`entry`=43657 /*43657*/ AND `locale`='deDE') OR (`entry`=52161 /*52161*/ AND `locale`='deDE') OR (`entry`=3745 /*3745*/ AND `locale`='deDE') OR (`entry`=3696 /*3696*/ AND `locale`='deDE') OR (`entry`=3932 /*3932*/ AND `locale`='deDE') OR (`entry`=3897 /*3897*/ AND `locale`='deDE') OR (`entry`=34597 /*34597*/ AND `locale`='deDE') OR (`entry`=12759 /*12759*/ AND `locale`='deDE') OR (`entry`=3917 /*3917*/ AND `locale`='deDE') OR (`entry`=40829 /*40829*/ AND `locale`='deDE') OR (`entry`=40800 /*40800*/ AND `locale`='deDE') OR (`entry`=40799 /*40799*/ AND `locale`='deDE') OR (`entry`=39096 /*39096*/ AND `locale`='deDE') OR (`entry`=39071 /*39071*/ AND `locale`='deDE') OR (`entry`=39070 /*39070*/ AND `locale`='deDE') OR (`entry`=40820 /*40820*/ AND `locale`='deDE') OR (`entry`=34596 /*34596*/ AND `locale`='deDE') OR (`entry`=34591 /*34591*/ AND `locale`='deDE') OR (`entry`=34590 /*34590*/ AND `locale`='deDE') OR (`entry`=10641 /*10641*/ AND `locale`='deDE') OR (`entry`=3780 /*3780*/ AND `locale`='deDE') OR (`entry`=3809 /*3809*/ AND `locale`='deDE') OR (`entry`=3819 /*3819*/ AND `locale`='deDE') OR (`entry`=34592 /*34592*/ AND `locale`='deDE') OR (`entry`=3750 /*3750*/ AND `locale`='deDE') OR (`entry`=3743 /*3743*/ AND `locale`='deDE') OR (`entry`=25670 /*25670*/ AND `locale`='deDE') OR (`entry`=34283 /*34283*/ AND `locale`='deDE') OR (`entry`=34314 /*34314*/ AND `locale`='deDE') OR (`entry`=34292 /*34292*/ AND `locale`='deDE') OR (`entry`=34290 /*34290*/ AND `locale`='deDE') OR (`entry`=34289 /*34289*/ AND `locale`='deDE') OR (`entry`=8956 /*8956*/ AND `locale`='deDE') OR (`entry`=4273 /*4273*/ AND `locale`='deDE') OR (`entry`=3833 /*3833*/ AND `locale`='deDE') OR (`entry`=3815 /*3815*/ AND `locale`='deDE') OR (`entry`=34389 /*34389*/ AND `locale`='deDE') OR (`entry`=34377 /*34377*/ AND `locale`='deDE') OR (`entry`=3797 /*3797*/ AND `locale`='deDE') OR (`entry`=34518 /*34518*/ AND `locale`='deDE') OR (`entry`=12677 /*12677*/ AND `locale`='deDE') OR (`entry`=34624 /*34624*/ AND `locale`='deDE') OR (`entry`=3880 /*3880*/ AND `locale`='deDE') OR (`entry`=3916 /*3916*/ AND `locale`='deDE') OR (`entry`=34335 /*34335*/ AND `locale`='deDE') OR (`entry`=34499 /*34499*/ AND `locale`='deDE') OR (`entry`=8015 /*8015*/ AND `locale`='deDE') OR (`entry`=34366 /*34366*/ AND `locale`='deDE') OR (`entry`=4054 /*4054*/ AND `locale`='deDE') OR (`entry`=34426 /*34426*/ AND `locale`='deDE') OR (`entry`=34500 /*34500*/ AND `locale`='deDE') OR (`entry`=12676 /*12676*/ AND `locale`='deDE') OR (`entry`=33281 /*33281*/ AND `locale`='deDE') OR (`entry`=3736 /*3736*/ AND `locale`='deDE') OR (`entry`=11749 /*11749*/ AND `locale`='deDE') OR (`entry`=56894 /*56894*/ AND `locale`='deDE') OR (`entry`=12877 /*12877*/ AND `locale`='deDE') OR (`entry`=12720 /*12720*/ AND `locale`='deDE') OR (`entry`=12724 /*12724*/ AND `locale`='deDE') OR (`entry`=12616 /*12616*/ AND `locale`='deDE') OR (`entry`=11820 /*11820*/ AND `locale`='deDE') OR (`entry`=17100 /*17100*/ AND `locale`='deDE') OR (`entry`=11829 /*11829*/ AND `locale`='deDE') OR (`entry`=12867 /*12867*/ AND `locale`='deDE') OR (`entry`=12837 /*12837*/ AND `locale`='deDE') OR (`entry`=12737 /*12737*/ AND `locale`='deDE') OR (`entry`=12196 /*12196*/ AND `locale`='deDE') OR (`entry`=6028 /*6028*/ AND `locale`='deDE') OR (`entry`=15131 /*15131*/ AND `locale`='deDE') OR (`entry`=12723 /*12723*/ AND `locale`='deDE') OR (`entry`=12722 /*12722*/ AND `locale`='deDE') OR (`entry`=17355 /*17355*/ AND `locale`='deDE') OR (`entry`=11720 /*11720*/ AND `locale`='deDE') OR (`entry`=33825 /*33825*/ AND `locale`='deDE') OR (`entry`=33766 /*33766*/ AND `locale`='deDE') OR (`entry`=33760 /*33760*/ AND `locale`='deDE') OR (`entry`=12859 /*12859*/ AND `locale`='deDE') OR (`entry`=12858 /*12858*/ AND `locale`='deDE') OR (`entry`=12897 /*12897*/ AND `locale`='deDE') OR (`entry`=12896 /*12896*/ AND `locale`='deDE') OR (`entry`=14963 /*14963*/ AND `locale`='deDE') OR (`entry`=19914 /*19914*/ AND `locale`='deDE') OR (`entry`=19908 /*19908*/ AND `locale`='deDE') OR (`entry`=14753 /*14753*/ AND `locale`='deDE') OR (`entry`=14733 /*14733*/ AND `locale`='deDE') OR (`entry`=14715 /*14715*/ AND `locale`='deDE') OR (`entry`=33294 /*33294*/ AND `locale`='deDE') OR (`entry`=33295 /*33295*/ AND `locale`='deDE') OR (`entry`=33201 /*33201*/ AND `locale`='deDE') OR (`entry`=33336 /*33336*/ AND `locale`='deDE') OR (`entry`=33266 /*33266*/ AND `locale`='deDE') OR (`entry`=33267 /*33267*/ AND `locale`='deDE') OR (`entry`=33193 /*33193*/ AND `locale`='deDE') OR (`entry`=33195 /*33195*/ AND `locale`='deDE') OR (`entry`=33421 /*33421*/ AND `locale`='deDE') OR (`entry`=33440 /*33440*/ AND `locale`='deDE') OR (`entry`=33777 /*33777*/ AND `locale`='deDE') OR (`entry`=3942 /*3942*/ AND `locale`='deDE') OR (`entry`=33765 /*33765*/ AND `locale`='deDE') OR (`entry`=12856 /*12856*/ AND `locale`='deDE') OR (`entry`=33688 /*33688*/ AND `locale`='deDE') OR (`entry`=12903 /*12903*/ AND `locale`='deDE') OR (`entry`=33726 /*33726*/ AND `locale`='deDE') OR (`entry`=33728 /*33728*/ AND `locale`='deDE') OR (`entry`=33847 /*33847*/ AND `locale`='deDE') OR (`entry`=3940 /*3940*/ AND `locale`='deDE') OR (`entry`=3803 /*3803*/ AND `locale`='deDE') OR (`entry`=3941 /*3941*/ AND `locale`='deDE') OR (`entry`=3928 /*3928*/ AND `locale`='deDE') OR (`entry`=3799 /*3799*/ AND `locale`='deDE') OR (`entry`=3821 /*3821*/ AND `locale`='deDE') OR (`entry`=33446 /*33446*/ AND `locale`='deDE') OR (`entry`=6072 /*6072*/ AND `locale`='deDE') OR (`entry`=17300 /*17300*/ AND `locale`='deDE') OR (`entry`=33767 /*33767*/ AND `locale`='deDE') OR (`entry`=6073 /*6073*/ AND `locale`='deDE') OR (`entry`=11697 /*11697*/ AND `locale`='deDE') OR (`entry`=3825 /*3825*/ AND `locale`='deDE') OR (`entry`=17303 /*17303*/ AND `locale`='deDE') OR (`entry`=17287 /*17287*/ AND `locale`='deDE') OR (`entry`=34208 /*34208*/ AND `locale`='deDE') OR (`entry`=3770 /*3770*/ AND `locale`='deDE') OR (`entry`=3767 /*3767*/ AND `locale`='deDE') OR (`entry`=3771 /*3771*/ AND `locale`='deDE') OR (`entry`=3824 /*3824*/ AND `locale`='deDE') OR (`entry`=33697 /*33697*/ AND `locale`='deDE') OR (`entry`=12836 /*12836*/ AND `locale`='deDE') OR (`entry`=3763 /*3763*/ AND `locale`='deDE') OR (`entry`=3759 /*3759*/ AND `locale`='deDE') OR (`entry`=3762 /*3762*/ AND `locale`='deDE') OR (`entry`=3758 /*3758*/ AND `locale`='deDE') OR (`entry`=3834 /*3834*/ AND `locale`='deDE') OR (`entry`=4619 /*4619*/ AND `locale`='deDE') OR (`entry`=3920 /*3920*/ AND `locale`='deDE') OR (`entry`=3757 /*3757*/ AND `locale`='deDE') OR (`entry`=3755 /*3755*/ AND `locale`='deDE') OR (`entry`=3754 /*3754*/ AND `locale`='deDE') OR (`entry`=3752 /*3752*/ AND `locale`='deDE') OR (`entry`=33859 /*33859*/ AND `locale`='deDE') OR (`entry`=3765 /*3765*/ AND `locale`='deDE') OR (`entry`=22936 /*22936*/ AND `locale`='deDE') OR (`entry`=22935 /*22935*/ AND `locale`='deDE') OR (`entry`=17541 /*17541*/ AND `locale`='deDE') OR (`entry`=17409 /*17409*/ AND `locale`='deDE') OR (`entry`=17406 /*17406*/ AND `locale`='deDE') OR (`entry`=17291 /*17291*/ AND `locale`='deDE') OR (`entry`=3848 /*3848*/ AND `locale`='deDE') OR (`entry`=17412 /*17412*/ AND `locale`='deDE') OR (`entry`=33727 /*33727*/ AND `locale`='deDE') OR (`entry`=3901 /*3901*/ AND `locale`='deDE') OR (`entry`=12498 /*12498*/ AND `locale`='deDE') OR (`entry`=5314 /*5314*/ AND `locale`='deDE') OR (`entry`=12476 /*12476*/ AND `locale`='deDE') OR (`entry`=12475 /*12475*/ AND `locale`='deDE') OR (`entry`=12864 /*12864*/ AND `locale`='deDE') OR (`entry`=34233 /*34233*/ AND `locale`='deDE') OR (`entry`=34232 /*34232*/ AND `locale`='deDE') OR (`entry`=49842 /*49842*/ AND `locale`='deDE') OR (`entry`=6115 /*6115*/ AND `locale`='deDE') OR (`entry`=3784 /*3784*/ AND `locale`='deDE') OR (`entry`=3782 /*3782*/ AND `locale`='deDE') OR (`entry`=17310 /*17310*/ AND `locale`='deDE') OR (`entry`=33736 /*33736*/ AND `locale`='deDE') OR (`entry`=34294 /*34294*/ AND `locale`='deDE') OR (`entry`=34204 /*34204*/ AND `locale`='deDE') OR (`entry`=34138 /*34138*/ AND `locale`='deDE') OR (`entry`=34177 /*34177*/ AND `locale`='deDE') OR (`entry`=34182 /*34182*/ AND `locale`='deDE') OR (`entry`=34242 /*34242*/ AND `locale`='deDE') OR (`entry`=34241 /*34241*/ AND `locale`='deDE') OR (`entry`=12037 /*12037*/ AND `locale`='deDE') OR (`entry`=12474 /*12474*/ AND `locale`='deDE') OR (`entry`=11681 /*11681*/ AND `locale`='deDE') OR (`entry`=11684 /*11684*/ AND `locale`='deDE') OR (`entry`=33706 /*33706*/ AND `locale`='deDE') OR (`entry`=17304 /*17304*/ AND `locale`='deDE') OR (`entry`=11683 /*11683*/ AND `locale`='deDE') OR (`entry`=11682 /*11682*/ AND `locale`='deDE') OR (`entry`=37556 /*37556*/ AND `locale`='deDE') OR (`entry`=14234 /*14234*/ AND `locale`='deDE') OR (`entry`=51061 /*51061*/ AND `locale`='deDE') OR (`entry`=66966 /*66966*/ AND `locale`='deDE') OR (`entry`=66439 /*66439*/ AND `locale`='deDE') OR (`entry`=66438 /*66438*/ AND `locale`='deDE') OR (`entry`=66436 /*66436*/ AND `locale`='deDE') OR (`entry`=23797 /*23797*/ AND `locale`='deDE') OR (`entry`=44383 /*44383*/ AND `locale`='deDE') OR (`entry`=23601 /*23601*/ AND `locale`='deDE') OR (`entry`=23600 /*23600*/ AND `locale`='deDE') OR (`entry`=6546 /*6546*/ AND `locale`='deDE') OR (`entry`=23942 /*23942*/ AND `locale`='deDE') OR (`entry`=5089 /*5089*/ AND `locale`='deDE') OR (`entry`=12919 /*12919*/ AND `locale`='deDE') OR (`entry`=14236 /*14236*/ AND `locale`='deDE') OR (`entry`=4403 /*4403*/ AND `locale`='deDE') OR (`entry`=10321 /*10321*/ AND `locale`='deDE') OR (`entry`=51069 /*51069*/ AND `locale`='deDE') OR (`entry`=4334 /*4334*/ AND `locale`='deDE') OR (`entry`=62201 /*62201*/ AND `locale`='deDE') OR (`entry`=23687 /*23687*/ AND `locale`='deDE') OR (`entry`=4323 /*4323*/ AND `locale`='deDE') OR (`entry`=4324 /*4324*/ AND `locale`='deDE') OR (`entry`=14237 /*14237*/ AND `locale`='deDE') OR (`entry`=4331 /*4331*/ AND `locale`='deDE') OR (`entry`=4329 /*4329*/ AND `locale`='deDE') OR (`entry`=4328 /*4328*/ AND `locale`='deDE') OR (`entry`=40358 /*40358*/ AND `locale`='deDE') OR (`entry`=23995 /*23995*/ AND `locale`='deDE') OR (`entry`=23572 /*23572*/ AND `locale`='deDE') OR (`entry`=23571 /*23571*/ AND `locale`='deDE') OR (`entry`=40345 /*40345*/ AND `locale`='deDE') OR (`entry`=40344 /*40344*/ AND `locale`='deDE') OR (`entry`=23636 /*23636*/ AND `locale`='deDE') OR (`entry`=23579 /*23579*/ AND `locale`='deDE') OR (`entry`=23573 /*23573*/ AND `locale`='deDE') OR (`entry`=23570 /*23570*/ AND `locale`='deDE') OR (`entry`=50764 /*50764*/ AND `locale`='deDE') OR (`entry`=23873 /*23873*/ AND `locale`='deDE') OR (`entry`=4356 /*4356*/ AND `locale`='deDE') OR (`entry`=4357 /*4357*/ AND `locale`='deDE') OR (`entry`=23637 /*23637*/ AND `locale`='deDE') OR (`entry`=5057 /*5057*/ AND `locale`='deDE') OR (`entry`=50901 /*50901*/ AND `locale`='deDE') OR (`entry`=23751 /*23751*/ AND `locale`='deDE') OR (`entry`=4980 /*4980*/ AND `locale`='deDE') OR (`entry`=23832 /*23832*/ AND `locale`='deDE') OR (`entry`=4393 /*4393*/ AND `locale`='deDE') OR (`entry`=4415 /*4415*/ AND `locale`='deDE') OR (`entry`=4394 /*4394*/ AND `locale`='deDE') OR (`entry`=23752 /*23752*/ AND `locale`='deDE') OR (`entry`=4345 /*4345*/ AND `locale`='deDE') OR (`entry`=23753 /*23753*/ AND `locale`='deDE') OR (`entry`=23595 /*23595*/ AND `locale`='deDE') OR (`entry`=23594 /*23594*/ AND `locale`='deDE') OR (`entry`=14233 /*14233*/ AND `locale`='deDE') OR (`entry`=4348 /*4348*/ AND `locale`='deDE') OR (`entry`=4344 /*4344*/ AND `locale`='deDE') OR (`entry`=4501 /*4501*/ AND `locale`='deDE') OR (`entry`=24208 /*24208*/ AND `locale`='deDE') OR (`entry`=17095 /*17095*/ AND `locale`='deDE') OR (`entry`=11899 /*11899*/ AND `locale`='deDE') OR (`entry`=4926 /*4926*/ AND `locale`='deDE') OR (`entry`=4884 /*4884*/ AND `locale`='deDE') OR (`entry`=4500 /*4500*/ AND `locale`='deDE') OR (`entry`=4502 /*4502*/ AND `locale`='deDE') OR (`entry`=5087 /*5087*/ AND `locale`='deDE') OR (`entry`=4883 /*4883*/ AND `locale`='deDE') OR (`entry`=4879 /*4879*/ AND `locale`='deDE') OR (`entry`=50342 /*50342*/ AND `locale`='deDE') OR (`entry`=4983 /*4983*/ AND `locale`='deDE') OR (`entry`=23567 /*23567*/ AND `locale`='deDE') OR (`entry`=4503 /*4503*/ AND `locale`='deDE') OR (`entry`=4343 /*4343*/ AND `locale`='deDE') OR (`entry`=50784 /*50784*/ AND `locale`='deDE') OR (`entry`=6567 /*6567*/ AND `locale`='deDE') OR (`entry`=39144 /*39144*/ AND `locale`='deDE') OR (`entry`=10036 /*10036*/ AND `locale`='deDE') OR (`entry`=9552 /*9552*/ AND `locale`='deDE') OR (`entry`=4791 /*4791*/ AND `locale`='deDE') OR (`entry`=45227 /*45227*/ AND `locale`='deDE') OR (`entry`=4378 /*4378*/ AND `locale`='deDE') OR (`entry`=4379 /*4379*/ AND `locale`='deDE') OR (`entry`=4376 /*4376*/ AND `locale`='deDE') OR (`entry`=50945 /*50945*/ AND `locale`='deDE') OR (`entry`=4385 /*4385*/ AND `locale`='deDE') OR (`entry`=4834 /*4834*/ AND `locale`='deDE') OR (`entry`=23723 /*23723*/ AND `locale`='deDE') OR (`entry`=23568 /*23568*/ AND `locale`='deDE') OR (`entry`=17119 /*17119*/ AND `locale`='deDE') OR (`entry`=5184 /*5184*/ AND `locale`='deDE') OR (`entry`=4966 /*4966*/ AND `locale`='deDE') OR (`entry`=4347 /*4347*/ AND `locale`='deDE') OR (`entry`=4412 /*4412*/ AND `locale`='deDE') OR (`entry`=23869 /*23869*/ AND `locale`='deDE') OR (`entry`=23868 /*23868*/ AND `locale`='deDE') OR (`entry`=23720 /*23720*/ AND `locale`='deDE') OR (`entry`=23714 /*23714*/ AND `locale`='deDE') OR (`entry`=23593 /*23593*/ AND `locale`='deDE') OR (`entry`=4382 /*4382*/ AND `locale`='deDE') OR (`entry`=23592 /*23592*/ AND `locale`='deDE') OR (`entry`=4880 /*4880*/ AND `locale`='deDE') OR (`entry`=14232 /*14232*/ AND `locale`='deDE') OR (`entry`=23843 /*23843*/ AND `locale`='deDE') OR (`entry`=4792 /*4792*/ AND `locale`='deDE') OR (`entry`=5086 /*5086*/ AND `locale`='deDE') OR (`entry`=5085 /*5085*/ AND `locale`='deDE') OR (`entry`=23841 /*23841*/ AND `locale`='deDE') OR (`entry`=23554 /*23554*/ AND `locale`='deDE') OR (`entry`=23979 /*23979*/ AND `locale`='deDE') OR (`entry`=23585 /*23585*/ AND `locale`='deDE') OR (`entry`=23555 /*23555*/ AND `locale`='deDE') OR (`entry`=4341 /*4341*/ AND `locale`='deDE') OR (`entry`=50735 /*50735*/ AND `locale`='deDE') OR (`entry`=4351 /*4351*/ AND `locale`='deDE') OR (`entry`=4352 /*4352*/ AND `locale`='deDE') OR (`entry`=15553 /*15553*/ AND `locale`='deDE') OR (`entry`=15591 /*15591*/ AND `locale`='deDE') OR (`entry`=115485 /*115485*/ AND `locale`='deDE') OR (`entry`=115482 /*115482*/ AND `locale`='deDE') OR (`entry`=4374 /*4374*/ AND `locale`='deDE') OR (`entry`=115481 /*115481*/ AND `locale`='deDE') OR (`entry`=38622 /*38622*/ AND `locale`='deDE') OR (`entry`=38664 /*38664*/ AND `locale`='deDE') OR (`entry`=38661 /*38661*/ AND `locale`='deDE') OR (`entry`=38623 /*38623*/ AND `locale`='deDE') OR (`entry`=38627 /*38627*/ AND `locale`='deDE') OR (`entry`=4363 /*4363*/ AND `locale`='deDE') OR (`entry`=4361 /*4361*/ AND `locale`='deDE') OR (`entry`=4362 /*4362*/ AND `locale`='deDE') OR (`entry`=4359 /*4359*/ AND `locale`='deDE') OR (`entry`=23569 /*23569*/ AND `locale`='deDE') OR (`entry`=23591 /*23591*/ AND `locale`='deDE') OR (`entry`=23679 /*23679*/ AND `locale`='deDE') OR (`entry`=23590 /*23590*/ AND `locale`='deDE') OR (`entry`=23589 /*23589*/ AND `locale`='deDE') OR (`entry`=4346 /*4346*/ AND `locale`='deDE') OR (`entry`=4414 /*4414*/ AND `locale`='deDE') OR (`entry`=50957 /*50957*/ AND `locale`='deDE') OR (`entry`=63546 /*63546*/ AND `locale`='deDE') OR (`entry`=4390 /*4390*/ AND `locale`='deDE') OR (`entry`=4389 /*4389*/ AND `locale`='deDE') OR (`entry`=4401 /*4401*/ AND `locale`='deDE') OR (`entry`=4404 /*4404*/ AND `locale`='deDE') OR (`entry`=51924 /*51924*/ AND `locale`='deDE') OR (`entry`=53404 /*53404*/ AND `locale`='deDE') OR (`entry`=4965 /*4965*/ AND `locale`='deDE') OR (`entry`=53410 /*53410*/ AND `locale`='deDE') OR (`entry`=27703 /*27703*/ AND `locale`='deDE') OR (`entry`=4967 /*4967*/ AND `locale`='deDE') OR (`entry`=4890 /*4890*/ AND `locale`='deDE') OR (`entry`=27704 /*27704*/ AND `locale`='deDE') OR (`entry`=53415 /*53415*/ AND `locale`='deDE') OR (`entry`=8141 /*8141*/ AND `locale`='deDE') OR (`entry`=23949 /*23949*/ AND `locale`='deDE') OR (`entry`=12939 /*12939*/ AND `locale`='deDE') OR (`entry`=4973 /*4973*/ AND `locale`='deDE') OR (`entry`=4892 /*4892*/ AND `locale`='deDE') OR (`entry`=4964 /*4964*/ AND `locale`='deDE') OR (`entry`=23892 /*23892*/ AND `locale`='deDE') OR (`entry`=4944 /*4944*/ AND `locale`='deDE') OR (`entry`=4794 /*4794*/ AND `locale`='deDE') OR (`entry`=4889 /*4889*/ AND `locale`='deDE') OR (`entry`=5083 /*5083*/ AND `locale`='deDE') OR (`entry`=8140 /*8140*/ AND `locale`='deDE') OR (`entry`=4951 /*4951*/ AND `locale`='deDE') OR (`entry`=4948 /*4948*/ AND `locale`='deDE') OR (`entry`=4952 /*4952*/ AND `locale`='deDE') OR (`entry`=4891 /*4891*/ AND `locale`='deDE') OR (`entry`=5200 /*5200*/ AND `locale`='deDE') OR (`entry`=5095 /*5095*/ AND `locale`='deDE') OR (`entry`=4950 /*4950*/ AND `locale`='deDE') OR (`entry`=53403 /*53403*/ AND `locale`='deDE') OR (`entry`=5094 /*5094*/ AND `locale`='deDE') OR (`entry`=5093 /*5093*/ AND `locale`='deDE') OR (`entry`=5092 /*5092*/ AND `locale`='deDE') OR (`entry`=5090 /*5090*/ AND `locale`='deDE') OR (`entry`=4924 /*4924*/ AND `locale`='deDE') OR (`entry`=4923 /*4923*/ AND `locale`='deDE') OR (`entry`=4922 /*4922*/ AND `locale`='deDE') OR (`entry`=5199 /*5199*/ AND `locale`='deDE') OR (`entry`=5096 /*5096*/ AND `locale`='deDE') OR (`entry`=5091 /*5091*/ AND `locale`='deDE') OR (`entry`=4902 /*4902*/ AND `locale`='deDE') OR (`entry`=4894 /*4894*/ AND `locale`='deDE') OR (`entry`=13277 /*13277*/ AND `locale`='deDE') OR (`entry`=4893 /*4893*/ AND `locale`='deDE') OR (`entry`=6732 /*6732*/ AND `locale`='deDE') OR (`entry`=53405 /*53405*/ AND `locale`='deDE') OR (`entry`=4901 /*4901*/ AND `locale`='deDE') OR (`entry`=6272 /*6272*/ AND `locale`='deDE') OR (`entry`=53436 /*53436*/ AND `locale`='deDE') OR (`entry`=53437 /*53437*/ AND `locale`='deDE') OR (`entry`=4397 /*4397*/ AND `locale`='deDE') OR (`entry`=53421 /*53421*/ AND `locale`='deDE') OR (`entry`=10047 /*10047*/ AND `locale`='deDE') OR (`entry`=5388 /*5388*/ AND `locale`='deDE') OR (`entry`=4921 /*4921*/ AND `locale`='deDE') OR (`entry`=4895 /*4895*/ AND `locale`='deDE') OR (`entry`=23951 /*23951*/ AND `locale`='deDE') OR (`entry`=4900 /*4900*/ AND `locale`='deDE') OR (`entry`=4899 /*4899*/ AND `locale`='deDE') OR (`entry`=4898 /*4898*/ AND `locale`='deDE') OR (`entry`=39138 /*39138*/ AND `locale`='deDE') OR (`entry`=24006 /*24006*/ AND `locale`='deDE') OR (`entry`=53409 /*53409*/ AND `locale`='deDE') OR (`entry`=24005 /*24005*/ AND `locale`='deDE') OR (`entry`=4888 /*4888*/ AND `locale`='deDE') OR (`entry`=4941 /*4941*/ AND `locale`='deDE') OR (`entry`=4886 /*4886*/ AND `locale`='deDE') OR (`entry`=4885 /*4885*/ AND `locale`='deDE') OR (`entry`=53407 /*53407*/ AND `locale`='deDE') OR (`entry`=4897 /*4897*/ AND `locale`='deDE') OR (`entry`=11052 /*11052*/ AND `locale`='deDE') OR (`entry`=23620 /*23620*/ AND `locale`='deDE') OR (`entry`=23704 /*23704*/ AND `locale`='deDE') OR (`entry`=23950 /*23950*/ AND `locale`='deDE') OR (`entry`=24007 /*24007*/ AND `locale`='deDE') OR (`entry`=4896 /*4896*/ AND `locale`='deDE') OR (`entry`=4321 /*4321*/ AND `locale`='deDE') OR (`entry`=4968 /*4968*/ AND `locale`='deDE') OR (`entry`=23835 /*23835*/ AND `locale`='deDE') OR (`entry`=23566 /*23566*/ AND `locale`='deDE') OR (`entry`=2616 /*2616*/ AND `locale`='deDE') OR (`entry`=23896 /*23896*/ AND `locale`='deDE') OR (`entry`=23905 /*23905*/ AND `locale`='deDE') OR (`entry`=44390 /*44390*/ AND `locale`='deDE') OR (`entry`=23907 /*23907*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(11553, 'deDE', 'Holzbieger der Holzschlundfeste', '', NULL, NULL, 23420), -- 11553
(48461, 'deDE', 'Ferli', '', 'Junges der Holzschlundfeste', NULL, 23420), -- 48461
(15395, 'deDE', 'Nafien', '', NULL, NULL, 23420), -- 15395
(10017, 'deDE', 'Besudelte Kakerlake', '', NULL, NULL, 23420), -- 10017
(7158, 'deDE', 'Schamane der Totenwaldfelle', '', NULL, NULL, 23420), -- 7158
(7157, 'deDE', 'Rächer der Totenwaldfelle', '', NULL, NULL, 23420), -- 7157
(10016, 'deDE', 'Besudelte Ratte', '', NULL, NULL, 23420), -- 10016
(7156, 'deDE', 'Höhlenbehüter der Totenwaldfelle', '', NULL, NULL, 23420), -- 7156
(11558, 'deDE', 'Kernda', '', NULL, NULL, 23420), -- 11558
(11555, 'deDE', 'Gorn Einauge', '', NULL, NULL, 23420), -- 11555
(11557, 'deDE', 'Meilosh', '', NULL, NULL, 23420), -- 11557
(10738, 'deDE', 'Oberhäuptling der Winterfelle', '', NULL, NULL, 23420), -- 10738
(48765, 'deDE', 'Reifpelz', '', NULL, NULL, 23420), -- 48765
(10916, 'deDE', 'Läufer der Winterfelle', '', NULL, NULL, 23420), -- 10916
(10307, 'deDE', 'Hexendoktorin Mau''ari', '', NULL, NULL, 23420), -- 10307
(9298, 'deDE', 'Donova Schneeweh', '', NULL, NULL, 23420), -- 9298
(11516, 'deDE', 'Wärter der Holzschlundfeste', '', NULL, NULL, 23420), -- 11516
(11552, 'deDE', 'Mystiker der Holzschlundfeste', '', NULL, NULL, 23420), -- 11552
(11556, 'deDE', 'Salfa', '', NULL, NULL, 23420), -- 11556
(7441, 'deDE', 'Totemiker der Winterfelle', '', NULL, NULL, 23420), -- 7441
(7440, 'deDE', 'Höhlenbehüter der Winterfelle', '', NULL, NULL, 23420), -- 7440
(7442, 'deDE', 'Pfadfinder der Winterfelle', '', NULL, NULL, 23420), -- 7442
(48767, 'deDE', 'Brühende Quelle', '', NULL, NULL, 23420), -- 48767
(48768, 'deDE', 'Kochende Quellblase', '', NULL, NULL, 23420), -- 48768
(10920, 'deDE', 'Kelek Himmelshüter', '', 'Der Smaragdkreis', NULL, 23420), -- 10920
(48665, 'deDE', 'Elendes Schreckgespenst', '', NULL, NULL, 23420), -- 48665
(48664, 'deDE', 'Elender Geist', '', NULL, NULL, 23420), -- 48664
(48658, 'deDE', 'Geist der Kaldorei', '', NULL, NULL, 23420), -- 48658
(7524, 'deDE', 'Gepeinigte Hochgeborene', '', NULL, NULL, 23420), -- 7524
(7523, 'deDE', 'Leidender Hochgeborener', '', NULL, NULL, 23420), -- 7523
(15606, 'deDE', 'Urahnin Prunkspeer', '', NULL, NULL, 23420), -- 15606
(48740, 'deDE', 'Erzmagier Maenius', '', NULL, NULL, 23420), -- 48740
(49217, 'deDE', 'Winterrebenpeitscher', '', NULL, NULL, 23420), -- 49217
(11138, 'deDE', 'Maethrya', '', 'Hippogryphenmeisterin', NULL, 23420), -- 11138
(11757, 'deDE', 'Umaron Stragarelm', '', NULL, NULL, 23420), -- 11757
(11183, 'deDE', 'Blixxrak', '', 'Händler für leichte Rüstungen', NULL, 23420), -- 11183
(11182, 'deDE', 'Nixxrak', '', 'Händler für Kettenrüstungen', NULL, 23420), -- 11182
(48952, 'deDE', 'Frostlaubtreant', '', NULL, NULL, 23420), -- 48952
(48670, 'deDE', 'Verratener Waldläufer', '', NULL, NULL, 23420), -- 48670
(48960, 'deDE', 'Frostscherbenrumpler', '', NULL, NULL, 23420), -- 48960
(11186, 'deDE', 'Lunnix Schmierspross', '', 'Bergbaubedarf', NULL, 23420), -- 11186
(11184, 'deDE', 'Wixxrak', '', 'Waffenschmied & Büchsenmacher', NULL, 23420), -- 11184
(11754, 'deDE', 'Meggi Frickelprotz', '', NULL, NULL, 23420), -- 11754
(11546, 'deDE', 'Jack Silberling', '', NULL, NULL, 23420), -- 11546
(10978, 'deDE', 'Legacki', '', NULL, NULL, 23420), -- 10978
(10468, 'deDE', 'Felnok Stahlfeder', '', NULL, NULL, 23420), -- 10468
(52831, 'deDE', 'Winterquelljunges', '', NULL, NULL, 23420), -- 52831
(52830, 'deDE', 'Michelle De Rum', '', 'Haustiersammlerin', NULL, 23420), -- 52830
(11187, 'deDE', 'Himmik', '', 'Speis & Trank', NULL, 23420), -- 11187
(62119, 'deDE', 'Roboglucke', '', NULL, 'wildpetcapturable', 23420), -- 62119
(16416, 'deDE', 'Bronn Fitzzang', '', NULL, NULL, 23420), -- 16416
(11118, 'deDE', 'Gastwirt Vizzie', '', 'Gastwirtin', NULL, 23420), -- 11118
(50366, 'deDE', 'Nymn', '', NULL, NULL, 23420), -- 50366
(11755, 'deDE', 'Harlo Wackelwert', '', NULL, NULL, 23420), -- 11755
(11753, 'deDE', 'Gogo', '', NULL, NULL, 23420), -- 11753
(11185, 'deDE', 'Xizzer Zischbolz', '', 'Ingenieursbedarf', NULL, 23420), -- 11185
(10637, 'deDE', 'Malyfous Düsterhammer', '', 'Die Thoriumbruderschaft', NULL, 23420), -- 10637
(15574, 'deDE', 'Urahne Steinkeil', '', NULL, NULL, 23420), -- 15574
(48965, 'deDE', 'Deez Felsklau', '', 'Extra-kompakte Waren', NULL, 23420), -- 48965
(14742, 'deDE', 'Zap Weitschleuder', '', 'Unausgeglichener Ingenieur', NULL, 23420), -- 14742
(11189, 'deDE', 'Qia', '', 'Handwerkswaren', NULL, 23420), -- 11189
(11188, 'deDE', 'Evie Wirbelbräu', '', 'Alchemiebedarf', NULL, 23420), -- 11188
(13917, 'deDE', 'Izzy Kupferzock', '', 'Bankier', NULL, 23420), -- 13917
(11193, 'deDE', 'Seril Geißelbann', '', NULL, NULL, 23420), -- 11193
(11192, 'deDE', 'Kilram', '', NULL, NULL, 23420), -- 11192
(11191, 'deDE', 'Lilith die Liebliche', '', NULL, NULL, 23420), -- 11191
(11190, 'deDE', 'Haudrauf der Ewigen Warte', '', NULL, NULL, 23420), -- 11190
(9857, 'deDE', 'Auktionatorin Grizzlin', '', NULL, NULL, 23420), -- 9857
(49773, 'deDE', 'Roboglucke', '', NULL, NULL, 23420), -- 49773
(12150, 'deDE', 'Reitkodo (Lila)', '', NULL, NULL, 23420), -- 12150
(11119, 'deDE', 'Azzelby', '', 'Stallmeister', NULL, 23420), -- 11119
(10305, 'deDE', 'Umi Rumpelkicher', '', NULL, NULL, 23420), -- 10305
(5198, 'deDE', 'Arktischer Wolf', '', NULL, NULL, 23420), -- 5198
(4779, 'deDE', 'Brauner Widder', '', NULL, NULL, 23420), -- 4779
(20102, 'deDE', 'Goblinbürger', '', NULL, NULL, 23420), -- 20102
(11139, 'deDE', 'Yugrek', '', 'Windreitermeister', NULL, 23420), -- 11139
(48918, 'deDE', 'Winterhornhirsch', '', NULL, NULL, 23420), -- 48918
(50044, 'deDE', 'Höllenschuhu', '', NULL, NULL, 23420), -- 50044
(7443, 'deDE', 'Splitterzahnraufer', '', NULL, NULL, 23420), -- 7443
(49346, 'deDE', 'Krabbler der Kaltlaurer', '', NULL, NULL, 23420), -- 49346
(2084, 'deDE', 'Natheril Regenrufer', '', 'Gemischtwaren', NULL, 23420), -- 2084
(49402, 'deDE', 'Sana Vorhangfeuer', '', NULL, NULL, 23420), -- 49402
(49347, 'deDE', 'Wühler der Kaltlaurer', '', NULL, NULL, 23420), -- 49347
(11079, 'deDE', 'Wynd Nachtjagd', '', NULL, NULL, 23420), -- 11079
(3779, 'deDE', 'Syurana', '', 'Handwerkswaren', NULL, 23420), -- 3779
(48622, 'deDE', 'Gormir Steinformer', '', 'Forscherliga', NULL, 23420), -- 48622
(2303, 'deDE', 'Lyranne Federsang', '', 'Speis & Trank', NULL, 23420), -- 2303
(10619, 'deDE', 'Gletscher', '', 'Riverns Wächter', NULL, 23420), -- 10619
(10618, 'deDE', 'Rivern Frostwind', '', 'Wintersäblerausbilder', NULL, 23420), -- 10618
(10737, 'deDE', 'Shy-Rotam', '', NULL, NULL, 23420), -- 10737
(7434, 'deDE', 'Rudelbehüterin der Frostsäbler', '', NULL, NULL, 23420), -- 7434
(7430, 'deDE', 'Junger Frostsäbler', '', NULL, NULL, 23420), -- 7430
(51681, 'deDE', 'Frostsäblerjunges', '', NULL, 'Pickup', 23420), -- 51681
(50094, 'deDE', 'Fallensteller Kiefer', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 50094
(50092, 'deDE', 'Barbie Knackkiste', '', 'Speis & Trank', NULL, 23420), -- 50092
(49537, 'deDE', 'Jeb Guthrie', '', NULL, NULL, 23420), -- 49537
(49436, 'deDE', 'Francis Morcott', '', NULL, NULL, 23420), -- 49436
(49396, 'deDE', 'Jez Gutmampf', '', NULL, NULL, 23420), -- 49396
(7431, 'deDE', 'Frostsäbler', '', NULL, NULL, 23420), -- 7431
(7433, 'deDE', 'Frostsäblerjägerin', '', NULL, NULL, 23420), -- 7433
(7432, 'deDE', 'Frostsäblerpirscherin', '', NULL, NULL, 23420), -- 7432
(11751, 'deDE', 'Rilan Howacht', '', NULL, NULL, 23420), -- 11751
(11718, 'deDE', 'Sar Nussauge', '', NULL, NULL, 23420), -- 11718
(7448, 'deDE', 'Eiswindschimäre', '', NULL, NULL, 23420), -- 7448
(7444, 'deDE', 'Splitterzahnbär', '', NULL, NULL, 23420), -- 7444
(7455, 'deDE', 'Winterquelleule', '', NULL, NULL, 23420), -- 7455
(51711, 'deDE', 'Schneebedeckter Bau', '', NULL, NULL, 23420), -- 51711
(49565, 'deDE', 'Schneetummelfuchs', '', NULL, NULL, 23420), -- 49565
(7451, 'deDE', 'Tobende Eulenbestie', '', NULL, NULL, 23420), -- 7451
(11696, 'deDE', 'Chal Sanftwind', '', 'Wintersäblerausbilderin', NULL, 23420), -- 11696
(7456, 'deDE', 'Winterquellkreischer', '', NULL, NULL, 23420), -- 7456
(50995, 'deDE', 'Haudrauf', '', NULL, NULL, 23420), -- 50995
(7450, 'deDE', 'Struppige Eulenbestie', '', NULL, NULL, 23420), -- 7450
(48723, 'deDE', 'Tanrir', '', 'Holzschlundfeste', NULL, 23420), -- 48723
(48722, 'deDE', 'Burndl', '', 'Holzschlundfeste', NULL, 23420), -- 48722
(49177, 'deDE', 'Erdtotem der Winterfelle', '', NULL, NULL, 23420), -- 49177
(49235, 'deDE', 'Eisklatscher', '', NULL, NULL, 23420), -- 49235
(49178, 'deDE', 'Grolnar der Berserker', '', NULL, NULL, 23420), -- 49178
(7439, 'deDE', 'Schamane der Winterfelle', '', NULL, NULL, 23420), -- 7439
(49233, 'deDE', 'Solides Eis', '', NULL, NULL, 23420), -- 49233
(7460, 'deDE', 'Eisdistelpatriarch', '', NULL, NULL, 23420), -- 7460
(7438, 'deDE', 'Ursa der Winterfelle', '', NULL, NULL, 23420), -- 7438
(49772, 'deDE', 'Mümmelbot', '', NULL, NULL, 23420), -- 49772
(68839, 'deDE', 'Eloxiertes Robojunges', '', NULL, 'wildpetcapturable', 23420), -- 68839
(7458, 'deDE', 'Eisdistelyeti', '', NULL, NULL, 23420), -- 7458
(7459, 'deDE', 'Eisdistelmatriarchin', '', NULL, NULL, 23420), -- 7459
(66470, 'deDE', 'Winzigos', '', NULL, NULL, 23420), -- 66470
(66469, 'deDE', 'Frostrachen', '', NULL, NULL, 23420), -- 66469
(66468, 'deDE', 'Blizzy', '', NULL, NULL, 23420), -- 66468
(66466, 'deDE', 'Die eiskalte Trixxy', '', 'Großmeistertierzähmerin', NULL, 23420), -- 66466
(50422, 'deDE', 'Zakk Sinon', '', NULL, NULL, 23420), -- 50422
(50129, 'deDE', 'Daleohm', '', 'Schmiedekunstbedarf', NULL, 23420), -- 50129
(50126, 'deDE', 'Maseel', '', 'Gemischtwaren', NULL, 23420), -- 50126
(10929, 'deDE', 'Haleh', '', NULL, NULL, 23420), -- 10929
(10301, 'deDE', 'Jaron Steinschleifer', '', NULL, NULL, 23420), -- 10301
(50291, 'deDE', 'Arkaner Tesserakt', '', NULL, NULL, 23420), -- 50291
(49161, 'deDE', 'Verändertes Tier', '', NULL, NULL, 23420), -- 49161
(16015, 'deDE', 'Vi''el', '', 'Händler für exotische Reagenzien', NULL, 23420), -- 16015
(10918, 'deDE', 'Lorax', '', NULL, NULL, 23420), -- 10918
(62435, 'deDE', 'Kristallspinne', '', NULL, 'wildpetcapturable', 23420), -- 62435
(50251, 'deDE', 'Frostilicus', '', NULL, NULL, 23420), -- 50251
(32261, 'deDE', 'Kristallspinne', '', NULL, NULL, 23420), -- 32261
(50250, 'deDE', 'Eisavatar', '', NULL, NULL, 23420), -- 50250
(10198, 'deDE', 'Kashoch der Häscher', '', NULL, NULL, 23420), -- 10198
(50258, 'deDE', 'Frosthagelkipper', '', NULL, NULL, 23420), -- 50258
(10807, 'deDE', 'Brumeran', '', NULL, NULL, 23420), -- 10807
(50263, 'deDE', 'Jadrag der Schlitzer', '', NULL, NULL, 23420), -- 50263
(50280, 'deDE', 'Klauengeist', '', NULL, NULL, 23420), -- 50280
(61690, 'deDE', 'Berghase', '', NULL, 'wildpetcapturable', 23420), -- 61690
(50281, 'deDE', 'Lebensgeist', '', NULL, NULL, 23420), -- 50281
(7454, 'deDE', 'Berserkereulenbestie', '', NULL, NULL, 23420), -- 7454
(50282, 'deDE', 'Mondgeist', '', NULL, NULL, 23420), -- 50282
(7453, 'deDE', 'Mondbestrahlte Eulenbestie', '', NULL, NULL, 23420), -- 7453
(7452, 'deDE', 'Wahnsinnige Eulenbestie', '', NULL, NULL, 23420), -- 7452
(7429, 'deDE', 'Frosthagelbewahrer', '', NULL, NULL, 23420), -- 7429
(47285, 'deDE', 'Siamat', '', 'Herrscher des Südwinds', NULL, 23360), -- 47285
(51759, 'deDE', 'Eingeschworener Kürschner', '', NULL, NULL, 23360), -- 51759
(48028, 'deDE', 'Adarrah', '', NULL, NULL, 23360), -- 48028
(48011, 'deDE', 'Kerkermeister von Neferset', '', NULL, NULL, 23360), -- 48011
(46334, 'deDE', 'Infanterist von Ramkahen', '', NULL, NULL, 23360), -- 46334
(46884, 'deDE', 'Schmuggler von Neferset', '', NULL, NULL, 23360), -- 46884
(47699, 'deDE', 'Feldarbeiter von Ramkahen', '', NULL, NULL, 23360), -- 47699
(48040, 'deDE', 'Pygmäentrampel', '', NULL, NULL, 23360), -- 48040
(48041, 'deDE', 'Pygmäenspäher', '', NULL, NULL, 23360), -- 48041
(48043, 'deDE', 'Pygmäendieb', '', NULL, NULL, 23360), -- 48043
(47318, 'deDE', 'Mack', '', NULL, NULL, 23360), -- 47318
(49148, 'deDE', 'Dunkler Ritualist Zakahn', '', NULL, NULL, 23360), -- 49148
(48861, 'deDE', 'Umi', '', 'Gemüse', NULL, 23360), -- 48861
(48860, 'deDE', 'Hanbal', '', 'Inschriften- und Schneidereibedarf', NULL, 23360), -- 48860
(48856, 'deDE', 'Husani', '', 'Metzger', NULL, 23360), -- 48856
(49156, 'deDE', 'Ritualwächter', '', NULL, NULL, 23360), -- 49156
(48853, 'deDE', 'Samaki', '', 'Angelbedarf', NULL, 23360), -- 48853
(66824, 'deDE', 'Obalis', '', 'Großmeistertierzähmer', NULL, 23360), -- 66824
(66814, 'deDE', 'Pyth', '', NULL, NULL, 23360), -- 66814
(66813, 'deDE', 'Frühlo', '', NULL, NULL, 23360), -- 66813
(66812, 'deDE', 'Klacksi', '', NULL, NULL, 23360), -- 66812
(47062, 'deDE', 'Priester der Vir''Naal', '', NULL, NULL, 23360), -- 47062
(46135, 'deDE', 'Hohepriester Amet', '', NULL, NULL, 23360), -- 46135
(50065, 'deDE', 'Armagürtlon', '', NULL, NULL, 23360), -- 50065
(50066, 'deDE', 'Armagürtlons Stachel', '', NULL, NULL, 23360), -- 50066
(46340, 'deDE', 'Wache von Neferset', '', NULL, NULL, 23360), -- 46340
(48267, 'deDE', 'Bewohner von Neferset', '', NULL, NULL, 23360), -- 48267
(35374, 'deDE', 'Generic Trigger LAB - Multiphase', '', NULL, NULL, 23360), -- 35374
(47567, 'deDE', 'Kavem der Gefühllose', '', NULL, NULL, 23360), -- 47567
(46317, 'deDE', 'Krokilisk von Neferset', '', NULL, NULL, 23360), -- 46317
(46496, 'deDE', 'Tahet', '', NULL, 'Speak', 23360), -- 46496
(40350, 'deDE', 'Generic Trigger LAB', '', NULL, NULL, 23360), -- 40350
(51676, 'deDE', 'Marschenschlange', '', NULL, NULL, 23360), -- 51676
(46315, 'deDE', 'Schuppenjäger von Neferset', '', NULL, NULL, 23360), -- 46315
(47813, 'deDE', 'Obelisk von Neferset', '', NULL, NULL, 23360), -- 47813
(47730, 'deDE', 'Hohepriester Sekhemet', '', NULL, NULL, 23360), -- 47730
(47742, 'deDE', 'Skorpionfürst Namkhare', '', NULL, NULL, 23360), -- 47742
(47741, 'deDE', 'Giftbewahrer von Neferset', '', NULL, NULL, 23360), -- 47741
(47762, 'deDE', 'Ritualist von Neferset', '', NULL, NULL, 23360), -- 47762
(47810, 'deDE', 'Pyramidenbehüter', '', NULL, NULL, 23360), -- 47810
(47760, 'deDE', 'Fluchbringer von Neferset', '', NULL, NULL, 23360), -- 47760
(47753, 'deDE', 'Dunkler Pharao Tekahn', '', NULL, NULL, 23360), -- 47753
(47801, 'deDE', 'Tempelskarabäus', '', NULL, NULL, 23360), -- 47801
(47738, 'deDE', 'Schlangenbeschwörer von Neferset', '', NULL, NULL, 23360), -- 47738
(47726, 'deDE', 'Tiger', '', NULL, NULL, 23360), -- 47726
(47722, 'deDE', 'Rüstungsschmied von Neferset', '', NULL, NULL, 23360), -- 47722
(47729, 'deDE', 'Wilder von Neferset', '', NULL, NULL, 23360), -- 47729
(47725, 'deDE', 'Händler der Tol''vir', '', NULL, NULL, 23360), -- 47725
(47727, 'deDE', 'Klingenwirbler von Neferset', '', NULL, NULL, 23360), -- 47727
(49345, 'deDE', 'Salhet', '', NULL, NULL, 23360), -- 49345
(48550, 'deDE', 'Champion von Neferset', '', NULL, NULL, 23360), -- 48550
(49244, 'deDE', 'Waldläuferhauptmann von Ramkahen', '', NULL, 'Speak', 23360), -- 49244
(49242, 'deDE', 'Waldläufer von Ramkahen', '', NULL, NULL, 23360), -- 49242
(46877, 'deDE', 'Tanzar', '', NULL, NULL, 23360), -- 46877
(46880, 'deDE', 'Samir', '', NULL, NULL, 23360), -- 46880
(46879, 'deDE', 'Harkor', '', NULL, NULL, 23360), -- 46879
(46878, 'deDE', 'Mack', '', NULL, NULL, 23360), -- 46878
(46875, 'deDE', 'Budd', '', NULL, NULL, 23360), -- 46875
(46872, 'deDE', 'Prinz Nadun', '', NULL, NULL, 23360), -- 46872
(48237, 'deDE', 'Salhet', '', NULL, NULL, 23360), -- 48237
(48514, 'deDE', 'Schütze von Ramkahen', '', NULL, NULL, 23360), -- 48514
(48512, 'deDE', 'Legionär von Ramkahen', '', NULL, NULL, 23360), -- 48512
(48548, 'deDE', 'Unsterblicher Koloss', '', NULL, NULL, 23360), -- 48548
(48564, 'deDE', 'König Phaoris', '', 'Herrscher von Ramkahen', NULL, 23360), -- 48564
(47032, 'deDE', 'Titanischer Wächter', '', NULL, NULL, 23360), -- 47032
(47037, 'deDE', 'Titanic Guardian Eye', '', NULL, NULL, 23360), -- 47037
(49415, 'deDE', 'Blutlache', '', NULL, NULL, 23360), -- 49415
(49414, 'deDE', 'Blutlache', '', NULL, NULL, 23360), -- 49414
(50401, 'deDE', 'Titanenmechanismus', '', NULL, NULL, 23360), -- 50401
(47122, 'deDE', 'Completion Bunny', '', NULL, NULL, 23360), -- 47122
(48606, 'deDE', 'Chamber of the Moon - Beam Effect Bunny', '', NULL, NULL, 23360), -- 48606
(49411, 'deDE', 'Stiefels', '', NULL, NULL, 23360), -- 49411
(46979, 'deDE', 'Lufteinheit von Schnottz', '', NULL, NULL, 23360), -- 46979
(46993, 'deDE', 'Luftoffizier von Schnottz', '', NULL, NULL, 23360), -- 46993
(47190, 'deDE', 'Struppiger Wüstenkojote', '', NULL, NULL, 23360), -- 47190
(47201, 'deDE', 'Wüstenfuchs', '', NULL, 'LootAll', 23360), -- 47201
(48239, 'deDE', 'Gürteltier', '', NULL, NULL, 23360), -- 48239
(47202, 'deDE', 'Kranker Geier', '', NULL, NULL, 23360), -- 47202
(48874, 'deDE', 'Wächter von Mar''at', '', NULL, NULL, 23360), -- 48874
(48857, 'deDE', 'Mosegi', '', 'Schneiderbedarf', NULL, 23360), -- 48857
(51648, 'deDE', 'Tabat', '', 'Wasserverkäufer', NULL, 23360), -- 51648
(48868, 'deDE', 'Chuma', '', 'Handwerkswaren', NULL, 23360), -- 48868
(47645, 'deDE', 'Harkor', '', NULL, NULL, 23360), -- 47645
(48168, 'deDE', 'Salhets Löwe', '', NULL, NULL, 23360), -- 48168
(43718, 'deDE', 'Generic Trigger LAB (Gigantic AOI)', '', NULL, NULL, 23360), -- 43718
(48012, 'deDE', 'Unteroffizier Mehat', '', NULL, NULL, 23360), -- 48012
(46134, 'deDE', 'Hochkommandant Kamses', '', NULL, NULL, 23360), -- 46134
(46333, 'deDE', 'Arbeiter von Ramkahen', '', NULL, 'Speak', 23360), -- 46333
(47720, 'deDE', 'Kamel', '', NULL, NULL, 23360), -- 47720
(45353, 'deDE', 'Murrbluthyäne', '', NULL, NULL, 23360), -- 45353
(46136, 'deDE', 'Wesir Tanotep', '', NULL, NULL, 23360), -- 46136
(46024, 'deDE', 'Orsis Colossus Vehicle', '', NULL, NULL, 23360), -- 46024
(46041, 'deDE', 'Koloss der Sonne', '', NULL, 'Interact', 23360), -- 46041
(47219, 'deDE', 'Toter Gardist', '', NULL, 'LootAll', 23360), -- 47219
(47213, 'deDE', 'Toter Gardist', '', NULL, 'LootAll', 23360), -- 47213
(55210, 'deDE', 'Urahne Sekhemi', '', NULL, NULL, 23360), -- 55210
(47207, 'deDE', 'Toter Gardist', '', NULL, 'LootAll', 23360), -- 47207
(47216, 'deDE', 'Toter Gardist', '', NULL, 'LootAll', 23360), -- 47216
(47220, 'deDE', 'Wütendes Schreckgespenst', '', NULL, NULL, 23360), -- 47220
(47227, 'deDE', 'Ewiger Beschützer', '', NULL, NULL, 23360), -- 47227
(47981, 'deDE', 'Kapitän Margun', '', NULL, NULL, 23360), -- 47981
(47978, 'deDE', 'Kapitän Kronkh', '', NULL, NULL, 23360), -- 47978
(47980, 'deDE', 'Kapitän Cork', '', NULL, NULL, 23360), -- 47980
(45772, 'deDE', 'General Ammantep', '', NULL, NULL, 23360), -- 45772
(45799, 'deDE', 'Prinz Nadun', '', NULL, NULL, 23360), -- 45799
(46087, 'deDE', 'Orsis Colossus Vehicle', '', NULL, NULL, 23360), -- 46087
(46042, 'deDE', 'Koloss des Mondes', '', NULL, 'Interact', 23360), -- 46042
(45716, 'deDE', 'Orsis Survivor Vehicle', '', NULL, NULL, 23360), -- 45716
(45715, 'deDE', 'Überlebender von Orsis', '', NULL, 'Interact', 23360), -- 45715
(45755, 'deDE', 'Abkömmling von Al''Akir', '', NULL, NULL, 23360), -- 45755
(47982, 'deDE', 'Seemann von Schnottz', '', NULL, NULL, 23360), -- 47982
(47670, 'deDE', 'Bellok Leuchtklinge', '', NULL, NULL, 23360), -- 47670
(47159, 'deDE', 'Kommandant Schnottz', '', NULL, NULL, 23360), -- 47159
(47520, 'deDE', 'Anmaßender Geschäftsmann', '', NULL, NULL, 23360), -- 47520
(49525, 'deDE', 'Geborgene Güter', '', NULL, NULL, 23360), -- 49525
(48959, 'deDE', 'Rostiger Amboss', '', NULL, NULL, 23360), -- 48959
(49528, 'deDE', 'Arkanes Gästebuch', '', NULL, NULL, 23360), -- 49528
(48273, 'deDE', 'Evax Ölfunke', '', 'Flugmeister', NULL, 23360), -- 48273
(47707, 'deDE', 'Angehender Künstler', '', NULL, NULL, 23360), -- 47707
(47516, 'deDE', 'Produktiver Schriftsteller', '', NULL, NULL, 23360), -- 47516
(47515, 'deDE', 'Botschafter Laurent', '', NULL, NULL, 23360), -- 47515
(47703, 'deDE', 'Kultivierter Gentleman', '', NULL, NULL, 23360), -- 47703
(47706, 'deDE', 'Blutlache', '', NULL, NULL, 23360), -- 47706
(47705, 'deDE', 'Blutlache', '', NULL, NULL, 23360), -- 47705
(47704, 'deDE', 'Blutlache', '', NULL, NULL, 23360), -- 47704
(47702, 'deDE', 'Bedrohlicher Abgesandter', '', NULL, NULL, 23360), -- 47702
(47708, 'deDE', 'Aufstrebendes Filmsternchen', '', NULL, NULL, 23360), -- 47708
(47701, 'deDE', 'Bedrohlicher Abgesandter', '', NULL, NULL, 23360), -- 47701
(47519, 'deDE', 'Gutgestellte Dame der Gesellschaft', '', NULL, NULL, 23360), -- 47519
(47193, 'deDE', 'Leibwächter von Schnottz', '', NULL, NULL, 23360), -- 47193
(51672, 'deDE', 'Fegende Winde', '', NULL, NULL, 23360), -- 51672
(47732, 'deDE', 'Belagerungspanzer von Schnottz', '', NULL, 'vehichleCursor', 23360), -- 47732
(47455, 'deDE', 'Belagerungspanzerkanone', '', NULL, 'vehichleCursor', 23360), -- 47455
(47452, 'deDE', 'Belagerungspanzer von Schnottz', '', NULL, 'vehichleCursor', 23360), -- 47452
(47772, 'deDE', 'Belagerungspanzerkanone', '', NULL, 'vehichleCursor', 23360), -- 47772
(47546, 'deDE', 'Altersschwacher Behüter', '', NULL, NULL, 23360), -- 47546
(47292, 'deDE', 'Faulenzender Arbeiter', '', NULL, NULL, 23360), -- 47292
(47690, 'deDE', 'Arbeiter von Schnottz'' Landeplatz', '', NULL, NULL, 23360), -- 47690
(46003, 'deDE', 'Prophet Hadassi', '', NULL, NULL, 23360), -- 46003
(48518, 'deDE', 'Wächter der Ahnen', '', NULL, NULL, 23360), -- 48518
(48668, 'deDE', 'Elitesoldat von Schnottz', '', NULL, NULL, 23360), -- 48668
(48534, 'deDE', 'Leiche eines Elitesoldaten von Schnottz', '', NULL, NULL, 23360), -- 48534
(47716, 'deDE', 'Sonnenpriester Iset', '', NULL, NULL, 23360), -- 47716
(47715, 'deDE', 'Sonnenpriester Asaris', '', NULL, NULL, 23360), -- 47715
(47755, 'deDE', 'Kriegsfürst Ihsenn', '', NULL, NULL, 23360), -- 47755
(48205, 'deDE', 'Aufseher von Schnottz', '', NULL, NULL, 23360), -- 48205
(48204, 'deDE', 'Ausgräber von Schnottz', '', NULL, NULL, 23360), -- 48204
(48374, 'deDE', 'Myzerian', '', NULL, NULL, 23360), -- 48374
(48214, 'deDE', 'Kämpfer von Schnottz', '', NULL, NULL, 23360), -- 48214
(47709, 'deDE', 'Akolyth der Sonne', '', NULL, NULL, 23360), -- 47709
(51713, 'deDE', 'Langschreitergazelle', '', NULL, NULL, 23360), -- 51713
(46871, 'deDE', 'Schuppenmutter Hevna', '', NULL, NULL, 23360), -- 46871
(48644, 'deDE', 'Beschlagnahmte Artillerie', '', NULL, NULL, 23360), -- 48644
(48694, 'deDE', 'Wüstenbewohner', '', NULL, NULL, 23360), -- 48694
(11732, 'deDE', 'Feuerspucker des Regalschwarms', '', NULL, NULL, 23360), -- 11732
(11731, 'deDE', 'Einbuddler des Regalschwarms', '', NULL, NULL, 23360), -- 11731
(15215, 'deDE', 'Herrin Natalia Mar''alith', '', 'Hohepriesterin von C''Thun', NULL, 23360), -- 15215
(11734, 'deDE', 'Schwarmlord des Regalschwarms', '', NULL, NULL, 23360), -- 11734
(11733, 'deDE', 'Sklavenjäger des Regalschwarms', '', NULL, NULL, 23360), -- 11733
(49408, 'deDE', 'Farah Tamina', '', 'Stallmeisterin', NULL, 23360), -- 49408
(49406, 'deDE', 'Yasmin', '', 'Gastwirtin', NULL, 23360), -- 49406
(49410, 'deDE', 'Aaron "Sandzehe" Willmann', '', 'Reagenzien', NULL, 23360), -- 49410
(49409, 'deDE', 'Der röchelnde Jake', '', 'Ausgrabungszubehör & Reparaturen', NULL, 23360), -- 49409
(48274, 'deDE', 'Jock Lindsey', '', 'Flugmeister', NULL, 23360), -- 48274
(48203, 'deDE', 'Sullah', '', NULL, NULL, 23360), -- 48203
(50470, 'deDE', 'Karawanengeschirr', '', NULL, NULL, 23360), -- 50470
(50939, 'deDE', 'Domestizierter Kodo', '', NULL, NULL, 23360), -- 50939
(51675, 'deDE', 'Oasenkrokilisk', '', NULL, NULL, 23360), -- 51675
(51217, 'deDE', 'Hüpfender Pygmäe', '', NULL, NULL, 23360), -- 51217
(62894, 'deDE', 'Gehörnte Echse', '', NULL, 'wildpetcapturable', 23360), -- 62894
(51673, 'deDE', 'Giftschuppenspeier', '', NULL, NULL, 23360), -- 51673
(62896, 'deDE', 'Leopardskorpid', '', NULL, 'wildpetcapturable', 23360), -- 62896
(49197, 'deDE', 'Schild der Lade', '', NULL, NULL, 23360), -- 49197
(47803, 'deDE', 'Sandskorpid', '', NULL, NULL, 23360), -- 47803
(47283, 'deDE', 'Sandschlange', '', NULL, NULL, 23360), -- 47283
(51753, 'deDE', 'Aufseher von Schnottz', '', NULL, NULL, 23360), -- 51753
(51193, 'deDE', 'Wildes Kamel', '', NULL, NULL, 23360), -- 51193
(47223, 'deDE', 'Heißluftballon von Schnottz', '', NULL, NULL, 23360), -- 47223
(47224, 'deDE', 'Strickleiter', '', NULL, 'vehichleCursor', 23360), -- 47224
(62186, 'deDE', 'Wüstenspinne', '', NULL, 'wildpetcapturable', 23360), -- 62186
(47048, 'deDE', 'Sensor des Tempels von Uldum', '', NULL, NULL, 23360), -- 47048
(49832, 'deDE', 'Leopardskorpid', '', NULL, NULL, 23360), -- 49832
(51674, 'deDE', 'Sandpanzerskarabäus', '', NULL, NULL, 23360), -- 51674
(51671, 'deDE', 'Tollwütige Hyäne', '', NULL, NULL, 23360), -- 51671
(62899, 'deDE', 'Tol''vir-Skarabäus', '', NULL, 'wildpetcapturable', 23360), -- 62899
(47221, 'deDE', 'Vorratschopper von Schnottz', '', NULL, NULL, 23360), -- 47221
(47222, 'deDE', 'Vorräte von Schnottz', '', NULL, NULL, 23360), -- 47222
(48631, 'deDE', 'Eliteinfanterist von Schnottz', '', NULL, NULL, 23360), -- 48631
(48629, 'deDE', 'Infanterist von Schnottz', '', NULL, NULL, 23360), -- 48629
(45859, 'deDE', 'Giftblutskorpid', '', NULL, NULL, 23360), -- 45859
(62523, 'deDE', 'Seitenwinder-Klapperschlange', '', NULL, 'wildpetcapturable', 23360), -- 62523
(49835, 'deDE', 'Wüstenspinne', '', NULL, NULL, 23360), -- 49835
(49727, 'deDE', 'Seitenwinder-Klapperschlange', '', NULL, NULL, 23360), -- 49727
(45905, 'deDE', 'Vorhut der Wüstenläufer', '', NULL, NULL, 23360), -- 45905
(48334, 'deDE', 'Staubteufel', '', NULL, NULL, 23360), -- 48334
(62895, 'deDE', 'Oasenmotte', '', NULL, 'wildpetcapturable', 23360), -- 62895
(45302, 'deDE', 'Sonnenjägergazelle', '', NULL, NULL, 23360), -- 45302
(45321, 'deDE', 'Flussbettkrokilisk', '', NULL, NULL, 23360), -- 45321
(62892, 'deDE', 'Laubfrosch', '', NULL, 'wildpetcapturable', 23360), -- 62892
(48858, 'deDE', 'Ishaq', '', 'Taschen', NULL, 23360), -- 48858
(46868, 'deDE', 'Stillwassergleiter', '', NULL, NULL, 23360), -- 46868
(47656, 'deDE', 'Budd', '', NULL, NULL, 23360), -- 47656
(62893, 'deDE', 'Heuschrecke', '', NULL, 'wildpetcapturable', 23360), -- 62893
(51712, 'deDE', 'Farnbrüterwespe', '', NULL, NULL, 23360), -- 51712
(3386, 'deDE', 'Bewahrerin von Theramore', '', NULL, NULL, 23360), -- 3386
(3385, 'deDE', 'Marinesoldat von Theramore', '', NULL, NULL, 23360), -- 3385
(38621, 'deDE', 'Jeffrey Lang', '', NULL, NULL, 23360), -- 38621
(39322, 'deDE', 'Horton Kardanherz', '', NULL, NULL, 23360), -- 39322
(38697, 'deDE', 'Nathan Blaine', '', 'Signaloffizier', NULL, 23360), -- 38697
(38619, 'deDE', 'Admiral Aubrey', '', NULL, NULL, 23360), -- 38619
(44307, 'deDE', 'Cranston Zischelspieß', '', 'Alchemiebedarf', NULL, 23360), -- 44307
(44283, 'deDE', 'Camran', '', 'Kettenrüstungen', NULL, 23360), -- 44283
(44348, 'deDE', 'Karoline Willis', '', 'Stallmeisterin', NULL, 23360), -- 44348
(44268, 'deDE', 'Festenbehüter Kerry', '', 'Gastwirt', NULL, 23360), -- 44268
(39212, 'deDE', 'Bill Wilhelmsen', '', 'Flugmeister', NULL, 23360), -- 39212
(44303, 'deDE', 'Rüstmeister Rutherford', '', 'Gemischtwaren', NULL, 23360), -- 44303
(38804, 'deDE', 'Tolliver Hundezahn', '', NULL, NULL, 23360), -- 38804
(38620, 'deDE', 'Thomas Paxton', '', NULL, NULL, 23360), -- 38620
(3454, 'deDE', 'Kanonier Hecksler', '', NULL, NULL, 23360), -- 3454
(38801, 'deDE', 'Norbin', '', NULL, NULL, 23360), -- 38801
(38800, 'deDE', 'Killick', '', NULL, NULL, 23360), -- 38800
(3465, 'deDE', 'Feuerzweig', '', NULL, NULL, 23360), -- 3465
(38754, 'deDE', 'Küstengeschütz der Nordwacht', '', NULL, 'Gunner', 23360), -- 38754
(3455, 'deDE', 'Kanonier Knoch', '', NULL, NULL, 23360), -- 3455
(6020, 'deDE', 'Schleimschalenmakrura', '', NULL, NULL, 23360), -- 6020
(38747, 'deDE', 'Ruderboot von Donnerschrei', '', NULL, NULL, 23360), -- 38747
(38818, 'deDE', 'Katapult von Donnerschrei', '', NULL, NULL, 23360), -- 38818
(38805, 'deDE', 'Verwundeter Verteidiger', '', NULL, NULL, 23360), -- 38805
(38624, 'deDE', 'Verteidiger der Nordwacht', '', NULL, NULL, 23360), -- 38624
(38659, 'deDE', 'Leutnant von Donnerschrei', '', NULL, NULL, 23360), -- 38659
(38658, 'deDE', 'Grunzer von Donnerschrei', '', NULL, NULL, 23360), -- 38658
(38878, 'deDE', 'Geschundener Leichnam', '', NULL, NULL, 23360), -- 38878
(3256, 'deDE', 'Sonnenschuppensensenklaue', '', NULL, NULL, 23360), -- 3256
(3255, 'deDE', 'Sonnenschuppenkreischer', '', NULL, NULL, 23360), -- 3255
(3234, 'deDE', 'Verirrter Brachlandkodo', '', NULL, NULL, 23360), -- 3234
(3461, 'deDE', 'Oasenschnappkiefer', '', NULL, NULL, 23360), -- 3461
(3275, 'deDE', 'Marodeur der Kolkar', '', NULL, NULL, 23360), -- 3275
(9523, 'deDE', 'Sturmseherin der Kolkar', '', NULL, NULL, 23360), -- 9523
(3425, 'deDE', 'Savannenstreuner', '', NULL, NULL, 23360), -- 3425
(34641, 'deDE', 'Invisible Stalker (Walk 5, Run 14)', '', NULL, NULL, 23360), -- 34641
(71457, 'deDE', 'Gyrokopter der Ehrenwacht', '', NULL, NULL, 23360), -- 71457
(71384, 'deDE', 'Karawanenführer der Ehrenwacht', '', NULL, NULL, 23360), -- 71384
(3246, 'deDE', 'Flüchtiger Ebenenschreiter', '', NULL, NULL, 23360), -- 3246
(3272, 'deDE', 'Zänker der Kolkar', '', NULL, NULL, 23360), -- 3272
(3273, 'deDE', 'Sturmruferin der Kolkar', '', NULL, NULL, 23360), -- 3273
(38380, 'deDE', 'Janice Mattingly', '', 'Vorratsoffizierin', NULL, 23360), -- 38380
(38382, 'deDE', 'Leutnant Worley', '', NULL, NULL, 23360), -- 38382
(44299, 'deDE', 'Beathan Feuerbräu', '', 'Hochprozentiges', NULL, 23360), -- 44299
(44219, 'deDE', 'Logistikoffizier Renaldo', '', 'Gastwirt', NULL, 23360), -- 44219
(39210, 'deDE', 'John Johnson', '', 'Flugmeister', NULL, 23360), -- 39210
(44346, 'deDE', 'Brandon Heiterhimmel', '', 'Stallmeister', NULL, 23360), -- 44346
(38383, 'deDE', 'Nibb Spindelgang', '', NULL, NULL, 23360), -- 38383
(38379, 'deDE', 'Kommandant Singleton', '', NULL, NULL, 23360), -- 38379
(44294, 'deDE', 'Rüstmeister Lawson', '', 'Gemischtwaren', NULL, 23360), -- 44294
(44277, 'deDE', 'Gary Henton', '', 'Waffenverkäufer', NULL, 23360), -- 44277
(34848, 'deDE', 'Scharfschütze der Ehrenwacht', '', NULL, NULL, 23360), -- 34848
(37160, 'deDE', 'Turmwächter', '', NULL, NULL, 23360), -- 37160
(37180, 'deDE', 'Wache der Ehrenwacht', '', NULL, NULL, 23360), -- 37180
(34855, 'deDE', 'Arbeiter der Ehrenwacht', '', NULL, NULL, 23360), -- 34855
(37216, 'deDE', 'Offizier der Ehrenwacht', '', NULL, NULL, 23360), -- 37216
(51857, 'deDE', 'Kriegerheld des Jägerhügels', '', NULL, NULL, 23360), -- 51857
(44349, 'deDE', 'Munada', '', 'Stallmeister', NULL, 23360), -- 44349
(44270, 'deDE', 'Gastwirt Hurnahet', '', 'Gastwirt', NULL, 23360), -- 44270
(37154, 'deDE', 'Kilrok Grindhammer', '', NULL, NULL, 23360), -- 37154
(4049, 'deDE', 'Seereth Bruchstein', '', NULL, NULL, 23360), -- 4049
(37138, 'deDE', 'Onatay', '', NULL, NULL, 23360), -- 37138
(44297, 'deDE', 'Kurinika Seelensucher', '', 'Gemischtwaren', NULL, 23360), -- 44297
(39340, 'deDE', 'Unega', '', 'Flugmeister', NULL, 23360), -- 39340
(44285, 'deDE', 'Ramja Himmelsweber', '', 'Tuchmacher', NULL, 23360), -- 44285
(37204, 'deDE', 'Kriegerheld des Jägerhügels', '', NULL, NULL, 23360), -- 37204
(62131, 'deDE', 'Savannengepard', '', NULL, NULL, 23360), -- 62131
(62129, 'deDE', 'Gepardenjunges', '', NULL, 'wildpetcapturable', 23360), -- 62129
(37166, 'deDE', 'Assistent des Feldmessers', '', NULL, NULL, 23360), -- 37166
(37165, 'deDE', 'Henry Zykes', '', NULL, NULL, 23360), -- 37165
(51856, 'deDE', 'Wache von Una''fe', '', NULL, NULL, 23360), -- 51856
(44286, 'deDE', 'Murhane', '', 'Lederrüstungen', NULL, 23360), -- 44286
(9983, 'deDE', 'Kelsuwa', '', 'Ehemaliger Stallmeister', NULL, 23360), -- 9983
(3810, 'deDE', 'Alter Eschentalbär', '', NULL, NULL, 23360), -- 3810
(11857, 'deDE', 'Makaba Flachhuf', '', NULL, NULL, 23360), -- 11857
(7714, 'deDE', 'Byula', '', 'Ehemaliger Gastwirt', NULL, 23360), -- 7714
(37517, 'deDE', 'Waisenkind aus Taurajo', '', NULL, NULL, 23360), -- 37517
(37516, 'deDE', 'Tawane', '', NULL, NULL, 23360), -- 37516
(3705, 'deDE', 'Gahroot', '', 'Metzger', NULL, 23360), -- 3705
(37515, 'deDE', 'Lane Hochgras', '', NULL, NULL, 23360), -- 37515
(44296, 'deDE', 'Turrana', '', 'Gemischtwaren', NULL, 23360), -- 44296
(37220, 'deDE', 'Wache von Una''fe', '', NULL, NULL, 23360), -- 37220
(3426, 'deDE', 'Zhevrastürmer', '', NULL, NULL, 23360), -- 3426
(37170, 'deDE', 'Späher des Jägerhügels', '', NULL, NULL, 23360), -- 37170
(37161, 'deDE', 'Fußsoldat der Ehrenwacht', '', NULL, NULL, 23360), -- 37161
(38314, 'deDE', 'Muyoh', '', 'Jünger von Naralex', NULL, 23360), -- 38314
(37570, 'deDE', 'Naralex', '', NULL, NULL, 23360), -- 37570
(5829, 'deDE', 'Snort der Spucker', '', NULL, NULL, 23360), -- 5829
(37548, 'deDE', 'Klammernder Überwuchs', '', NULL, NULL, 23360), -- 37548
(5864, 'deDE', 'Speerträger Schweingart', '', NULL, NULL, 23360), -- 5864
(37487, 'deDE', 'Flüchtling aus Taurajo', '', NULL, NULL, 23360), -- 37487
(38554, 'deDE', 'Toter Flüchtling aus Taurajo', '', NULL, NULL, 23360), -- 38554
(5863, 'deDE', 'Geopriester Gukk''rok', '', NULL, NULL, 23360), -- 5863
(3704, 'deDE', 'Mahani', '', 'Schneiderlehrerin', NULL, 23360), -- 3704
(37717, 'deDE', 'Winnoa Kiefernwald', '', NULL, NULL, 23360), -- 37717
(37679, 'deDE', 'Kriegsfürst Blutheft', '', NULL, NULL, 23360), -- 37679
(10380, 'deDE', 'Sanuye Runentotem', '', 'Lederrüstungshändler', NULL, 23360), -- 10380
(3418, 'deDE', 'Kirge Sternhorn', '', NULL, NULL, 23360), -- 3418
(3387, 'deDE', 'Jorn Himmelsdeuter', '', NULL, NULL, 23360), -- 3387
(52060, 'deDE', 'Lognah', '', 'Windreitermeister', NULL, 23360), -- 52060
(15588, 'deDE', 'Urahne Hohenberg', '', NULL, NULL, 23360), -- 15588
(3433, 'deDE', 'Tatternack Stahlformer', '', NULL, NULL, 23360), -- 3433
(34346, 'deDE', 'Dave''s Industrial Light and Magic Bunny', '', NULL, NULL, 23360), -- 34346
(3261, 'deDE', 'Dornenwirker der Borstennacken', '', NULL, NULL, 23360), -- 3261
(37706, 'deDE', 'Ödnisgrunzer', '', NULL, NULL, 23360), -- 37706
(38327, 'deDE', 'Belagerungsingenieur der Nordwacht', '', NULL, NULL, 23360), -- 38327
(10378, 'deDE', 'Omusa Donnerhorn', '', 'Windreitermeister', NULL, 23360), -- 10378
(5944, 'deDE', 'Yonada', '', 'Schneiderei- & Lederverarbeitungsbedarf', NULL, 23360), -- 5944
(3703, 'deDE', 'Krulmoo Vollmond', '', 'Lederverarbeitungslehrer', NULL, 23360), -- 3703
(6387, 'deDE', 'Dranh', '', 'Kürschnerlehrer', NULL, 23360), -- 6387
(8016, 'deDE', 'Wache des Brachlandes', '', NULL, NULL, 23360), -- 8016
(37743, 'deDE', 'Plünderer von Taurajo', '', NULL, NULL, 23360), -- 37743
(51851, 'deDE', 'Ödniswache', '', NULL, NULL, 23360), -- 51851
(39330, 'deDE', 'Crador', '', 'Flugmeister', 'Taxi', 23360), -- 39330
(37909, 'deDE', 'Tomusa', '', NULL, NULL, 23360), -- 37909
(44287, 'deDE', 'Terndak', '', 'Waffenhändler', NULL, 23360), -- 44287
(44300, 'deDE', 'Rüstmeister Dernhak', '', 'Gemischtwaren', NULL, 23360), -- 44300
(38033, 'deDE', 'Merkwürdig schlaksiger Orc', '', NULL, NULL, 23360), -- 38033
(44354, 'deDE', 'Grantor', '', 'Stallmeister', NULL, 23360), -- 44354
(37910, 'deDE', 'Crawgol', '', NULL, NULL, 23360), -- 37910
(37811, 'deDE', 'Kriegsherr Gar''dul', '', NULL, NULL, 23360), -- 37811
(37866, 'deDE', 'Bloodhilt''s Area Trigger and Carry Bunny', '', NULL, NULL, 23360), -- 37866
(44276, 'deDE', 'Gastwirt Lhakadd', '', 'Gastwirt', NULL, 23360), -- 44276
(5859, 'deDE', 'Hagg Taurenfluch', '', 'Held der Klingenmähnen', NULL, 23360), -- 5859
(37940, 'deDE', 'Quetschzahn', '', 'Häuptling der Klingenmähnen', NULL, 23360), -- 37940
(37661, 'deDE', 'Seher der Klingenmähnen', '', NULL, NULL, 23360), -- 37661
(66429, 'deDE', 'Dingsbums', '', NULL, NULL, 23360), -- 66429
(66428, 'deDE', 'Gackatron', '', NULL, NULL, 23360), -- 66428
(66427, 'deDE', 'Wirbel', '', NULL, NULL, 23360), -- 66427
(66422, 'deDE', 'Kassandra Kabumm', '', 'Meistertierzähmerin', NULL, 23360), -- 66422
(34527, 'deDE', 'Dave''s Industrial Light and Magic Bunny (Small)', '', NULL, NULL, 23360), -- 34527
(39282, 'deDE', 'Dirne von Frazzelcraz', '', NULL, NULL, 23360), -- 39282
(39281, 'deDE', 'Fenton Frazzelcraz', '', NULL, NULL, 23360), -- 39281
(39279, 'deDE', 'Großknecht von Frazzelcraz', '', NULL, NULL, 23360), -- 39279
(39280, 'deDE', 'Minenarbeiter von Frazzelcraz', '', NULL, NULL, 23360), -- 39280
(5899, 'deDE', 'Brine', '', NULL, NULL, 23360), -- 5899
(39154, 'deDE', 'Hurlston Steinwurf', '', NULL, NULL, 23360), -- 39154
(39118, 'deDE', 'General Doppelzopf', '', NULL, NULL, 23360), -- 39118
(37560, 'deDE', 'Pfadfinder der Klingenmähnen', '', NULL, NULL, 23360), -- 37560
(37931, 'deDE', 'Romo''s Vehicle Bunny', '', NULL, NULL, 23360), -- 37931
(37933, 'deDE', 'Leichnam der Klingenmähnen', '', NULL, NULL, 23360), -- 37933
(37847, 'deDE', 'Mankrik', '', NULL, NULL, 23360), -- 37847
(37660, 'deDE', 'Kriegsberserker der Klingenmähnen', '', NULL, NULL, 23360), -- 37660
(37836, 'deDE', 'Larhasha', '', 'Der Irdene Ring', NULL, 23360), -- 37836
(37835, 'deDE', 'Dorn Roterde', '', 'Der Irdene Ring', NULL, 23360), -- 37835
(37817, 'deDE', 'Zang''do', '', 'Der Irdene Ring', NULL, 23360), -- 37817
(37834, 'deDE', 'Tauna Himmelsjäger', '', 'Der Irdene Ring', NULL, 23360), -- 37834
(37812, 'deDE', 'Mahka', '', 'Der Irdene Ring', NULL, 23360), -- 37812
(37559, 'deDE', 'Savanneneber', '', NULL, NULL, 23360), -- 37559
(39156, 'deDE', 'Doppelzopfs Leibwache', '', NULL, NULL, 23360), -- 39156
(39155, 'deDE', 'Marley Doppelzopf', '', NULL, NULL, 23360), -- 39155
(5847, 'deDE', 'Heggin Steinbart', '', 'Bael''duns Chefingenieur', NULL, 23360), -- 5847
(5849, 'deDE', 'Buddler Flammenschmied', '', 'Ausgrabungsspezialist', NULL, 23360), -- 5849
(39174, 'deDE', 'Feegly der Verbannte', '', NULL, NULL, 23360), -- 39174
(5851, 'deDE', 'Hauptmann Gerogg Hammerzeh', '', 'Bael''duns Hauptmann der Wache', NULL, 23360), -- 5851
(38109, 'deDE', 'Zwergische Artilleriegranate', '', NULL, 'Interact', 23360), -- 38109
(38119, 'deDE', 'Artillery Shell Rear Chain', '', NULL, NULL, 23360), -- 38119
(38118, 'deDE', 'Artillery Shell Front Chain', '', NULL, NULL, 23360), -- 38118
(38127, 'deDE', 'Artillery Shell Rear Mount', '', NULL, NULL, 23360), -- 38127
(38190, 'deDE', 'Zwergische Artilleriegranate', '', NULL, 'Interact', 23360), -- 38190
(38115, 'deDE', 'Artillery Shell Front Mount', '', NULL, NULL, 23360), -- 38115
(38140, 'deDE', 'Fux Schattenschleicher', '', NULL, NULL, 23360), -- 38140
(3375, 'deDE', 'Großknecht von Bael''dun', '', NULL, NULL, 23360), -- 3375
(3374, 'deDE', 'Ausgräber von Bael''dun', '', NULL, NULL, 23360), -- 3374
(38183, 'deDE', 'Haggis Armok', '', 'Festungsarchitekt', NULL, 23360), -- 38183
(38215, 'deDE', 'Bael Modan Burning Airplane Bunny', '', NULL, NULL, 23360), -- 38215
(3378, 'deDE', 'Offizier von Bael''dun', '', NULL, NULL, 23360), -- 3378
(39153, 'deDE', 'Räuber der Ausgrabungsstätte', '', NULL, NULL, 23360), -- 39153
(3444, 'deDE', 'Grubenratte', '', NULL, NULL, 23360), -- 3444
(3377, 'deDE', 'Scharfschütze von Bael''dun', '', NULL, NULL, 23360), -- 3377
(5848, 'deDE', 'Malgin Gerstenbräu', '', 'Bael''duns Vertrauensoffizier', NULL, 23360), -- 5848
(3376, 'deDE', 'Soldat von Bael''dun', '', NULL, NULL, 23360), -- 3376
(38290, 'deDE', 'Kriegerheld des Brachlands', '', NULL, NULL, 23360), -- 38290
(3341, 'deDE', 'Gann Steinkeil', '', NULL, NULL, 23360), -- 3341
(39697, 'deDE', 'Nato Regenbaum', '', NULL, NULL, 23360), -- 39697
(37553, 'deDE', 'Gestörter Erdelementar', '', NULL, NULL, 23360), -- 37553
(37908, 'deDE', 'Calder Grau', '', NULL, NULL, 23360), -- 37908
(44305, 'deDE', 'Trank', '', 'Sprengstoff', NULL, 23360), -- 44305
(37924, 'deDE', 'Agent des Brachlands', '', 'Feldagent des SI:7', NULL, 23360), -- 37924
(38070, 'deDE', 'Ödniswache', '', NULL, NULL, 23360), -- 38070
(39009, 'deDE', 'Hawthornes Ross', '', NULL, NULL, 23360), -- 39009
(38323, 'deDE', 'General Hawthorne', '', NULL, NULL, 23360), -- 38323
(38986, 'deDE', 'Botschafter Gaines', '', NULL, NULL, 23360), -- 38986
(39024, 'deDE', 'Karl', '', NULL, NULL, 23360), -- 39024
(39003, 'deDE', 'Sam Trawley', '', NULL, NULL, 23360), -- 39003
(37669, 'deDE', 'Fields of Blood Capture Point Bunny', '', NULL, NULL, 23360), -- 37669
(37585, 'deDE', 'Aufklärer der Nordwacht', '', NULL, NULL, 23360), -- 37585
(44302, 'deDE', 'Rüstmeister Winfred', '', 'Gemischtwaren', NULL, 23360), -- 44302
(39005, 'deDE', 'Scharfschütze der Nordwacht', '', NULL, NULL, 23360), -- 39005
(6247, 'deDE', 'Doan Karhan', '', NULL, NULL, 23360), -- 6247
(6244, 'deDE', 'Takar der Seher', '', NULL, NULL, 23360), -- 6244
(37219, 'deDE', 'Barrens Fire Cyclone', '', NULL, NULL, 23360), -- 37219
(37092, 'deDE', 'Auswuchs', '', NULL, NULL, 23360), -- 37092
(37091, 'deDE', 'Deviatebenenschreiter', '', NULL, NULL, 23360), -- 37091
(37090, 'deDE', 'Deviatschreckenszahn', '', NULL, NULL, 23360), -- 37090
(37199, 'deDE', 'Invisible Stalker', '', NULL, NULL, 23360), -- 37199
(37082, 'deDE', 'Staubhufgiraffe', '', NULL, NULL, 23360), -- 37082
(37088, 'deDE', 'Altes Zhevra', '', NULL, NULL, 23360), -- 37088
(37093, 'deDE', 'Peitschenranke', '', NULL, NULL, 23360), -- 37093
(37083, 'deDE', 'Schreckenszahnläufer', '', NULL, NULL, 23360), -- 37083
(38873, 'deDE', 'Goucho', '', 'Lederverarbeitungsbedarf', NULL, 23360), -- 38873
(38876, 'deDE', 'Hannah Brückwässer', '', NULL, NULL, 23360), -- 38876
(38875, 'deDE', 'Schwarzer Peter', '', NULL, NULL, 23360), -- 38875
(38871, 'deDE', 'Korporal Teegan', '', NULL, NULL, 23360), -- 38871
(37084, 'deDE', 'Schreckenszahnsensenklaue', '', NULL, NULL, 23360), -- 37084
(62127, 'deDE', 'Smaragdboa', '', NULL, 'wildpetcapturable', 23360), -- 62127
(19444, 'deDE', 'Arbeiter', '', NULL, NULL, 23360), -- 19444
(34640, 'deDE', 'Brachlandgeier', '', NULL, NULL, 23360), -- 34640
(49725, 'deDE', 'Smaragdboa', '', NULL, NULL, 23360), -- 49725
(37738, 'deDE', 'Entkräftete Otter', '', NULL, 'LootAll', 23360), -- 37738
(37086, 'deDE', 'Fleischreißerstrauchdieb', '', NULL, NULL, 23360), -- 37086
(38941, 'deDE', 'Dreizahn', '', NULL, NULL, 23360), -- 38941
(38940, 'deDE', 'Deviatgreisin', '', NULL, NULL, 23360), -- 38940
(37511, 'deDE', 'Klingenwächter der Borstennacken', '', NULL, NULL, 23360), -- 37511
(44279, 'deDE', 'Donnach', '', 'Schmiedekunstbedarf', NULL, 23360), -- 44279
(39025, 'deDE', 'Shaina', '', NULL, NULL, 23360), -- 39025
(38884, 'deDE', 'Deviatdornweber', '', NULL, NULL, 23360), -- 38884
(37085, 'deDE', 'Gewaltiger Ebenenschreiter', '', NULL, NULL, 23360), -- 37085
(62130, 'deDE', 'Giraffenkalb', '', NULL, 'wildpetcapturable', 23360), -- 62130
(3240, 'deDE', 'Sturmschnauze', '', NULL, NULL, 23360), -- 3240
(37206, 'deDE', 'Ebenenstreuner', '', NULL, NULL, 23360), -- 37206
(38015, 'deDE', 'Karthog', '', NULL, NULL, 23360), -- 38015
(37659, 'deDE', 'Feldverteidiger der Horde', '', NULL, NULL, 23360), -- 37659
(37207, 'deDE', 'Ebenenstolzmähne', '', NULL, NULL, 23360), -- 37207
(37971, 'deDE', 'Ödnishimmelsreißer', '', NULL, NULL, 23360), -- 37971
(37922, 'deDE', 'Ödnisräuber', '', NULL, NULL, 23360), -- 37922
(44301, 'deDE', 'Roger Sternbach', '', 'Handwerkswaren', NULL, 23360), -- 44301
(44280, 'deDE', 'Serena Bogenlicht', '', 'Ingenieursbedarf', NULL, 23360), -- 44280
(44304, 'deDE', 'Rüstmeister Higgins', '', 'Gemischtwaren', NULL, 23360), -- 44304
(39211, 'deDE', 'Steve Stevenson', '', 'Flugmeister', NULL, 23360), -- 39211
(39124, 'deDE', 'Mizzys Lehrling', '', NULL, NULL, 23360), -- 39124
(39094, 'deDE', 'Barton Trask', '', NULL, NULL, 23360), -- 39094
(39085, 'deDE', 'Logan Krallenschlag', '', NULL, NULL, 23360), -- 39085
(39084, 'deDE', 'Mizzy Kolbenhammer', '', NULL, NULL, 23360), -- 39084
(39083, 'deDE', 'Kommandant Roberts', '', NULL, NULL, 23360), -- 39083
(39006, 'deDE', 'Belagerungspanzeringenieur', '', NULL, NULL, 23360), -- 39006
(44347, 'deDE', 'Werner Osterbruch', '', 'Stallmeister', NULL, 23360), -- 44347
(44267, 'deDE', 'Logistikoffizierin Salista', '', 'Gastwirtin', NULL, 23360), -- 44267
(39129, 'deDE', 'Bäuerlicher Hilfsarbeiter', '', NULL, NULL, 23360), -- 39129
(38055, 'deDE', 'Silithid Mound Bunny', '', NULL, NULL, 23360), -- 38055
(37926, 'deDE', 'Hauptmann der Triumphfeste', '', NULL, NULL, 23360), -- 37926
(37923, 'deDE', 'Vorhut der Triumphfeste', '', NULL, NULL, 23360), -- 37923
(39136, 'deDE', 'Wachposten der Triumphfeste', '', NULL, NULL, 23360), -- 39136
(37974, 'deDE', 'Ödniswindreiter', '', NULL, NULL, 23360), -- 37974
(39139, 'deDE', 'Scharfschütze der Triumphfeste', '', NULL, NULL, 23360), -- 39139
(37925, 'deDE', 'Pionierspezialist', '', NULL, NULL, 23360), -- 37925
(3248, 'deDE', 'Brachlandgiraffe', '', NULL, NULL, 23360), -- 3248
(37978, 'deDE', 'Kriegsgreif der Triumphfeste', '', NULL, NULL, 23360), -- 37978
(37977, 'deDE', 'Söldner der Wildhämmer', '', NULL, NULL, 23360), -- 37977
(5832, 'deDE', 'Donnerstampfer', '', NULL, NULL, 23360), -- 5832
(37208, 'deDE', 'Donnerkopf', '', NULL, NULL, 23360), -- 37208
(37555, 'deDE', 'Landbeberkodo', '', NULL, NULL, 23360), -- 37555
(37557, 'deDE', 'Donnerfalkenwolkenkitzler', '', NULL, NULL, 23360), -- 37557
(43742, 'deDE', 'Rachsüchtiger Beschützer', '', NULL, 'vehichleCursor', 23420), -- 43742
(33913, 'deDE', 'Shatterspear Hut Fire Bunny', '', NULL, NULL, 23420), -- 33913
(33178, 'deDE', 'Jägerin Sandrya Mondsturz', '', NULL, NULL, 23420), -- 33178
(32969, 'deDE', 'Schildwache von Lor''danel', '', NULL, NULL, 23420), -- 32969
(33071, 'deDE', 'Räuber der Splitterspeere', '', NULL, NULL, 23420), -- 33071
(32862, 'deDE', 'Jor''kil der Seelenreißer', '', NULL, NULL, 23420), -- 32862
(32858, 'deDE', 'Champion der Splitterspeere', '', NULL, NULL, 23420), -- 32858
(33115, 'deDE', 'Schildwache von Lor''danel', '', NULL, NULL, 23420), -- 33115
(11956, 'deDE', 'Großer Bärengeist', '', NULL, NULL, 23420), -- 11956
(11799, 'deDE', 'Tajarri', '', NULL, NULL, 23420), -- 11799
(102432, 'deDE', 'Malfurion Sturmgrimm', '', NULL, NULL, 23420), -- 102432
(11832, 'deDE', 'Bewahrer Remulos', '', NULL, NULL, 23420), -- 11832
(101627, 'deDE', 'Jadeeule', '', NULL, NULL, 23420), -- 101627
(12740, 'deDE', 'Faustron', '', 'Windreitermeister', NULL, 23420), -- 12740
(15719, 'deDE', 'Feiernder von Donnerfels', '', NULL, NULL, 23420), -- 15719
(15694, 'deDE', 'Feiernder von Sturmwind', '', NULL, NULL, 23420), -- 15694
(15906, 'deDE', 'Feiernder von Eisenschmiede', '', NULL, NULL, 23420), -- 15906
(66416, 'deDE', 'Leuchtfeuer', '', NULL, NULL, 23420), -- 66416
(66417, 'deDE', 'Grünchen', '', NULL, NULL, 23420), -- 66417
(66414, 'deDE', 'Seidenschwinge', '', NULL, NULL, 23420), -- 66414
(66412, 'deDE', 'Elena Flatterflug', '', 'Meistertierzähmerin', NULL, 23420), -- 66412
(15907, 'deDE', 'Feiernder von Unterstadt', '', NULL, NULL, 23420), -- 15907
(15905, 'deDE', 'Feiernder von Darnassus', '', NULL, NULL, 23420), -- 15905
(15908, 'deDE', 'Feiernder von Orgrimmar', '', NULL, NULL, 23420), -- 15908
(11801, 'deDE', 'Rabine Saturna', '', NULL, NULL, 23420), -- 11801
(12024, 'deDE', 'Meliri', '', 'Waffenschmiedin', NULL, 23420), -- 12024
(12025, 'deDE', 'Malvor', '', 'Kräuterkundelehrer', NULL, 23420), -- 12025
(11798, 'deDE', 'Bunthen Steppenwind', '', 'Flugmeister von Donnerfels', 'Taxi', 23420), -- 11798
(11797, 'deDE', 'Moren Flusslauf', '', NULL, NULL, 23420), -- 11797
(39865, 'deDE', 'Botschafterin Windweise', '', NULL, NULL, 23420), -- 39865
(11800, 'deDE', 'Silva Fil''naveth', '', 'Flugmeisterin von Darnassus', 'Taxi', 23420), -- 11800
(70164, 'deDE', 'Vater Brunet', '', NULL, NULL, 23420), -- 70164
(70163, 'deDE', 'Mutter Igarashi', '', NULL, NULL, 23420), -- 70163
(12026, 'deDE', 'My''lanna', '', 'Speis & Trank', NULL, 23420), -- 12026
(11939, 'deDE', 'Umber', '', NULL, NULL, 23420), -- 11939
(12021, 'deDE', 'Daeolyn Sommerblatt', '', 'Gemischtwaren', NULL, 23420), -- 12021
(11796, 'deDE', 'Bessany Steppenwind', '', NULL, NULL, 23420), -- 11796
(12022, 'deDE', 'Lorelae Winterklang', '', 'Handwerkswaren', NULL, 23420), -- 12022
(39140, 'deDE', 'Aronus', '', NULL, 'vehichleCursor', 23420), -- 39140
(13476, 'deDE', 'Zen''Balai', '', 'Druidenlehrerin', NULL, 23420), -- 13476
(12019, 'deDE', 'Dargon', '', 'Speis & Trank', NULL, 23420), -- 12019
(11795, 'deDE', 'Mylentha Flusslauf', '', NULL, NULL, 23420), -- 11795
(7940, 'deDE', 'Darnall', '', 'Schneiderbedarf', 'Buy', 23420), -- 7940
(4184, 'deDE', 'Geenia Sonnenfleck', '', 'Spezialdamenschneiderin', NULL, 23420), -- 4184
(53563, 'deDE', 'Aufseher der Mondlichtung', '', NULL, NULL, 23420), -- 53563
(12023, 'deDE', 'Kharedon', '', 'Händler für leichte Rüstungen', NULL, 23420), -- 12023
(11802, 'deDE', 'Dendrite Sternenschauer', '', NULL, NULL, 23420), -- 11802
(12042, 'deDE', 'Loganaar', '', 'Druidenlehrer', NULL, 23420), -- 12042
(15864, 'deDE', 'Valadar Sternensang', '', 'Sammler für Münzen der Urahnen', NULL, 23420), -- 15864
(15909, 'deDE', 'Fariel Sternensang', '', 'Sammlerin für Münzen der Urahnen', NULL, 23420), -- 15909
(15961, 'deDE', 'Schildwache des Mondfests', '', NULL, NULL, 23420), -- 15961
(11716, 'deDE', 'Celes Irdenschoß', '', NULL, NULL, 23420), -- 11716
(12029, 'deDE', 'Narianna', '', 'Bogenmacherin', NULL, 23420), -- 12029
(11822, 'deDE', 'Aufseher der Mondlichtung', '', NULL, NULL, 23420), -- 11822
(22889, 'deDE', 'Manifestation des Alptraums', '', NULL, NULL, 23420), -- 22889
(22902, 'deDE', 'Geisterhafter Peitscher', '', NULL, NULL, 23420), -- 22902
(22837, 'deDE', 'Traumwächter Lurosa', '', NULL, NULL, 23420), -- 22837
(22835, 'deDE', 'Cenarischer Traumwächter', '', NULL, NULL, 23420), -- 22835
(103421, 'deDE', 'Ritualhase', '', NULL, NULL, 23420), -- 103421
(15467, 'deDE', 'Omen', '', NULL, NULL, 23420), -- 15467
(15466, 'deDE', 'Diener von Omen', '', NULL, NULL, 23420), -- 15466
(11957, 'deDE', 'Großer Katzengeist', '', NULL, NULL, 23420), -- 11957
(49735, 'deDE', 'Harlekinfrosch', '', NULL, NULL, 23420), -- 49735
(10897, 'deDE', 'Sindrayl', '', 'Hippogryphenmeisterin', NULL, 23420), -- 10897
(34886, 'deDE', 'Kalimdoradler', '', NULL, NULL, 23420), -- 34886
(34982, 'deDE', 'Smaragdgrüner Geist', '', NULL, NULL, 23420), -- 34982
(32639, 'deDE', 'Gnimo', '', 'Abenteuerlicher Tüftler', NULL, 23420), -- 32639
(32638, 'deDE', 'Hakmud von Argus', '', 'Handelsreisender', NULL, 23420), -- 32638
(51840, 'deDE', 'Kriegerheld des Geistwandlerpostens', '', NULL, NULL, 23420), -- 51840
(18241, 'deDE', 'Krusti', '', NULL, NULL, 23420), -- 18241
(62177, 'deDE', 'Waldmotte', '', NULL, 'wildpetcapturable', 23360), -- 62177
(62312, 'deDE', 'Frosch', '', NULL, 'wildpetcapturable', 23360), -- 62312
(36591, 'deDE', 'Frosch', '', NULL, NULL, 23360), -- 36591
(34212, 'deDE', 'Ashenvale Assassin Bunny 00', '', NULL, NULL, 23360), -- 34212
(3820, 'deDE', 'Wilddorngiftspucker', '', NULL, NULL, 23360), -- 3820
(34195, 'deDE', 'Kulg Blutspritzer', '', NULL, NULL, 23360), -- 34195
(3817, 'deDE', 'Schattenhornhirsch', '', NULL, NULL, 23360), -- 3817
(50043, 'deDE', 'Handelsprinz Donais', '', NULL, NULL, 23360), -- 50043
(52093, 'deDE', 'Zanagan Mischmeister', '', 'Schankkellner', NULL, 23360), -- 52093
(48343, 'deDE', 'Goblinischer Cocktail', '', NULL, 'Pickup', 23360), -- 48343
(48342, 'deDE', 'Goblinischer Cocktail', '', NULL, 'Pickup', 23360), -- 48342
(48341, 'deDE', 'Goblinischer Cocktail', '', NULL, 'Pickup', 23360), -- 48341
(48340, 'deDE', 'Getränketablett', '', NULL, NULL, 23360), -- 48340
(50045, 'deDE', 'Kellnerin in Gallywix'' Lustschloss', '', NULL, NULL, 23360), -- 50045
(52092, 'deDE', 'Izak Mischmeister', '', 'Schankkellner', NULL, 23360), -- 52092
(52039, 'deDE', 'ELM General Purpose Bunny (scale x0.75)', '', NULL, NULL, 23360), -- 52039
(52018, 'deDE', 'Palastscherge', '', NULL, 'Directions', 23360), -- 52018
(113940, 'deDE', 'Unsichtbarer Pirscher', '', NULL, NULL, 23360), -- 113940
(36822, 'deDE', 'Lord Kassarus', '', NULL, NULL, 23360), -- 36822
(6194, 'deDE', 'Schlangenwache der Grollflossen', '', NULL, NULL, 23360), -- 6194
(37143, 'deDE', 'Heart of Arkkoroc Aura Bunny', '', NULL, NULL, 23360), -- 37143
(36873, 'deDE', 'Hohepriesterin Silthera', '', NULL, NULL, 23360), -- 36873
(49900, 'deDE', 'Lawinus Maximus', '', 'Schamanenlehrer', NULL, 23360), -- 49900
(49901, 'deDE', 'Schwester Goldglanz', '', 'Priesterlehrerin', NULL, 23360), -- 49901
(49896, 'deDE', 'Zisch Zündel', '', 'Magierlehrer', NULL, 23360), -- 49896
(43773, 'deDE', 'Stella Bummbumm', '', 'Stallmeisterin', NULL, 23360), -- 43773
(42804, 'deDE', 'Bilgewässer Prominente', '', NULL, NULL, 23360), -- 42804
(36988, 'deDE', 'Gefreiter Permudo', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36988
(49909, 'deDE', 'Stinkie Scharfschauder', '', 'Schurkenlehrer', NULL, 23360), -- 49909
(49902, 'deDE', 'Kriegomatic NX-01', '', 'Kriegerlehrer', NULL, 23360), -- 49902
(49894, 'deDE', 'Bäm Megabombe', '', 'Jägerlehrer', NULL, 23360), -- 49894
(49885, 'deDE', 'HGK Ausbildomat Deluxe', '', 'Verkäufer und Lehrer für einen Beruf', NULL, 23360), -- 49885
(49882, 'deDE', 'Schmuddel Schmutzfinger', '', 'Gastwirt', NULL, 23360), -- 49882
(49879, 'deDE', 'Doc Knalldüse', '', 'Lehrer für Erste Hilfe', NULL, 23360), -- 49879
(43774, 'deDE', 'Feena Leichtsaft', '', 'Gifte & Reagenzien', NULL, 23360), -- 43774
(43771, 'deDE', 'Mixi', '', 'Gastwirtin', NULL, 23360), -- 43771
(51509, 'deDE', 'Bilgewässer Haudrauf', '', NULL, NULL, 23360), -- 51509
(42647, 'deDE', 'Krieger der Axtbeißer', '', NULL, NULL, 23360), -- 42647
(49884, 'deDE', 'Sally "Schrotti" Sandschrauber', '', 'Rüstungsverkäuferin', NULL, 23360), -- 49884
(42641, 'deDE', 'Kriegsfürst Krogg', '', NULL, NULL, 23360), -- 42641
(42640, 'deDE', 'Kapitän Krazz', '', NULL, NULL, 23360), -- 42640
(36959, 'deDE', 'Onkel Tollhaus', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36959
(37009, 'deDE', 'Bleenik Zischlunte', '', NULL, NULL, 23360), -- 37009
(70165, 'deDE', 'Julie Fabernackel', '', 'Goblinressourcenverwalterin', NULL, 23360), -- 70165
(36975, 'deDE', 'Volt', '', 'Goblintrupp', NULL, 23360), -- 36975
(36999, 'deDE', 'Teemo', '', NULL, NULL, 23360), -- 36999
(37010, 'deDE', 'Artillerietechniker', '', NULL, NULL, 23360), -- 37010
(37139, 'deDE', 'Stahlflügel', '', NULL, 'vehichleCursor', 23360), -- 37139
(36972, 'deDE', 'Grit', '', 'Goblintrupp', NULL, 23360), -- 36972
(36787, 'deDE', 'Ölkanister', '', NULL, NULL, 23360), -- 36787
(49880, 'deDE', 'Vorarbeiter Feuchtlunte', '', NULL, NULL, 23360), -- 49880
(36956, 'deDE', 'Hauptmann Desoto', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36956
(36917, 'deDE', 'Oberfläche-zu-Oberfläche-Transportrakete', '', NULL, 'vehichleCursor', 23360), -- 36917
(49881, 'deDE', 'Gizmo Gangzwinger', '', NULL, NULL, 23360), -- 49881
(43304, 'deDE', 'Heron Tuns', '', 'Munitionsspezialist', NULL, 23360), -- 43304
(50302, 'deDE', 'Importierter scheckiger Eber', '', NULL, NULL, 23360), -- 50302
(50301, 'deDE', 'Goblinischer Bewunderer', '', NULL, NULL, 23360), -- 50301
(49892, 'deDE', 'Sudsy Magee', '', NULL, NULL, 23360), -- 49892
(43301, 'deDE', 'Devon Rackled', '', 'Munitionsspezialist', NULL, 23360), -- 43301
(49891, 'deDE', 'Präz der Lufthammer', '', NULL, NULL, 23360), -- 49891
(49890, 'deDE', 'Frankie Gangschalter', '', NULL, NULL, 23360), -- 49890
(35867, 'deDE', 'Kalec', '', NULL, NULL, 23360), -- 35867
(49878, 'deDE', 'Trainer Blutgrätsch', '', 'Die Bilgewasserbukaniere', NULL, 23360), -- 49878
(42643, 'deDE', 'Fliep', '', NULL, NULL, 23360), -- 42643
(37142, 'deDE', 'Gurlorn', '', 'Windreiterveteran', NULL, 23360), -- 37142
(8610, 'deDE', 'Kroum', '', 'Windreitermeister', NULL, 23360), -- 8610
(42836, 'deDE', 'Bilgewässer Mechaniker', '', NULL, NULL, 23360), -- 42836
(43772, 'deDE', 'Stek Erznarr', '', 'Schmiedekunstbedarf', NULL, 23360), -- 43772
(42786, 'deDE', 'Bilgewässer Dockarbeiter', '', NULL, NULL, 23360), -- 42786
(49883, 'deDE', 'Izzy', '', NULL, NULL, 23360), -- 49883
(49875, 'deDE', 'Das Ass', '', NULL, NULL, 23360), -- 49875
(36974, 'deDE', 'Molch', '', 'Goblintrupp', NULL, 23360), -- 36974
(36973, 'deDE', 'Flick', '', 'Goblintrupp', NULL, 23360), -- 36973
(49895, 'deDE', 'Bosa Blick', '', 'Hexenmeisterlehrer', NULL, 23360), -- 49895
(51142, 'deDE', 'Joeyray', '', 'Smutje', NULL, 23360), -- 51142
(42777, 'deDE', 'Bilgewässer Großknecht', '', NULL, NULL, 23360), -- 42777
(42644, 'deDE', 'Smoot', '', NULL, NULL, 23360), -- 42644
(49887, 'deDE', 'Gappy Silberzahn', '', 'Klunkerhändler', NULL, 23360), -- 49887
(49876, 'deDE', 'Brett "Die Münze" McQuid', '', 'Gemischtwaren', NULL, 23360), -- 49876
(36976, 'deDE', 'Ticker', '', 'Goblintrupp', NULL, 23360), -- 36976
(49888, 'deDE', 'Szabo', '', 'Maßschneider', NULL, 23360), -- 49888
(49886, 'deDE', 'Missa Gläsa', '', 'Sonnenbrillenhändlerin', NULL, 23360), -- 49886
(37141, 'deDE', 'Airborne Priest Takeoff Controller', '', NULL, NULL, 23360), -- 37141
(37140, 'deDE', 'Fliegender Priester', '', 'Goblinmilitär', NULL, 23360), -- 37140
(43776, 'deDE', 'Zizo Meerbrutzel', '', 'Angelbedarf', NULL, 23360), -- 43776
(42646, 'deDE', 'Fahnenflüchtiger Grunzer', '', NULL, NULL, 23360), -- 42646
(43330, 'deDE', 'Blitz'' Gyrokopter', '', NULL, NULL, 23360), -- 43330
(43328, 'deDE', 'Blitz Kawummsospazz', '', 'Flugmeister', NULL, 23360), -- 43328
(37064, 'deDE', 'Feno Bummdübel', '', 'Bilgewässer Bataillon', NULL, 23360), -- 37064
(37061, 'deDE', 'Jellix Luntenzünder', '', 'Bilgewässer Bataillon', NULL, 23360), -- 37061
(43710, 'deDE', 'Meezle Drehfunke', '', 'Ingenieursbedarf', NULL, 23360), -- 43710
(36649, 'deDE', 'Malicion', '', NULL, NULL, 23360), -- 36649
(49861, 'deDE', 'Zwielichtkäfer', '', NULL, NULL, 23360), -- 49861
(36638, 'deDE', 'Zwielichtfürstin Katrana', '', NULL, NULL, 23360), -- 36638
(36637, 'deDE', 'Zwielichtschänder', '', NULL, NULL, 23360), -- 36637
(36722, 'deDE', 'Bilgewässer Hilfsarbeiter', '', NULL, NULL, 23360), -- 36722
(36336, 'deDE', 'Abbild von Erzmagier Xylem', '', NULL, NULL, 23360), -- 36336
(36364, 'deDE', 'Henny', '', 'Xylems Lehrling', NULL, 23360), -- 36364
(35064, 'deDE', 'Auge des Frosts', '', NULL, NULL, 23360), -- 35064
(36636, 'deDE', 'Zwielichtdrachenjäger', '', NULL, NULL, 23360), -- 36636
(35065, 'deDE', 'Eye of Frost Beam Bunny', '', NULL, NULL, 23360), -- 35065
(35142, 'deDE', 'Ergll', '', NULL, NULL, 23360), -- 35142
(36131, 'deDE', 'Übler Platscher', '', NULL, NULL, 23360), -- 36131
(6372, 'deDE', 'Makrinnischnappklaue', '', NULL, NULL, 23360), -- 6372
(36639, 'deDE', 'Finsterer Drakonid', '', NULL, NULL, 23360), -- 36639
(36640, 'deDE', 'Finsterer Drache', '', NULL, NULL, 23360), -- 36640
(36334, 'deDE', 'Abbild von Erzmagier Xylem', '', NULL, NULL, 23360), -- 36334
(36363, 'deDE', 'Lobos', '', 'Xylems Lehrling', NULL, 23360), -- 36363
(36408, 'deDE', 'Arcane Credit Bunny', '', NULL, NULL, 23360), -- 36408
(36361, 'deDE', 'Abbild von Erzmagier Xylem', '', NULL, NULL, 23360), -- 36361
(37900, 'deDE', 'Lobos Portal Bunny', '', NULL, NULL, 23360), -- 37900
(36367, 'deDE', 'Phloem', '', 'Xylems Lehrling', NULL, 23360), -- 36367
(36365, 'deDE', 'Zoey Wizzelfunk', '', 'Xylems Lehrling', NULL, 23360), -- 36365
(36366, 'deDE', 'Raethas Morgenseher', '', 'Xylems Lehrling', NULL, 23360), -- 36366
(35171, 'deDE', 'Arcane Detonation Bunny', '', NULL, NULL, 23360), -- 35171
(43709, 'deDE', 'Feezle Drehfunke', '', 'Ingenieursbedarf', NULL, 23360), -- 43709
(36210, 'deDE', 'Sorata Feuerspinner', '', 'Erschöpfter Lehrling', NULL, 23360), -- 36210
(36596, 'deDE', 'Andorel Sonnenschwur', '', NULL, NULL, 23360), -- 36596
(8586, 'deDE', 'Haggrum Blutfaust', '', NULL, NULL, 23360), -- 8586
(35648, 'deDE', 'Will Robotronik', '', NULL, NULL, 23360), -- 35648
(35754, 'deDE', 'Quarla Pfeifenbruch', '', 'Xylems Lehrling', NULL, 23360), -- 35754
(35756, 'deDE', 'Teresa Halmblatt', '', 'Xylems Lehrling', NULL, 23360), -- 35756
(35755, 'deDE', 'Tharkul Eisenschädel', '', 'Xylems Lehrling', NULL, 23360), -- 35755
(35187, 'deDE', 'Abbild von Erzmagier Xylem', '', NULL, NULL, 23360), -- 35187
(35759, 'deDE', 'Balboa', '', NULL, NULL, 23360), -- 35759
(6202, 'deDE', 'Höllenrufer der Legashi', '', NULL, NULL, 23360), -- 6202
(6200, 'deDE', 'Satyr der Legashi', '', NULL, NULL, 23360), -- 6200
(6201, 'deDE', 'Schurke der Legashi', '', NULL, NULL, 23360), -- 6201
(36599, 'deDE', 'Arkanes Konstrukt', '', NULL, NULL, 23360), -- 36599
(36594, 'deDE', 'Wissenshüterin Goldwind', '', NULL, NULL, 23360), -- 36594
(36592, 'deDE', 'Angehender Ermittler', '', NULL, NULL, 23360), -- 36592
(36593, 'deDE', 'Angehender Illuminator', '', NULL, NULL, 23360), -- 36593
(36611, 'deDE', 'Biologe der Talrendisspitze', '', NULL, NULL, 23360), -- 36611
(36987, 'deDE', 'Unteroffizier Dynamo', '', 'Bilgewässer Bataillon', 'LootAll', 23360), -- 36987
(8408, 'deDE', 'Kriegsherr Krellian', '', NULL, NULL, 23360), -- 8408
(43711, 'deDE', 'Leezle Drehfunke', '', 'Ingenieursbedarf', NULL, 23360), -- 43711
(36370, 'deDE', 'Lux', '', 'Xylems Lehrling', NULL, 23360), -- 36370
(36376, 'deDE', 'Fib Gyroschock', '', 'Xylems Lehrling', NULL, 23360), -- 36376
(36372, 'deDE', 'Joanna', '', 'Xylems Lehrling', NULL, 23360), -- 36372
(36371, 'deDE', 'Morta der Elende', '', 'Xylems Lehrling', NULL, 23360), -- 36371
(36375, 'deDE', 'Tex Vortaspul', '', 'Reagenzien', NULL, 23360), -- 36375
(36377, 'deDE', 'Beseelter Besen', '', NULL, NULL, 23360), -- 36377
(37152, 'deDE', 'Abbild von Erzmagier Xylem', '', NULL, NULL, 23360), -- 37152
(36373, 'deDE', 'Coral Mondsturm', '', 'Xylems Lehrling', NULL, 23360), -- 36373
(36374, 'deDE', 'Nyrill', '', 'Xylems Lehrling', NULL, 23360), -- 36374
(50299, 'deDE', 'Sanath Lim-yo', '', 'Lehrling von Xylem', NULL, 23360), -- 50299
(6651, 'deDE', 'Torwächter Donnerschrei', '', NULL, NULL, 23360), -- 6651
(36618, 'deDE', 'Ungarl', '', 'Häuptling des Schwarzschlunds', NULL, 23360), -- 36618
(36614, 'deDE', 'Botschafterin der Talrendisspitze', '', NULL, NULL, 23360), -- 36614
(36012, 'deDE', 'Schamane des Schwarzschlunds', '', NULL, NULL, 23360), -- 36012
(36015, 'deDE', 'Pfadfinder des Schwarzschlunds', '', NULL, NULL, 23360), -- 36015
(36013, 'deDE', 'Krieger des Schwarzschlunds', '', NULL, NULL, 23360), -- 36013
(36843, 'deDE', 'Runestone Bunny', '', NULL, NULL, 23360), -- 36843
(36800, 'deDE', 'Cover Bunny', '', NULL, NULL, 23360), -- 36800
(36795, 'deDE', 'Ruckus', '', NULL, NULL, 23360), -- 36795
(36951, 'deDE', 'Real Estate Bunny (Vista)', '', NULL, NULL, 23360), -- 36951
(36952, 'deDE', 'Real Estate Bunny (Tower)', '', NULL, NULL, 23360), -- 36952
(13896, 'deDE', 'Schuppenbart', '', NULL, NULL, 23360), -- 13896
(36921, 'deDE', 'Unteroffizier Hort', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36921
(36920, 'deDE', 'Leutnant Drex', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36920
(36953, 'deDE', 'Real Estate Bunny (Pagoda)', '', NULL, NULL, 23360), -- 36953
(6649, 'deDE', 'Lady Sesspira', '', NULL, NULL, 23360), -- 6649
(6195, 'deDE', 'Sirene der Grollflossen', '', NULL, NULL, 23360), -- 6195
(37740, 'deDE', 'Gelbflossenhai', '', NULL, NULL, 23360), -- 37740
(35881, 'deDE', 'Lady Vesthra', '', NULL, NULL, 23360), -- 35881
(35968, 'deDE', 'Vesthras Kraftstein', '', NULL, NULL, 23360), -- 35968
(15600, 'deDE', 'Urahne Himmelsglanz', '', NULL, NULL, 23360), -- 15600
(35892, 'deDE', 'Silisthras Kraftstein', '', NULL, NULL, 23360), -- 35892
(35880, 'deDE', 'Lady Silisthra', '', NULL, NULL, 23360), -- 35880
(6650, 'deDE', 'General Fangferror', '', NULL, NULL, 23360), -- 6650
(6350, 'deDE', 'Makrinniklingenklaue', '', NULL, NULL, 23360), -- 6350
(6370, 'deDE', 'Makrinnischarrer', '', NULL, NULL, 23360), -- 6370
(36125, 'deDE', 'Rotes, sich drehendes Warnblinklicht', '', NULL, NULL, 23360), -- 36125
(36958, 'deDE', 'Bulliger Laborgoblin', '', NULL, NULL, 23360), -- 36958
(36541, 'deDE', 'Versuchsobjekt vier', '', NULL, NULL, 23360), -- 36541
(36527, 'deDE', 'Die Sausestern', '', NULL, NULL, 23360), -- 36527
(36500, 'deDE', 'Versuchsobjekt neun', '', NULL, NULL, 23360), -- 36500
(35484, 'deDE', 'Erkundungsausrüstung', '', NULL, NULL, 23360), -- 35484
(107464, 'deDE', 'Verängstigter Praktikant', '', NULL, NULL, 23360), -- 107464
(35833, 'deDE', 'Priesterin der Grollflossen', '', NULL, NULL, 23360), -- 35833
(35832, 'deDE', 'Seeruferin der Grollflossen', '', NULL, NULL, 23360), -- 35832
(35831, 'deDE', 'Sturmwut der Grollflossen', '', NULL, NULL, 23360), -- 35831
(35829, 'deDE', 'Juniorbombardier Hackel', '', NULL, NULL, 23360), -- 35829
(35817, 'deDE', 'Bombardierhauptmann Smooks', '', NULL, NULL, 23360), -- 35817
(37015, 'deDE', 'Friz'' Gyrokopter', '', NULL, NULL, 23360), -- 37015
(37005, 'deDE', 'Friz Bodendreh', '', 'Flugmeister', NULL, 23360), -- 37005
(43705, 'deDE', 'Geezle Drehfunke', '', 'Ingenieursbedarf', NULL, 23360), -- 43705
(36379, 'deDE', 'Hobart Wurfhammer', '', NULL, NULL, 23360), -- 36379
(35657, 'deDE', 'Torg Zweimalmer', '', NULL, NULL, 23360), -- 35657
(36157, 'deDE', 'Mutiertes Tentakel', '', NULL, NULL, 23360), -- 36157
(36156, 'deDE', 'Mutierter Goblin', '', NULL, NULL, 23360), -- 36156
(36472, 'deDE', 'Türklingel', '', 'Archiv des Geheimlabors', 'Interact', 23360), -- 36472
(36146, 'deDE', 'Twistex Glückszange', '', 'Tierversuchsleiter', NULL, 23360), -- 36146
(36509, 'deDE', 'Versuchsraptor', '', NULL, NULL, 23360), -- 36509
(49774, 'deDE', 'Tollwütiger Nussschädling 5000', '', NULL, NULL, 23360), -- 49774
(43708, 'deDE', 'Beezle Drehfunke', '', 'Ingenieursbedarf', NULL, 23360), -- 43708
(35091, 'deDE', 'Horzak Zignibbel', '', NULL, NULL, 23360), -- 35091
(35085, 'deDE', 'Großmagd Fisk', '', NULL, NULL, 23360), -- 35085
(35526, 'deDE', 'Clubniks Planierraupe', '', NULL, NULL, 23360), -- 35526
(35088, 'deDE', 'Custer Clubnik', '', NULL, NULL, 23360), -- 35088
(35087, 'deDE', 'Malynea Himmelshäscher', '', NULL, NULL, 23360), -- 35087
(36752, 'deDE', 'Gefreiter Worcester', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36752
(6193, 'deDE', 'Kreischerin der Grollflossen', '', NULL, NULL, 23360), -- 6193
(36756, 'deDE', 'Toter Soldat', '', 'Bilgewässer Bataillon', 'LootAll', 23360), -- 36756
(36748, 'deDE', 'Hauptmann Grektar', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36748
(36768, 'deDE', 'Bilgewässer Mörser', '', NULL, 'Gunner', 23360), -- 36768
(36749, 'deDE', 'Kommandant Molotov', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36749
(36746, 'deDE', 'Xiz "Das Auge" Salvoschuss', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36746
(36744, 'deDE', 'Glix Wetzschloss', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36744
(6196, 'deDE', 'Myrmidone der Grollflossen', '', NULL, NULL, 23360), -- 6196
(7885, 'deDE', 'Kampfmeister der Grollflossen', '', NULL, NULL, 23360), -- 7885
(36868, 'deDE', 'Versklavter Sohn von Arkkoroc', '', NULL, NULL, 23360), -- 36868
(36922, 'deDE', 'Verwundeter Soldat', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36922
(36932, 'deDE', 'Azshara Bombing Bunny', '', NULL, NULL, 23360), -- 36932
(36925, 'deDE', 'Soldat von Bilgewasser', '', 'Bilgewässer Bataillon', NULL, 23360), -- 36925
(37741, 'deDE', 'Bilgewasserrobbe', '', NULL, NULL, 23360), -- 37741
(6352, 'deDE', 'Korallenpanzerschleicher', '', NULL, NULL, 23360), -- 6352
(7886, 'deDE', 'Verzauberin der Grollflossen', '', NULL, NULL, 23360), -- 7886
(6190, 'deDE', 'Krieger der Grollflossen', '', NULL, NULL, 23360), -- 6190
(36437, 'deDE', 'Ratte der Raketenbahn', '', NULL, 'vehichleCursor', 23360), -- 36437
(62120, 'deDE', 'Tollwütiger Nussschädling 5000', '', NULL, 'wildpetcapturable', 23360), -- 62120
(36077, 'deDE', 'Assistentin Greely', '', NULL, NULL, 23360), -- 36077
(36126, 'deDE', 'Azsharite Core Cart Summon Bunny', '', NULL, NULL, 23360), -- 36126
(36297, 'deDE', 'Gormungan', '', NULL, NULL, 23360), -- 36297
(36025, 'deDE', 'Geheimlaborfeuer', '', NULL, NULL, 23360), -- 36025
(36061, 'deDE', 'Laborpraktikant', '', NULL, NULL, 23360), -- 36061
(36147, 'deDE', 'Statisch aufgeladener Hippogryph', '', NULL, NULL, 23360), -- 36147
(36989, 'deDE', 'Eindringling der Grollflossen', '', NULL, NULL, 23360), -- 36989
(62121, 'deDE', 'Türkisschildkröte', '', NULL, 'wildpetcapturable', 23360), -- 62121
(36407, 'deDE', 'Bingham Federdings', '', NULL, NULL, 23360), -- 36407
(36385, 'deDE', 'Netzgewehrgnom', '', NULL, NULL, 23360), -- 36385
(36384, 'deDE', 'Britzelgnom', '', NULL, NULL, 23360), -- 36384
(35466, 'deDE', 'Ruheloser Geist', '', NULL, NULL, 23360), -- 35466
(43244, 'deDE', 'Rocketway Event Bunny (CSA)', '', NULL, NULL, 23360), -- 43244
(36304, 'deDE', 'Nebelschwingenkliffbewohner', '', NULL, NULL, 23360), -- 36304
(43217, 'deDE', 'Bilgewässer Raketenreiter', '', NULL, NULL, 23360), -- 43217
(36785, 'deDE', 'Bombardierhauptmann Smooks', '', NULL, NULL, 23360), -- 36785
(36662, 'deDE', 'Gorek', '', NULL, NULL, 23360), -- 36662
(36887, 'deDE', 'Bombenziel der Talrendisspitze', '', NULL, NULL, 23360), -- 36887
(36884, 'deDE', 'Kommandant Jarrodenus', '', NULL, NULL, 23360), -- 36884
(36918, 'deDE', 'Beschwörungsstein der Wissenshüter', '', NULL, NULL, 23360), -- 36918
(36720, 'deDE', 'Tower Scaling Vehicle Seat 03', '', NULL, NULL, 23360), -- 36720
(36688, 'deDE', 'Leibwächter der Nordwacht', '', NULL, NULL, 23360), -- 36688
(36687, 'deDE', 'Mariel Morgenlied', '', NULL, NULL, 23360), -- 36687
(36680, 'deDE', 'Hauptmann Grunwald', '', 'Expeditionseinheit der Nordwacht', NULL, 23360), -- 36680
(36755, 'deDE', 'Tower Scaling Vehicle NPC Seat 03', '', NULL, NULL, 23360), -- 36755
(36754, 'deDE', 'Tower Scaling Vehicle NPC Seat 02', '', NULL, NULL, 23360), -- 36754
(36753, 'deDE', 'Tower Scaling Vehicle NPC Seat 01', '', NULL, NULL, 23360), -- 36753
(36718, 'deDE', 'Tower Scaling Vehicle Seat 02', '', NULL, NULL, 23360), -- 36718
(36716, 'deDE', 'Tower Scaling Vehicle Seat 01', '', NULL, NULL, 23360), -- 36716
(36654, 'deDE', 'Dave''s Big Giant Industrial Light and Magic Bunny', '', NULL, NULL, 23360), -- 36654
(36729, 'deDE', 'Slinky Scharfklinge', '', NULL, NULL, 23360), -- 36729
(36738, 'deDE', 'Azshara 4.x GrappleBunny 02', '', NULL, NULL, 23360), -- 36738
(36707, 'deDE', 'Azshara 4.x GrappleBunny 01', '', NULL, NULL, 23360), -- 36707
(36815, 'deDE', 'Grunzer von Valormok', '', NULL, NULL, 23360), -- 36815
(36903, 'deDE', 'Juniorbombardier Hackel', '', NULL, NULL, 23360), -- 36903
(36902, 'deDE', 'Windreiter Gorsch', '', 'Höllschreis Höllenhunde', NULL, 23360), -- 36902
(36900, 'deDE', 'Gestrandeter Windreiter', '', NULL, 'vehichleCursor', 23360), -- 36900
(36730, 'deDE', 'Chawg', '', NULL, NULL, 23360), -- 36730
(36728, 'deDE', 'Kroum', '', 'Windreiterhauptmann', NULL, 23360), -- 36728
(36919, 'deDE', 'Andorel Sonnenschwur', '', NULL, NULL, 23360), -- 36919
(6377, 'deDE', 'Donnerkopfflügler', '', NULL, NULL, 23360), -- 6377
(37002, 'deDE', 'Klippenkracher', '', NULL, NULL, 23360), -- 37002
(8660, 'deDE', 'Evalcharr', '', NULL, NULL, 23360), -- 8660
(8764, 'deDE', 'Nebelschwingenverheerer', '', NULL, NULL, 23360), -- 8764
(8761, 'deDE', 'Mooshufrenner', '', NULL, NULL, 23360), -- 8761
(6375, 'deDE', 'Donnerkopfhippogryph', '', NULL, NULL, 23360), -- 6375
(36660, 'deDE', 'Schütze der Talrendisspitze', '', NULL, NULL, 23360), -- 36660
(36849, 'deDE', 'Wissenshüterin der Talrendisspitze', '', NULL, NULL, 23360), -- 36849
(36914, 'deDE', 'Schildwache der Talrendisspitze', '', NULL, NULL, 23360), -- 36914
(36816, 'deDE', 'Verteidiger der Talrendisspitze', '', NULL, NULL, 23360), -- 36816
(36852, 'deDE', 'Himmelsjägerhippogryph', '', NULL, NULL, 23360), -- 36852
(36850, 'deDE', 'Talrendis Himmelsjäger', '', NULL, NULL, 23360), -- 36850
(36890, 'deDE', 'Glevenschleuder der Talrendisspitze', '', NULL, NULL, 23360), -- 36890
(36665, 'deDE', 'Angriffswindreiter des Kriegshymnenklans', '', NULL, NULL, 23360), -- 36665
(36673, 'deDE', 'Bombenschütze von Azshara', '', NULL, NULL, 23360), -- 36673
(36936, 'deDE', 'Bergfußgrunzer', '', NULL, NULL, 23360), -- 36936
(35312, 'deDE', 'Saboteur der Talrendisspitze', '', NULL, NULL, 23360), -- 35312
(35095, 'deDE', 'Späher der Talrendisspitze', '', NULL, NULL, 23360), -- 35095
(35296, 'deDE', 'Sprengstoffkiste', '', NULL, NULL, 23360), -- 35296
(35245, 'deDE', 'Grausteinbasilisk', '', NULL, NULL, 23360), -- 35245
(35257, 'deDE', 'Bergfußminenarbeiter', '', NULL, 'Interact', 23360), -- 35257
(35111, 'deDE', 'Außer Kontrolle geratener Schredder', '', NULL, 'vehichleCursor', 23360), -- 35111
(34626, 'deDE', 'Jerrik Hochberg', '', NULL, NULL, 23360), -- 34626
(34638, 'deDE', 'Sho''e', '', NULL, NULL, 23360), -- 34638
(34634, 'deDE', 'Gorgal Zornesnarbe', '', NULL, NULL, 23360), -- 34634
(34846, 'deDE', 'Wyneth', '', NULL, NULL, 23360), -- 34846
(5841, 'deDE', 'Felslanze', '', NULL, NULL, 23360), -- 5841
(5838, 'deDE', 'Bruchspeer', '', NULL, NULL, 23360), -- 5838
(3397, 'deDE', 'Blutstürmer der Kolkar', '', NULL, NULL, 23360), -- 3397
(4316, 'deDE', 'Rudelhund der Kolkar', '', NULL, NULL, 23360), -- 4316
(3274, 'deDE', 'Rudelläufer der Kolkar', '', NULL, NULL, 23360), -- 3274
(34967, 'deDE', 'Raptor Nest Bunny', '', NULL, NULL, 23360), -- 34967
(38820, 'deDE', 'Klingenwerfer von Donnerschrei', '', NULL, NULL, 23360), -- 38820
(34729, 'deDE', 'Charlie', '', NULL, NULL, 23360), -- 34729
(3467, 'deDE', 'Baron Küstenschipper', '', 'Südmeerfreibeuter', NULL, 23360), -- 3467
(34727, 'deDE', 'Der schläfrige Joe', '', NULL, NULL, 23360), -- 34727
(34752, 'deDE', 'Leutnant Pyre', '', 'Expeditionseinheit der Nordwacht', NULL, 23360), -- 34752
(4975, 'deDE', 'Theramorezielscheibe 2', '', NULL, NULL, 23360), -- 4975
(3641, 'deDE', 'Deviatlauerer', '', NULL, NULL, 23360), -- 3641
(3655, 'deDE', 'Zausel der Verrückte', '', NULL, NULL, 23360), -- 3655
(3638, 'deDE', 'Verschlingendes Ektoplasma', '', NULL, NULL, 23360), -- 3638
(3634, 'deDE', 'Deviatpirscher', '', NULL, NULL, 23360), -- 3634
(20797, 'deDE', 'Deviatwinderjungtier', '', NULL, NULL, 23360), -- 20797
(3630, 'deDE', 'Deviatwinder', '', NULL, NULL, 23360), -- 3630
(3631, 'deDE', 'Deviatdornenpeitscher', '', NULL, NULL, 23360), -- 3631
(3835, 'deDE', 'Gallkröte', '', NULL, NULL, 23360), -- 3835
(3633, 'deDE', 'Deviattöter', '', NULL, NULL, 23360), -- 3633
(3632, 'deDE', 'Deviatkrabbler', '', NULL, NULL, 23360), -- 3632
(44170, 'deDE', 'Hezruls Blutwache', '', NULL, NULL, 23360), -- 44170
(3396, 'deDE', 'Hezrul Blutmal', '', NULL, NULL, 23360), -- 3396
(3398, 'deDE', 'Gesharahan', '', NULL, NULL, 23360), -- 3398
(3254, 'deDE', 'Sonnenschuppenschmetterschwanz', '', NULL, NULL, 23360), -- 3254
(4005, 'deDE', 'Kluftmooskrabbler', '', NULL, NULL, 23360), -- 4005
(12034, 'deDE', 'Koiter', '', NULL, NULL, 23360), -- 12034
(37167, 'deDE', 'Gefangener aus dem Steinkrallengebirge', '', NULL, NULL, 23360), -- 37167
(37157, 'deDE', 'Hauptmann Peake', '', NULL, NULL, 23360), -- 37157
(37136, 'deDE', 'Oltarg', '', NULL, NULL, 23360), -- 37136
(40995, 'deDE', 'Azurblaues Eisenerz', '', NULL, NULL, 23360), -- 40995
(14781, 'deDE', 'Hauptmann Schädelbrecher', '', NULL, NULL, 23360), -- 14781
(14754, 'deDE', 'Kelm Hargunth', '', 'Versorgungsoffizier des Kriegshymnenklans', NULL, 23360), -- 14754
(14718, 'deDE', 'Hilfsarbeiter der Horde', '', NULL, NULL, 23360), -- 14718
(19910, 'deDE', 'Gargok', '', 'Kampfmeister der Kriegshymnenschlucht', NULL, 23360), -- 19910
(14717, 'deDE', 'Elitesoldat der Horde', '', NULL, NULL, 23360), -- 14717
(71001, 'deDE', 'Söldnerschredder', '', NULL, NULL, 23360), -- 71001
(70999, 'deDE', 'Marodeur der Kor''kron', '', NULL, NULL, 23360), -- 70999
(70997, 'deDE', 'Höllschreis Arbeiter', '', NULL, NULL, 23360), -- 70997
(34841, 'deDE', 'Telar Hochschreiter', '', NULL, NULL, 23360), -- 34841
(34613, 'deDE', 'Ta''jari', '', NULL, NULL, 23360), -- 34613
(3470, 'deDE', 'Rathorian', '', NULL, NULL, 23360), -- 3470
(52312, 'deDE', 'Xelnaz', '', NULL, NULL, 23360), -- 52312
(3986, 'deDE', 'Sarilus Faulmut', '', NULL, NULL, 23360), -- 3986
(60736, 'deDE', 'Chain Bunny', '', NULL, NULL, 23360), -- 60736
(52526, 'deDE', 'Ruby''s Dance Vehicle', '', NULL, NULL, 23360), -- 52526
(52460, 'deDE', 'Rubin', '', NULL, NULL, 23360), -- 52460
(52456, 'deDE', 'Jesi', '', NULL, NULL, 23360), -- 52456
(52448, 'deDE', 'Gef', '', NULL, NULL, 23360), -- 52448
(52457, 'deDE', 'Kodokalbling', '', NULL, NULL, 23360), -- 52457
(14892, 'deDE', 'Reißzahn', '', NULL, NULL, 23360), -- 14892
(14857, 'deDE', 'Erk', '', NULL, NULL, 23360), -- 14857
(71011, 'deDE', 'Schlachtwolf der Kor''kron', '', NULL, NULL, 23360), -- 71011
(73590, 'deDE', 'Vorhut der Kor''kron', '', NULL, NULL, 23360), -- 73590
(71010, 'deDE', 'Vorhut der Kor''kron', '', NULL, NULL, 23360), -- 71010
(3236, 'deDE', 'Brachlandkodo', '', NULL, NULL, 23360), -- 3236
(71012, 'deDE', 'Schlächter der Kor''kron', '', NULL, NULL, 23360), -- 71012
(3389, 'deDE', 'Regthar Totenfurt', '', NULL, NULL, 23360), -- 3389
(9990, 'deDE', 'Lanti''gah', '', NULL, NULL, 23360), -- 9990
(5837, 'deDE', 'Steinarm', '', NULL, NULL, 23360), -- 5837
(3394, 'deDE', 'Barak Kodobann', '', NULL, NULL, 23360), -- 3394
(3449, 'deDE', 'Darsok Schnelldolch', '', NULL, NULL, 23360), -- 3449
(43955, 'deDE', 'Naman', '', 'Händlerin für Kettenrüstungen', NULL, 23360), -- 43955
(52196, 'deDE', 'Haudrauf der Brennenden Klinge', '', NULL, NULL, 23360), -- 52196
(5830, 'deDE', 'Schwester Wildkralle', '', NULL, NULL, 23360), -- 5830
(3452, 'deDE', 'Serena Blutfeder', '', NULL, NULL, 23360), -- 3452
(3280, 'deDE', 'Windruferin der Hexenschwingen', '', NULL, NULL, 23360), -- 3280
(3279, 'deDE', 'Wegelagerin der Hexenschwingen', '', NULL, NULL, 23360), -- 3279
(3278, 'deDE', 'Töterin der Hexenschwingen', '', NULL, NULL, 23360), -- 3278
(3682, 'deDE', 'Vrang Wildblut', '', 'Waffen- & Rüstungsmacher', NULL, 23360), -- 3682
(3277, 'deDE', 'Wildfeder der Hexenschwingen', '', NULL, NULL, 23360), -- 3277
(3276, 'deDE', 'Harpyie der Hexenschwingen', '', NULL, NULL, 23360), -- 3276
(3380, 'deDE', 'Akolyth der Brennenden Klinge', '', NULL, NULL, 23360), -- 3380
(5865, 'deDE', 'Dishu', '', NULL, NULL, 23360), -- 5865
(43951, 'deDE', 'Durnok', '', 'Lederrüstungshändler', NULL, 23360), -- 43951
(43946, 'deDE', 'Gastwirtin Kerntis', '', 'Gastwirtin', NULL, 23360), -- 43946
(14873, 'deDE', 'Okla', '', NULL, NULL, 23360), -- 14873
(43988, 'deDE', 'Carthok', '', 'Stallmeister', NULL, 23360), -- 43988
(34509, 'deDE', 'Ralton', '', NULL, NULL, 23360), -- 34509
(52207, 'deDE', 'Nagala Peitschenschwinger', '', 'Karawanenlieferantin', NULL, 23360), -- 52207
(52227, 'deDE', 'Balgor Peitschenschwinger', '', 'Karawanenführer', NULL, 23360), -- 52227
(71006, 'deDE', 'Aufseher der Kor''kron', '', NULL, NULL, 23360), -- 71006
(5842, 'deDE', 'Takk der Springer', '', NULL, NULL, 23360), -- 5842
(116079, 'deDE', 'Sperbitak', '', NULL, NULL, 23360), -- 116079
(116078, 'deDE', 'Bussi', '', NULL, NULL, 23360), -- 116078
(116077, 'deDE', 'Falko', '', NULL, NULL, 23360), -- 116077
(115286, 'deDE', 'Crysa', '', 'Zähmerin fliegender Haustiere', NULL, 23360), -- 115286
(50870, 'deDE', 'Baxter', '', NULL, NULL, 23360), -- 50870
(3439, 'deDE', 'Kurbelwizz'' Schredder', '', NULL, NULL, 23360), -- 3439
(5836, 'deDE', 'Ingenieur Wirbelgig', '', NULL, NULL, 23360), -- 5836
(3295, 'deDE', 'Schlickanomalie', '', NULL, NULL, 23360), -- 3295
(52357, 'deDE', 'Söldner der Venture Co.', '', NULL, NULL, 23360), -- 52357
(52356, 'deDE', 'Zwangsarbeiter der Venture Co.', '', NULL, NULL, 23360), -- 52356
(5835, 'deDE', 'Großknecht Grills', '', NULL, NULL, 23360), -- 5835
(3445, 'deDE', 'Vorsteher Lugwizz', '', NULL, NULL, 23360), -- 3445
(71005, 'deDE', 'Verwüster der Kor''kron', '', NULL, NULL, 23360), -- 71005
(14909, 'deDE', 'Pooka', '', NULL, NULL, 23360), -- 14909
(14908, 'deDE', 'Mogg', '', NULL, NULL, 23360), -- 14908
(52192, 'deDE', 'Brogor', '', NULL, NULL, 23360), -- 52192
(9316, 'deDE', 'Wenikee Kübelbolz', '', NULL, NULL, 23360), -- 9316
(3379, 'deDE', 'Haudrauf der Brennenden Klinge', '', NULL, NULL, 23360), -- 3379
(34647, 'deDE', 'Brachlandeber', '', NULL, NULL, 23360), -- 34647
(52338, 'deDE', 'Angreifer der Brennenden Klinge', '', NULL, NULL, 23360), -- 52338
(3471, 'deDE', 'Tüftler Kichers', '', NULL, NULL, 23360), -- 3471
(3285, 'deDE', 'Peon der Venture Co.', '', NULL, NULL, 23360), -- 3285
(71002, 'deDE', 'Einäscherer der Kor''kron', '', NULL, NULL, 23360), -- 71002
(71000, 'deDE', 'Söldneringenieur', '', NULL, NULL, 23360), -- 71000
(4127, 'deDE', 'Fleischreißerhyäne', '', NULL, NULL, 23360), -- 4127
(7310, 'deDE', 'Mutierte Drohne der Venture Co.', '', NULL, NULL, 23360), -- 7310
(7307, 'deDE', 'Aufklärer der Venture Co.', '', NULL, NULL, 23360), -- 7307
(7288, 'deDE', 'Oberster Großknecht Puzik Gallywix', '', NULL, NULL, 23360), -- 7288
(7287, 'deDE', 'Großknecht Silixiz', '', NULL, NULL, 23360), -- 7287
(7067, 'deDE', 'Drohne der Venture Co.', '', NULL, NULL, 23360), -- 7067
(7308, 'deDE', 'Patrouille der Venture Co.', '', NULL, NULL, 23360), -- 7308
(7233, 'deDE', 'Zuchtmeister Fitzel', '', NULL, NULL, 23360), -- 7233
(3282, 'deDE', 'Söldner der Venture Co.', '', NULL, NULL, 23360), -- 3282
(52171, 'deDE', 'Schlammige Spuren', '', NULL, 'Inspect', 23360), -- 52171
(3284, 'deDE', 'Zwangsarbeiter der Venture Co.', '', NULL, NULL, 23360), -- 3284
(43982, 'deDE', 'Vernon Sauerspreu', '', 'Stallmeister', NULL, 23360), -- 43982
(40558, 'deDE', 'Gazrix', '', 'Flugmeister', NULL, 23360), -- 40558
(3442, 'deDE', 'Stotterspritter', '', 'Unabhängiger Vertragspartner', NULL, 23360), -- 3442
(43945, 'deDE', 'Gastwirtin Kritzle', '', 'Gastwirtin', NULL, 23360), -- 43945
(34698, 'deDE', 'Düsentopf', '', NULL, NULL, 23360), -- 34698
(43957, 'deDE', 'Frazzik', '', 'Rüstungsschmied & Schildmacher', NULL, 23360), -- 43957
(34674, 'deDE', 'Brak Polterrohr', '', NULL, NULL, 23360), -- 34674
(43953, 'deDE', 'Niriap', '', 'Gemischtwaren', NULL, 23360), -- 43953
(44167, 'deDE', 'Sonnenschuppenraptor', '', NULL, NULL, 23360), -- 44167
(34730, 'deDE', 'Navigator Zippik', '', 'Die Zephyr', NULL, 23360), -- 34730
(34723, 'deDE', 'Behüter Tolwe', '', 'Die Zephyr', NULL, 23360), -- 34723
(34721, 'deDE', 'Leitende Offizierin Ograh', '', 'Die Zephyr', NULL, 23360), -- 34721
(34719, 'deDE', 'Besatzungsmitglied Schroter', '', 'Die Zephyr', NULL, 23360), -- 34719
(34718, 'deDE', 'Besatzungsmitglied Schlossriegel', '', 'Die Zephyr', NULL, 23360), -- 34718
(34717, 'deDE', 'Besatzungsmitglied Rohrschlüssel', '', 'Die Zephyr', NULL, 23360), -- 34717
(34715, 'deDE', 'Himmelskapitän "Staubwolke" Sprengteufel', '', 'Die Zephyr', NULL, 23360), -- 34715
(3084, 'deDE', 'Behüter von Donnerfels', '', NULL, 'Directions', 23360), -- 3084
(34513, 'deDE', 'Togrik', '', NULL, NULL, 23360), -- 34513
(5907, 'deDE', 'Kranal Fiss', '', NULL, NULL, 23360), -- 5907
(14894, 'deDE', 'Bienenschwarm', '', NULL, NULL, 23360), -- 14894
(14874, 'deDE', 'Karu', '', NULL, NULL, 23360), -- 14874
(14872, 'deDE', 'Trok', '', NULL, NULL, 23360), -- 14872
(10685, 'deDE', 'Schwein', '', NULL, NULL, 23360), -- 10685
(3432, 'deDE', 'Mankrik', '', NULL, NULL, 23360), -- 3432
(34504, 'deDE', 'Grunzer von Grol''dom', '', NULL, NULL, 23360), -- 34504
(34563, 'deDE', 'Verletzter Grunzer von Grol''dom', '', NULL, NULL, 23360), -- 34563
(34560, 'deDE', 'Una Wolfsklaue', '', NULL, NULL, 23360), -- 34560
(34578, 'deDE', 'Rocco Peitschenschwinger', '', 'Karawanenführer', NULL, 23360), -- 34578
(14893, 'deDE', 'Wache Kurall', '', NULL, NULL, 23360), -- 14893
(3487, 'deDE', 'Kalyimah Sturmwolke', '', 'Taschen & Beutel', NULL, 23360), -- 3487
(3486, 'deDE', 'Halija Schneegänger', '', 'Tuchmacherin', NULL, 23360), -- 3486
(3488, 'deDE', 'Uthrok', '', 'Bogen- & Büchsenmacher', NULL, 23360), -- 3488
(3478, 'deDE', 'Traugh', '', 'Schmiedekunstlehrer', NULL, 23360), -- 3478
(3477, 'deDE', 'Hraq', '', 'Schmiedekunstbedarf', NULL, 23360), -- 3477
(3483, 'deDE', 'Jahan Falkenschwinge', '', 'Händler für Leder- & Kettenrüstungen', NULL, 23360), -- 3483
(3479, 'deDE', 'Nargal Totauge', '', 'Waffenschmied', NULL, 23360), -- 3479
(3428, 'deDE', 'Korran', '', NULL, NULL, 23360), -- 3428
(15597, 'deDE', 'Urahne Mondwacht', '', NULL, NULL, 23360), -- 15597
(3615, 'deDE', 'Devrak', '', 'Windreitermeister', NULL, 23360), -- 3615
(3490, 'deDE', 'Hula''mahi', '', 'Reagenzien, Kräuter & Gifte', NULL, 23360), -- 3490
(3431, 'deDE', 'Grenthar', '', NULL, NULL, 23360), -- 3431
(3429, 'deDE', 'Thork', '', NULL, NULL, 23360), -- 3429
(3390, 'deDE', 'Apotheker Helbrim', '', NULL, NULL, 23360), -- 3390
(3485, 'deDE', 'Wrahk', '', 'Schneiderbedarf', NULL, 23360), -- 3485
(3484, 'deDE', 'Kil''hala', '', 'Schneiderlehrer', NULL, 23360), -- 3484
(50033, 'deDE', 'Ging', '', 'Kriegerlehrer', NULL, 23360), -- 50033
(9981, 'deDE', 'Sikwa', '', 'Stallmeisterin', NULL, 23360), -- 9981
(5871, 'deDE', 'Larhka', '', 'Getränkehändler', NULL, 23360), -- 5871
(5774, 'deDE', 'Reitwolf', '', NULL, NULL, 23360), -- 5774
(3934, 'deDE', 'Gastwirt Boorand Steppenwind', '', 'Gastwirt', NULL, 23360), -- 3934
(3482, 'deDE', 'Tari''qa', '', 'Handwerkswaren', NULL, 23360), -- 3482
(3481, 'deDE', 'Barg', '', 'Gemischtwaren', NULL, 23360), -- 3481
(3448, 'deDE', 'Tonga Runentotem', '', NULL, NULL, 23360), -- 3448
(18739, 'deDE', 'Giraffe', '', NULL, NULL, 23360), -- 18739
(34828, 'deDE', 'Kala''ma', '', NULL, NULL, 23360), -- 34828
(44164, 'deDE', 'Sonnenschuppenhetzer', '', NULL, NULL, 23360), -- 44164
(34547, 'deDE', 'Kodo von Grol''dom', '', NULL, NULL, 23360), -- 34547
(3271, 'deDE', 'Mystiker der Klingenmähnen', '', NULL, NULL, 23360), -- 3271
(34543, 'deDE', 'Fez', '', NULL, NULL, 23360), -- 34543
(34544, 'deDE', 'Tortusk', '', NULL, NULL, 23360), -- 34544
(3438, 'deDE', 'Kreenig Grantelschnauze', '', NULL, NULL, 23360), -- 3438
(34829, 'deDE', 'König Schnitterklaue', '', NULL, NULL, 23360), -- 34829
(44165, 'deDE', 'Sonnenschuppengefährtin', '', NULL, NULL, 23360), -- 44165
(3241, 'deDE', 'Savannenpatriarch', '', NULL, NULL, 23360), -- 3241
(5828, 'deDE', 'Humar der Rudellord', '', NULL, NULL, 23360), -- 5828
(3416, 'deDE', 'Savannenmatriarchin', '', NULL, NULL, 23360), -- 3416
(5831, 'deDE', 'Flinkmähne', '', NULL, NULL, 23360), -- 5831
(34576, 'deDE', 'Karawanenkodo des Wegekreuzes', '', NULL, NULL, 23360), -- 34576
(71211, 'deDE', 'Brachlandfels', '', NULL, NULL, 23360), -- 71211
(3245, 'deDE', 'Störrischer Ebenenschreiter', '', NULL, NULL, 23360), -- 3245
(34759, 'deDE', 'Zechender Peon', '', NULL, NULL, 23360), -- 34759
(34754, 'deDE', 'Chefingenieur Foote', '', 'Expeditionseinheit der Nordwacht', NULL, 23360), -- 34754
(34656, 'deDE', 'Hargash', '', 'Höllschreis Ermittler', NULL, 23360), -- 34656
(34651, 'deDE', 'Sashya', '', NULL, NULL, 23360), -- 34651
(6791, 'deDE', 'Gastwirt Wiley', '', 'Gastwirt', NULL, 23360), -- 6791
(6253, 'deDE', 'Akolyth Fenrick', '', NULL, NULL, 23360), -- 6253
(6252, 'deDE', 'Akolythin Magaz', '', NULL, NULL, 23360), -- 6252
(23536, 'deDE', 'Nagulon', '', 'Matero Zeshuwals Diener', NULL, 23360), -- 23536
(23535, 'deDE', 'Matero Zeshuwal', '', 'Dämonenausbilder', NULL, 23360), -- 23535
(23534, 'deDE', 'Babagaya Schattenkluft', '', 'Hexenmeisterlehrerin', NULL, 23360), -- 23534
(6254, 'deDE', 'Akolythin Wytula', '', NULL, NULL, 23360), -- 6254
(6266, 'deDE', 'Menara Leerbringer', '', NULL, NULL, 23360), -- 6266
(6251, 'deDE', 'Strahad Farsan', '', NULL, NULL, 23360), -- 6251
(7166, 'deDE', 'Wrenix'' Elektrodingsda-Apparat', '', NULL, NULL, 23360), -- 7166
(7161, 'deDE', 'Wrenix der Jämmerliche', '', NULL, NULL, 23360), -- 7161
(8738, 'deDE', 'Vazario Kuppelschmier', '', 'Lehrer für goblinsche Ingenieurskunst', NULL, 23360), -- 8738
(3391, 'deDE', 'Gazlowe', '', NULL, NULL, 23360), -- 3391
(3495, 'deDE', 'Quietschspross', '', 'Ingenieursbedarf', NULL, 23360), -- 3495
(3494, 'deDE', 'Tüftelwiz', '', 'Lehrer für Ingenieurskunst', NULL, 23360), -- 3494
(15582, 'deDE', 'Urahnin Windtotem', '', NULL, NULL, 23360), -- 15582
(10063, 'deDE', 'Reggifuz', '', 'Stallmeister', NULL, 23360), -- 10063
(8119, 'deDE', 'Zikkel', '', 'Bankier', NULL, 23360), -- 8119
(3496, 'deDE', 'Fuzzruckel', '', 'Bankier', NULL, 23360), -- 3496
(3572, 'deDE', 'Zizzek', '', 'Angelbedarf', NULL, 23360), -- 3572
(3339, 'deDE', 'Kapitän Thalo''thas Blendsonn', '', NULL, NULL, 23360), -- 3339
(3493, 'deDE', 'Grazlix', '', 'Rüstungsschmied & Schildmacher', NULL, 23360), -- 3493
(3491, 'deDE', 'Eisenzar', '', 'Waffenschmied', NULL, 23360), -- 3491
(3446, 'deDE', 'Mebok Mizzyrix', '', NULL, NULL, 23360), -- 3446
(3492, 'deDE', 'Grimmspindel', '', 'Tuch- & Lederrüstungshändler', NULL, 23360), -- 3492
(3499, 'deDE', 'Ranik', '', 'Handwerkswaren', NULL, 23360), -- 3499
(3292, 'deDE', 'Braumeister Drohn', '', NULL, NULL, 23360), -- 3292
(3498, 'deDE', 'Jazzik', '', 'Gemischtwaren', NULL, 23360), -- 3498
(3464, 'deDE', 'Gazrog', '', NULL, NULL, 23360), -- 3464
(16418, 'deDE', 'Mupsi Kettelfridd', '', NULL, NULL, 23360), -- 16418
(71007, 'deDE', 'Verbrenner der Kor''kron', '', NULL, NULL, 23360), -- 71007
(71207, 'deDE', 'Brachlandfels', '', NULL, NULL, 23360), -- 71207
(71188, 'deDE', 'Erderschütterer der Kor''kron', '', NULL, NULL, 23360), -- 71188
(71187, 'deDE', 'Wilde Erde', '', NULL, NULL, 23360), -- 71187
(14859, 'deDE', 'Wache Taruc', '', NULL, NULL, 23360), -- 14859
(71009, 'deDE', 'Flammenworg der Kor''kron', '', NULL, NULL, 23360), -- 71009
(8306, 'deDE', 'Duhng', '', 'Kochkunstlehrer', NULL, 23360), -- 8306
(3443, 'deDE', 'Grub', '', NULL, NULL, 23360), -- 3443
(14850, 'deDE', 'Gruk', '', NULL, NULL, 23360), -- 14850
(8307, 'deDE', 'Tarban Kornbäcker', '', 'Bäcker', NULL, 23360), -- 8307
(50028, 'deDE', 'Chintoka', '', 'Hexenmeisterlehrer', NULL, 23360), -- 50028
(3338, 'deDE', 'Sergra Dunkeldorn', '', NULL, NULL, 23360), -- 3338
(50029, 'deDE', 'Egnom', '', 'Magierlehrer', NULL, 23360), -- 50029
(50032, 'deDE', 'Tarmod', '', 'Jägerlehrer', NULL, 23360), -- 50032
(50027, 'deDE', 'Dargad', '', 'Schurkenlehrer', NULL, 23360), -- 50027
(3659, 'deDE', 'Jorb', '', 'Lizzariks Leibwache', NULL, 23360), -- 3659
(3658, 'deDE', 'Lizzarik', '', 'Waffenanbieter', NULL, 23360), -- 3658
(3480, 'deDE', 'Moorane Kornbäcker', '', 'Bäckerin', NULL, 23360), -- 3480
(3489, 'deDE', 'Zargh', '', 'Metzger', NULL, 23360), -- 3489
(34503, 'deDE', 'Plünderer der Klingenmähnen', '', NULL, NULL, 23360), -- 34503
(3269, 'deDE', 'Geomant der Klingenmähnen', '', NULL, NULL, 23360), -- 3269
(34545, 'deDE', 'Berauschter der Klingenmähnen', '', NULL, NULL, 23360), -- 34545
(3268, 'deDE', 'Dornenwirker der Klingenmähnen', '', NULL, NULL, 23360), -- 3268
(3266, 'deDE', 'Verteidiger der Klingenmähnen', '', NULL, NULL, 23360), -- 3266
(34287, 'deDE', 'Kettenursprung', '', NULL, NULL, 23360), -- 34287
(34285, 'deDE', 'Gefangener Wolf', '', NULL, NULL, 23360), -- 34285
(14901, 'deDE', 'Peon', '', NULL, NULL, 23360), -- 14901
(34284, 'deDE', 'Dorak', '', NULL, NULL, 23360), -- 34284
(5810, 'deDE', 'Uzzek', '', NULL, NULL, 23360), -- 5810
(43956, 'deDE', 'Lokarbo', '', 'Metzger', NULL, 23360), -- 43956
(51914, 'deDE', 'Wache der Horde', '', NULL, NULL, 23360), -- 51914
(43949, 'deDE', 'Martang', '', 'Tuch- & Lederrüstungshändler', NULL, 23360), -- 43949
(34258, 'deDE', 'Halga Blutauge', '', 'Karawanenführerin', NULL, 23360), -- 34258
(9336, 'deDE', 'Boss Kupferdraht', '', NULL, NULL, 23360), -- 9336
(3286, 'deDE', 'Vorarbeiter der Venture Co.', '', NULL, NULL, 23360), -- 3286
(3283, 'deDE', 'Vollstrecker der Venture Co.', '', NULL, NULL, 23360), -- 3283
(3296, 'deDE', 'Grunzer von Orgrimmar', '', NULL, 'Directions', 23360), -- 3296
(51346, 'deDE', 'Windreiter von Orgrimmar', '', NULL, 'Directions', 23360), -- 51346
(74228, 'deDE', 'Kopfjäger der Dunkelspeere', '', NULL, 'directions', 23360), -- 74228
(39596, 'deDE', 'Belebter Wasserwächter', '', NULL, NULL, 23360), -- 39596
(39595, 'deDE', 'Wütender Erdwächter', '', NULL, NULL, 23360), -- 39595
(39385, 'deDE', 'Hiebkreischer', '', NULL, NULL, 23360), -- 39385
(3127, 'deDE', 'Siechstachelskorpid', '', NULL, NULL, 23360), -- 3127
(3126, 'deDE', 'Gepanzerter Skorpid', '', NULL, NULL, 23360), -- 3126
(39324, 'deDE', 'Zen''Taji', '', NULL, NULL, 23360), -- 39324
(34259, 'deDE', 'Karawanenkodo', '', NULL, NULL, 23360), -- 34259
(3337, 'deDE', 'Kargal Schlachtnarbe', '', NULL, NULL, 23360), -- 3337
(34261, 'deDE', 'Karawanenpeon', '', NULL, NULL, 23360), -- 34261
(3521, 'deDE', 'Ak''zeloth', '', NULL, NULL, 23360), -- 3521
(3123, 'deDE', 'Blutkrallensensenschlund', '', NULL, NULL, 23360), -- 3123
(44166, 'deDE', 'Grasendes Zhevra', '', NULL, NULL, 23360), -- 44166
(34280, 'deDE', 'Barrens Burning Barn Fire Bunny', '', NULL, NULL, 23360), -- 34280
(3122, 'deDE', 'Blutkrallenschwanzpeitscher', '', NULL, NULL, 23360), -- 3122
(3267, 'deDE', 'Brandschatzer der Klingenmähnen', '', NULL, NULL, 23360), -- 3267
(39337, 'deDE', 'Launischer Ebenenschreiter', '', NULL, NULL, 23360), -- 39337
(3100, 'deDE', 'Alter scheckiger Eber', '', NULL, NULL, 23360), -- 3100
(39452, 'deDE', 'Schaudermagenzähneknirscher', '', NULL, NULL, 23360), -- 39452
(3113, 'deDE', 'Staubläufer der Klingenmähnen', '', NULL, NULL, 23360), -- 3113
(3114, 'deDE', 'Schlachtwache der Klingenmähnen', '', NULL, NULL, 23360), -- 3114
(3265, 'deDE', 'Jäger der Klingenmähnen', '', NULL, NULL, 23360), -- 3265
(3244, 'deDE', 'Großer Ebenenschreiter', '', NULL, NULL, 23360), -- 3244
(44057, 'deDE', 'Rennboot', '', NULL, 'vehichleCursor', 23360), -- 44057
(3110, 'deDE', 'Schaudermagenkrokilisk', '', NULL, NULL, 23360), -- 3110
(5435, 'deDE', 'Sandhai', '', NULL, NULL, 23360), -- 5435
(16227, 'deDE', 'Bragok', '', 'Flugmeister', NULL, 23360), -- 16227
(44058, 'deDE', 'Horton Hornbläser', '', NULL, NULL, 23360), -- 44058
(3497, 'deDE', 'Kilxx', '', 'Angler', NULL, 23360), -- 3497
(3665, 'deDE', 'Kranführer Moppelfuzz', '', NULL, NULL, 23360), -- 3665
(8496, 'deDE', 'Liv Ritzelflick', '', 'Werkstattassistentin', NULL, 23360), -- 8496
(9558, 'deDE', 'Grimbel', '', 'Schiffsmeister', NULL, 23360), -- 9558
(3502, 'deDE', 'Haudrauf von Ratschet', '', NULL, NULL, 23360), -- 3502
(3453, 'deDE', 'Werftmeister Flunkerblick', '', NULL, NULL, 23360), -- 3453
(5629, 'deDE', 'Kommandosoldat von Theramore', '', NULL, NULL, 23360), -- 5629
(5901, 'deDE', 'Islen Wasserdeuter', '', NULL, NULL, 23360), -- 5901
(3388, 'deDE', 'Mahren Himmelsdeuter', '', NULL, NULL, 23360), -- 3388
(34707, 'deDE', 'Matrose von Theramore', '', NULL, NULL, 23360), -- 34707
(3382, 'deDE', 'Kanonier der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 3382
(6240, 'deDE', 'Ringkampfherausforderer', '', NULL, NULL, 23360), -- 6240
(44168, 'deDE', 'Südmeerrekrut', '', NULL, NULL, 23360), -- 44168
(34804, 'deDE', 'Küchenchef Toofus', '', 'Südmeerpiraten', NULL, 23360), -- 34804
(34750, 'deDE', 'Käpt''n Garvey', '', 'Südmeerfreibeuter', NULL, 23360), -- 34750
(34749, 'deDE', 'Tony Zweihauer', '', NULL, NULL, 23360), -- 34749
(3476, 'deDE', 'Isha Awak', '', NULL, NULL, 23360), -- 3476
(25297, 'deDE', 'Ausbildungsattrappe', '', NULL, NULL, 23360), -- 25297
(34747, 'deDE', 'Glomp', '', 'Der Schatzmeister', NULL, 23360), -- 34747
(3384, 'deDE', 'Kaperer der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 3384
(3383, 'deDE', 'Halsabschneider der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 3383
(34706, 'deDE', 'Scharfschütze von Theramore', '', NULL, NULL, 23360), -- 34706
(4955, 'deDE', 'Theramorezielscheibe 1', '', NULL, NULL, 23360), -- 4955
(34733, 'deDE', 'Konteradmiral Hartley', '', 'Expeditionseinheit der Nordwacht', NULL, 23360), -- 34733
(35198, 'deDE', 'Urtum der Talrendisspitze', '', NULL, NULL, 23360), -- 35198
(35096, 'deDE', 'Geschwächter Mooshufhirsch', '', NULL, NULL, 23360), -- 35096
(11046, 'deDE', 'Whuut', '', 'Alchemielehrling', NULL, 23360), -- 11046
(8659, 'deDE', 'Jes''rimon', '', NULL, NULL, 23360), -- 8659
(3348, 'deDE', 'Kor''geld', '', 'Alchemiebedarf', NULL, 23360), -- 3348
(3347, 'deDE', 'Yelmak', '', 'Alchemielehrer', NULL, 23360), -- 3347
(58199, 'deDE', 'Lord Itharius', '', 'Botschafter des grünen Drachenschwarms', NULL, 23360), -- 58199
(51230, 'deDE', 'Erdheiler', '', NULL, NULL, 23360), -- 51230
(45244, 'deDE', 'Scharfseher Krogar', '', NULL, NULL, 23360), -- 45244
(44780, 'deDE', 'Isashi', '', 'Handwerkswaren', NULL, 23360), -- 44780
(47336, 'deDE', 'Grunzer von Baradin', '', NULL, NULL, 23360), -- 47336
(47321, 'deDE', 'Zugra Flammenfaust', '', 'Kampfmagierin von Tol Barad', NULL, 23360), -- 47321
(47335, 'deDE', 'Grunzer von Baradin', '', NULL, NULL, 23360), -- 47335
(54113, 'deDE', 'Spooks', '', 'Jagas Haustier', NULL, 23360), -- 54113
(44779, 'deDE', 'Owato', '', 'Gemischtwaren', NULL, 23360), -- 44779
(44770, 'deDE', 'Tatepi', '', 'Bankierin', NULL, 23360), -- 44770
(3403, 'deDE', 'Sian''tsu', '', 'Schamanenlehrerin', NULL, 23360), -- 3403
(13417, 'deDE', 'Sagorne Gratläufer', '', 'Schamanenlehrer', NULL, 23360), -- 13417
(5892, 'deDE', 'Searn Feuerwächter', '', NULL, NULL, 23360), -- 5892
(44785, 'deDE', 'Miwana', '', 'Gastwirtin', NULL, 23360), -- 44785
(44745, 'deDE', 'Sonnenläufer Atohmos Kodo', '', NULL, NULL, 23360), -- 44745
(44726, 'deDE', 'Shalla Weißblatt', '', 'Druidenlehrer', NULL, 23360), -- 44726
(44740, 'deDE', 'Sahi Wolkensänger', '', 'Schamanenlehrerin', NULL, 23360), -- 44740
(44735, 'deDE', 'Seherin Liwatha', '', 'Priesterlehrerin', NULL, 23360), -- 44735
(45230, 'deDE', 'Kriegerheld von Orgrimmar', '', NULL, 'Directions', 23360), -- 45230
(44788, 'deDE', 'Lonto', '', 'Stallmeister', NULL, 23360), -- 44788
(19176, 'deDE', 'Taurenbürger', '', NULL, NULL, 23360), -- 19176
(44787, 'deDE', 'Auktionatorin Sowata', '', NULL, NULL, 23360), -- 44787
(44782, 'deDE', 'Rento', '', 'Kürschnerlehrer', NULL, 23360), -- 44782
(15188, 'deDE', 'Cenarius'' Abgesandter Schwarzhuf', '', NULL, NULL, 23360), -- 15188
(44783, 'deDE', 'Hiwahi Dreifeder', '', 'Schneiderlehrerin', NULL, 23360), -- 44783
(3322, 'deDE', 'Kaja', '', 'Verkäuferin für Bögen & Gewehre', NULL, 23360), -- 3322
(44781, 'deDE', 'Opuno Eisenhorn', '', 'Schmiedekunstlehrer', NULL, 23360), -- 44781
(44743, 'deDE', 'Nohi Ebenenwanderer', '', 'Jägerlehrer', NULL, 23360), -- 44743
(44723, 'deDE', 'Nahu Wuthuf', '', 'Kriegerlehrer', NULL, 23360), -- 44723
(44725, 'deDE', 'Sonnenläufer Atohmo', '', 'Paladinlehrer', NULL, 23360), -- 44725
(69978, 'deDE', 'Unteroffizier Donnerhorn', '', 'Rüstmeister für kataklysmische Gladiatoren', NULL, 23360), -- 69978
(69977, 'deDE', 'Blutwache Zar''shi', '', 'Rüstmeister für ruchlose Gladiatoren', NULL, 23360), -- 69977
(12791, 'deDE', 'Häuptling Erdbinder', '', NULL, NULL, 23360), -- 12791
(12790, 'deDE', 'Berater Willington', '', NULL, NULL, 23360), -- 12790
(29143, 'deDE', 'Bebri Haubenlocke', '', 'Barbier', NULL, 23360), -- 29143
(73151, 'deDE', 'Todeswache Netharian', '', 'Rüstmeister für Kampfreittiere', NULL, 23360), -- 73151
(12798, 'deDE', 'Grunzerin Bek''rah', '', NULL, NULL, 23360), -- 12798
(12796, 'deDE', 'Räuber Bork', '', 'Rüstmeister für Kriegsreittiere', NULL, 23360), -- 12796
(10880, 'deDE', 'Kriegsrufer Gorlach', '', NULL, NULL, 23360), -- 10880
(5910, 'deDE', 'Zankaja', '', NULL, NULL, 23360), -- 5910
(3314, 'deDE', 'Urtharo', '', 'Waffenhändler', NULL, 23360), -- 3314
(49622, 'deDE', 'Shok Narnes', '', NULL, NULL, 23360), -- 49622
(4047, 'deDE', 'Zor Einbaum', '', 'Ältestenscharfseher', NULL, 23360), -- 4047
(3144, 'deDE', 'Etrigg', '', NULL, NULL, 23360), -- 3144
(45339, 'deDE', 'Dunkle Klerikerin Cecille', '', 'Priesterlehrerin', NULL, 23360), -- 45339
(46142, 'deDE', 'Delegationstodeswache der Verlassenen', '', NULL, NULL, 23360), -- 46142
(3334, 'deDE', 'Rekkul', '', 'Gifte', NULL, 23360), -- 3334
(44338, 'deDE', 'Weißes Huhn', '', NULL, NULL, 23360), -- 44338
(88543, 'deDE', 'Magierin von Orgrimmar', '', NULL, NULL, 23360), -- 88543
(23128, 'deDE', 'Meister Pyreanor', '', 'Paladinlehrer', NULL, 23360), -- 23128
(35068, 'deDE', 'Gotura Vierwind', '', 'Der Irdene Ring', NULL, 23360), -- 35068
(46555, 'deDE', 'Gunra', '', 'Rüstmeisterin für Gerechtigkeitspunkte', NULL, 23360), -- 46555
(58155, 'deDE', 'Rugok', '', 'Antiquitätenrüstmeister für Gerechtigkeitspunkte', NULL, 23360), -- 58155
(46556, 'deDE', 'Jamus''Vaz', '', 'Rüstmeister für Tapferkeitspunkte', NULL, 23360), -- 46556
(52034, 'deDE', 'Togar', '', 'Gerechtigkeitshandwerkswaren', NULL, 23360), -- 52034
(44160, 'deDE', 'Verdächtiger Peon', '', NULL, NULL, 23360), -- 44160
(34765, 'deDE', 'Zelli Heißdüse', '', 'Zeppelinmeisterin von Donnerfels', NULL, 23360), -- 34765
(3335, 'deDE', 'Hagrus', '', 'Reagenzien', NULL, 23360), -- 3335
(5816, 'deDE', 'Katis', '', 'Zauberstabhändlerin', NULL, 23360), -- 5816
(3330, 'deDE', 'Muragus', '', 'Stabhändler', NULL, 23360), -- 3330
(89175, 'deDE', 'Dysong', '', NULL, NULL, 23360), -- 89175
(3189, 'deDE', 'Kor''ghan', '', NULL, NULL, 23360), -- 3189
(3328, 'deDE', 'Ormok', '', 'Schurkenlehrer', NULL, 23360), -- 3328
(3327, 'deDE', 'Gest', '', 'Schurkenlehrer', NULL, 23360), -- 3327
(47233, 'deDE', 'Gordul', '', 'Schurkenlehrer', NULL, 23360), -- 47233
(3323, 'deDE', 'Horthus', '', 'Reagenzien', NULL, 23360), -- 3323
(45337, 'deDE', 'Tyelis', '', 'Priesterlehrer', NULL, 23360), -- 45337
(17098, 'deDE', 'Botschafterin Morgensänger', '', NULL, NULL, 23360), -- 17098
(46140, 'deDE', 'Wächter der Delegation Silbermonds', '', NULL, 'Directions', 23360), -- 46140
(47571, 'deDE', 'Bellok Leuchtklinge', '', 'Archäologielehrer', NULL, 23360), -- 47571
(26537, 'deDE', 'Grieb Rammrakete', '', 'Zeppelinmeister, Boreanische Tundra', NULL, 23360), -- 26537
(47253, 'deDE', 'Rundok', '', 'Portallehrer', NULL, 23360), -- 47253
(3331, 'deDE', 'Kareth', '', 'Klingenhändler', NULL, 23360), -- 3331
(20490, 'deDE', 'Schneller lila Windreiter', '', NULL, NULL, 23360), -- 20490
(20489, 'deDE', 'Schneller grüner Windreiter', '', NULL, NULL, 23360), -- 20489
(20491, 'deDE', 'Schneller roter Windreiter', '', NULL, NULL, 23360), -- 20491
(5909, 'deDE', 'Cazul', '', NULL, NULL, 23360), -- 5909
(47254, 'deDE', 'Gizput', '', 'Diener von Kurgul', NULL, 23360), -- 47254
(88706, 'deDE', 'Murgul', '', 'Dämonenausbilder', NULL, 23360), -- 88706
(5875, 'deDE', 'Mordak Dunkelfaust', '', NULL, NULL, 23360), -- 5875
(88704, 'deDE', 'Gran''dul', '', 'Hexenmeisterlehrer', NULL, 23360), -- 88704
(88705, 'deDE', 'Kranosh', '', 'Hexenmeisterlehrer', NULL, 23360), -- 88705
(47248, 'deDE', 'Gija', '', 'Magierlehrerin', NULL, 23360), -- 47248
(47246, 'deDE', 'Ureda', '', 'Magierlehrerin', NULL, 23360), -- 47246
(47247, 'deDE', 'Marud', '', 'Magierlehrer', NULL, 23360), -- 47247
(14375, 'deDE', 'Späher Starkpranke', '', NULL, NULL, 23360), -- 14375
(20486, 'deDE', 'Blauer Windreiter', '', NULL, NULL, 23360), -- 20486
(20488, 'deDE', 'Lohfarbener Windreiter', '', NULL, NULL, 23360), -- 20488
(20493, 'deDE', 'Grüner Windreiter', '', NULL, NULL, 23360), -- 20493
(20492, 'deDE', 'Schneller gelber Windreiter', '', NULL, NULL, 23360), -- 20492
(47897, 'deDE', 'Verkäufer des Mondfests', '', NULL, NULL, 23360), -- 47897
(44948, 'deDE', 'Windreiterjunges', '', 'Drakmas Begleiter', NULL, 23360), -- 44948
(44918, 'deDE', 'Drakma', '', 'Windreitermeister', NULL, 23360), -- 44918
(19177, 'deDE', 'Trollbürger', '', NULL, NULL, 23360), -- 19177
(14720, 'deDE', 'Hochfürst Saurfang', '', NULL, NULL, 23360), -- 14720
(3329, 'deDE', 'Kor''jus', '', 'Pilzverkäufer', NULL, 23360), -- 3329
(5639, 'deDE', 'Craven Drok', '', NULL, NULL, 23360), -- 5639
(3216, 'deDE', 'Arnak Feuerklinge', '', NULL, NULL, 23360), -- 3216
(62194, 'deDE', 'Ukos Blutwisper', '', NULL, NULL, 23360), -- 62194
(62198, 'deDE', 'Omakka Wolfsbruder', '', NULL, NULL, 23360), -- 62198
(44919, 'deDE', 'Maztha', '', 'Fluglehrerin', NULL, 23360), -- 44919
(3310, 'deDE', 'Doras', '', 'Windreitermeister', NULL, 23360), -- 3310
(50477, 'deDE', 'Champion Uru''zin', '', 'Rüstmeister der Dunkelspeere', NULL, 23360), -- 50477
(50323, 'deDE', 'Frizzo Villamar', '', 'Rüstmeisterin des Bilgewasserkartells', NULL, 23360), -- 50323
(3346, 'deDE', 'Kithas', '', 'Verzauberkunstbedarf', NULL, 23360), -- 3346
(46181, 'deDE', 'Verzauberer Farendin', '', NULL, NULL, 23360), -- 46181
(23635, 'deDE', 'Krixx', '', 'Zeppeliningenieur', NULL, 23360), -- 23635
(50488, 'deDE', 'Steingardist Nargol', '', 'Rüstmeister von Orgrimmar', NULL, 23360), -- 50488
(15891, 'deDE', 'Herold des Mondfests', '', NULL, NULL, 23360), -- 15891
(15579, 'deDE', 'Urahnin Dunkelhorn', '', NULL, NULL, 23360), -- 15579
(44158, 'deDE', 'Peon des Himmelspfads von Orgrimmar', '', NULL, NULL, 23360), -- 44158
(43062, 'deDE', 'Bort', '', NULL, NULL, 23360), -- 43062
(9564, 'deDE', 'Frezza', '', 'Zeppelinmeister, Tirisfal', NULL, 23360), -- 9564
(11066, 'deDE', 'Jhag', '', 'Verzauberkunstlehrling', NULL, 23360), -- 11066
(3345, 'deDE', 'Godan', '', 'Verzauberkunstlehrer', NULL, 23360), -- 3345
(42506, 'deDE', 'Marogg', '', 'Chefkoch der Infanterie', NULL, 23360), -- 42506
(49737, 'deDE', 'Shazdar', '', 'Souschefin', NULL, 23360), -- 49737
(63626, 'deDE', 'Varzok', '', 'Kampfhaustiertrainer', NULL, 23360), -- 63626
(12136, 'deDE', 'Snurk Zasterwill', '', 'Zeppelinmeisterin, Schlingendorntal', NULL, 23360), -- 12136
(52810, 'deDE', 'Toy Cart Bunny', '', NULL, NULL, 23360), -- 52810
(52812, 'deDE', 'Spielzeugwagen', '', NULL, NULL, 23360), -- 52812
(52809, 'deDE', 'Blax Flaschenzünder', '', 'Spielzeuge und Kuriositäten', NULL, 23360), -- 52809
(3369, 'deDE', 'Gotri', '', 'Taschenverkäufer', NULL, 23360), -- 3369
(46709, 'deDE', 'Arugi', '', 'Kochkunstlehrerin', NULL, 23360), -- 46709
(3368, 'deDE', 'Borstan', '', 'Fleischverkäufer', NULL, 23360), -- 3368
(46708, 'deDE', 'Suja', '', 'Kochbedarf', NULL, 23360), -- 46708
(46742, 'deDE', 'Brunda', '', 'Kräuterkundebedarf', NULL, 23360), -- 46742
(46716, 'deDE', 'Nerog', '', 'Inschriftenkundelehrer', NULL, 23360), -- 46716
(7010, 'deDE', 'Zilzibin Tomtom', '', NULL, NULL, 23360), -- 7010
(49131, 'deDE', 'Goblinischer Hot Rod', '', NULL, NULL, 23360), -- 49131
(3315, 'deDE', 'Tor''phan', '', 'Tuch- & Lederrüstungshändler', NULL, 23360), -- 3315
(14499, 'deDE', 'Waisenkind der Horde', '', NULL, NULL, 23360), -- 14499
(2857, 'deDE', 'Thund', '', 'Ingenieurslehrling', NULL, 23360), -- 2857
(14498, 'deDE', 'Tosamina', '', NULL, NULL, 23360), -- 14498
(3412, 'deDE', 'Nogg', '', 'Ingenieurslehrling', NULL, 23360), -- 3412
(62199, 'deDE', 'Gogu', '', NULL, NULL, 23360), -- 62199
(62200, 'deDE', 'Sasi', '', NULL, NULL, 23360), -- 62200
(11017, 'deDE', 'Roxxik', '', 'Ingenieurskunstlehrer', NULL, 23360), -- 11017
(3413, 'deDE', 'Sovik', '', 'Ingenieursbedarf', NULL, 23360), -- 3413
(14451, 'deDE', 'Waisenmatrone Schlachterbe', '', NULL, NULL, 23360), -- 14451
(52032, 'deDE', 'Sinzi Funkenschrieb', '', 'Tintenhändlerin', NULL, 23360), -- 52032
(46741, 'deDE', 'Muraga', '', 'Kräuterkundelehrerin', NULL, 23360), -- 46741
(46718, 'deDE', 'Moraka', '', 'Inschriftenkundebedarf', NULL, 23360), -- 46718
(3372, 'deDE', 'Sarlek', '', NULL, NULL, 23360), -- 3372
(3371, 'deDE', 'Tamaro', '', NULL, NULL, 23360), -- 3371
(3367, 'deDE', 'Felika', '', 'Handwerkswaren', NULL, 23360), -- 3367
(3351, 'deDE', 'Magenius', '', 'Reagenzien', NULL, 23360), -- 3351
(3350, 'deDE', 'Asoran', '', 'Gemischtwaren', NULL, 23360), -- 3350
(3364, 'deDE', 'Borya', '', 'Schneiderbedarf', NULL, 23360), -- 3364
(3363, 'deDE', 'Magar', '', 'Schneiderlehrer', NULL, 23360), -- 3363
(3317, 'deDE', 'Ollanus', '', 'Stoffrüstungshändler', NULL, 23360), -- 3317
(2855, 'deDE', 'Snang', '', 'Schneiderlehrling', NULL, 23360), -- 2855
(3321, 'deDE', 'Morgum', '', 'Lederrüstungshändler', NULL, 23360), -- 3321
(6986, 'deDE', 'Dran Droffers', '', 'Droffers und Sohn Bergungen', NULL, 23360), -- 6986
(6987, 'deDE', 'Malton Droffers', '', 'Droffers und Sohn Bergungen', NULL, 23360), -- 6987
(3366, 'deDE', 'Tamar', '', 'Lederverarbeitungsbedarf', NULL, 23360), -- 3366
(3225, 'deDE', 'Verderbter scheckiger Eber', '', NULL, NULL, 23360), -- 3225
(5811, 'deDE', 'Kamari', '', 'Lederverarbeitungslehrling', NULL, 23360), -- 5811
(3365, 'deDE', 'Karolek', '', 'Lederverarbeitungslehrer', NULL, 23360), -- 3365
(7088, 'deDE', 'Thuwd', '', 'Kürschnerlehrer', NULL, 23360), -- 7088
(3316, 'deDE', 'Handor', '', 'Tuch- & Lederrüstungshändler', NULL, 23360), -- 3316
(62196, 'deDE', 'Flekky Nox', '', NULL, NULL, 23360), -- 62196
(62197, 'deDE', 'Zazzle', '', NULL, NULL, 23360), -- 62197
(42709, 'deDE', 'Gravy', '', 'Schankkellner', NULL, 23360), -- 42709
(4485, 'deDE', 'Belgrom Felshammer', '', NULL, NULL, 23360), -- 4485
(42638, 'deDE', 'Herezegor Flammenhauer', '', NULL, NULL, 23360), -- 42638
(46619, 'deDE', 'Binzella', '', 'Bankierin', NULL, 23360), -- 46619
(46620, 'deDE', 'Vink', '', 'Bankier', NULL, 23360), -- 46620
(46618, 'deDE', 'Fibi', '', 'Bankierin', NULL, 23360), -- 46618
(46622, 'deDE', 'Vuvanzi', '', 'Bankierin', NULL, 23360), -- 46622
(46621, 'deDE', 'Pank', '', 'Bankier', NULL, 23360), -- 46621
(46642, 'deDE', 'Gastwirtin Nufa', '', 'Gastwirtin', NULL, 23360), -- 46642
(19175, 'deDE', 'Orcbürger', '', NULL, NULL, 23360), -- 19175
(3359, 'deDE', 'Kiro', '', 'Kriegsharnischschmiedin', NULL, 23360), -- 3359
(47663, 'deDE', 'Späherin Obrok', '', NULL, NULL, 23360), -- 47663
(72559, 'deDE', 'Verteidiger von Donnerfels', '', NULL, 'directions', 23360), -- 72559
(62193, 'deDE', 'Rok''kal', '', NULL, NULL, 23360), -- 62193
(16076, 'deDE', 'Tharl Steinblut', '', NULL, NULL, 23360), -- 16076
(16069, 'deDE', 'Gurky', '', NULL, NULL, 23360), -- 16069
(66022, 'deDE', 'Schildkrötenmeisterin Odai', '', 'Drachenschildkrötenzüchterin', NULL, 23360), -- 66022
(70301, 'deDE', 'Sanftpfote', '', 'Reitlehrer', NULL, 23360), -- 70301
(12351, 'deDE', 'Terrorwolf', '', NULL, NULL, 23360), -- 12351
(356, 'deDE', 'Schwarzer Wolf', '', NULL, NULL, 23360), -- 356
(14540, 'deDE', 'Schneller brauner Wolf', '', NULL, NULL, 23360), -- 14540
(14541, 'deDE', 'Schneller grauer Wolf', '', NULL, NULL, 23360), -- 14541
(3332, 'deDE', 'Lumak', '', 'Angellehrer', NULL, 23360), -- 3332
(3333, 'deDE', 'Shankys', '', 'Angelbedarf', NULL, 23360), -- 3333
(66437, 'deDE', 'Arkanist Xu', '', 'Magier der Horde', NULL, 23360), -- 66437
(12353, 'deDE', 'Waldwolf', '', NULL, NULL, 23360), -- 12353
(64105, 'deDE', 'Muffin', '', NULL, NULL, 23360), -- 64105
(5195, 'deDE', 'Brauner Wolf', '', NULL, NULL, 23360), -- 5195
(43239, 'deDE', 'Razgar', '', 'Anglermeister', NULL, 23360), -- 43239
(3362, 'deDE', 'Ogunaro Wolfsläufer', '', 'Zwingermeister', NULL, 23360), -- 3362
(4752, 'deDE', 'Kildar', '', 'Reitlehrer', NULL, 23360), -- 4752
(47808, 'deDE', 'Schwarzer Wolf', '', NULL, NULL, 23360), -- 47808
(15186, 'deDE', 'Murky', '', NULL, NULL, 23360), -- 15186
(69333, 'deDE', 'Schüler Jusi', '', 'Rüstmeister der Huojin', NULL, 23360), -- 69333
(7951, 'deDE', 'Zas''Tysh', '', NULL, NULL, 23360), -- 7951
(68869, 'deDE', 'Luo Luo', '', NULL, NULL, 23360), -- 68869
(62445, 'deDE', 'Ji Feuerpfote', '', 'Mönchslehrer', NULL, 23360), -- 62445
(65008, 'deDE', 'Mönch der Huojin', '', NULL, NULL, 23360), -- 65008
(16869, 'deDE', 'Jising', '', NULL, NULL, 23360), -- 16869
(16868, 'deDE', 'Winsum', '', NULL, NULL, 23360), -- 16868
(2756, 'deDE', 'Chao-Ju', '', NULL, NULL, 23360), -- 2756
(5034, 'deDE', 'Winwa', '', NULL, NULL, 23360), -- 5034
(5029, 'deDE', 'Jiming', '', NULL, NULL, 23360), -- 5029
(47809, 'deDE', 'Brauner Wolf', '', NULL, NULL, 23360), -- 47809
(31768, 'deDE', 'Im Stall abgestellter Jägerbegleiter', '', NULL, NULL, 23360), -- 31768
(31769, 'deDE', 'Im Stall abgestellter Jägerbegleiter', '', NULL, NULL, 23360), -- 31769
(47764, 'deDE', 'Murog', '', 'Stallmeister', NULL, 23360), -- 47764
(31757, 'deDE', 'Im Stall abgestellter Jägerbegleiter', '', NULL, NULL, 23360), -- 31757
(31756, 'deDE', 'Im Stall abgestellter Jägerbegleiter', '', NULL, NULL, 23360), -- 31756
(47771, 'deDE', 'Drukma', '', 'Tierausbilder', NULL, 23360), -- 47771
(31758, 'deDE', 'Im Stall abgestellter Jägerbegleiter', '', NULL, NULL, 23360), -- 31758
(46639, 'deDE', 'Auktionatorin Zilbeena', '', NULL, NULL, 23360), -- 46639
(46637, 'deDE', 'Auktionator Drezbit', '', NULL, NULL, 23360), -- 46637
(46640, 'deDE', 'Auktionatorin Kuvi', '', NULL, NULL, 23360), -- 46640
(31755, 'deDE', 'Im Stall abgestellter Jägerbegleiter', '', NULL, NULL, 23360), -- 31755
(14539, 'deDE', 'Schneller Waldwolf', '', NULL, NULL, 23360), -- 14539
(46638, 'deDE', 'Auktionator Vizput', '', NULL, NULL, 23360), -- 46638
(47818, 'deDE', 'Zahna', '', 'Begleiter von Korla', NULL, 23360), -- 47818
(47817, 'deDE', 'Krabbler', '', 'Begleiter von Guldor', NULL, 23360), -- 47817
(47815, 'deDE', 'Grimmfang', '', 'Begleiter von Ormak', NULL, 23360), -- 47815
(47788, 'deDE', 'Guldor', '', 'Jägerlehrer', NULL, 23360), -- 47788
(47767, 'deDE', 'Korla', '', 'Jägerlehrerin', NULL, 23360), -- 47767
(3352, 'deDE', 'Ormak Grimmschlag', '', 'Jägerlehrer', NULL, 23360), -- 3352
(35162, 'deDE', 'Azsharischer Schössling', '', NULL, NULL, 23360), -- 35162
(35086, 'deDE', 'Arbeitsaufseher Grabbit', '', NULL, NULL, 23360), -- 35086
(8576, 'deDE', 'Ag''tor Blutfaust', '', NULL, NULL, 23360), -- 8576
(42672, 'deDE', 'Großknecht Glibbs', '', NULL, NULL, 23360), -- 42672
(42673, 'deDE', 'Höllschreis Verwüster', '', NULL, 'EngineerSkin', 23360), -- 42673
(42671, 'deDE', 'Chefingenieur', '', NULL, NULL, 23360), -- 42671
(42650, 'deDE', 'Goblinischer Belagerungsarbeiter', '', NULL, NULL, 23360), -- 42650
(57922, 'deDE', 'Taryssa Lazuria', '', 'Juwelierskunstbedarf', NULL, 23360), -- 57922
(50482, 'deDE', 'Marith Lazuria', '', 'Juwelierskunstbedarf', NULL, 23360), -- 50482
(46675, 'deDE', 'Lugrah', '', 'Juwelierskunstlehrerin', NULL, 23360), -- 46675
(3358, 'deDE', 'Gorina', '', 'Bergbaubedarf', NULL, 23360), -- 3358
(3357, 'deDE', 'Makaru', '', 'Bergbaulehrer', NULL, 23360), -- 3357
(11178, 'deDE', 'Borgosh Glutformer', '', 'Schmiedekunstlehrer', NULL, 23360), -- 11178
(11177, 'deDE', 'Okothos Eisenwüter', '', 'Schmiedekunstlehrer', NULL, 23360), -- 11177
(11176, 'deDE', 'Krathok Glutfaust', '', NULL, NULL, 23360), -- 11176
(10266, 'deDE', 'Ug''thok', '', 'Schmiedekunstlehrling', NULL, 23360), -- 10266
(7793, 'deDE', 'Ochse', '', 'Der Mithrilorden', NULL, 23360), -- 7793
(7792, 'deDE', 'Aturk der Amboss', '', NULL, NULL, 23360), -- 7792
(5812, 'deDE', 'Tumi', '', 'Händlerin für Kettenrüstungen', NULL, 23360), -- 5812
(4043, 'deDE', 'Galthuk', '', 'Zweihandwaffenhändler', NULL, 23360), -- 4043
(3409, 'deDE', 'Zendo''jian', '', 'Waffenverkäufer', NULL, 23360), -- 3409
(3356, 'deDE', 'Sumi', '', 'Schmiedekunstbedarf', NULL, 23360), -- 3356
(3355, 'deDE', 'Saru Stahlzorn', '', 'Schmiedekunstlehrer', NULL, 23360), -- 3355
(1383, 'deDE', 'Murr', '', 'Schmiedekunstlehrling', NULL, 23360), -- 1383
(42548, 'deDE', 'Schlammiger Flusskrebs', '', NULL, 'LootAll', 23360), -- 42548
(7790, 'deDE', 'Orokk Omosh', '', NULL, NULL, 23360), -- 7790
(7231, 'deDE', 'Kelgruk Blutaxt', '', 'Schmiedekunstlehrer', NULL, 23360), -- 7231
(7230, 'deDE', 'Shayis Stahlzorn', '', 'Schmiedekunstlehrerin', NULL, 23360), -- 7230
(3361, 'deDE', 'Shoma', '', 'Waffenverkäufer', NULL, 23360), -- 3361
(3360, 'deDE', 'Koru', '', 'Streitkolben- und Stabverkäufer', NULL, 23360), -- 3360
(13842, 'deDE', 'Botschafterin Rokhstrom der Frostwölfe', '', NULL, NULL, 23360), -- 13842
(46667, 'deDE', 'Klingenmeister Ronakada', '', 'Kriegerlehrer', NULL, 23360), -- 46667
(3354, 'deDE', 'Sorek', '', 'Kriegerlehrer', NULL, 23360), -- 3354
(3353, 'deDE', 'Grezz Zornfaust', '', 'Kriegerlehrer', NULL, 23360), -- 3353
(35364, 'deDE', 'Slahtz', '', 'Erfahrungseliminierer', NULL, 23360), -- 35364
(34955, 'deDE', 'Karg Grindschädel', '', 'Kampfmeister', NULL, 23360), -- 34955
(49573, 'deDE', 'Karba Flammenschlund', '', 'Kampfmeisterin', NULL, 23360), -- 49573
(3199, 'deDE', 'Kultist der Brennenden Klinge', '', NULL, NULL, 23360), -- 3199
(3196, 'deDE', 'Neophyt der Brennenden Klinge', '', NULL, NULL, 23360), -- 3196
(3195, 'deDE', 'Rohling der Brennenden Klinge', '', NULL, NULL, 23360), -- 3195
(3117, 'deDE', 'Wilde der Staubschwingen', '', NULL, NULL, 23360), -- 3117
(39604, 'deDE', 'Vek''nag', '', NULL, NULL, 23360), -- 39604
(3208, 'deDE', 'Margoz', '', NULL, NULL, 23360), -- 3208
(3118, 'deDE', 'Sturmhexe der Staubschwingen', '', NULL, NULL, 23360), -- 3118
(3203, 'deDE', 'Zischel Dunkelklaue', '', NULL, NULL, 23360), -- 3203
(66145, 'deDE', 'Stachel', '', NULL, NULL, 23360), -- 66145
(66144, 'deDE', 'Mumtar', '', NULL, NULL, 23360), -- 66144
(66126, 'deDE', 'Zunta', '', 'Aufstrebender Tierzähmer', NULL, 23360), -- 66126
(39603, 'deDE', 'Geschwollener Leerwandler', '', NULL, NULL, 23360), -- 39603
(40971, 'deDE', 'Fieberkratzer', '', NULL, NULL, 23360), -- 40971
(40970, 'deDE', 'Tednug', '', NULL, NULL, 23360), -- 40970
(3035, 'deDE', 'Flachlandpuma', '', NULL, NULL, 23360), -- 3035
(44599, 'deDE', 'Dünenscherenbrutlord', '', NULL, NULL, 23360), -- 44599
(3293, 'deDE', 'Rezlak', '', 'Tüftlerverband', NULL, 23360), -- 3293
(3116, 'deDE', 'Plünderin der Staubschwingen', '', NULL, NULL, 23360), -- 3116
(3115, 'deDE', 'Harpyie der Staubschwingen', '', NULL, NULL, 23360), -- 3115
(3193, 'deDE', 'Misha Tor''kren', '', NULL, NULL, 23360), -- 3193
(39325, 'deDE', 'Großmatrone Tekla', '', NULL, NULL, 23360), -- 39325
(43331, 'deDE', 'Goldener Steinfisch', '', NULL, NULL, 23360), -- 43331
(39464, 'deDE', 'Ertrunkene Donnerechse', '', NULL, NULL, 23360), -- 39464
(40949, 'deDE', 'Kraka', '', NULL, NULL, 23360), -- 40949
(40948, 'deDE', 'Bauer von Dranosh''ar', '', NULL, NULL, 23360), -- 40948
(39380, 'deDE', 'Shin Steinsäule', '', 'Scharfseher', NULL, 23360), -- 39380
(39379, 'deDE', 'Gor der Vollstrecker', '', NULL, NULL, 23360), -- 39379
(42504, 'deDE', 'Ausgewachsenes Schwein', '', NULL, NULL, 23360), -- 42504
(42859, 'deDE', 'Wildes ausgewachsenes Schwein', '', NULL, NULL, 23360), -- 42859
(71100, 'deDE', 'Unterbezahlter Ingenieur', '', NULL, NULL, 23360), -- 71100
(4311, 'deDE', 'Holgar Sturmaxt', '', NULL, NULL, 23360), -- 4311
(3204, 'deDE', 'Gazz''uz', '', NULL, NULL, 23360), -- 3204
(40891, 'deDE', 'Arbeiter von Dranosh''ar', '', NULL, NULL, 23360), -- 40891
(3198, 'deDE', 'Lehrling der Brennenden Klinge', '', NULL, NULL, 23360), -- 3198
(3197, 'deDE', 'Fanatiker der Brennenden Klinge', '', NULL, NULL, 23360), -- 3197
(88750, 'deDE', 'Raptor', '', NULL, NULL, 23360), -- 88750
(86884, 'deDE', 'Loyalistin der Dunkelspeere', '', NULL, NULL, 23360), -- 86884
(41621, 'deDE', 'Kommandant Thorak', '', NULL, NULL, 23360), -- 41621
(40893, 'deDE', 'Fischer von Dranosh''ar', '', NULL, NULL, 23360), -- 40893
(3108, 'deDE', 'Verkrusteter Brandungskriecher', '', NULL, NULL, 23360), -- 3108
(39353, 'deDE', 'Griswold Hanniston', '', NULL, NULL, 23360), -- 39353
(39590, 'deDE', 'Spitzzahn', '', 'Sklaventreiber', NULL, 23360), -- 39590
(39351, 'deDE', 'Ghislania', '', NULL, NULL, 23360), -- 39351
(39519, 'deDE', 'Cosmetic Trigger - PRK', '', NULL, NULL, 23360), -- 39519
(39352, 'deDE', 'Gaur Eishorn', '', NULL, NULL, 23360), -- 39352
(39323, 'deDE', 'Thonk', '', NULL, NULL, 23360), -- 39323
(11943, 'deDE', 'Magga', '', NULL, NULL, 23360), -- 11943
(6928, 'deDE', 'Gastwirt Grosk', '', 'Gastwirt', NULL, 23360), -- 6928
(6787, 'deDE', 'Yelnagi Schwarzarm', '', NULL, NULL, 23360), -- 6787
(3175, 'deDE', 'Krunn', '', 'Bergbaulehrer', NULL, 23360), -- 3175
(3174, 'deDE', 'Dwukk', '', 'Schmiedekunstlehrer', NULL, 23360), -- 3174
(3167, 'deDE', 'Wuark', '', 'Rüstungsschmied & Schildmacher', NULL, 23360), -- 3167
(3163, 'deDE', 'Uhgar', '', 'Waffenschmied', NULL, 23360), -- 3163
(11025, 'deDE', 'Mukdrak', '', 'Ingenieurskunstlehrer', NULL, 23360), -- 11025
(63063, 'deDE', 'Schleichi', '', 'Kampfhaustier', NULL, 23360), -- 63063
(63061, 'deDE', 'Narzak', '', 'Kampfhaustiertrainer', NULL, 23360), -- 63061
(3166, 'deDE', 'Cutac', '', 'Tuch- & Lederrüstungshändler', NULL, 23360), -- 3166
(3165, 'deDE', 'Ghrawt', '', 'Bogenmacher', NULL, 23360), -- 3165
(44380, 'deDE', 'Jabul', '', 'Druidenlehrer', NULL, 23360), -- 44380
(9987, 'deDE', 'Shoja''my', '', 'Stallmeisterin', NULL, 23360), -- 9987
(3294, 'deDE', 'Ophek', '', NULL, NULL, 23360), -- 3294
(3172, 'deDE', 'Dhugru Blutgier', '', 'Hexenmeisterlehrer', NULL, 23360), -- 3172
(37912, 'deDE', 'Ertrunkener Marinesoldat von Kul Tiras', '', NULL, NULL, 23360), -- 37912
(39270, 'deDE', 'Verletzter Grunzer von Klingenhügel', '', NULL, 'Speak', 23360), -- 39270
(6027, 'deDE', 'Kitha', '', 'Dämonenausbilderin', NULL, 23360), -- 6027
(39272, 'deDE', 'Schäumender Meereselementar', '', NULL, NULL, 23360), -- 39272
(39072, 'deDE', 'Naj''tess', '', 'Gemahl der Meerhexe', NULL, 23360), -- 39072
(37989, 'deDE', 'Flinkklaue', '', NULL, NULL, 23360), -- 37989
(39157, 'deDE', 'Verirrtes Blutkrallenjungtier', '', NULL, NULL, 23360), -- 39157
(37987, 'deDE', 'Tegashi', '', NULL, NULL, 23360), -- 37987
(37969, 'deDE', 'Kijara', '', NULL, NULL, 23360), -- 37969
(37961, 'deDE', 'Verderbte Blutkralle', '', NULL, NULL, 23360), -- 37961
(38282, 'deDE', 'Jägernovize der Dunkelspeere', '', NULL, NULL, 23360), -- 38282
(38247, 'deDE', 'Ortezza', '', 'Jägerlehrerin', NULL, 23360), -- 38247
(38245, 'deDE', 'Tunari', '', 'Priesterlehrerin', NULL, 23360), -- 38245
(38280, 'deDE', 'Druidennovizin der Dunkelspeere', '', NULL, NULL, 23360), -- 38280
(38278, 'deDE', 'Priesternovize der Dunkelspeere', '', NULL, NULL, 23360), -- 38278
(38243, 'deDE', 'Zen''tabra', '', 'Druidenlehrerin', NULL, 23360), -- 38243
(47057, 'deDE', 'Verwundete Wache der Dunkelspeere', '', NULL, NULL, 23360), -- 47057
(38279, 'deDE', 'Magiernovize der Dunkelspeere', '', NULL, NULL, 23360), -- 38279
(38246, 'deDE', 'Soratha', '', 'Magierlehrer', NULL, 23360), -- 38246
(38966, 'deDE', 'Vol''jin', '', NULL, NULL, 23360), -- 38966
(90113, 'deDE', 'Ardsami', '', NULL, NULL, 23360), -- 90113
(39033, 'deDE', 'Sortura', '', 'Tuch- & Lederrüstungshändler', NULL, 23360), -- 39033
(39027, 'deDE', 'Vanira', '', NULL, NULL, 23360), -- 39027
(39008, 'deDE', 'Hira''jin', '', NULL, NULL, 23360), -- 39008
(39007, 'deDE', 'Tora''jin', '', NULL, NULL, 23360), -- 39007
(38989, 'deDE', 'Jornun', '', NULL, NULL, 23360), -- 38989
(39031, 'deDE', 'Torenda', '', 'Speis & Trank', NULL, 23360), -- 39031
(39032, 'deDE', 'Gora''tin', '', 'Gemischtwaren', NULL, 23360), -- 39032
(38005, 'deDE', 'Moraya', '', NULL, NULL, 23360), -- 38005
(37960, 'deDE', 'Blutkrallenjungtier', '', NULL, NULL, 23360), -- 37960
(38988, 'deDE', 'Notera', '', NULL, NULL, 23360), -- 38988
(38990, 'deDE', 'Kortin', '', NULL, NULL, 23360), -- 38990
(38987, 'deDE', 'Blutkrallenhauer', '', NULL, NULL, 23360), -- 38987
(37956, 'deDE', 'Blutkrallenraptor', '', NULL, NULL, 23360), -- 37956
(37951, 'deDE', 'Jin''thala', '', NULL, NULL, 23360), -- 37951
(38037, 'deDE', 'Nortet', '', 'Kriegerlehrer', NULL, 23360), -- 38037
(63310, 'deDE', 'Zabrax', '', 'Mönchslehrer', NULL, 23360), -- 63310
(63309, 'deDE', 'Tsu der Wanderer', '', 'Pandarenentdecker', NULL, 23360), -- 63309
(38281, 'deDE', 'Schamanennovizin der Dunkelspeere', '', NULL, NULL, 23360), -- 38281
(39062, 'deDE', 'Gefängnisaufseher der Dunkelspeere', '', NULL, NULL, 23360), -- 39062
(38272, 'deDE', 'Schurkennovize der Dunkelspeere', '', NULL, NULL, 23360), -- 38272
(38268, 'deDE', 'Kriegernovize der Dunkelspeere', '', NULL, NULL, 23360), -- 38268
(38244, 'deDE', 'Legati', '', 'Schurkenlehrer', NULL, 23360), -- 38244
(38242, 'deDE', 'Nekali', '', 'Schamanenlehrerin', NULL, 23360), -- 38242
(38142, 'deDE', 'Gefangener Späher der Gallschuppen', '', NULL, NULL, 23360), -- 38142
(42619, 'deDE', 'Hexenmeisternovize der Dunkelspeere', '', NULL, NULL, 23360), -- 42619
(42618, 'deDE', 'Voldreka', '', 'Hexenmeisterlehrer', NULL, 23360), -- 42618
(38038, 'deDE', 'Tikiziel', '', NULL, NULL, 23360), -- 38038
(38046, 'deDE', 'Wildmähnenkatze', '', NULL, NULL, 23360), -- 38046
(38141, 'deDE', 'Zahmer Inseleber', '', NULL, NULL, 23360), -- 38141
(39269, 'deDE', 'Leutnant Palliter', '', 'Expeditionsstreitkräfte der Nordwacht', NULL, 23360), -- 39269
(39267, 'deDE', 'Marinesoldat der Nordwacht', '', 'Expeditionsstreitkräfte der Nordwacht', NULL, 23360), -- 39267
(37911, 'deDE', 'Ertrunkener Matrose von Kul Tiras', '', NULL, NULL, 23360), -- 37911
(3129, 'deDE', 'Marinesoldat von Kul Tiras', '', NULL, NULL, 23360), -- 3129
(38442, 'deDE', 'Morakki', '', 'Hauptmann der Wache', NULL, 23360), -- 38442
(38326, 'deDE', 'Schamane der Dunkelspeere', '', NULL, NULL, 23360), -- 38326
(38324, 'deDE', 'Stammesmitglied der Dunkelspeere', '', NULL, NULL, 23360), -- 38324
(38440, 'deDE', 'Tortunga', '', NULL, NULL, 23360), -- 38440
(38217, 'deDE', 'Wache der Dunkelspeere', '', NULL, NULL, 23360), -- 38217
(38437, 'deDE', 'Vanira', '', NULL, NULL, 23360), -- 38437
(38306, 'deDE', 'Zar''jira', '', 'Die Meerhexe', NULL, 23360), -- 38306
(38423, 'deDE', 'Zuni', '', NULL, NULL, 23360), -- 38423
(38003, 'deDE', 'Echo Isles Quest Bunny', '', NULL, NULL, 23360), -- 38003
(38560, 'deDE', 'Spitescale Flag Bunny', '', NULL, NULL, 23360), -- 38560
(38301, 'deDE', 'Sirene der Gallschuppen', '', NULL, NULL, 23360), -- 38301
(38300, 'deDE', 'Wellendrescher der Gallschuppen', '', NULL, NULL, 23360), -- 38300
(3107, 'deDE', 'Ausgewachsener Brandungskriecher', '', NULL, NULL, 23360), -- 3107
(12350, 'deDE', 'Violetter Reitraptor', '', NULL, NULL, 23360), -- 12350
(10676, 'deDE', 'Räuber Jhash', '', NULL, NULL, 23360), -- 10676
(14543, 'deDE', 'Schneller olivfarbener Raptor', '', NULL, NULL, 23360), -- 14543
(12349, 'deDE', 'Türkisfarbener Reitraptor', '', NULL, NULL, 23360), -- 12349
(41142, 'deDE', 'Tierführer Marnlek', '', 'Fledermausführer', NULL, 23360), -- 41142
(40222, 'deDE', 'Spähfledermaus', '', NULL, NULL, 23360), -- 40222
(3933, 'deDE', 'Hai''zan', '', 'Metzger', NULL, 23360), -- 3933
(3184, 'deDE', 'Miao''zan', '', 'Alchemielehrer', NULL, 23360), -- 3184
(50004, 'deDE', 'Jamai', '', 'Jägerlehrerin', NULL, 23360), -- 50004
(50015, 'deDE', 'Munalti', '', 'Schurkenlehrer', NULL, 23360), -- 50015
(10369, 'deDE', 'Trayexir', '', 'Waffenhändler', NULL, 23360), -- 10369
(3186, 'deDE', 'K''waii', '', 'Gemischtwaren', NULL, 23360), -- 3186
(49998, 'deDE', 'Gusini', '', 'Hexenmeisterlehrer', NULL, 23360), -- 49998
(62114, 'deDE', 'Dornenechse', '', NULL, 'wildpetcapturable', 23360), -- 62114
(3194, 'deDE', 'Vel''rin Fang', '', NULL, NULL, 23360), -- 3194
(14545, 'deDE', 'Schneller blauer Raptor', '', NULL, NULL, 23360), -- 14545
(14544, 'deDE', 'Schneller orangefarbener Raptor', '', NULL, NULL, 23360), -- 14544
(12346, 'deDE', 'Smaragdgrüner Reitraptor', '', NULL, NULL, 23360), -- 12346
(7953, 'deDE', 'Xar''Ti', '', 'Reitlehrerin', NULL, 23360), -- 7953
(7952, 'deDE', 'Zjolnir', '', 'Raptorführer', NULL, 23360), -- 7952
(3304, 'deDE', 'Meister Vornal', '', NULL, NULL, 23360), -- 3304
(3188, 'deDE', 'Meister Gadrin', '', NULL, NULL, 23360), -- 3188
(3140, 'deDE', 'Lar Lauerzahn', '', NULL, NULL, 23360), -- 3140
(3187, 'deDE', 'Tai''tasi', '', 'Handwerkswaren', NULL, 23360), -- 3187
(51913, 'deDE', 'Wächter von Sen''jin', '', NULL, NULL, 23360), -- 51913
(50002, 'deDE', 'Bomsanchu', '', 'Magierlehrer', NULL, 23360), -- 50002
(50001, 'deDE', 'Parata', '', 'Priesterlehrerin', NULL, 23360), -- 50001
(6408, 'deDE', 'Ula''elek', '', NULL, NULL, 23360), -- 6408
(10578, 'deDE', 'Bom''bay', '', 'Hexendoktor in Ausbildung', NULL, 23360), -- 10578
(3185, 'deDE', 'Mishiki', '', 'Kräuterkundelehrerin', NULL, 23360), -- 3185
(49997, 'deDE', 'Den''chulu', '', 'Druidenlehrerin', NULL, 23360), -- 49997
(50011, 'deDE', 'Cona', '', 'Schamanenlehrerin', NULL, 23360), -- 50011
(5942, 'deDE', 'Zansoa', '', 'Angelbedarf', NULL, 23360), -- 5942
(5941, 'deDE', 'Lau''Tiki', '', 'Angellehrer', NULL, 23360), -- 5941
(3297, 'deDE', 'Behüter von Sen''jin', '', NULL, NULL, 23360), -- 3297
(39268, 'deDE', 'Heckenschütze der Nordwacht', '', 'Expeditionsstreitkräfte der Nordwacht', NULL, 23360), -- 39268
(5824, 'deDE', 'Hauptmann Stumpfhauer', '', 'Hauptmann der Schlachtwache', NULL, 23360), -- 5824
(3111, 'deDE', 'Stacheleber der Klingenmähnen', '', NULL, NULL, 23360), -- 3111
(75687, 'deDE', 'Verteidiger von Donnerfels', '', NULL, 'directions', 23360), -- 75687
(3881, 'deDE', 'Grimtak', '', 'Metzger', NULL, 23360), -- 3881
(3191, 'deDE', 'Koch Torka', '', NULL, NULL, 23360), -- 3191
(3112, 'deDE', 'Späher der Klingenmähnen', '', NULL, NULL, 23360), -- 3112
(3336, 'deDE', 'Takrin Pfadsucher', '', NULL, NULL, 23360), -- 3336
(5880, 'deDE', 'Un''Thuwa', '', 'Magierlehrer', NULL, 23360), -- 5880
(3171, 'deDE', 'Thotar', '', 'Jägerlehrer', NULL, 23360), -- 3171
(5943, 'deDE', 'Rawrk', '', 'Lehrer für Erste Hilfe', NULL, 23360), -- 5943
(47418, 'deDE', 'Runda', '', 'Berufsausbilderin', NULL, 23360), -- 47418
(3139, 'deDE', 'Gar''Thok', '', NULL, NULL, 23360), -- 3139
(3168, 'deDE', 'Flakk', '', 'Handwerkswaren', NULL, 23360), -- 3168
(3164, 'deDE', 'Jark', '', 'Gemischtwaren', NULL, 23360), -- 3164
(3170, 'deDE', 'Kaplak', '', 'Schurkenlehrer', NULL, 23360), -- 3170
(3620, 'deDE', 'Harruk', '', 'Tierausbilder', NULL, 23360), -- 3620
(3142, 'deDE', 'Orgnil Narbenseele', '', NULL, NULL, 23360), -- 3142
(3173, 'deDE', 'Swart', '', 'Schamanenlehrer', NULL, 23360), -- 3173
(3169, 'deDE', 'Tarshaw Raunarbe', '', 'Kriegerlehrer', NULL, 23360), -- 3169
(12430, 'deDE', 'Grunzerin Kor''ja', '', NULL, NULL, 23360), -- 12430
(3706, 'deDE', 'Tai''jin', '', 'Priesterlehrerin', NULL, 23360), -- 3706
(75686, 'deDE', 'Wächter der Dunkelspeere', '', NULL, 'directions', 23360), -- 75686
(15572, 'deDE', 'Urahnin Runentotem', '', NULL, NULL, 23360), -- 15572
(39423, 'deDE', 'Gail Krauselzopf', '', NULL, NULL, 23360), -- 39423
(45015, 'deDE', 'Kopfjäger der Dunkelspeere', '', NULL, 'Directions', 23360), -- 45015
(75685, 'deDE', 'Grunzer von Klingenhügel', '', NULL, 'Directions', 23360), -- 75685
(41140, 'deDE', 'Burok', '', 'Flugmeister', NULL, 23360), -- 41140
(3125, 'deDE', 'Rasselnder Skorpid', '', NULL, NULL, 23360), -- 3125
(11378, 'deDE', 'Großknecht Thazz''ril', '', NULL, NULL, 23360), -- 11378
(3145, 'deDE', 'Zureetha Fernblick', '', NULL, NULL, 23360), -- 3145
(51787, 'deDE', 'Grunzer des Höhlenbaus', '', NULL, NULL, 23360), -- 51787
(39400, 'deDE', 'Bäuerin Krella', '', NULL, NULL, 23360), -- 39400
(39399, 'deDE', 'Bauer Lok''lub', '', NULL, NULL, 23360), -- 39399
(39408, 'deDE', 'Landarbeiter von Durotar', '', NULL, NULL, 23360), -- 39408
(12776, 'deDE', 'Hraug', '', 'Dämonenausbilder', NULL, 23360), -- 12776
(9796, 'deDE', 'Galgar', '', NULL, NULL, 23360), -- 9796
(3882, 'deDE', 'Zlagk', '', 'Metzger', NULL, 23360), -- 3882
(3161, 'deDE', 'Rarc', '', 'Rüstungsschmied & Schildmacher', NULL, 23360), -- 3161
(3160, 'deDE', 'Huklah', '', 'Tuch- & Lederrüstungshändler', NULL, 23360), -- 3160
(3159, 'deDE', 'Kzan Dornenhieb', '', 'Waffenschmied', NULL, 23360), -- 3159
(3158, 'deDE', 'Duokna', '', 'Gemischtwaren', NULL, 23360), -- 3158
(3156, 'deDE', 'Nartok', '', 'Hexenmeisterlehrer', NULL, 23360), -- 3156
(3281, 'deDE', 'Sarkoth', '', NULL, NULL, 23360), -- 3281
(3287, 'deDE', 'Hana''zua', '', NULL, NULL, 23360), -- 3287
(39326, 'deDE', 'Raggaran', '', NULL, NULL, 23360), -- 39326
(3183, 'deDE', 'Yarrog Schattenbann', '', NULL, NULL, 23360), -- 3183
(3102, 'deDE', 'Teufelspirscher', '', NULL, NULL, 23360), -- 3102
(3101, 'deDE', 'Übler Familiar', '', NULL, NULL, 23360), -- 3101
(3098, 'deDE', 'Scheckiger Eber', '', NULL, NULL, 23360), -- 3098
(10176, 'deDE', 'Kaltunk', '', NULL, NULL, 23360), -- 10176
(3143, 'deDE', 'Gornek', '', NULL, NULL, 23360), -- 3143
(44820, 'deDE', 'Trainingsattrappe', '', NULL, NULL, 23360), -- 44820
(63296, 'deDE', 'Gato', '', 'Mönchslehrer', NULL, 23360), -- 63296
(39214, 'deDE', 'Karranisha', '', 'Jägerlehrer', NULL, 23360), -- 39214
(39206, 'deDE', 'Acrypha', '', 'Magierlehrerin', NULL, 23360), -- 39206
(5887, 'deDE', 'Canaga Erdenrufer', '', NULL, NULL, 23360), -- 5887
(3157, 'deDE', 'Shikrik', '', 'Schamanenlehrerin', NULL, 23360), -- 3157
(3155, 'deDE', 'Rwag', '', 'Schurkenlehrer', NULL, 23360), -- 3155
(3153, 'deDE', 'Frang', '', 'Kriegerlehrer', NULL, 23360), -- 3153
(39215, 'deDE', 'Gefangener Späher der Nordwacht', '', NULL, NULL, 23360), -- 39215
(3124, 'deDE', 'Skorpidarbeiter', '', NULL, NULL, 23360), -- 3124
(5952, 'deDE', 'Grunzer des Höhlenbaus', '', NULL, NULL, 23360), -- 5952
(39224, 'deDE', 'Lo''Shall', '', NULL, NULL, 23360), -- 39224
(49837, 'deDE', 'Dornenechse', '', NULL, NULL, 23360), -- 49837
(5890, 'deDE', 'Rotfelserdgeist', '', NULL, NULL, 23360), -- 5890
(39261, 'deDE', 'Bogenschütze der Nordwacht', '', 'Expeditionsstreitkräfte der Nordwacht', NULL, 23360), -- 39261
(3099, 'deDE', 'Scheckiger Terroreber', '', NULL, NULL, 23360), -- 3099
(62116, 'deDE', 'Krabbelkäfer', '', NULL, 'wildpetcapturable', 23360), -- 62116
(49743, 'deDE', 'Mistkäfer', '', NULL, NULL, 23360), -- 49743
(39251, 'deDE', 'Vorratskiste der Nordwacht', '', NULL, NULL, 23360), -- 39251
(39249, 'deDE', 'Träger der Nordwacht', '', NULL, NULL, 23360), -- 39249
(39245, 'deDE', 'Träger der Nordwacht', '', NULL, NULL, 23360), -- 39245
(62115, 'deDE', 'Mistkäfer', '', NULL, 'wildpetcapturable', 23360), -- 62115
(39255, 'deDE', 'Northwatch Caravan Spawner A', '', NULL, NULL, 23360), -- 39255
(3106, 'deDE', 'Brandungskriecher', '', NULL, NULL, 23360), -- 3106
(39004, 'deDE', 'Zwergbrandungskriecher', '', NULL, NULL, 23360), -- 39004
(39260, 'deDE', 'Fußsoldat der Nordwacht', '', 'Expeditionsstreitkräfte der Nordwacht', NULL, 23360), -- 39260
(39317, 'deDE', 'Späher der Nordwacht', '', NULL, NULL, 23360), -- 39317
(10556, 'deDE', 'Fauler Peon', '', NULL, NULL, 23360), -- 10556
(5823, 'deDE', 'Todesschinder', '', NULL, NULL, 23360), -- 5823
(34431, 'deDE', 'Balgor Peitschenschwinger', '', 'Karawanentreiber', NULL, 23360), -- 34431
(34438, 'deDE', 'Beifahrer', '', NULL, NULL, 23360), -- 34438
(51795, 'deDE', 'Friedensbewahrer der Blutwacht', '', NULL, NULL, 23420), -- 51795
(17529, 'deDE', '[PH]Channel Target', '', 'Ihr solltet mich nicht sehen', NULL, 23420), -- 17529
(22060, 'deDE', 'Fenissa die Assassine', '', NULL, NULL, 23420), -- 22060
(17508, 'deDE', 'Galaens Leichnam', '', NULL, NULL, 23420), -- 17508
(17606, 'deDE', 'Pionier der Sonnenfalken', '', NULL, NULL, 23420), -- 17606
(17850, 'deDE', 'Pioniergenerator der Sonnenfalken', '', NULL, NULL, 23420), -- 17850
(17496, 'deDE', 'Schandflosse', '', 'Rawgrlgrlgrlgrlgrrgle', NULL, 23420), -- 17496
(17327, 'deDE', 'Gezeitenrufer der Schwarzschlammküste', '', NULL, NULL, 23420), -- 17327
(17325, 'deDE', 'Futterwühler der Schwarzschlammküste', '', NULL, NULL, 23420), -- 17325
(17326, 'deDE', 'Späher der Schwarzschlammküste', '', NULL, NULL, 23420), -- 17326
(17528, 'deDE', 'Tzerak', '', NULL, NULL, 23420), -- 17528
(17337, 'deDE', 'Satyr der Nazzivus', '', NULL, NULL, 23420), -- 17337
(17339, 'deDE', 'Teufelsanbeter der Nazzivus', '', NULL, NULL, 23420), -- 17339
(17338, 'deDE', 'Schurke der Nazzivus', '', NULL, NULL, 23420), -- 17338
(17664, 'deDE', 'Matis der Grausame', '', 'Herold von Sironas', NULL, 23420), -- 17664
(17887, 'deDE', 'Geschöpf der Leere', '', NULL, NULL, 23420), -- 17887
(17886, 'deDE', 'Portalaufseher der Sonnenfalken', '', NULL, NULL, 23420), -- 17886
(17678, 'deDE', 'Sironas', '', NULL, NULL, 23420), -- 17678
(17979, 'deDE', 'Teslaspule der Blutmythosinsel', '', NULL, NULL, 23420), -- 17979
(17686, 'deDE', 'Forscher Cornelius', '', NULL, NULL, 23420), -- 17686
(17683, 'deDE', 'Zarakh', '', NULL, NULL, 23420), -- 17683
(17680, 'deDE', 'Eingesponnene Kreatur', '', NULL, NULL, 23420), -- 17680
(17610, 'deDE', 'Agent der Sonnenfalken', '', NULL, NULL, 23420), -- 17610
(17609, 'deDE', 'Saboteur der Sonnenfalken', '', NULL, NULL, 23420), -- 17609
(17607, 'deDE', 'Verteidiger der Sonnenfalken', '', NULL, NULL, 23420), -- 17607
(17608, 'deDE', 'Pyromant der Sonnenfalken', '', NULL, NULL, 23420), -- 17608
(61828, 'deDE', 'Infiziertes Eichhörnchen', '', NULL, 'wildpetcapturable', 23420), -- 61828
(17353, 'deDE', 'Verderbter Stampfer', '', NULL, NULL, 23420), -- 17353
(17346, 'deDE', 'Mutierter Greifer', '', NULL, NULL, 23420), -- 17346
(17522, 'deDE', 'Mythosspinner', '', NULL, NULL, 23420), -- 17522
(17523, 'deDE', 'Mythosfänger', '', NULL, NULL, 23420), -- 17523
(17358, 'deDE', 'Besudelter Wassergeist', '', NULL, NULL, 23420), -- 17358
(17927, 'deDE', 'Späherin Jorli', '', NULL, NULL, 23420), -- 17927
(17926, 'deDE', 'Späherin Loryi', '', NULL, NULL, 23420), -- 17926
(17986, 'deDE', 'Verteidigerin Corin', '', NULL, NULL, 23420), -- 17986
(17982, 'deDE', 'Demolierer Legoso', '', NULL, NULL, 23420), -- 17982
(17527, 'deDE', 'Wütender Blutfelshetzer', '', NULL, NULL, 23420), -- 17527
(17340, 'deDE', 'Schattenpirscher von Axxarien', '', NULL, NULL, 23420), -- 17340
(17342, 'deDE', 'Höllenrufer von Axxarien', '', NULL, NULL, 23420), -- 17342
(17341, 'deDE', 'Schwindler von Axxarien', '', NULL, NULL, 23420), -- 17341
(17494, 'deDE', 'Zevrax', '', NULL, NULL, 23420), -- 17494
(17661, 'deDE', 'Todespranke', '', NULL, NULL, 23420), -- 17661
(17421, 'deDE', 'Klopper Knacksklug', '', 'Forscherliga', NULL, 23420), -- 17421
(17329, 'deDE', 'Krieger der Schwarzschlammküste', '', NULL, NULL, 23420), -- 17329
(17713, 'deDE', 'Naga des Blutfluchs', '', NULL, NULL, 23420), -- 17713
(17712, 'deDE', 'Kapitän Edward Hanes', '', NULL, NULL, 23420), -- 17712
(17592, 'deDE', 'Messerrachen', '', NULL, NULL, 23420), -- 17592
(17674, 'deDE', 'Prinz Toreth', '', 'Drachenreiter von Loreth''Aran', NULL, 23420), -- 17674
(17588, 'deDE', 'Viridianwelpe', '', NULL, NULL, 23420), -- 17588
(17589, 'deDE', 'Viridianbrutling', '', NULL, NULL, 23420), -- 17589
(17328, 'deDE', 'Küstenschläger der Schwarzschlammküste', '', NULL, NULL, 23420), -- 17328
(17330, 'deDE', 'Seher der Schwarzschlammküste', '', NULL, NULL, 23420), -- 17330
(17550, 'deDE', 'Anomalie der Leere', '', NULL, NULL, 23420), -- 17550
(17344, 'deDE', 'Mutierter Würger', '', NULL, NULL, 23420), -- 17344
(17348, 'deDE', 'Alter Braunbär', '', NULL, NULL, 23420), -- 17348
(17350, 'deDE', 'Königsblauer Falter', '', NULL, NULL, 23420), -- 17350
(17604, 'deDE', 'Spion der Sonnenfalken', '', NULL, NULL, 23420), -- 17604
(17323, 'deDE', 'Kontaminierter Wildekin', '', NULL, NULL, 23420), -- 17323
(17324, 'deDE', 'Bestrahlter Wildekin', '', NULL, NULL, 23420), -- 17324
(17322, 'deDE', 'Infizierter Wildekin', '', NULL, NULL, 23420), -- 17322
(17704, 'deDE', 'Schwertkämpfer der Hand von Argus', '', 'Hand von Argus', NULL, 23420), -- 17704
(17600, 'deDE', 'Draeneikartograf', '', NULL, NULL, 23420), -- 17600
(18020, 'deDE', 'Verteidigerin Adrielle', '', 'Hand von Argus', NULL, 23420), -- 18020
(18021, 'deDE', 'Verteidiger Kaegan', '', 'Hand von Argus', NULL, 23420), -- 18021
(17554, 'deDE', 'Laando', '', 'Hippogryphenmeister', NULL, 23420), -- 17554
(17658, 'deDE', 'Exarch Admetius', '', NULL, NULL, 23420), -- 17658
(17423, 'deDE', 'Herold Mikolaas', '', NULL, NULL, 23420), -- 17423
(17988, 'deDE', 'Eventmanager des großen Finales', '', NULL, NULL, 23420), -- 17988
(18804, 'deDE', 'Ausgrabungsleiter Nachlan', '', 'Forscherliga', NULL, 23420), -- 18804
(49961, 'deDE', 'Viik', '', 'Kriegerlehrerin', NULL, 23420), -- 49961
(49959, 'deDE', 'Lennah', '', 'Magierlehrer', NULL, 23420), -- 49959
(17974, 'deDE', 'Gefangener Agent der Sonnenfalken, unsichtbarer Auslöser', '', NULL, NULL, 23420), -- 17974
(17424, 'deDE', 'Anachoret Paetheus', '', 'Lehrer für Erste Hilfe', NULL, 23420), -- 17424
(18026, 'deDE', 'Verteidiger Haqi', '', 'Hand von Argus', NULL, 23420), -- 18026
(17824, 'deDE', 'Gefangener Agent der Sonnenfalken', '', NULL, NULL, 23420), -- 17824
(17883, 'deDE', 'Ereignisauslöser für Velen', '', NULL, NULL, 23420), -- 17883
(17825, 'deDE', 'Vernehmungsbeauftragte Elysia', '', NULL, NULL, 23420), -- 17825
(17843, 'deDE', 'Verteidiger Kuros', '', 'Triumvirat der Hand', NULL, 23420), -- 17843
(17844, 'deDE', 'Verteidiger Aesom', '', 'Triumvirat der Hand', NULL, 23420), -- 17844
(17684, 'deDE', 'Verteidiger Boros', '', 'Triumvirat der Hand', NULL, 23420), -- 17684
(17663, 'deDE', 'Jessera von Mac''Aree', '', NULL, NULL, 23420), -- 17663
(17703, 'deDE', 'Bote Hermesius', '', NULL, NULL, 23420), -- 17703
(18803, 'deDE', 'Botschafter Frasaboo der Tannenruhfeste', '', 'Gesandter der Tannenruhfeste', NULL, 23420), -- 18803
(18427, 'deDE', 'Fazu', '', 'Handwerker', NULL, 23420), -- 18427
(18030, 'deDE', 'Ritterliche Verteidigerin Zunade', '', 'Hand von Argus', NULL, 23420), -- 18030
(17676, 'deDE', 'Achelus', '', NULL, NULL, 23420), -- 17676
(17667, 'deDE', 'Beega', '', 'Bogenmacherin', NULL, 23420), -- 17667
(49964, 'deDE', 'Adrihi', '', 'Schamanenlehrer', NULL, 23420), -- 49964
(49962, 'deDE', 'Andrenatol', '', 'Priesterlehrer', NULL, 23420), -- 49962
(49957, 'deDE', 'Delemm', '', 'Paladinlehrerin', NULL, 23420), -- 49957
(17434, 'deDE', 'Morae', '', 'Kräuterkundelehrerin', NULL, 23420), -- 17434
(17642, 'deDE', 'Fährtenleser Lyceon', '', NULL, NULL, 23420), -- 17642
(18811, 'deDE', 'Meriaad', '', 'Gemischtwaren', NULL, 23420), -- 18811
(18028, 'deDE', 'Verteidigerin Akee', '', 'Hand von Argus', NULL, 23420), -- 18028
(17433, 'deDE', 'Verteidigerin Aalesia', '', NULL, NULL, 23420), -- 17433
(18029, 'deDE', 'Verteidiger Kajad', '', 'Hand von Argus', NULL, 23420), -- 18029
(18025, 'deDE', 'Verteidiger Auston', '', 'Hand von Argus', NULL, 23420), -- 18025
(17549, 'deDE', 'Friedensbewahrer der Blutwacht', '', NULL, NULL, 23420), -- 17549
(17553, 'deDE', 'Versorger Topher Loaal', '', 'Gastwirt', NULL, 23420), -- 17553
(18023, 'deDE', 'Verteidiger Kranos', '', 'Hand von Argus', NULL, 23420), -- 18023
(49966, 'deDE', 'Shaniri', '', 'Jägerlehrerin', NULL, 23420), -- 49966
(17666, 'deDE', 'Astur', '', 'Stallmeister', NULL, 23420), -- 17666
(10780, 'deDE', 'Infiziertes Reh', '', NULL, NULL, 23420), -- 10780
(17682, 'deDE', 'Prinzessin der Tannenruhfeste', '', NULL, NULL, 23420), -- 17682
(17701, 'deDE', 'Lord Xiz', '', NULL, NULL, 23420), -- 17701
(17702, 'deDE', 'Oberhäuptling der Sichelklauen', '', NULL, NULL, 23420), -- 17702
(17542, 'deDE', 'Junger Schamane der Furbolgs', '', NULL, NULL, 23420), -- 17542
(17321, 'deDE', 'Krieger der Sichelklauen', '', NULL, NULL, 23420), -- 17321
(17320, 'deDE', 'Schamane der Sichelklauen', '', NULL, NULL, 23420), -- 17320
(17659, 'deDE', 'Klinge von Argus', '', NULL, NULL, 23420), -- 17659
(21145, 'deDE', 'Klein Azimi', '', 'Lebensmittelverkäufer', NULL, 23420), -- 21145
(17649, 'deDE', 'Kessel', '', 'Elekklord', NULL, 23420), -- 17649
(17586, 'deDE', 'Vorkhan der Elekktreiber', '', NULL, NULL, 23420), -- 17586
(17599, 'deDE', 'Aonar', '', NULL, NULL, 23420), -- 17599
(17352, 'deDE', 'Verderbter Treant', '', NULL, NULL, 23420), -- 17352
(18173, 'deDE', 'Elekk der Blutmythosinsel', '', NULL, NULL, 23420), -- 18173
(17673, 'deDE', 'Stinkmorchelkieme', '', NULL, NULL, 23420), -- 17673
(62051, 'deDE', 'Felshetzerjungtier', '', NULL, 'wildpetcapturable', 23420), -- 62051
(17347, 'deDE', 'Ergrauter Braunbär', '', NULL, NULL, 23420), -- 17347
(17343, 'deDE', 'Distelpeitscher', '', NULL, NULL, 23420), -- 17343
(17334, 'deDE', 'Marodeur der Hasspranken', '', NULL, NULL, 23420), -- 17334
(17336, 'deDE', 'Zauberin der Hasspranken', '', NULL, NULL, 23420), -- 17336
(10779, 'deDE', 'Infiziertes Eichhörnchen', '', NULL, NULL, 23420), -- 10779
(17333, 'deDE', 'Kreischerin der Hasspranken', '', NULL, NULL, 23420), -- 17333
(17331, 'deDE', 'Küstenpirscher der Hasspranken', '', NULL, NULL, 23420), -- 17331
(61827, 'deDE', 'Infiziertes Rehkitz', '', NULL, 'wildpetcapturable', 23420), -- 61827
(17349, 'deDE', 'Blauer Falter', '', NULL, NULL, 23420), -- 17349
(17345, 'deDE', 'Braunbär', '', NULL, NULL, 23420), -- 17345
(17525, 'deDE', 'Junger Blutfelshetzer', '', NULL, NULL, 23420), -- 17525
(39020, 'deDE', 'Wahnsinniger Bilgewässer Überlebender', '', NULL, NULL, 23360), -- 39020
(46128, 'deDE', 'Smaragdskarabäus', '', NULL, NULL, 23360), -- 46128
(46126, 'deDE', 'Türkisskarabäus', '', NULL, NULL, 23360), -- 46126
(46127, 'deDE', 'Saphirskarabäus', '', NULL, NULL, 23360), -- 46127
(46129, 'deDE', 'Amethystskarabäus', '', NULL, NULL, 23360), -- 46129
(55211, 'deDE', 'Urahne Menkhaf', '', NULL, NULL, 23360), -- 55211
(45875, 'deDE', 'Spähertagebuch', '', NULL, 'Speak', 23360), -- 45875
(45874, 'deDE', 'Späher von Schnottz', '', NULL, NULL, 23360), -- 45874
(46196, 'deDE', 'Evening Star TARGET', '', NULL, NULL, 23360), -- 46196
(46197, 'deDE', 'Shooting Star TARGET', '', NULL, NULL, 23360), -- 46197
(46195, 'deDE', 'Morning Star TARGET', '', NULL, NULL, 23360), -- 46195
(46198, 'deDE', 'North Star TARGET', '', NULL, NULL, 23360), -- 46198
(45204, 'deDE', 'Skarf', '', NULL, NULL, 23360), -- 45204
(45205, 'deDE', 'Sultan Oogah', '', NULL, NULL, 23360), -- 45205
(45202, 'deDE', 'Räudige Hyäne', '', NULL, NULL, 23360), -- 45202
(45190, 'deDE', 'Sandpygmäe', '', NULL, NULL, 23360), -- 45190
(49228, 'deDE', 'Unteroffizier von Ramkahen', '', NULL, 'Speak', 23360), -- 49228
(49224, 'deDE', 'Champion von Ramkahen', '', NULL, NULL, 23360), -- 49224
(48761, 'deDE', 'Salhet', '', NULL, NULL, 23360), -- 48761
(48701, 'deDE', 'Sonnenpriester von Nahom', '', NULL, NULL, 23360), -- 48701
(46335, 'deDE', 'Unteroffizier von Ramkahen', '', NULL, NULL, 23360), -- 46335
(46883, 'deDE', 'Salhet', '', NULL, NULL, 23360), -- 46883
(46336, 'deDE', 'Bogenschütze von Ramkahen', '', NULL, NULL, 23360), -- 46336
(48697, 'deDE', 'Bogenschütze von Ramkahen', '', NULL, NULL, 23360), -- 48697
(48501, 'deDE', 'Sonnenprophet Tumet', '', NULL, NULL, 23360), -- 48501
(44417, 'deDE', 'Verpflegungsoffizierin Flexgang', '', 'Speis & Trank', NULL, 23360), -- 44417
(40560, 'deDE', 'Verdeckter Agent', '', 'Geheimdienst von Gnomeregan', NULL, 23360), -- 40560
(62953, 'deDE', 'Möwe', '', NULL, 'wildpetcapturable', 23360), -- 62953
(44598, 'deDE', 'Wüstenblume', '', NULL, NULL, 23360), -- 44598
(40527, 'deDE', 'Uferdünenkrebs', '', NULL, NULL, 23360), -- 40527
(5431, 'deDE', 'Brandungsgleiter', '', NULL, NULL, 23360), -- 5431
(12124, 'deDE', 'Großer Hai', '', NULL, NULL, 23360), -- 12124
(48626, 'deDE', 'Räuber von Neferset', '', NULL, NULL, 23360), -- 48626
(48627, 'deDE', 'Plünderer von Neferset', '', NULL, NULL, 23360), -- 48627
(48625, 'deDE', 'Seher von Neferset', '', NULL, NULL, 23360), -- 48625
(49214, 'deDE', 'Räuberfürst Havat', '', NULL, NULL, 23360), -- 49214
(46362, 'deDE', 'Uldumsphäre', '', NULL, NULL, 23360), -- 46362
(46750, 'deDE', 'Fusionskern', '', NULL, NULL, 23360), -- 46750
(46361, 'deDE', 'Uldumsphäre', '', NULL, NULL, 23360), -- 46361
(46715, 'deDE', 'Wibsongroßrechner', '', NULL, NULL, 23360), -- 46715
(47014, 'deDE', 'Verfluchter Feldmesser', '', NULL, NULL, 23360), -- 47014
(46888, 'deDE', 'Instabiler Bombenbot', '', NULL, 'Interact', 23360), -- 46888
(46617, 'deDE', 'Verfluchter Ingenieur', '', NULL, NULL, 23360), -- 46617
(46592, 'deDE', 'Robogenieur Mischgeweb', '', NULL, NULL, 23360), -- 46592
(46590, 'deDE', 'Wahnsinniger Gräber', '', NULL, NULL, 23360), -- 46590
(46587, 'deDE', 'Pfuschelflux der Wahnsinnige', '', NULL, NULL, 23360), -- 46587
(51760, 'deDE', 'Aasvogel', '', NULL, NULL, 23360), -- 51760
(46462, 'deDE', 'Generic Trigger LAB (Uninteractible)', '', NULL, NULL, 23360), -- 46462
(46425, 'deDE', 'Gefangener von Ramkahen', '', NULL, NULL, 23360), -- 46425
(46311, 'deDE', 'Wachposten von Neferset', '', NULL, NULL, 23360), -- 46311
(48263, 'deDE', 'Oberste Wache von Neferset', '', NULL, NULL, 23360), -- 48263
(47306, 'deDE', 'Schildwache von Neferset', '', NULL, NULL, 23360), -- 47306
(50748, 'deDE', 'Nyaj', '', NULL, NULL, 23420), -- 50748
(41135, 'deDE', '"Hahnentritt" Johnson', '', NULL, NULL, 23420), -- 41135
(41131, 'deDE', 'Tirth, der verrückte Magus', '', NULL, NULL, 23420), -- 41131
(7394, 'deDE', 'Anconahuhn', '', NULL, NULL, 23420), -- 7394
(46206, 'deDE', 'Tonys Schatz', '', NULL, NULL, 23420), -- 46206
(40958, 'deDE', 'Tony Zweihauer', '', NULL, NULL, 23420), -- 40958
(40888, 'deDE', 'Flämmer', '', NULL, NULL, 23420), -- 40888
(40959, 'deDE', 'Höhlenknüppler', '', NULL, NULL, 23420), -- 40959
(40885, 'deDE', 'Wizzel Kupferbolz', '', NULL, NULL, 23420), -- 40885
(2110, 'deDE', 'Schwarze Ratte', '', NULL, NULL, 23420), -- 2110
(41060, 'deDE', 'Ajamon Geistrufer', '', NULL, NULL, 23420), -- 41060
(48128, 'deDE', 'Gestrandete Spiegelpanzerschildkröte', '', NULL, NULL, 23360), -- 48128
(44722, 'deDE', 'Verzerrtes Spiegelbild von Narrain', '', NULL, NULL, 23360), -- 44722
(15520, 'deDE', 'O''Reily', '', NULL, NULL, 23360), -- 15520
(38571, 'deDE', 'Überlebender des Dampfdruckkartells', '', NULL, NULL, 23360), -- 38571
(40583, 'deDE', 'Blockierer der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 40583
(21448, 'deDE', 'Heckenschütze von Gadgetzan', '', NULL, NULL, 23360), -- 21448
(40604, 'deDE', 'Rettungsballon des Dampfdruckkartells', '', NULL, NULL, 23360), -- 40604
(7733, 'deDE', 'Gastwirt Zischgrimbel', '', 'Gastwirt', NULL, 23360), -- 7733
(9985, 'deDE', 'Laziphus', '', 'Stallmeister', NULL, 23360), -- 9985
(14743, 'deDE', 'Jhordy Lapforge', '', 'Ingenieur', NULL, 23360), -- 14743
(40508, 'deDE', 'Schlächterbot', '', NULL, NULL, 23360), -- 40508
(39178, 'deDE', 'Driz Zupfbogen', '', NULL, NULL, 23360), -- 39178
(16014, 'deDE', 'Mux Manamix', '', NULL, NULL, 23360), -- 16014
(8126, 'deDE', 'Nixx Sprossfeder', '', 'Lehrer für goblinsche Ingenieurskunst', NULL, 23360), -- 8126
(43964, 'deDE', 'Grux Funkelknack', '', 'Reagenzien', NULL, 23360), -- 43964
(40589, 'deDE', 'Dirge Schnetzelhack', '', 'Metzger', NULL, 23360), -- 40589
(40588, 'deDE', 'Flüchtling des Dampfdruckkartells', '', NULL, NULL, 23360), -- 40588
(39159, 'deDE', 'Mazoga Credit Stalker', '', NULL, NULL, 23360), -- 39159
(8124, 'deDE', 'Qizzik', '', 'Bankier', NULL, 23360), -- 8124
(7799, 'deDE', 'Gaunerdorn', '', 'Bankier', NULL, 23360), -- 7799
(7824, 'deDE', 'Bulkrek Zornfaust', '', 'Windreitermeister', NULL, 23360), -- 7824
(39034, 'deDE', 'Dr. Haudenlukas', '', 'Kampfveranstalter', NULL, 23360), -- 39034
(69323, 'deDE', 'Der winzige Tayger', '', 'Rüstmeister für ruchlose Gladiatoren', NULL, 23360), -- 69323
(69322, 'deDE', 'Kappe Carlin', '', 'Rüstmeister für kataklysmische Gladiatoren', NULL, 23360), -- 69322
(40216, 'deDE', 'Blazzek der Beißer', '', 'Rüstmeister für boshafte Gladiatoren', NULL, 23360), -- 40216
(20278, 'deDE', 'Vixton Quetschpfeife', '', 'Rüstmeister für blutrünstige Gladiatoren', NULL, 23360), -- 20278
(19860, 'deDE', 'Katrina Turner', '', NULL, NULL, 23360), -- 19860
(5594, 'deDE', 'Alchemist Stößelbruch', '', 'Alchemiebedarf', NULL, 23360), -- 5594
(8883, 'deDE', 'Reitpferd', '', NULL, NULL, 23360), -- 8883
(4708, 'deDE', 'Kainer', '', NULL, NULL, 23360), -- 4708
(28126, 'deDE', 'Don Carlos', '', NULL, NULL, 23360), -- 28126
(8882, 'deDE', 'Reittiger', '', NULL, NULL, 23360), -- 8882
(16417, 'deDE', 'Rumsen Zischelbrack', '', NULL, NULL, 23360), -- 16417
(8884, 'deDE', 'Skelettreittier', '', NULL, NULL, 23360), -- 8884
(40581, 'deDE', 'Gargantapid', '', NULL, NULL, 23360), -- 40581
(44398, 'deDE', 'Snart Razzelgrins', '', 'Gemischtwaren', NULL, 23360), -- 44398
(40582, 'deDE', 'Chelsea Rieselrost', '', NULL, NULL, 23360), -- 40582
(40580, 'deDE', 'Gus Rieselrost', '', NULL, NULL, 23360), -- 40580
(38927, 'deDE', 'Mazoga', '', NULL, NULL, 23360), -- 38927
(7804, 'deDE', 'Trenton Lichthammer', '', 'Der Mithrilorden', NULL, 23360), -- 7804
(7270, 'deDE', 'Zombie der Sandwüter', '', NULL, NULL, 23360), -- 7270
(5649, 'deDE', 'Bluttrinker der Sandwüter', '', NULL, NULL, 23360), -- 5649
(5645, 'deDE', 'Balgabzieher der Sandwüter', '', NULL, NULL, 23360), -- 5645
(5647, 'deDE', 'Feuerrufer der Sandwüter', '', NULL, NULL, 23360), -- 5647
(38909, 'deDE', 'Zombie der Sandwüter', '', NULL, NULL, 23360), -- 38909
(5646, 'deDE', 'Axtwerfer der Sandwüter', '', NULL, NULL, 23360), -- 5646
(38502, 'deDE', 'Nablya', '', 'Die Wächterin', NULL, 23360), -- 38502
(5459, 'deDE', 'Tunnelgräber der Centipaar', '', NULL, NULL, 23360), -- 5459
(40717, 'deDE', 'Dünenscherenpeitscher', '', NULL, NULL, 23360), -- 40717
(40666, 'deDE', 'Dünenscherenbrutling', '', NULL, NULL, 23360), -- 40666
(40656, 'deDE', 'Dünenscherenmatriarchin', '', NULL, NULL, 23360), -- 40656
(39186, 'deDE', 'Höllenblick', '', NULL, NULL, 23360), -- 39186
(40572, 'deDE', 'Eingebildete Modeschöpferin', '', 'Modeschöpferin', NULL, 23360), -- 40572
(8131, 'deDE', 'Blizrik Bockschuss', '', 'Büchsenmacher', NULL, 23360), -- 8131
(8129, 'deDE', 'Wrinkle Goldstahl', '', 'Überragender Rüstungsschmied', NULL, 23360), -- 8129
(6568, 'deDE', 'Vizzklick', '', 'Schneiderbedarf', NULL, 23360), -- 6568
(15586, 'deDE', 'Urahne Traumdeuter', '', NULL, NULL, 23360), -- 15586
(11756, 'deDE', 'Quinn', '', NULL, NULL, 23360), -- 11756
(8128, 'deDE', 'Pikkle', '', 'Bergbaulehrer', NULL, 23360), -- 8128
(7823, 'deDE', 'Bera Steinhammer', '', 'Greifenmeisterin', NULL, 23360), -- 7823
(8736, 'deDE', 'Buzzek Pratzenschwing', '', 'Ingenieurskunstlehrer', NULL, 23360), -- 8736
(8661, 'deDE', 'Auktionator Barto', '', NULL, NULL, 23360), -- 8661
(43418, 'deDE', 'Sprinkel Noggenfogger', '', 'Noggenfoggers Elixier', NULL, 23360), -- 43418
(40806, 'deDE', 'Kelseys Kommunikator', '', NULL, NULL, 23360), -- 40806
(38535, 'deDE', 'Kelsey Stahlfunken', '', 'Geheimdienst von Gnomeregan', NULL, 23360), -- 38535
(38534, 'deDE', 'Megs Schredderschreck', '', 'Bilgewasserkartell', NULL, 23360), -- 38534
(38532, 'deDE', 'Marin Noggenfogger', '', 'Baron von Gadgetzan', NULL, 23360), -- 38532
(14567, 'deDE', 'Derotain Matschnipper', '', 'Die Thoriumbruderschaft', NULL, 23360), -- 14567
(9460, 'deDE', 'Haudrauf von Gadgetzan', '', NULL, NULL, 23360), -- 9460
(5411, 'deDE', 'Krinkle Goldstahl', '', 'Schmiedekunstbedarf', NULL, 23360), -- 5411
(5426, 'deDE', 'Eiterpfotenhyäne', '', NULL, NULL, 23360), -- 5426
(5458, 'deDE', 'Arbeiter der Centipaar', '', NULL, NULL, 23360), -- 5458
(5455, 'deDE', 'Wespe der Centipaar', '', NULL, NULL, 23360), -- 5455
(5460, 'deDE', 'Sandhäscher der Centipaar', '', NULL, NULL, 23360), -- 5460
(48185, 'deDE', 'Walhai', '', NULL, NULL, 23360), -- 48185
(40648, 'deDE', 'Zakkaru', '', 'Brut von Makkari', NULL, 23360), -- 40648
(41730, 'deDE', 'Sandvortex', '', NULL, NULL, 23360), -- 41730
(47044, 'deDE', 'Lamba Kornpfeife', '', NULL, NULL, 23360), -- 47044
(47043, 'deDE', 'Lixi Öldüse', '', NULL, NULL, 23360), -- 47043
(47042, 'deDE', 'Schantal Blechlagerung', '', NULL, NULL, 23360), -- 47042
(62257, 'deDE', 'Sandkätzchen', '', NULL, 'wildpetcapturable', 23360), -- 62257
(5473, 'deDE', 'Ogermagier der Dünenbrecher', '', NULL, NULL, 23360), -- 5473
(38880, 'deDE', 'Sandscharrer', '', NULL, NULL, 23360), -- 38880
(5475, 'deDE', 'Hexenmeister der Dünenbrecher', '', NULL, NULL, 23360), -- 5475
(5471, 'deDE', 'Oger der Dünenbrecher', '', NULL, NULL, 23360), -- 5471
(5474, 'deDE', 'Schläger der Dünenbrecher', '', NULL, NULL, 23360), -- 5474
(5472, 'deDE', 'Vollstrecker der Dünenbrecher', '', NULL, NULL, 23360), -- 5472
(38856, 'deDE', '"Abgesandter" der Dünenbrecher', '', NULL, NULL, 23360), -- 38856
(38849, 'deDE', 'Megs Schredderschreck', '', 'Bilgewasserkartell', NULL, 23360), -- 38849
(38847, 'deDE', 'Grohk', '', 'Gemischtwaren', NULL, 23360), -- 38847
(38916, 'deDE', 'Sandsteinirdener', '', NULL, NULL, 23360), -- 38916
(15573, 'deDE', 'Urahnin Rachtotem', '', NULL, NULL, 23360), -- 15573
(40754, 'deDE', 'Besen von Tanaris', '', NULL, NULL, 23360), -- 40754
(44374, 'deDE', 'Sherm', '', 'Anwerber von Marschalls Expedition', NULL, 23360), -- 44374
(41215, 'deDE', 'Raina Sonnenrutsch', '', 'Flugmeisterin', NULL, 23360), -- 41215
(40747, 'deDE', 'Selia Sonnenglanz', '', NULL, NULL, 23360), -- 40747
(43980, 'deDE', 'Chase Ewigstreb', '', 'Handwerkswaren', NULL, 23360), -- 43980
(43972, 'deDE', 'Cordelia Ewigstreb', '', 'Speis & Trank', NULL, 23360), -- 43972
(38922, 'deDE', 'Prüfer Andoren Morgendunst', '', 'Die Archäologische Akademie', NULL, 23360), -- 38922
(38914, 'deDE', 'Sandsteingolem', '', NULL, NULL, 23360), -- 38914
(39185, 'deDE', 'Geiferrachen', '', NULL, NULL, 23360), -- 39185
(5430, 'deDE', 'Sengender Roc', '', NULL, NULL, 23360), -- 5430
(40662, 'deDE', 'Opfer des Dampfdruckkartells', '', NULL, NULL, 23360), -- 40662
(5419, 'deDE', 'Glashautbasilisk', '', NULL, NULL, 23360), -- 5419
(40827, 'deDE', 'Thurda', '', 'Flugmeisterin', NULL, 23360), -- 40827
(40815, 'deDE', 'Fedli Hochfass', '', 'Speis & Trank', NULL, 23360), -- 40815
(39191, 'deDE', 'Hilda Runenschwur', '', 'Forscherliga', NULL, 23360), -- 39191
(38578, 'deDE', 'Flinn', '', 'Anwerber von Marschalls Expedition', NULL, 23360), -- 38578
(40826, 'deDE', 'Brod Ambossbart', '', 'Gemischtwaren', NULL, 23360), -- 40826
(40109, 'deDE', 'Ausgrabungsleiter Gunstan', '', 'Forscherliga', NULL, 23360), -- 40109
(38997, 'deDE', 'Buddler der Archäologischen Akademie', '', NULL, NULL, 23360), -- 38997
(38998, 'deDE', 'Buddler der Forscherliga', '', NULL, NULL, 23360), -- 38998
(44694, 'deDE', 'Giftiger Tunnelgräber', '', NULL, NULL, 23360), -- 44694
(44869, 'deDE', 'Marvon Nietensucher', '', NULL, NULL, 23360), -- 44869
(44546, 'deDE', 'Dünenscherenwühler', '', NULL, NULL, 23360), -- 44546
(44750, 'deDE', 'Kalif Skorpidstich', '', NULL, NULL, 23360), -- 44750
(44613, 'deDE', 'Dunkelzauberer der Wüstenwanderer', '', NULL, NULL, 23360), -- 44613
(38714, 'deDE', 'Carmen Ibanozzle', '', 'Gastwirtin', NULL, 23360), -- 38714
(20025, 'deDE', 'Versuchsobjekt: Düsterjaguar', '', NULL, NULL, 23360), -- 20025
(38742, 'deDE', 'Wrangled Bug Credit Bunny', '', NULL, NULL, 23360), -- 38742
(38706, 'deDE', 'Zeke Scheuerschuh', '', NULL, NULL, 23360), -- 38706
(41214, 'deDE', 'Slick Senkfall', '', 'Flugmeister', NULL, 23360), -- 41214
(19959, 'deDE', 'Eingekerkerte ewige Drachenbrut', '', NULL, NULL, 23360), -- 19959
(19933, 'deDE', 'Nozari', '', 'Hüter der Zeit', NULL, 23360), -- 19933
(19932, 'deDE', 'Andormu', '', 'Hüter der Zeit', NULL, 23360), -- 19932
(20131, 'deDE', 'Nozari', '', 'Hüter der Zeit', NULL, 23360), -- 20131
(20130, 'deDE', 'Andormu', '', 'Hüter der Zeit', NULL, 23360), -- 20130
(19936, 'deDE', 'Arazmodu', '', 'Die Wächter der Sande', NULL, 23360), -- 19936
(19935, 'deDE', 'Soridormi', '', 'Die Wächter der Sande', NULL, 23360), -- 19935
(18542, 'deDE', 'Alexston Chrom', '', 'Taverne der Zeit', NULL, 23360), -- 18542
(20142, 'deDE', 'Ordner der Zeit', '', 'Hüter der Zeit', NULL, 23360), -- 20142
(8198, 'deDE', 'Tick', '', NULL, NULL, 23360), -- 8198
(80675, 'deDE', 'Auridormi', '', 'Wächterin des Schlachtzugsbrowsers', NULL, 23360), -- 80675
(8197, 'deDE', 'Chronalis', '', NULL, NULL, 23360), -- 8197
(22872, 'deDE', 'Scherbenwelt Kinderwoche Höhlen der Zeit Auslöser', '', NULL, NULL, 23360), -- 22872
(44587, 'deDE', 'Dünenscherenpirscher', '', NULL, NULL, 23360), -- 44587
(21643, 'deDE', 'Alurmi', '', 'Rüstmeisterin der Hüter der Zeit', NULL, 23360), -- 21643
(20082, 'deDE', 'Yarley', '', 'Rüstungsschmied', NULL, 23360), -- 20082
(20081, 'deDE', 'Bortega', '', 'Reagenzien & Gifte', NULL, 23360), -- 20081
(20080, 'deDE', 'Galgrom', '', 'Versorger', NULL, 23360), -- 20080
(68820, 'deDE', 'Jungtier des ewigen Drachenschwarms', '', NULL, 'wildpetcapturable', 23360), -- 68820
(40657, 'deDE', 'Sonnenbadende Kobra', '', NULL, NULL, 23360), -- 40657
(38750, 'deDE', 'Strömungsbrecher', '', NULL, NULL, 23360), -- 38750
(44759, 'deDE', 'Andre Feuerbart', '', NULL, NULL, 23360), -- 44759
(38749, 'deDE', 'Kapitän Schreckensbart', '', NULL, NULL, 23360), -- 38749
(44763, 'deDE', 'Ertrunkener Schläger der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 44763
(44762, 'deDE', 'Durchnässter Musketier der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 44762
(38660, 'deDE', 'Lost Rigger Building 01 Bunny', '', NULL, NULL, 23360), -- 38660
(40635, 'deDE', 'Schläger der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 40635
(40632, 'deDE', 'Musketier der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 40632
(38662, 'deDE', 'Lost Rigger Building 02 Bunny', '', NULL, NULL, 23360), -- 38662
(38650, 'deDE', 'Mietschredder', '', NULL, NULL, 23360), -- 38650
(44861, 'deDE', 'Tanaris 4.x Bunny', '', NULL, NULL, 23360), -- 44861
(40593, 'deDE', 'Zuchtmeister der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 40593
(40636, 'deDE', 'Fahrensmann der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 40636
(38649, 'deDE', 'Geheimdienststampfer', '', NULL, NULL, 23360), -- 38649
(38665, 'deDE', 'Lost Rigger Building 03 Bunny', '', NULL, NULL, 23360), -- 38665
(7855, 'deDE', 'Pirat der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 7855
(7858, 'deDE', 'Schwadroneur der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 7858
(38646, 'deDE', 'Geheimdienstkampfanzugträger', '', NULL, NULL, 23360), -- 38646
(38648, 'deDE', 'Bilgewässer Schlägertyp', '', NULL, NULL, 23360), -- 38648
(38823, 'deDE', 'Toter Südmeerpirat', '', NULL, NULL, 23360), -- 38823
(38824, 'deDE', 'Toter Schwadroneur der Südmeerfreibeuter', '', NULL, NULL, 23360), -- 38824
(38719, 'deDE', 'Brennender Südmeerpirat', '', NULL, NULL, 23360), -- 38719
(14123, 'deDE', 'Stahlkieferschnapper', '', NULL, NULL, 23360), -- 14123
(40764, 'deDE', 'Wüstenkriecher', '', NULL, NULL, 23360), -- 40764
(39022, 'deDE', 'Gezeitenschreiter', '', NULL, NULL, 23360), -- 39022
(8667, 'deDE', 'Windstoßvortex', '', NULL, NULL, 23360), -- 8667
(20069, 'deDE', 'Versuchsobjekt: Nachtelfenirrwisch', '', NULL, NULL, 23360), -- 20069
(5465, 'deDE', 'Landwüterich', '', NULL, NULL, 23360), -- 5465
(15192, 'deDE', 'Anachronos', '', NULL, NULL, 23360), -- 15192
(20053, 'deDE', 'Versuchsobjekt: Wachposten von Lodaeron', '', NULL, NULL, 23360), -- 20053
(20055, 'deDE', 'Versuchsobjekt: Bauer von Tarrens Mühle', '', NULL, NULL, 23360), -- 20055
(20054, 'deDE', 'Versuchsobjekt: Wachmann von Lordaeron', '', NULL, NULL, 23360), -- 20054
(44573, 'deDE', 'Dünenwurm', '', NULL, NULL, 23360), -- 44573
(40528, 'deDE', 'Käferjäger des Dampfdruckkartells', '', NULL, NULL, 23360), -- 40528
(11811, 'deDE', 'Narain Pfauentraum', '', NULL, NULL, 23360), -- 11811
(38994, 'deDE', 'Pheromone Bunny C', '', NULL, NULL, 23360), -- 38994
(38993, 'deDE', 'Pheromone Bunny B', '', NULL, NULL, 23360), -- 38993
(44873, 'deDE', 'Bussard', '', NULL, NULL, 23360), -- 44873
(49836, 'deDE', 'Stinkkäfer', '', NULL, NULL, 23360), -- 49836
(44611, 'deDE', 'Überlebenskünstler der Wüstenläufer', '', NULL, NULL, 23360), -- 44611
(44612, 'deDE', 'Fährtenleser der Wüstenwanderer', '', NULL, NULL, 23360), -- 44612
(9397, 'deDE', 'Ausgegrabenes Fossil', '', NULL, NULL, 23360), -- 9397
(62256, 'deDE', 'Stinkkäfer', '', NULL, 'wildpetcapturable', 23360), -- 62256
(44594, 'deDE', 'Sonnenotter', '', NULL, NULL, 23360), -- 44594
(44714, 'deDE', 'Fronkel der Gestörte', '', NULL, NULL, 23360), -- 44714
(7770, 'deDE', 'Winkey', '', NULL, NULL, 23360), -- 7770
(44568, 'deDE', 'Dünenklapperer', '', NULL, NULL, 23360), -- 44568
(19934, 'deDE', 'Zaladormu', '', NULL, NULL, 23360), -- 19934
(29867, 'deDE', 'Exemplar eines Fußsoldaten von Lordaeron', '', NULL, NULL, 23360), -- 29867
(29865, 'deDE', 'Exemplar eines Bürgers von Stratholme', '', NULL, NULL, 23360), -- 29865
(44569, 'deDE', 'Sandpeitscher', '', NULL, NULL, 23360), -- 44569
(29873, 'deDE', 'Exemplar einer Katze', '', NULL, NULL, 23360), -- 29873
(29868, 'deDE', 'Exemplar eines Jungen von Stratholme', '', NULL, NULL, 23360), -- 29868
(29866, 'deDE', 'Exemplar eines Einwohners von Stratholme', '', NULL, NULL, 23360), -- 29866
(19951, 'deDE', 'Aufseher der Zeit', '', NULL, NULL, 23360), -- 19951
(19918, 'deDE', 'Wächter der Zeit', '', NULL, NULL, 23360), -- 19918
(5427, 'deDE', 'Tollwütige Eiterpfote', '', NULL, NULL, 23360), -- 5427
(20027, 'deDE', 'Versuchsobjekt: Schwarzfangtarantel', '', NULL, NULL, 23360), -- 20027
(5420, 'deDE', 'Glashautstarrer', '', NULL, NULL, 23360), -- 5420
(20026, 'deDE', 'Versuchsobjekt: Dunkelwasserkrokilisk', '', NULL, NULL, 23360), -- 20026
(19950, 'deDE', 'Bewahrerin der Zeit', '', NULL, NULL, 23360), -- 19950
(7784, 'deDE', 'Leitroboter OOX-17/TN', '', NULL, NULL, 23360), -- 7784
(5450, 'deDE', 'Stecher der Hazzali', '', NULL, NULL, 23360), -- 5450
(39081, 'deDE', 'Kokon der Hazzali', '', NULL, NULL, 23360), -- 39081
(38992, 'deDE', 'Pheromone Bunny A', '', NULL, NULL, 23360), -- 38992
(8205, 'deDE', 'Haarka der Gefräßige', '', NULL, NULL, 23360), -- 8205
(5454, 'deDE', 'Sandhäscher der Hazzali', '', NULL, NULL, 23360), -- 5454
(62258, 'deDE', 'Silithidjunges', '', NULL, 'wildpetcapturable', 23360), -- 62258
(5451, 'deDE', 'Schwärmer der Hazzali', '', NULL, NULL, 23360), -- 5451
(5452, 'deDE', 'Arbeiter der Hazzali', '', NULL, NULL, 23360), -- 5452
(44595, 'deDE', 'Sandgleiter', '', NULL, NULL, 23360), -- 44595
(39061, 'deDE', 'Hackvisage', '', NULL, NULL, 23360), -- 39061
(44710, 'deDE', 'Sandkätzchen', '', NULL, NULL, 23360), -- 44710
(47583, 'deDE', 'Bollwerkverteidiger', '', NULL, NULL, 23420), -- 47583
(61169, 'deDE', 'Schabe', '', NULL, 'wildpetcapturable', 23420), -- 61169
(4076, 'deDE', 'Schabe', '', NULL, NULL, 23420), -- 4076
(48130, 'deDE', 'Skorpidklippenkrabbler', '', NULL, NULL, 23420), -- 48130
(9999, 'deDE', 'Ringo', '', NULL, 'Point', 23420), -- 9999
(9376, 'deDE', 'Sturmflamme', '', NULL, NULL, 23420), -- 9376
(10541, 'deDE', 'Krakles Thermometer', '', NULL, NULL, 23420), -- 10541
(28092, 'deDE', 'Der Etymidian', '', NULL, NULL, 23420), -- 28092
(9118, 'deDE', 'Larion', '', NULL, NULL, 23420), -- 9118
(10583, 'deDE', 'Gryfe', '', 'Flugmeister', NULL, 23420), -- 10583
(38269, 'deDE', 'Zen''Aliri', '', 'Der Zirkel des Cenarius', NULL, 23420), -- 38269
(38488, 'deDE', 'Gastwirt Dreedle', '', 'Marschalls Expedition', NULL, 23420), -- 38488
(10302, 'deDE', 'Krakle', '', 'Marschalls Expedition', NULL, 23420), -- 10302
(9119, 'deDE', 'Muigin', '', NULL, NULL, 23420), -- 9119
(38270, 'deDE', 'Nolen Tacker', '', 'Küchenchef und Hauptmann der Wache', NULL, 23420), -- 38270
(12959, 'deDE', 'Nergal', '', 'Gemischtwaren', NULL, 23420), -- 12959
(10977, 'deDE', 'Quixxil', '', 'Marschalls Expedition', NULL, 23420), -- 10977
(9997, 'deDE', 'Spraggel Frock', '', 'Marschalls Expedition', NULL, 23420), -- 9997
(9271, 'deDE', 'Hol''anyee Marschall', '', 'Marschalls Expedition', NULL, 23420), -- 9271
(9270, 'deDE', 'Williden Marschall', '', 'Marschalls Expedition', NULL, 23420), -- 9270
(3000, 'deDE', 'Gibbert', '', 'Waffenhändler', NULL, 23420), -- 3000
(38354, 'deDE', 'Un''Goro Pit Bunny', '', NULL, NULL, 23420), -- 38354
(9117, 'deDE', 'J.D. Collie', '', 'Marschalls Expedition', NULL, 23420), -- 9117
(61328, 'deDE', 'Feuerkäfer', '', NULL, 'wildpetcapturable', 23420), -- 61328
(6520, 'deDE', 'Versengender Elementar', '', NULL, NULL, 23420), -- 6520
(6521, 'deDE', 'Lebendiges Feuer', '', NULL, NULL, 23420), -- 6521
(62364, 'deDE', 'Ascheneidechse', '', NULL, 'wildpetcapturable', 23420), -- 62364
(50478, 'deDE', 'Ascheneidechse', '', NULL, NULL, 23420), -- 50478
(9699, 'deDE', 'Feuerkäfer', '', NULL, NULL, 23420), -- 9699
(6557, 'deDE', 'Urzeitschlamm', '', NULL, NULL, 23420), -- 6557
(38276, 'deDE', 'Tara', '', NULL, NULL, 23420), -- 38276
(115923, 'deDE', 'Ko''Zan', '', 'Kurier', NULL, 23420), -- 115923
(38561, 'deDE', 'Dramm Flusshorn', '', 'Händler für leichte Rüstungen', NULL, 23420), -- 38561
(38277, 'deDE', 'Doreen', '', NULL, NULL, 23420), -- 38277
(38275, 'deDE', 'Gremix', '', 'Schatzsucher', NULL, 23420), -- 38275
(9998, 'deDE', 'Sappalot', '', 'Marschalls Expedition', NULL, 23420), -- 9998
(9618, 'deDE', 'Karna Wirrbart', '', 'Marschalls Expedition', NULL, 23420), -- 9618
(39175, 'deDE', 'Flizzy Spulspann', '', 'Flugmeister', NULL, 23420), -- 39175
(9683, 'deDE', 'Lar''korwis Weibchen', '', NULL, NULL, 23420), -- 9683
(6498, 'deDE', 'Teufelssaurier', '', NULL, NULL, 23420), -- 6498
(38307, 'deDE', 'Gormashh', '', NULL, NULL, 23420), -- 38307
(15583, 'deDE', 'Urahne Donnerhorn', '', NULL, NULL, 23420), -- 15583
(9163, 'deDE', 'Dimetrodon', '', NULL, NULL, 23420), -- 9163
(9166, 'deDE', 'Pterrordax', '', NULL, NULL, 23420), -- 9166
(38373, 'deDE', 'Pimento', '', 'Maximillians Ross', NULL, 23420), -- 38373
(38237, 'deDE', 'Maximillian von Nordhain', '', 'Fahrender Ritter', NULL, 23420), -- 38237
(9272, 'deDE', 'Funks Nilminer', '', 'Marschalls Expedition', NULL, 23420), -- 9272
(61317, 'deDE', 'Langschwanzmaulwurf', '', NULL, 'wildpetcapturable', 23420), -- 61317
(61384, 'deDE', 'Kakerlake', '', NULL, 'wildpetcapturable', 23420), -- 61384
(6519, 'deDE', 'Teerfürst', '', NULL, NULL, 23420), -- 6519
(6518, 'deDE', 'Teerlauerer', '', NULL, NULL, 23420), -- 6518
(9622, 'deDE', 'U''cha', '', NULL, NULL, 23420), -- 9622
(6516, 'deDE', 'Donnerer von Un''Goro', '', NULL, NULL, 23420), -- 6516
(48972, 'deDE', 'Langschwanzmaulwurf', '', NULL, NULL, 23420), -- 48972
(6514, 'deDE', 'Gorilla von Un''Goro', '', NULL, NULL, 23420), -- 6514
(9623, 'deDE', 'A-Me 01', '', NULL, NULL, 23420), -- 9623
(6513, 'deDE', 'Stampfer von Un''Goro', '', NULL, NULL, 23420), -- 6513
(6527, 'deDE', 'Teerkriecher', '', NULL, NULL, 23420), -- 6527
(6517, 'deDE', 'Teerbestie', '', NULL, NULL, 23420), -- 6517
(98222, 'deDE', 'Ryno Blumenfeld', '', 'Kräuterkundemeister', NULL, 23420), -- 98222
(6500, 'deDE', 'Tyrannoteufelssaurier', '', NULL, NULL, 23420), -- 6500
(38263, 'deDE', 'Ithis Mondwärter', '', 'Der Zirkel des Cenarius', NULL, 23420), -- 38263
(38205, 'deDE', 'Neugeborene Blutblüte', '', NULL, NULL, 23420), -- 38205
(38214, 'deDE', 'Gruppe von halbwüchsigen Blutblüten', '', NULL, NULL, 23420), -- 38214
(6507, 'deDE', 'Ravasaurusjäger', '', NULL, NULL, 23420), -- 6507
(6508, 'deDE', 'Gifthautravasaurus', '', NULL, NULL, 23420), -- 6508
(9162, 'deDE', 'Junges Dimetrodon', '', NULL, NULL, 23420), -- 9162
(34158, 'deDE', 'Junger Gifthautravasaurus', '', NULL, NULL, 23420), -- 34158
(11701, 'deDE', 'Mor''vek', '', 'Ravasaurusausbilder', NULL, 23420), -- 11701
(38274, 'deDE', 'Garl Sturmklaue', '', 'Der Zirkel des Cenarius', NULL, 23420), -- 38274
(38203, 'deDE', 'Bloodpetal Seed Spawner', '', NULL, NULL, 23420), -- 38203
(9619, 'deDE', 'Torwa Pfadfinder', '', NULL, NULL, 23420), -- 9619
(62370, 'deDE', 'Gescheckter Laubfrosch', '', NULL, 'wildpetcapturable', 23420), -- 62370
(45439, 'deDE', 'Kakerlake', '', NULL, NULL, 23420), -- 45439
(6506, 'deDE', 'Ravasaurusläufer', '', NULL, NULL, 23420), -- 6506
(6505, 'deDE', 'Ravasaurus', '', NULL, NULL, 23420), -- 6505
(6554, 'deDE', 'Stecher der Gorishi', '', NULL, NULL, 23420), -- 6554
(62373, 'deDE', 'Seidenmotte', '', NULL, 'wildpetcapturable', 23420), -- 62373
(38305, 'deDE', 'Kolossjunges der Gorishi', '', NULL, NULL, 23420), -- 38305
(6552, 'deDE', 'Arbeiter der Gorishi', '', NULL, NULL, 23420), -- 6552
(6555, 'deDE', 'Tunnelgräber der Gorishi', '', NULL, NULL, 23420), -- 6555
(6553, 'deDE', 'Häscher der Gorishi', '', NULL, NULL, 23420), -- 6553
(6551, 'deDE', 'Wespe der Gorishi', '', NULL, NULL, 23420), -- 6551
(38329, 'deDE', 'Durrin Düsterschippe', '', NULL, NULL, 23420), -- 38329
(38346, 'deDE', 'Königin der Teufelssaurier', '', NULL, NULL, 23420), -- 38346
(6503, 'deDE', 'Stachelstegodon', '', NULL, NULL, 23420), -- 6503
(6509, 'deDE', 'Blutblütenpeitscher', '', NULL, NULL, 23420), -- 6509
(6504, 'deDE', 'Donnerstampferstegodon', '', NULL, NULL, 23420), -- 6504
(62375, 'deDE', 'Dimetrodonjunges', '', NULL, 'wildpetcapturable', 23420), -- 62375
(6502, 'deDE', 'Plattenpanzerstegodon', '', NULL, NULL, 23420), -- 6502
(6501, 'deDE', 'Stegodon', '', NULL, NULL, 23420), -- 6501
(6511, 'deDE', 'Blutblütendrescher', '', NULL, NULL, 23420), -- 6511
(38254, 'deDE', 'Dampfende Furie', '', NULL, NULL, 23420), -- 38254
(9164, 'deDE', 'Altes Dimetrodon', '', NULL, NULL, 23420), -- 9164
(6559, 'deDE', 'Klebriger Schlamm', '', NULL, NULL, 23420), -- 6559
(6510, 'deDE', 'Blutblütenschinder', '', NULL, NULL, 23420), -- 6510
(61318, 'deDE', 'Baumpython', '', NULL, 'wildpetcapturable', 23420), -- 61318
(6512, 'deDE', 'Blutblütenfalle', '', NULL, NULL, 23420), -- 6512
(9167, 'deDE', 'Rasender Pterrordax', '', NULL, NULL, 23420), -- 9167
(61313, 'deDE', 'Papagei', '', NULL, 'wildpetcapturable', 23420), -- 61313
(49734, 'deDE', 'Gescheckter Laubfrosch', '', NULL, NULL, 23420), -- 49734
(9600, 'deDE', 'Papagei', '', NULL, NULL, 23420), -- 9600
(6560, 'deDE', 'Steinwächter', '', NULL, NULL, 23420), -- 6560
(49722, 'deDE', 'Baumpython', '', NULL, NULL, 23420), -- 49722
(48935, 'deDE', 'Berghase', '', NULL, NULL, 23420), -- 48935
(7554, 'deDE', 'Schneeeule', '', NULL, 'wildpetcapturable', 23420), -- 7554
(7428, 'deDE', 'Frosthagelriese', '', NULL, NULL, 23420), -- 7428
(7446, 'deDE', 'Tollwütiger Splitterzahn', '', NULL, NULL, 23420), -- 7446
(41861, 'deDE', 'Fayran Elthas', '', 'Flugmeister', NULL, 23420), -- 41861
(106250, 'deDE', 'Bewahrer Remulos', '', NULL, NULL, 23420), -- 106250
(43410, 'deDE', 'Toron Felshuf', '', 'Schmiedekunstbedarf', NULL, 23420), -- 43410
(40833, 'deDE', 'Tiala Weißmähne', '', NULL, NULL, 23420), -- 40833
(40254, 'deDE', 'Verletzter Druide', '', NULL, NULL, 23420), -- 40254
(55227, 'deDE', 'Urahnin Immerschatten', '', NULL, NULL, 23420), -- 55227
(40278, 'deDE', 'Tholo Weißhuf', '', NULL, NULL, 23420), -- 40278
(39925, 'deDE', 'Anren Schattensucher', '', NULL, NULL, 23420), -- 39925
(50314, 'deDE', 'Versorgerin Wolkenweiß', '', 'Rüstmeisterin der Wächter des Hyjal', NULL, 23420), -- 50314
(43411, 'deDE', 'Lenedil Mondschwinge', '', 'Gemischtwaren', NULL, 23420), -- 43411
(43408, 'deDE', 'Aili Grünweide', '', 'Stallmeisterin', NULL, 23420), -- 43408
(40289, 'deDE', 'Ysera', '', NULL, NULL, 23420), -- 40289
(71304, 'deDE', 'Iris Mondtraum', '', 'Rüstmeisterin', NULL, 23420), -- 71304
(40843, 'deDE', 'Sebelia', '', 'Gastwirtin', NULL, 23420), -- 40843
(43427, 'deDE', 'Wütender Wächter des Hyjal', '', NULL, NULL, 23420), -- 43427
(66819, 'deDE', 'Brok', '', 'Meistertierzähmer', NULL, 23420), -- 66819
(66808, 'deDE', 'Kali', '', NULL, NULL, 23420), -- 66808
(66807, 'deDE', 'Ascheschwanz', '', NULL, NULL, 23420), -- 66807
(66806, 'deDE', 'Kautero', '', NULL, NULL, 23420), -- 66806
(106303, 'deDE', 'Generic Bunny', '', NULL, NULL, 23420), -- 106303
(47264, 'deDE', 'Flüchtende Hirschkuh', '', NULL, NULL, 23420), -- 47264
(47263, 'deDE', 'Flüchtender Hirsch', '', NULL, NULL, 23420), -- 47263
(59113, 'deDE', 'Generic Bunny', '', NULL, NULL, 23420), -- 59113
(40229, 'deDE', 'Brühender Felselementar', '', NULL, NULL, 23420), -- 40229
(39921, 'deDE', 'Feendrache', '', NULL, NULL, 23420), -- 39921
(62888, 'deDE', 'Irrwisch von Nordrassil', '', NULL, 'wildpetcapturable', 23420), -- 62888
(40140, 'deDE', 'Erzdruide Fandral Hirschhaupt', '', NULL, NULL, 23420), -- 40140
(40139, 'deDE', 'Hauptmann Saynna Sturmläufer', '', NULL, NULL, 23420), -- 40139
(40882, 'deDE', 'Zwielichtspitzel', '', NULL, NULL, 23420), -- 40882
(62884, 'deDE', 'Grottenwühlmaus', '', NULL, 'wildpetcapturable', 23420), -- 62884
(20725, 'deDE', 'Fledermaus', '', NULL, NULL, 23420), -- 20725
(38951, 'deDE', 'Zwielichtassassine', '', NULL, NULL, 23420), -- 38951
(38934, 'deDE', 'Tiefenwächter der Grabhügel', '', NULL, NULL, 23420), -- 38934
(39919, 'deDE', 'Generic Trigger LAB - Multiphase (Gigantic AOI Not Sessile)', '', NULL, NULL, 23420), -- 39919
(36286, 'deDE', 'Generic Trigger LAB - Multiphase (Gigantic AOI)', '', NULL, NULL, 23420), -- 36286
(39869, 'deDE', 'Windsprecherin Tamila', '', NULL, NULL, 23420), -- 39869
(39857, 'deDE', 'Malfurion Sturmgrimm', '', NULL, NULL, 23420), -- 39857
(49445, 'deDE', 'Raz Schädelberster', '', 'Finkles Leibwächter', NULL, 23420), -- 49445
(49444, 'deDE', 'Finkle Einhorn', '', NULL, NULL, 23420), -- 49444
(40149, 'deDE', 'Telessra', '', NULL, NULL, 23420), -- 40149
(40147, 'deDE', 'Baron Geddon', '', NULL, NULL, 23420), -- 40147
(38902, 'deDE', 'Eisenborkenurtum', '', NULL, NULL, 23420), -- 38902
(40150, 'deDE', 'Erzdruide des Hyjal', '', NULL, NULL, 23420), -- 40150
(40148, 'deDE', 'Galrond von der Klaue', '', NULL, NULL, 23420), -- 40148
(38952, 'deDE', 'Druide von Nordrassil', '', NULL, NULL, 23420), -- 38952
(38896, 'deDE', 'Lohengebundener Elementar', '', NULL, NULL, 23420), -- 38896
(40096, 'deDE', 'Späherin Larandia', '', NULL, NULL, 23420), -- 40096
(40123, 'deDE', 'Zwielichtvorarbeiter', '', NULL, NULL, 23420), -- 40123
(62118, 'deDE', 'Zwielichtkäfer', '', NULL, 'wildpetcapturable', 23420), -- 62118
(38926, 'deDE', 'Zwielichtflammenrufer', '', NULL, NULL, 23420), -- 38926
(39429, 'deDE', 'Oomla Weißhorn', '', NULL, NULL, 23420), -- 39429
(39427, 'deDE', 'Jadi Falaryn', '', NULL, NULL, 23420), -- 39427
(38913, 'deDE', 'Zwielichtbezwinger', '', NULL, NULL, 23420), -- 38913
(39437, 'deDE', 'Zwielichtjäger', '', NULL, NULL, 23420), -- 39437
(41308, 'deDE', 'Aviana', '', NULL, NULL, 23420), -- 41308
(50080, 'deDE', 'Choluna', '', 'Druidin der Kralle', NULL, 23420), -- 50080
(50070, 'deDE', 'Jandunel Schilfwind', '', 'Gemischtwaren', NULL, 23420), -- 50070
(42660, 'deDE', 'Hyjaldunkelfalke', '', NULL, NULL, 23420), -- 42660
(42657, 'deDE', 'Hyjaladler', '', NULL, NULL, 23420), -- 42657
(50082, 'deDE', 'Himmelsfürst Omnuron', '', 'Druide der Kralle', NULL, 23420), -- 50082
(54313, 'deDE', 'Thrall', '', NULL, NULL, 23420), -- 54313
(50079, 'deDE', 'Borun Donnerhimmel', '', 'Druide der Kralle', NULL, 23420), -- 50079
(50081, 'deDE', 'Morthis Flügelraunen', '', 'Druide der Kralle', NULL, 23420), -- 50081
(50068, 'deDE', 'Isara Flussgänger', '', 'Gastwirtin', NULL, 23420), -- 50068
(50071, 'deDE', 'Nunaha Grashufe', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 50071
(41006, 'deDE', 'Thisalee Krähe', '', 'Druidin der Kralle', NULL, 23420), -- 41006
(50069, 'deDE', 'Oltarin Grauwolke', '', 'Stallmeister', NULL, 23420), -- 50069
(50084, 'deDE', 'Dinorae Flinkfeder', '', 'Flugmeisterin', NULL, 23420), -- 50084
(50083, 'deDE', 'Druide der Kralle', '', NULL, NULL, 23420), -- 50083
(42659, 'deDE', 'Hyjalkreischer', '', NULL, NULL, 23420), -- 42659
(42663, 'deDE', 'Hyjaleule', '', NULL, NULL, 23420), -- 42663
(42658, 'deDE', 'Hyjalroc', '', NULL, NULL, 23420), -- 42658
(42664, 'deDE', 'Jägerin des Hyjal', '', NULL, NULL, 23420), -- 42664
(40845, 'deDE', 'Schmiedemeister Pyrendius', '', NULL, NULL, 23420), -- 40845
(40844, 'deDE', 'Aschenmaul', '', 'Der Portalmeister', NULL, 23420), -- 40844
(40834, 'deDE', 'Jordan Olafson', '', NULL, NULL, 23420), -- 40834
(40837, 'deDE', 'Yargra Schwarznarbe', '', NULL, NULL, 23420), -- 40837
(39846, 'deDE', 'Leyden Kupferkleist', '', NULL, NULL, 23420), -- 39846
(40841, 'deDE', 'Sengender Wächter', '', NULL, NULL, 23420), -- 40841
(40838, 'deDE', 'Arbeiter der Dunkeleisenzwerge', '', NULL, NULL, 23420), -- 40838
(39600, 'deDE', 'Twilight Proveditor Spawn Controller Bunny 02', '', NULL, NULL, 23420), -- 39600
(39438, 'deDE', 'Zwielichtsklaventreiber', '', NULL, NULL, 23420), -- 39438
(39431, 'deDE', 'Zwielichtsklave', '', NULL, NULL, 23420), -- 39431
(50419, 'deDE', 'Grottenwühlmaus', '', NULL, NULL, 23420), -- 50419
(40868, 'deDE', 'Twilight Anvil Effects Bunny', '', NULL, NULL, 23420), -- 40868
(39737, 'deDE', 'Tiegel der Erde', '', NULL, NULL, 23420), -- 39737
(39736, 'deDE', 'Tiegel der Luft', '', NULL, NULL, 23420), -- 39736
(39730, 'deDE', 'Tiegel des Feuers', '', NULL, NULL, 23420), -- 39730
(39738, 'deDE', 'Tiegel des Wassers', '', NULL, NULL, 23420), -- 39738
(39789, 'deDE', 'Kristoff''s Chain Vehicle', '', NULL, NULL, 23420), -- 39789
(39640, 'deDE', 'Kristoff Manheim', '', NULL, NULL, 23420), -- 39640
(39646, 'deDE', 'Gar''gol', '', NULL, NULL, 23420), -- 39646
(62887, 'deDE', 'Totenkopfkakerlake', '', NULL, 'wildpetcapturable', 23420), -- 62887
(39643, 'deDE', 'Schattenwirker des Unterschlupfs', '', NULL, NULL, 23420), -- 39643
(39436, 'deDE', 'Zwielichtordner', '', NULL, NULL, 23420), -- 39436
(39642, 'deDE', 'Schläger des Unterschlupfs', '', NULL, NULL, 23420), -- 39642
(43548, 'deDE', 'Berin Connad', '', 'Schmiedekunstbedarf', NULL, 23420), -- 43548
(39432, 'deDE', 'Takrik Wutheuler', '', NULL, NULL, 23420), -- 39432
(39435, 'deDE', 'Royce Abendwisper', '', NULL, NULL, 23420), -- 39435
(39433, 'deDE', 'Ian Duran', '', NULL, NULL, 23420), -- 39433
(39434, 'deDE', 'Rio Duran', '', NULL, NULL, 23420), -- 39434
(39644, 'deDE', 'Zwielichtbediensteter', '', NULL, NULL, 23420), -- 39644
(43547, 'deDE', 'Mirala Kitzsänger', '', 'Gemischtwaren', NULL, 23420), -- 43547
(39637, 'deDE', 'Verteidiger von Goldrinn', '', NULL, NULL, 23420), -- 39637
(39588, 'deDE', 'Hyjalhirsch', '', NULL, NULL, 23420), -- 39588
(39859, 'deDE', 'Dave''s Industrial Light and Magic Bunny (Large)(Sessile)', '', NULL, NULL, 23420), -- 39859
(39844, 'deDE', 'Heulender Rissbewohner', '', NULL, NULL, 23420), -- 39844
(39843, 'deDE', 'Zwielichtsturmrufer', '', NULL, NULL, 23420), -- 39843
(53781, 'deDE', 'Tomo', '', 'Gemischtwaren', NULL, 23420), -- 53781
(53783, 'deDE', 'Elizil Wintermotte', '', 'Flugmeisterin', NULL, 23420), -- 53783
(53780, 'deDE', 'Limiah Weißzweig', '', 'Stallmeisterin', NULL, 23420), -- 53780
(53782, 'deDE', 'Jalin Seetief', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 53782
(53779, 'deDE', 'Salirn Mondbär', '', 'Gastwirt', NULL, 23420), -- 53779
(52688, 'deDE', 'Hyjalbärenjunges', '', NULL, 'LootAll', 23420), -- 52688
(52676, 'deDE', 'Kletterbaum', '', NULL, 'vehichleCursor', 23420), -- 52676
(52682, 'deDE', 'Baumkrone', '', NULL, NULL, 23420), -- 52682
(53841, 'deDE', 'Zen''tabra', '', NULL, NULL, 23420), -- 53841
(53842, 'deDE', 'Mardant Starkeiche', '', NULL, NULL, 23420), -- 53842
(53840, 'deDE', 'Gart Nebelläufer', '', NULL, NULL, 23420), -- 53840
(44775, 'deDE', 'Generic Bunny - PRK (Large AOI)', '', NULL, NULL, 23420), -- 44775
(52816, 'deDE', 'Verkohlter Eindringling', '', NULL, NULL, 23420), -- 52816
(52594, 'deDE', 'Goldschwingenfalke', '', NULL, NULL, 23420), -- 52594
(54362, 'deDE', 'Vernarbter Akolyth', '', 'Druide der Flamme', NULL, 23420), -- 54362
(52795, 'deDE', 'Schwefelhund', '', NULL, NULL, 23420), -- 52795
(52595, 'deDE', 'Gebirgssingvogel', '', NULL, NULL, 23420), -- 52595
(52596, 'deDE', 'Waldeule', '', NULL, NULL, 23420), -- 52596
(53844, 'deDE', 'Celestine Erntedank', '', NULL, NULL, 23420), -- 53844
(40757, 'deDE', 'Numa Himmelsklaue', '', NULL, NULL, 23420), -- 40757
(40650, 'deDE', 'Zwielichtfeuervogel', '', NULL, NULL, 23420), -- 40650
(40660, 'deDE', 'Zwielichttjoster', '', NULL, NULL, 23420), -- 40660
(40708, 'deDE', 'Fallender Fels', '', NULL, NULL, 23420), -- 40708
(40578, 'deDE', 'Farden Klauenwürger', '', NULL, NULL, 23420), -- 40578
(40723, 'deDE', 'Avianas Wächter', '', NULL, 'vehichleCursor', 23420), -- 40723
(40720, 'deDE', 'Avianas Wächter', '', NULL, 'vehichleCursor', 23420), -- 40720
(41200, 'deDE', 'Generic Bunny - PRK', '', NULL, NULL, 23420), -- 41200
(46464, 'deDE', 'Generic Bunny - PRK - Extra-Small', '', NULL, NULL, 23420), -- 46464
(52425, 'deDE', 'Tooga', '', NULL, NULL, 23420), -- 52425
(52906, 'deDE', 'Ahnenzweig', '', 'Urtum der Lehren', NULL, 23420), -- 52906
(53075, 'deDE', 'Inoho Starkfell', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 53075
(52844, 'deDE', 'Hauptmann Saynna Sturmläufer', '', NULL, NULL, 23420), -- 52844
(52670, 'deDE', 'Verteidiger von Malorne', '', NULL, NULL, 23420), -- 52670
(52669, 'deDE', 'Matookla', '', NULL, NULL, 23420), -- 52669
(55224, 'deDE', 'Urahnin Windsang', '', NULL, NULL, 23420), -- 55224
(54393, 'deDE', 'Ranela Federtal', '', 'Flugmeisterin', NULL, 23420), -- 54393
(52177, 'deDE', 'Abkömmling von Tortolla', '', NULL, 'Pickup', 23420), -- 52177
(52547, 'deDE', 'Uralter Beschützer des Hyjal', '', NULL, NULL, 23420), -- 52547
(53073, 'deDE', 'Hauptmann Soren Mondsturz', '', NULL, NULL, 23420), -- 53073
(52986, 'deDE', 'Dorda''en Nachtweber', '', 'Hamuuls Knecht', NULL, 23420), -- 52986
(52937, 'deDE', 'Druidischer Knecht', '', NULL, NULL, 23420), -- 52937
(52932, 'deDE', 'Erzdruide Hamuul Runentotem', '', NULL, NULL, 23420), -- 52932
(52671, 'deDE', 'Mylune', '', NULL, NULL, 23420), -- 52671
(52219, 'deDE', 'Flammenschrecken', '', NULL, NULL, 23420), -- 52219
(53076, 'deDE', 'Nenduil Auenschatten', '', 'Gemischtwaren', NULL, 23420), -- 53076
(54320, 'deDE', 'Ban''thalos', '', NULL, NULL, 23420), -- 54320
(53267, 'deDE', 'Andrazar', '', 'Fürst der Asche', NULL, 23420), -- 53267
(52289, 'deDE', 'Feuriges Ungetüm', '', NULL, NULL, 23420), -- 52289
(52300, 'deDE', 'Wutentbrannter Pyrolord', '', NULL, NULL, 23420), -- 52300
(52195, 'deDE', 'Wütendes kleines Eichhörnchen', '', NULL, NULL, 23420), -- 52195
(52176, 'deDE', 'Geist von Malorne', '', NULL, NULL, 23420), -- 52176
(54251, 'deDE', 'Feuerball', '', NULL, NULL, 23420), -- 54251
(52791, 'deDE', 'Verkohlter der Flammenschürer', '', NULL, NULL, 23420), -- 52791
(52794, 'deDE', 'Schwefelzerstörer', '', NULL, NULL, 23420), -- 52794
(53805, 'deDE', 'Ystelle Eichensang', '', NULL, NULL, 23420), -- 53805
(53823, 'deDE', 'Wächter des Hyjal', '', NULL, NULL, 23420), -- 53823
(41507, 'deDE', 'Niden', '', NULL, NULL, 23420), -- 41507
(38915, 'deDE', 'Wächter des Hyjal', '', NULL, NULL, 23420), -- 38915
(36198, 'deDE', 'Generic Trigger LAB - Multiphase (Ground)', '', NULL, NULL, 23420), -- 36198
(62885, 'deDE', 'Aasratte', '', NULL, 'wildpetcapturable', 23420), -- 62885
(40563, 'deDE', 'Zwielichtvollstrecker', '', NULL, 'Speak', 23420), -- 40563
(41504, 'deDE', 'Tortolla', '', NULL, NULL, 23420), -- 41504
(41497, 'deDE', 'Logram', '', NULL, NULL, 23420), -- 41497
(41492, 'deDE', 'Hauptmann Eisenstamm', '', NULL, NULL, 23420), -- 41492
(41509, 'deDE', 'Eisenwaldwächter', '', NULL, NULL, 23420), -- 41509
(41499, 'deDE', 'Verschollener Wächter', '', NULL, NULL, 23420), -- 41499
(41614, 'deDE', 'Nemesis', '', 'Der Usurpator', NULL, 23420), -- 41614
(41557, 'deDE', 'Abkömmling von Tortolla', '', NULL, NULL, 23420), -- 41557
(41498, 'deDE', 'Garunda Bergkuppe', '', NULL, NULL, 23420), -- 41498
(41563, 'deDE', 'Meister der Schattenflamme', '', NULL, NULL, 23420), -- 41563
(41565, 'deDE', 'Geschmolzener Peiniger', '', NULL, NULL, 23420), -- 41565
(41502, 'deDE', 'Zwielichtfeldhauptmann', '', NULL, NULL, 23420), -- 41502
(41500, 'deDE', 'Sengender Zwielichtfürst', '', NULL, NULL, 23420), -- 41500
(40564, 'deDE', 'Feuriger Instrukteur', '', NULL, 'Speak', 23420), -- 40564
(40562, 'deDE', 'Zwielichtinitiand', '', NULL, 'Speak', 23420), -- 40562
(40573, 'deDE', 'Zwielichtsturmschürer', '', NULL, NULL, 23420), -- 40573
(40575, 'deDE', 'Zwielichtsturmschürer', '', NULL, NULL, 23420), -- 40575
(40780, 'deDE', 'Smaragddrache', '', NULL, NULL, 23420), -- 40780
(41026, 'deDE', 'Präsenz von Cenarius', '', NULL, NULL, 23420), -- 41026
(40928, 'deDE', 'Ysera', '', NULL, NULL, 23420), -- 40928
(40772, 'deDE', 'Kommandant Jarod Schattensang', '', NULL, NULL, 23420), -- 40772
(40934, 'deDE', 'Smaragddrache', '', NULL, 'vehichleCursor', 23420), -- 40934
(40931, 'deDE', 'Generic Trigger LAB 1.80', '', NULL, NULL, 23420), -- 40931
(40687, 'deDE', 'Junger Zwielichtdrache', '', NULL, NULL, 23420), -- 40687
(50485, 'deDE', 'Aasratte', '', NULL, NULL, 23420), -- 50485
(40463, 'deDE', 'Zwielichtunterwerfer', '', NULL, NULL, 23420), -- 40463
(40709, 'deDE', 'Flammenaszendent', '', NULL, NULL, 23420), -- 40709
(40814, 'deDE', 'Azralon der Torwächter', '', NULL, NULL, 23420), -- 40814
(65223, 'deDE', 'Totenkopfkakerlake', '', NULL, NULL, 23420), -- 65223
(40755, 'deDE', 'Abgesandter der Flamme', '', NULL, NULL, 23420), -- 40755
(40767, 'deDE', 'Zwielichtanhänger', '', NULL, NULL, 23420), -- 40767
(40713, 'deDE', 'Zwielichtaugur', '', NULL, NULL, 23420), -- 40713
(62886, 'deDE', 'Feuerfeste Kakerlake', '', NULL, 'wildpetcapturable', 23420), -- 62886
(34398, 'deDE', 'Alptraumwächter', '', NULL, NULL, 23420), -- 34398
(33166, 'deDE', 'Thessera', '', NULL, NULL, 23420), -- 33166
(34399, 'deDE', 'Alptraumsaat', '', NULL, NULL, 23420), -- 34399
(33057, 'deDE', 'Zwielichtzelot', '', NULL, NULL, 23420), -- 33057
(3823, 'deDE', 'Geisterpfotenläufer', '', NULL, NULL, 23420), -- 3823
(32996, 'deDE', 'Sharax der Schänder', '', NULL, NULL, 23420), -- 32996
(32997, 'deDE', 'Flinkfuß', '', NULL, NULL, 23420), -- 32997
(34334, 'deDE', 'Bubble Bunny', '', NULL, NULL, 23420), -- 34334
(33043, 'deDE', 'Wahnsinniger der Schwarzfelle', '', NULL, NULL, 23420), -- 33043
(3816, 'deDE', 'Wildbock', '', NULL, NULL, 23420), -- 3816
(33044, 'deDE', 'Verderbter der Schwarzfelle', '', NULL, NULL, 23420), -- 33044
(32968, 'deDE', 'Gren Fetzfell', '', NULL, NULL, 23420), -- 32968
(43425, 'deDE', 'Syleath Waldlichtung', '', 'Gemischtwaren', NULL, 23420), -- 43425
(34404, 'deDE', 'Larien', '', NULL, NULL, 23420), -- 34404
(34403, 'deDE', 'Felros', '', NULL, NULL, 23420), -- 34403
(34402, 'deDE', 'Balren von der Klaue', '', NULL, NULL, 23420), -- 34402
(33250, 'deDE', 'Foriel Rundblatt', '', NULL, NULL, 23420), -- 33250
(33072, 'deDE', 'Onu', '', 'Urtum der Lehren', NULL, 23420), -- 33072
(34392, 'deDE', 'Orseus', '', NULL, NULL, 23420), -- 34392
(33253, 'deDE', 'Delanea', '', 'Flugmeisterin', NULL, 23420), -- 33253
(34301, 'deDE', 'Kathrena Winterleuchten', '', NULL, NULL, 23420), -- 34301
(33058, 'deDE', 'Bezauberter Erdelementar', '', NULL, NULL, 23420), -- 33058
(2192, 'deDE', 'Feuerrufer Radison', '', NULL, NULL, 23420), -- 2192
(34326, 'deDE', 'Unheilsbote Trevellion', '', NULL, NULL, 23420), -- 34326
(34427, 'deDE', 'Thalya die Leerruferin', '', NULL, NULL, 23420), -- 34427
(33112, 'deDE', 'Selenn', '', 'Urtum des Krieges', NULL, 23420), -- 33112
(2184, 'deDE', 'Lady Mondblick', '', NULL, NULL, 23420), -- 2184
(34299, 'deDE', 'Überreste eines Erdelementars', '', NULL, NULL, 23420), -- 34299
(34316, 'deDE', 'Zwielichtportal', '', NULL, NULL, 23420), -- 34316
(33091, 'deDE', 'Malfurion Sturmgrimm', '', NULL, NULL, 23420), -- 33091
(34385, 'deDE', 'Horoo der Flammenbewahrer', '', NULL, NULL, 23420), -- 34385
(33119, 'deDE', 'Aroom', '', NULL, NULL, 23420), -- 33119
(33107, 'deDE', 'Priesterin Alinya', '', NULL, NULL, 23420), -- 33107
(3694, 'deDE', 'Schildwache Selarin', '', NULL, NULL, 23420), -- 3694
(32987, 'deDE', 'Corvine Mondaufgang', '', 'Druide der Kralle', NULL, 23420), -- 32987
(4194, 'deDE', 'Ullanna', '', 'Handwerkswaren', NULL, 23420), -- 4194
(4190, 'deDE', 'Kyndri', '', 'Bäckerin', NULL, 23420), -- 4190
(33231, 'deDE', 'Elisa Stahlhand', '', 'Schmiedekunstbedarf', NULL, 23420), -- 33231
(34304, 'deDE', 'Wildekingeist', '', NULL, NULL, 23420), -- 34304
(33083, 'deDE', 'Wütender Erdelementar', '', NULL, NULL, 23420), -- 33083
(34302, 'deDE', 'Aufgezehrter Distelbär', '', NULL, NULL, 23420), -- 34302
(32967, 'deDE', 'Ältester Brolg', '', NULL, NULL, 23420), -- 32967
(34406, 'deDE', 'Vorarbeiter Balsoth', '', NULL, NULL, 23420), -- 34406
(34413, 'deDE', 'Gesichtsloser', '', NULL, NULL, 23420), -- 34413
(34405, 'deDE', 'Zwielichtarbeiter', '', NULL, NULL, 23420), -- 34405
(34417, 'deDE', 'Junger ergrauter Distelbär', '', NULL, NULL, 23420), -- 34417
(34396, 'deDE', 'Weißschwanzhirschkuh', '', NULL, NULL, 23420), -- 34396
(62250, 'deDE', 'Dunkelküstenjunges', '', NULL, 'wildpetcapturable', 23420), -- 62250
(2165, 'deDE', 'Ergrauter Distelbär', '', NULL, NULL, 23420), -- 2165
(34318, 'deDE', 'Weißschwanzhirsch', '', NULL, NULL, 23420), -- 34318
(34306, 'deDE', 'Dunkelküstenirrwisch', '', NULL, 'LootAll', 23420), -- 34306
(2071, 'deDE', 'Mondpirschermatriarchin', '', NULL, NULL, 23420), -- 2071
(33117, 'deDE', 'Ältester Braunpfote', '', NULL, NULL, 23420), -- 33117
(33084, 'deDE', 'Furbolg der Schwarzfelle', '', NULL, NULL, 23420), -- 33084
(34282, 'deDE', 'Zwielichtreiter', '', NULL, NULL, 23420), -- 34282
(34293, 'deDE', 'Zwielichtreiter', '', NULL, NULL, 23420), -- 34293
(2321, 'deDE', 'Waldschreiterjunges', '', NULL, NULL, 23420), -- 2321
(2172, 'deDE', 'Schreitergelegemutter', '', NULL, NULL, 23420), -- 2172
(2237, 'deDE', 'Mondpirscherpatriarch', '', NULL, NULL, 23420), -- 2237
(34342, 'deDE', 'Juniorarchäologe Ferd', '', 'Forscherliga', NULL, 23420), -- 34342
(34340, 'deDE', 'Archäologe Groff', '', 'Forscherliga', NULL, 23420), -- 34340
(34343, 'deDE', 'Ausgrabungsleiter Wirrbart', '', 'Forscherliga', NULL, 23420), -- 34343
(34339, 'deDE', 'Flüchtling der Grauflossen', '', NULL, NULL, 23420), -- 34339
(2207, 'deDE', 'Orakel der Grauflossen', '', NULL, NULL, 23420), -- 2207
(34423, 'deDE', 'Kriegsfürst Zornrücken', '', NULL, NULL, 23420), -- 34423
(34414, 'deDE', 'Sirene der Dunkelschuppen', '', NULL, NULL, 23420), -- 34414
(24042, 'deDE', 'Generic Trigger LAB - OLD', '', NULL, NULL, 23420), -- 24042
(34415, 'deDE', 'Priesterin der Dunkelschuppen', '', NULL, NULL, 23420), -- 34415
(33079, 'deDE', 'Myrmidone der Dunkelschuppen', '', NULL, NULL, 23420), -- 33079
(34356, 'deDE', 'Buried Treasure Bunny', '', NULL, NULL, 23420), -- 34356
(34350, 'deDE', 'Drohfisch', '', NULL, NULL, 23420), -- 34350
(2206, 'deDE', 'Jäger der Grauflossen', '', NULL, NULL, 23420), -- 2206
(2233, 'deDE', 'Verkrusteter Gezeitenkriecher', '', NULL, NULL, 23420), -- 2233
(34351, 'deDE', 'Scheusalfisch', '', NULL, NULL, 23420), -- 34351
(32999, 'deDE', 'Unbändiger Feuerelementar', '', NULL, NULL, 23420), -- 32999
(33035, 'deDE', 'Taldan', '', 'Getränkeverkäufer', NULL, 23420), -- 33035
(6887, 'deDE', 'Yalda', '', NULL, NULL, 23420), -- 6887
(33232, 'deDE', 'Archäologin Hollee', '', 'Forscherliga', NULL, 23420), -- 33232
(33039, 'deDE', 'Wütender Hippogryph', '', NULL, NULL, 23420), -- 33039
(33037, 'deDE', 'Caylais Mondfeder', '', 'Hippogryphenmeisterin', NULL, 23420), -- 33037
(32989, 'deDE', 'Windmeister Tzu-Tzu', '', NULL, NULL, 23420), -- 32989
(33047, 'deDE', 'Aetherion Vortex Bunny', '', NULL, NULL, 23420), -- 33047
(33045, 'deDE', 'ELM General Purpose Bunny Large (scale x5)', '', NULL, NULL, 23420), -- 33045
(33041, 'deDE', 'Aetherion', '', NULL, NULL, 23420), -- 33041
(33111, 'deDE', 'ELM General Purpose Bunny (scale x3) Large', '', NULL, NULL, 23420), -- 33111
(33040, 'deDE', 'Transformierter rasender Zyklon', '', NULL, NULL, 23420), -- 33040
(32986, 'deDE', 'Frenzied Cyclone Bracers Kill Credit Bunny', '', NULL, NULL, 23420), -- 32986
(33106, 'deDE', 'Flüchtling aus Auberdine', '', NULL, NULL, 23420), -- 33106
(33053, 'deDE', 'Grimmklaue', '', NULL, NULL, 23420), -- 33053
(32990, 'deDE', 'Himmelsfürst Braax', '', NULL, NULL, 23420), -- 32990
(33001, 'deDE', 'Thundris Windwirker', '', NULL, NULL, 23420), -- 33001
(33033, 'deDE', 'Schildwache Elissa Sternenhauch', '', NULL, NULL, 23420), -- 33033
(32988, 'deDE', 'Wolkenzähmerin Wildmähne', '', NULL, NULL, 23420), -- 32988
(32985, 'deDE', 'Rasender Zyklon', '', NULL, NULL, 23420), -- 32985
(34315, 'deDE', 'Marodierender Wilderer', '', NULL, NULL, 23420), -- 34315
(33126, 'deDE', 'Seraphine', '', NULL, NULL, 23420), -- 33126
(33048, 'deDE', 'Bewahrer Karithus', '', NULL, NULL, 23420), -- 33048
(2175, 'deDE', 'Schattenklaue', '', NULL, NULL, 23420), -- 2175
(33177, 'deDE', 'Arya Herbstlicht', '', NULL, NULL, 23420), -- 33177
(32932, 'deDE', 'Mondpriesterin Tharill', '', NULL, NULL, 23420), -- 32932
(3841, 'deDE', 'Teldira Mondfeder', '', 'Hippogryphenmeisterin', NULL, 23420), -- 3841
(32974, 'deDE', 'Laird', '', NULL, NULL, 23420), -- 32974
(32973, 'deDE', 'Dentaria Silbertal', '', 'Mondpriesterin', NULL, 23420), -- 32973
(32972, 'deDE', 'Serendia Eichwisper', '', 'Mondpriesterin', NULL, 23420), -- 32972
(43431, 'deDE', 'Periale', '', 'Bergbaulehrerin', NULL, 23420), -- 43431
(43420, 'deDE', 'Gastwirtin Kyteran', '', 'Gastwirtin', NULL, 23420), -- 43420
(32978, 'deDE', 'Tharnariun Baumwahrer', '', NULL, NULL, 23420), -- 32978
(32977, 'deDE', 'Hexknall Kurbeldreh', '', NULL, NULL, 23420), -- 32977
(32971, 'deDE', 'Waldläuferin Glynda Nal''Shea', '', NULL, NULL, 23420), -- 32971
(51997, 'deDE', 'Stephanie Krutsick', '', 'Archäologielehrerin', NULL, 23420), -- 51997
(43436, 'deDE', 'Ceriale Abendwisper', '', 'Tuchmacherin', NULL, 23420), -- 43436
(43428, 'deDE', 'Faeyrin Weidenmond', '', 'Schneiderlehrer', NULL, 23420), -- 43428
(11037, 'deDE', 'Jenna Lemkenilli', '', 'Ingenieurskunstlehrerin', NULL, 23420), -- 11037
(49923, 'deDE', 'Schildwache Mondschwinge', '', 'Kriegerlehrerin', NULL, 23420), -- 49923
(43439, 'deDE', 'Nyrisse', '', 'Lederrüstungen', NULL, 23420), -- 43439
(43429, 'deDE', 'Taryel Feuerschlag', '', 'Schmiedekunstlehrer', NULL, 23420), -- 43429
(43424, 'deDE', 'Ayriala', '', 'Gemischtwaren', NULL, 23420), -- 43424
(32979, 'deDE', 'Gorbold Stahlhand', '', 'Handwerkswaren', NULL, 23420), -- 32979
(63084, 'deDE', 'Poe', '', 'Kampfhaustier', NULL, 23420), -- 63084
(63083, 'deDE', 'Will Larsons', '', 'Kampfhaustiertrainer', NULL, 23420), -- 63083
(49968, 'deDE', 'Lareth Beld', '', 'Magierlehrer', NULL, 23420), -- 49968
(49963, 'deDE', 'Laera Dubois', '', 'Hexenmeisterlehrerin', NULL, 23420), -- 49963
(49940, 'deDE', 'Irlara Morgenglanz', '', 'Priesterlehrerin', NULL, 23420), -- 49940
(49939, 'deDE', 'Kenral Nachtwind', '', 'Schurkenlehrer', NULL, 23420), -- 49939
(49927, 'deDE', 'Lanla Bogenblatt', '', 'Jägerlehrerin', NULL, 23420), -- 49927
(32912, 'deDE', 'Schildwache Lendra', '', NULL, NULL, 23420), -- 32912
(10085, 'deDE', 'Jaelysia', '', 'Stallmeisterin', NULL, 23420), -- 10085
(4187, 'deDE', 'Harlon Dornenwacht', '', 'Rüstungs- & Schildschmied', NULL, 23420), -- 4187
(49942, 'deDE', 'Dular', '', 'Druidenlehrer', NULL, 23420), -- 49942
(15601, 'deDE', 'Urahnin Sternenweber', '', NULL, NULL, 23420), -- 15601
(33864, 'deDE', 'Übler Schrecken', '', NULL, NULL, 23420), -- 33864
(32928, 'deDE', 'Widerwärtige Gischt', '', NULL, NULL, 23420), -- 32928
(33127, 'deDE', 'Mondpirscher', '', NULL, 'LootAll', 23420), -- 33127
(32899, 'deDE', 'Hochkultist Azerynn', '', NULL, NULL, 23420), -- 32899
(32890, 'deDE', 'Wütender Gezeitengeist', '', NULL, NULL, 23420), -- 32890
(32888, 'deDE', 'Fanatiker des Schattenhammers', '', NULL, NULL, 23420), -- 32888
(33296, 'deDE', 'Ritual Bond Bunny', '', NULL, NULL, 23420), -- 33296
(33181, 'deDE', 'Anaya Dämmerflucht', '', NULL, NULL, 23420), -- 33181
(33207, 'deDE', 'Lady Janira', '', NULL, NULL, 23420), -- 33207
(33206, 'deDE', 'Späher der Dunkelschuppen', '', NULL, NULL, 23420), -- 33206
(33180, 'deDE', 'Zuckender Hochgeborener', '', NULL, NULL, 23420), -- 33180
(33978, 'deDE', 'Hungriger Distelbär', '', NULL, 'LootAll', 23420), -- 33978
(33179, 'deDE', 'Verfluchte Hochgeborene', '', NULL, NULL, 23420), -- 33179
(33997, 'deDE', 'Werkbank der Klippenquellfälle', '', NULL, NULL, 23420), -- 33997
(33020, 'deDE', 'Zenn Faulhuf', '', NULL, NULL, 23420), -- 33020
(33981, 'deDE', 'Greef', '', 'Furrows Diener', NULL, 23420), -- 33981
(33980, 'deDE', 'Apotheker Furrows', '', NULL, NULL, 23420), -- 33980
(33022, 'deDE', 'Übler Schänder', '', NULL, NULL, 23420), -- 33022
(33024, 'deDE', 'Unverderbter Distelbär', '', NULL, NULL, 23420), -- 33024
(33023, 'deDE', 'Unverderbte Nachtratte', '', NULL, NULL, 23420), -- 33023
(33021, 'deDE', 'Übler Grell', '', NULL, NULL, 23420), -- 33021
(32868, 'deDE', 'Lorenth Donnerruf', '', NULL, NULL, 23420), -- 32868
(48764, 'deDE', 'Telarius Leerenwanderer', '', 'Herold der Dunklen Umklammerung', NULL, 23420), -- 48764
(48763, 'deDE', 'Unglückseliger Hochgeborener', '', NULL, NULL, 23420), -- 48763
(48650, 'deDE', 'Whrilpool Controller Bunny (CSA)', '', NULL, NULL, 23420), -- 48650
(48648, 'deDE', 'Whirlpool Camera Bunny', '', NULL, NULL, 23420), -- 48648
(33905, 'deDE', 'Verderbte Distelbärmatriarchin', '', NULL, NULL, 23420), -- 33905
(33903, 'deDE', 'Distelbärenjunges', '', NULL, NULL, 23420), -- 33903
(33884, 'deDE', 'Verderbte Nachtratte', '', NULL, NULL, 23420), -- 33884
(34033, 'deDE', 'Teegan Holloway', '', 'Vernichter des Lichts', NULL, 23420), -- 34033
(34231, 'deDE', 'Shatterspear Water Cyclone Bunny', '', NULL, NULL, 23420), -- 34231
(32869, 'deDE', 'Sheya Sturmwirker', '', NULL, NULL, 23420), -- 32869
(32860, 'deDE', 'Schamane der Splitterspeere', '', NULL, NULL, 23420), -- 32860
(34103, 'deDE', 'Keynira Eulenschwinge', '', NULL, NULL, 23420), -- 34103
(32966, 'deDE', 'Balthule Schattenhieb', '', NULL, NULL, 23420), -- 32966
(32965, 'deDE', 'Schildwache Tysha Mondklinge', '', NULL, NULL, 23420), -- 32965
(32963, 'deDE', 'Leutnant Morra Sternenhauch', '', NULL, NULL, 23420), -- 32963
(43419, 'deDE', 'Rembar Bellanne', '', 'Versorger', NULL, 23420), -- 43419
(34041, 'deDE', 'Mathas Wilderwald', '', NULL, NULL, 23420), -- 34041
(33313, 'deDE', 'Geflecktes Reh', '', NULL, 'LootAll', 23420), -- 33313
(34030, 'deDE', 'Opfer des Dunklen Strangs', '', NULL, NULL, 23420), -- 34030
(34046, 'deDE', 'Plünderer der Verlassenen', '', NULL, NULL, 23420), -- 34046
(33009, 'deDE', 'Verderbter Distelbär', '', NULL, NULL, 23420), -- 33009
(32975, 'deDE', 'Verrottender Distelbär', '', NULL, 'LootAll', 23420), -- 32975
(34056, 'deDE', 'Wachsamer Beschützer', '', NULL, NULL, 23420), -- 34056
(33175, 'deDE', 'Johnathan Staats', '', NULL, NULL, 23420), -- 33175
(32936, 'deDE', 'Gezeitenkriecherjungtier', '', NULL, NULL, 23420), -- 32936
(33311, 'deDE', 'Dunkelküstenhirsch', '', NULL, NULL, 23420), -- 33311
(32935, 'deDE', 'Verderbter Gezeitenkriecher', '', NULL, NULL, 23420), -- 32935
(62246, 'deDE', 'Schimmerschnecke', '', NULL, 'wildpetcapturable', 23420), -- 62246
(7015, 'deDE', 'Platsch der Grausame', '', NULL, NULL, 23420), -- 7015
(33277, 'deDE', 'Krieger der Grauflossen', '', NULL, NULL, 23420), -- 33277
(33262, 'deDE', 'Gezeitenjäger der Grauflossen', '', NULL, NULL, 23420), -- 33262
(64375, 'deDE', 'Schimmerschnecke', '', NULL, 'wildpetcatptureable', 23420), -- 64375
(33345, 'deDE', 'Luk''gar', '', 'Flugmeister der Splitterspeere', NULL, 23420), -- 33345
(32964, 'deDE', 'Schildwache Aynasha', '', NULL, NULL, 23420), -- 32964
(33055, 'deDE', 'Alanndarian Nachtweise', '', NULL, NULL, 23420), -- 33055
(32970, 'deDE', 'Rit''ko', '', 'Folterer der Splitterspeere', NULL, 23420), -- 32970
(34248, 'deDE', 'Mystiker der Splitterspeere', '', NULL, NULL, 23420), -- 34248
(33056, 'deDE', 'Shatterspear Supplies Credit Bunny', '', NULL, NULL, 23420), -- 33056
(32859, 'deDE', 'Vollstrecker der Horde', '', NULL, NULL, 23420), -- 32859
(32863, 'deDE', 'Aufseher der Splitterspeere', '', NULL, NULL, 23420), -- 32863
(32861, 'deDE', 'Arbeiter der Splitterspeere', '', NULL, NULL, 23420), -- 32861
(34321, 'deDE', 'Shatterspear Pass Fire Bunny', '', NULL, NULL, 23420), -- 34321
(34309, 'deDE', 'Räuber der Splitterspeere', '', NULL, NULL, 23420), -- 34309
(33176, 'deDE', 'Kerlonian Schattenspiel', '', NULL, NULL, 23420), -- 33176
(33359, 'deDE', 'Nachtsäblerreiterin', '', NULL, NULL, 23420), -- 33359
(32855, 'deDE', 'Priesterin der Splitterspeere', '', NULL, NULL, 23420), -- 32855
(48024, 'deDE', 'Winna''s Slime Bunny Left', '', NULL, NULL, 23420), -- 48024
(48025, 'deDE', 'Winna''s Slime Bunny Center', '', NULL, NULL, 23420), -- 48025
(47679, 'deDE', 'Winna Hazzard', '', NULL, NULL, 23420), -- 47679
(50833, 'deDE', 'Dämmermantel', '', NULL, NULL, 23420), -- 50833
(50724, 'deDE', 'Schauderkrabbler', '', NULL, NULL, 23420), -- 50724
(48333, 'deDE', 'Großknecht Pikwik', '', NULL, NULL, 23420), -- 48333
(48493, 'deDE', 'Alton Redding', '', NULL, NULL, 23420), -- 48493
(47923, 'deDE', 'Feronas Sündschwelger', '', NULL, NULL, 23420), -- 47923
(48007, 'deDE', 'Heiler von Wisperwind', '', 'Der Smaragdkreis', NULL, 23420), -- 48007
(51513, 'deDE', 'Haudrauf von Eisenwald', '', NULL, NULL, 23420), -- 51513
(48236, 'deDE', 'Muzz Flimspann', '', 'Gemischtwaren', NULL, 23420), -- 48236
(48235, 'deDE', 'Buzz Hacksäge', '', 'Handwerkswaren', NULL, 23420), -- 48235
(48127, 'deDE', 'Darla Schürfraupe', '', NULL, NULL, 23420), -- 48127
(43085, 'deDE', 'Dirzak Pryowizz', '', 'Flugmeisterin', NULL, 23420), -- 43085
(48332, 'deDE', 'Stellvertreter Klobig', '', NULL, NULL, 23420), -- 48332
(48228, 'deDE', 'Kester Mordbombe', '', 'Schmiedekunstbedarf', NULL, 23420), -- 48228
(48238, 'deDE', 'Spinz Borkenschredder', '', 'Ingenieursbedarf', NULL, 23420), -- 48238
(48310, 'deDE', 'Ölballon', '', NULL, NULL, 23420), -- 48310
(7104, 'deDE', 'Moosmannus', '', NULL, NULL, 23420), -- 7104
(50864, 'deDE', 'Dickicht', '', NULL, NULL, 23420), -- 50864
(47556, 'deDE', 'Drizle', '', 'Junges der Holzschlundfeste', NULL, 23420), -- 47556
(48453, 'deDE', 'Eisenwaldhacker', '', NULL, NULL, 23420), -- 48453
(48452, 'deDE', 'Nachtlaubverteidiger', '', NULL, NULL, 23420), -- 48452
(48553, 'deDE', 'Jennette Doyle', '', 'Reagenzien', NULL, 23420), -- 48553
(48492, 'deDE', 'Lyros Flinkwind', '', NULL, NULL, 23420), -- 48492
(48258, 'deDE', 'Willard Harrington', '', 'Schmiedekunstbedarf', NULL, 23420), -- 48258
(26043, 'deDE', 'Dampfstoß', '', NULL, NULL, 23420), -- 26043
(543, 'deDE', 'Nalesette Wildbringer', '', 'Tierausbilderin', NULL, 23420), -- 543
(48552, 'deDE', 'Elizabeth Nesworth', '', 'Angeln', NULL, 23420), -- 48552
(47931, 'deDE', 'Höhlenmutter Ulrica', '', 'Gastwirtin', NULL, 23420), -- 47931
(48555, 'deDE', 'James Trussel', '', 'Speis & Trank', NULL, 23420), -- 48555
(15315, 'deDE', 'Mylini Eismond', '', 'Waffenhändler', NULL, 23420), -- 15315
(2803, 'deDE', 'Malygen', '', 'Gemischtwaren', NULL, 23420), -- 2803
(12578, 'deDE', 'Mishellena', '', 'Hippogryphenmeisterin', NULL, 23420), -- 12578
(11181, 'deDE', 'Shi''alune', '', 'Kaerbrus'' Tier', NULL, 23420), -- 11181
(5501, 'deDE', 'Kaerbrus', '', 'Jägerlehrer', NULL, 23420), -- 5501
(48556, 'deDE', 'Nachtlaubwächter', '', NULL, NULL, 23420),
(48551, 'deDE', 'Darren Clease', '', 'Handwerkswaren', NULL, 23420), -- 48551
(7100, 'deDE', 'Wucherborkenschinder', '', NULL, NULL, 23420), -- 7100
(14343, 'deDE', 'Olm der Weise', '', NULL, NULL, 23420), -- 14343
(48259, 'deDE', 'Eisenwaldschredder', '', NULL, NULL, 23420), -- 48259
(48454, 'deDE', 'Nachtlaubirrwisch', '', NULL, NULL, 23420), -- 48454
(7149, 'deDE', 'Siechender Beschützer', '', NULL, NULL, 23420), -- 7149
(7139, 'deDE', 'Eisenwaldstampfer', '', NULL, NULL, 23420), -- 7139
(48077, 'deDE', 'Eisenwaldinsel', '', NULL, NULL, 23420), -- 48077
(48587, 'deDE', 'Moahi', '', 'Schneiderbedarf', NULL, 23420), -- 48587
(48581, 'deDE', 'Niluut', '', 'Inschriftenkundebedarf', NULL, 23420), -- 48581
(48580, 'deDE', 'Desaan', '', 'Kochbedarf', NULL, 23420), -- 48580
(48456, 'deDE', 'Tollwütiger Kreischer', '', NULL, NULL, 23420), -- 48456
(48459, 'deDE', 'Hüterin Purhain', '', 'Der Smaragdkreis', NULL, 23420), -- 48459
(43073, 'deDE', 'Hanah Südhymne', '', 'Hippogryphenmeisterin', NULL, 23420), -- 43073
(48577, 'deDE', 'Ciana', '', 'Waffenschmiedin', NULL, 23420), -- 48577
(48574, 'deDE', 'Felaana', '', 'Handwerkswaren', NULL, 23420), -- 48574
(48573, 'deDE', 'Chaewel', '', 'Schmiedekunstbedarf', NULL, 23420), -- 48573
(47843, 'deDE', 'Jägerin Selura', '', NULL, NULL, 23420), -- 47843
(48349, 'deDE', 'Hurak Wildhorn', '', 'Der Smaragdkreis', NULL, 23420), -- 48349
(48339, 'deDE', 'Elessa Sternenhauch', '', 'Der Smaragdkreis', NULL, 23420), -- 48339
(48126, 'deDE', 'Isural Waldschwur', '', 'Der Smaragdkreis', NULL, 23420), -- 48126
(48491, 'deDE', 'James Hallow', '', NULL, NULL, 23420), -- 48491
(48469, 'deDE', 'Fez Mischmasch', '', 'PR', NULL, 23420), -- 48469
(48216, 'deDE', 'Hurah', '', 'Stallmeister', NULL, 23420), -- 48216
(47844, 'deDE', 'Beschützer von Wisperwind', '', NULL, NULL, 23420), -- 47844
(47842, 'deDE', 'Erzdruide Navarax', '', NULL, NULL, 23420), -- 47842
(48215, 'deDE', 'Gastwirtin Wylaria', '', 'Gastwirtin', NULL, 23420), -- 48215
(47747, 'deDE', 'Peitscher von Wisperwind', '', NULL, 'Interact', 23420), -- 47747
(48038, 'deDE', 'Eisenholzbrummer', '', NULL, NULL, 23420), -- 48038
(50777, 'deDE', 'Nadel', '', NULL, NULL, 23420), -- 50777
(48455, 'deDE', 'Teufelsmoderrenner', '', NULL, NULL, 23420), -- 48455
(48457, 'deDE', 'Besudeltes Eichhörnchen', '', NULL, 'questinteract', 23420), -- 48457
(48344, 'deDE', 'Kroshius', '', 'Herold des Falls der Legionen', NULL, 23420), -- 48344
(62317, 'deDE', 'Höllchen', '', NULL, 'wildpetcapturable', 23420), -- 62317
(7136, 'deDE', 'Höllenbestienwachposten', '', NULL, NULL, 23420), -- 7136
(9879, 'deDE', 'Entropischer Schrecken', '', NULL, NULL, 23420), -- 9879
(14345, 'deDE', 'Der Ongar', '', NULL, NULL, 23420), -- 14345
(9454, 'deDE', 'Xavathras', '', NULL, NULL, 23420), -- 9454
(107596, 'deDE', 'Grimmrott', '', NULL, NULL, 23420), -- 107596
(9517, 'deDE', 'Schattenlord Fel''dan', '', NULL, NULL, 23420), -- 9517
(9516, 'deDE', 'Lord Schattenfluch', '', NULL, NULL, 23420), -- 9516
(48240, 'deDE', 'Romo''s Double Bunny', '', NULL, NULL, 23420), -- 48240
(48023, 'deDE', 'Winna''s Slime Bunny Right', '', NULL, NULL, 23420), -- 48023
(9877, 'deDE', 'Prinz Xavalis', '', NULL, NULL, 23420), -- 9877
(50362, 'deDE', 'Schwarzsumpf der Fangzahn', '', NULL, NULL, 23420), -- 50362
(51664, 'deDE', 'Andalar Dämmertal', '', 'Der Smaragdkreis', NULL, 23420), -- 51664
(7093, 'deDE', 'Übler Schlamm', '', NULL, NULL, 23420), -- 7093
(9861, 'deDE', 'Moora', '', NULL, NULL, 23420), -- 9861
(9860, 'deDE', 'Salia', '', NULL, NULL, 23420), -- 9860
(15603, 'deDE', 'Urahne Nachtwind', '', NULL, NULL, 23420), -- 15603
(47687, 'deDE', 'Winnas Kätzchen', '', NULL, NULL, 23420), -- 47687
(9862, 'deDE', 'Legionär von Jaedenar', '', NULL, NULL, 23420), -- 9862
(7125, 'deDE', 'Hund von Jaedenar', '', NULL, NULL, 23420), -- 7125
(7110, 'deDE', 'Jadefeuerschattenpirscher', '', NULL, NULL, 23420), -- 7110
(47392, 'deDE', 'Smaragdbärkin', '', NULL, NULL, 23420), -- 47392
(7106, 'deDE', 'Jadefeuerschurke', '', NULL, NULL, 23420), -- 7106
(14344, 'deDE', 'Mongress', '', NULL, NULL, 23420), -- 14344
(7113, 'deDE', 'Wächter von Jaedenar', '', NULL, NULL, 23420), -- 7113
(7112, 'deDE', 'Kultist von Jaedenar', '', NULL, NULL, 23420), -- 7112
(7115, 'deDE', 'Adept von Jaedenar', '', NULL, NULL, 23420), -- 7115
(66447, 'deDE', 'Zoltankultist', '', NULL, NULL, 23420), -- 66447
(66445, 'deDE', 'Hasswandler', '', NULL, NULL, 23420), -- 66445
(66444, 'deDE', 'Strahler', '', NULL, NULL, 23420), -- 66444
(66443, 'deDE', 'Ultramus', '', NULL, NULL, 23420), -- 66443
(66442, 'deDE', 'Zoltan', '', 'Meistertierzähmer', NULL, 23420), -- 66442
(47675, 'deDE', 'Schleimsklave des Blutgiftpostens', '', NULL, NULL, 23420), -- 47675
(7126, 'deDE', 'Jäger von Jaedenar', '', NULL, NULL, 23420), -- 7126
(7120, 'deDE', 'Hexenmeister von Jaedenar', '', NULL, NULL, 23420), -- 7120
(7118, 'deDE', 'Dunkelwirker von Jaedenar', '', NULL, NULL, 23420), -- 7118
(51025, 'deDE', 'Dilennaa', '', NULL, NULL, 23420), -- 51025
(7114, 'deDE', 'Vollstrecker von Jaedenar', '', NULL, NULL, 23420), -- 7114
(47696, 'deDE', 'Kelnir Blattlied', '', 'Der Smaragdkreis', NULL, 23420), -- 47696
(47692, 'deDE', 'Altsoba Rachtotem', '', NULL, NULL, 23420), -- 47692
(7092, 'deDE', 'Besudelter Schlamm', '', NULL, NULL, 23420), -- 7092
(48599, 'deDE', 'Gastwirtin Teenycaugh', '', 'Gastwirtin', NULL, 23420), -- 48599
(47617, 'deDE', 'Farlus Wildherz', '', 'Der Smaragdkreis', NULL, 23420), -- 47617
(43079, 'deDE', 'Chyella Stillau', '', 'Hippogryphenmeisterin', NULL, 23420), -- 43079
(11019, 'deDE', 'Jessir Mondsichel', '', NULL, NULL, 23420), -- 11019
(10922, 'deDE', 'Greta Mooshuf', '', 'Der Smaragdkreis', NULL, 23420), -- 10922
(8958, 'deDE', 'Wutklauenraufer', '', NULL, NULL, 23420), -- 8958
(7099, 'deDE', 'Eisenschnabeljäger', '', NULL, NULL, 23420), -- 7099
(8960, 'deDE', 'Teufelspfotenaasfresser', '', NULL, NULL, 23420), -- 8960
(47339, 'deDE', 'Impsy', '', NULL, NULL, 23420), -- 47339
(14339, 'deDE', 'Todesheuler', '', NULL, NULL, 23420), -- 14339
(10923, 'deDE', 'Tenell Laubschleicher', '', 'Der Smaragdkreis', NULL, 23420), -- 10923
(10921, 'deDE', 'Taronn Rotfeder', '', 'Der Smaragdkreis', NULL, 23420), -- 10921
(48608, 'deDE', 'Kamar', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 48608
(48607, 'deDE', 'Muurald', '', 'Gemischtwaren', NULL, 23420), -- 48607
(22931, 'deDE', 'Gorrim', '', 'Flugmeister des Smaragdkreises', NULL, 23420), -- 22931
(11554, 'deDE', 'Grazle', '', NULL, NULL, 23420), -- 11554
(10924, 'deDE', 'Ivy Laubschleicher', '', 'Der Smaragdkreis', NULL, 23420), -- 10924
(3792, 'deDE', 'Terrowulfrudelführer', '', NULL, NULL, 23420), -- 3792
(3919, 'deDE', 'Siechendes Urtum', '', NULL, NULL, 23420), -- 3919
(14342, 'deDE', 'Wutpranke', '', NULL, NULL, 23420), -- 14342
(62314, 'deDE', 'Besudelte Kakerlake', '', NULL, 'wildpetcapturable', 23420), -- 62314
(7154, 'deDE', 'Gärtner der Totenwaldfelle', '', NULL, NULL, 23420), -- 7154
(7153, 'deDE', 'Krieger der Totenwaldfelle', '', NULL, NULL, 23420), -- 7153
(7155, 'deDE', 'Pfadfinder der Totenwaldfelle', '', NULL, NULL, 23420), -- 7155
(47308, 'deDE', 'Felwood Honey Bunny', '', NULL, NULL, 23420), -- 47308
(14340, 'deDE', 'Alshirr Teufelsodem', '', NULL, NULL, 23420), -- 14340
(50003, 'deDE', 'Besudelte Motte', '', NULL, NULL, 23420), -- 50003
(7109, 'deDE', 'Jadefeuerteufelsanbeter', '', NULL, NULL, 23420), -- 7109
(7105, 'deDE', 'Jadefeuersatyr', '', NULL, NULL, 23420), -- 7105
(47369, 'deDE', 'Jadefeuerwichtel', '', NULL, NULL, 23420), -- 47369
(9116, 'deDE', 'Eridan Blauwind', '', 'Der Smaragdkreis', NULL, 23420), -- 9116
(47341, 'deDE', 'Arkanistin Delaris', '', 'Die Hochgeborene', NULL, 23420), -- 47341
(62315, 'deDE', 'Besudelte Motte', '', NULL, 'wildpetcapturable', 23420), -- 62315
(17425, 'deDE', 'Taljäger', '', NULL, NULL, 23420), -- 17425
(63335, 'deDE', 'Mojo Sturmbräu', '', 'Mönchslehrer', NULL, 23420), -- 63335
(17071, 'deDE', 'Technikerin Zhanaa', '', NULL, NULL, 23420), -- 17071
(16920, 'deDE', 'Ryosh', '', 'Gemischtwaren', NULL, 23420), -- 16920
(16919, 'deDE', 'Mura', '', 'Waffenschmied', NULL, 23420), -- 16919
(16500, 'deDE', 'Valaatu', '', 'Magierlehrerin', NULL, 23420), -- 16500
(16535, 'deDE', 'Verteidiger Aldar', '', NULL, NULL, 23420), -- 16535
(16503, 'deDE', 'Kore', '', 'Kriegerlehrer', NULL, 23420), -- 16503
(16499, 'deDE', 'Keilnei', '', 'Jägerlehrerin', NULL, 23420), -- 16499
(16502, 'deDE', 'Zalduun', '', 'Priesterlehrer', NULL, 23420), -- 16502
(16537, 'deDE', 'Mutierter Eulkin', '', NULL, NULL, 23420), -- 16537
(16518, 'deDE', 'Nisteleulkin', '', NULL, NULL, 23420), -- 16518
(20239, 'deDE', 'Neutralizing Emote Placeholder', '', NULL, NULL, 23420), -- 20239
(16922, 'deDE', 'Rote Strahlung', '', NULL, NULL, 23420), -- 16922
(17181, 'deDE', 'Geist des Wassers', '', NULL, NULL, 23420), -- 17181
(17182, 'deDE', 'Geist des Feuers', '', NULL, NULL, 23420), -- 17182
(17180, 'deDE', 'Geist der Luft', '', NULL, NULL, 23420), -- 17180
(17179, 'deDE', 'Ruheloser Geist der Erde', '', NULL, NULL, 23420), -- 17179
(17087, 'deDE', 'Geist des Tals', '', NULL, NULL, 23420), -- 17087
(16475, 'deDE', 'Megelon', '', NULL, NULL, 23420), -- 16475
(16971, 'deDE', 'Verletzter Draenei', '', NULL, NULL, 23420), -- 16971
(16918, 'deDE', 'Jel', '', 'Tuch- & Lederhändlerin', NULL, 23420), -- 16918
(16501, 'deDE', 'Aurelon', '', 'Paladinlehrer', NULL, 23420), -- 16501
(16917, 'deDE', 'Aurok', '', 'Rüstungsschmied', NULL, 23420), -- 16917
(44703, 'deDE', 'Trainingsattrappe', '', NULL, NULL, 23420), -- 44703
(20233, 'deDE', 'Lehrling Vishael', '', NULL, NULL, 23420), -- 20233
(20227, 'deDE', 'Lehrling Tedon', '', NULL, NULL, 23420), -- 20227
(16514, 'deDE', 'Botanikerin Taerix', '', NULL, NULL, 23420), -- 16514
(17073, 'deDE', 'Trauernder Draenei', '', NULL, NULL, 23420), -- 17073
(16477, 'deDE', 'Proenitus', '', NULL, NULL, 23420), -- 16477
(16517, 'deDE', 'Mutierter Wurzelpeitscher', '', NULL, NULL, 23420), -- 16517
(17947, 'deDE', 'Red Crystal Bunny', '', NULL, NULL, 23420), -- 17947
(16522, 'deDE', 'Gutachterin Kandress', '', NULL, NULL, 23420), -- 16522
(16546, 'deDE', 'Tolaan', '', NULL, NULL, 23420), -- 16546
(16521, 'deDE', 'Blutelfenspäher', '', NULL, NULL, 23420), -- 16521
(16516, 'deDE', 'Unberechenbare Mutation', '', NULL, NULL, 23420), -- 16516
(16483, 'deDE', 'Überlebender der Draenei', '', NULL, NULL, 23420), -- 16483
(16520, 'deDE', 'Talmotte', '', NULL, NULL, 23420), -- 16520
(16921, 'deDE', 'Wächter des Am''mentals', '', NULL, NULL, 23420), -- 16921
(16554, 'deDE', 'Aeun', '', NULL, NULL, 23420), -- 16554
(18810, 'deDE', 'Otonambusi', '', 'Gemischtwaren', NULL, 23420), -- 18810
(17116, 'deDE', 'Exarch Menelaous', '', NULL, NULL, 23420), -- 17116
(17214, 'deDE', 'Anachoretin Fateema', '', 'Lehrerin für Erste Hilfe', NULL, 23420), -- 17214
(17117, 'deDE', 'Verletzte Nachtelfenpriesterin', '', NULL, NULL, 23420), -- 17117
(17983, 'deDE', 'Heur', '', 'Kräuterkundelehrer', NULL, 23420), -- 17983
(17215, 'deDE', 'Daedal', '', 'Alchemielehrer', NULL, 23420), -- 17215
(17488, 'deDE', 'Dulvi', '', 'Bergbaulehrerin', NULL, 23420), -- 17488
(17114, 'deDE', 'Arugoo von der Tannenruhfeste', '', NULL, NULL, 23420), -- 17114
(17360, 'deDE', 'Totem von Akida', '', NULL, NULL, 23420), -- 17360
(17232, 'deDE', 'Kryptograf Aurren', '', NULL, NULL, 23420), -- 17232
(17222, 'deDE', 'Konstrukteur Daelo', '', 'Ingenieurskunstlehrer', NULL, 23420), -- 17222
(17212, 'deDE', 'Tuluun', '', 'Schamanenlehrer', NULL, 23420), -- 17212
(16551, 'deDE', 'Techniker Dyvuun', '', NULL, NULL, 23420), -- 16551
(16476, 'deDE', 'Jaeleil', '', NULL, NULL, 23420), -- 16476
(17480, 'deDE', 'Ruada', '', 'Kriegerlehrerin', NULL, 23420), -- 17480
(17482, 'deDE', 'Guvan', '', 'Priesterlehrer', NULL, 23420), -- 17482
(16553, 'deDE', 'Versorgerin Chellan', '', 'Gastwirtin', NULL, 23420), -- 16553
(17485, 'deDE', 'Esbina', '', 'Stallmeisterin', NULL, 23420), -- 17485
(17483, 'deDE', 'Tullas', '', 'Paladinlehrerin', NULL, 23420), -- 17483
(17481, 'deDE', 'Semid', '', 'Magierlehrer', NULL, 23420), -- 17481
(17228, 'deDE', 'Draeneikonstrukteur', '', NULL, NULL, 23420), -- 17228
(43991, 'deDE', 'Zaldaan', '', 'Flugmeister', NULL, 23420), -- 43991
(17101, 'deDE', 'Diktynna', '', 'Angellehrerin & Angelbedarf', NULL, 23420), -- 17101
(17243, 'deDE', 'Ingenieur "Funks" Oberschleif', '', 'Gesandter von Mekkadrill', NULL, 23420), -- 17243
(17311, 'deDE', 'Cowlen', '', NULL, NULL, 23420), -- 17311
(17186, 'deDE', 'Verstörte Eulenbestie', '', NULL, NULL, 23420), -- 17186
(17187, 'deDE', 'Verwirrte Eulenbestie', '', NULL, NULL, 23420), -- 17187
(17312, 'deDE', 'Magwin', '', NULL, NULL, 23420), -- 17312
(17188, 'deDE', 'Irre Eulenbestie', '', NULL, NULL, 23420), -- 17188
(21376, 'deDE', 'Krusti Bob', '', NULL, NULL, 23420), -- 21376
(17614, 'deDE', 'Jägerin Kella Nachtbogen', '', NULL, NULL, 23420), -- 17614
(16721, 'deDE', 'Shalannius', '', 'Druidenlehrer', NULL, 23420), -- 16721
(17274, 'deDE', 'Glutos'' Ziel', '', NULL, NULL, 23420), -- 17274
(17205, 'deDE', 'Glutos', '', NULL, NULL, 23420), -- 17205
(17361, 'deDE', 'Totem von Coo', '', NULL, NULL, 23420), -- 17361
(17551, 'deDE', 'Tavara', '', NULL, NULL, 23420), -- 17551
(17110, 'deDE', 'Acteon', '', 'Jägerlehrer', NULL, 23420), -- 17110
(17484, 'deDE', 'Buruk', '', 'Tierausbilder', NULL, 23420), -- 17484
(63078, 'deDE', 'Taumel', '', 'Kampfhaustier', NULL, 23420), -- 63078
(63077, 'deDE', 'Lehna', '', 'Kampfhaustiertrainerin', NULL, 23420), -- 63077
(47431, 'deDE', 'Valn', '', 'Berufsausbilder', NULL, 23420), -- 47431
(17929, 'deDE', 'Kioni', '', 'Tuch- & Lederhändlerin', NULL, 23420), -- 17929
(17486, 'deDE', 'Ziz', '', 'Handwerker', NULL, 23420), -- 17486
(17930, 'deDE', 'Nabek', '', 'Waffen- & Rüstungshändler', NULL, 23420), -- 17930
(17279, 'deDE', 'Edelsteinkundiger der Venture Co.', '', NULL, NULL, 23420), -- 17279
(17278, 'deDE', 'Saboteur der Venture Co.', '', NULL, NULL, 23420), -- 17278
(17242, 'deDE', 'Archäologe Adamant Eisenherz', '', 'Forscherliga', NULL, 23420), -- 17242
(17241, 'deDE', 'Priesterin Kyleen Il''dinare', '', NULL, NULL, 23420), -- 17241
(17240, 'deDE', 'Admiral Odesyus', '', 'Marinekommando der Allianz', NULL, 23420), -- 17240
(17285, 'deDE', 'Ragnarose', '', 'Kräckas Papagei', NULL, 23420), -- 17285
(17246, 'deDE', '"Kräcka" Aschebäscha', '', 'Kochkunstlehrer & -bedarf', NULL, 23420), -- 17246
(17245, 'deDE', 'Schmiedin Calypso', '', 'Schmiedekunstlehrerin & -bedarf', NULL, 23420), -- 17245
(17489, 'deDE', 'Logan Daniel', '', 'Gemischtwaren', NULL, 23420), -- 17489
(17487, 'deDE', 'Erin Kelly', '', 'Schneiderlehrerin', NULL, 23420), -- 17487
(16225, 'deDE', 'Packmuli', '', NULL, NULL, 23420), -- 16225
(620, 'deDE', 'Huhn', '', NULL, NULL, 23420), -- 620
(17244, 'deDE', 'Axtkämpfer der Allianz', '', NULL, NULL, 23420), -- 17244
(17247, 'deDE', 'Holzarbeiter der Allianz', '', NULL, NULL, 23420), -- 17247
(17587, 'deDE', 'Junger Draenei', '', NULL, NULL, 23420), -- 17587
(17196, 'deDE', 'Lauerwurz', '', NULL, NULL, 23420), -- 17196
(17372, 'deDE', 'Grünschreiterjunges', '', NULL, NULL, 23420), -- 17372
(17217, 'deDE', 'Stachelkriecher', '', NULL, NULL, 23420), -- 17217
(17298, 'deDE', 'Kriegsherr Sriss''tiz', '', NULL, NULL, 23420), -- 17298
(17195, 'deDE', 'Sirene der Hasspranken', '', NULL, NULL, 23420), -- 17195
(17193, 'deDE', 'Naga der Hasspranken', '', NULL, NULL, 23420), -- 17193
(17194, 'deDE', 'Myrmidone der Hasspranken', '', NULL, NULL, 23420), -- 17194
(17364, 'deDE', 'Totem von Vark', '', NULL, NULL, 23420), -- 17364
(17375, 'deDE', 'Gefangener Furbolg der Tannenruhfeste', '', NULL, NULL, 23420), -- 17375
(17183, 'deDE', 'Furbolg der Sichelklauen', '', NULL, NULL, 23420), -- 17183
(17184, 'deDE', 'Windrufer der Sichelklauen', '', NULL, NULL, 23420), -- 17184
(17185, 'deDE', 'Ursa der Sichelklauen', '', NULL, NULL, 23420), -- 17185
(18038, 'deDE', 'Friedensbewahrer der Azurmythosinsel', '', NULL, 'Directions', 23420), -- 18038
(17453, 'deDE', 'Vision von Kurken', '', NULL, NULL, 23420), -- 17453
(17452, 'deDE', 'Vision des prophezeiten Helden', '', NULL, NULL, 23420), -- 17452
(17448, 'deDE', 'Häuptling Oomooroo', '', NULL, NULL, 23420), -- 17448
(17490, 'deDE', 'Ergh der Tannenruhfeste', '', 'Handwerkswaren', NULL, 23420), -- 17490
(17438, 'deDE', 'Kadaver eines wahnsinnigen Wildekin', '', NULL, NULL, 23420), -- 17438
(17437, 'deDE', 'Leichnam eines Verteidigers der Tannenruhfeste', '', NULL, NULL, 23420), -- 17437
(17444, 'deDE', 'Magtor der blinde Behüter', '', NULL, NULL, 23420), -- 17444
(17440, 'deDE', 'Oberhäuptling der Tannenruhfeste', '', NULL, NULL, 23420), -- 17440
(17441, 'deDE', 'Gurf', '', 'Kürschnerlehrer', NULL, 23420), -- 17441
(17439, 'deDE', 'Jäger der Tannenruhfeste', '', NULL, NULL, 23420), -- 17439
(17445, 'deDE', 'Jüngling Tannenruh', '', NULL, NULL, 23420), -- 17445
(17442, 'deDE', 'Moordo', '', 'Lederverarbeitungslehrer', NULL, 23420), -- 17442
(17446, 'deDE', 'Parkat Stahlfell', '', 'Gemischtwaren', NULL, 23420), -- 17446
(17443, 'deDE', 'Kurz der Weise', '', NULL, NULL, 23420), -- 17443
(17432, 'deDE', 'Verteidiger der Tannenruhfeste', '', NULL, NULL, 23420), -- 17432
(61255, 'deDE', 'Stinktier', '', NULL, 'wildpetcapturable', 23420), -- 61255
(17272, 'deDE', 'Flammender Aspekt', '', NULL, NULL, 23420), -- 17272
(17556, 'deDE', 'Todesfelshetzer', '', NULL, NULL, 23420), -- 17556
(17199, 'deDE', 'Felshetzerexemplar', '', NULL, NULL, 23420), -- 17199
(17495, 'deDE', 'Räuber der Tannenruhfeste', '', NULL, NULL, 23420), -- 17495
(17189, 'deDE', 'Wahnsinniger Wildekin', '', NULL, NULL, 23420), -- 17189
(17467, 'deDE', 'Stinktier', '', NULL, NULL, 23420), -- 17467
(17192, 'deDE', 'Jäger der Schlickflossen', '', NULL, NULL, 23420), -- 17192
(17191, 'deDE', 'Orakel der Schlickflossen', '', NULL, NULL, 23420), -- 17191
(17475, 'deDE', 'Murgurgula', '', NULL, NULL, 23420), -- 17475
(17190, 'deDE', 'Murloc der Schlickflossen', '', NULL, NULL, 23420), -- 17190
(17374, 'deDE', 'Großer Grünschreiter', '', NULL, NULL, 23420), -- 17374
(17203, 'deDE', 'Nachtpirscher', '', NULL, NULL, 23420), -- 17203
(18815, 'deDE', 'Proselyt der Exodar', '', NULL, NULL, 23420), -- 18815
(20914, 'deDE', 'Aalun', '', 'Reitlehrerin', NULL, 23420), -- 20914
(20850, 'deDE', 'Großer lila Elekk', '', NULL, NULL, 23420),
(20848, 'deDE', 'Großer blauer Elekk', '', NULL, NULL, 23420), -- 20848
(20847, 'deDE', 'Lila Elekk', '', NULL, NULL, 23420), -- 20847
(20846, 'deDE', 'Grauer Elekk', '', NULL, NULL, 23420), -- 20846
(17584, 'deDE', 'Torallius der Herdentreiber', '', 'Elekkzüchter', NULL, 23420), -- 17584
(19658, 'deDE', 'Brauner Elekk', '', NULL, NULL, 23420), -- 19658
(20849, 'deDE', 'Großer grüner Elekk', '', NULL, NULL, 23420), -- 20849
(17530, 'deDE', 'Elekk', '', NULL, NULL, 23420), -- 17530
(17197, 'deDE', 'Dreschwurz', '', NULL, NULL, 23420), -- 17197
(16739, 'deDE', 'Versorger Breel', '', 'Gastwirt', NULL, 23420), -- 16739
(17520, 'deDE', 'Gurrag', '', 'Schamanenlehrerin', NULL, 23420), -- 17520
(21019, 'deDE', 'Sixx', '', 'Mottenpflegerin', NULL, 23420), -- 21019
(21018, 'deDE', 'Weiße Motte', '', NULL, NULL, 23420), -- 21018
(21010, 'deDE', 'Blaue Motte', '', NULL, NULL, 23420), -- 21010
(21008, 'deDE', 'Gelbe Motte', '', NULL, NULL, 23420), -- 21008
(18919, 'deDE', 'Sayari', '', NULL, NULL, 23420), -- 18919
(18918, 'deDE', 'Drysc', '', NULL, NULL, 23420), -- 18918
(18917, 'deDE', 'Chakaa', '', NULL, NULL, 23420), -- 18917
(22905, 'deDE', 'Scherbenwelt Kinderwoche Exodar 02 Auslöser', '', NULL, NULL, 23420), -- 22905
(18916, 'deDE', 'Valon', '', NULL, NULL, 23420), -- 18916
(18915, 'deDE', 'Großknecht Dunaer', '', NULL, NULL, 23420), -- 18915
(17204, 'deDE', 'Scharfseher Nobundo', '', 'Schamanenlehrer', NULL, 23420), -- 17204
(16708, 'deDE', 'Dekin', '', 'Angelbedarf', NULL, 23420), -- 16708
(16774, 'deDE', 'Erett', '', 'Angellehrerin', NULL, 23420), -- 16774
(17519, 'deDE', 'Hobahken', '', 'Schamanenlehrer', NULL, 23420), -- 17519
(17219, 'deDE', 'Sulaa', '', 'Schamanenlehrerin', NULL, 23420), -- 17219
(16757, 'deDE', 'Bildine', '', 'Reagenzien', NULL, 23420), -- 16757
(17435, 'deDE', 'Susurrus', '', NULL, NULL, 23420), -- 17435
(17436, 'deDE', 'Aspekt der Luft', '', NULL, NULL, 23420), -- 17436
(62050, 'deDE', 'Graue Motte', '', NULL, 'wildpetcapturable', 23420), -- 62050
(18813, 'deDE', 'Duumehi', '', NULL, NULL, 23420), -- 18813
(18812, 'deDE', 'Ereuso', '', NULL, NULL, 23420), -- 18812
(16731, 'deDE', 'Nus', '', 'Lehrerin für Erste Hilfe', NULL, 23420), -- 16731
(30732, 'deDE', 'Sessoh', '', 'Inschriftenkundebedarf', NULL, 23420), -- 30732
(30716, 'deDE', 'Thoth', '', 'Inschriftenkundelehrer', NULL, 23420), -- 30716
(19778, 'deDE', 'Farii', '', 'Juwelierskunstlehrerin', NULL, 23420), -- 19778
(18985, 'deDE', 'Seher Skaltesh', '', NULL, NULL, 23420), -- 18985
(17431, 'deDE', 'Velaada', '', NULL, NULL, 23420), -- 17431
(18892, 'deDE', 'Minenarbeiter der Zerschlagenen', '', NULL, NULL, 23420), -- 18892
(17512, 'deDE', 'Arred', '', 'Juwelierskunstbedarf', NULL, 23420), -- 17512
(16727, 'deDE', 'Padaar', '', 'Juwelierskunstlehrling', NULL, 23420), -- 16727
(16718, 'deDE', 'Phea', '', 'Kochbedarf', NULL, 23420), -- 16718
(16719, 'deDE', 'Mumman', '', 'Kochkunstlehrerin', NULL, 23420), -- 16719
(16764, 'deDE', 'Arthaid', '', 'Stallmeister', NULL, 23420), -- 16764
(17555, 'deDE', 'Stephanos', '', 'Hippogryphenmeister', NULL, 23420), -- 17555
(50306, 'deDE', 'Kadu', '', 'Rüstmeister der Exodar', NULL, 23420), -- 50306
(18921, 'deDE', 'Konstrukteur Drenin', '', NULL, NULL, 23420), -- 18921
(18924, 'deDE', 'Konstrukteur Andren', '', NULL, NULL, 23420), -- 18924
(19073, 'deDE', 'Nibblet', '', NULL, NULL, 23420), -- 19073
(17200, 'deDE', 'Mondweidenhirsch', '', NULL, NULL, 23420), -- 17200
(16765, 'deDE', 'Ellomin', '', 'Händlerin für stumpfe Waffen', NULL, 23420), -- 16765
(16747, 'deDE', 'Mahri', '', 'Lederrüstungshändlerin', NULL, 23420), -- 16747
(16716, 'deDE', 'Gornii', '', 'Stoffrüstungshändlerin', NULL, 23420), -- 16716
(16714, 'deDE', 'Ven', '', 'Klingenhändler', NULL, 23420), -- 16714
(16762, 'deDE', 'Treall', '', 'Schildhändlerin', NULL, 23420), -- 16762
(16753, 'deDE', 'Gotaan', '', 'Händlerin für Plattenrüstung', NULL, 23420), -- 16753
(16750, 'deDE', 'Yil', '', 'Händler für Kettenrüstungen', NULL, 23420), -- 16750
(16745, 'deDE', 'Feruul', '', 'Lederverarbeitungslehrling', NULL, 23420), -- 16745
(16728, 'deDE', 'Akham', '', 'Lederverarbeitungslehrer', NULL, 23420), -- 16728
(16712, 'deDE', 'Ganaar', '', 'Tierausbilder', NULL, 23420), -- 16712
(17505, 'deDE', 'Killac', '', 'Jägerlehrer', NULL, 23420), -- 17505
(16735, 'deDE', 'Muhaa', '', 'Büchsenmacher', NULL, 23420), -- 16735
(17122, 'deDE', 'Vord', '', 'Jägerlehrer', NULL, 23420), -- 17122
(16738, 'deDE', 'Deremiis', '', 'Jägerlehrer', NULL, 23420), -- 16738
(16715, 'deDE', 'Avelii', '', 'Bogenmacher', NULL, 23420), -- 16715
(16752, 'deDE', 'Muaat', '', 'Bergbaulehrer', NULL, 23420), -- 16752
(16751, 'deDE', 'Merran', '', 'Bergbaubedarf', NULL, 23420), -- 16751
(16743, 'deDE', 'Ghermas', '', 'Ingenieurslehrling', NULL, 23420), -- 16743
(16740, 'deDE', 'Edrem', '', 'Schmiedekunstlehrling', NULL, 23420), -- 16740
(16726, 'deDE', 'Ockil', '', 'Ingenieurskunstlehrer', NULL, 23420), -- 16726
(16724, 'deDE', 'Miall', '', 'Schmiedekunstlehrerin', NULL, 23420), -- 16724
(16713, 'deDE', 'Arras', '', 'Schmiedekunstbedarf', NULL, 23420), -- 16713
(16657, 'deDE', 'Feera', '', 'Ingenieursbedarf', NULL, 23420), -- 16657
(20121, 'deDE', 'Fingin', '', 'Gifte', NULL, 23420), -- 20121
(17504, 'deDE', 'Kazi', '', 'Kriegerlehrerin', NULL, 23420), -- 17504
(16771, 'deDE', 'Ahonan', '', 'Kriegerlehrer', NULL, 23420), -- 16771
(62464, 'deDE', 'Pan des Kampfgeists', '', 'Mönchslehrer', NULL, 23420), -- 62464
(17373, 'deDE', 'Grünschreiter', '', NULL, NULL, 23420), -- 17373
(17120, 'deDE', 'Behomat', '', 'Kriegerlehrer', NULL, 23420), -- 17120
(16748, 'deDE', 'Haferet', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 16748
(16763, 'deDE', 'Remere', '', 'Kürschnerlehrerin', NULL, 23420), -- 16763
(16767, 'deDE', 'Neii', '', 'Schneiderbedarf', NULL, 23420), -- 16767
(16746, 'deDE', 'Kayaart', '', 'Schneiderlehrling', NULL, 23420), -- 16746
(16729, 'deDE', 'Refik', '', 'Schneiderlehrer', NULL, 23420), -- 16729
(18349, 'deDE', 'Auktionatorin Iressa', '', NULL, NULL, 23420), -- 18349
(16707, 'deDE', 'Auktionator Eoch', '', NULL, NULL, 23420), -- 16707
(18348, 'deDE', 'Auktionator Fanin', '', NULL, NULL, 23420), -- 18348
(22851, 'deDE', 'Scherbenwelt Kinderwoche Exodar 01 Auslöser', '', NULL, NULL, 23420), -- 22851
(17538, 'deDE', 'O''ros', '', NULL, NULL, 23420), -- 17538
(16761, 'deDE', 'Baatun', '', 'Paladinlehrer', NULL, 23420), -- 16761
(17121, 'deDE', 'Kavaan', '', 'Paladinlehrer', NULL, 23420), -- 17121
(17509, 'deDE', 'Jol', '', 'Paladinlehrerin', NULL, 23420), -- 17509
(17216, 'deDE', 'Raubkriecher', '', NULL, NULL, 23420), -- 17216
(18800, 'deDE', 'Arbeiter', '', NULL, NULL, 23420), -- 18800
(34987, 'deDE', 'Hunara', '', 'Kampfmeisterin', NULL, 23420), -- 34987
(16736, 'deDE', 'Cemmorhan', '', 'Kräuterkundelehrer', NULL, 23420), -- 16736
(16723, 'deDE', 'Lucc', '', 'Alchemielehrer', NULL, 23420), -- 16723
(16705, 'deDE', 'Altaa', '', 'Alchemiebedarf', NULL, 23420), -- 16705
(16741, 'deDE', 'Deriz', '', 'Alchemielehrling', NULL, 23420), -- 16741
(34986, 'deDE', 'Liedel der Gerechte', '', 'Kampfmeister', NULL, 23420), -- 34986
(47570, 'deDE', 'Diya', '', 'Archäologielehrerin', NULL, 23420), -- 47570
(18903, 'deDE', 'Audrid', '', NULL, NULL, 23420), -- 18903
(18902, 'deDE', 'Emony', '', NULL, NULL, 23420), -- 18902
(18901, 'deDE', 'Curzon', '', NULL, NULL, 23420), -- 18901
(18900, 'deDE', 'Tobin', '', NULL, NULL, 23420), -- 18900
(18899, 'deDE', 'Torias', '', NULL, NULL, 23420), -- 18899
(20674, 'deDE', 'Schild Velens', '', NULL, 'Directions', 23420), -- 20674
(18814, 'deDE', 'Unsichtbarer Pirscher der Exodar', '', NULL, NULL, 23420), -- 18814
(18896, 'deDE', 'Holografischer Emitter der Exodar', '', NULL, NULL, 23420), -- 18896
(47586, 'deDE', 'Karabrel Mondlanze', '', NULL, NULL, 23420), -- 47586
(16742, 'deDE', 'Kudrii', '', 'Verzauberkunstlehrling', NULL, 23420), -- 16742
(16725, 'deDE', 'Nahogg', '', 'Verzauberkunstlehrer', NULL, 23420), -- 16725
(16722, 'deDE', 'Egomis', '', 'Verzauberkunstbedarf', NULL, 23420), -- 16722
(17511, 'deDE', 'Fallat', '', 'Priesterlehrer', NULL, 23420), -- 17511
(17510, 'deDE', 'Izmir', '', 'Priesterlehrer', NULL, 23420), -- 17510
(16756, 'deDE', 'Caedmos', '', 'Priesterlehrer', NULL, 23420), -- 16756
(18350, 'deDE', 'Jaela', '', 'Bankierin', NULL, 23420), -- 18350
(17773, 'deDE', 'Ossco', '', 'Bankier', NULL, 23420), -- 17773
(16710, 'deDE', 'Kellag', '', 'Bankier', NULL, 23420), -- 16710
(20722, 'deDE', 'Herold Bran''daan', '', NULL, NULL, 23420), -- 20722
(19171, 'deDE', 'Draeneibürger', '', NULL, NULL, 23420), -- 19171
(16768, 'deDE', 'Nurguni', '', 'Handwerkerin', NULL, 23420), -- 16768
(16732, 'deDE', 'Onnis', '', 'Gemischtwaren', NULL, 23420), -- 16732
(16709, 'deDE', 'Cuzi', '', 'Taschenverkäuferin', NULL, 23420), -- 16709
(17201, 'deDE', 'Mondweidenbock', '', NULL, NULL, 23420), -- 17201
(17202, 'deDE', 'Infiziertes Nachtpirscherjunges', '', NULL, NULL, 23420), -- 17202
(17468, 'deDE', 'Prophet Velen', '', NULL, NULL, 23420), -- 17468
(18955, 'deDE', 'Kamerawackeln: 30 - 90 Sekunden', '', NULL, NULL, 23420), -- 18955
(51501, 'deDE', 'Nuri', '', 'Gildenhändlerin', NULL, 23420), -- 51501
(16766, 'deDE', 'Issca', '', 'Verkäuferin für Gildenwappenröcke', NULL, 23420), -- 16766
(16734, 'deDE', 'Funaam', '', 'Gildenmeister', NULL, 23420), -- 16734
(16733, 'deDE', 'Friedensbewahrer der Exodar', '', NULL, 'Directions', 23420), -- 16733
(16706, 'deDE', 'Musal', '', 'Alchemiebedarf & Reagenzien', NULL, 23420), -- 16706
(17514, 'deDE', 'Bati', '', 'Magierlehrerin', NULL, 23420), -- 17514
(16755, 'deDE', 'Lunaraa', '', 'Portallehrerin', NULL, 23420), -- 16755
(17513, 'deDE', 'Harnan', '', 'Magierlehrer', NULL, 23420), -- 17513
(16749, 'deDE', 'Edirah', '', 'Magierlehrerin', NULL, 23420), -- 16749
(16632, 'deDE', 'Oss', '', 'Zauberstabverkäufer', NULL, 23420), -- 16632
(14379, 'deDE', 'Jägerin Rabeneiche', '', NULL, NULL, 23420), -- 14379
(10604, 'deDE', 'Jägerin Nhemai', '', NULL, NULL, 23420), -- 10604
(6091, 'deDE', 'Dellylah', '', 'Speis & Trank', NULL, 23420), -- 6091
(3587, 'deDE', 'Lyrai', '', 'Gemischtwaren', NULL, 23420), -- 3587
(3589, 'deDE', 'Keina', '', 'Bogenmacherin', NULL, 23420), -- 3589
(3588, 'deDE', 'Khardan Klingenstolz', '', 'Waffenschmied', NULL, 23420), -- 3588
(34757, 'deDE', 'Doranel Ammerblatt', '', NULL, NULL, 23420), -- 34757
(34756, 'deDE', 'Moriana Dämmerlicht', '', NULL, NULL, 23420), -- 34756
(3514, 'deDE', 'Tenaron Sturmgewalt', '', NULL, NULL, 23420), -- 3514
(3597, 'deDE', 'Mardant Starkeiche', '', 'Druidenlehrer', NULL, 23420), -- 3597
(49479, 'deDE', 'Dentaria Silbertal', '', 'Mondpriesterin', NULL, 23420), -- 49479
(8584, 'deDE', 'Iverron', '', NULL, NULL, 23420), -- 8584
(7690, 'deDE', 'Gestreifter Nachtsäbler', '', NULL, NULL, 23420), -- 7690
(43006, 'deDE', 'Rhyanda', '', 'Magierlehrerin', NULL, 23420), -- 43006
(3596, 'deDE', 'Ayanna Immerlauf', '', 'Jägerlehrerin', NULL, 23420), -- 3596
(3595, 'deDE', 'Shanda', '', 'Priesterlehrerin', NULL, 23420), -- 3595
(3594, 'deDE', 'Frahun Schattenflüsterer', '', 'Schurkenlehrer', NULL, 23420), -- 3594
(3593, 'deDE', 'Alyissia', '', 'Kriegerlehrerin', NULL, 23420), -- 3593
(3591, 'deDE', 'Freja Nachtschwinge', '', 'Lederrüstungshändlerin', NULL, 23420), -- 3591
(3590, 'deDE', 'Janna Mondenschein', '', 'Tuchmacherin', NULL, 23420), -- 3590
(63331, 'deDE', 'Laoxi', '', 'Mönchslehrer', NULL, 23420), -- 63331
(44617, 'deDE', 'Verwundete Schildwache', '', NULL, NULL, 23420), -- 44617
(3592, 'deDE', 'Andiss', '', 'Rüstungsschmied & Schildmacher', NULL, 23420), -- 3592
(2082, 'deDE', 'Gilshalan Windspäher', '', NULL, NULL, 23420), -- 2082
(8583, 'deDE', 'Dirania Silberschein', '', NULL, NULL, 23420), -- 8583
(2079, 'deDE', 'Ilthalaine', '', NULL, NULL, 23420), -- 2079
(2077, 'deDE', 'Melithar Hirschhaupt', '', NULL, NULL, 23420), -- 2077
(1988, 'deDE', 'Grell', '', NULL, NULL, 23420), -- 1988
(1994, 'deDE', 'Githyiss die Üble', '', NULL, NULL, 23420), -- 1994
(49598, 'deDE', 'Verderbnistotem der Knarzklauen', '', NULL, NULL, 23420), -- 49598
(1986, 'deDE', 'Waldweberspinne', '', NULL, NULL, 23420), -- 1986
(1985, 'deDE', 'Disteleber', '', NULL, NULL, 23420), -- 1985
(2032, 'deDE', 'Räudiger Nachtsäbler', '', NULL, NULL, 23420), -- 2032
(1989, 'deDE', 'Grellkin', '', NULL, NULL, 23420), -- 1989
(44614, 'deDE', 'Trainingsattrappe', '', NULL, NULL, 23420), -- 44614
(1984, 'deDE', 'Junger Disteleber', '', NULL, NULL, 23420), -- 1984
(2031, 'deDE', 'Junger Nachtsäbler', '', NULL, NULL, 23420), -- 2031
(6780, 'deDE', 'Porthannius', '', NULL, NULL, 23420), -- 6780
(12160, 'deDE', 'Schildwache des Laubschattentals', '', NULL, NULL, 23420), -- 12160
(2038, 'deDE', 'Lord Melenas', '', NULL, NULL, 23420), -- 2038
(2004, 'deDE', 'Dunkler Grimmling', '', NULL, NULL, 23420), -- 2004
(2005, 'deDE', 'Heimtückischer Grell', '', NULL, NULL, 23420), -- 2005
(2002, 'deDE', 'Frecher Grimmling', '', NULL, NULL, 23420), -- 2002
(14432, 'deDE', 'Threggil', '', NULL, NULL, 23420), -- 14432
(2003, 'deDE', 'Schattengrimmling', '', NULL, NULL, 23420), -- 2003
(3535, 'deDE', 'Schwarzmoos der Stinker', '', NULL, NULL, 23420), -- 3535
(6094, 'deDE', 'Byancie', '', 'Lehrerin für Erste Hilfe', NULL, 23420), -- 6094
(4266, 'deDE', 'Danlyia', '', 'Speis & Trank', NULL, 23420), -- 4266
(3614, 'deDE', 'Narret Schattenhain', '', 'Handwerkswaren', NULL, 23420), -- 3614
(3608, 'deDE', 'Aldia', '', 'Gemischtwaren', NULL, 23420), -- 3608
(3600, 'deDE', 'Laurna Morgenglanz', '', 'Priesterlehrerin', NULL, 23420), -- 3600
(3567, 'deDE', 'Tallonkai Flinkwurzel', '', NULL, NULL, 23420), -- 3567
(12429, 'deDE', 'Schildwache Shaya', '', NULL, NULL, 23420), -- 12429
(3515, 'deDE', 'Corithras Mondzorn', '', NULL, NULL, 23420), -- 3515
(40553, 'deDE', 'Fidelio', '', 'Hippogryphenmeister', NULL, 23420), -- 40553
(2083, 'deDE', 'Syral Messerblatt', '', NULL, NULL, 23420), -- 2083
(65097, 'deDE', 'Huh', '', 'Kampfhaustier', NULL, 23420), -- 65097
(63070, 'deDE', 'Valeena', '', 'Kampfhaustiertrainerin', NULL, 23420), -- 63070
(6781, 'deDE', 'Melarith', '', NULL, NULL, 23420), -- 6781
(6736, 'deDE', 'Gastwirt Keldamyr', '', 'Gastwirt', NULL, 23420), -- 6736
(3602, 'deDE', 'Kal', '', 'Druidenlehrer', NULL, 23420), -- 3602
(3610, 'deDE', 'Jeena Federbogen', '', 'Bogenmacherin', NULL, 23420), -- 3610
(34761, 'deDE', 'Citarre Ahornherz', '', NULL, NULL, 23420), -- 34761
(2081, 'deDE', 'Schildwache Kyra Sternensang', '', NULL, NULL, 23420), -- 2081
(2078, 'deDE', 'Athridas Bärenfell', '', NULL, NULL, 23420), -- 2078
(3611, 'deDE', 'Brannol Adlermond', '', 'Tuchmacher', NULL, 23420), -- 3611
(3571, 'deDE', 'Schildwache von Teldrassil', '', NULL, 'Directions', 23420), -- 3571
(43005, 'deDE', 'Irriende', '', 'Magierlehrerin', NULL, 23420), -- 43005
(3598, 'deDE', 'Kyra Windklinge', '', 'Kriegerlehrerin', NULL, 23420), -- 3598
(11942, 'deDE', 'Orenthil Wisperwind', '', NULL, NULL, 23420), -- 11942
(3613, 'deDE', 'Meri Eisenweber', '', 'Rüstungsschmiedin & Schildmacherin', NULL, 23420), -- 3613
(6286, 'deDE', 'Zarrin', '', 'Kochkunstlehrer', NULL, 23420), -- 6286
(3599, 'deDE', 'Jannok Windsang', '', 'Schurkenlehrer', NULL, 23420), -- 3599
(3609, 'deDE', 'Shalomon', '', 'Waffenschmied', NULL, 23420), -- 3609
(3612, 'deDE', 'Sinda', '', 'Lederrüstungshändlerin', NULL, 23420), -- 3612
(47420, 'deDE', 'Iranis Schattenblüte', '', 'Berufsausbilder', NULL, 23420), -- 47420
(4265, 'deDE', 'Nyoma', '', 'Kochbedarf', NULL, 23420), -- 4265
(10051, 'deDE', 'Seriadne', '', 'Stallmeisterin', NULL, 23420), -- 10051
(3601, 'deDE', 'Dazalar', '', 'Jägerlehrer', NULL, 23420), -- 3601
(15595, 'deDE', 'Urahne Messerblatt', '', NULL, NULL, 23420), -- 15595
(3306, 'deDE', 'Keldas', '', 'Tierausbilder', NULL, 23420), -- 3306
(3604, 'deDE', 'Malorne Messerblatt', '', 'Kräuterkundelehrer', NULL, 23420), -- 3604
(3603, 'deDE', 'Cyndra Samtwisper', '', 'Alchemielehrerin', NULL, 23420), -- 3603
(2150, 'deDE', 'Zenn Faulhuf', '', NULL, NULL, 23420), -- 2150
(7234, 'deDE', 'Ferocitas der Traumfresser', '', NULL, NULL, 23420), -- 7234
(7235, 'deDE', 'Mystiker der Knarzklauen', '', NULL, NULL, 23420), -- 7235
(14428, 'deDE', 'Uruson', '', NULL, NULL, 23420), -- 14428
(2107, 'deDE', 'Gaerolas Talvethren', '', NULL, NULL, 23420), -- 2107
(2006, 'deDE', 'Ursa der Knarzklauen', '', NULL, NULL, 23420), -- 2006
(2007, 'deDE', 'Gärtner der Knarzklauen', '', NULL, NULL, 23420), -- 2007
(2022, 'deDE', 'Bäumling', '', NULL, NULL, 23420), -- 2022
(14430, 'deDE', 'Dämmerpirscher', '', NULL, NULL, 23420), -- 14430
(2008, 'deDE', 'Krieger der Knarzklauen', '', NULL, NULL, 23420), -- 2008
(2042, 'deDE', 'Nachtsäbler', '', NULL, NULL, 23420), -- 2042
(10606, 'deDE', 'Jägerin Yaeliura', '', NULL, NULL, 23420), -- 10606
(2151, 'deDE', 'Mondpriesterin Amara', '', NULL, NULL, 23420), -- 2151
(7317, 'deDE', 'Oben Zornklaue', '', NULL, NULL, 23420), -- 7317
(7318, 'deDE', 'Zornklaue', '', NULL, NULL, 23420), -- 7318
(2025, 'deDE', 'Bäumlingborkenreißer', '', NULL, NULL, 23420), -- 2025
(2009, 'deDE', 'Schamane der Knarzklauen', '', NULL, NULL, 23420), -- 2009
(2010, 'deDE', 'Verteidiger der Knarzklauen', '', NULL, NULL, 23420), -- 2010
(34521, 'deDE', 'Zweig der Verderbnis', '', 'Überbleibsel von Xavius', NULL, 23420), -- 34521
(34642, 'deDE', 'Army of Corruption Controller', '', NULL, NULL, 23420), -- 34642
(34517, 'deDE', 'Verbündeter der Natur', '', NULL, NULL, 23420), -- 34517
(34524, 'deDE', 'Verderbter Diener', '', NULL, NULL, 23420), -- 34524
(34522, 'deDE', 'Verderbter Diener', '', NULL, NULL, 23420), -- 34522
(34530, 'deDE', 'Uralter Beschützer von Teldrassil', '', NULL, NULL, 23420), -- 34530
(34525, 'deDE', 'Verderbte Knarzklaue', '', NULL, NULL, 23420), -- 34525
(1992, 'deDE', 'Tarindrella', '', NULL, NULL, 23420), -- 1992
(2043, 'deDE', 'Pirschender Nachtsäbler', '', NULL, NULL, 23420), -- 2043
(2011, 'deDE', 'Augur der Knarzklauen', '', NULL, NULL, 23420), -- 2011
(1995, 'deDE', 'Strigideule', '', NULL, NULL, 23420), -- 1995
(1998, 'deDE', 'Waldweberlauerer', '', NULL, NULL, 23420),
(2166, 'deDE', 'Grimmeiche', '', NULL, NULL, 23420), -- 2166
(7313, 'deDE', 'Priesterin A''moora', '', NULL, NULL, 23420), -- 7313
(3519, 'deDE', 'Schildwache Arynia Wolkenbruch', '', NULL, NULL, 23420), -- 3519
(34575, 'deDE', 'Moonwell Bunny', '', NULL, NULL, 23420), -- 34575
(3517, 'deDE', 'Rellian Grünwipfel', '', NULL, NULL, 23420), -- 3517
(2080, 'deDE', 'Denalan', '', NULL, NULL, 23420), -- 2080
(44027, 'deDE', 'Ardan Sanftmond', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 44027
(6287, 'deDE', 'Radnaal Schopfweber', '', 'Kürschnerlehrer', NULL, 23420), -- 6287
(3605, 'deDE', 'Nadyia Schopfweber', '', 'Lederverarbeitungslehrerin', NULL, 23420), -- 3605
(2027, 'deDE', 'Bäumlingtrampler', '', NULL, NULL, 23420), -- 2027
(2029, 'deDE', 'Morastbäumling', '', NULL, NULL, 23420), -- 2029
(2030, 'deDE', 'Alter Bäumling', '', NULL, NULL, 23420), -- 2030
(2001, 'deDE', 'Riesige Waldweberspinne', '', NULL, NULL, 23420), -- 2001
(7319, 'deDE', 'Lady Sathrah', '', NULL, NULL, 23420), -- 7319
(2034, 'deDE', 'Wilder Nachtsäbler', '', NULL, NULL, 23420), -- 2034
(61165, 'deDE', 'Rehkitz', '', NULL, 'wildpetcapturable', 23420), -- 61165
(3568, 'deDE', 'Nebel', '', NULL, NULL, 23420), -- 3568
(2021, 'deDE', 'Matriarchin der Blutfedern', '', NULL, NULL, 23420), -- 2021
(2019, 'deDE', 'Furie der Blutfedern', '', NULL, NULL, 23420), -- 2019
(2020, 'deDE', 'Windhexe der Blutfedern', '', NULL, NULL, 23420), -- 2020
(2000, 'deDE', 'Waldweberseidenspinner', '', NULL, NULL, 23420), -- 2000
(44030, 'deDE', 'Draelan', '', 'Verzauberkunstbedarf', NULL, 23420), -- 44030
(3606, 'deDE', 'Alanna Rabenauge', '', 'Verzauberkunstlehrerin', NULL, 23420), -- 3606
(14431, 'deDE', 'Furie Shelda', '', NULL, NULL, 23420), -- 14431
(2018, 'deDE', 'Zauberin der Blutfedern', '', NULL, NULL, 23420), -- 2018
(2015, 'deDE', 'Harpyie der Blutfedern', '', NULL, NULL, 23420), -- 2015
(2033, 'deDE', 'Alter Nachtsäbler', '', NULL, NULL, 23420), -- 2033
(2017, 'deDE', 'Schurkin der Blutfedern', '', NULL, NULL, 23420), -- 2017
(890, 'deDE', 'Rehkitz', '', NULL, NULL, 23420), -- 890
(15624, 'deDE', 'Waldirrlicht', '', NULL, NULL, 23420), -- 15624
(1997, 'deDE', 'Strigidjäger', '', NULL, NULL, 23420), -- 1997
(1999, 'deDE', 'Waldwebergiftzahn', '', NULL, NULL, 23420), -- 1999
(2155, 'deDE', 'Schildwache Shayla Nachtluft', '', NULL, NULL, 23420), -- 2155
(48624, 'deDE', 'Irrwisch', '', NULL, NULL, 23420), -- 48624
(1996, 'deDE', 'Strigidkreischer', '', NULL, NULL, 23420), -- 1996
(62242, 'deDE', 'Kroneneule', '', NULL, 'wildpetcapturable', 23420), -- 62242
(4235, 'deDE', 'Turian', '', 'Wurfwaffenhändler', NULL, 23420), -- 4235
(4173, 'deDE', 'Landria', '', 'Bogenhändlerin', NULL, 23420), -- 4173
(4233, 'deDE', 'Mythidan', '', 'Streitkolben- und Stabhändler', NULL, 23420), -- 4233
(4232, 'deDE', 'Glorandiir', '', 'Axthändler', NULL, 23420), -- 4232
(4231, 'deDE', 'Kieran', '', 'Waffenhändler', NULL, 23420), -- 4231
(4171, 'deDE', 'Merelyssa', '', 'Klingenhändlerin', NULL, 23420), -- 4171
(14380, 'deDE', 'Jägerin Laubschleicher', '', NULL, NULL, 23420), -- 14380
(4230, 'deDE', 'Yldan', '', 'Taschenhändler', NULL, 23420), -- 4230
(4170, 'deDE', 'Ellandrieth', '', 'Gemischtwaren', NULL, 23420), -- 4170
(15678, 'deDE', 'Auktionator Silva''las', '', NULL, NULL, 23420), -- 15678
(8723, 'deDE', 'Auktionator Golothas', '', NULL, NULL, 23420), -- 8723
(15679, 'deDE', 'Auktionator Cazarez', '', NULL, NULL, 23420), -- 15679
(8669, 'deDE', 'Auktionator Tolon', '', NULL, NULL, 23420), -- 8669
(4164, 'deDE', 'Cylania', '', 'Nachtelfenrüstungsschmiedin', NULL, 23420), -- 4164
(6142, 'deDE', 'Mathiel', '', NULL, NULL, 23420), -- 6142
(52641, 'deDE', 'Layna Karner', '', 'Schmiedekunstbedarf', NULL, 23420), -- 52641
(52640, 'deDE', 'Rolf Karner', '', 'Schmiedekunstlehrer', NULL, 23420), -- 52640
(4203, 'deDE', 'Ariyell Himmelsschatten', '', 'Waffenhändlerin', NULL, 23420), -- 4203
(4089, 'deDE', 'Sildanair', '', 'Kriegerlehrerin', NULL, 23420), -- 4089
(8026, 'deDE', 'Thyn''tel Klingenweber', '', NULL, NULL, 23420), -- 8026
(34989, 'deDE', 'Rissa Schattenblatt', '', 'Kampfmeisterin', NULL, 23420), -- 34989
(4088, 'deDE', 'Elanaria', '', NULL, NULL, 23420), -- 4088
(4160, 'deDE', 'Ainethil', '', 'Alchemielehrerin', NULL, 23420), -- 4160
(34988, 'deDE', 'Landuen Mondklaue', '', 'Kampfmeister', NULL, 23420), -- 34988
(11041, 'deDE', 'Milla Fairancora', '', 'Alchemielehrling', NULL, 23420), -- 11041
(4784, 'deDE', 'Argentumwache Manados', '', 'Die Argentumdämmerung', NULL, 23420), -- 4784
(7315, 'deDE', 'Darnath Klingenlied', '', 'Kriegerlehrer', NULL, 23420), -- 7315
(4786, 'deDE', 'Dämmerungsbehüter Shaedlass', '', 'Die Argentumdämmerung', NULL, 23420), -- 4786
(4087, 'deDE', 'Arias''ta Klingenlied', '', 'Kriegerlehrerin', NULL, 23420), -- 4087
(4226, 'deDE', 'Ulthir', '', 'Alchemiebedarf', NULL, 23420), -- 4226
(11042, 'deDE', 'Sylvanna Mondwald', '', 'Alchemielehrling', NULL, 23420), -- 11042
(4783, 'deDE', 'Dämmerungsbehüter Selgorm', '', 'Die Argentumdämmerung', NULL, 23420), -- 4783
(52645, 'deDE', 'Aessa Silbertau', '', 'Juwelierskunstlehrerin', NULL, 23420), -- 52645
(52644, 'deDE', 'Tarien Silbertau', '', 'Juwelierskunstbedarf', NULL, 23420), -- 52644
(47584, 'deDE', 'Aladrel Weißspitz', '', NULL, NULL, 23420), -- 47584
(30731, 'deDE', 'Illianna Mondschreiber', '', 'Inschriftenkundebedarf', NULL, 23420), -- 30731
(30715, 'deDE', 'Feyden Tintenrot', '', 'Inschriftenkundelehrer', NULL, 23420), -- 30715
(11070, 'deDE', 'Lalina Sommermond', '', 'Verzauberkunstlehrling', NULL, 23420), -- 11070
(4228, 'deDE', 'Vaean', '', 'Verzauberkunstbedarf', NULL, 23420), -- 4228
(4213, 'deDE', 'Taladan', '', 'Verzauberkunstmeister', NULL, 23420), -- 4213
(4229, 'deDE', 'Mythrin''dir', '', 'Handwerkswaren', NULL, 23420), -- 4229
(4159, 'deDE', 'Me''lynn', '', 'Schneiderlehrerin', NULL, 23420), -- 4159
(4225, 'deDE', 'Saenorion', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 4225
(11050, 'deDE', 'Trianna', '', 'Schneiderlehrling', NULL, 23420), -- 11050
(6292, 'deDE', 'Eladriel', '', 'Kürschnerlehrerin', NULL, 23420), -- 6292
(6034, 'deDE', 'Lotherias', '', NULL, NULL, 23420), -- 6034
(11083, 'deDE', 'Darianna', '', 'Lederverarbeitungslehrling', NULL, 23420), -- 11083
(11081, 'deDE', 'Faldron', '', 'Lederverarbeitungslehrling', NULL, 23420), -- 11081
(6735, 'deDE', 'Gastwirtin Saelienne', '', 'Gastwirtin', NULL, 23420), -- 6735
(4212, 'deDE', 'Telonis', '', 'Lederverarbeitungslehrer', NULL, 23420), -- 4212
(4168, 'deDE', 'Elynna', '', 'Schneiderbedarf', NULL, 23420), -- 4168
(5047, 'deDE', 'Ellaercia', '', 'Designerin für Gildenwappenröcke', NULL, 23420), -- 5047
(4161, 'deDE', 'Lysheana', '', 'Gildenmeisterin', NULL, 23420), -- 4161
(51504, 'deDE', 'Velia Mondbogen', '', 'Gildenhändlerin', NULL, 23420), -- 51504
(5191, 'deDE', 'Shalumon', '', 'Verkäufer für Gildenwappenröcke', NULL, 23420), -- 5191
(3469, 'deDE', 'Urtum des Krieges', '', NULL, NULL, 23420), -- 3469
(8665, 'deDE', 'Shylenai', '', 'Eulenausbilderin', NULL, 23420), -- 8665
(7555, 'deDE', 'Sperbereule', '', NULL, NULL, 23420), -- 7555
(7553, 'deDE', 'Großer Uhu', '', NULL, NULL, 23420), -- 7553
(4241, 'deDE', 'Mydrannul', '', 'Gemischtwaren', NULL, 23420), -- 4241
(4181, 'deDE', 'Fyrenna', '', 'Speis & Trank', NULL, 23420), -- 4181
(48623, 'deDE', 'Irrwisch', '', NULL, NULL, 23420), -- 48623
(4169, 'deDE', 'Jaeana', '', 'Fleischverkäuferin', NULL, 23420), -- 4169
(11709, 'deDE', 'Jareth Wilderwald', '', NULL, NULL, 23420), -- 11709
(4521, 'deDE', 'Treshala Bachquell', '', NULL, NULL, 23420), -- 4521
(4180, 'deDE', 'Ealyshia Wispertau', '', 'Zweihandwaffenhändlerin', NULL, 23420), -- 4180
(4175, 'deDE', 'Vinasia', '', 'Stoffrüstungshändlerin', NULL, 23420), -- 4175
(48735, 'deDE', 'Gwen Armstead', '', 'Gastwirtin', NULL, 23420), -- 48735
(50506, 'deDE', 'Talran aus der Wildnis', '', 'Druidenlehrer', NULL, 23420), -- 50506
(50502, 'deDE', 'Vitus Dunkelwandler', '', 'Hexenmeisterlehrer', NULL, 23420), -- 50502
(50499, 'deDE', 'Myriam Zauberwache', '', 'Magierlehrerin', NULL, 23420), -- 50499
(50498, 'deDE', 'Loren die Hehlerin', '', 'Schurkenlehrerin', NULL, 23420), -- 50498
(36479, 'deDE', 'Erzmagier Mordent Schattenfall', '', 'Die Hochgeborenen', NULL, 23420), -- 36479
(50518, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50518
(50519, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50519
(50520, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50520
(50507, 'deDE', 'Vassandra Sturmklaue', '', 'Druidenlehrerin', NULL, 23420), -- 50507
(50516, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50516
(50517, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50517
(50521, 'deDE', 'Gilnearischer Flüchtling', '', NULL, NULL, 23420), -- 50521
(50509, 'deDE', 'Jenn Stravaganza', '', NULL, NULL, 23420), -- 50509
(50505, 'deDE', 'Lyros Flinkwind', '', 'Druidenlehrer', NULL, 23420), -- 50505
(50510, 'deDE', 'Rachel DeSimone', '', NULL, NULL, 23420), -- 50510
(50508, 'deDE', 'Carrie Eileen Steen', '', NULL, NULL, 23420), -- 50508
(50513, 'deDE', 'Jamie Harriott', '', NULL, NULL, 23420), -- 50513
(50501, 'deDE', 'Schwester Almyra', '', 'Priesterlehrerin', NULL, 23420), -- 50501
(50504, 'deDE', 'Belysra Sternenhauch', '', 'Mondpriesterin', NULL, 23420), -- 50504
(50500, 'deDE', 'Unteroffizier Cleese', '', 'Kriegerlehrer', NULL, 23420), -- 50500
(50497, 'deDE', 'Jäger Blake', '', 'Jägerlehrer', NULL, 23420), -- 50497
(55285, 'deDE', 'Astrid Langstrumpf', '', 'Bergpferdehändlerin', NULL, 23420), -- 55285
(4211, 'deDE', 'Dannelor', '', 'Lehrer für Erste Hilfe', NULL, 23420), -- 4211
(3468, 'deDE', 'Urtum der Lehren', '', NULL, NULL, 23420), -- 3468
(55272, 'deDE', 'Bergpferd', '', NULL, NULL, 23420), -- 55272
(52642, 'deDE', 'Großknecht Pernic', '', 'Bergbaulehrer', NULL, 23420), -- 52642
(52643, 'deDE', 'Rissa Halding', '', 'Bergbaubedarf', NULL, 23420), -- 52643
(55273, 'deDE', 'Schnelles Bergpferd', '', NULL, NULL, 23420), -- 55273
(52637, 'deDE', 'Hugo Lentner', '', 'Ingenieursbedarf', NULL, 23420), -- 52637
(52636, 'deDE', 'Tana Lentner', '', 'Ingenieurskunstlehrerin', NULL, 23420), -- 52636
(4243, 'deDE', 'Nachtschatten', '', 'Jeen''ras Tier', NULL, 23420), -- 4243
(4138, 'deDE', 'Jeen''ra Nachtläufer', '', 'Jägerlehrerin', NULL, 23420), -- 4138
(9047, 'deDE', 'Jenal', '', 'Grabenbuddler', NULL, 23420), -- 9047
(4244, 'deDE', 'Schatten', '', 'Dorions Tier', NULL, 23420), -- 4244
(4205, 'deDE', 'Dorion', '', 'Jägerlehrer', NULL, 23420), -- 4205
(10089, 'deDE', 'Silvaria', '', 'Tierausbilderin', NULL, 23420), -- 10089
(4219, 'deDE', 'Fylerian Nachtschwinge', '', 'Druidenlehrer', NULL, 23420), -- 4219
(4218, 'deDE', 'Denatharion', '', 'Druidenlehrer', NULL, 23420), -- 4218
(7296, 'deDE', 'Corand', '', NULL, NULL, 23420), -- 7296
(4217, 'deDE', 'Mathrengyl Bärenfährte', '', 'Druidenlehrer', NULL, 23420), -- 4217
(4220, 'deDE', 'Cyroen', '', 'Reagenzien', NULL, 23420), -- 4220
(4146, 'deDE', 'Jocaste', '', 'Jägerlehrerin', NULL, 23420), -- 4146
(10056, 'deDE', 'Alassin', '', 'Stallmeister', NULL, 23420), -- 10056
(15598, 'deDE', 'Urahne Klingentanz', '', NULL, NULL, 23420), -- 15598
(3561, 'deDE', 'Kyrai', '', 'Gifte', NULL, 23420), -- 3561
(2796, 'deDE', 'Faelyssa', '', NULL, NULL, 23420), -- 2796
(4210, 'deDE', 'Alegorn', '', 'Kochkunstlehrer', NULL, 23420), -- 4210
(4223, 'deDE', 'Fyldan', '', 'Kochbedarf', NULL, 23420), -- 4223
(14602, 'deDE', 'Schneller Sturmsäbler', '', NULL, NULL, 23420), -- 14602
(35168, 'deDE', 'Gestreifter Dämmersäbler', '', NULL, NULL, 23420), -- 35168
(12358, 'deDE', 'Gestreifter Frostsäbler', '', NULL, NULL, 23420), -- 12358
(14556, 'deDE', 'Schneller Frostsäbler', '', NULL, NULL, 23420), -- 14556
(12360, 'deDE', 'Gestreifter Nachtsäbler', '', NULL, NULL, 23420), -- 12360
(4730, 'deDE', 'Lelanai', '', 'Säblerführerin', NULL, 23420), -- 4730
(4167, 'deDE', 'Dendrythis', '', 'Speis & Trank', NULL, 23420), -- 4167
(4753, 'deDE', 'Jartsam', '', 'Reitlehrer', NULL, 23420), -- 4753
(14555, 'deDE', 'Schneller Schattensäbler', '', NULL, NULL, 23420), -- 14555
(12359, 'deDE', 'Gefleckter Frostsäbler', '', NULL, NULL, 23420), -- 12359
(4215, 'deDE', 'Anishar', '', 'Schurkenlehrer', NULL, 23420), -- 4215
(4163, 'deDE', 'Syurna', '', 'Schurkenlehrerin', NULL, 23420), -- 4163
(5782, 'deDE', 'Crildor', '', NULL, NULL, 23420), -- 5782
(4423, 'deDE', 'Beschützer von Darnassus', '', NULL, NULL, 23420), -- 4423
(4214, 'deDE', 'Erion Schattenflüsterer', '', 'Schurkenlehrer', NULL, 23420), -- 4214
(4156, 'deDE', 'Astaia', '', 'Angellehrerin', NULL, 23420), -- 4156
(4216, 'deDE', 'Chardryn', '', 'Kräuterkundebedarf', NULL, 23420), -- 4216
(10878, 'deDE', 'Herold Mondpirscher', '', NULL, NULL, 23420), -- 10878
(4240, 'deDE', 'Caynrus', '', 'Schildhändler', NULL, 23420), -- 4240
(4177, 'deDE', 'Melea', '', 'Händlerin für Kettenrüstungen', NULL, 23420), -- 4177
(2041, 'deDE', 'Uralter Beschützer', '', NULL, NULL, 23420), -- 2041
(4236, 'deDE', 'Cyridan', '', 'Lederrüstungshändler', NULL, 23420), -- 4236
(4234, 'deDE', 'Andrus', '', 'Stabhändler', NULL, 23420), -- 4234
(4172, 'deDE', 'Anadyia', '', 'Robenverkäuferin', NULL, 23420), -- 4172
(4242, 'deDE', 'Frostsäblergefährtin', '', NULL, NULL, 23420), -- 4242
(36506, 'deDE', 'Daros Mondlanze', '', 'Die Hochgeborenen', NULL, 23420), -- 36506
(47569, 'deDE', 'Hammon der Ermattete', '', 'Archäologielehrer', NULL, 23420), -- 47569
(8396, 'deDE', 'Schildwache Dalia Sonnenklinge', '', NULL, NULL, 23420), -- 8396
(4092, 'deDE', 'Lariia', '', 'Priesterlehrerin', NULL, 23420), -- 4092
(50715, 'deDE', 'Maelir', '', 'Magierlehrer', NULL, 23420), -- 50715
(50714, 'deDE', 'Dyrhara', '', 'Magierlehrerin', NULL, 23420), -- 50714
(50690, 'deDE', 'Tarelvir', '', 'Magierlehrer', NULL, 23420), -- 50690
(2912, 'deDE', 'Chefarchäologe Grauflaum', '', 'Forscherliga', NULL, 23420), -- 2912
(7740, 'deDE', 'Gracina Glaubensmacht', '', NULL, NULL, 23420), -- 7740
(61757, 'deDE', 'Rotschwänziges Streifenhörnchen', '', NULL, 'wildpetcapturable', 23420), -- 61757
(11401, 'deDE', 'Priesterin Alathea', '', 'Priesterlehrerin', NULL, 23420), -- 11401
(52292, 'deDE', 'Droha', '', 'Schamanenlehrer', NULL, 23420), -- 52292
(4091, 'deDE', 'Jandria', '', 'Priesterlehrerin', NULL, 23420), -- 4091
(35281, 'deDE', 'Rukua', '', 'Paladinlehrerin', NULL, 23420), -- 35281
(4204, 'deDE', 'Firodren Mondrufer', '', 'Kräuterkundelehrer', NULL, 23420), -- 4204
(43845, 'deDE', 'Malfurion Sturmgrimm', '', NULL, NULL, 23420), -- 43845
(42936, 'deDE', 'Schildwache Cordressa Dornenbogen', '', NULL, NULL, 23420), -- 42936
(17105, 'deDE', 'Abgesandte Valustraa', '', NULL, NULL, 23420), -- 17105
(4165, 'deDE', 'Elissa Dumas', '', 'Portallehrerin', NULL, 23420), -- 4165
(4090, 'deDE', 'Astarii Sternsucher', '', 'Priesterlehrerin', NULL, 23420), -- 4090
(54116, 'deDE', 'Dori''thur', '', 'Tyrandes Begleiter', NULL, 23420), -- 54116
(7999, 'deDE', 'Tyrande Wisperwind', '', 'Hohepriesterin von Elune', NULL, 23420), -- 7999
(4221, 'deDE', 'Talaelar', '', 'Fischverkäufer', NULL, 23420), -- 4221
(62450, 'deDE', 'Lanfen, Brauerin der feinen Tees', '', 'Mönchslehrerin', NULL, 23420), -- 62450
(4222, 'deDE', 'Voloren', '', 'Angelbedarf', NULL, 23420), -- 4222
(15892, 'deDE', 'Abgesandter des Mondfests', '', NULL, NULL, 23420), -- 15892
(19173, 'deDE', 'Nachtelfenbürger', '', NULL, NULL, 23420), -- 19173
(14378, 'deDE', 'Jägerin Himmelshaar', '', NULL, NULL, 23420), -- 14378
(51371, 'deDE', 'Hippogryphenreiter von Darnassus', '', NULL, NULL, 23420), -- 51371
(50307, 'deDE', 'Lord Candren', '', 'Rüstmeister von Gilneas', NULL, 23420), -- 50307
(50305, 'deDE', 'Mondpriesterin Lasara', '', 'Rüstmeisterin von Darnassus', NULL, 23420), -- 50305
(40552, 'deDE', 'Leora', '', 'Hippogryphenmeisterin', NULL, 23420), -- 40552
(11700, 'deDE', 'Sarin Sternenfeuer', '', NULL, NULL, 23420), -- 11700
(7316, 'deDE', 'Schwester Aquinne', '', 'Priester-Novizin', NULL, 23420), -- 7316
(4209, 'deDE', 'Garryeth', '', 'Bankier', NULL, 23420), -- 4209
(4208, 'deDE', 'Lairn', '', 'Bankier', NULL, 23420), -- 4208
(4155, 'deDE', 'Idriana', '', 'Bankierin', NULL, 23420), -- 4155
(3681, 'deDE', 'Irrwisch', '', NULL, NULL, 23420), -- 3681
(3562, 'deDE', 'Alaindia', '', 'Reagenzien', NULL, 23420), -- 3562
(10118, 'deDE', 'Nessa Schattensang', '', 'Angelbedarf', NULL, 23420), -- 10118
(3607, 'deDE', 'Androl Eichenhand', '', 'Angellehrer', NULL, 23420), -- 3607
(7907, 'deDE', 'Daryn Leuchtwind', '', 'Bewahrerin der Cenarischen Lehren', NULL, 23420), -- 7907
(34295, 'deDE', 'Lord Magmathar', '', NULL, NULL, 23420), -- 34295
(3818, 'deDE', 'Alter Schattenhornhirsch', '', NULL, NULL, 23420), -- 3818
(39254, 'deDE', 'Schildwache der Sternenstaubspitze', '', NULL, NULL, 23420), -- 39254
(11074, 'deDE', 'Hgarth', '', 'Verzauberkunstlehrer', NULL, 23420), -- 11074
(11914, 'deDE', 'Narbenhuf der Schwarze', '', NULL, NULL, 23420), -- 11914
(11858, 'deDE', 'Grundig Düsterwolke', '', 'Häuptling', NULL, 23420), -- 11858
(3411, 'deDE', 'Denni''ka', '', 'Metzgerin', NULL, 23420), -- 3411
(4197, 'deDE', 'Ken''zigla', '', NULL, NULL, 23420), -- 4197
(42023, 'deDE', 'Unterwerfer Devo', '', NULL, NULL, 23420), -- 42023
(12816, 'deDE', 'Xen''Zilla', '', NULL, NULL, 23420), -- 12816
(3995, 'deDE', 'Hexendoktor Jin''Zil', '', NULL, NULL, 23420), -- 3995
(35139, 'deDE', 'Zillane', '', 'Flugmeister', NULL, 23420), -- 35139
(41370, 'deDE', 'General Vol''tar', '', NULL, NULL, 23420), -- 41370
(50825, 'deDE', 'Feras', '', NULL, NULL, 23420), -- 50825
(51062, 'deDE', 'Khep-Re', '', NULL, NULL, 23420), -- 51062
(41407, 'deDE', 'Blutwache von Krom''gar', '', NULL, NULL, 23420), -- 41407
(41350, 'deDE', 'Ton Windbogen', '', 'Botschafter der Grimmtotems', NULL, 23420), -- 41350
(11913, 'deDE', 'Zauberer der Grimmtotems', '', NULL, NULL, 23420), -- 11913
(11910, 'deDE', 'Grobian der Grimmtotems', '', NULL, NULL, 23420), -- 11910
(41360, 'deDE', 'Marodeur von Krom''gar', '', NULL, NULL, 23420), -- 41360
(11912, 'deDE', 'Schläger der Grimmtotems', '', NULL, NULL, 23420), -- 11912
(11911, 'deDE', 'Söldner der Grimmtotems', '', NULL, NULL, 23420), -- 11911
(41230, 'deDE', 'Scharfschütze von Krom''gar', '', NULL, NULL, 23420), -- 41230
(11821, 'deDE', 'Scholli Klauengriff', '', NULL, NULL, 23420), -- 11821
(11915, 'deDE', 'Steinschlagfelsbewahrer', '', NULL, NULL, 23420), -- 11915
(4008, 'deDE', 'Klippenstürmer', '', NULL, NULL, 23420), -- 4008
(50759, 'deDE', 'Iriss die Witwe', '', NULL, NULL, 23420), -- 50759
(61327, 'deDE', 'Spinne', '', NULL, 'wildpetcapturable', 23420), -- 61327
(14881, 'deDE', 'Spinne', '', NULL, NULL, 23420), -- 14881
(62191, 'deDE', 'Giftspuckerjungtier', '', NULL, 'wildpetcapturable', 23420), -- 62191
(66335, 'deDE', 'Boa-Ej', '', NULL, NULL, 23420),
(66334, 'deDE', 'Mufforon', '', NULL, NULL, 23420), -- 66334
(66333, 'deDE', 'Säurus', '', NULL, NULL, 23420), -- 66333
(66137, 'deDE', 'Zonya die Sadistin', '', 'Meistertierzähmerin', NULL, 23420), -- 66137
(41186, 'deDE', 'Blaustahl', '', 'Kosaks treuer Begleiter', 'vehichleCursor', 23420), -- 41186
(41316, 'deDE', 'Support Clamp Trigger 03', '', NULL, NULL, 23420), -- 41316
(41311, 'deDE', 'Meisterassassine Kel''istra', '', NULL, NULL, 23420), -- 41311
(41351, 'deDE', 'Leibwächter der Grimmtotems', '', NULL, NULL, 23420), -- 41351
(35136, 'deDE', 'Kaluna Singflug', '', 'Flugmeisterin', NULL, 23420), -- 35136
(41397, 'deDE', 'Kräckas Feuertopf', '', NULL, NULL, 23420), -- 41397
(41278, 'deDE', '"Kräcka" Aschebäscha', '', 'Küchenchef', NULL, 23420), -- 41278
(41229, 'deDE', 'Truppenkommandant Valen', '', NULL, NULL, 23420), -- 41229
(41286, 'deDE', 'Lyanath', '', 'Gastwirtin', NULL, 23420), -- 41286
(41275, 'deDE', 'Forscherin Tabby Triloc', '', 'Händlerin', NULL, 23420), -- 41275
(41274, 'deDE', 'Hauptforscher Jansun', '', 'Schmied', NULL, 23420), -- 41274
(42026, 'deDE', 'Gebändigter Kobold', '', NULL, NULL, 23420), -- 42026
(41361, 'deDE', 'Infanterist der Nordwacht', '', NULL, NULL, 23420), -- 41361
(41408, 'deDE', 'Goblinischer Akkordminenarbeiter', '', NULL, NULL, 23420), -- 41408
(34843, 'deDE', 'Zwergischer Ausgräber', '', 'Forscherliga', NULL, 23420), -- 34843
(38636, 'deDE', 'Wache von Nordwacht', '', NULL, NULL, 23420), -- 38636
(41282, 'deDE', 'Korporal Wocard', '', NULL, NULL, 23420), -- 41282
(41277, 'deDE', 'Leutnant Paulson', '', NULL, NULL, 23420), -- 41277
(41276, 'deDE', 'Stahlzeh McGee', '', 'Sprengmeister', NULL, 23420), -- 41276
(41291, 'deDE', 'Auftragsmörder von Krom''gar', '', NULL, NULL, 23420), -- 41291
(41318, 'deDE', 'Cave In Triggers', '', NULL, NULL, 23420), -- 41318
(41314, 'deDE', 'Support Clamp Trigger 01', '', NULL, NULL, 23420), -- 41314
(41315, 'deDE', 'Support Clamp Trigger 02', '', NULL, NULL, 23420), -- 41315
(41317, 'deDE', 'Support Clamp Trigger 04', '', NULL, NULL, 23420), -- 41317
(41283, 'deDE', 'Verängstigter Minenarbeiter', '', NULL, 'Inspect', 23420), -- 41283
(41992, 'deDE', 'Sprengenieur Igore', '', NULL, NULL, 23420), -- 41992
(41991, 'deDE', 'Unteroffizier Dontrag', '', NULL, NULL, 23420), -- 41991
(41990, 'deDE', 'Späher Utvoch', '', NULL, NULL, 23420), -- 41990
(4489, 'deDE', 'Braug Dämmergeist', '', NULL, NULL, 23420), -- 4489
(51820, 'deDE', 'Vollstrecker von Krom''gar', '', NULL, NULL, 23420), -- 51820
(41854, 'deDE', 'Wegelagerin der Schildwache', '', NULL, NULL, 23420), -- 41854
(43002, 'deDE', 'Bastia', '', 'Kilag Blutmauls Begleiter', NULL, 23420), -- 43002
(41676, 'deDE', 'Ya''mon', '', 'Händler', NULL, 23420), -- 41676
(41675, 'deDE', 'Vernal der Flicker', '', 'Ingenieur', NULL, 23420), -- 41675
(41674, 'deDE', 'Taluka der Jäger', '', 'Speis & Trank', NULL, 23420), -- 41674
(40926, 'deDE', 'Kilag Blutmaul', '', NULL, NULL, 23420), -- 40926
(39257, 'deDE', 'Bogenschützin der Sternenstaubspitze', '', NULL, NULL, 23420), -- 39257
(39256, 'deDE', 'Kalen Freipfeil', '', NULL, NULL, 23420), -- 39256
(40993, 'deDE', 'Der Gnomcorder', '', NULL, NULL, 23420), -- 40993
(34341, 'deDE', 'Saurboz', '', 'Der Einäscherer', NULL, 23420), -- 34341
(32254, 'deDE', 'Heckenschützenkanone', '', NULL, 'Gunner', 23420), -- 32254
(41673, 'deDE', 'Vollstrecker von Krom''gar', '', NULL, NULL, 23420), -- 41673
(41064, 'deDE', 'Felskiefer', '', NULL, NULL, 23420), -- 41064
(40957, 'deDE', 'Vorrat des Kriegshymnenklans', '', NULL, NULL, 23420), -- 40957
(34353, 'deDE', 'Verbrenner von Krom''gar', '', NULL, NULL, 23420), -- 34353
(34345, 'deDE', 'Ölkanister', '', NULL, NULL, 23420), -- 34345
(34834, 'deDE', 'Tree MoveTo Trigger', '', NULL, NULL, 23420), -- 34834
(34795, 'deDE', 'Tree Fire Trigger', '', NULL, NULL, 23420), -- 34795
(40901, 'deDE', 'Sprengenieur Fusselpfiff', '', NULL, NULL, 23420), -- 40901
(41865, 'deDE', 'Späher von Darnassus', '', NULL, NULL, 23420), -- 41865
(41864, 'deDE', 'Jägerin Davinia', '', NULL, NULL, 23420), -- 41864
(40881, 'deDE', 'Minx', '', NULL, NULL, 23420), -- 40881
(51827, 'deDE', 'Vollstrecker von Krom''gar', '', NULL, NULL, 23420), -- 51827
(41993, 'deDE', 'Tobender Erdelementar', '', NULL, NULL, 23420), -- 41993
(41989, 'deDE', 'Verängstigter Peon', '', NULL, 'Inspect', 23420), -- 41989
(41892, 'deDE', 'Felonius Stark', '', 'Gastwirt', NULL, 23420), -- 41892
(41890, 'deDE', 'Barshuk Schwerhammer', '', 'Schmied', NULL, 23420), -- 41890
(40909, 'deDE', 'Rosie', '', NULL, NULL, 23420), -- 40909
(40907, 'deDE', 'Clarissa', '', NULL, NULL, 23420), -- 40907
(41893, 'deDE', 'Gelbin', '', 'Stallmeister', NULL, 23420), -- 41893
(41891, 'deDE', 'Mirkin', '', 'Händler', NULL, 23420), -- 41891
(42028, 'deDE', 'Rüstmeister von Krom''gar', '', 'Rüstmeister', NULL, 23420), -- 42028
(35140, 'deDE', 'Kormal der Geschwinde', '', 'Flugmeister', NULL, 23420), -- 35140
(41023, 'deDE', 'Oberanführer Krom''gar', '', NULL, NULL, 23420), -- 41023
(41677, 'deDE', 'Grunzer von Krom''gar', '', NULL, NULL, 23420), -- 41677
(40902, 'deDE', 'Obersprengenieurin Bombgutz', '', NULL, NULL, 23420), -- 40902
(40903, 'deDE', 'Herrin der Spione Anara', '', NULL, NULL, 23420), -- 40903
(41071, 'deDE', 'Jägerin Illiona', '', NULL, NULL, 23420), -- 41071
(41062, 'deDE', 'Infanterist von Krom''gar', '', NULL, NULL, 23420), -- 41062
(41070, 'deDE', 'Gefangene Jägerin', '', NULL, NULL, 23420), -- 41070
(40973, 'deDE', 'Schildwache Heliana', '', NULL, NULL, 23420), -- 40973
(40879, 'deDE', 'Boog der "Zahnradflüsterer"', '', NULL, NULL, 23420), -- 40879
(40984, 'deDE', 'Heißluftballon von Krom''gar', '', NULL, NULL, 23420), -- 40984
(40988, 'deDE', 'Die Bombe', '', NULL, NULL, 23420), -- 40988
(40904, 'deDE', 'Peon von Krom''gar', '', NULL, NULL, 23420), -- 40904
(40905, 'deDE', 'Vorarbeiter der Scherwindmine', '', NULL, NULL, 23420), -- 40905
(34832, 'deDE', 'Kriegsreiter des Kriegshymnenklans', '', NULL, NULL, 23420), -- 34832
(40942, 'deDE', 'Schwadronskommandant des Kriegshymnenklans', '', NULL, NULL, 23420), -- 40942
(41937, 'deDE', 'Marschall Paltrow', '', NULL, NULL, 23420), -- 41937
(41936, 'deDE', 'Northwatch Tent Quest Credit', '', NULL, NULL, 23420), -- 41936
(41935, 'deDE', 'Eindringling der Nordwacht', '', NULL, NULL, 23420), -- 41935
(42029, 'deDE', 'Strickleiter', '', NULL, 'vehichleCursor', 23420), -- 42029
(41987, 'deDE', 'Heißluftballon von Krom''gar', '', NULL, NULL, 23420), -- 41987
(41894, 'deDE', 'Schwere Goblinkanone', '', NULL, NULL, 23420), -- 41894
(35204, 'deDE', 'Goblinernter', '', NULL, NULL, 23420), -- 35204
(35333, 'deDE', 'Goblintechniker', '', NULL, NULL, 23420), -- 35333
(35137, 'deDE', 'Allana Schnellschwinge', '', 'Flugmeisterin', NULL, 23420), -- 35137
(40910, 'deDE', 'Baunzer', '', NULL, NULL, 23420), -- 40910
(40908, 'deDE', 'Alice', '', NULL, NULL, 23420), -- 40908
(40915, 'deDE', 'Anderov Ryon', '', NULL, NULL, 23420), -- 40915
(35203, 'deDE', 'Gnombotstampfer', '', NULL, NULL, 23420), -- 35203
(35205, 'deDE', 'Gnomischer Pilot', '', NULL, NULL, 23420), -- 35205
(40900, 'deDE', 'Neophytin Sternenkranz', '', NULL, NULL, 23420), -- 40900
(41054, 'deDE', 'Der alte Eisenbock', '', 'Schmied', NULL, 23420), -- 41054
(40899, 'deDE', 'Arkanist Valdurian', '', NULL, NULL, 23420), -- 40899
(43021, 'deDE', 'Adrius', '', 'Stallmeister', NULL, 23420), -- 43021
(41053, 'deDE', 'Ryan Mühlens', '', 'Händler', NULL, 23420), -- 41053
(40898, 'deDE', 'Alithia Brachweiher', '', 'Gastwirtin', NULL, 23420), -- 40898
(35334, 'deDE', 'Gnomeningenieur', '', NULL, NULL, 23420), -- 35334
(41047, 'deDE', 'Verletzter Gnomeningenieur', '', NULL, NULL, 23420), -- 41047
(40897, 'deDE', 'Hauptmann der Nordwacht Kosak', '', NULL, NULL, 23420), -- 40897
(40896, 'deDE', 'Lord Brachweiher', '', NULL, NULL, 23420), -- 40896
(41039, 'deDE', 'Wächter der Scherwindfeste', '', NULL, NULL, 23420), -- 41039
(50343, 'deDE', 'Quall', '', NULL, NULL, 23420), -- 50343
(34866, 'deDE', 'Eingesponnenes Kaninchen', '', NULL, NULL, 23420), -- 34866
(4006, 'deDE', 'Kluftmoosnetzweber', '', NULL, NULL, 23420), -- 4006
(41185, 'deDE', 'Königin Silith', '', NULL, NULL, 23420), -- 41185
(4007, 'deDE', 'Kluftmoosgiftspucker', '', NULL, NULL, 23420), -- 4007
(50812, 'deDE', 'Arae', '', NULL, NULL, 23420), -- 50812
(12043, 'deDE', 'Kulwia', '', 'Handwerkswaren', NULL, 23420), -- 12043
(9549, 'deDE', 'Borand', '', 'Bogenmacher', NULL, 23420), -- 9549
(5870, 'deDE', 'Krond', '', 'Metzger', NULL, 23420), -- 5870
(4312, 'deDE', 'Tharm', '', 'Windreitermeister', NULL, 23420), -- 4312
(4082, 'deDE', 'Grawnal', '', 'Gemischtwaren', NULL, 23420), -- 4082
(7731, 'deDE', 'Gastwirt Jayka', '', 'Gastwirt', NULL, 23420), -- 7731
(10048, 'deDE', 'Gereck', '', 'Stallmeister', NULL, 23420), -- 10048
(4083, 'deDE', 'Jeeda', '', 'Hexendoktorlehrling', NULL, 23420), -- 4083
(50884, 'deDE', 'Staubflügel der Feigling', '', NULL, NULL, 23420), -- 50884
(34946, 'deDE', 'Händlerin Kendra', '', NULL, NULL, 23420), -- 34946
(34933, 'deDE', 'Lady Benel''derath', '', NULL, NULL, 23420), -- 34933
(34934, 'deDE', 'Lord Ro''minate', '', NULL, NULL, 23420), -- 34934
(34941, 'deDE', 'Allendril Bachsturz', '', NULL, NULL, 23420), -- 34941
(34940, 'deDE', 'Illyanna Steinholz', '', NULL, NULL, 23420), -- 34940
(34945, 'deDE', 'Hochgeborene Dame', '', NULL, NULL, 23420), -- 34945
(34939, 'deDE', 'Wache Belothiel', '', 'Hauptmann der Stadtwache', NULL, 23420), -- 34939
(34938, 'deDE', 'Hochgeborener Bürger', '', NULL, NULL, 23420), -- 34938
(34937, 'deDE', 'Ratsmitglied Stark I''ylar', '', NULL, NULL, 23420), -- 34937
(34932, 'deDE', 'Hochgeborener Zauberer', '', NULL, NULL, 23420), -- 34932
(34931, 'deDE', 'Hochgeborener Scharlatan', '', NULL, NULL, 23420), -- 34931
(42035, 'deDE', 'Getarnter Spion der Allianz', '', NULL, NULL, 23420), -- 42035
(42033, 'deDE', 'Jibbly Rakit', '', 'Aufseher der Schlickwerke', NULL, 23420), -- 42033
(42032, 'deDE', 'Vollstrecker der Schlickwerke', '', NULL, NULL, 23420), -- 42032
(41434, 'deDE', '"Goblin" Pumpenkontrolleur', '', NULL, NULL, 23420), -- 41434
(41246, 'deDE', 'Flok', '', 'Flugmeister', NULL, 23420), -- 41246
(4014, 'deDE', 'Prachtschwingenkonsort', '', NULL, NULL, 23420), -- 4014
(4012, 'deDE', 'Prachtschwingenwyvern', '', NULL, NULL, 23420), -- 4012
(41441, 'deDE', 'Herrin der Späher Yvonia', '', NULL, NULL, 23420), -- 41441
(41233, 'deDE', 'Späherkommandant Barus', '', NULL, NULL, 23420), -- 41233
(41432, 'deDE', 'Späher des Mirkfallonpostens', '', NULL, NULL, 23420), -- 41432
(41240, 'deDE', 'Fiora Mondsegler', '', 'Flugmeisterin', NULL, 23420), -- 41240
(41437, 'deDE', 'Goblinpumpenkontrolleur', '', NULL, NULL, 23420), -- 41437
(40906, 'deDE', 'Trampel', '', NULL, NULL, 23420), -- 40906
(41475, 'deDE', 'Ölpumpe', '', NULL, NULL, 23420), -- 41475
(5928, 'deDE', 'Trauerschwinge', '', NULL, NULL, 23420), -- 5928
(34818, 'deDE', 'Kriegsurtum des Steinkrallengebirges', '', NULL, NULL, 23420), -- 34818
(34817, 'deDE', 'Kriegsurtum des Steinkrallengebirges', '', NULL, NULL, 23420), -- 34817
(34807, 'deDE', 'Hexendoktor von Malaka''jin', '', NULL, NULL, 23420), -- 34807
(35141, 'deDE', 'Orna Himmelsschauer', '', 'Flugmeisterin', NULL, 23420), -- 35141
(34803, 'deDE', 'Kriegsurtum des Steinkrallengebirges', '', NULL, NULL, 23420), -- 34803
(42050, 'deDE', 'General Grebo', '', NULL, NULL, 23420), -- 42050
(42047, 'deDE', 'Masha Klippenläufer', '', NULL, NULL, 23420), -- 42047
(42039, 'deDE', 'Oberhäuptling Klippenläufer', '', NULL, NULL, 23420), -- 42039
(41870, 'deDE', 'Vollstrecker von Krom''gar', '', NULL, NULL, 23420), -- 41870
(42040, 'deDE', 'Kriegerheld der Klippenläufer', '', NULL, NULL, 23420), -- 42040
(34805, 'deDE', 'Steintotemscharfschütze', '', NULL, NULL, 23420), -- 34805
(34809, 'deDE', 'Grunzer des Steinkrallengebirges', '', NULL, NULL, 23420), -- 34809
(34811, 'deDE', 'Rollender Fels', '', NULL, NULL, 23420), -- 34811
(41490, 'deDE', 'Donald Gutgeschäft', '', 'Händler', NULL, 23420), -- 41490
(41493, 'deDE', 'Poppy', '', 'Ingenieur', NULL, 23420), -- 41493
(43017, 'deDE', 'Fahlestad', '', 'Stallmeister', NULL, 23420), -- 43017
(41491, 'deDE', 'Valos Schattenstille', '', 'Gastwirt', NULL, 23420), -- 41491
(41489, 'deDE', 'Mirin', '', NULL, NULL, 23420), -- 41489
(41488, 'deDE', 'Hauptmann der Schildwache Geleneth', '', NULL, NULL, 23420), -- 41488
(41486, 'deDE', 'Urahnin Sareth''na', '', NULL, NULL, 23420), -- 41486
(4057, 'deDE', 'Sohn des Cenarius', '', NULL, NULL, 23420), -- 4057
(4051, 'deDE', 'Cenarischer Botaniker', '', NULL, NULL, 23420), -- 4051
(4053, 'deDE', 'Tochter des Cenarius', '', NULL, NULL, 23420), -- 4053
(4409, 'deDE', 'Torwächter Kordurus', '', NULL, NULL, 23420), -- 4409
(4052, 'deDE', 'Cenarischer Druide', '', NULL, NULL, 23420), -- 4052
(4050, 'deDE', 'Cenarischer Verwalter', '', NULL, NULL, 23420), -- 4050
(4061, 'deDE', 'Mirkfallondryade', '', NULL, NULL, 23420), -- 4061
(5915, 'deDE', 'Bruder Rabeneiche', '', NULL, NULL, 23420), -- 5915
(4067, 'deDE', 'Mondlichtläufer', '', NULL, NULL, 23420), -- 4067
(34923, 'deDE', 'Herold Aph''lass', '', NULL, NULL, 23420), -- 34923
(34969, 'deDE', 'Korrumpierte Schildwache', '', NULL, NULL, 23420), -- 34969
(4018, 'deDE', 'Gehörnter Renner', '', NULL, NULL, 23420), -- 4018
(4019, 'deDE', 'Großer Renner', '', NULL, NULL, 23420), -- 4019
(61677, 'deDE', 'Bergstinktier', '', NULL, 'wildpetcapturable', 23420), -- 61677
(4017, 'deDE', 'Verschlagener Feendrache', '', NULL, NULL, 23420), -- 4017
(34961, 'deDE', 'Eindringender Zermalmer', '', NULL, NULL, 23420), -- 34961
(4021, 'deDE', 'Verderbte Blutsaftbestie', '', NULL, NULL, 23420), -- 4021
(4020, 'deDE', 'Blutsaftbestie', '', NULL, NULL, 23420), -- 4020
(4407, 'deDE', 'Teloren', '', 'Hippogryphenmeister', NULL, 23420), -- 4407
(34917, 'deDE', 'Eindringende Ranke', '', NULL, NULL, 23420), -- 34917
(34968, 'deDE', 'Schildwache des Steinkrallengebirges', '', NULL, 'Directions', 23420), -- 34968
(41485, 'deDE', 'Wächter von Thal''darah', '', NULL, NULL, 23420), -- 41485
(41482, 'deDE', 'Meister Thal''darah', '', NULL, NULL, 23420), -- 41482
(41019, 'deDE', 'Strickleiter', '', NULL, 'vehichleCursor', 23420), -- 41019
(41418, 'deDE', 'Heißluftballon von Krom''gar', '', NULL, 'vehichleCursor', 23420), -- 41418
(41417, 'deDE', 'Die Bombe', '', NULL, NULL, 23420), -- 41417
(48687, 'deDE', 'Maultierhirsch', '', NULL, NULL, 23420), -- 48687
(44177, 'deDE', 'Gastwirtin Bernice', '', 'Gastwirtin', NULL, 23420), -- 44177
(43019, 'deDE', 'Teldorae', '', 'Stallmeisterin', NULL, 23420), -- 43019
(35138, 'deDE', 'Ceyora', '', 'Flugmeisterin', NULL, 23420), -- 35138
(41638, 'deDE', 'Hundemeister Jonathan', '', NULL, NULL, 23420), -- 41638
(41637, 'deDE', 'Vera', '', NULL, NULL, 23420), -- 41637
(41635, 'deDE', 'Iolo', '', NULL, NULL, 23420), -- 41635
(41664, 'deDE', 'Salsbury der "Helfer"', '', NULL, NULL, 23420), -- 41664
(41487, 'deDE', 'Hierophant Malyk', '', NULL, NULL, 23420), -- 41487
(41651, 'deDE', 'Rabe', '', NULL, NULL, 23420), -- 41651
(41627, 'deDE', 'Weitblickwächter', '', NULL, NULL, 23420), -- 41627
(35152, 'deDE', 'Urtum der Kriegsnarbe', '', NULL, NULL, 23420), -- 35152
(35173, 'deDE', 'Irrwisch des Steinkrallengebirges', '', NULL, NULL, 23420), -- 35173
(35154, 'deDE', 'Nachtelfenmagier', '', NULL, NULL, 23420), -- 35154
(62189, 'deDE', 'Alpines Streifenhörnchen', '', NULL, 'wildpetcapturable', 23420), -- 62189
(35153, 'deDE', 'Draeneiischer Paladin', '', NULL, NULL, 23420), -- 35153
(42101, 'deDE', 'Hilfloser junger Druide', '', NULL, NULL, 23420), -- 42101
(42091, 'deDE', 'Orthus Klippenläufer', '', NULL, NULL, 23420), -- 42091
(50895, 'deDE', 'Volux', '', NULL, NULL, 23420), -- 50895
(23409, 'deDE', 'Invisible Stalker - Large AOI (Scale x3)', '', NULL, NULL, 23420), -- 23409
(42038, 'deDE', 'Heißluftballon von Krom''gar', '', NULL, NULL, 23420), -- 42038
(41988, 'deDE', 'Die Bombe', '', NULL, NULL, 23420), -- 41988
(35160, 'deDE', 'Holzarbeiter von Krom''gar', '', NULL, NULL, 23420), -- 35160
(41528, 'deDE', 'Hilfloser junger Druide', '', NULL, NULL, 23420), -- 41528
(35159, 'deDE', 'Hexendoktor von Krom''gar', '', NULL, NULL, 23420), -- 35159
(35174, 'deDE', 'Peon von Krom''gar', '', NULL, NULL, 23420), -- 35174
(35161, 'deDE', 'Krieger von Krom''gar', '', NULL, NULL, 23420), -- 35161
(35158, 'deDE', 'Berserker von Krom''gar', '', NULL, NULL, 23420), -- 35158
(35151, 'deDE', 'Wärterin des Steinkrallengebirges', '', NULL, NULL, 23420), -- 35151
(62190, 'deDE', 'Korallenschlange', '', NULL, 'wildpetcapturable', 23420), -- 62190
(34894, 'deDE', 'Steinkrallenwidder', '', NULL, NULL, 23420), -- 34894
(49724, 'deDE', 'Korallenschlange', '', NULL, NULL, 23420), -- 49724
(34896, 'deDE', 'Seldarria', '', NULL, NULL, 23420), -- 34896
(34911, 'deDE', 'Rissgeformter Feuerelementar', '', NULL, NULL, 23420), -- 34911
(34898, 'deDE', 'Schwarzer Großdrachenwelpe', '', NULL, NULL, 23420), -- 34898
(34897, 'deDE', 'Schwarzdrache', '', NULL, NULL, 23420), -- 34897
(49779, 'deDE', 'Alpines Streifenhörnchen', '', NULL, NULL, 23420), -- 49779
(35163, 'deDE', 'Verwüster von Krom''gar', '', NULL, 'vehichleCursor', 23420), -- 35163
(35150, 'deDE', 'Darnassische Glevenschleuder', '', NULL, 'vehichleCursor', 23420), -- 35150
(7730, 'deDE', 'Grunzer des Steinkrallengebirges', '', NULL, NULL, 23420), -- 7730
(31890, 'deDE', 'Bergstinktier', '', NULL, NULL, 23420), -- 31890
(5930, 'deDE', 'Schwester Sichelschwinge', '', NULL, NULL, 23420), -- 5930
(15169, 'deDE', 'Ralo''shan die ewige Wächterin', '', NULL, NULL, 23420), -- 15169
(11804, 'deDE', 'Zwielichtbewahrer Havunth', '', 'Schattenhammer', NULL, 23420), -- 11804
(15200, 'deDE', 'Zwielichtbewahrerin Mayna', '', 'Schattenhammer', NULL, 23420), -- 15200
(14478, 'deDE', 'Hurrikanus', '', NULL, NULL, 23420), -- 14478
(14347, 'deDE', 'Hochlord Demitrian', '', NULL, NULL, 23420), -- 14347
(15570, 'deDE', 'Urahne Urstein', '', NULL, NULL, 23420), -- 15570
(62184, 'deDE', 'Felsviper', '', NULL, 'wildpetcapturable', 23420), -- 62184
(50481, 'deDE', 'Felsviper', '', NULL, NULL, 23420), -- 50481
(14474, 'deDE', 'Zora', '', NULL, NULL, 23420), -- 14474
(50897, 'deDE', 'Ffexk der Dünenpirscher', '', NULL, NULL, 23420), -- 50897
(15610, 'deDE', 'Cenarische Späherin Azenel', '', NULL, NULL, 23420), -- 15610
(11726, 'deDE', 'Tunnelgräber des Zoraschwarms', '', NULL, NULL, 23420), -- 11726
(50742, 'deDE', 'Qem', '', NULL, NULL, 23420), -- 50742
(50745, 'deDE', 'Losaj', '', NULL, NULL, 23420), -- 50745
(11727, 'deDE', 'Wespe des Zoraschwarms', '', NULL, NULL, 23420),
(11725, 'deDE', 'Wegbehüter des Zoraschwarms', '', NULL, NULL, 23420), -- 11725
(11729, 'deDE', 'Schwester des Zoraschwarms', '', NULL, NULL, 23420), -- 11729
(11728, 'deDE', 'Häscher des Zoraschwarms', '', NULL, NULL, 23420), -- 11728
(15444, 'deDE', 'Arkanist Sprinkelstaub', '', NULL, NULL, 23420), -- 15444
(15443, 'deDE', 'Janela Starkhammer', '', NULL, NULL, 23420), -- 15443
(17765, 'deDE', 'Silithystschildwache der Allianz', '', NULL, NULL, 23420), -- 17765
(17090, 'deDE', 'Silithusstaub', '', NULL, NULL, 23420), -- 17090
(17080, 'deDE', 'Marschall Himmelswand', '', NULL, NULL, 23420), -- 17080
(17068, 'deDE', 'Oberster Expeditionslieferant Enkles', '', NULL, NULL, 23420), -- 17068
(15903, 'deDE', 'Unteroffizier Schwarz', '', NULL, NULL, 23420), -- 15903
(15442, 'deDE', 'Fußsoldat der Eisenschmiedebrigade', '', NULL, NULL, 23420), -- 15442
(15441, 'deDE', 'Scharfschütze der Eisenschmiedebrigade', '', NULL, NULL, 23420), -- 15441
(11723, 'deDE', 'Sandpirscher des Ashischwarms', '', NULL, NULL, 23420), -- 11723
(51375, 'deDE', 'Cenarische Späherin Jalia', '', NULL, NULL, 23420), -- 51375
(15611, 'deDE', 'Cenarische Späherin Jalia', '', NULL, NULL, 23420), -- 15611
(14475, 'deDE', 'Rex Ashil', '', NULL, NULL, 23420), -- 14475
(11721, 'deDE', 'Arbeiter des Ashischwarms', '', NULL, NULL, 23420), -- 11721
(50744, 'deDE', 'Qu''rik', '', NULL, NULL, 23420), -- 50744
(50370, 'deDE', 'Karapax', '', NULL, NULL, 23420), -- 50370
(51752, 'deDE', 'Ölschleim', '', NULL, NULL, 23420), -- 51752
(6491, 'deDE', 'Geistheiler', '', NULL, NULL, 23420), -- 6491
(15202, 'deDE', 'Vyral der Üble', '', 'Schattenhammer', NULL, 23420), -- 15202
(15201, 'deDE', 'Zwielichtflammenhäscher', '', NULL, NULL, 23420), -- 15201
(15213, 'deDE', 'Zwielichtoberanführer', '', 'Schattenhammer', NULL, 23420), -- 15213
(11744, 'deDE', 'Staubstürmer', '', NULL, NULL, 23420), -- 11744
(14476, 'deDE', 'Krellack', '', NULL, NULL, 23420), -- 14476
(15542, 'deDE', 'Zwielichtmarodeur', '', 'Schattenhammer', NULL, 23420), -- 15542
(15541, 'deDE', 'Zwielichtmarodeurin Morna', '', 'Schattenhammer', NULL, 23420), -- 15541
(13220, 'deDE', 'Layo Sternenschlag', '', 'Der Zirkel des Cenarius', NULL, 23420), -- 13220
(12956, 'deDE', 'Zannok Ledernaht', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 12956
(11805, 'deDE', 'Jarund Firmschreiter', '', 'Der Zirkel des Cenarius', NULL, 23420), -- 11805
(11722, 'deDE', 'Verteidiger des Ashischwarms', '', NULL, NULL, 23420), -- 11722
(15182, 'deDE', 'Vish Kozus', '', 'Hauptmann der Wache', NULL, 23420), -- 15182
(17082, 'deDE', 'Scharfschütze Torrig', '', NULL, NULL, 23420), -- 17082
(15177, 'deDE', 'Wolke Himmelstänzer', '', 'Hippogryphenmeister', NULL, 23420), -- 15177
(50588, 'deDE', 'Magier von Nethergarde', '', NULL, NULL, 23420), -- 50588
(15181, 'deDE', 'Kommandant Mar''alith', '', NULL, NULL, 23420), -- 15181
(50587, 'deDE', 'Magier der Okril''lon', '', NULL, NULL, 23420), -- 50587
(15179, 'deDE', 'Mishta', '', 'Handwerkswaren', NULL, 23420), -- 15179
(15540, 'deDE', 'Windrufer Kaldon', '', NULL, NULL, 23420), -- 15540
(17081, 'deDE', 'Späher Blutfaust', '', NULL, NULL, 23420), -- 17081
(15722, 'deDE', 'Knappe Leoren Mal''derath', '', 'Stallmeister', NULL, 23420), -- 15722
(15180, 'deDE', 'Baristolth von den Sandstürmen', '', NULL, NULL, 23420), -- 15180
(15178, 'deDE', 'Runk Windlenker', '', 'Windreitermeister', NULL, 23420), -- 15178
(15175, 'deDE', 'Khur Hornstürmer', '', 'Reagenzien', NULL, 23420), -- 15175
(17074, 'deDE', 'Cenarischer Späher', '', NULL, NULL, 23420), -- 17074
(15306, 'deDE', 'Bor Wildmähne', '', NULL, NULL, 23420), -- 15306
(15270, 'deDE', 'Huum Wildmähne', '', NULL, NULL, 23420), -- 15270
(15599, 'deDE', 'Urahnin Klingensang', '', NULL, NULL, 23420), -- 15599
(15183, 'deDE', 'Geologin Lerchenbann', '', NULL, NULL, 23420), -- 15183
(15191, 'deDE', 'Windruferin Prachthorn', '', NULL, NULL, 23420), -- 15191
(15176, 'deDE', 'Vargus', '', 'Schmied', NULL, 23420), -- 15176
(15282, 'deDE', 'Aurel Goldblatt', '', NULL, NULL, 23420), -- 15282
(15190, 'deDE', 'Noggle Wankelhupf', '', NULL, NULL, 23420), -- 15190
(15189, 'deDE', 'Beetix Wankelhupf', '', NULL, NULL, 23420), -- 15189
(15614, 'deDE', 'J.D. Schattenklang', '', NULL, NULL, 23420), -- 15614
(16543, 'deDE', 'Garon Hutchins', '', NULL, NULL, 23420), -- 16543
(11740, 'deDE', 'Baggerschläger', '', NULL, NULL, 23420), -- 11740
(11738, 'deDE', 'Sandhuscher', '', NULL, NULL, 23420), -- 11738
(11735, 'deDE', 'Steinpanzerskorpid', '', NULL, NULL, 23420), -- 11735
(16091, 'deDE', 'Dirk Donnerholz', '', NULL, NULL, 23420), -- 16091
(15419, 'deDE', 'Kania', '', 'Verzauberkunstbedarf', NULL, 23420), -- 15419
(15184, 'deDE', 'Infanterist der Burg Cenarius', '', NULL, NULL, 23420), -- 15184
(15174, 'deDE', 'Calandrath', '', 'Gastwirt', NULL, 23420), -- 15174
(11698, 'deDE', 'Stecher des Ashischwarms', '', NULL, NULL, 23420), -- 11698
(15293, 'deDE', 'Aendel Windspeer', '', NULL, NULL, 23420), -- 15293
(12199, 'deDE', 'Schemen von Mondglanz', '', NULL, NULL, 23420), -- 12199
(11724, 'deDE', 'Schwärmer des Ashischwarms', '', NULL, NULL, 23420), -- 11724
(61441, 'deDE', 'Stachelechse', '', NULL, 'wildpetcapturable', 23420), -- 61441
(11746, 'deDE', 'Wüstenrumpler', '', NULL, NULL, 23420), -- 11746
(12179, 'deDE', 'Gequälte Schildwache', '', NULL, NULL, 23420), -- 12179
(12178, 'deDE', 'Gequälter Druide', '', NULL, NULL, 23420), -- 12178
(15194, 'deDE', 'Ortell der Einsiedler', '', NULL, NULL, 23420), -- 15194
(50743, 'deDE', 'Manax', '', NULL, NULL, 23420), -- 50743
(15609, 'deDE', 'Cenarischer Späher Landion', '', NULL, NULL, 23420), -- 15609
(11730, 'deDE', 'Wegelagerer des Regalschwarms', '', NULL, NULL, 23420), -- 11730
(14473, 'deDE', 'Lapress', '', NULL, NULL, 23420), -- 14473
(15545, 'deDE', 'Cenarische Vorhut', '', NULL, NULL, 23420), -- 15545
(11736, 'deDE', 'Steinpanzerzange', '', NULL, NULL, 23420), -- 11736
(11741, 'deDE', 'Baggerbrecher', '', NULL, NULL, 23420), -- 11741
(15616, 'deDE', 'Grunzer der Orgrimmarlegion', '', NULL, NULL, 23420), -- 15616
(15613, 'deDE', 'Merok Langschreiter', '', NULL, NULL, 23420), -- 15613
(15612, 'deDE', 'Krug Schädelspalt', '', 'Hauptmann der Orgrimmarlegion', NULL, 23420), -- 15612
(18199, 'deDE', 'Silithusstaub, Hordequest', '', NULL, NULL, 23420), -- 18199
(15617, 'deDE', 'Axtwerfer der Orgrimmarlegion', '', NULL, NULL, 23420), -- 15617
(15615, 'deDE', 'Schattenpriesterin Shai', '', NULL, NULL, 23420), -- 15615
(17766, 'deDE', 'Silithystschildwache der Horde', '', NULL, NULL, 23420), -- 17766
(17079, 'deDE', 'General Kirika', '', NULL, NULL, 23420), -- 17079
(17070, 'deDE', 'Apotheker Quinard', '', NULL, NULL, 23420), -- 17070
(61319, 'deDE', 'Käfer', '', NULL, 'wildpetcapturable', 23420), -- 61319
(11745, 'deDE', 'Wirbelsturmkrieger', '', NULL, NULL, 23420), -- 11745
(15308, 'deDE', 'Zwielichtprophetin', '', 'Schattenhammer', NULL, 23420), -- 15308
(61326, 'deDE', 'Skorpid', '', NULL, 'wildpetcapturable', 23420), -- 61326
(49840, 'deDE', 'Stachelechse', '', NULL, NULL, 23420), -- 49840
(11803, 'deDE', 'Zwielichtbewahrer Exeter', '', 'Schattenhammer', NULL, 23420), -- 11803
(11882, 'deDE', 'Zwielichtsteinrufer', '', 'Schattenhammer', NULL, 23420), -- 11882
(11883, 'deDE', 'Zwielichtmeister', '', 'Schattenhammer', NULL, 23420), -- 11883
(11881, 'deDE', 'Zwielichtgeofürst', '', 'Schattenhammer', NULL, 23420), -- 11881
(11880, 'deDE', 'Zwielichträcher', '', 'Schattenhammer', NULL, 23420), -- 11880
(15693, 'deDE', 'Jonathan der Weise', '', NULL, NULL, 23420), -- 15693
(15500, 'deDE', 'Keyl Flinkklaue', '', NULL, NULL, 23420), -- 15500
(15499, 'deDE', 'Aufseher Haro', '', NULL, NULL, 23420), -- 15499
(15498, 'deDE', 'Windruferin Yessendra', '', NULL, NULL, 23420), -- 15498
(5785, 'deDE', 'Schwester Hasspeitsche', '', NULL, NULL, 23420), -- 5785
(3051, 'deDE', 'Vorsteher Zischelspross', '', NULL, NULL, 23420), -- 3051
(43613, 'deDE', 'Todessprecher Weisenpfad', '', 'Schattenhammer', NULL, 23420), -- 43613
(34766, 'deDE', 'Krendel Großtasch', '', 'Zeppelinmeister von Orgrimmar', NULL, 23420), -- 34766
(3419, 'deDE', 'Apothekerin Zamah', '', 'Königliche Apothekervereinigung', NULL, 23420), -- 3419
(3049, 'deDE', 'Thurston Xane', '', 'Magierlehrer', NULL, 23420), -- 3049
(51640, 'deDE', 'Lama Wolkenlied', '', 'Schamanenlehrerin', NULL, 23420), -- 51640
(3045, 'deDE', 'Malakai Kreuz', '', 'Priesterlehrer', NULL, 23420), -- 3045
(5906, 'deDE', 'Xanis Flammenwirker', '', NULL, NULL, 23420), -- 5906
(30724, 'deDE', 'Mertle Trockenfeder', '', 'Inschriftenkundebedarf', NULL, 23420), -- 30724
(30709, 'deDE', 'Poshken Rückenbinder', '', 'Inschriftenkundelehrer', NULL, 23420), -- 30709
(5543, 'deDE', 'Clarice Foster', '', NULL, NULL, 23420), -- 5543
(3447, 'deDE', 'Pawe Nebelläufer', '', NULL, NULL, 23420), -- 3447
(3046, 'deDE', 'Vater Cobb', '', 'Priesterlehrer', NULL, 23420), -- 3046
(2798, 'deDE', 'Pand Steinbinder', '', 'Lehrer für Erste Hilfe', NULL, 23420), -- 2798
(3016, 'deDE', 'Tand', '', 'Korbmacher', NULL, 23420), -- 3016
(3009, 'deDE', 'Bena Winterhuf', '', 'Alchemielehrerin', NULL, 23420), -- 3009
(3010, 'deDE', 'Mani Winterhuf', '', 'Alchemiebedarf', NULL, 23420), -- 3010
(96470, 'deDE', 'Jana Winterhuf', '', 'Gewichte & Maße', NULL, 23420), -- 96470
(11047, 'deDE', 'Kray', '', 'Alchemielehrling', NULL, 23420), -- 11047
(3014, 'deDE', 'Nida Winterhuf', '', 'Kräuterkundebedarf', NULL, 23420), -- 3014
(47589, 'deDE', 'Verzauberin Herim', '', NULL, NULL, 23420), -- 47589
(11071, 'deDE', 'Mot Morgenwandler', '', 'Verzauberkunstlehrling', NULL, 23420), -- 11071
(3012, 'deDE', 'Nata Morgenwandler', '', 'Verzauberkunstbedarf', NULL, 23420), -- 3012
(3011, 'deDE', 'Teg Morgenwandler', '', 'Verzauberkunstlehrer', NULL, 23420), -- 3011
(11084, 'deDE', 'Tarn', '', 'Lederverarbeitungslehrling', NULL, 23420), -- 11084
(11051, 'deDE', 'Vhan', '', 'Schneiderlehrling', NULL, 23420), -- 11051
(7089, 'deDE', 'Mooranta', '', 'Kürschnerlehrerin', NULL, 23420), -- 7089
(3093, 'deDE', 'Grod', '', 'Lederrüstungshändler', NULL, 23420), -- 3093
(3050, 'deDE', 'Veren Weitschreiter', '', NULL, NULL, 23420), -- 3050
(3007, 'deDE', 'Una', '', 'Lederverarbeitungslehrerin', NULL, 23420), -- 3007
(3008, 'deDE', 'Mak', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 3008
(3092, 'deDE', 'Tagain', '', 'Stoffrüstungshändlerin', NULL, 23420), -- 3092
(3004, 'deDE', 'Tepa', '', 'Schneiderlehrerin', NULL, 23420), -- 3004
(3005, 'deDE', 'Mahu', '', 'Schneiderbedarf', NULL, 23420), -- 3005
(3095, 'deDE', 'Fela', '', 'Händlerin für Kettenrüstungen', NULL, 23420), -- 3095
(3978, 'deDE', 'Weiser Wahrspruch', '', NULL, NULL, 23420), -- 3978
(52658, 'deDE', 'Paku Wolkenjäger', '', 'Juwelierskunstbedarf', NULL, 23420), -- 52658
(52657, 'deDE', 'Nahari Wolkenjäger', '', 'Juwelierskunstlehrerin', NULL, 23420), -- 52657
(3002, 'deDE', 'Krum Steinhuf', '', 'Bergbaubedarf', NULL, 23420), -- 3002
(8722, 'deDE', 'Auktionator Gullem', '', NULL, NULL, 23420), -- 8722
(3001, 'deDE', 'Brek Steinhuf', '', 'Bergbaulehrer', NULL, 23420), -- 3001
(8674, 'deDE', 'Auktionator Rikno Noragg', '', NULL, NULL, 23420), -- 8674
(52651, 'deDE', 'Ingenieur Bleichhuf', '', 'Ingenieurskunstlehrer', NULL, 23420), -- 52651
(52655, 'deDE', 'Bleichhufs großer Ersatzteilbeutel', '', 'Ingenieursbedarf', NULL, 23420), -- 52655
(51503, 'deDE', 'Randah Liedhorn', '', 'Gildenhändlerin', NULL, 23420), -- 51503
(10278, 'deDE', 'Thrag Steinhuf', '', 'Schmiedekunstlehrling', NULL, 23420), -- 10278
(8361, 'deDE', 'Chepi', '', 'Reagenzien', NULL, 23420), -- 8361
(6410, 'deDE', 'Orm Steinhuf', '', NULL, NULL, 23420), -- 6410
(5189, 'deDE', 'Thrumn', '', 'Verkäufer für Gildenwappenröcke', NULL, 23420), -- 5189
(5054, 'deDE', 'Krumn', '', 'Gildenmeister', NULL, 23420), -- 5054
(3003, 'deDE', 'Fyr Nebelläufer', '', 'Brotverkäuferin', NULL, 23420), -- 3003
(2999, 'deDE', 'Taur Steinhuf', '', 'Schmiedekunstbedarf', NULL, 23420), -- 2999
(2998, 'deDE', 'Karn Steinhuf', '', 'Schmiedekunstlehrer', NULL, 23420), -- 2998
(8362, 'deDE', 'Kuruk', '', 'Gemischtwaren', NULL, 23420), -- 8362
(8364, 'deDE', 'Pakwa', '', 'Taschenverkäuferin', NULL, 23420), -- 8364
(8363, 'deDE', 'Shadi Nebelläufer', '', 'Handwerkswaren', NULL, 23420), -- 8363
(2997, 'deDE', 'Jyn Steinhuf', '', 'Waffenhändlerin', NULL, 23420), -- 2997
(3042, 'deDE', 'Sark Rachtotem', '', 'Kriegerlehrer', NULL, 23420), -- 3042
(3039, 'deDE', 'Holt Donnerhorn', '', 'Jägerlehrer', NULL, 23420), -- 3039
(31144, 'deDE', 'Trainingsattrappe', '', NULL, NULL, 23420), -- 31144
(10086, 'deDE', 'Hesuwa Donnerhorn', '', 'Tierausbilder', NULL, 23420), -- 10086
(3043, 'deDE', 'Ker Rachtotem', '', 'Kriegerlehrerin', NULL, 23420), -- 3043
(3041, 'deDE', 'Torm Rachtotem', '', 'Kriegerlehrer', NULL, 23420), -- 3041
(3040, 'deDE', 'Urek Donnerhorn', '', 'Jägerlehrer', NULL, 23420), -- 3040
(3038, 'deDE', 'Kary Donnerhorn', '', 'Jägerlehrerin', NULL, 23420), -- 3038
(32667, 'deDE', 'Trainingsattrappe', '', NULL, NULL, 23420), -- 32667
(32666, 'deDE', 'Trainingsattrappe', '', NULL, NULL, 23420), -- 32666
(31146, 'deDE', 'Trainingsattrappe des Schlachtzuges', '', NULL, NULL, 23420), -- 31146
(6393, 'deDE', 'Henen Rachtotem', '', NULL, NULL, 23420), -- 6393
(4943, 'deDE', 'Mosarn', '', NULL, NULL, 23420), -- 4943
(34978, 'deDE', 'Mosha Sternhorn', '', 'Kampfmeisterin', NULL, 23420), -- 34978
(34976, 'deDE', 'Ruk Kriegsstampfer', '', 'Kampfmeister', NULL, 23420), -- 34976
(5599, 'deDE', 'Kon Gelbauge', '', NULL, NULL, 23420), -- 5599
(49750, 'deDE', 'Herold des Kriegshäuptlings', '', NULL, NULL, 23420), -- 49750
(14441, 'deDE', 'Jäger Rachtotem', '', NULL, NULL, 23420), -- 14441
(10881, 'deDE', 'Kliffläufer Windschreiter', '', NULL, NULL, 23420), -- 10881
(6746, 'deDE', 'Gastwirtin Pala', '', 'Gastwirtin', NULL, 23420), -- 6746
(10054, 'deDE', 'Bulrug', '', 'Stallmeister', NULL, 23420), -- 10054
(3013, 'deDE', 'Komin Winterhuf', '', 'Kräuterkundelehrer', NULL, 23420), -- 3013
(3017, 'deDE', 'Nan Nebelläufer', '', 'Obstverkäuferin', NULL, 23420), -- 3017
(8360, 'deDE', 'Elki', '', 'Händler für Kettenrüstungen', NULL, 23420), -- 8360
(8358, 'deDE', 'Hewa', '', 'Stoffrüstungshändler', NULL, 23420), -- 8358
(8359, 'deDE', 'Ahanu', '', 'Lederrüstungshändler', NULL, 23420), -- 8359
(3015, 'deDE', 'Kuna Donnerhorn', '', 'Bogen- & Pfeilmacherbedarf', NULL, 23420), -- 3015
(50483, 'deDE', 'Kriegerheld Tuho', '', 'Rüstmeister von Donnerfels', NULL, 23420), -- 50483
(2995, 'deDE', 'Tal', '', 'Windreitermeister', NULL, 23420), -- 2995
(8357, 'deDE', 'Atepa', '', 'Bankier', NULL, 23420), -- 8357
(8356, 'deDE', 'Chesmu', '', 'Bankierin', NULL, 23420), -- 8356
(2996, 'deDE', 'Torn', '', 'Bankier', NULL, 23420), -- 2996
(3022, 'deDE', 'Sunn Rachtotem', '', 'Stabhändlerin', NULL, 23420), -- 3022
(3026, 'deDE', 'Aska Nebelläufer', '', 'Kochkunstlehrerin', NULL, 23420), -- 3026
(8398, 'deDE', 'Ohanko', '', 'Zweihandwaffenhändler', NULL, 23420), -- 8398
(3027, 'deDE', 'Naal Nebelläufer', '', 'Kochbedarf', NULL, 23420), -- 3027
(3023, 'deDE', 'Sura Wildmähne', '', 'Kriegsharnischverkäuferin', NULL, 23420), -- 3023
(3021, 'deDE', 'Kard Rachtotem', '', 'Schwert- & Dolchhändler', NULL, 23420), -- 3021
(3020, 'deDE', 'Etu Rachtotem', '', 'Streitkolben- und Stabhändler', NULL, 23420), -- 3020
(14440, 'deDE', 'Jäger Weisenwind', '', NULL, NULL, 23420), -- 14440
(12383, 'deDE', 'Nibbles', '', 'Präriehund', NULL, 23420), -- 12383
(8401, 'deDE', 'Halpa', '', 'Präriehundverkäufer', NULL, 23420), -- 8401
(3019, 'deDE', 'Delgo Rachtotem', '', 'Axthändler', NULL, 23420), -- 3019
(3018, 'deDE', 'Hogor Donnerhuf', '', 'Schusswaffenhändler', NULL, 23420), -- 3018
(15164, 'deDE', 'Mulgore Auslöser', '', NULL, NULL, 23420), -- 15164
(3025, 'deDE', 'Kaga Nebelläufer', '', 'Fleischverkäuferin', NULL, 23420), -- 3025
(4721, 'deDE', 'Zangen Steinhuf', '', NULL, NULL, 23420), -- 4721
(3024, 'deDE', 'Tah Winterhuf', '', NULL, NULL, 23420), -- 3024
(3029, 'deDE', 'Sewa Nebelläufer', '', 'Angelbedarf', NULL, 23420), -- 3029
(3083, 'deDE', 'Ehrenwache', '', NULL, NULL, 23420), -- 3083
(3028, 'deDE', 'Kah Nebelläufer', '', 'Angellehrer', NULL, 23420), -- 3028
(14442, 'deDE', 'Jäger Donnerhorn', '', NULL, NULL, 23420), -- 14442
(3037, 'deDE', 'Sheza Wildmähne', '', NULL, NULL, 23420), -- 3037
(9087, 'deDE', 'Bashana Runentotem', '', NULL, NULL, 23420), -- 9087
(15898, 'deDE', 'Verkäufer des Mondfests', '', NULL, NULL, 23420), -- 15898
(15895, 'deDE', 'Botin des Mondfests', '', NULL, NULL, 23420), -- 15895
(15893, 'deDE', 'Mondfeuerwerk Credit Marker', '', NULL, NULL, 23420), -- 15893
(15894, 'deDE', 'Mondraketenbündel Credit Marker', '', NULL, NULL, 23420), -- 15894
(15897, 'deDE', 'Großes Spotlight', '', NULL, NULL, 23420), -- 15897
(47572, 'deDE', 'Otoh Grauhaut', '', 'Archäologielehrer', NULL, 23420), -- 47572
(15580, 'deDE', 'Urahne Weizhuf', '', NULL, NULL, 23420), -- 15580
(43870, 'deDE', 'Seher Beryl', '', 'Priesterlehrer', NULL, 23420), -- 43870
(5770, 'deDE', 'Nara Wildmähne', '', NULL, NULL, 23420), -- 5770
(3036, 'deDE', 'Kym Wildmähne', '', 'Druidenlehrerin', NULL, 23420), -- 3036
(5769, 'deDE', 'Erzdruide Hamuul Runentotem', '', NULL, NULL, 23420), -- 5769
(3034, 'deDE', 'Sheal Runentotem', '', 'Druidenlehrerin', NULL, 23420), -- 3034
(3033, 'deDE', 'Turak Runentotem', '', 'Druidenlehrer', NULL, 23420), -- 3033
(43796, 'deDE', 'Tahu Weisenwind', '', 'Priesterlehrer', NULL, 23420), -- 43796
(43004, 'deDE', 'Seherin Kaya', '', 'Priesterlehrerin', NULL, 23420), -- 43004
(9076, 'deDE', 'Ghede', '', NULL, NULL, 23420), -- 9076
(36832, 'deDE', 'Vandale der Grimmtotems', '', NULL, NULL, 23420), -- 36832
(37024, 'deDE', 'Una Wildmähne', '', NULL, NULL, 23420), -- 37024
(36648, 'deDE', 'Baine Bluthuf', '', 'Oberhäuptling', NULL, 23420), -- 36648
(51143, 'deDE', 'Sonnenläufer Jo''hsu', '', NULL, NULL, 23420), -- 51143
(3441, 'deDE', 'Melor Steinhuf', '', NULL, NULL, 23420), -- 3441
(43795, 'deDE', 'Aponi Lichtmähne', '', 'Paladinlehrerin', NULL, 23420), -- 43795
(43001, 'deDE', 'Sonnenläufer Reha', '', 'Paladinlehrer', NULL, 23420), -- 43001
(8664, 'deDE', 'Sonnenläuferin Saern', '', 'Paladinlehrerin', NULL, 23420), -- 8664
(51372, 'deDE', 'Windreiter von Donnerfels', '', NULL, NULL, 23420), -- 51372
(36828, 'deDE', 'Schänder der Grimmtotems', '', NULL, NULL, 23420), -- 36828
(36827, 'deDE', 'Vagabund der Grimmtotems', '', NULL, NULL, 23420), -- 36827
(43975, 'deDE', 'Jazzranach', '', NULL, NULL, 23420), -- 43975
(43974, 'deDE', 'Razzranach', '', NULL, NULL, 23420), -- 43974
(43973, 'deDE', 'Hazzranach', '', NULL, NULL, 23420), -- 43973
(3068, 'deDE', 'Mazzranache', '', NULL, NULL, 23420), -- 3068
(5787, 'deDE', 'Vollstrecker Emilgund', '', NULL, NULL, 23420), -- 5787
(36825, 'deDE', 'Nachzügler der Grimmtotems', '', NULL, NULL, 23420), -- 36825
(5786, 'deDE', 'Stummelspeer', '', NULL, NULL, 23420), -- 5786
(2973, 'deDE', 'Kodobulle', '', NULL, NULL, 23420),
(51515, 'deDE', 'Beschützer von Mulgore', '', NULL, NULL, 23420), -- 51515
(23618, 'deDE', 'Ahab Weizhuf', '', 'Der alte Bauer', NULL, 23420), -- 23618
(15575, 'deDE', 'Urahne Bluthuf', '', NULL, NULL, 23420), -- 15575
(3219, 'deDE', 'Kriegerheldin Springender Hirsch', '', NULL, 'Directions', 23420), -- 3219
(12355, 'deDE', 'Grauer Kodo', '', NULL, NULL, 23420), -- 12355
(34155, 'deDE', 'Weißer Kodo', '', NULL, NULL, 23420), -- 34155
(63068, 'deDE', 'Miles', '', 'Kampfhaustier', NULL, 23420), -- 63068
(63067, 'deDE', 'Naleen', '', 'Kampfhaustiertrainerin', NULL, 23420), -- 63067
(49996, 'deDE', 'Bergbaumwollschwänzchen', '', NULL, NULL, 23420), -- 49996
(3215, 'deDE', 'Kriegerheld Schmetternder Schlag', '', NULL, 'Directions', 23420), -- 3215
(51637, 'deDE', 'Tarl Wolkenlied', '', 'Schamanenlehrer', NULL, 23420), -- 51637
(3064, 'deDE', 'Gennia Runentotem', '', 'Druidenlehrerin', NULL, 23420), -- 3064
(23616, 'deDE', 'Kyle der Wilde', '', NULL, NULL, 23420), -- 23616
(3218, 'deDE', 'Kriegerheldin Flinker Wind', '', NULL, 'Directions', 23420), -- 3218
(43015, 'deDE', 'Seherin Alsoomse', '', 'Priesterlehrerin', NULL, 23420), -- 43015
(43013, 'deDE', 'Sonnenläufer Iopi', '', 'Paladinlehrer', NULL, 23420), -- 43013
(2947, 'deDE', 'Harken Windtotem', '', NULL, NULL, 23420), -- 2947
(10600, 'deDE', 'Thontek Trampelhuf', '', NULL, NULL, 23420), -- 10600
(3217, 'deDE', 'Kriegerheldin Sternenadler', '', NULL, 'Directions', 23420), -- 3217
(10721, 'deDE', 'Kriegernovize', '', NULL, NULL, 23420), -- 10721
(10599, 'deDE', 'Hulfnar Steintotem', '', NULL, NULL, 23420), -- 10599
(3063, 'deDE', 'Krang Steinhuf', '', 'Kriegerlehrer', NULL, 23420), -- 3063
(3221, 'deDE', 'Kriegerheld Felshorn', '', NULL, 'Directions', 23420), -- 3221
(2948, 'deDE', 'Mull Donnerhorn', '', NULL, NULL, 23420), -- 2948
(3223, 'deDE', 'Kriegerheldin Regenfänger', '', NULL, 'Directions', 23420), -- 3223
(3884, 'deDE', 'Jhawna Saatwind', '', 'Bäckerin', NULL, 23420), -- 3884
(2985, 'deDE', 'Ruul Adlerklaue', '', NULL, NULL, 23420), -- 2985
(5938, 'deDE', 'Uthan Stillwasser', '', 'Angellehrer', NULL, 23420), -- 5938
(36644, 'deDE', 'Ahmo Donnerhorn', '', NULL, NULL, 23420), -- 36644
(11944, 'deDE', 'Vorn Himmelsdeuter', '', NULL, NULL, 23420), -- 11944
(6747, 'deDE', 'Gastwirt Kauth', '', 'Gastwirt', NULL, 23420), -- 6747
(5939, 'deDE', 'Vira Junghuf', '', 'Lehrerin für Erste Hilfe', NULL, 23420), -- 5939
(3222, 'deDE', 'Kriegerheld Wildläufer', '', NULL, 'Directions', 23420), -- 3222
(10050, 'deDE', 'Seikwa', '', 'Stallmeisterin', NULL, 23420), -- 10050
(6776, 'deDE', 'Magrin Flussmähne', '', NULL, NULL, 23420), -- 6776
(40809, 'deDE', 'Tak', '', 'Windreitermeister', NULL, 23420), -- 40809
(14549, 'deDE', 'Großer brauner Kodo', '', NULL, NULL, 23420), -- 14549
(14542, 'deDE', 'Großer weißer Kodo', '', NULL, NULL, 23420), -- 14542
(3690, 'deDE', 'Kar Sturmsang', '', 'Reitlehrer', NULL, 23420), -- 3690
(12354, 'deDE', 'Brauner Kodo', '', NULL, NULL, 23420), -- 12354
(14550, 'deDE', 'Großer grauer Kodo', '', NULL, NULL, 23420), -- 14550
(11407, 'deDE', 'Var''jun', '', NULL, NULL, 23420), -- 11407
(3685, 'deDE', 'Harb Klauenhuf', '', 'Reitkodos', NULL, 23420), -- 3685
(3080, 'deDE', 'Harant Eisenzweig', '', 'Rüstungsschmied & Schildmacher', NULL, 23420), -- 3080
(3079, 'deDE', 'Varg Windraunen', '', 'Lederrüstungshändler', NULL, 23420), -- 3079
(3078, 'deDE', 'Kennah Falkenauge', '', 'Büchsenmacher', NULL, 23420), -- 3078
(3077, 'deDE', 'Mahnott Raunarbe', '', 'Waffenschmied', NULL, 23420), -- 3077
(3067, 'deDE', 'Pyall Leisetreter', '', 'Kochkunstlehrer', NULL, 23420), -- 3067
(3224, 'deDE', 'Kriegerheld Wolkenmähne', '', NULL, 'Directions', 23420), -- 3224
(53561, 'deDE', 'Steinbullenflusskrebs', '', NULL, 'LootAll', 23420), -- 53561
(47419, 'deDE', 'Lalum Dunkelmähne', '', 'Berufsausbilderin', NULL, 23420), -- 47419
(6290, 'deDE', 'Yonn Pelzscherer', '', 'Kürschnerlehrer', NULL, 23420), -- 6290
(5940, 'deDE', 'Harn Rutenwurf', '', 'Angelbedarf', NULL, 23420), -- 5940
(3076, 'deDE', 'Moorat Langschreiter', '', 'Gemischtwaren', NULL, 23420), -- 3076
(3069, 'deDE', 'Chaw Starkfell', '', 'Lederverarbeitungslehrer', NULL, 23420), -- 3069
(3055, 'deDE', 'Maur Regenrufer', '', NULL, NULL, 23420), -- 3055
(3212, 'deDE', 'Kriegerheld Eisenhorn', '', NULL, 'Directions', 23420), -- 3212
(3081, 'deDE', 'Wunna Dunkelmähne', '', 'Handwerkswaren', NULL, 23420), -- 3081
(3688, 'deDE', 'Reban Freiläufer', '', 'Tierausbilder', NULL, 23420), -- 3688
(3065, 'deDE', 'Yaw Scharfmähne', '', 'Jägerlehrer', NULL, 23420), -- 3065
(3054, 'deDE', 'Zarlman Zwillingsmond', '', NULL, NULL, 23420), -- 3054
(36803, 'deDE', 'Dyami Windsegler', '', NULL, NULL, 23420), -- 36803
(43599, 'deDE', 'Dornwächter', '', NULL, NULL, 23420), -- 43599
(44927, 'deDE', 'Rohaku Steinhuf', '', NULL, NULL, 23420), -- 44927
(36694, 'deDE', 'Adana Donnerhorn', '', NULL, NULL, 23420), -- 36694
(106, 'deDE', 'Kodowildtier', '', NULL, NULL, 23420), -- 106
(2980, 'deDE', 'Grull Falkenwind', '', NULL, NULL, 23420), -- 2980
(3213, 'deDE', 'Kriegerheld Rennender Wolf', '', NULL, NULL, 23420), -- 3213
(3214, 'deDE', 'Kriegerheld Großhuf', '', NULL, NULL, 23420), -- 3214
(3883, 'deDE', 'Moodan Sonnenkorn', '', 'Bäcker', NULL, 23420), -- 3883
(3210, 'deDE', 'Kriegerheld Stolzhaupt', '', NULL, NULL, 23420), -- 3210
(3075, 'deDE', 'Bronk Stahlgrimm', '', 'Rüstungsschmied & Schildmacher', NULL, 23420), -- 3075
(3073, 'deDE', 'Marjak Kühnklinge', '', 'Waffenschmied', NULL, 23420), -- 3073
(3209, 'deDE', 'Kriegerheldin Windfeder', '', NULL, NULL, 23420), -- 3209
(3074, 'deDE', 'Varia Hartfell', '', 'Lederrüstungshändlerin', NULL, 23420), -- 3074
(63327, 'deDE', 'Shoyu', '', 'Mönchslehrer', NULL, 23420), -- 63327
(38438, 'deDE', 'Mulgore Offering Credit Bunny', '', NULL, NULL, 23420), -- 38438
(37737, 'deDE', 'Sonnenläufer Helaku', '', 'Paladinlehrer', NULL, 23420), -- 37737
(37724, 'deDE', 'Seherin Rabenfeder', '', 'Priesterlehrerin', NULL, 23420), -- 37724
(3059, 'deDE', 'Harutt Donnerhorn', '', 'Kriegerlehrer', NULL, 23420), -- 3059
(2981, 'deDE', 'Häuptling Falkenwind', '', NULL, NULL, 23420), -- 2981
(89713, 'deDE', 'Koak Hoburn', '', 'Chauffeur', NULL, 23420), -- 89713
(3211, 'deDE', 'Kriegerheld Blitzhorn', '', NULL, NULL, 23420), -- 3211
(3062, 'deDE', 'Meela Morgenwandler', '', 'Schamanenlehrerin', NULL, 23420), -- 3062
(3061, 'deDE', 'Lanka Weitschuss', '', 'Jägerlehrer', NULL, 23420), -- 3061
(3060, 'deDE', 'Gart Nebelläufer', '', 'Druidenlehrer', NULL, 23420), -- 3060
(45199, 'deDE', 'Verwundeter Kriegerheld', '', NULL, NULL, 23420), -- 45199
(44848, 'deDE', 'Trainingsattrappe', '', NULL, NULL, 23420), -- 44848
(3072, 'deDE', 'Kawnie Sanfthauch', '', 'Gemischtwaren', NULL, 23420), -- 3072
(38345, 'deDE', 'Gefangener Kriegerheld', '', NULL, NULL, 23420), -- 38345
(36943, 'deDE', 'Eindringling der Borstennacken', '', NULL, NULL, 23420), -- 36943
(2966, 'deDE', 'Junger Kampfeber', '', NULL, NULL, 23420), -- 2966
(2952, 'deDE', 'Eindringling der Borstennacken', '', NULL, NULL, 23420), -- 2952
(36727, 'deDE', 'Erster Trog', '', NULL, NULL, 23420), -- 36727
(36708, 'deDE', 'Waffendieb der Borstennacken', '', NULL, NULL, 23420), -- 36708
(36697, 'deDE', 'Dornenrufer der Borstennacken', '', NULL, NULL, 23420), -- 36697
(37156, 'deDE', 'Dritter Trog', '', NULL, NULL, 23420), -- 37156
(37155, 'deDE', 'Zweiter Trog', '', NULL, NULL, 23420), -- 37155
(36696, 'deDE', 'Gepanzerter Kampfeber', '', NULL, NULL, 23420), -- 36696
(43720, 'deDE', '"Piekser" Dornenmantel', '', NULL, NULL, 23420), -- 43720
(36712, 'deDE', 'Häuptling Quieker Dornenmantel', '', NULL, NULL, 23420), -- 36712
(36942, 'deDE', 'Junger Kriegerheld', '', NULL, NULL, 23420), -- 36942
(7975, 'deDE', 'Kriegerheld von Camp Narache', '', NULL, NULL, 23420), -- 7975
(2955, 'deDE', 'Ebenenschreiter', '', NULL, NULL, 23420), -- 2955
(2951, 'deDE', 'Wilderer der Bleichmähnen', '', NULL, NULL, 23420), -- 2951
(2950, 'deDE', 'Kürschner der Bleichmähnen', '', NULL, NULL, 23420), -- 2950
(2949, 'deDE', 'Gerber der Bleichmähnen', '', NULL, NULL, 23420), -- 2949
(2962, 'deDE', 'Harpyie der Windfurien', '', NULL, NULL, 23420), -- 2962
(2963, 'deDE', 'Windhexe der Windfurien', '', NULL, NULL, 23420), -- 2963
(62462, 'deDE', 'Tseh Rah, Friedensblumenteemeisterin', '', 'Mönchslehrerin', NULL, 23420), -- 62462
(51639, 'deDE', 'Kador Wolkenlied', '', 'Schamanenlehrer', NULL, 23420), -- 51639
(51638, 'deDE', 'Garn Wolkenlied', '', 'Schamanenlehrer', NULL, 23420), -- 51638
(3044, 'deDE', 'Miles Welsch', '', 'Priesterlehrer', NULL, 23420), -- 3044
(5957, 'deDE', 'Birgitte Kranstein', '', 'Portallehrerin', NULL, 23420), -- 5957
(3047, 'deDE', 'Erzmagier Shymm', '', 'Magierlehrer', NULL, 23420), -- 3047
(43892, 'deDE', 'Morairania Horton', '', 'Hexenmeisterlehrerin', NULL, 23420), -- 43892
(43883, 'deDE', 'Jensen Thomasson', '', 'Hexenmeisterlehrer', NULL, 23420), -- 43883
(3048, 'deDE', 'Ursyn Ghull', '', 'Magierlehrerin', NULL, 23420), -- 3048
(43881, 'deDE', 'Delano Morisett', '', 'Hexenmeisterlehrer', NULL, 23420), -- 43881
(37185, 'deDE', 'Lastkodo der Grimmtotems', '', NULL, NULL, 23420), -- 37185
(37058, 'deDE', 'Baine Bluthuf', '', 'Oberhäuptling', NULL, 23420), -- 37058
(36931, 'deDE', 'Orno Grimmtotem', '', NULL, NULL, 23420), -- 36931
(37175, 'deDE', 'Kriegerheld von Donnerfels', '', NULL, NULL, 23420), -- 37175
(37178, 'deDE', 'Verteidiger der Grimmtotems', '', NULL, NULL, 23420), -- 37178
(2965, 'deDE', 'Matriarchin der Windfurien', '', NULL, NULL, 23420), -- 2965
(3233, 'deDE', 'Wissenshüter Regentotem', '', NULL, NULL, 23420), -- 3233
(3052, 'deDE', 'Skorn Wolkenweiß', '', NULL, 'repair', 23420), -- 3052
(10636, 'deDE', 'Lastkodo', '', NULL, NULL, 23420), -- 10636
(2987, 'deDE', 'Eyahn Adlerklaue', '', NULL, NULL, 23420), -- 2987
(5807, 'deDE', 'Der Kratzer', '', NULL, NULL, 23420), -- 5807
(2964, 'deDE', 'Zauberin der Windfurien', '', NULL, NULL, 23420), -- 2964
(2977, 'deDE', 'Zuchtmeister der Venture Co.', '', NULL, NULL, 23420), -- 2977
(2976, 'deDE', 'Hilfsarbeiter der Venture Co.', '', NULL, NULL, 23420), -- 2976
(2988, 'deDE', 'Morin Wolkenpirscher', '', NULL, NULL, 23420), -- 2988
(2958, 'deDE', 'Präriewolf', '', NULL, NULL, 23420), -- 2958
(2970, 'deDE', 'Sturzflieger', '', NULL, NULL, 23420), -- 2970
(2972, 'deDE', 'Kodokalb', '', NULL, NULL, 23420), -- 2972
(2974, 'deDE', 'Kodomatriarchin', '', NULL, NULL, 23420), -- 2974
(2979, 'deDE', 'Vorsteher der Venture Co.', '', NULL, NULL, 23420), -- 2979
(2978, 'deDE', 'Tagelöhner der Venture Co.', '', NULL, NULL, 23420), -- 2978
(2969, 'deDE', 'Drahtiger Sturzflieger', '', NULL, NULL, 23420), -- 2969
(2956, 'deDE', 'Ausgewachsener Ebenenschreiter', '', NULL, NULL, 23420), -- 2956
(2959, 'deDE', 'Präriepirscher', '', NULL, NULL, 23420), -- 2959
(6271, 'deDE', 'Maus', '', NULL, NULL, 23420), -- 6271
(2971, 'deDE', 'Kralliger Sturzflieger', '', NULL, NULL, 23420), -- 2971
(61143, 'deDE', 'Maus', '', NULL, 'wildpetcapturable', 23420), -- 61143
(2957, 'deDE', 'Alter Ebenenschreiter', '', NULL, NULL, 23420), -- 2957
(3566, 'deDE', 'Flachlandstreuner', '', NULL, NULL, 23420), -- 3566
(62176, 'deDE', 'Gazellenkitz', '', NULL, 'wildpetcapturable', 23420), -- 62176
(2960, 'deDE', 'Präriewolfalpha', '', NULL, NULL, 23420), -- 2960
(34281, 'deDE', '[DND]Azeroth Children''s Week Trigger', '', NULL, NULL, 23420), -- 34281
(2994, 'deDE', 'Geist der Ahnen', '', NULL, NULL, 23420), -- 2994
(3232, 'deDE', 'Eindringling der Borstennacken', '', NULL, NULL, 23420), -- 3232
(40474, 'deDE', 'Moro Sonnenkorn', '', 'Handwerkswaren', NULL, 23420), -- 40474
(44386, 'deDE', 'Cohanae', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 44386
(44384, 'deDE', 'Sora', '', 'Stallmeister', NULL, 23420), -- 44384
(40473, 'deDE', 'Tono', '', 'Windreitermeister', NULL, 23420), -- 40473
(40467, 'deDE', 'Adene Baumtotem', '', 'Gastwirtin', NULL, 23420), -- 40467
(39377, 'deDE', 'Konu Runentotem', '', NULL, NULL, 23420), -- 39377
(7776, 'deDE', 'Talo Dornhuf', '', NULL, NULL, 23420), -- 7776
(14227, 'deDE', 'Hissperak', '', NULL, NULL, 23420), -- 14227
(7775, 'deDE', 'Gregan Hopfenspei', '', NULL, NULL, 23420), -- 7775
(5349, 'deDE', 'Arash-ethis', '', NULL, NULL, 23420), -- 5349
(40972, 'deDE', 'Verderbter Klippengigant', '', NULL, NULL, 23420), -- 40972
(39339, 'deDE', 'Klippenriese', '', NULL, NULL, 23420), -- 39339
(5366, 'deDE', 'Windruferin der Nordschwingen', '', NULL, NULL, 23420), -- 5366
(8075, 'deDE', 'Edana Hasskralle', '', NULL, NULL, 23420), -- 8075
(5362, 'deDE', 'Harpyie der Nordschwingen', '', NULL, NULL, 23420), -- 5362
(50532, 'deDE', 'Brian Horn', '', NULL, NULL, 23420), -- 50532
(50544, 'deDE', 'Rose', '', NULL, NULL, 23420), -- 50544
(50542, 'deDE', 'Rocky', '', NULL, NULL, 23420), -- 50542
(50538, 'deDE', 'Casey Fulton', '', NULL, NULL, 23420), -- 50538
(50537, 'deDE', 'Lucas Merino', '', NULL, NULL, 23420), -- 50537
(50536, 'deDE', 'Matias Volkert', '', NULL, NULL, 23420), -- 50536
(50533, 'deDE', 'Judah Mehler', '', NULL, NULL, 23420), -- 50533
(50529, 'deDE', 'Jonny Throop', '', NULL, NULL, 23420), -- 50529
(50541, 'deDE', 'Som Vanderpool', '', NULL, NULL, 23420), -- 50541
(50539, 'deDE', 'Brandan Vanderpool', '', NULL, NULL, 23420), -- 50539
(50535, 'deDE', 'Larissa Bussell', '', NULL, NULL, 23420), -- 50535
(50534, 'deDE', 'Mary Ann Skweres', '', NULL, NULL, 23420), -- 50534
(50531, 'deDE', 'Jake Patton', '', NULL, NULL, 23420), -- 50531
(50530, 'deDE', 'Ryan Sly', '', NULL, NULL, 23420), -- 50530
(50543, 'deDE', 'Sandy', '', NULL, NULL, 23420), -- 50543
(39394, 'deDE', 'Lethlas', '', NULL, NULL, 23420), -- 39394
(39834, 'deDE', 'Dream Portal Bunny', '', NULL, NULL, 23420), -- 39834
(40966, 'deDE', 'Selor', '', 'Flugmeister', NULL, 23420), -- 40966
(5345, 'deDE', 'Diamantenkopf', '', NULL, NULL, 23420), -- 5345
(7772, 'deDE', 'Kalin Windsegler', '', NULL, NULL, 23420), -- 7772
(7773, 'deDE', 'Marli Traumläufer', '', NULL, NULL, 23420), -- 7773
(39395, 'deDE', 'Jademir Echobrut', '', NULL, NULL, 23420), -- 39395
(39384, 'deDE', 'Giftiger Welpe', '', NULL, NULL, 23420), -- 39384
(40032, 'deDE', 'Telaron Windsegler', '', NULL, NULL, 23420), -- 40032
(40969, 'deDE', 'Druide der Kralle', '', NULL, NULL, 23420), -- 40969
(40968, 'deDE', 'Andoril', '', 'Gastwirt', NULL, 23420), -- 40968
(40967, 'deDE', 'Afran', '', 'Bogenmacher', NULL, 23420), -- 40967
(40035, 'deDE', 'Erina Weidenkind', '', NULL, NULL, 23420), -- 40035
(44382, 'deDE', 'Veir', '', 'Stallmeister', NULL, 23420), -- 44382
(40224, 'deDE', 'Wutschrammyeti', '', NULL, NULL, 23420), -- 40224
(40194, 'deDE', 'Taleule', '', NULL, NULL, 23420), -- 40194
(7941, 'deDE', 'Mardrack Grünquell', '', 'Speis & Trank', NULL, 23420), -- 7941
(39677, 'deDE', 'Hippogryphenreiter der Mondfederfeste', '', NULL, NULL, 23420), -- 39677
(40051, 'deDE', 'General Skessesh', '', NULL, NULL, 23420), -- 40051
(5343, 'deDE', 'Lady Szallah', '', NULL, NULL, 23420), -- 5343
(11717, 'deDE', 'Bethan Quellwasser', '', NULL, NULL, 23420), -- 11717
(39723, 'deDE', 'Tambre', '', NULL, NULL, 23420), -- 39723
(11825, 'deDE', 'Paige Felixe', '', NULL, NULL, 23420), -- 11825
(11824, 'deDE', 'Erik Felixe', '', NULL, NULL, 23420), -- 11824
(39733, 'deDE', 'Zauberin der Hasskämme', '', NULL, NULL, 23420), -- 39733
(44400, 'deDE', 'Scharmützlerin der Mondfederfeste', '', NULL, NULL, 23420), -- 44400
(39728, 'deDE', 'Krieger der Hasskämme', '', NULL, NULL, 23420), -- 39728
(5327, 'deDE', 'Küstenkriecherschnappklaue', '', NULL, NULL, 23420), -- 5327
(5328, 'deDE', 'Küstenkriechertiefseher', '', NULL, NULL, 23420), -- 5328
(40362, 'deDE', 'Kriegerin der Mondklaue', '', NULL, NULL, 23420), -- 40362
(40226, 'deDE', 'Pratt McGrubben', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 40226
(7946, 'deDE', 'Brannock', '', 'Angellehrer', NULL, 23420), -- 7946
(7945, 'deDE', 'Savanne', '', 'Angelbedarf', NULL, 23420), -- 7945
(7943, 'deDE', 'Harklane', '', 'Fischverkäufer', NULL, 23420), -- 7943
(3700, 'deDE', 'Jadenvis Meeresblick', '', 'Reagenzien', NULL, 23420), -- 3700
(37162, 'deDE', 'Romo''s Half-Size Bunny', '', NULL, NULL, 23420), -- 37162
(10293, 'deDE', 'Dulciea Eismond', '', 'Stoffrüstungshändlerin', NULL, 23420), -- 10293
(7942, 'deDE', 'Faralorn', '', 'Gemischtwaren', NULL, 23420), -- 7942
(7947, 'deDE', 'Vivianna', '', 'Handwerkswaren', NULL, 23420), -- 7947
(39314, 'deDE', 'Jägerin der Mondfederfeste', '', NULL, NULL, 23420), -- 39314
(40036, 'deDE', 'Karonas', '', 'Urtum des Krieges', NULL, 23420), -- 40036
(96485, 'deDE', 'Myshalla Flussbrise', '', 'Alchemistische Maßeinheiten', NULL, 23420), -- 96485
(8157, 'deDE', 'Logannas', '', 'Alchemiebedarf', NULL, 23420), -- 8157
(7949, 'deDE', 'Xylinnia Sternenschein', '', 'Verzauberkunstlehrerin', NULL, 23420), -- 7949
(7948, 'deDE', 'Kylanna Windraunen', '', 'Alchemielehrerin', NULL, 23420), -- 7948
(44397, 'deDE', 'Quantas Lebenshand', '', NULL, NULL, 23420), -- 44397
(44391, 'deDE', 'Gastwirtin Shyria', '', 'Gastwirtin', NULL, 23420), -- 44391
(41383, 'deDE', 'Irela Mondfeder', '', 'Hippogryphenmeisterin', NULL, 23420), -- 41383
(10059, 'deDE', 'Antarius', '', 'Stallmeister', NULL, 23420), -- 10059
(7939, 'deDE', 'Schildwache der Mondfederfeste', '', NULL, NULL, 23420), -- 7939
(14637, 'deDE', 'Zorbin von Schiller', '', NULL, NULL, 23420), -- 14637
(40360, 'deDE', 'Bogenschützin der Mondfederfeste', '', NULL, NULL, 23420), -- 40360
(3936, 'deDE', 'Shandris Mondfeder', '', 'Generalin der Schildwachenarmee', NULL, 23420), -- 3936
(5307, 'deDE', 'Talkreischer', '', NULL, NULL, 23420), -- 5307
(5462, 'deDE', 'Meeresschauer', '', NULL, NULL, 23420), -- 5462
(5461, 'deDE', 'Meereselementar', '', NULL, NULL, 23420), -- 5461
(5359, 'deDE', 'Uferschreiter', '', NULL, NULL, 23420), -- 5359
(54533, 'deDE', 'Prinz Lakma', '', 'Der letzte Chimaerok', NULL, 23420), -- 54533
(52562, 'deDE', 'Johnny Imba', '', NULL, NULL, 23420), -- 52562
(51943, 'deDE', 'Resortbesucher', '', NULL, NULL, 23420), -- 51943
(51935, 'deDE', 'Resortbesucher', '', NULL, NULL, 23420), -- 51935
(51946, 'deDE', 'Resortbesucher', '', NULL, NULL, 23420), -- 51946
(52008, 'deDE', 'Resortangestellter', '', NULL, NULL, 23420), -- 52008
(51942, 'deDE', 'Resortbesucher', '', NULL, NULL, 23420), -- 51942
(51944, 'deDE', 'Resortbesucher', '', NULL, NULL, 23420), -- 51944
(51936, 'deDE', 'Resortbesucher', '', NULL, NULL, 23420), -- 51936
(51734, 'deDE', 'Gordon Tramsay', '', 'Chefkoch', NULL, 23420),
(51733, 'deDE', 'Lasha Schaltrad', '', 'Resortdirektorin', NULL, 23420), -- 51733
(51717, 'deDE', 'Resortbesucher', '', NULL, NULL, 23420), -- 51717
(51735, 'deDE', 'God Wiedelspann', '', 'Stellvertretender Resortdirektor', NULL, 23420), -- 51735
(51758, 'deDE', 'Draeneikind', '', NULL, NULL, 23420), -- 51758
(51755, 'deDE', 'Menschenjunge', '', NULL, NULL, 23420), -- 51755
(51726, 'deDE', 'Großer Roc', '', NULL, NULL, 23420), -- 51726
(51756, 'deDE', 'Blutelfenjunge', '', NULL, NULL, 23420), -- 51756
(51719, 'deDE', 'Reitmammut', '', NULL, NULL, 23420), -- 51719
(51718, 'deDE', 'Reitmammut', '', NULL, NULL, 23420), -- 51718
(54228, 'deDE', 'Resortangestellter', '', NULL, NULL, 23420), -- 54228
(51757, 'deDE', 'Orcjunge', '', NULL, NULL, 23420), -- 51757
(51865, 'deDE', 'Kokosnuss', '', NULL, 'LootAll', 23420), -- 51865
(53418, 'deDE', 'Resortangestellter', '', NULL, NULL, 23420), -- 53418
(51722, 'deDE', 'Resortangestellter', '', NULL, NULL, 23420), -- 51722
(51945, 'deDE', 'Resortbesucher', '', NULL, NULL, 23420), -- 51945
(51941, 'deDE', 'Resortbesucher', '', NULL, NULL, 23420), -- 51941
(51750, 'deDE', 'Trip Rumpfbolz', '', NULL, NULL, 23420), -- 51750
(52057, 'deDE', 'Glückseimer', '', NULL, 'vehichleCursor', 23420), -- 52057
(51714, 'deDE', 'Königskriecher', '', NULL, NULL, 23420), -- 51714
(32780, 'deDE', 'Invisible Stalker (All Phases)', '', NULL, NULL, 23420), -- 32780
(43896, 'deDE', 'Feralas 4.x Chogall Bunny', '', NULL, NULL, 23420), -- 43896
(15587, 'deDE', 'Urahnin Nebelgänger', '', NULL, NULL, 23420), -- 15587
(11442, 'deDE', 'Raufer der Gordok', '', NULL, NULL, 23420), -- 11442
(5346, 'deDE', 'Blutschrei der Pirscher', '', NULL, NULL, 23420), -- 5346
(68805, 'deDE', 'Verkümmerter Yeti', '', NULL, 'wildpetcapturable', 23420), -- 68805
(7807, 'deDE', 'Leitroboter OOX-22/FE', '', NULL, NULL, 23420), -- 7807
(92462, 'deDE', 'Controller Bunny', '', NULL, NULL, 23420), -- 92462
(39896, 'deDE', 'Wildschrammyeti', '', NULL, NULL, 23420), -- 39896
(40052, 'deDE', 'Estulan', '', 'Die Hochgeborenen', NULL, 23420), -- 40052
(44381, 'deDE', 'Schülerin von Estulan', '', 'Verzauberkunstbedarf', NULL, 23420), -- 44381
(40493, 'deDE', 'Arcosin', '', 'Urtum des Arkanen', NULL, 23420), -- 40493
(39653, 'deDE', 'Silvia', '', NULL, NULL, 23420), -- 39653
(41580, 'deDE', 'Aryenda', '', 'Flugmeisterin', NULL, 23420), -- 41580
(40496, 'deDE', 'Schüler von Estulan', '', NULL, NULL, 23420), -- 40496
(40078, 'deDE', 'Tierführerin Tessina', '', NULL, NULL, 23420), -- 40078
(40497, 'deDE', 'Schülerin von Estulan', '', NULL, NULL, 23420), -- 40497
(40132, 'deDE', 'Vestia Mondspeer', '', NULL, NULL, 23420), -- 40132
(37522, 'deDE', 'Romo''s Standard Bunny', '', NULL, NULL, 23420), -- 37522
(5347, 'deDE', 'Antilus der Aufsteiger', '', NULL, NULL, 23420), -- 5347
(5304, 'deDE', 'Fransenfederflügler', '', NULL, NULL, 23420), -- 5304
(5300, 'deDE', 'Fransenfederhippogryph', '', NULL, NULL, 23420), -- 5300
(5240, 'deDE', 'Hexenmeister der Gordunni', '', NULL, NULL, 23420), -- 5240
(5234, 'deDE', 'Raufer der Gordunni', '', NULL, NULL, 23420), -- 5234
(5236, 'deDE', 'Schamane der Gordunni', '', NULL, NULL, 23420), -- 5236
(66364, 'deDE', 'Glimmer', '', NULL, NULL, 23420), -- 66364
(66361, 'deDE', 'Tänzler', '', NULL, NULL, 23420), -- 66361
(66363, 'deDE', 'Kratz', '', NULL, NULL, 23420), -- 66363
(66352, 'deDE', 'Gluk der Verräter', '', 'Meistertierzähmer', NULL, 23420), -- 66352
(40079, 'deDE', 'Irrwisch von Feralas', '', NULL, NULL, 23420), -- 40079
(41605, 'deDE', 'Mergek', '', 'Windreitermeister', NULL, 23420), -- 41605
(44377, 'deDE', 'Kerthunk', '', 'Schmiedekunstbedarf', NULL, 23420), -- 44377
(44379, 'deDE', 'Lucretia', '', 'Speis & Trank', NULL, 23420), -- 44379
(44378, 'deDE', 'Ajaye', '', 'Stallmeister', NULL, 23420), -- 44378
(40498, 'deDE', 'Wuu', '', 'Schweineführer', NULL, 23420), -- 40498
(39840, 'deDE', 'Swar''jan', '', NULL, NULL, 23420), -- 39840
(44376, 'deDE', 'Chonk', '', 'Gastwirt', NULL, 23420), -- 44376
(39656, 'deDE', 'Orhan Ogerklinge', '', NULL, NULL, 23420), -- 39656
(39889, 'deDE', 'Steinbrecherschwein', '', NULL, NULL, 23420), -- 39889
(39894, 'deDE', 'Gombana', '', 'Hexenkunstlehrling', NULL, 23420), -- 39894
(39657, 'deDE', 'Oger der Steinbrecher', '', NULL, NULL, 23420), -- 39657
(7854, 'deDE', 'Jangdor Flinkschreiter', '', 'Lederverarbeitungsbedarf', NULL, 23420), -- 7854
(12418, 'deDE', 'Hyäne der Gordok', '', NULL, NULL, 23420), -- 12418
(62395, 'deDE', 'Netherfeendrache', '', NULL, 'wildpetcapturable', 23420), -- 62395
(11443, 'deDE', 'Ogermagier der Gordok', '', NULL, NULL, 23420), -- 11443
(11440, 'deDE', 'Vollstrecker der Gordok', '', NULL, NULL, 23420), -- 11440
(40193, 'deDE', 'Scharfhornhirsch', '', NULL, NULL, 23420), -- 40193
(40367, 'deDE', 'Seyala Elmsfeuer', '', 'Hippogryphenmeisterin', NULL, 23420), -- 40367
(40369, 'deDE', 'Falfindel Wegeshut', '', NULL, NULL, 23420), -- 40369
(39725, 'deDE', 'Adella', '', NULL, NULL, 23420), -- 39725
(44385, 'deDE', 'Cymerdi', '', 'Waffenverkäufer', NULL, 23420), -- 44385
(40914, 'deDE', 'Versorgerin Tria', '', 'Gemischtwarenhändlerin', NULL, 23420), -- 40914
(40913, 'deDE', 'Tierführerin Jesana', '', NULL, NULL, 23420), -- 40913
(14661, 'deDE', 'Zornstachel', '', NULL, NULL, 23420), -- 14661
(5350, 'deDE', 'Qirot', '', NULL, NULL, 23420), -- 5350
(5244, 'deDE', 'Stecher der Zukk''ash', '', NULL, NULL, 23420), -- 5244
(5245, 'deDE', 'Wespe der Zukk''ash', '', NULL, NULL, 23420), -- 5245
(5246, 'deDE', 'Arbeiter der Zukk''ash', '', NULL, NULL, 23420), -- 5246
(40129, 'deDE', 'Verschollener Lehrling', '', NULL, NULL, 23420), -- 40129
(40059, 'deDE', 'Hochgeborener Poltergeist', '', NULL, NULL, 23420), -- 40059
(5247, 'deDE', 'Tunnelgräber der Zukk''ash', '', NULL, NULL, 23420), -- 5247
(11447, 'deDE', 'Mushgog', '', NULL, NULL, 23420), -- 11447
(40026, 'deDE', 'Bergriese', '', NULL, NULL, 23420), -- 40026
(5255, 'deDE', 'Häscher der Waldpfoten', '', NULL, NULL, 23420), -- 5255
(5258, 'deDE', 'Alpha der Waldpfoten', '', NULL, NULL, 23420), -- 5258
(7957, 'deDE', 'Jer''kai Mondweber', '', NULL, NULL, 23420), -- 7957
(7956, 'deDE', 'Kindal Mondweber', '', NULL, NULL, 23420), -- 7956
(7997, 'deDE', 'Gefangener Grimmlingflitzer', '', NULL, NULL, 23420), -- 7997
(7727, 'deDE', 'Schamane der Grimmtotems', '', NULL, NULL, 23420), -- 7727
(7725, 'deDE', 'Räuber der Grimmtotems', '', NULL, NULL, 23420), -- 7725
(5354, 'deDE', 'Laubbruder Knarz', '', NULL, NULL, 23420), -- 5354
(51920, 'deDE', 'Kriegerheld von Camp Mojache', '', NULL, NULL, 23420), -- 51920
(8142, 'deDE', 'Jannos Leichthuf', '', 'Druidenlehrer', NULL, 23420), -- 8142
(8115, 'deDE', 'Hexendoktor Uzer''i', '', NULL, NULL, 23420), -- 8115
(14373, 'deDE', 'Weiser Korolusk', '', NULL, NULL, 23420), -- 14373
(9986, 'deDE', 'Shyrka Wolfsläufer', '', 'Stallmeisterin', NULL, 23420), -- 9986
(11752, 'deDE', 'Blaise Montgomery', '', NULL, NULL, 23420), -- 11752
(4544, 'deDE', 'Krueg Schädelspalter', '', NULL, NULL, 23420), -- 4544
(11098, 'deDE', 'Hahrana Eisenhaut', '', 'Lederverarbeitungslehrerin', NULL, 23420), -- 11098
(39893, 'deDE', 'Jawn Hochfeld', '', NULL, NULL, 23420), -- 39893
(39847, 'deDE', 'Häuptling Geisterhorn', '', NULL, NULL, 23420), -- 39847
(8144, 'deDE', 'Kulleg Steinhorn', '', 'Kürschnerlehrer', NULL, 23420), -- 8144
(5390, 'deDE', 'Weiser Bleichlauf', '', NULL, NULL, 23420), -- 5390
(8159, 'deDE', 'Worb Starkahle', '', 'Händler für leichte Rüstungen', NULL, 23420), -- 8159
(8145, 'deDE', 'Sheendra Hochschilf', '', 'Handwerkswaren', NULL, 23420), -- 8145
(7875, 'deDE', 'Hadoken Flinkschreiter', '', NULL, NULL, 23420), -- 7875
(5278, 'deDE', 'Grimmlingflitzer', '', NULL, NULL, 23420), -- 5278
(7726, 'deDE', 'Naturalist der Grimmtotems', '', NULL, NULL, 23420), -- 7726
(39958, 'deDE', 'Großfaust', '', NULL, NULL, 23420), -- 39958
(39957, 'deDE', 'Zähmer der Gordunni', '', NULL, NULL, 23420), -- 39957
(15581, 'deDE', 'Urahne Grimmtotem', '', NULL, NULL, 23420), -- 15581
(36208, 'deDE', 'Xerash Feuerklinge', '', NULL, NULL, 23420), -- 36208
(14355, 'deDE', 'Azj''Tordin', '', 'Haus Shen''dralar', NULL, 23420), -- 14355
(39965, 'deDE', 'Kanalisierer der Gordunni', '', NULL, NULL, 23420), -- 39965
(39952, 'deDE', 'Hügelwächter der Gordunni', '', NULL, NULL, 23420), -- 39952
(11498, 'deDE', 'Skarr der Gebrochene', '', NULL, NULL, 23420), -- 11498
(11818, 'deDE', 'Orik''ando', '', NULL, NULL, 23420), -- 11818
(8158, 'deDE', 'Bronk', '', 'Alchemiebedarf', NULL, 23420), -- 8158
(8146, 'deDE', 'Ruw', '', 'Kräuterkundelehrer', NULL, 23420), -- 8146
(8143, 'deDE', 'Loorana', '', 'Speis & Trank', NULL, 23420), -- 8143
(39898, 'deDE', 'Shyn', '', 'Windreitermeisterin', NULL, 23420), -- 39898
(3500, 'deDE', 'Tarhus', '', 'Reagenzien', NULL, 23420), -- 3500
(7737, 'deDE', 'Gastwirtin Greul', '', 'Gastwirtin', NULL, 23420), -- 7737
(5260, 'deDE', 'Groddocaffe', '', NULL, NULL, 23420), -- 5260
(9548, 'deDE', 'Cawind Blattschuss', '', 'Bogen- & Büchsenmacher', NULL, 23420), -- 9548
(14395, 'deDE', 'Griniblix der Beobachter', '', NULL, NULL, 23420), -- 14395
(11497, 'deDE', 'Der Razza', '', NULL, NULL, 23420), -- 11497
(5253, 'deDE', 'Schläger der Waldpfoten', '', NULL, NULL, 23420), -- 5253
(5249, 'deDE', 'Bastard der Waldpfoten', '', NULL, NULL, 23420), -- 5249
(61081, 'deDE', 'Eichhörnchen', '', NULL, 'wildpetcapturable', 23420), -- 61081
(5356, 'deDE', 'Knurrer', '', NULL, NULL, 23420), -- 5356
(40332, 'deDE', 'Flüchtling des Freiwindpostens', '', NULL, NULL, 23420), -- 40332
(8147, 'deDE', 'Kriegerheld von Camp Mojache', '', NULL, NULL, 23420), -- 8147
(883, 'deDE', 'Reh', '', NULL, NULL, 23420), -- 883
(5254, 'deDE', 'Mystiker der Waldpfoten', '', NULL, NULL, 23420), -- 5254
(5251, 'deDE', 'Fallensteller der Waldpfoten', '', NULL, NULL, 23420), -- 5251
(5268, 'deDE', 'Eisenfellbär', '', NULL, NULL, 23420), -- 5268
(7584, 'deDE', 'Wandernder Waldgänger', '', NULL, NULL, 23420), -- 7584
(5286, 'deDE', 'Langzahnläufer', '', NULL, NULL, 23420), -- 5286
(61080, 'deDE', 'Kaninchen', '', NULL, 'wildpetcapturable', 23420), -- 61080
(39946, 'deDE', 'Caryssia Jägermond', '', 'Anführerin von Neu-Thalanaar', NULL, 23420), -- 39946
(2914, 'deDE', 'Schlange', '', NULL, NULL, 23420), -- 2914
(61142, 'deDE', 'Schlange', '', NULL, 'wildpetcapturable', 23420), -- 61142
(40168, 'deDE', 'Verderbte Dryade', '', NULL, NULL, 23420), -- 40168
(12921, 'deDE', 'Wütende Fäulnisklaue', '', NULL, NULL, 23360), -- 12921
(34618, 'deDE', 'Ota Wen', '', NULL, NULL, 23360), -- 34618
(3891, 'deDE', 'Teronis'' Leichnam', '', NULL, NULL, 23360), -- 3891
(33443, 'deDE', 'Bewahrer Herzensweg', '', NULL, NULL, 23360), -- 33443
(33419, 'deDE', 'Ranke aus der Unterwelt', '', NULL, NULL, 23360), -- 33419
(24734, 'deDE', 'Aleanna Edune', '', NULL, NULL, 23360), -- 24734
(24739, 'deDE', 'Benjari Edune', '', NULL, NULL, 23360), -- 24739
(24737, 'deDE', 'William Dunadaire', '', NULL, NULL, 23360), -- 24737
(24736, 'deDE', 'Sulan Dunadaire', '', NULL, NULL, 23360), -- 24736
(24735, 'deDE', 'Becanna Edune', '', NULL, NULL, 23360), -- 24735
(24740, 'deDE', 'Gutsel', '', 'Elenna Edunes Tier', NULL, 23360), -- 24740
(24738, 'deDE', 'Elenna Edune', '', NULL, NULL, 23360), -- 24738
(3774, 'deDE', 'Teufelstöter', '', NULL, NULL, 23360), -- 3774
(3772, 'deDE', 'Niedere Teufelswache', '', NULL, NULL, 23360), -- 3772
(10644, 'deDE', 'Nebelheuler', '', NULL, NULL, 23360), -- 10644
(66331, 'deDE', 'Herr Zwickzwack', '', NULL, NULL, 23360), -- 66331
(66330, 'deDE', 'Schleimer', '', NULL, NULL, 23360), -- 66330
(66329, 'deDE', 'Flatterling', '', NULL, NULL, 23360), -- 66329
(66136, 'deDE', 'Analynn', '', 'Meistertierzähmerin', NULL, 23360), -- 66136
(43606, 'deDE', 'Gastwirtin Duras', '', 'Gastwirtin', NULL, 23360), -- 43606
(43621, 'deDE', 'Targol', '', NULL, NULL, 23360), -- 43621
(43619, 'deDE', 'Beh''tor', '', 'Gifte & Reagenzien', NULL, 23360), -- 43619
(43617, 'deDE', 'Lursa', '', 'Stallmeisterin', NULL, 23360), -- 43617
(12717, 'deDE', 'Muglash', '', NULL, NULL, 23360), -- 12717
(12863, 'deDE', 'Läufer des Kriegshymnenklans', '', NULL, NULL, 23360), -- 12863
(12719, 'deDE', 'Marukai', '', NULL, NULL, 23360), -- 12719
(34122, 'deDE', 'Kommandant Grimmfang', '', NULL, NULL, 23360), -- 34122
(34303, 'deDE', 'Dagrun Zornhammer', '', 'Waffenschmied', NULL, 23360), -- 34303
(43615, 'deDE', 'Toral', '', 'Schmiedekunstbedarf', NULL, 23360), -- 43615
(11901, 'deDE', 'Andruk', '', 'Windreitermeister', NULL, 23360), -- 11901
(12962, 'deDE', 'Wik''Tar', '', 'Fischhändler & Angelbedarf', NULL, 23360), -- 12962
(12961, 'deDE', 'Kil''Hiwana', '', 'Angellehrerin', NULL, 23360), -- 12961
(34329, 'deDE', 'Zoram''gar Lighthouse Bunny', '', NULL, NULL, 23360), -- 34329
(3711, 'deDE', 'Myrmidone der Rächerflossen', '', NULL, NULL, 23360), -- 3711
(3944, 'deDE', 'Priesterin der Rächerflossen', '', NULL, NULL, 23360), -- 3944
(3943, 'deDE', 'Ruuzel', '', NULL, NULL, 23360), -- 3943
(3721, 'deDE', 'Nebelpeitscherhydra', '', NULL, NULL, 23360), -- 3721
(10559, 'deDE', 'Lady Vespia', '', NULL, NULL, 23360), -- 10559
(4802, 'deDE', 'Gezeitenpriesterin der Tiefschwarzen Grotte', '', NULL, NULL, 23360), -- 4802
(3814, 'deDE', 'Stachelkriecher', '', NULL, NULL, 23360), -- 3814
(3812, 'deDE', 'Rasselnder Kriecher', '', NULL, NULL, 23360), -- 3812
(62313, 'deDE', 'Rostschnecke', '', NULL, 'wildpetcapturable', 23360), -- 62313
(3713, 'deDE', 'Wellenreiter der Rächerflossen', '', NULL, NULL, 23360), -- 3713
(3717, 'deDE', 'Zauberin der Rächerflossen', '', NULL, NULL, 23360), -- 3717
(34374, 'deDE', 'Solais', '', 'Flugmeisterin', NULL, 23360), -- 34374
(33265, 'deDE', 'Varas', '', 'Waffenverkäufer', NULL, 23360), -- 33265
(3846, 'deDE', 'Talen', '', NULL, NULL, 23360), -- 3846
(3845, 'deDE', 'Shindrell Feuerflink', '', NULL, NULL, 23360), -- 3845
(33209, 'deDE', 'Visera Lößlehm', '', 'Speis & Trank', NULL, 23360), -- 33209
(33208, 'deDE', 'Orendils Lehrling', '', NULL, NULL, 23360), -- 33208
(33204, 'deDE', 'Evenar Stillwisper', '', NULL, NULL, 23360), -- 33204
(33187, 'deDE', 'Schildwache Shyela', '', NULL, NULL, 23360), -- 33187
(33182, 'deDE', 'Bathran', '', NULL, NULL, 23360), -- 33182
(33183, 'deDE', 'Bathrans Leichnam', '', NULL, NULL, 23360), -- 33183
(3734, 'deDE', 'Orcischer Aufseher', '', NULL, NULL, 23360), -- 3734
(3733, 'deDE', 'Kräuterkundiger der Verlassenen', '', NULL, NULL, 23360), -- 3733
(3730, 'deDE', 'Ausgräber des Dunklen Strangs', '', NULL, NULL, 23360), -- 3730
(3728, 'deDE', 'Adept des Dunklen Strangs', '', NULL, NULL, 23360), -- 3728
(3727, 'deDE', 'Vollstrecker des Dunklen Strangs', '', NULL, NULL, 23360), -- 3727
(33389, 'deDE', 'Weiser der Distelfelle', '', NULL, NULL, 23360), -- 33389
(33390, 'deDE', 'Krieger der Distelfelle', '', NULL, NULL, 23360), -- 33390
(3725, 'deDE', 'Kultist des Dunklen Strangs', '', NULL, NULL, 23360), -- 3725
(33445, 'deDE', 'Schildwache Avana', '', NULL, NULL, 23360), -- 33445
(11219, 'deDE', 'Liladris Mondbach', '', NULL, NULL, 23360), -- 11219
(33381, 'deDE', 'Tomarrek', '', 'Rüstungsverkäufer', NULL, 23360), -- 33381
(33276, 'deDE', 'Mondpriesterin Maestra', '', NULL, NULL, 23360), -- 33276
(3847, 'deDE', 'Orendil Rundblatt', '', NULL, NULL, 23360), -- 3847
(33375, 'deDE', 'Verletzte Schildwache', '', NULL, NULL, 23360), -- 33375
(33278, 'deDE', 'Schildwache von Maestras Posten', '', NULL, NULL, 23360), -- 33278
(11806, 'deDE', 'Schildwache Onaeya', '', NULL, NULL, 23360), -- 11806
(33348, 'deDE', 'Feero Eisenhand', '', NULL, 'LootAll', 23360), -- 33348
(33444, 'deDE', 'Bote Aphotic', '', NULL, NULL, 23360), -- 33444
(34608, 'deDE', 'Großer Baobob', '', 'Urtum des Krieges', NULL, 23360), -- 34608
(33347, 'deDE', 'Delgren der Läuterer', '', NULL, 'LootAll', 23360), -- 33347
(33356, 'deDE', 'Orux der dreimal Verdammte', '', NULL, NULL, 23360), -- 33356
(33334, 'deDE', 'Eindringling der Horde', '', NULL, NULL, 23360), -- 33334
(33283, 'deDE', 'Zerstörer des Eschentals', '', NULL, NULL, 23360), -- 33283
(33407, 'deDE', 'Goblinpilot', '', NULL, NULL, 23360), -- 33407
(3987, 'deDE', 'Dal Blutklaue', '', NULL, NULL, 23360), -- 3987
(12818, 'deDE', 'Ruul Schneehuf', '', NULL, NULL, 23360), -- 12818
(3922, 'deDE', 'Totemiker der Distelfelle', '', NULL, NULL, 23360), -- 3922
(34251, 'deDE', 'Vear Dunkelschnauze', '', NULL, NULL, 23360), -- 34251
(10639, 'deDE', 'Grummelkehle', '', NULL, NULL, 23360), -- 10639
(12757, 'deDE', 'Karang Amakkar', '', NULL, NULL, 23360), -- 12757
(34429, 'deDE', 'Thraka', '', 'Windreitermeisterin', NULL, 23360), -- 34429
(34424, 'deDE', 'Broken-down Wagon Bunny', '', NULL, NULL, 23360), -- 34424
(3921, 'deDE', 'Ursa der Distelfelle', '', NULL, NULL, 23360), -- 3921
(3925, 'deDE', 'Rächer der Distelfelle', '', NULL, NULL, 23360), -- 3925
(3926, 'deDE', 'Pfadfinder der Distelfelle', '', NULL, NULL, 23360), -- 3926
(3924, 'deDE', 'Schamane der Distelfelle', '', NULL, NULL, 23360), -- 3924
(43630, 'deDE', 'Drek', '', 'Stallmeister', NULL, 23360), -- 43630
(43624, 'deDE', 'Gastwirtin Linkasa', '', 'Gastwirtin', NULL, 23360), -- 43624
(34510, 'deDE', 'Broyk', '', NULL, NULL, 23360), -- 34510
(12721, 'deDE', 'Mitsuwa', '', NULL, NULL, 23360), -- 12721
(34359, 'deDE', 'Hauptmann Goggath', '', NULL, NULL, 23360), -- 34359
(11137, 'deDE', 'Xai''ander', '', 'Waffenschmied', NULL, 23360), -- 11137
(3967, 'deDE', 'Aayndia Blütenwind', '', 'Lederverarbeitungslehrerin', NULL, 23360), -- 3967
(3954, 'deDE', 'Dalria', '', 'Handwerkswaren', NULL, 23360), -- 3954
(6738, 'deDE', 'Gastwirtin Kimlya', '', 'Gastwirtin', NULL, 23360), -- 6738
(3959, 'deDE', 'Nantar', '', 'Bäcker', NULL, 23360), -- 3959
(3915, 'deDE', 'Dagri', '', 'Raenes Begleiter', NULL, 23360), -- 3915
(10052, 'deDE', 'Maluressier', '', 'Stallmeister', NULL, 23360), -- 10052
(3961, 'deDE', 'Maliynn', '', 'Speis & Trank', NULL, 23360), -- 3961
(3691, 'deDE', 'Raene Wolfsläufer', '', NULL, NULL, 23360), -- 3691
(3888, 'deDE', 'Korra', '', NULL, NULL, 23360), -- 3888
(34239, 'deDE', 'Hephaestus Pilger', '', NULL, NULL, 23360), -- 34239
(3969, 'deDE', 'Fahran Flüsterklinge', '', 'Giftverkäufer', NULL, 23420),
(51770, 'deDE', 'Schildwache von Astranaar', '', NULL, NULL, 23360), -- 51770
(33512, 'deDE', 'Palanaars Elekk', '', NULL, NULL, 23360), -- 33512
(3996, 'deDE', 'Faldreas Goeth''Shael', '', NULL, NULL, 23360), -- 3996
(3962, 'deDE', 'Haljan Eichenherz', '', 'Gemischtwaren', NULL, 23360), -- 3962
(17106, 'deDE', 'Verteidigerin Palanaar', '', NULL, NULL, 23360), -- 17106
(15605, 'deDE', 'Urahnin Flussweise', '', NULL, NULL, 23360), -- 15605
(4079, 'deDE', 'Schildwache Thenysil', '', NULL, NULL, 23360), -- 4079
(3958, 'deDE', 'Lardan', '', 'Lederverarbeitungsbedarf', NULL, 23360), -- 3958
(3953, 'deDE', 'Tandaan Lichthaupt', '', 'Lederrüstungshändler', NULL, 23360), -- 3953
(3952, 'deDE', 'Aeolynn', '', 'Tuchmacherin', NULL, 23360), -- 3952
(33454, 'deDE', 'Schildwache Luara', '', NULL, NULL, 23360), -- 33454
(3970, 'deDE', 'Llana', '', 'Reagenzienbedarf', NULL, 23360), -- 3970
(33451, 'deDE', 'Eschentalnachtsäbler', '', NULL, NULL, 23360), -- 33451
(4267, 'deDE', 'Daelyshia', '', 'Hippogryphenmeisterin', NULL, 23360), -- 4267
(43625, 'deDE', 'Shenara', '', 'Schmiedekunstbedarf', NULL, 23360), -- 43625
(34395, 'deDE', 'Twidel', '', NULL, NULL, 23360), -- 34395
(43653, 'deDE', 'Höllschreis Grunzer', '', NULL, NULL, 23360), -- 43653
(34420, 'deDE', 'Offizier von Astranaar', '', NULL, NULL, 23360), -- 34420
(34419, 'deDE', 'Scharmützler von Astranaar', '', NULL, NULL, 23360), -- 34419
(3894, 'deDE', 'Pelturas Weißmond', '', NULL, NULL, 23360), -- 3894
(3892, 'deDE', 'Relara Weißmond', '', NULL, NULL, 23360), -- 3892
(34132, 'deDE', 'Schleuder von Astranaar', '', NULL, 'vehichleCursor', 23360), -- 34132
(34123, 'deDE', 'Astranaar''s Burning! Fire Bunny', '', NULL, NULL, 23360), -- 34123
(6087, 'deDE', 'Schildwache von Astranaar', '', NULL, NULL, 23360), -- 6087
(34160, 'deDE', 'Wachwindreiter', '', NULL, NULL, 23360), -- 34160
(34163, 'deDE', 'Höllschreis Höllenhund', '', NULL, NULL, 23360), -- 34163
(12678, 'deDE', 'Ursangus', '', NULL, NULL, 23360), -- 12678
(3781, 'deDE', 'Schattendickichtholzformer', '', NULL, NULL, 23360), -- 3781
(34378, 'deDE', 'Myre Mondgleit', '', 'Flugmeisterin', NULL, 23360), -- 34378
(3951, 'deDE', 'Bhaldaran Rabenschwarz', '', 'Bogenmacher', NULL, 23360), -- 3951
(34354, 'deDE', 'Jägerin Jalin', '', 'Die Meisterschützin', NULL, 23360), -- 34354
(40895, 'deDE', 'Professor Xakxak Gyromat', '', NULL, NULL, 23360), -- 40895
(40894, 'deDE', 'Gnombus, der X-Terminator', '', NULL, NULL, 23360), -- 40894
(3885, 'deDE', 'Schildwache Velene Sternenschlag', '', NULL, NULL, 23360), -- 3885
(3783, 'deDE', 'Schattendickichtregenrufer', '', NULL, NULL, 23360), -- 3783
(10642, 'deDE', 'Eck''alom', '', NULL, NULL, 23360), -- 10642
(43063, 'deDE', 'Allumpass', '', NULL, NULL, 23360), -- 43063
(34559, 'deDE', 'Hauptmann Tarkan', '', NULL, NULL, 23360), -- 34559
(3963, 'deDE', 'Danlaar Nachtwandler', '', 'Jägerlehrer', NULL, 23360), -- 3963
(34569, 'deDE', 'Flooz', '', NULL, NULL, 23360), -- 34569
(3964, 'deDE', 'Kylanna', '', 'Alchemielehrerin', NULL, 23360), -- 3964
(41680, 'deDE', 'Späher Utvoch', '', NULL, NULL, 23360), -- 41680
(41679, 'deDE', 'Unteroffizier Dontrag', '', NULL, NULL, 23360), -- 41679
(41678, 'deDE', 'Blutwache Aldo Steinregen', '', NULL, NULL, 23360), -- 41678
(41681, 'deDE', 'Infanterist der Horde', '', NULL, NULL, 23360), -- 41681
(34943, 'deDE', 'Windzähmerin Shoshok', '', 'Flugmeisterin', NULL, 23360), -- 34943
(3965, 'deDE', 'Cylania Dornenpirscher', '', 'Kräuterkundelehrerin', NULL, 23360), -- 3965
(41744, 'deDE', 'Wagen von Krom''gar', '', NULL, 'vehichleCursor', 23360), -- 41744
(41743, 'deDE', 'Karawanenführer von Krom''gar', '', NULL, NULL, 23360), -- 41743
(6288, 'deDE', 'Jayla', '', 'Kürschnerlehrerin', NULL, 23360), -- 6288
(4320, 'deDE', 'Caelyb', '', 'Tierausbilder', NULL, 23360), -- 4320
(3960, 'deDE', 'Ulthaan', '', 'Metzger', NULL, 23360), -- 3960
(34617, 'deDE', 'Gefangener Furbolg', '', NULL, NULL, 23360), -- 34617
(3956, 'deDE', 'Harklan Mondhain', '', 'Alchemiebedarf', NULL, 23360), -- 3956
(12696, 'deDE', 'Senani Donnerherz', '', NULL, NULL, 23360), -- 12696
(43633, 'deDE', 'Gastwirt Chin''toka', '', 'Gastwirt', NULL, 23360), -- 43633
(43634, 'deDE', 'Vorcha', '', 'Stallmeisterin', NULL, 23360), -- 43634
(3955, 'deDE', 'Shandrina', '', 'Handwerkswaren', NULL, 23360), -- 3955
(43568, 'deDE', 'Dro Schattenfrei', '', 'Lederverarbeitungsbedarf', NULL, 23360), -- 43568
(52000, 'deDE', 'Schildwache von Astranaar', '', NULL, NULL, 23360), -- 52000
(43646, 'deDE', 'Kitanga', '', 'Gifte & Reagenzien', NULL, 23360), -- 43646
(43657, 'deDE', 'Bezwinger von Silberwind', '', NULL, NULL, 23360), -- 43657
(52161, 'deDE', 'Pfadfinder der Fäulnisklauen', '', NULL, NULL, 23360), -- 52161
(3745, 'deDE', 'Pfadfinder der Fäulnisklauen', '', NULL, NULL, 23360), -- 3745
(3696, 'deDE', 'Ran Blutreißer', '', NULL, NULL, 23360), -- 3696
(3932, 'deDE', 'Wache von Blutreißer', '', NULL, NULL, 23360), -- 3932
(3897, 'deDE', 'Krolg', '', NULL, NULL, 23360), -- 3897
(34597, 'deDE', 'Flooz Target Bunny', '', NULL, NULL, 23360), -- 34597
(12759, 'deDE', 'Tideress', '', NULL, NULL, 23360), -- 12759
(3917, 'deDE', 'Beschmutzter Wasserelementar', '', NULL, NULL, 23360), -- 3917
(40829, 'deDE', 'Toter Gnom', '', 'Versklavter Gnom', NULL, 23360), -- 40829
(40800, 'deDE', 'Minx', '', 'Versklavte Gnomin', NULL, 23360), -- 40800
(40799, 'deDE', 'Boog der "Zahnradflüsterer"', '', 'Versklavter Gnom', NULL, 23360), -- 40799
(39096, 'deDE', 'Schmerzensmeister Thundrak', '', NULL, NULL, 23360), -- 39096
(39071, 'deDE', 'Wagen der nimmer endenden Agonie', '', NULL, NULL, 23360), -- 39071
(39070, 'deDE', 'Harnisch der nimmer endenden Agonie', '', NULL, NULL, 23360), -- 39070
(40820, 'deDE', 'Kriegskodo', '', NULL, NULL, 23360), -- 40820
(34596, 'deDE', 'Vorarbeiter Jinx', '', NULL, NULL, 23360), -- 34596
(34591, 'deDE', 'Leitender Bombentechniker Splodier', '', NULL, NULL, 23360), -- 34591
(34590, 'deDE', 'Goblintechniker', '', NULL, NULL, 23360), -- 34590
(10641, 'deDE', 'Astschnapper', '', NULL, NULL, 23360), -- 10641
(3780, 'deDE', 'Angesengter Schlurfer', '', NULL, NULL, 23360), -- 3780
(3809, 'deDE', 'Eschentalbär', '', NULL, NULL, 23360), -- 3809
(3819, 'deDE', 'Wilddornpirscher', '', NULL, NULL, 23360), -- 3819
(34592, 'deDE', 'Eroberer von Silberwind', '', NULL, NULL, 23360), -- 34592
(3750, 'deDE', 'Totemiker der Fäulnisklauen', '', NULL, NULL, 23360), -- 3750
(3743, 'deDE', 'Krieger der Fäulnisklauen', '', NULL, NULL, 23360), -- 3743
(25670, 'deDE', 'ELM General Purpose Bunny (scale x3)', '', NULL, NULL, 23360), -- 25670
(34283, 'deDE', 'Sabina Pilger', '', NULL, NULL, 23360), -- 34283
(34314, 'deDE', 'Lavawüter', '', NULL, NULL, 23360), -- 34314
(34292, 'deDE', 'Arctanus', '', 'Der Irdene Ring', NULL, 23360), -- 34292
(34290, 'deDE', 'Kern', '', 'Der Irdene Ring', NULL, 23360), -- 34290
(34289, 'deDE', 'Der Vortex', '', 'Der Irdene Ring', NULL, 23360), -- 34289
(8956, 'deDE', 'Wutklauenbär', '', NULL, NULL, 23360), -- 8956
(4273, 'deDE', 'Bewahrer Ordanus', '', NULL, NULL, 23360), -- 4273
(3833, 'deDE', 'Cenarischer Verteidiger', '', NULL, NULL, 23360), -- 3833
(3815, 'deDE', 'Flimmerdrache', '', NULL, NULL, 23360), -- 3815
(34389, 'deDE', 'ELM General Purpose Bunny (Normal & Phase I)', '', NULL, NULL, 23360), -- 34389
(34377, 'deDE', 'Halannia', '', NULL, NULL, 23360), -- 34377
(3797, 'deDE', 'Cenarischer Beschützer', '', NULL, NULL, 23360), -- 3797
(34518, 'deDE', 'Thagg', '', NULL, NULL, 23360), -- 34518
(12677, 'deDE', 'Schattumbra', '', NULL, NULL, 23360), -- 12677
(34624, 'deDE', 'Elestren', '', 'Händler für Kettenrüstungen', NULL, 23360), -- 34624
(3880, 'deDE', 'Schildwache Melyria Frostschatten', '', NULL, NULL, 23360), -- 3880
(3916, 'deDE', 'Shael''dryn', '', NULL, NULL, 23360), -- 3916
(34335, 'deDE', 'Avrus Böswisper', '', NULL, NULL, 23360), -- 34335
(34499, 'deDE', 'Oso Dornennarbe', '', NULL, NULL, 23360), -- 34499
(8015, 'deDE', 'Schildwache des Eschentals', '', NULL, NULL, 23360), -- 8015
(34366, 'deDE', 'Vorposten des Kriegshymnenklans', '', NULL, NULL, 23360), -- 34366
(4054, 'deDE', 'Lachende Schwester', '', NULL, NULL, 23360), -- 4054
(34426, 'deDE', 'Lachende Schwester', '', NULL, NULL, 23360), -- 34426
(34500, 'deDE', 'Wächter von Schattenwinkel', '', NULL, NULL, 23360), -- 34500
(12676, 'deDE', 'Scharfkralle', '', NULL, NULL, 23360), -- 12676
(33281, 'deDE', 'Wachmann von Mor''shan', '', NULL, NULL, 23360), -- 33281
(3736, 'deDE', 'Dunkeltöter Mordenthal', '', NULL, NULL, 23360), -- 3736
(11749, 'deDE', 'Feran Starkwind', '', NULL, NULL, 23360), -- 11749
(56894, 'deDE', 'Wache des Splitterholzpostens', '', NULL, NULL, 23360), -- 56894
(12877, 'deDE', 'Ertog Zornhauer', '', NULL, NULL, 23360), -- 12877
(12720, 'deDE', 'Framnali', '', NULL, NULL, 23360), -- 12720
(12724, 'deDE', 'Pixel', '', NULL, NULL, 23360), -- 12724
(12616, 'deDE', 'Vhulgra', '', 'Windreitermeisterin', NULL, 23360), -- 12616
(11820, 'deDE', 'Locke Okarr', '', NULL, NULL, 23360), -- 11820
(17100, 'deDE', 'Berater Sonnenschwur', '', NULL, NULL, 23360), -- 17100
(11829, 'deDE', 'Fahrak', '', NULL, NULL, 23360), -- 11829
(12867, 'deDE', 'Kuray''bin', '', NULL, NULL, 23360), -- 12867
(12837, 'deDE', 'Yama Schneehuf', '', NULL, NULL, 23360), -- 12837
(12737, 'deDE', 'Mastok Wutfauch', '', NULL, NULL, 23360), -- 12737
(12196, 'deDE', 'Gastwirtin Kaylisk', '', 'Gastwirtin', NULL, 23360), -- 12196
(6028, 'deDE', 'Burkrum', '', 'Händler für Kettenrüstungen', NULL, 23360), -- 6028
(15131, 'deDE', 'Qeeju', '', 'Stallmeister', NULL, 23360), -- 15131
(12723, 'deDE', 'Har''alen', '', NULL, NULL, 23360), -- 12723
(12722, 'deDE', 'Vera Nachtschatten', '', NULL, NULL, 23360), -- 12722
(17355, 'deDE', 'Valusha', '', NULL, NULL, 23360), -- 17355
(11720, 'deDE', 'Loruk Waldschleicher', '', NULL, NULL, 23360), -- 11720
(33825, 'deDE', 'Aufseher des Splitterholzpostens', '', NULL, NULL, 23360), -- 33825
(33766, 'deDE', 'Peon des Splitterholzpostens', '', NULL, NULL, 23360), -- 33766
(33760, 'deDE', 'Durak', '', NULL, NULL, 23360), -- 33760
(12859, 'deDE', 'Räuber des Splitterholzpostens', '', NULL, NULL, 23360), -- 12859
(12858, 'deDE', 'Torek', '', NULL, NULL, 23360), -- 12858
(12897, 'deDE', 'Krieger der Silberschwingen', '', NULL, NULL, 23360), -- 12897
(12896, 'deDE', 'Schildwache der Silberschwingen', '', NULL, NULL, 23360), -- 12896
(14963, 'deDE', 'Gapp Klingeltaschen', '', 'Kokelwälder', NULL, 23360), -- 14963
(19914, 'deDE', 'Hutihu', '', 'Su''ura Flinkpfeils Tier', NULL, 23360), -- 19914
(19908, 'deDE', 'Su''ura Flinkpfeil', '', 'Kampfmeisterin der Kriegshymnenschlucht', NULL, 23360), -- 19908
(14753, 'deDE', 'Illiyana Mondflamm', '', 'Versorgungsoffizierin der Silberschwingen', NULL, 23360), -- 14753
(14733, 'deDE', 'Schildwache Weltensang', '', NULL, NULL, 23360), -- 14733
(14715, 'deDE', 'Elitesoldatin der Silberschwingen', '', NULL, NULL, 23360), -- 14715
(33294, 'deDE', 'Gorat', '', NULL, NULL, 23360), -- 33294
(33295, 'deDE', 'Gefallener Auftragsmörder der Horde', '', NULL, NULL, 23360), -- 33295
(33201, 'deDE', 'Verteidiger von Mor''shan', '', NULL, NULL, 23360), -- 33201
(33336, 'deDE', 'Gorat''s Target Bunny', '', NULL, NULL, 23360), -- 33336
(33266, 'deDE', 'Verletzter Verteidiger von Mor''shan', '', NULL, NULL, 23360), -- 33266
(33267, 'deDE', 'Wounded Waypoint Bunny', '', NULL, NULL, 23360), -- 33267
(33193, 'deDE', 'Scharmützler des Eschentals', '', NULL, NULL, 23360), -- 33193
(33195, 'deDE', 'Bogenschützin des Eschentals', '', NULL, NULL, 23360), -- 33195
(33421, 'deDE', 'Gorka', '', NULL, NULL, 23360), -- 33421
(33440, 'deDE', 'Demoralisierter Peon', '', NULL, NULL, 23360), -- 33440
(33777, 'deDE', 'Gaivan Schattenwandler', '', 'Druide der Klaue', NULL, 23360), -- 33777
(3942, 'deDE', 'Mavoris Wolkenbruch', '', NULL, NULL, 23360), -- 3942
(33765, 'deDE', 'ELM General Purpose Bunny (scale x2)', '', NULL, NULL, 23360), -- 33765
(12856, 'deDE', 'Kundschafterin des Eschentals', '', NULL, NULL, 23360), -- 12856
(33688, 'deDE', 'Tobendes Urtum', '', NULL, NULL, 23360), -- 33688
(12903, 'deDE', 'Wache des Splitterholzpostens', '', NULL, NULL, 23360), -- 12903
(33726, 'deDE', 'Verwüster des Splitterholzpostens', '', NULL, NULL, 23360), -- 33726
(33728, 'deDE', 'Draaka', '', NULL, NULL, 23360), -- 33728
(33847, 'deDE', 'Forest Heart Bunny (Horde)', '', NULL, NULL, 23360), -- 33847
(3940, 'deDE', 'Taneel Finsterwald', '', NULL, NULL, 23360), -- 3940
(3803, 'deDE', 'Abtrünniger Bewahrer', '', NULL, NULL, 23360), -- 3803
(3941, 'deDE', 'Uthil Mondweise', '', NULL, NULL, 23360), -- 3941
(3928, 'deDE', 'Faulender Schleim', '', NULL, NULL, 23360), -- 3928
(3799, 'deDE', 'Abtrünniger Druide', '', NULL, NULL, 23360), -- 3799
(3821, 'deDE', 'Wilddornlauerer', '', NULL, NULL, 23360), -- 3821
(33446, 'deDE', 'Peon Lumber Bunny', '', NULL, NULL, 23360), -- 33446
(6072, 'deDE', 'Diathorus der Sucher', '', NULL, NULL, 23360), -- 6072
(17300, 'deDE', 'Gorgannon', '', NULL, NULL, 23360), -- 17300
(33767, 'deDE', 'Demon Gate Master Bunny', '', NULL, NULL, 23360), -- 33767
(6073, 'deDE', 'Sengende Höllenbestie', '', NULL, NULL, 23360), -- 6073
(11697, 'deDE', 'Mannorocpeitscher', '', NULL, NULL, 23360), -- 11697
(3825, 'deDE', 'Geisterpfotenalpha', '', NULL, NULL, 23360), -- 3825
(17303, 'deDE', 'Verteidiger Vedaar', '', 'Hand von Argus', NULL, 23360), -- 17303
(17287, 'deDE', 'Schildwache Luciel Sternwisper', '', 'Schildwachen der Silberschwingen', NULL, 23360), -- 17287
(34208, 'deDE', 'Beschützer Endolar', '', NULL, NULL, 23360), -- 34208
(3770, 'deDE', 'Schattenpirscher der Herzschinder', '', NULL, NULL, 23360), -- 3770
(3767, 'deDE', 'Schwindler der Herzschinder', '', NULL, NULL, 23360), -- 3767
(3771, 'deDE', 'Höllenrufer der Herzschinder', '', NULL, NULL, 23360), -- 3771
(3824, 'deDE', 'Geisterpfotenheuler', '', NULL, NULL, 23360), -- 3824
(33697, 'deDE', 'Tobendes Urtumziel', '', NULL, NULL, 23360), -- 33697
(12836, 'deDE', 'Wandernder Beschützer', '', NULL, NULL, 23360), -- 12836
(3763, 'deDE', 'Schattenpirscher der Teufelshufe', '', NULL, NULL, 23360), -- 3763
(3759, 'deDE', 'Schurke der Teufelshufe', '', NULL, NULL, 23360), -- 3759
(3762, 'deDE', 'Teufelsanbeter der Teufelshufe', '', NULL, NULL, 23360), -- 3762
(3758, 'deDE', 'Satyr der Teufelshufe', '', NULL, NULL, 23360), -- 3758
(3834, 'deDE', 'Wahnsinniges Urtum', '', NULL, NULL, 23360), -- 3834
(4619, 'deDE', 'Geltharis', '', NULL, NULL, 23360), -- 4619
(3920, 'deDE', 'Anilia', '', NULL, NULL, 23360), -- 3920
(3757, 'deDE', 'Höllenrufer von Xavian', '', NULL, NULL, 23360), -- 3757
(3755, 'deDE', 'Teufelsanbeter von Xavian', '', NULL, NULL, 23360), -- 3755
(3754, 'deDE', 'Verräter von Xavian', '', NULL, NULL, 23360), -- 3754
(3752, 'deDE', 'Schurke von Xavian', '', NULL, NULL, 23360), -- 3752
(33859, 'deDE', 'Smoldering Brazier Bunny', '', NULL, NULL, 23360), -- 33859
(3765, 'deDE', 'Satyr der Herzschinder', '', NULL, NULL, 23360), -- 3765
(22936, 'deDE', 'Auhula', '', 'Hippogryphenmeisterlehrling', NULL, 23360), -- 22936
(22935, 'deDE', 'Suralais Windstreif', '', 'Hippogryphenmeister', NULL, 23360), -- 22935
(17541, 'deDE', 'Draeneirüstmeisterin', '', NULL, NULL, 23360), -- 17541
(17409, 'deDE', 'Lehrling Boulian', '', 'Architektenlehrling', NULL, 23360), -- 17409
(17406, 'deDE', 'Konstrukteur', '', NULL, NULL, 23360), -- 17406
(17291, 'deDE', 'Architekt Nemos', '', NULL, NULL, 23360), -- 17291
(3848, 'deDE', 'Kayneth Stillwind', '', NULL, NULL, 23360), -- 3848
(17412, 'deDE', 'Phaedra', '', 'Waffenhändlerin', NULL, 23360), -- 17412
(33727, 'deDE', 'Anachoret Buurq', '', NULL, NULL, 23360), -- 33727
(3901, 'deDE', 'Illiyana', '', NULL, NULL, 23360), -- 3901
(12498, 'deDE', 'Traumpirscher', '', NULL, NULL, 23360), -- 12498
(5314, 'deDE', 'Phantim', '', NULL, NULL, 23360), -- 5314
(12476, 'deDE', 'Smaragdonorakel', '', NULL, NULL, 23360), -- 12476
(12475, 'deDE', 'Smaragdonbaumwärter', '', NULL, NULL, 23360), -- 12475
(12864, 'deDE', 'Vorhut des Kriegshymnenklans', '', NULL, NULL, 23360), -- 12864
(34233, 'deDE', 'Wächter Menerin', '', NULL, NULL, 23360), -- 34233
(34232, 'deDE', 'Gesandte Sheelah', '', NULL, NULL, 23360), -- 34232
(49842, 'deDE', 'Waldmotte', '', NULL, NULL, 23360), -- 49842
(6115, 'deDE', 'Streunende Teufelswache', '', NULL, NULL, 23360), -- 6115
(3784, 'deDE', 'Schattendickichtborkenreißer', '', NULL, NULL, 23360), -- 3784
(3782, 'deDE', 'Schattendickichtsteinrücker', '', NULL, NULL, 23360), -- 3782
(17310, 'deDE', 'Knorre', '', 'Urtum des Krieges', NULL, 23360), -- 17310
(33736, 'deDE', 'Fel Fire Bunny', '', NULL, NULL, 23360), -- 33736
(34294, 'deDE', 'Beschützerin Dorinar', '', NULL, NULL, 23360), -- 34294
(34204, 'deDE', 'Beschützer Arminon', '', NULL, NULL, 23360), -- 34204
(34138, 'deDE', 'Ashenvale Oak Bunny', '', NULL, NULL, 23360), -- 34138
(34177, 'deDE', 'Späher des Eschentals', '', NULL, NULL, 23360), -- 34177
(34182, 'deDE', 'Warsong Fires Target', '', NULL, NULL, 23360), -- 34182
(34242, 'deDE', 'Wächter Gurtar', '', NULL, NULL, 23360), -- 34242
(34241, 'deDE', 'Wächter des Kriegshymnenklans', '', NULL, NULL, 23360), -- 34241
(12037, 'deDE', 'Ursol''lok', '', NULL, NULL, 23360), -- 12037
(12474, 'deDE', 'Smaragdonastwache', '', NULL, NULL, 23360), -- 12474
(11681, 'deDE', 'Holzarbeiter des Kriegshymnenklans', '', NULL, NULL, 23360), -- 11681
(11684, 'deDE', 'Goblinroder', '', NULL, NULL, 23360), -- 11684
(33706, 'deDE', 'Schredder des Kriegshymnenklans', '', NULL, 'vehichleCursor', 23360), -- 33706
(17304, 'deDE', 'Vorarbeiter Gorthak', '', NULL, NULL, 23360), -- 17304
(11683, 'deDE', 'Schamane des Kriegshymnenklans', '', NULL, NULL, 23360), -- 11683
(11682, 'deDE', 'Grunzer des Kriegshymnenklans', '', NULL, NULL, 23360), -- 11682
(37556, 'deDE', 'Landbeberbulle', '', NULL, NULL, 23360), -- 37556
(14234, 'deDE', 'Hayoc', '', NULL, NULL, 23360), -- 14234
(51061, 'deDE', 'Roth-Salam', '', NULL, NULL, 23360), -- 51061
(66966, 'deDE', 'Lohe', '', NULL, NULL, 23360), -- 66966
(66439, 'deDE', 'Flammenklaue', '', NULL, NULL, 23360), -- 66439
(66438, 'deDE', 'Feuerzahn', '', NULL, NULL, 23360), -- 66438
(66436, 'deDE', 'Grazzel der Große', '', 'Meistertierzähmer', NULL, 23360), -- 66436
(23797, 'deDE', 'Moxie Stahlgrill', '', NULL, NULL, 23360), -- 23797
(44383, 'deDE', 'Regina Salister', '', 'Handwerkswaren', NULL, 23360), -- 44383
(23601, 'deDE', 'Lehrling Garion', '', 'Tabethas Lehrling', NULL, 23360), -- 23601
(23600, 'deDE', 'Lehrling Morlann', '', 'Tabethas Lehrling', NULL, 23420),
(6546, 'deDE', 'Tabetha', '', NULL, NULL, 23360), -- 6546
(23942, 'deDE', 'Andello Porter', '', 'SI:7', NULL, 23360), -- 23942
(5089, 'deDE', 'Balos Jacken', '', NULL, NULL, 23360), -- 5089
(12919, 'deDE', 'Nat Pagle', '', NULL, NULL, 23360), -- 12919
(14236, 'deDE', 'Lord Angler', '', NULL, NULL, 23360), -- 14236
(4403, 'deDE', 'Matschpanzerzange', '', NULL, NULL, 23360), -- 4403
(10321, 'deDE', 'Aschenschwinge', '', NULL, NULL, 23360), -- 10321
(51069, 'deDE', 'Schieferschillerer', '', NULL, NULL, 23360), -- 51069
(4334, 'deDE', 'Flammenrufer der Feuermähnen', '', NULL, NULL, 23360), -- 4334
(62201, 'deDE', 'Brut von Onyxia', '', NULL, 'wildpetcapturable', 23360), -- 62201
(23687, 'deDE', 'Schwelschuppendrache', '', NULL, NULL, 23360), -- 23687
(4323, 'deDE', 'Sengendes Jungtier', '', NULL, NULL, 23360), -- 4323
(4324, 'deDE', 'Sengender Welpe', '', NULL, NULL, 23360), -- 4324
(14237, 'deDE', 'Schlammwurm', '', NULL, NULL, 23360), -- 14237
(4331, 'deDE', 'Aschenschweif der Feuermähnen', '', NULL, NULL, 23360), -- 4331
(4329, 'deDE', 'Späher der Feuermähnen', '', NULL, NULL, 23360), -- 4329
(4328, 'deDE', 'Bannschuppe der Feuermähnen', '', NULL, NULL, 23360), -- 4328
(40358, 'deDE', 'Dyslix Silberraff', '', 'Flugmeister', NULL, 23360), -- 40358
(23995, 'deDE', 'Axel', '', 'Gastwirt', NULL, 23360), -- 23995
(23572, 'deDE', 'Drazzit Tropfhahn', '', NULL, NULL, 23360), -- 23572
(23571, 'deDE', 'Razbo Rostritzel', '', 'Waffen- & Rüstungshändler', NULL, 23360), -- 23571
(40345, 'deDE', 'Thyssiana', '', NULL, NULL, 23360), -- 40345
(40344, 'deDE', 'Nyse', '', NULL, NULL, 23360), -- 40344
(23636, 'deDE', 'Haudrauf von Morastwinkel', '', NULL, NULL, 23360), -- 23636
(23579, 'deDE', 'Brogg', '', 'Überlebender der Steinbrecher', NULL, 23360), -- 23579
(23573, 'deDE', 'Krixil Klopfknips', '', 'Speis & Trank', NULL, 23360), -- 23573
(23570, 'deDE', 'Gizzix Rußgurgel', '', NULL, NULL, 23360), -- 23570
(50764, 'deDE', 'Paraliss', '', NULL, NULL, 23360), -- 50764
(23873, 'deDE', 'Schlächterklaue der Gefräßige', '', NULL, NULL, 23360), -- 23873
(4356, 'deDE', 'Blutsumpfscharfzahn', '', NULL, NULL, 23360), -- 4356
(4357, 'deDE', 'Blutsumpfschmetterschwanz', '', NULL, NULL, 23360), -- 4357
(23637, 'deDE', 'Leutnant der Deserteure', '', NULL, NULL, 23360), -- 23637
(5057, 'deDE', 'Deserteur von Theramore', '', NULL, NULL, 23360), -- 5057
(50901, 'deDE', 'Teromak', '', NULL, NULL, 23360), -- 50901
(23751, 'deDE', 'Nördliches Zelt', '', NULL, NULL, 23360), -- 23751
(4980, 'deDE', 'Paval Reethe', '', NULL, NULL, 23360), -- 4980
(23832, 'deDE', 'Zeppelinkraftkern', '', NULL, NULL, 23360), -- 23832
(4393, 'deDE', 'Säurehaltiger Sumpfschlamm', '', NULL, NULL, 23360), -- 4393
(4415, 'deDE', 'Riesige Dunkelzahnspinne', '', NULL, NULL, 23360), -- 4415
(4394, 'deDE', 'Brodelnder Sumpfschlamm', '', NULL, NULL, 23360), -- 4394
(23752, 'deDE', 'Nordöstliches Zelt', '', NULL, NULL, 23360), -- 23752
(4345, 'deDE', 'Staubsuhlerdolchrachen', '', NULL, NULL, 23360), -- 4345
(23753, 'deDE', 'Östliches Zelt', '', NULL, NULL, 23360), -- 23753
(23595, 'deDE', 'Erdbinder der Grimmtotems', '', NULL, NULL, 23360), -- 23595
(23594, 'deDE', 'Zerstörer der Grimmtotems', '', NULL, NULL, 23360), -- 23594
(14233, 'deDE', 'Reißerschuppe', '', NULL, NULL, 23360), -- 14233
(4348, 'deDE', 'Giftiger Schredder', '', NULL, NULL, 23360), -- 4348
(4344, 'deDE', 'Scheckiger Staubsuhlerkrokilisk', '', NULL, NULL, 23360), -- 4344
(4501, 'deDE', 'Draz''Zilb', '', NULL, NULL, 23360), -- 4501
(24208, 'deDE', '''Klein'' Logok', '', 'Gastwirt', NULL, 23360), -- 24208
(17095, 'deDE', 'Balandar Sternenlicht', '', NULL, NULL, 23360), -- 17095
(11899, 'deDE', 'Shardi', '', 'Windreitermeisterin', NULL, 23360), -- 11899
(4926, 'deDE', 'Krog', '', NULL, NULL, 23360), -- 4926
(4884, 'deDE', 'Zulrg', '', 'Waffenschmied', NULL, 23360), -- 4884
(4500, 'deDE', 'Oberanführer Mok''Morokk', '', NULL, NULL, 23360), -- 4500
(4502, 'deDE', 'Tharg', '', NULL, NULL, 23360), -- 4502
(5087, 'deDE', 'Do''gol', '', NULL, NULL, 23360), -- 5087
(4883, 'deDE', 'Krak', '', 'Rüstungsschmied', NULL, 23360), -- 4883
(4879, 'deDE', 'Ogg''marr', '', 'Metzger', NULL, 23360), -- 4879
(50342, 'deDE', 'Heronis', '', NULL, NULL, 23360), -- 50342
(4983, 'deDE', 'Ogron', '', NULL, NULL, 23360), -- 4983
(23567, 'deDE', 'Inspektor Tarem', '', NULL, NULL, 23360), -- 23567
(4503, 'deDE', 'Schlammpatsch Dreckfuß', '', NULL, NULL, 23360), -- 4503
(4343, 'deDE', 'Staubsuhlerschnapper', '', NULL, NULL, 23360), -- 4343
(50784, 'deDE', 'Anith', '', NULL, NULL, 23360), -- 50784
(6567, 'deDE', 'Ghok''kah', '', 'Schneiderbedarf', NULL, 23360), -- 6567
(39144, 'deDE', 'Alto Steinkeil', '', 'Reagenzien & Gifte', NULL, 23360), -- 39144
(10036, 'deDE', 'Vollstrecker von Brackenwall', '', NULL, NULL, 23360), -- 10036
(9552, 'deDE', 'Zanara', '', 'Bogenmacherin', NULL, 23360), -- 9552
(4791, 'deDE', 'Nazeer Blutlanze', '', NULL, NULL, 23360), -- 4791
(45227, 'deDE', 'Graunebelbrutkönigin', '', NULL, NULL, 23360), -- 45227
(4378, 'deDE', 'Graunebeleremit', '', NULL, NULL, 23360), -- 4378
(4379, 'deDE', 'Graunebelseidenspinner', '', NULL, NULL, 23360), -- 4379
(4376, 'deDE', 'Graunebelspinne', '', NULL, NULL, 23360), -- 4376
(50945, 'deDE', 'Gammler', '', NULL, NULL, 23360), -- 50945
(4385, 'deDE', 'Moderrankenwüterich', '', NULL, NULL, 23360), -- 4385
(4834, 'deDE', 'Spitzel von Theramore', '', NULL, NULL, 23360), -- 4834
(23723, 'deDE', 'Unteroffizier Lukas', '', NULL, NULL, 23360), -- 23723
(23568, 'deDE', 'Hauptmann Darill', '', NULL, NULL, 23360), -- 23568
(17119, 'deDE', 'Ithania', '', NULL, NULL, 23360), -- 17119
(5184, 'deDE', 'Wachposten von Theramore', '', NULL, NULL, 23360), -- 5184
(4966, 'deDE', 'Gefreiter Hendel', '', NULL, NULL, 23360), -- 4966
(4347, 'deDE', 'Giftiger Häscher', '', NULL, NULL, 23360), -- 4347
(4412, 'deDE', 'Dunkelzahnkrabbler', '', NULL, NULL, 23360), -- 4412
(23869, 'deDE', 'Invis Zelfrax Origin', '', NULL, NULL, 23360), -- 23869
(23868, 'deDE', 'Invis Zelfrax Target', '', NULL, NULL, 23360), -- 23868
(23720, 'deDE', 'Gefangener von Theramore', '', NULL, NULL, 23360), -- 23720
(23714, 'deDE', 'Ältester der Grimmtotems', '', NULL, NULL, 23360), -- 23714
(23593, 'deDE', 'Geistwandler der Grimmtotems', '', NULL, NULL, 23360), -- 23593
(4382, 'deDE', 'Moderrankenkrabbler', '', NULL, NULL, 23360), -- 4382
(23592, 'deDE', 'Bezwinger der Grimmtotems', '', NULL, NULL, 23360), -- 23592
(4880, 'deDE', 'Stinki Ignatz', '', NULL, NULL, 23360), -- 4880
(14232, 'deDE', 'Pfeil', '', NULL, NULL, 23360), -- 14232
(23843, 'deDE', 'Mordant Grimsby', '', NULL, NULL, 23360), -- 23843
(4792, 'deDE', '"Sumpfauge" Jarl', '', NULL, NULL, 23360), -- 4792
(5086, 'deDE', 'Hauptmann Wymor', '', NULL, NULL, 23360), -- 5086
(5085, 'deDE', 'Wache der Späherwacht', '', NULL, NULL, 23360), -- 5085
(23841, 'deDE', 'Messerwirbel', '', NULL, NULL, 23360), -- 23841
(23554, 'deDE', 'Auferstandener Geist', '', NULL, NULL, 23360), -- 23554
(23979, 'deDE', 'Riesiger Marschenfrosch', '', NULL, NULL, 23360), -- 23979
(23585, 'deDE', 'Hexenlicht', '', NULL, NULL, 23360), -- 23585
(23555, 'deDE', 'Auferstandene Gebeine', '', NULL, NULL, 23360), -- 23555
(4341, 'deDE', 'Staubsuhlerkrokilisk', '', NULL, NULL, 23360), -- 4341
(50735, 'deDE', 'Zwinkerauge die Klappernde', '', NULL, NULL, 23360), -- 50735
(4351, 'deDE', 'Blutsumpfraptor', '', NULL, NULL, 23360), -- 4351
(4352, 'deDE', 'Blutsumpfkreischer', '', NULL, NULL, 23360), -- 4352
(15553, 'deDE', 'Wächterbot', '', NULL, NULL, 23360), -- 15553
(15591, 'deDE', 'Gepanzertes Verteidigungsgeschütz', '', NULL, NULL, 23360), -- 15591
(115485, 'deDE', 'Annäherungsmine', '', NULL, NULL, 23360), -- 115485
(115482, 'deDE', 'Clint', '', NULL, NULL, 23360), -- 115482
(4374, 'deDE', 'Hydra der Strashaz', '', NULL, NULL, 23360), -- 4374
(115481, 'deDE', 'Luftabwehrkanone', '', NULL, NULL, 23360), -- 115481
(38622, 'deDE', 'Kapitän Fischer', '', 'Herr der Schwarzborn', NULL, 23360), -- 38622
(38664, 'deDE', 'Besatzungsmitglied der Schwarzborn', '', NULL, NULL, 23360), -- 38664
(38661, 'deDE', 'Seebär von Donnerschrei', '', NULL, NULL, 23360), -- 38661
(38623, 'deDE', 'Dockmeister Lewis', '', NULL, NULL, 23360), -- 38623
(38627, 'deDE', 'Dockarbeiter der Nordwacht', '', NULL, NULL, 23360), -- 38627
(4363, 'deDE', 'Orakel der Schlammflossen', '', NULL, NULL, 23360), -- 4363
(4361, 'deDE', 'Matschwirbler der Schlammflossen', '', NULL, NULL, 23360), -- 4361
(4362, 'deDE', 'Küstenläufer der Schlammflossen', '', NULL, NULL, 23360), -- 4362
(4359, 'deDE', 'Murloc der Schlammflossen', '', NULL, NULL, 23360), -- 4359
(23569, 'deDE', 'Renn McGill', '', 'SI:7-Bergungstaucher', NULL, 23360), -- 23569
(23591, 'deDE', 'Taucher der Defias', '', NULL, NULL, 23360), -- 23591
(23679, 'deDE', 'Garn Mathers', '', NULL, NULL, 23360), -- 23679
(23590, 'deDE', 'Herbeizauberer der Defias', '', NULL, NULL, 23360), -- 23590
(23589, 'deDE', 'Schnüffler der Defias', '', NULL, NULL, 23360), -- 23589
(4346, 'deDE', 'Giftiger Schinder', '', NULL, NULL, 23360), -- 4346
(4414, 'deDE', 'Dunkelzahngiftspucker', '', NULL, NULL, 23360), -- 4414
(50957, 'deDE', 'Riesenkralle', '', NULL, NULL, 23360), -- 50957
(63546, 'deDE', 'Zidormi', '', NULL, NULL, 23360), -- 63546
(4390, 'deDE', 'Alter Düsterdrescher', '', NULL, NULL, 23360), -- 4390
(4389, 'deDE', 'Düsterdrescher', '', NULL, NULL, 23360), -- 4389
(4401, 'deDE', 'Matschpanzerklacker', '', NULL, NULL, 23360), -- 4401
(4404, 'deDE', 'Matschpanzerscharrer', '', NULL, NULL, 23360), -- 4404
(51924, 'deDE', 'Wache von Theramore', '', NULL, NULL, 23360), -- 51924
(53404, 'deDE', 'Redia Vaunt', '', 'Hexenmeisterlehrerin', NULL, 23360), -- 53404
(4965, 'deDE', 'Die Leidende', '', NULL, NULL, 23360), -- 4965
(53410, 'deDE', 'Lissah Zauberdocht', '', 'Verzauberkunstlehrerin & Handwerkswaren', NULL, 23360), -- 53410
(27703, 'deDE', 'Ysuria', '', 'Portallehrerin', NULL, 23360), -- 27703
(4967, 'deDE', 'Erzmagier Tervosh', '', NULL, NULL, 23360), -- 4967
(4890, 'deDE', 'Piter Verance', '', 'Waffen- & Rüstungsschmied', NULL, 23360), -- 4890
(27704, 'deDE', 'Horaz Alder', '', 'Magierlehrer', NULL, 23360), -- 27704
(53415, 'deDE', 'Theoden Manners', '', 'Inschriftenkundelehrer & Handwerkswaren', NULL, 23360), -- 53415
(8141, 'deDE', 'Hauptmann Ebenstab', '', 'Kriegerlehrer', NULL, 23360), -- 8141
(23949, 'deDE', 'Leutnant Nath', '', NULL, NULL, 23360), -- 23949
(12939, 'deDE', 'Doktor Gustav van Howzen', '', 'Lehrer für Erste Hilfe', NULL, 23360), -- 12939
(4973, 'deDE', 'Wache Lasiter', '', NULL, NULL, 23360), -- 4973
(4892, 'deDE', 'Jensen Farran', '', 'Jägerlehrer & Bogenmacher', NULL, 23360), -- 4892
(4964, 'deDE', 'Kommandant Samaul', '', NULL, NULL, 23360), -- 4964
(23892, 'deDE', 'Babs Zischeldrill', '', NULL, NULL, 23360), -- 23892
(4944, 'deDE', 'Hauptmann Garran Mumm', '', NULL, NULL, 23360), -- 4944
(4794, 'deDE', 'Morgan Stern', '', NULL, NULL, 23360), -- 4794
(4889, 'deDE', 'Torq Sprengeisen', '', 'Büchsenmacher', NULL, 23360), -- 4889
(5083, 'deDE', 'Zahlmeister Lendry', '', 'Bankier', NULL, 23360), -- 5083
(8140, 'deDE', 'Bruder Karman', '', 'Paladinlehrer', NULL, 23360), -- 8140
(4951, 'deDE', 'Übende Wache von Theramore', '', NULL, NULL, 23360), -- 4951
(4948, 'deDE', 'Adjutant Tesoran', '', NULL, NULL, 23360), -- 4948
(4952, 'deDE', 'Theramorekampfattrappe', '', NULL, NULL, 23360), -- 4952
(4891, 'deDE', 'Dwane Wertle', '', 'Küchenchefin', NULL, 23360), -- 4891
(5200, 'deDE', 'Sanitäterin Helaina', '', NULL, NULL, 23360), -- 5200
(5095, 'deDE', 'Hauptmann Andrews', '', 'Hauptmann des Roten Teams', NULL, 23360), -- 5095
(4950, 'deDE', 'Spot', '', NULL, NULL, 23360), -- 4950
(53403, 'deDE', 'Allen Glanz', '', 'Priesterlehrer', NULL, 23360), -- 53403
(5094, 'deDE', 'Wache Tark', '', NULL, NULL, 23360), -- 5094
(5093, 'deDE', 'Wache Narrisha', '', NULL, NULL, 23360), -- 5093
(5092, 'deDE', 'Wache Lana', '', NULL, NULL, 23360), -- 5092
(5090, 'deDE', 'Gefechtmeister Szigeti', '', NULL, NULL, 23360), -- 5090
(4924, 'deDE', 'Gefechtmeister Criton', '', NULL, NULL, 23360), -- 4924
(4923, 'deDE', 'Wache Jarad', '', NULL, NULL, 23360), -- 4923
(4922, 'deDE', 'Wache Edward', '', NULL, NULL, 23360), -- 4922
(5199, 'deDE', 'Sanitäterin Tamberlyn', '', NULL, NULL, 23360), -- 5199
(5096, 'deDE', 'Hauptmann Thomas', '', 'Hauptmann des Blauen Teams', NULL, 23360), -- 5096
(5091, 'deDE', 'Wache Kahil', '', NULL, NULL, 23360), -- 5091
(4902, 'deDE', 'Mikal Pierce', '', NULL, NULL, 23360), -- 4902
(4894, 'deDE', 'Craig Nollwacht', '', 'Kochlehrer', NULL, 23360), -- 4894
(13277, 'deDE', 'Dahne Pierce', '', NULL, NULL, 23360), -- 13277
(4893, 'deDE', 'Schankkellnerin Lillian', '', 'Schankkellnerin', NULL, 23360), -- 4893
(6732, 'deDE', 'Amie Pierce', '', NULL, NULL, 23360), -- 6732
(53405, 'deDE', 'Raedra Windhammer', '', 'Schamanenlehrerin', NULL, 23360), -- 53405
(4901, 'deDE', 'Sara Pierce', '', NULL, NULL, 23360), -- 4901
(6272, 'deDE', 'Gastwirtin Janene', '', 'Gastwirtin', NULL, 23360), -- 6272
(53436, 'deDE', 'Eustace Tanwell', '', 'Lederverarbeitungslehrer & Handwerkswaren', NULL, 23360), -- 53436
(53437, 'deDE', 'Humbert Tanwell', '', 'Kürschnerlehrer', NULL, 23360), -- 53437
(4397, 'deDE', 'Dornenschlammpanzer', '', NULL, NULL, 23360), -- 4397
(53421, 'deDE', 'Faena Wollbusch', '', 'Archäologielehrerin', NULL, 23360), -- 53421
(10047, 'deDE', 'Michael', '', 'Stallmeister', NULL, 23360), -- 10047
(5388, 'deDE', 'Ingo Wollbusch', '', 'Juwelierskunstlehrer & Handwerkswaren', NULL, 23360), -- 5388
(4921, 'deDE', 'Wache Byron', '', NULL, NULL, 23360), -- 4921
(4895, 'deDE', 'Lächelnder Jim', '', NULL, NULL, 23360), -- 4895
(23951, 'deDE', 'Leutnant Aden', '', NULL, NULL, 23360), -- 23951
(4900, 'deDE', 'Alchemist Narett', '', 'Alchemielehrer', NULL, 23360), -- 4900
(4899, 'deDE', 'Uma Bartulm', '', 'Kräuterkunde- & Alchemiebedarf', NULL, 23360), -- 4899
(4898, 'deDE', 'Brant Jasperblum', '', 'Kräuterkundelehrer', NULL, 23360), -- 4898
(39138, 'deDE', 'Straßenwache von Theramore', '', NULL, NULL, 23360), -- 39138
(24006, 'deDE', 'Großknecht Tionn', '', NULL, NULL, 23360), -- 24006
(53409, 'deDE', '"Kobold" Kerik', '', 'Bergbaulehrer & Handwerkswaren', NULL, 23360), -- 53409
(24005, 'deDE', 'Belagerungsarbeiter', '', NULL, NULL, 23360), -- 24005
(4888, 'deDE', 'Marie Haltsten', '', 'Schmiedekunstlehrerin', NULL, 23360), -- 4888
(4941, 'deDE', 'Caz Doppelspross', '', 'Ingenieurskunstlehrer', NULL, 23360), -- 4941
(4886, 'deDE', 'Hans Weston', '', 'Rüstungs- & Waffenschmied', NULL, 23360), -- 4886
(4885, 'deDE', 'Gregor MacVince', '', 'Pferdezüchter', NULL, 23360), -- 4885
(53407, 'deDE', 'Sansha MacVince', '', 'Reitlehrerin', NULL, 23360), -- 53407
(4897, 'deDE', 'Helenia Olden', '', 'Handwerkswaren', NULL, 23360), -- 4897
(11052, 'deDE', 'Timothy Wertinger', '', 'Schneiderlehrer', NULL, 23360), -- 11052
(23620, 'deDE', 'Freibeuter', '', NULL, NULL, 23360), -- 23620
(23704, 'deDE', 'Cassa Purpurschwinge', '', 'Greifenmeisterlehrling', NULL, 23360), -- 23704
(23950, 'deDE', 'Leutnant Khand', '', NULL, NULL, 23360), -- 23950
(24007, 'deDE', 'Werkskurier', '', NULL, NULL, 23360), -- 24007
(4896, 'deDE', 'Charity Mipsy', '', 'Gemischtwaren & Reagenzien', NULL, 23360), -- 4896
(4321, 'deDE', 'Baldruc', '', 'Greifenmeister', NULL, 23360), -- 4321
(4968, 'deDE', 'Lady Jaina Prachtmeer', '', 'Herrscherin über Theramore', NULL, 23360), -- 4968
(23835, 'deDE', 'Unteroffizierin Amelyn', '', NULL, NULL, 23360), -- 23835
(23566, 'deDE', 'Calia Hastings', '', 'Schurkenlehrerin', NULL, 23360), -- 23566
(2616, 'deDE', 'Freibeuter Groy', '', NULL, NULL, 23360), -- 2616
(23896, 'deDE', 'Michael "Schmutz" Fink', '', 'Angellehrer & Handwerkswaren', NULL, 23360), -- 23896
(23905, 'deDE', 'Major Mühlens', '', NULL, NULL, 23360), -- 23905
(44390, 'deDE', 'Stachelige Steinkrabbe', '', NULL, NULL, 23360), -- 44390
(23907, 'deDE', 'Kanone von Theramore', '', NULL, NULL, 23360);