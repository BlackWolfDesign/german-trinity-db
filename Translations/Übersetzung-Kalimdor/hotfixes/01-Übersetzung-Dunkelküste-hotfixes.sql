-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 03/16/2017 22:46:21


SET NAMES 'utf8';
INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID0`, `EmoteID1`, `EmoteID2`, `EmoteDelay0`, `EmoteDelay1`, `EmoteDelay2`, `UnkEmoteID`, `Language`, `Type`, `SoundID0`, `SoundID1`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(69980, "Also, wer ist dabei?", "", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 46036, 23420),
(69981, "Das w�r's! Der Kampf hat begonnen � keine Wetten mehr!", "", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 46036, 23420),
(69966, "Schlie�t jetzt Eure Wetten ab und setzt auf den Sieger des Kampfes.", "", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 47751, 23420),
(69967, "Das w�r's! Keine Wetten mehr! Dann wollen wir mal sehen, was passiert!", "", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 47751, 23420),
(34473, "", "W�hrend die Elementare ihr Bestes tun, um Dunkelk�ste in St�cke zu rei�en, habe ich f�r den Erhalt der verbleibenden Wildtiere gek�mpft.$B$BKann ich auf Eure Hilfe z�hlen, $C?", 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(34512, "", "Ich sch�tze Eure Hilfe, die hiesigen Wildtiere zu sch�tzen. Hoffentlich kommen wir nicht zu sp�t.", 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(34515, "", "Ich w�nschte, ich k�nnte Euch mehr erz�hlen, aber bis vor Kurzem wusste niemand, dass solche Kreaturen existieren. Lange, bevor die Titanen das heutige Azeroth erschufen, vor dem nat�rlichen Kreislauf der Natur, herrschten die Urwesen �ber die Elemente und korrumpierten die Sch�pfung.$B$BWie Onu mir erz�hlte, besiegten die Titanen schlie�lich die Urwesen. Oder sperrten sie zumindest unter der Erde ein. Kann so etwas denn �berhaupt jemals get�tet werden? Ich w�nschte, ich w�sste mehr, aber das ist alles pr�historisch.$B$BIch wei�, dass es solche gibt, die die Urwesen anbeten, so unglaublich dies auch sein mag. Das ist, als w�rde man f�r den Weltuntergang beten. Diese Alten G�tter machen die Leute wahnsinnig!", 274, 1, 6, 0, 800, 800, 0, 0, 1, 0, 0, 0, 23420);


DELETE FROM `broadcast_text_locale` WHERE ('entry'=69980 AND `locale`='deDE') OR ('entry'=69981 AND `locale`='deDE') OR ('entry'=69966 AND `locale`='deDE') OR ('entry'=69967 AND `locale`='deDE') OR ('entry'=34473 AND `locale`='deDE') OR ('entry'=34512 AND `locale`='deDE') OR ('entry'=34515 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(69980, 'deDE', "Also, wer ist dabei?", "", 23420),
(69981, 'deDE', "Das w�r's! Der Kampf hat begonnen � keine Wetten mehr!", "", 23420),
(69966, 'deDE', "Schlie�t jetzt Eure Wetten ab und setzt auf den Sieger des Kampfes.", "", 23420),
(69967, 'deDE', "Das w�r's! Keine Wetten mehr! Dann wollen wir mal sehen, was passiert!", "", 23420),
(34473, 'deDE', "", "W�hrend die Elementare ihr Bestes tun, um Dunkelk�ste in St�cke zu rei�en, habe ich f�r den Erhalt der verbleibenden Wildtiere gek�mpft.$B$BKann ich auf Eure Hilfe z�hlen, $C?", 23420),
(34512, 'deDE', "", "Ich sch�tze Eure Hilfe, die hiesigen Wildtiere zu sch�tzen. Hoffentlich kommen wir nicht zu sp�t.", 23420),
(34515, 'deDE', "", "Ich w�nschte, ich k�nnte Euch mehr erz�hlen, aber bis vor Kurzem wusste niemand, dass solche Kreaturen existieren. Lange, bevor die Titanen das heutige Azeroth erschufen, vor dem nat�rlichen Kreislauf der Natur, herrschten die Urwesen �ber die Elemente und korrumpierten die Sch�pfung.$B$BWie Onu mir erz�hlte, besiegten die Titanen schlie�lich die Urwesen. Oder sperrten sie zumindest unter der Erde ein. Kann so etwas denn �berhaupt jemals get�tet werden? Ich w�nschte, ich w�sste mehr, aber das ist alles pr�historisch.$B$BIch wei�, dass es solche gibt, die die Urwesen anbeten, so unglaublich dies auch sein mag. Das ist, als w�rde man f�r den Weltuntergang beten. Diese Alten G�tter machen die Leute wahnsinnig!", 23420);

