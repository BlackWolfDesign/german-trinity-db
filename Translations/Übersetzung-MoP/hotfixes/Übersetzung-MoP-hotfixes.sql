-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23360
-- Detected locale: 'deDE'
-- Targeted database: Legion
-- Parsing date: 02/23/2017 13:03:48

SET NAMES 'utf8';


DELETE FROM `broadcast_text_locale` WHERE(`ID`=75955 AND `locale`='deDE') OR (`ID`=75457 AND `locale`='deDE') OR (`ID`=75449 AND `locale`='deDE') OR (`ID`=75445 AND `locale`='deDE') OR (`ID`=75440 AND `locale`='deDE') OR (`ID`=75438 AND `locale`='deDE') OR (`ID`=75439 AND `locale`='deDE') OR (`ID`=75437 AND `locale`='deDE') OR (`ID`=75432 AND `locale`='deDE') OR (`ID`=75431 AND `locale`='deDE') OR (`ID`=75451 AND `locale`='deDE') OR (`ID`=74578 AND `locale`='deDE') OR (`ID`=75796 AND `locale`='deDE') OR (`ID`=75802 AND `locale`='deDE') OR (`ID`=76561 AND `locale`='deDE') OR (`ID`=76553 AND `locale`='deDE') OR (`ID`=75572 AND `locale`='deDE') OR (`ID`=76739 AND `locale`='deDE') OR (`ID`=74577 AND `locale`='deDE') OR (`ID`=74540 AND `locale`='deDE') OR (`ID`=75570 AND `locale`='deDE') OR (`ID`=75568 AND `locale`='deDE') OR (`ID`=75569 AND `locale`='deDE') OR (`ID`=75627 AND `locale`='deDE') OR (`ID`=62617 AND `locale`='deDE') OR (`ID`=75628 AND `locale`='deDE') OR (`ID`=76792 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(75955, 'deDE', 'Seid bitte vorsichtig, $n. Die Himmlischen Erhabenen werden hier keine Aggressionen zwischen Allianz und Horde tolerieren.', '', 23360),
(75457, 'deDE', 'Ihr solltet etwas trinken. Trinkt! Habt Spaß!', '', 23360),
(75449, 'deDE', 'Nach dem Kampf gegen die Mogu frage ich mich, wie stark andere Gegner überhaupt noch sein können.', '', 23360),
(75445, 'deDE', 'Ihr kämpft mit viel Talent und Hingabe. Ich hoffe, eines Tages Eurem Weg folgen zu können.', '', 23360),
(75440, 'deDE', '', 'Es ist wahrlich eine Ehre, in der Gegenwart von Xuen und den anderen Erhabenen verweilen zu dürfen.', 23360),
(75438, 'deDE', 'Bleibt wachsam, aber vergesst nicht, auch die schönen Dinge des Lebens zu genießen: gutes Essen, gute Freunde und feines Gebräu!', '', 23360),
(75439, 'deDE', 'Wir können viel lernen, wenn wir andere beim Kämpfen beobachten.', '', 23360),
(75437, 'deDE', 'Entspannt Euch. Ihr seid unter Freunden.', '', 23360),
(75432, 'deDE', '', 'Bleibt Euch immer selbst treu, dann seid Ihr standhaft wie ein mächtiger Gletscher.', 23360),
(75431, 'deDE', '', 'Dieser Ort strahlt Ruhe aus. Er wurde nicht von den Kriegen heimgesucht, die das Festland verheeren.', 23360),
(75451, 'deDE', 'Das Turnier der Erhabenen stellt Eure Qualitäten als Anführer auf die Probe, indem Ihr gegen andere Teilnehmer Haustierkämpfe austragt. Wenn Ihr gut genug seid, tretet Ihr sogar gegen die Kinder der Erhabenen an.', '', 23360),
(74578, 'deDE', '', 'Seid gegrüßt, $n! Ich freue mich, Euch wiederzusehen. Alle reden immer noch davon, wie Ihr letztes Mal das Turnier gewonnen habt – ich habe nicht alles mitbekommen, aber ich musste eine ganze Menge Schnitte und Verbrennungen heilen. Aber keine Sorge: Sie sind alle wieder auf den Beinen!', 23360),
(75796, 'deDE', 'Jene, die sich wegen ihrer eigenen, selbstsüchtigen Ziele in die Zeitlinien einmischen, sind meist gefährlich. Aus diesem Grund schloss ich mich den Wächtern, den standhaften Verteidigern der Zeitwanderer, an. Wir schützen die Behüter und Weber bei ihren Aufgaben.', '', 23360),
(75802, 'deDE', 'Die Weber sind Arkanisten, die sich auf die Chronomantie, also die Kunst des Zeitwebens, spezialisiert haben. Unter den Zeitwanderern sind es allein wir, die Schäden an den Zeitlinien reparieren, temporale Portale öffnen und ähnliche Dinge tun können. Einst waren diese Dinge für den bronzenen Drachenschwarm einfach, aber jetzt leider nicht mehr.', '', 23360),
(76561, 'deDE', '', 'Oh, hallo!$b$bBitte, kommt doch rein! Ruht Euch ein Weilchen aus.$b$bMöchtet Ihr etwas essen? Oder trinken?', 23360),
(76553, 'deDE', 'Ihr wollt ein Paket abholen? Einen Brief senden?$b$bDas geht hier alles ruckzuck!', '', 23360),
(75572, 'deDE', 'Eure Geschichte setzt sich fort.$B$BUnd sie ist es wert, gelesen zu werden.', '', 23360),
(76739, 'deDE', 'Nun gut, ich brauche Hilfe. Ich suche meinen Körper. Er ist irgendwo hier in dieser Höhle, aber Ihr könnt ihn nur sehen, wenn Ihr Euch in der Geisterwelt befindet. Wenn Ihr mir helfen möchtet, ihn zu finden, lasst es mich wissen. Doch seid gewarnt: Die zeitlosen Geister können uns aus der Geisterwelt werfen, und wenn sie das tun sollten, glaube ich, dass ich eine Weile lang nicht stark genug bin, um Euch wieder dorthin zu bringen.', '', 23360),
(74577, 'deDE', 'Willkommen, $GReisender:Reisende;! Ich biete Schätze aus aller Welt feil.$b$bWollt Ihr Euer Glück auf die Probe stellen? Nur ein paar zeitlose Münzen und Ihr erhaltet einen Schlüssel.', '', 23360),
(74540, 'deDE', 'Die Statue brummt sanft, als Ihr Euch nähert.', 'Die Statue brummt sanft, als Ihr Euch nähert.', 23360),
(75570, 'deDE', 'Nur ein arbeitsamer Grummel ist ein guter Grummel!', '', 23360),
(75568, 'deDE', 'Heute ist uns das Glück hold. Es gibt dort viele Leute, denen man Dinge liefern kann.', '', 23360),
(75569, 'deDE', 'Ihr bewegt Euch wie ein Champion, $R.', '', 23360),
(75627, 'deDE', 'Es gibt nichts Schöneres als ein feines Gebräu!', '', 23360),
(62617, 'deDE', 'Hmm... Wenn ich das damit mische... Oh, ja. Hallo!', '', 23360),
(75628, 'deDE', 'Wie wäre es mit einer neuen Runde?', '', 23360),
(76792, 'deDE', 'Es freut mich so, diesen Augenblick mit meinem Sohn teilen zu können.', '', 23360);


INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(75955, 'Seid bitte vorsichtig, $n. Die Himmlischen Erhabenen werden hier keine Aggressionen zwischen Allianz und Horde tolerieren.', '', 396, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75457, 'Ihr solltet etwas trinken. Trinkt! Habt Spaß!', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75449, 'Nach dem Kampf gegen die Mogu frage ich mich, wie stark andere Gegner überhaupt noch sein können.', '', 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75445, 'Ihr kämpft mit viel Talent und Hingabe. Ich hoffe, eines Tages Eurem Weg folgen zu können.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75440, '', 'Es ist wahrlich eine Ehre, in der Gegenwart von Xuen und den anderen Erhabenen verweilen zu dürfen.', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75438, 'Bleibt wachsam, aber vergesst nicht, auch die schönen Dinge des Lebens zu genießen: gutes Essen, gute Freunde und feines Gebräu!', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75439, 'Wir können viel lernen, wenn wir andere beim Kämpfen beobachten.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75437, 'Entspannt Euch. Ihr seid unter Freunden.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75432, '', 'Bleibt Euch immer selbst treu, dann seid Ihr standhaft wie ein mächtiger Gletscher.', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75431, '', 'Dieser Ort strahlt Ruhe aus. Er wurde nicht von den Kriegen heimgesucht, die das Festland verheeren.', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75451, 'Das Turnier der Erhabenen stellt Eure Qualitäten als Anführer auf die Probe, indem Ihr gegen andere Teilnehmer Haustierkämpfe austragt. Wenn Ihr gut genug seid, tretet Ihr sogar gegen die Kinder der Erhabenen an.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(74578, '', 'Seid gegrüßt, $n! Ich freue mich, Euch wiederzusehen. Alle reden immer noch davon, wie Ihr letztes Mal das Turnier gewonnen habt – ich habe nicht alles mitbekommen, aber ich musste eine ganze Menge Schnitte und Verbrennungen heilen. Aber keine Sorge: Sie sind alle wieder auf den Beinen!', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(75796, 'Jene, die sich wegen ihrer eigenen, selbstsüchtigen Ziele in die Zeitlinien einmischen, sind meist gefährlich. Aus diesem Grund schloss ich mich den Wächtern, den standhaften Verteidigern der Zeitwanderer, an. Wir schützen die Behüter und Weber bei ihren Aufgaben.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75802, 'Die Weber sind Arkanisten, die sich auf die Chronomantie, also die Kunst des Zeitwebens, spezialisiert haben. Unter den Zeitwanderern sind es allein wir, die Schäden an den Zeitlinien reparieren, temporale Portale öffnen und ähnliche Dinge tun können. Einst waren diese Dinge für den bronzenen Drachenschwarm einfach, aber jetzt leider nicht mehr.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(76561, '', 'Oh, hallo!$b$bBitte, kommt doch rein! Ruht Euch ein Weilchen aus.$b$bMöchtet Ihr etwas essen? Oder trinken?', 3, 1, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 23360),
(76553, 'Ihr wollt ein Paket abholen? Einen Brief senden?$b$bDas geht hier alles ruckzuck!', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75572, 'Eure Geschichte setzt sich fort.$B$BUnd sie ist es wert, gelesen zu werden.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(76739, 'Nun gut, ich brauche Hilfe. Ich suche meinen Körper. Er ist irgendwo hier in dieser Höhle, aber Ihr könnt ihn nur sehen, wenn Ihr Euch in der Geisterwelt befindet. Wenn Ihr mir helfen möchtet, ihn zu finden, lasst es mich wissen. Doch seid gewarnt: Die zeitlosen Geister können uns aus der Geisterwelt werfen, und wenn sie das tun sollten, glaube ich, dass ich eine Weile lang nicht stark genug bin, um Euch wieder dorthin zu bringen.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(74577, 'Willkommen, $GReisender:Reisende;! Ich biete Schätze aus aller Welt feil.$b$bWollt Ihr Euer Glück auf die Probe stellen? Nur ein paar zeitlose Münzen und Ihr erhaltet einen Schlüssel.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(74540, 'Die Statue brummt sanft, als Ihr Euch nähert.', 'Die Statue brummt sanft, als Ihr Euch nähert.', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(75570, 'Nur ein arbeitsamer Grummel ist ein guter Grummel!', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75568, 'Heute ist uns das Glück hold. Es gibt dort viele Leute, denen man Dinge liefern kann.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75569, 'Ihr bewegt Euch wie ein Champion, $R.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75627, 'Es gibt nichts Schöneres als ein feines Gebräu!', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(62617, 'Hmm... Wenn ich das damit mische... Oh, ja. Hallo!', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(75628, 'Wie wäre es mit einer neuen Runde?', '', 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(76792, 'Es freut mich so, diesen Augenblick mit meinem Sohn teilen zu können.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360);
