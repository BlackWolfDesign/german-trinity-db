-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_0_23222
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/22/2017 18:44:56


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=31613 AND `locale`='deDE') OR (`ID`=30567 AND `locale`='deDE') OR (`ID`=43242 AND `locale`='deDE') OR (`ID`=43245 AND `locale`='deDE') OR (`ID`=43301 AND `locale`='deDE') OR (`ID`=43287 AND `locale`='deDE') OR (`ID`=43300 AND `locale`='deDE') OR (`ID`=43298 AND `locale`='deDE') OR (`ID`=43288 AND `locale`='deDE') OR (`ID`=43290 AND `locale`='deDE') OR (`ID`=43289 AND `locale`='deDE') OR (`ID`=43299 AND `locale`='deDE') OR (`ID`=43282 AND `locale`='deDE') OR (`ID`=44911 AND `locale`='deDE') OR (`ID`=44910 AND `locale`='deDE') OR (`ID`=44909 AND `locale`='deDE') OR (`ID`=44908 AND `locale`='deDE') OR (`ID`=44891 AND `locale`='deDE') OR (`ID`=43303 AND `locale`='deDE') OR (`ID`=43179 AND `locale`='deDE') OR (`ID`=42422 AND `locale`='deDE') OR (`ID`=42421 AND `locale`='deDE') OR (`ID`=42420 AND `locale`='deDE') OR (`ID`=42234 AND `locale`='deDE') OR (`ID`=42233 AND `locale`='deDE') OR (`ID`=42170 AND `locale`='deDE') OR (`ID`=41761 AND `locale`='deDE') OR (`ID`=37479 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(31613, 'deDE', 'Brisantes Grünsteinbräu', 'Helft bei der Verteidigung von Grünstein.', 'Ich arbeite an einer Ladung meines berühmten brisanten Grünsteinbräus.$b$bSollte sich hervorragend dafür eignen, mit den Strolchen und den schelmischen Elementaren aufzuräumen, die wir in letzter Zeit in der Nähe der Minen gesehen haben.$b$bIch sag Euch was, wie wär''s, wenn Ihr ein paar Eurer Freunde zusammentrommelt und sie herbringt, um etwas davon auszuprobieren?$b$bDie erste Runde geht aufs Haus!', '', '', '', '', '', '', 23222),
(30567, 'deDE', 'Blanches Hammerbräu', 'Schließt das Szenario Ein Sturm braut sich zusammen ab.', 'Riecht Ihr es in der Luft? Ein Sturm braut sich zusammen. Da braue ich doch gleich mit!$B$BKommt hoch auf den Hügel, wenn der Sturm da ist, dann brauen wir gemeinsam ein Fässchen meines berühmtesten Gebräus. Kann ich auf Eure Hilfe zählen?', '', '', '', '', '', '', 23222),
(43242, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23222),
(43245, 'deDE', 'Invasion: Westfall', '', '', '', '', '', '', '', '', 23222),
(43301, 'deDE', 'Invasion: Azshara', '', '', '', '', '', '', '', '', 23222),
(43287, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23222),
(43300, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23222),
(43298, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23222),
(43288, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23222),
(43290, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23222),
(43289, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23222),
(43299, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23222),
(43282, 'deDE', 'Invasion: Nördliches Brachland', '', '', '', '', '', '', '', '', 23222),
(44911, 'deDE', 'Unbenutzt', '', '', '', '', '', '', '', '', 23222),
(44910, 'deDE', 'Unbenutzt', '', '', '', '', '', '', '', '', 23222),
(44909, 'deDE', 'Wöchentliches gewertetes Schlachtfeld', '', '', '', '', '', '', '', '', 23222),
(44908, 'deDE', 'Wöchentliche 3vs3-Quest', '', '', '', '', '', '', '', '', 23222),
(44891, 'deDE', 'Wöchentliche 2vs2-Quest', '', '', '', '', '', '', '', '', 23222),
(43303, 'deDE', 'Reif für Randale!', 'Sprecht mit David Globetrotter und findet heraus, wie man an der Rabenwehrrandale teilnimmt.', 'Willkommen zur Rabenwehrrandale! Es haben schon viele Kämpfer in der Arena der Rabenwehr teilgenommen, um ihre Stärke unter Beweis zu stellen, und jetzt seid Ihr an der Reihe.', '', '', '', '', '', '', 23222),
(43179, 'deDE', 'Die Kirin Tor von Dalaran', 'Schließt 4 Weltquests der Kirin Tor ab.', 'Unterstützt die Kirin Tor von Dalaran, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Kriegsmagier Silva in Dalaran zurück.', 23222),
(42422, 'deDE', 'Die Wächterinnen', 'Schließt 4 Weltquests der Wächterinnen ab.', 'Unterstützt die Wächterinnen, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Marin Klingenflügel auf der Insel der Behüter zurück.', 23222),
(42421, 'deDE', 'Die Nachtsüchtigen', 'Schließt beliebige 4 Weltquests in Suramar ab.', 'Unterstützt die Nachtsüchtigen von Suramar, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zur Ersten Arkanistin Thalyssra in Suramar zurück.', 23222),
(42420, 'deDE', 'Farondis'' Hofstaat', 'Schließt beliebige 4 Weltquests in Azsuna ab.', 'Unterstützt Farondis'' Hofstaat in Azsuna, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Veridis Fallon in Azsuna zurück.', 23222),
(42234, 'deDE', 'Die Valarjar', 'Schließt 4 beliebige Weltquests in Sturmheim ab.', 'Unterstützt die Vrykul von Sturmheim, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Valdemar Sturmsucher in Sturmheim zurück.', 23222),
(42233, 'deDE', 'Hochbergstämme', 'Schließt beliebige 4 Weltquests am Hochberg ab.', 'Unterstützt die Tauren vom Hochberg, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Ransa Graufeder in Donnertotem am Hochberg zurück.', 23222),
(42170, 'deDE', 'Die Traumweber', 'Schließt beliebige 4 Weltquests in Val''sharah ab.', 'Unterstützt die Druiden von Val''sharah, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Sylvio Hirschhorn in Val''sharah zurück.', 23222),
(41761, 'deDE', 'Zusammentreiben der Winterelche', 'Rettet 4 Talbuks auf Kuuros Hof.', 'Dieser Hof gehört meinem Vater Karmaan, und unser Viehbestand besteht größtenteils aus Talbuks. Ein Schotling muss das Tor ihres Pferchs geöffnet haben, denn sie sind alle entkommen!$B$BDie kleinen Mistviecher hätten sie direkt im Gehege töten können, doch sie jagen ihre Beute gerne. Ich befürchte, dass ich ihr nächstes Opfer sein werde, wenn ich noch länger hierbleibe!$B$BKönnt Ihr die Talbuks zusammentreiben und herbringen? Ich schaffe es nicht und mein Vater darf nichts davon erfahren!', '', '', '', '', '', '', 23222),
(37479, 'deDE', 'Bonusziel: Allianzanführer töten', '', '', '', '', '', '', '', '', 23222);


DELETE FROM `quest_objectives_locale` WHERE (`ID`=268789 AND `locale`='deDE') OR (`ID`=253450 AND `locale`='deDE') OR (`ID`=285159 AND `locale`='deDE') OR (`ID`=285073 AND `locale`='deDE') OR (`ID`=284172 AND `locale`='deDE') OR (`ID`=284171 AND `locale`='deDE') OR (`ID`=284170 AND `locale`='deDE') OR (`ID`=283946 AND `locale`='deDE') OR (`ID`=283945 AND `locale`='deDE') OR (`ID`=283830 AND `locale`='deDE') OR (`ID`=282762 AND `locale`='deDE') OR (`ID`=276271 AND `locale`='deDE') OR (`ID`=276270 AND `locale`='deDE') OR (`ID`=276269 AND `locale`='deDE');
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(268789, 'deDE', 31613, 0, 'Dorfbewohner von Grünstein verteidigt', 23222),
(253450, 'deDE', 30567, 0, 'Szenario abgeschlossen', 23222),
(285159, 'deDE', 43303, 0, 'Sprecht mit David Globetrotter', 23222),
(285073, 'deDE', 43179, 0, 'Schließt 4 Weltquests der Kirin Tor ab', 23222),
(284172, 'deDE', 42422, 0, 'Schließt 4 Weltquests der Wächterinnen ab', 23222),
(284171, 'deDE', 42421, 0, 'Schließt 4 Weltquests in Suramar ab', 23222),
(284170, 'deDE', 42420, 0, 'Schließt 4 Weltquests in Azsuna ab', 23222),
(283946, 'deDE', 42234, 0, 'Schließt 4 Weltquests in Sturmheim ab', 23222),
(283945, 'deDE', 42233, 0, 'Schließt 4 Weltquests am Hochberg ab', 23222),
(283830, 'deDE', 42170, 0, 'Schließt 4 Weltquests in Val''sharah ab', 23222),
(282762, 'deDE', 41761, 0, 'Elche zusammengetrieben', 23222),
(276271, 'deDE', 37479, 1, 'Marketa getötet', 23222),
(276270, 'deDE', 37479, 0, 'Valant getötet', 23222),
(276269, 'deDE', 37479, 2, 'Anenga getötet', 23222);


DELETE FROM `gameobject_template_locale` WHERE (`entry`=213741 AND `locale`='deDE') OR (`entry`=214338 AND `locale`='deDE') OR (`entry`=210691 AND `locale`='deDE') OR (`entry`=210690 AND `locale`='deDE') OR (`entry`=210724 AND `locale`='deDE') OR (`entry`=212621 AND `locale`='deDE') OR (`entry`=212625 AND `locale`='deDE') OR (`entry`=212624 AND `locale`='deDE') OR (`entry`=212623 AND `locale`='deDE') OR (`entry`=212622 AND `locale`='deDE') OR (`entry`=209345 AND `locale`='deDE') OR (`entry`=209344 AND `locale`='deDE') OR (`entry`=211701 AND `locale`='deDE') OR (`entry`=211097 AND `locale`='deDE') OR (`entry`=211700 AND `locale`='deDE') OR (`entry`=211098 AND `locale`='deDE') OR (`entry`=211862 AND `locale`='deDE') OR (`entry`=211476 AND `locale`='deDE') OR (`entry`=214510 AND `locale`='deDE') OR (`entry`=210921 AND `locale`='deDE') OR (`entry`=213832 AND `locale`='deDE') OR (`entry`=210054 AND `locale`='deDE') OR (`entry`=210086 AND `locale`='deDE') OR (`entry`=210085 AND `locale`='deDE') OR (`entry`=214567 AND `locale`='deDE') OR (`entry`=214566 AND `locale`='deDE') OR (`entry`=213833 AND `locale`='deDE') OR (`entry`=214408 AND `locale`='deDE') OR (`entry`=212898 AND `locale`='deDE') OR (`entry`=212769 AND `locale`='deDE') OR (`entry`=212770 AND `locale`='deDE') OR (`entry`=215781 AND `locale`='deDE') OR (`entry`=213170 AND `locale`='deDE') OR (`entry`=213421 AND `locale`='deDE') OR (`entry`=212613 AND `locale`='deDE') OR (`entry`=212612 AND `locale`='deDE') OR (`entry`=212611 AND `locale`='deDE') OR (`entry`=212899 AND `locale`='deDE') OR (`entry`=210565 AND `locale`='deDE') OR (`entry`=212887 AND `locale`='deDE') OR (`entry`=214414 AND `locale`='deDE') OR (`entry`=212897 AND `locale`='deDE') OR (`entry`=210116 AND `locale`='deDE') OR (`entry`=214425 AND `locale`='deDE') OR (`entry`=214410 AND `locale`='deDE') OR (`entry`=212892 AND `locale`='deDE') OR (`entry`=210807 AND `locale`='deDE') OR (`entry`=212632 AND `locale`='deDE') OR (`entry`=212631 AND `locale`='deDE') OR (`entry`=212629 AND `locale`='deDE') OR (`entry`=212628 AND `locale`='deDE') OR (`entry`=212626 AND `locale`='deDE') OR (`entry`=213436 AND `locale`='deDE') OR (`entry`=188216 AND `locale`='deDE') OR (`entry`=212633 AND `locale`='deDE') OR (`entry`=212630 AND `locale`='deDE') OR (`entry`=212627 AND `locale`='deDE') OR (`entry`=210806 AND `locale`='deDE') OR (`entry`=209890 AND `locale`='deDE') OR (`entry`=209889 AND `locale`='deDE') OR (`entry`=209888 AND `locale`='deDE') OR (`entry`=209887 AND `locale`='deDE') OR (`entry`=209885 AND `locale`='deDE') OR (`entry`=215779 AND `locale`='deDE') OR (`entry`=210613 AND `locale`='deDE') OR (`entry`=175544 AND `locale`='deDE') OR (`entry`=210674 AND `locale`='deDE') OR (`entry`=210616 AND `locale`='deDE') OR (`entry`=210614 AND `locale`='deDE') OR (`entry`=210611 AND `locale`='deDE') OR (`entry`=211795 AND `locale`='deDE') OR (`entry`=210612 AND `locale`='deDE') OR (`entry`=210623 AND `locale`='deDE') OR (`entry`=210622 AND `locale`='deDE') OR (`entry`=210615 AND `locale`='deDE') OR (`entry`=209595 AND `locale`='deDE') OR (`entry`=209586 AND `locale`='deDE') OR (`entry`=211773 AND `locale`='deDE') OR (`entry`=211576 AND `locale`='deDE') OR (`entry`=211573 AND `locale`='deDE') OR (`entry`=210892 AND `locale`='deDE') OR (`entry`=211575 AND `locale`='deDE') OR (`entry`=211577 AND `locale`='deDE') OR (`entry`=211574 AND `locale`='deDE') OR (`entry`=211609 AND `locale`='deDE') OR (`entry`=211608 AND `locale`='deDE') OR (`entry`=211607 AND `locale`='deDE') OR (`entry`=214644 AND `locale`='deDE') OR (`entry`=209932 AND `locale`='deDE') OR (`entry`=209830 AND `locale`='deDE') OR (`entry`=209829 AND `locale`='deDE') OR (`entry`=209828 AND `locale`='deDE') OR (`entry`=209827 AND `locale`='deDE') OR (`entry`=215183 AND `locale`='deDE') OR (`entry`=215182 AND `locale`='deDE') OR (`entry`=215181 AND `locale`='deDE') OR (`entry`=215184 AND `locale`='deDE') OR (`entry`=209853 AND `locale`='deDE') OR (`entry`=209852 AND `locale`='deDE') OR (`entry`=209851 AND `locale`='deDE') OR (`entry`=209850 AND `locale`='deDE') OR (`entry`=210859 AND `locale`='deDE') OR (`entry`=213191 AND `locale`='deDE') OR (`entry`=211660 AND `locale`='deDE') OR (`entry`=209845 AND `locale`='deDE') OR (`entry`=209328 AND `locale`='deDE') OR (`entry`=215883 AND `locale`='deDE') OR (`entry`=215878 AND `locale`='deDE') OR (`entry`=215655 AND `locale`='deDE') OR (`entry`=215652 AND `locale`='deDE') OR (`entry`=215352 AND `locale`='deDE') OR (`entry`=215653 AND `locale`='deDE') OR (`entry`=215657 AND `locale`='deDE') OR (`entry`=215654 AND `locale`='deDE') OR (`entry`=215656 AND `locale`='deDE') OR (`entry`=215351 AND `locale`='deDE') OR (`entry`=212171 AND `locale`='deDE') OR (`entry`=214565 AND `locale`='deDE') OR (`entry`=214564 AND `locale`='deDE') OR (`entry`=215122 AND `locale`='deDE') OR (`entry`=215123 AND `locale`='deDE') OR (`entry`=211718 AND `locale`='deDE') OR (`entry`=211717 AND `locale`='deDE') OR (`entry`=213840 AND `locale`='deDE') OR (`entry`=213839 AND `locale`='deDE') OR (`entry`=213838 AND `locale`='deDE') OR (`entry`=213841 AND `locale`='deDE') OR (`entry`=212169 AND `locale`='deDE') OR (`entry`=215187 AND `locale`='deDE') OR (`entry`=215185 AND `locale`='deDE') OR (`entry`=215350 AND `locale`='deDE') OR (`entry`=215349 AND `locale`='deDE') OR (`entry`=215348 AND `locale`='deDE') OR (`entry`=215354 AND `locale`='deDE') OR (`entry`=215353 AND `locale`='deDE') OR (`entry`=212174 AND `locale`='deDE') OR (`entry`=209353 AND `locale`='deDE') OR (`entry`=214868 AND `locale`='deDE') OR (`entry`=214867 AND `locale`='deDE') OR (`entry`=214865 AND `locale`='deDE') OR (`entry`=209330 AND `locale`='deDE') OR (`entry`=214866 AND `locale`='deDE') OR (`entry`=214945 AND `locale`='deDE') OR (`entry`=214877 AND `locale`='deDE') OR (`entry`=212023 AND `locale`='deDE') OR (`entry`=212036 AND `locale`='deDE') OR (`entry`=212024 AND `locale`='deDE') OR (`entry`=212026 AND `locale`='deDE') OR (`entry`=212021 AND `locale`='deDE') OR (`entry`=212025 AND `locale`='deDE') OR (`entry`=212033 AND `locale`='deDE') OR (`entry`=212032 AND `locale`='deDE') OR (`entry`=212031 AND `locale`='deDE') OR (`entry`=209355 AND `locale`='deDE') OR (`entry`=212027 AND `locale`='deDE') OR (`entry`=195569 AND `locale`='deDE') OR (`entry`=215862 AND `locale`='deDE') OR (`entry`=213327 AND `locale`='deDE') OR (`entry`=212035 AND `locale`='deDE') OR (`entry`=212034 AND `locale`='deDE') OR (`entry`=212028 AND `locale`='deDE') OR (`entry`=212030 AND `locale`='deDE') OR (`entry`=212029 AND `locale`='deDE') OR (`entry`=210240 AND `locale`='deDE') OR (`entry`=210277 AND `locale`='deDE') OR (`entry`=210239 AND `locale`='deDE') OR (`entry`=210238 AND `locale`='deDE') OR (`entry`=214835 AND `locale`='deDE') OR (`entry`=214834 AND `locale`='deDE') OR (`entry`=214832 AND `locale`='deDE') OR (`entry`=210226 AND `locale`='deDE') OR (`entry`=214831 AND `locale`='deDE') OR (`entry`=214833 AND `locale`='deDE') OR (`entry`=209350 AND `locale`='deDE') OR (`entry`=210225 AND `locale`='deDE') OR (`entry`=214163 AND `locale`='deDE') OR (`entry`=212746 AND `locale`='deDE') OR (`entry`=212745 AND `locale`='deDE') OR (`entry`=211813 AND `locale`='deDE') OR (`entry`=211812 AND `locale`='deDE') OR (`entry`=211811 AND `locale`='deDE') OR (`entry`=211810 AND `locale`='deDE') OR (`entry`=211809 AND `locale`='deDE') OR (`entry`=213975 AND `locale`='deDE') OR (`entry`=209833 AND `locale`='deDE') OR (`entry`=209826 AND `locale`='deDE') OR (`entry`=209951 AND `locale`='deDE') OR (`entry`=187457 AND `locale`='deDE') OR (`entry`=186472 AND `locale`='deDE') OR (`entry`=186738 AND `locale`='deDE') OR (`entry`=186475 AND `locale`='deDE') OR (`entry`=210941 AND `locale`='deDE') OR (`entry`=210953 AND `locale`='deDE') OR (`entry`=210939 AND `locale`='deDE') OR (`entry`=210940 AND `locale`='deDE') OR (`entry`=215799 AND `locale`='deDE') OR (`entry`=214416 AND `locale`='deDE') OR (`entry`=213976 AND `locale`='deDE') OR (`entry`=213446 AND `locale`='deDE') OR (`entry`=211464 AND `locale`='deDE') OR (`entry`=209886 AND `locale`='deDE') OR (`entry`=211465 AND `locale`='deDE') OR (`entry`=211466 AND `locale`='deDE') OR (`entry`=211651 AND `locale`='deDE') OR (`entry`=210925 AND `locale`='deDE') OR (`entry`=213977 AND `locale`='deDE') OR (`entry`=211769 AND `locale`='deDE') OR (`entry`=214411 AND `locale`='deDE') OR (`entry`=216309 AND `locale`='deDE') OR (`entry`=214953 AND `locale`='deDE') OR (`entry`=214950 AND `locale`='deDE') OR (`entry`=214949 AND `locale`='deDE') OR (`entry`=214954 AND `locale`='deDE') OR (`entry`=214986 AND `locale`='deDE') OR (`entry`=214955 AND `locale`='deDE') OR (`entry`=214952 AND `locale`='deDE') OR (`entry`=214951 AND `locale`='deDE') OR (`entry`=214845 AND `locale`='deDE') OR (`entry`=209313 AND `locale`='deDE') OR (`entry`=211620 AND `locale`='deDE') OR (`entry`=211621 AND `locale`='deDE') OR (`entry`=211619 AND `locale`='deDE') OR (`entry`=214855 AND `locale`='deDE') OR (`entry`=211622 AND `locale`='deDE') OR (`entry`=214844 AND `locale`='deDE') OR (`entry`=214843 AND `locale`='deDE') OR (`entry`=214506 AND `locale`='deDE') OR (`entry`=214505 AND `locale`='deDE') OR (`entry`=215963 AND `locale`='deDE') OR (`entry`=212149 AND `locale`='deDE') OR (`entry`=215962 AND `locale`='deDE') OR (`entry`=209836 AND `locale`='deDE') OR (`entry`=216427 AND `locale`='deDE') OR (`entry`=214379 AND `locale`='deDE') OR (`entry`=213415 AND `locale`='deDE') OR (`entry`=213512 AND `locale`='deDE') OR (`entry`=210856 AND `locale`='deDE') OR (`entry`=210512 AND `locale`='deDE') OR (`entry`=212178 AND `locale`='deDE') OR (`entry`=209464 AND `locale`='deDE') OR (`entry`=211494 AND `locale`='deDE') OR (`entry`=211496 AND `locale`='deDE') OR (`entry`=211495 AND `locale`='deDE') OR (`entry`=209462 AND `locale`='deDE') OR (`entry`=209463 AND `locale`='deDE') OR (`entry`=209461 AND `locale`='deDE') OR (`entry`=209460 AND `locale`='deDE') OR (`entry`=209436 AND `locale`='deDE') OR (`entry`=209863 AND `locale`='deDE') OR (`entry`=211771 AND `locale`='deDE') OR (`entry`=214372 AND `locale`='deDE') OR (`entry`=213982 AND `locale`='deDE') OR (`entry`=213981 AND `locale`='deDE') OR (`entry`=213980 AND `locale`='deDE') OR (`entry`=213979 AND `locale`='deDE') OR (`entry`=213978 AND `locale`='deDE') OR (`entry`=211956 AND `locale`='deDE') OR (`entry`=211955 AND `locale`='deDE') OR (`entry`=211957 AND `locale`='deDE') OR (`entry`=211917 AND `locale`='deDE') OR (`entry`=211919 AND `locale`='deDE') OR (`entry`=211914 AND `locale`='deDE') OR (`entry`=214878 AND `locale`='deDE') OR (`entry`=214871 AND `locale`='deDE') OR (`entry`=214870 AND `locale`='deDE') OR (`entry`=214869 AND `locale`='deDE') OR (`entry`=209622 AND `locale`='deDE') OR (`entry`=211918 AND `locale`='deDE') OR (`entry`=211913 AND `locale`='deDE') OR (`entry`=211910 AND `locale`='deDE') OR (`entry`=211954 AND `locale`='deDE') OR (`entry`=211912 AND `locale`='deDE') OR (`entry`=211920 AND `locale`='deDE') OR (`entry`=211921 AND `locale`='deDE') OR (`entry`=211915 AND `locale`='deDE') OR (`entry`=211916 AND `locale`='deDE') OR (`entry`=214884 AND `locale`='deDE') OR (`entry`=211922 AND `locale`='deDE') OR (`entry`=211923 AND `locale`='deDE') OR (`entry`=214873 AND `locale`='deDE') OR (`entry`=211926 AND `locale`='deDE') OR (`entry`=211925 AND `locale`='deDE') OR (`entry`=211924 AND `locale`='deDE') OR (`entry`=211947 AND `locale`='deDE') OR (`entry`=214890 AND `locale`='deDE') OR (`entry`=214892 AND `locale`='deDE') OR (`entry`=214891 AND `locale`='deDE') OR (`entry`=211946 AND `locale`='deDE') OR (`entry`=209699 AND `locale`='deDE') OR (`entry`=211927 AND `locale`='deDE') OR (`entry`=211945 AND `locale`='deDE') OR (`entry`=211931 AND `locale`='deDE') OR (`entry`=211930 AND `locale`='deDE') OR (`entry`=211928 AND `locale`='deDE') OR (`entry`=211929 AND `locale`='deDE') OR (`entry`=214898 AND `locale`='deDE') OR (`entry`=214897 AND `locale`='deDE') OR (`entry`=209628 AND `locale`='deDE') OR (`entry`=211934 AND `locale`='deDE') OR (`entry`=214903 AND `locale`='deDE') OR (`entry`=214902 AND `locale`='deDE') OR (`entry`=209788 AND `locale`='deDE') OR (`entry`=209787 AND `locale`='deDE') OR (`entry`=209786 AND `locale`='deDE') OR (`entry`=211933 AND `locale`='deDE') OR (`entry`=211932 AND `locale`='deDE') OR (`entry`=214905 AND `locale`='deDE') OR (`entry`=211935 AND `locale`='deDE') OR (`entry`=211937 AND `locale`='deDE') OR (`entry`=211936 AND `locale`='deDE') OR (`entry`=214904 AND `locale`='deDE') OR (`entry`=211642 AND `locale`='deDE') OR (`entry`=209825 AND `locale`='deDE') OR (`entry`=209551 AND `locale`='deDE') OR (`entry`=209550 AND `locale`='deDE') OR (`entry`=215694 AND `locale`='deDE') OR (`entry`=215967 AND `locale`='deDE') OR (`entry`=215681 AND `locale`='deDE') OR (`entry`=215647 AND `locale`='deDE') OR (`entry`=215588 AND `locale`='deDE') OR (`entry`=215646 AND `locale`='deDE') OR (`entry`=215650 AND `locale`='deDE') OR (`entry`=215649 AND `locale`='deDE') OR (`entry`=215709 AND `locale`='deDE') OR (`entry`=215692 AND `locale`='deDE') OR (`entry`=215691 AND `locale`='deDE') OR (`entry`=215690 AND `locale`='deDE') OR (`entry`=215702 AND `locale`='deDE') OR (`entry`=214988 AND `locale`='deDE') OR (`entry`=209578 AND `locale`='deDE') OR (`entry`=210804 AND `locale`='deDE') OR (`entry`=214894 AND `locale`='deDE') OR (`entry`=214893 AND `locale`='deDE') OR (`entry`=214987 AND `locale`='deDE') OR (`entry`=212592 AND `locale`='deDE') OR (`entry`=212591 AND `locale`='deDE') OR (`entry`=212582 AND `locale`='deDE') OR (`entry`=212579 AND `locale`='deDE') OR (`entry`=212578 AND `locale`='deDE') OR (`entry`=212563 AND `locale`='deDE') OR (`entry`=212562 AND `locale`='deDE') OR (`entry`=212589 AND `locale`='deDE') OR (`entry`=212570 AND `locale`='deDE') OR (`entry`=212565 AND `locale`='deDE') OR (`entry`=212580 AND `locale`='deDE') OR (`entry`=212561 AND `locale`='deDE') OR (`entry`=212559 AND `locale`='deDE') OR (`entry`=212594 AND `locale`='deDE') OR (`entry`=212566 AND `locale`='deDE') OR (`entry`=212571 AND `locale`='deDE') OR (`entry`=212581 AND `locale`='deDE') OR (`entry`=212590 AND `locale`='deDE') OR (`entry`=212587 AND `locale`='deDE') OR (`entry`=212560 AND `locale`='deDE') OR (`entry`=212567 AND `locale`='deDE') OR (`entry`=212576 AND `locale`='deDE') OR (`entry`=212568 AND `locale`='deDE') OR (`entry`=212588 AND `locale`='deDE') OR (`entry`=212593 AND `locale`='deDE') OR (`entry`=212569 AND `locale`='deDE') OR (`entry`=212595 AND `locale`='deDE') OR (`entry`=212564 AND `locale`='deDE') OR (`entry`=212572 AND `locale`='deDE') OR (`entry`=212583 AND `locale`='deDE') OR (`entry`=212584 AND `locale`='deDE') OR (`entry`=212586 AND `locale`='deDE') OR (`entry`=212585 AND `locale`='deDE') OR (`entry`=215695 AND `locale`='deDE') OR (`entry`=215859 AND `locale`='deDE') OR (`entry`=215860 AND `locale`='deDE') OR (`entry`=219095 AND `locale`='deDE') OR (`entry`=215693 AND `locale`='deDE') OR (`entry`=215687 AND `locale`='deDE') OR (`entry`=215683 AND `locale`='deDE') OR (`entry`=215686 AND `locale`='deDE') OR (`entry`=215684 AND `locale`='deDE') OR (`entry`=212325 AND `locale`='deDE') OR (`entry`=215404 AND `locale`='deDE') OR (`entry`=214723 AND `locale`='deDE') OR (`entry`=214791 AND `locale`='deDE') OR (`entry`=215882 AND `locale`='deDE') OR (`entry`=212181 AND `locale`='deDE') OR (`entry`=215844 AND `locale`='deDE') OR (`entry`=214864 AND `locale`='deDE') OR (`entry`=212182 AND `locale`='deDE') OR (`entry`=212185 AND `locale`='deDE') OR (`entry`=211990 AND `locale`='deDE') OR (`entry`=212193 AND `locale`='deDE') OR (`entry`=212183 AND `locale`='deDE') OR (`entry`=212186 AND `locale`='deDE') OR (`entry`=212191 AND `locale`='deDE') OR (`entry`=212184 AND `locale`='deDE') OR (`entry`=215849 AND `locale`='deDE') OR (`entry`=212192 AND `locale`='deDE') OR (`entry`=215843 AND `locale`='deDE') OR (`entry`=212900 AND `locale`='deDE') OR (`entry`=212966 AND `locale`='deDE') OR (`entry`=213366 AND `locale`='deDE') OR (`entry`=212967 AND `locale`='deDE') OR (`entry`=215850 AND `locale`='deDE') OR (`entry`=215422 AND `locale`='deDE') OR (`entry`=215563 AND `locale`='deDE') OR (`entry`=215423 AND `locale`='deDE') OR (`entry`=215564 AND `locale`='deDE') OR (`entry`=215562 AND `locale`='deDE') OR (`entry`=213748 AND `locale`='deDE') OR (`entry`=209815 AND `locale`='deDE') OR (`entry`=209814 AND `locale`='deDE') OR (`entry`=211488 AND `locale`='deDE') OR (`entry`=211861 AND `locale`='deDE') OR (`entry`=209855 AND `locale`='deDE') OR (`entry`=214417 AND `locale`='deDE') OR (`entry`=211860 AND `locale`='deDE') OR (`entry`=212969 AND `locale`='deDE') OR (`entry`=212926 AND `locale`='deDE') OR (`entry`=212925 AND `locale`='deDE') OR (`entry`=212288 AND `locale`='deDE') OR (`entry`=209780 AND `locale`='deDE') OR (`entry`=212924 AND `locale`='deDE') OR (`entry`=177664 AND `locale`='deDE') OR (`entry`=213004 AND `locale`='deDE') OR (`entry`=214421 AND `locale`='deDE') OR (`entry`=212693 AND `locale`='deDE') OR (`entry`=212287 AND `locale`='deDE') OR (`entry`=211796 AND `locale`='deDE') OR (`entry`=212285 AND `locale`='deDE') OR (`entry`=212286 AND `locale`='deDE') OR (`entry`=211797 AND `locale`='deDE') OR (`entry`=212284 AND `locale`='deDE') OR (`entry`=214420 AND `locale`='deDE') OR (`entry`=214419 AND `locale`='deDE') OR (`entry`=209857 AND `locale`='deDE') OR (`entry`=212283 AND `locale`='deDE') OR (`entry`=212296 AND `locale`='deDE') OR (`entry`=212404 AND `locale`='deDE') OR (`entry`=209903 AND `locale`='deDE');
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(213741, 'deDE', 'Uralter Stab der Jinyu', '', NULL, 23222),(214338, 'deDE', 'Opfergabe des Erinnerns', '', NULL, 23222),(210691, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(210690, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210724, 'deDE', 'Briefkasten', '', NULL, 23222),
(212621, 'deDE', 'Hocker', '', NULL, 23222),
(212625, 'deDE', 'Bett', '', NULL, 23222),
(212624, 'deDE', 'Hocker', '', NULL, 23222),
(212623, 'deDE', 'Hocker', '', NULL, 23222),
(212622, 'deDE', 'Hocker', '', NULL, 23222),
(209345, 'deDE', 'Obstgartenwerkzeug', '', NULL, 23222),
(209344, 'deDE', 'Obstgartenwerkzeug', '', NULL, 23222),
(211701, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(211097, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211700, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(211098, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211862, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211476, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214510, 'deDE', 'Sha-berührtes Kraut', '', NULL, 23222),
(210921, 'deDE', 'Himmlische Jade', '', NULL, 23222),
(213832, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(210054, 'deDE', 'Übungsziel', '', NULL, 23222),
(210086, 'deDE', 'Uraltes Sutra', '', NULL, 23222),
(210085, 'deDE', 'Uraltes Sutra', '', NULL, 23222),
(214567, 'deDE', 'Hocker', '', NULL, 23222),
(214566, 'deDE', 'Hocker', '', NULL, 23222),
(213833, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214408, 'deDE', 'Der General und die Grummel', '', NULL, 23222),
(212898, 'deDE', 'Bank', '', NULL, 23222),
(212769, 'deDE', 'Bank', '', NULL, 23222),
(212770, 'deDE', 'Bank', '', NULL, 23222),
(215781, 'deDE', 'Shaohaos Glocke', '', NULL, 23222),
(213170, 'deDE', 'Versammlungsstein', '', NULL, 23222),
(213421, 'deDE', 'Des Kaisers Bürde - Teil 3', '', NULL, 23222),
(212613, 'deDE', 'Feuerstelle', '', NULL, 23222),
(212612, 'deDE', 'Hocker', '', NULL, 23222),
(212611, 'deDE', 'Hocker', '', NULL, 23222),
(212899, 'deDE', 'Instance Portal (Party + Heroic + Challenge)', '', NULL, 23222),
(210565, 'deDE', 'Dunkle Erde', '', NULL, 23222),
(212887, 'deDE', 'Bank', '', NULL, 23222),
(214414, 'deDE', 'Der Kriegsherr und der Mönch', '', NULL, 23222),
(212897, 'deDE', 'Bank', '', NULL, 23222),
(210116, 'deDE', 'Glocke der Tausend Flüsterworte', '', NULL, 23222),
(214425, 'deDE', 'Die Gründung des Ordens der Wolkenschlange', '', NULL, 23222),
(214410, 'deDE', 'Der Ackerbauer und der Mönch', '', NULL, 23222),
(212892, 'deDE', 'Bank', '', NULL, 23222),
(210807, 'deDE', 'Briefkasten', '', NULL, 23222),
(212632, 'deDE', 'Hocker', '', NULL, 23222),
(212631, 'deDE', 'Bank', '', NULL, 23222),
(212629, 'deDE', 'Hocker', '', NULL, 23222),
(212628, 'deDE', 'Hocker', '', NULL, 23222),
(212626, 'deDE', 'Hocker', '', NULL, 23222),
(213436, 'deDE', 'Holzschnitte', '', NULL, 23222),
(188216, 'deDE', 'Rüstungsständer', '', NULL, 23222),
(212633, 'deDE', 'Bett', '', NULL, 23222),
(212630, 'deDE', 'Bank', '', NULL, 23222),
(212627, 'deDE', 'Hocker', '', NULL, 23222),
(210806, 'deDE', 'Grill Counter', '', NULL, 23222),
(209890, 'deDE', 'Stärkendes Herz', '', NULL, 23222),
(209889, 'deDE', 'Stärkende Seele', '', NULL, 23222),
(209888, 'deDE', 'Stärkender Geist', '', NULL, 23222),
(209887, 'deDE', 'Altar', '', NULL, 23222),
(209885, 'deDE', 'Stärkender Leib', '', NULL, 23222),
(215779, 'deDE', 'Wasserschmieden', '', NULL, 23222),
(210613, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(175544, 'deDE', 'Kugel und Kette', '', NULL, 23222),
(210674, 'deDE', 'Ho-zen-Stab', '', NULL, 23222),
(210616, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210614, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210611, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211795, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(210612, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210623, 'deDE', 'Amboss', '', NULL, 23222),
(210622, 'deDE', 'Schmiede', '', NULL, 23222),
(210615, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(209595, 'deDE', 'Ho-zen-Schädel', '', NULL, 23222),
(209586, 'deDE', 'Ho-zen-Käfig', '', NULL, 23222),
(211773, 'deDE', 'Fischkorb der Jinyu', '', NULL, 23222),
(211576, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211573, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210892, 'deDE', 'Flagge der Allianz', '', NULL, 23222),
(211575, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211577, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211574, 'deDE', 'Briefkasten', '', NULL, 23222),
(211609, 'deDE', 'Amboss', '', NULL, 23222),
(211608, 'deDE', 'Amboss', '', NULL, 23222),
(211607, 'deDE', 'Amboss', '', NULL, 23222),
(214644, 'deDE', 'Schmiede', '', NULL, 23222),
(209932, 'deDE', 'Wasserschmiede', '', NULL, 23222),
(209830, 'deDE', 'Zenlotus', '', NULL, 23222),
(209829, 'deDE', 'Zenlotus', '', NULL, 23222),
(209828, 'deDE', 'Zenlotus', '', NULL, 23222),
(209827, 'deDE', 'Zenlotus', '', NULL, 23222),
(215183, 'deDE', 'Amboss', '', NULL, 23222),
(215182, 'deDE', 'Schmiede', '', NULL, 23222),
(215181, 'deDE', 'Amboss', '', NULL, 23222),
(215184, 'deDE', 'Amboss', '', NULL, 23222),
(209853, 'deDE', 'Beruhigendes Räucherwerk', '', NULL, 23222),
(209852, 'deDE', 'Meditationskerze', '', NULL, 23222),
(209851, 'deDE', 'Meditationskerze', '', NULL, 23222),
(209850, 'deDE', 'Meditationskerze', '', NULL, 23222),
(210859, 'deDE', 'Stein des Sehers', '', NULL, 23222),
(213191, 'deDE', 'Vogelfutter', '', NULL, 23222),
(211660, 'deDE', 'Geformter Baum', '', NULL, 23222),
(209845, 'deDE', 'Köstliches Gebräu', '', NULL, 23222),
(209328, 'deDE', 'Reiches Geistereisenvorkommen', '', NULL, 23222),
(215883, 'deDE', 'Mogualtar', '', NULL, 23222),
(215878, 'deDE', 'Ga''truls Aufzeichnungen', '', NULL, 23222),
(215655, 'deDE', 'Dämonisches Portal', '', NULL, 23222),
(215652, 'deDE', 'Dämonisches Portal', '', NULL, 23222),
(215352, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(215653, 'deDE', 'Dämonisches Portal', '', NULL, 23222),
(215657, 'deDE', 'Dämonisches Portal', '', NULL, 23222),
(215654, 'deDE', 'Dämonisches Portal', '', NULL, 23222),
(215656, 'deDE', 'Dämonisches Portal', '', NULL, 23222),
(215351, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212171, 'deDE', 'Ein Schwarm Jadelungenfische', '', NULL, 23222),
(214565, 'deDE', 'Hocker', '', NULL, 23222),
(214564, 'deDE', 'Hocker', '', NULL, 23222),
(215122, 'deDE', 'Leere Notfalltaucherhelmkiste', '', NULL, 23222),
(215123, 'deDE', 'Notfalltaucherhelm', '', NULL, 23222),
(211718, 'deDE', 'Werkzeugkasten', '', NULL, 23222),
(211717, 'deDE', 'Werkzeugkasten', '', NULL, 23222),
(213840, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(213839, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(213838, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(213841, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(212169, 'deDE', 'Ein Schwarm riesiger Mantisgarnelen', '', NULL, 23222),
(215187, 'deDE', 'Schraubenmutter', '', NULL, 23222),
(215185, 'deDE', 'Quelle', '', NULL, 23222),
(215350, 'deDE', 'Amboss', '', NULL, 23222),
(215349, 'deDE', 'Amboss', '', NULL, 23222),
(215348, 'deDE', 'Schmiede', '', NULL, 23222),
(215354, 'deDE', 'Kessel', '', NULL, 23222),
(215353, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212174, 'deDE', 'Ein Schwarm Riffkraken', '', NULL, 23222),
(209353, 'deDE', 'Regenmohn', '', NULL, 23222),
(214868, 'deDE', 'Gestohlene Stiefel', '', NULL, 23222),
(214867, 'deDE', 'Gestohlene Stiefel', '', NULL, 23222),
(214865, 'deDE', 'Gestohlene Stiefel', '', NULL, 23222),
(209330, 'deDE', 'Reiche Trilliumader', '', NULL, 23222),
(214866, 'deDE', 'Gestohlene Stiefel', '', NULL, 23222),
(214945, 'deDE', 'Onyxei', '', NULL, 23222),
(214877, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212023, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212036, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212024, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212026, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212021, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212025, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212033, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212032, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212031, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(209355, 'deDE', 'Narrenkappe', '', NULL, 23222),
(212027, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(195569, 'deDE', 'Ritueller Schädelhaufen', '', NULL, 23222),
(215862, 'deDE', 'Saurokbanner', '', NULL, 23222),
(213327, 'deDE', 'Die Saurok', '', NULL, 23222),
(212035, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212034, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212028, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212030, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212029, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210240, 'deDE', 'Schlangenei', '', NULL, 23222),
(210277, 'deDE', 'Schlängelschuppenwaffen', '', NULL, 23222),
(210239, 'deDE', 'Schlangenei', '', NULL, 23222),
(210238, 'deDE', 'Schlangenei', '', NULL, 23222),
(214835, 'deDE', 'Zerbrochenes Schlangenei', '', NULL, 23222),
(214834, 'deDE', 'Zerbrochenes Schlangenei', '', NULL, 23222),
(214832, 'deDE', 'Zerbrochenes Schlangenei', '', NULL, 23222),
(210226, 'deDE', 'Zerbrochenes Schlangenei', '', NULL, 23222),
(214831, 'deDE', 'Zerbrochenes Schlangenei', '', NULL, 23222),
(214833, 'deDE', 'Zerbrochenes Schlangenei', '', NULL, 23222),
(209350, 'deDE', 'Seidenkraut', '', NULL, 23222),
(210225, 'deDE', 'Windstillbusch', '', NULL, 23222),
(214163, 'deDE', 'Ofen', '', NULL, 23222),
(212746, 'deDE', 'Stuhl', '', NULL, 23222),
(212745, 'deDE', 'Herd', '', NULL, 23222),
(211813, 'deDE', 'Hocker', '', NULL, 23222),
(211812, 'deDE', 'Hocker', '', NULL, 23222),
(211811, 'deDE', 'Hocker', '', NULL, 23222),
(211810, 'deDE', 'Hocker', '', NULL, 23222),
(211809, 'deDE', 'Hocker', '', NULL, 23222),
(213975, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(209833, 'deDE', 'Seidenstück', '', NULL, 23222),
(209826, 'deDE', 'Seidenstück', '', NULL, 23222),
(209951, 'deDE', 'Seidenstück', '', NULL, 23222),
(187457, 'deDE', 'Kohlenpfanne der tanzenden Flammen', '', NULL, 23222),
(186472, 'deDE', 'Kessel mit Goblingumbo', '', NULL, 23222),
(186738, 'deDE', 'Braufestfässchen', '', NULL, 23222),
(186475, 'deDE', 'Angelstuhl', '', NULL, 23222),
(210941, 'deDE', 'Kochtopf', '', NULL, 23222),
(210953, 'deDE', 'Briefkasten', '', NULL, 23222),
(210939, 'deDE', 'Amboss', '', NULL, 23222),
(210940, 'deDE', 'Schmiede', '', NULL, 23222),
(215799, 'deDE', 'Des Kaisers Bürde - Teil 1', '', NULL, 23222),
(214416, 'deDE', 'Die Natur des Friedens', '', NULL, 23222),
(213976, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(213446, 'deDE', 'Schrein zu Ehren des letzten Kaisers', '', NULL, 23222),
(211464, 'deDE', 'Schmiede', '', NULL, 23222),
(209886, 'deDE', 'Amboss', '', NULL, 23222),
(211465, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211466, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211651, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210925, 'deDE', 'Blood Elf Pillow 02 (Scale 2)', '', NULL, 23222),
(213977, 'deDE', 'Hocker', '', NULL, 23222),
(211769, 'deDE', 'Briefkasten', '', NULL, 23222),
(214411, 'deDE', 'Der Angler und die Mönche', '', NULL, 23222),
(216309, 'deDE', 'Grilltisch', '', NULL, 23222),
(214953, 'deDE', 'Doodad_hz_cage_005', '', NULL, 23222),
(214950, 'deDE', 'Doodad_hz_cage_002', '', NULL, 23222),
(214949, 'deDE', 'Doodad_hz_cage_001', '', NULL, 23222),
(214954, 'deDE', 'Doodad_hz_cage_006', '', NULL, 23222),
(214986, 'deDE', 'Gestohlenes Fass aus Sri-La', '', NULL, 23222),
(214955, 'deDE', 'Doodad_hz_cage_007', '', NULL, 23222),
(214952, 'deDE', 'Doodad_hz_cage_004', '', NULL, 23222),
(214951, 'deDE', 'Doodad_hz_cage_003', '', NULL, 23222),
(214845, 'deDE', 'Uunabierkrug', '', NULL, 23222),
(209313, 'deDE', 'Trilliumader', '', NULL, 23222),
(211620, 'deDE', 'Effekt des Sha', '', NULL, 23222),
(211621, 'deDE', 'Effekt des Sha', '', NULL, 23222),
(211619, 'deDE', 'Effekt des Sha', '', NULL, 23222),
(214855, 'deDE', 'Sha-Befall', '', NULL, 23222),
(211622, 'deDE', 'Effekt des Sha', '', NULL, 23222),
(214844, 'deDE', 'Schlangenschuppe', '', NULL, 23222),
(214843, 'deDE', 'Schlangenschuppe', '', NULL, 23222),
(214506, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214505, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(215963, 'deDE', 'Hocker', '', NULL, 23222),
(212149, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(215962, 'deDE', 'Hocker', '', NULL, 23222),
(209836, 'deDE', 'Frisch gefallene Blütenblätter', '', NULL, 23222),
(216427, 'deDE', 'Hammer der Gefährten', '', NULL, 23222),
(214379, 'deDE', 'Zerbrochenes Räuchergefäß', '', NULL, 23222),
(213415, 'deDE', 'Die ersten Mönche', '', NULL, 23222),
(213512, 'deDE', 'Xin Wo Yin mit dem gebrochenen Herzen', '', NULL, 23222),
(210856, 'deDE', 'Blanches Destille', '', NULL, 23222),
(210512, 'deDE', 'Blanches Blitzableiter', '', NULL, 23222),
(212178, 'deDE', 'Reisschüssel', '', NULL, 23222),
(209464, 'deDE', 'Rattanrute', '', NULL, 23222),
(211494, 'deDE', 'Briefkasten', '', NULL, 23222),
(211496, 'deDE', 'Schmiede', '', NULL, 23222),
(211495, 'deDE', 'Amboss', '', NULL, 23222),
(209462, 'deDE', 'Rattanrute', '', NULL, 23222),
(209463, 'deDE', 'Rattanrute', '', NULL, 23222),
(209461, 'deDE', 'Rattanrute', '', NULL, 23222),
(209460, 'deDE', 'Rattanrute', '', NULL, 23222),
(209436, 'deDE', 'Reife Orange', '', NULL, 23222),
(209863, 'deDE', 'Jadebrocken', '', NULL, 23222),
(211771, 'deDE', 'Jade Mines Rubble', '', NULL, 23222),
(214372, 'deDE', 'Das Vermächtnis von Kaiser Tsao', '', NULL, 23222),
(213982, 'deDE', 'Hocker', '', NULL, 23222),
(213981, 'deDE', 'Hocker', '', NULL, 23222),
(213980, 'deDE', 'Hocker', '', NULL, 23222),
(213979, 'deDE', 'Hocker', '', NULL, 23222),
(213978, 'deDE', 'Hocker', '', NULL, 23222),
(211956, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211955, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211957, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211917, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211919, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211914, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214878, 'deDE', 'Waffe einer zerbrochenen Mogustatue', '', NULL, 23222),
(214871, 'deDE', 'Zerschmetterter Zerstörer', '', NULL, 23222),
(214870, 'deDE', 'Aufrechte zerbrochene Mogustatue', '', NULL, 23222),
(214869, 'deDE', 'Schulter einer zerbrochenen Mogustatue', '', NULL, 23222),
(209622, 'deDE', 'Arm einer zerbrochenen Mogustatue', '', NULL, 23222),
(211918, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211913, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211910, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211954, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211912, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211920, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211921, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211915, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211916, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214884, 'deDE', 'Geisterbindergefäß', '', NULL, 23222),
(211922, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211923, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214873, 'deDE', 'Leerer Zerstörer', '', NULL, 23222),
(211926, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211925, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211924, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211947, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214890, 'deDE', 'Seelengefäßständer', '', NULL, 23222),
(214892, 'deDE', 'Zerbrochenes Seelengefäß', '', NULL, 23222),
(214891, 'deDE', 'Großes Seelengefäß', '', NULL, 23222),
(211946, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(209699, 'deDE', 'Jadecong', '', NULL, 23222),
(211927, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211945, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211931, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211930, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211928, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211929, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214898, 'deDE', 'PA Book Open 03', '', NULL, 23222),
(214897, 'deDE', 'Überreste von Pei-Zhi', '', NULL, 23222),
(209628, 'deDE', 'Mogualtar', '', NULL, 23222),
(211934, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214903, 'deDE', 'Der Tan-Chao', '', NULL, 23222),
(214902, 'deDE', 'Tidemist Cap Prop', '', NULL, 23222),
(209788, 'deDE', 'Jade Cong Prop', '', NULL, 23222),
(209787, 'deDE', 'Gesprungene Ritualschale', '', NULL, 23222),
(209786, 'deDE', 'Halter für geruchsintensive Ritualkerze', '', NULL, 23222),
(211933, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211932, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214905, 'deDE', 'Mogu Statue Ruined', '', NULL, 23222),
(211935, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211937, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(211936, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214904, 'deDE', 'Mogu Statue Base 01', '', NULL, 23222),
(211642, 'deDE', 'Goldene Glücksmünze', '', NULL, 23222),
(209825, 'deDE', 'Gezeitennebelkappe', '', NULL, 23222),
(209551, 'deDE', 'Kochender Kessel', '', NULL, 23222),
(209550, 'deDE', 'Rötelblattbüschel', '', NULL, 23222),
(215694, 'deDE', 'Brennende Barrikadenstütze', '', NULL, 23222),
(215967, 'deDE', 'Kanonenkugelstapel', '', NULL, 23222),
(215681, 'deDE', 'Barrikade', '', NULL, 23222),
(215647, 'deDE', 'Barrikade', '', NULL, 23222),
(215588, 'deDE', 'Beschädigte Kanone der Donnerwehr', '', NULL, 23222),
(215646, 'deDE', 'Barrikade', '', NULL, 23222),
(215650, 'deDE', 'Sprengstoff der Donnerwehr', '', NULL, 23222),
(215649, 'deDE', 'Munitionsvorrat der Donnerwehr', '', NULL, 23222),
(215709, 'deDE', 'Fels', '', NULL, 23222),
(215692, 'deDE', 'Zerbrochenes Honigbräufass', '', NULL, 23222),
(215691, 'deDE', 'Wagenteil', '', NULL, 23222),
(215690, 'deDE', 'Wagen', '', NULL, 23222),
(215702, 'deDE', 'Ast', '', NULL, 23222),
(214988, 'deDE', 'Briefkasten', '', NULL, 23222),
(209578, 'deDE', 'Instabiler Solenoid', '', NULL, 23222),
(210804, 'deDE', 'Portal nach Orgrimmar', '', NULL, 23222),
(214894, 'deDE', 'Amboss', '', NULL, 23222),
(214893, 'deDE', 'Schmiede', '', NULL, 23222),
(214987, 'deDE', 'Briefkasten', '', NULL, 23222),
(212592, 'deDE', 'Hocker', '', NULL, 23222),
(212591, 'deDE', 'Hocker', '', NULL, 23222),
(212582, 'deDE', 'Feuerstelle', '', NULL, 23222),
(212579, 'deDE', 'Hocker', '', NULL, 23222),
(212578, 'deDE', 'Hocker', '', NULL, 23222),
(212563, 'deDE', 'Bank', '', NULL, 23222),
(212562, 'deDE', 'Bank', '', NULL, 23222),
(212589, 'deDE', 'Hocker', '', NULL, 23222),
(212570, 'deDE', 'Hocker', '', NULL, 23222),
(212565, 'deDE', 'Bank', '', NULL, 23222),
(212580, 'deDE', 'Bank', '', NULL, 23222),
(212561, 'deDE', 'Bank', '', NULL, 23222),
(212559, 'deDE', 'Bank', '', NULL, 23222),
(212594, 'deDE', 'Herd', '', NULL, 23222),
(212566, 'deDE', 'Bank', '', NULL, 23222),
(212571, 'deDE', 'Hocker', '', NULL, 23222),
(212581, 'deDE', 'Bank', '', NULL, 23222),
(212590, 'deDE', 'Hocker', '', NULL, 23222),
(212587, 'deDE', 'Tisch', '', NULL, 23222),
(212560, 'deDE', 'Bank', '', NULL, 23222),
(212567, 'deDE', 'Bank', '', NULL, 23222),
(212576, 'deDE', 'Hocker', '', NULL, 23222),
(212568, 'deDE', 'Bank', '', NULL, 23222),
(212588, 'deDE', 'Hocker', '', NULL, 23222),
(212593, 'deDE', 'Hocker', '', NULL, 23222),
(212569, 'deDE', 'Bank', '', NULL, 23222),
(212595, 'deDE', 'Herd', '', NULL, 23222),
(212564, 'deDE', 'Bank', '', NULL, 23222),
(212572, 'deDE', 'Hocker', '', NULL, 23222),
(212583, 'deDE', 'Bank', '', NULL, 23222),
(212584, 'deDE', 'Bank', '', NULL, 23222),
(212586, 'deDE', 'Bank', '', NULL, 23222),
(212585, 'deDE', 'Bank', '', NULL, 23222),
(215695, 'deDE', 'Fass Honigbräu', '', NULL, 23222),
(215859, 'deDE', 'Sha GroundPatch Small Tendrils', '', NULL, 23222),
(215860, 'deDE', 'Sha GroundPatch Med Tendrils', '', NULL, 23222),
(219095, 'deDE', 'Mogu Statue Feet - Collision', '', NULL, 23222),
(215693, 'deDE', 'Brennende Barrikadenstütze', '', NULL, 23222),
(215687, 'deDE', 'Barrikadenstütze', '', NULL, 23222),
(215683, 'deDE', 'Barrikade der Horde', '', NULL, 23222),
(215686, 'deDE', 'Vorräte der Horde', '', NULL, 23222),
(215684, 'deDE', 'Vorräte der Horde', '', NULL, 23222),
(212325, 'deDE', 'Doodad_hz_camp_fire_001', '', NULL, 23222),
(215404, 'deDE', 'Nebelschleier', '', NULL, 23222),
(214723, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214791, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(215882, 'deDE', 'Dorens Aufzeichnungen', '', NULL, 23222),
(212181, 'deDE', 'Uralte Statue', '', NULL, 23222),
(215844, 'deDE', 'Flaggenmast', '', NULL, 23222),
(214864, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212182, 'deDE', 'Torso einer uralten Statue', '', NULL, 23222),
(212185, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211990, 'deDE', 'Ho-zen-Sprache', '', NULL, 23222),
(212193, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212183, 'deDE', 'Arm einer uralten Statue', '', NULL, 23222),
(212186, 'deDE', 'Kopf einer uralten Statue', '', NULL, 23222),
(212191, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212184, 'deDE', 'Schulter einer uralten Statue', '', NULL, 23222),
(215849, 'deDE', 'Banner der Horde (groß)', '', NULL, 23222),
(212192, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(215843, 'deDE', 'Flagge der Horde', '', NULL, 23222),
(212900, 'deDE', 'Chos Teekessel', '', NULL, 23222),
(212966, 'deDE', 'Stuhl', '', NULL, 23222),
(213366, 'deDE', 'Uralter pandarischer Teekessel', '', NULL, 23222),
(212967, 'deDE', 'Stuhl', '', NULL, 23222),
(215850, 'deDE', 'Banner der Horde (klein)', '', NULL, 23222),
(215422, 'deDE', 'Jade Forest Alliance Ship Cosmetic', '', NULL, 23222),
(215563, 'deDE', 'Jade Forest Horde Ship Cosmetic', '', NULL, 23222),
(215423, 'deDE', 'Jade Forest Alliance Ship Cosmetic', '', NULL, 23222),
(215564, 'deDE', 'Jade Forest Horde Ship Cosmetic', '', NULL, 23222),
(215562, 'deDE', 'Jade Forest Horde Ship Cosmetic', '', NULL, 23222),
(213748, 'deDE', 'Pandarischer Ritualstein', '', NULL, 23222),
(209815, 'deDE', 'Zerbrochene Töpferwaren', '', NULL, 23222),
(209814, 'deDE', 'Zerbrochene Töpferwaren', '', NULL, 23222),
(211488, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211861, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(209855, 'deDE', 'Glitschischnur', '', NULL, 23222),
(214417, 'deDE', 'Hai-pu', '', NULL, 23222),
(211860, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212969, 'deDE', 'Räucherwerkbündel', '', NULL, 23222),
(212926, 'deDE', 'Steinmonolith', '', NULL, 23222),
(212925, 'deDE', 'Steinmonolith', '', NULL, 23222),
(212288, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(209780, 'deDE', 'Meditationskerze', '', NULL, 23222),
(212924, 'deDE', 'Steinmonolith', '', NULL, 23222),
(177664, 'deDE', 'Ranziges Fleischstück', '', NULL, 23222),
(213004, 'deDE', 'Briefkasten', '', NULL, 23222),
(214421, 'deDE', 'Hai-pu', '', NULL, 23222),
(212693, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212287, 'deDE', 'Amboss', '', NULL, 23222),
(211796, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212285, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212286, 'deDE', 'Schmiede', '', NULL, 23222),
(211797, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212284, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214420, 'deDE', 'Hai-pu', '', NULL, 23222),
(214419, 'deDE', 'Hai-pu', '', NULL, 23222),
(209857, 'deDE', 'Jinyukäfig', '', NULL, 23222),
(212283, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212296, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212404, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(209903, 'deDE', 'Grüner Zweig', '', NULL, 23222);


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=13530 AND `id`=1) OR (`menu_id`=13282 AND `id`=0) OR (`menu_id`=9868 AND `id`=1) OR (`menu_id`=9868 AND `id`=0) OR (`menu_id`=13374 AND `id`=2) OR (`menu_id`=13374 AND `id`=1) OR (`menu_id`=9821 AND `id`=2) OR (`menu_id`=9821 AND `id`=1) OR (`menu_id`=14629 AND `id`=0) OR (`menu_id`=14625 AND `id`=0) OR (`menu_id`=14626 AND `id`=1) OR (`menu_id`=14626 AND `id`=0) OR (`menu_id`=14649 AND `id`=8) OR (`menu_id`=10188 AND `id`=0) OR (`menu_id`=8903 AND `id`=0) OR (`menu_id`=14432 AND `id`=0) OR (`menu_id`=14433 AND `id`=0) OR (`menu_id`=83 AND `id`=0) OR (`menu_id`=13580 AND `id`=1) OR (`menu_id`=13580 AND `id`=0) OR (`menu_id`=13105 AND `id`=0) OR (`menu_id`=14624 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(13530, 1, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13282, 0, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9868, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9868, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13374, 2, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13374, 1, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9821, 2, '', '', 'Ich würde gern meine Kampfhaustiere heilen und wiederbeleben.', '', '', '', '', '', '', '', 'Es wird eine kleine Gebühr für die medizinische Hilfe erhoben.', '', '', '', '', ''),
(9821, 1, '', '', 'Ich suche nach einem verlorenen Gefährten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14629, 0, '', '', 'Unterrichtet mich in der Verzauberkunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14625, 0, '', '', 'Unterrichtet mich in der Juwelierskunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14626, 1, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14626, 0, '', '', 'Unterrichtet mich in der Schmiedekunst.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14649, 8, '', '', 'Warteschlange für Grünstein.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10188, 0, '', '', 'Ich sehe mich nur mal um.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8903, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14432, 0, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14433, 0, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(83, 0, '', '', 'Bringt mich ins Leben zurück.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13580, 1, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13580, 0, '', '', 'Warteschlange für Ein Sturm braut sich zusammen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13105, 0, '', '', 'Was ist das hier für ein Ort?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14624, 0, '', '', 'Unterrichtet mich in Bergbau.', '', '', '', '', '', '', '', '', '', '', '', '', '');


DELETE FROM `locales_creature_text` WHERE (`entry`=54989 AND `groupid`=0 ) OR (`entry`=55470 AND `groupid`=0 ) OR (`entry`=56349 AND `groupid`=0 ) OR (`entry`=56349 AND `groupid`=1 ) OR (`entry`=56444 AND `groupid`=0 ) OR (`entry`=56444 AND `groupid`=1 ) OR (`entry`=56444 AND `groupid`=2 ) OR (`entry`=56464 AND `groupid`=0 ) OR (`entry`=56464 AND `groupid`=1 ) OR (`entry`=57313 AND `groupid`=0 ) OR (`entry`=59348 AND `groupid`=0 ) OR (`entry`=59348 AND `groupid`=1 ) OR (`entry`=62323 AND `groupid`=0 ) OR (`entry`=63742 AND `groupid`=0 ) OR (`entry`=63742 AND `groupid`=1 ) OR (`entry`=65923 AND `groupid`=0 ) OR (`entry`=66235 AND `groupid`=0 );
INSERT INTO `world`.`locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(54989, 0, 0, '', '', '$gUnverschämter kleiner:Unverschämte kleine:r; $R!', '', '', '', '', ''),
(55470, 0, 0, '', '', 'Stampft die nappeligen Nappel von weit weg in Boden!', '', '', '', '', ''),
(56349, 0, 0, '', '', 'Meiner!', '', '', '', '', ''),
(56349, 1, 0, '', '', 'Gefangen!', '', '', '', '', ''),
(56444, 0, 0, '', '', 'Am Ende herrscht nur noch die Stille.', '', '', '', '', ''),
(56444, 1, 0, '', '', 'Ein würdiges Opfer für unseren Meister.', '', '', '', '', ''),
(56444, 2, 0, '', '', 'Die Lebenden sind hier nicht willkommen!', '', '', '', '', ''),
(56464, 0, 0, '', '', 'Dankeschööön!', '', '', '', '', ''),
(56464, 1, 0, '', '', 'Grr... Komm'' nur näher, Mistviech. Ich beiß dir die Beine ab.', '', '', '', '', ''),
(57313, 0, 0, '', '', 'Willkommen in meinem Gasthaus. Wir haben warme Mahlzeiten und weiche Betten.', '', '', '', '', ''),
(59348, 0, 0, '', '', '$R, Ihr seid hier sehr willkommen. Karasshi der Draufgänger hat schon von Euren Heldentaten berichtet.', '', '', '', '', ''),
(59348, 1, 0, '', '', 'Ihr müsst $gder:die:r; $R sein, $gder:die; Karasshi den Draufgänger und unsere Brüder befreit hat! Wir schulden Euch Dank.', '', '', '', '', ''),
(62323, 0, 0, '', '', 'Wie geht''s den Nudeln heute?', '', '', '', '', ''),
(63742, 0, 0, '', '', 'Knatz gebrammt!', '', '', '', '', ''),
(63742, 1, 0, '', '', 'Werd geflotscht, Knatzel!', '', '', '', '', ''),
(65923, 0, 0, '', '', 'Ich hoffe, dass Ihr Recht habt. Ich hoffe es sehr.', '', '', '', '', ''),
(66235, 0, 0, '', '', 'Meine Kinder werden den Krieg nicht kennenlernen, dafür sorge ich!', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=64244 /*64244*/ AND `locale`='deDE') OR (`entry`=59411 /*59411*/ AND `locale`='deDE') OR (`entry`=59756 /*59756*/ AND `locale`='deDE') OR (`entry`=54854 /*54854*/ AND `locale`='deDE') OR (`entry`=54697 /*54697*/ AND `locale`='deDE') OR (`entry`=55209 /*55209*/ AND `locale`='deDE') OR (`entry`=55081 /*55081*/ AND `locale`='deDE') OR (`entry`=54702 /*54702*/ AND `locale`='deDE') OR (`entry`=60782 /*60782*/ AND `locale`='deDE') OR (`entry`=54763 /*54763*/ AND `locale`='deDE') OR (`entry`=43499 /*43499*/ AND `locale`='deDE') OR (`entry`=54703 /*54703*/ AND `locale`='deDE') OR (`entry`=64310 /*64310*/ AND `locale`='deDE') OR (`entry`=55009 /*55009*/ AND `locale`='deDE') OR (`entry`=61472 /*61472*/ AND `locale`='deDE') OR (`entry`=55016 /*55016*/ AND `locale`='deDE') OR (`entry`=54990 /*54990*/ AND `locale`='deDE') OR (`entry`=54989 /*54989*/ AND `locale`='deDE') OR (`entry`=59641 /*59641*/ AND `locale`='deDE') OR (`entry`=65216 /*65216*/ AND `locale`='deDE') OR (`entry`=59770 /*59770*/ AND `locale`='deDE') OR (`entry`=59769 /*59769*/ AND `locale`='deDE') OR (`entry`=64311 /*64311*/ AND `locale`='deDE') OR (`entry`=59747 /*59747*/ AND `locale`='deDE') OR (`entry`=59748 /*59748*/ AND `locale`='deDE') OR (`entry`=59768 /*59768*/ AND `locale`='deDE') OR (`entry`=66898 /*66898*/ AND `locale`='deDE') OR (`entry`=59899 /*59899*/ AND `locale`='deDE') OR (`entry`=61615 /*61615*/ AND `locale`='deDE') OR (`entry`=59418 /*59418*/ AND `locale`='deDE') OR (`entry`=59463 /*59463*/ AND `locale`='deDE') OR (`entry`=59417 /*59417*/ AND `locale`='deDE') OR (`entry`=59462 /*59462*/ AND `locale`='deDE') OR (`entry`=59434 /*59434*/ AND `locale`='deDE') OR (`entry`=66730 /*66730*/ AND `locale`='deDE') OR (`entry`=66711 /*66711*/ AND `locale`='deDE') OR (`entry`=66710 /*66710*/ AND `locale`='deDE') OR (`entry`=66709 /*66709*/ AND `locale`='deDE') OR (`entry`=59774 /*59774*/ AND `locale`='deDE') OR (`entry`=56788 /*56788*/ AND `locale`='deDE') OR (`entry`=67084 /*67084*/ AND `locale`='deDE') OR (`entry`=58363 /*58363*/ AND `locale`='deDE') OR (`entry`=58362 /*58362*/ AND `locale`='deDE') OR (`entry`=65711 /*65711*/ AND `locale`='deDE') OR (`entry`=57319 /*57319*/ AND `locale`='deDE') OR (`entry`=62650 /*62650*/ AND `locale`='deDE') OR (`entry`=62649 /*62649*/ AND `locale`='deDE') OR (`entry`=70169 /*70169*/ AND `locale`='deDE') OR (`entry`=62656 /*62656*/ AND `locale`='deDE') OR (`entry`=62657 /*62657*/ AND `locale`='deDE') OR (`entry`=58440 /*58440*/ AND `locale`='deDE') OR (`entry`=58168 /*58168*/ AND `locale`='deDE') OR (`entry`=57316 /*57316*/ AND `locale`='deDE') OR (`entry`=57318 /*57318*/ AND `locale`='deDE') OR (`entry`=57323 /*57323*/ AND `locale`='deDE') OR (`entry`=58694 /*58694*/ AND `locale`='deDE') OR (`entry`=57314 /*57314*/ AND `locale`='deDE') OR (`entry`=62647 /*62647*/ AND `locale`='deDE') OR (`entry`=57242 /*57242*/ AND `locale`='deDE') OR (`entry`=57325 /*57325*/ AND `locale`='deDE') OR (`entry`=58186 /*58186*/ AND `locale`='deDE') OR (`entry`=62655 /*62655*/ AND `locale`='deDE') OR (`entry`=62653 /*62653*/ AND `locale`='deDE') OR (`entry`=62644 /*62644*/ AND `locale`='deDE') OR (`entry`=62643 /*62643*/ AND `locale`='deDE') OR (`entry`=62642 /*62642*/ AND `locale`='deDE') OR (`entry`=62646 /*62646*/ AND `locale`='deDE') OR (`entry`=62645 /*62645*/ AND `locale`='deDE') OR (`entry`=62652 /*62652*/ AND `locale`='deDE') OR (`entry`=62651 /*62651*/ AND `locale`='deDE') OR (`entry`=57324 /*57324*/ AND `locale`='deDE') OR (`entry`=57445 /*57445*/ AND `locale`='deDE') OR (`entry`=57444 /*57444*/ AND `locale`='deDE') OR (`entry`=59767 /*59767*/ AND `locale`='deDE') OR (`entry`=59727 /*59727*/ AND `locale`='deDE') OR (`entry`=57313 /*57313*/ AND `locale`='deDE') OR (`entry`=66243 /*66243*/ AND `locale`='deDE') OR (`entry`=62999 /*62999*/ AND `locale`='deDE') OR (`entry`=63894 /*63894*/ AND `locale`='deDE') OR (`entry`=63893 /*63893*/ AND `locale`='deDE') OR (`entry`=57312 /*57312*/ AND `locale`='deDE') OR (`entry`=56812 /*56812*/ AND `locale`='deDE') OR (`entry`=62663 /*62663*/ AND `locale`='deDE') OR (`entry`=62662 /*62662*/ AND `locale`='deDE') OR (`entry`=62661 /*62661*/ AND `locale`='deDE') OR (`entry`=62660 /*62660*/ AND `locale`='deDE') OR (`entry`=62659 /*62659*/ AND `locale`='deDE') OR (`entry`=56811 /*56811*/ AND `locale`='deDE') OR (`entry`=57326 /*57326*/ AND `locale`='deDE') OR (`entry`=58698 /*58698*/ AND `locale`='deDE') OR (`entry`=59786 /*59786*/ AND `locale`='deDE') OR (`entry`=56032 /*56032*/ AND `locale`='deDE') OR (`entry`=68555 /*68555*/ AND `locale`='deDE') OR (`entry`=59753 /*59753*/ AND `locale`='deDE') OR (`entry`=59757 /*59757*/ AND `locale`='deDE') OR (`entry`=59754 /*59754*/ AND `locale`='deDE') OR (`entry`=59766 /*59766*/ AND `locale`='deDE') OR (`entry`=59665 /*59665*/ AND `locale`='deDE') OR (`entry`=59110 /*59110*/ AND `locale`='deDE') OR (`entry`=55565 /*55565*/ AND `locale`='deDE') OR (`entry`=59667 /*59667*/ AND `locale`='deDE') OR (`entry`=59782 /*59782*/ AND `locale`='deDE') OR (`entry`=66633 /*66633*/ AND `locale`='deDE') OR (`entry`=66803 /*66803*/ AND `locale`='deDE') OR (`entry`=66617 /*66617*/ AND `locale`='deDE') OR (`entry`=56441 /*56441*/ AND `locale`='deDE') OR (`entry`=62991 /*62991*/ AND `locale`='deDE') OR (`entry`=56444 /*56444*/ AND `locale`='deDE') OR (`entry`=59114 /*59114*/ AND `locale`='deDE') OR (`entry`=56233 /*56233*/ AND `locale`='deDE') OR (`entry`=59351 /*59351*/ AND `locale`='deDE') OR (`entry`=59356 /*59356*/ AND `locale`='deDE') OR (`entry`=56653 /*56653*/ AND `locale`='deDE') OR (`entry`=56683 /*56683*/ AND `locale`='deDE') OR (`entry`=56655 /*56655*/ AND `locale`='deDE') OR (`entry`=56654 /*56654*/ AND `locale`='deDE') OR (`entry`=56650 /*56650*/ AND `locale`='deDE') OR (`entry`=55196 /*55196*/ AND `locale`='deDE') OR (`entry`=55201 /*55201*/ AND `locale`='deDE') OR (`entry`=61605 /*61605*/ AND `locale`='deDE') OR (`entry`=61547 /*61547*/ AND `locale`='deDE') OR (`entry`=61546 /*61546*/ AND `locale`='deDE') OR (`entry`=59037 /*59037*/ AND `locale`='deDE') OR (`entry`=61558 /*61558*/ AND `locale`='deDE') OR (`entry`=61557 /*61557*/ AND `locale`='deDE') OR (`entry`=55490 /*55490*/ AND `locale`='deDE') OR (`entry`=55193 /*55193*/ AND `locale`='deDE') OR (`entry`=61562 /*61562*/ AND `locale`='deDE') OR (`entry`=61602 /*61602*/ AND `locale`='deDE') OR (`entry`=54960 /*54960*/ AND `locale`='deDE') OR (`entry`=59159 /*59159*/ AND `locale`='deDE') OR (`entry`=64475 /*64475*/ AND `locale`='deDE') OR (`entry`=59620 /*59620*/ AND `locale`='deDE') OR (`entry`=56737 /*56737*/ AND `locale`='deDE') OR (`entry`=59058 /*59058*/ AND `locale`='deDE') OR (`entry`=59060 /*59060*/ AND `locale`='deDE') OR (`entry`=54557 /*54557*/ AND `locale`='deDE') OR (`entry`=56672 /*56672*/ AND `locale`='deDE') OR (`entry`=56222 /*56222*/ AND `locale`='deDE') OR (`entry`=56227 /*56227*/ AND `locale`='deDE') OR (`entry`=55333 /*55333*/ AND `locale`='deDE') OR (`entry`=55283 /*55283*/ AND `locale`='deDE') OR (`entry`=55282 /*55282*/ AND `locale`='deDE') OR (`entry`=55284 /*55284*/ AND `locale`='deDE') OR (`entry`=61604 /*61604*/ AND `locale`='deDE') OR (`entry`=61747 /*61747*/ AND `locale`='deDE') OR (`entry`=59059 /*59059*/ AND `locale`='deDE') OR (`entry`=56687 /*56687*/ AND `locale`='deDE') OR (`entry`=61598 /*61598*/ AND `locale`='deDE') OR (`entry`=61596 /*61596*/ AND `locale`='deDE') OR (`entry`=59169 /*59169*/ AND `locale`='deDE') OR (`entry`=61599 /*61599*/ AND `locale`='deDE') OR (`entry`=56689 /*56689*/ AND `locale`='deDE') OR (`entry`=55370 /*55370*/ AND `locale`='deDE') OR (`entry`=56693 /*56693*/ AND `locale`='deDE') OR (`entry`=56690 /*56690*/ AND `locale`='deDE') OR (`entry`=61614 /*61614*/ AND `locale`='deDE') OR (`entry`=59084 /*59084*/ AND `locale`='deDE') OR (`entry`=61611 /*61611*/ AND `locale`='deDE') OR (`entry`=56591 /*56591*/ AND `locale`='deDE') OR (`entry`=54959 /*54959*/ AND `locale`='deDE') OR (`entry`=59348 /*59348*/ AND `locale`='deDE') OR (`entry`=56592 /*56592*/ AND `locale`='deDE') OR (`entry`=56585 /*56585*/ AND `locale`='deDE') OR (`entry`=62998 /*62998*/ AND `locale`='deDE') OR (`entry`=60971 /*60971*/ AND `locale`='deDE') OR (`entry`=55110 /*55110*/ AND `locale`='deDE') OR (`entry`=56701 /*56701*/ AND `locale`='deDE') OR (`entry`=55195 /*55195*/ AND `locale`='deDE') OR (`entry`=59085 /*59085*/ AND `locale`='deDE') OR (`entry`=59115 /*59115*/ AND `locale`='deDE') OR (`entry`=59113 /*59113*/ AND `locale`='deDE') OR (`entry`=60798 /*60798*/ AND `locale`='deDE') OR (`entry`=63919 /*63919*/ AND `locale`='deDE') OR (`entry`=64287 /*64287*/ AND `locale`='deDE') OR (`entry`=64261 /*64261*/ AND `locale`='deDE') OR (`entry`=63137 /*63137*/ AND `locale`='deDE') OR (`entry`=64295 /*64295*/ AND `locale`='deDE') OR (`entry`=55376 /*55376*/ AND `locale`='deDE') OR (`entry`=56304 /*56304*/ AND `locale`='deDE') OR (`entry`=64774 /*64774*/ AND `locale`='deDE') OR (`entry`=54558 /*54558*/ AND `locale`='deDE') OR (`entry`=59742 /*59742*/ AND `locale`='deDE') OR (`entry`=63716 /*63716*/ AND `locale`='deDE') OR (`entry`=63003 /*63003*/ AND `locale`='deDE') OR (`entry`=66276 /*66276*/ AND `locale`='deDE') OR (`entry`=54616 /*54616*/ AND `locale`='deDE') OR (`entry`=66274 /*66274*/ AND `locale`='deDE') OR (`entry`=66275 /*66275*/ AND `locale`='deDE') OR (`entry`=66281 /*66281*/ AND `locale`='deDE') OR (`entry`=66270 /*66270*/ AND `locale`='deDE') OR (`entry`=66271 /*66271*/ AND `locale`='deDE') OR (`entry`=66273 /*66273*/ AND `locale`='deDE') OR (`entry`=12922 /*12922*/ AND `locale`='deDE') OR (`entry`=59744 /*59744*/ AND `locale`='deDE') OR (`entry`=59743 /*59743*/ AND `locale`='deDE') OR (`entry`=66092 /*66092*/ AND `locale`='deDE') OR (`entry`=65910 /*65910*/ AND `locale`='deDE') OR (`entry`=66094 /*66094*/ AND `locale`='deDE') OR (`entry`=66093 /*66093*/ AND `locale`='deDE') OR (`entry`=66032 /*66032*/ AND `locale`='deDE') OR (`entry`=66034 /*66034*/ AND `locale`='deDE') OR (`entry`=66030 /*66030*/ AND `locale`='deDE') OR (`entry`=66035 /*66035*/ AND `locale`='deDE') OR (`entry`=66031 /*66031*/ AND `locale`='deDE') OR (`entry`=54615 /*54615*/ AND `locale`='deDE') OR (`entry`=54559 /*54559*/ AND `locale`='deDE') OR (`entry`=55175 /*55175*/ AND `locale`='deDE') OR (`entry`=55174 /*55174*/ AND `locale`='deDE') OR (`entry`=54623 /*54623*/ AND `locale`='deDE') OR (`entry`=55155 /*55155*/ AND `locale`='deDE') OR (`entry`=55176 /*55176*/ AND `locale`='deDE') OR (`entry`=55168 /*55168*/ AND `locale`='deDE') OR (`entry`=55173 /*55173*/ AND `locale`='deDE') OR (`entry`=55167 /*55167*/ AND `locale`='deDE') OR (`entry`=55177 /*55177*/ AND `locale`='deDE') OR (`entry`=66102 /*66102*/ AND `locale`='deDE') OR (`entry`=66148 /*66148*/ AND `locale`='deDE') OR (`entry`=66106 /*66106*/ AND `locale`='deDE') OR (`entry`=66153 /*66153*/ AND `locale`='deDE') OR (`entry`=57119 /*57119*/ AND `locale`='deDE') OR (`entry`=63152 /*63152*/ AND `locale`='deDE') OR (`entry`=67156 /*67156*/ AND `locale`='deDE') OR (`entry`=62994 /*62994*/ AND `locale`='deDE') OR (`entry`=62997 /*62997*/ AND `locale`='deDE') OR (`entry`=66750 /*66750*/ AND `locale`='deDE') OR (`entry`=66747 /*66747*/ AND `locale`='deDE') OR (`entry`=61446 /*61446*/ AND `locale`='deDE') OR (`entry`=66942 /*66942*/ AND `locale`='deDE') OR (`entry`=63001 /*63001*/ AND `locale`='deDE') OR (`entry`=55336 /*55336*/ AND `locale`='deDE') OR (`entry`=67011 /*67011*/ AND `locale`='deDE') OR (`entry`=66441 /*66441*/ AND `locale`='deDE') OR (`entry`=66460 /*66460*/ AND `locale`='deDE') OR (`entry`=66280 /*66280*/ AND `locale`='deDE') OR (`entry`=66300 /*66300*/ AND `locale`='deDE') OR (`entry`=66371 /*66371*/ AND `locale`='deDE') OR (`entry`=66688 /*66688*/ AND `locale`='deDE') OR (`entry`=66424 /*66424*/ AND `locale`='deDE') OR (`entry`=66423 /*66423*/ AND `locale`='deDE') OR (`entry`=66290 /*66290*/ AND `locale`='deDE') OR (`entry`=66457 /*66457*/ AND `locale`='deDE') OR (`entry`=59668 /*59668*/ AND `locale`='deDE') OR (`entry`=66279 /*66279*/ AND `locale`='deDE') OR (`entry`=66272 /*66272*/ AND `locale`='deDE') OR (`entry`=66267 /*66267*/ AND `locale`='deDE') OR (`entry`=66268 /*66268*/ AND `locale`='deDE') OR (`entry`=66269 /*66269*/ AND `locale`='deDE') OR (`entry`=66366 /*66366*/ AND `locale`='deDE') OR (`entry`=63715 /*63715*/ AND `locale`='deDE') OR (`entry`=63358 /*63358*/ AND `locale`='deDE') OR (`entry`=63057 /*63057*/ AND `locale`='deDE') OR (`entry`=65612 /*65612*/ AND `locale`='deDE') OR (`entry`=67085 /*67085*/ AND `locale`='deDE') OR (`entry`=63538 /*63538*/ AND `locale`='deDE') OR (`entry`=63540 /*63540*/ AND `locale`='deDE') OR (`entry`=63537 /*63537*/ AND `locale`='deDE') OR (`entry`=63536 /*63536*/ AND `locale`='deDE') OR (`entry`=58217 /*58217*/ AND `locale`='deDE') OR (`entry`=58212 /*58212*/ AND `locale`='deDE') OR (`entry`=63005 /*63005*/ AND `locale`='deDE') OR (`entry`=58675 /*58675*/ AND `locale`='deDE') OR (`entry`=63006 /*63006*/ AND `locale`='deDE') OR (`entry`=58230 /*58230*/ AND `locale`='deDE') OR (`entry`=65635 /*65635*/ AND `locale`='deDE') OR (`entry`=58275 /*58275*/ AND `locale`='deDE') OR (`entry`=58231 /*58231*/ AND `locale`='deDE') OR (`entry`=58225 /*58225*/ AND `locale`='deDE') OR (`entry`=58220 /*58220*/ AND `locale`='deDE') OR (`entry`=58213 /*58213*/ AND `locale`='deDE') OR (`entry`=58244 /*58244*/ AND `locale`='deDE') OR (`entry`=58416 /*58416*/ AND `locale`='deDE') OR (`entry`=58243 /*58243*/ AND `locale`='deDE') OR (`entry`=58218 /*58218*/ AND `locale`='deDE') OR (`entry`=63532 /*63532*/ AND `locale`='deDE') OR (`entry`=58219 /*58219*/ AND `locale`='deDE') OR (`entry`=58214 /*58214*/ AND `locale`='deDE') OR (`entry`=58236 /*58236*/ AND `locale`='deDE') OR (`entry`=62382 /*62382*/ AND `locale`='deDE') OR (`entry`=61640 /*61640*/ AND `locale`='deDE') OR (`entry`=56065 /*56065*/ AND `locale`='deDE') OR (`entry`=56064 /*56064*/ AND `locale`='deDE') OR (`entry`=25305 /*25305*/ AND `locale`='deDE') OR (`entry`=55233 /*55233*/ AND `locale`='deDE') OR (`entry`=55809 /*55809*/ AND `locale`='deDE') OR (`entry`=56775 /*56775*/ AND `locale`='deDE') OR (`entry`=56774 /*56774*/ AND `locale`='deDE') OR (`entry`=56776 /*56776*/ AND `locale`='deDE') OR (`entry`=56778 /*56778*/ AND `locale`='deDE') OR (`entry`=56707 /*56707*/ AND `locale`='deDE') OR (`entry`=65127 /*65127*/ AND `locale`='deDE') OR (`entry`=61861 /*61861*/ AND `locale`='deDE') OR (`entry`=56777 /*56777*/ AND `locale`='deDE') OR (`entry`=56769 /*56769*/ AND `locale`='deDE') OR (`entry`=56062 /*56062*/ AND `locale`='deDE') OR (`entry`=66293 /*66293*/ AND `locale`='deDE') OR (`entry`=66241 /*66241*/ AND `locale`='deDE') OR (`entry`=66306 /*66306*/ AND `locale`='deDE') OR (`entry`=56705 /*56705*/ AND `locale`='deDE') OR (`entry`=56708 /*56708*/ AND `locale`='deDE') OR (`entry`=55788 /*55788*/ AND `locale`='deDE') OR (`entry`=59173 /*59173*/ AND `locale`='deDE') OR (`entry`=59186 /*59186*/ AND `locale`='deDE') OR (`entry`=56348 /*56348*/ AND `locale`='deDE') OR (`entry`=59160 /*59160*/ AND `locale`='deDE') OR (`entry`=56659 /*56659*/ AND `locale`='deDE') OR (`entry`=55413 /*55413*/ AND `locale`='deDE') OR (`entry`=59383 /*59383*/ AND `locale`='deDE') OR (`entry`=59105 /*59105*/ AND `locale`='deDE') OR (`entry`=64522 /*64522*/ AND `locale`='deDE') OR (`entry`=56768 /*56768*/ AND `locale`='deDE') OR (`entry`=63899 /*63899*/ AND `locale`='deDE') OR (`entry`=63900 /*63900*/ AND `locale`='deDE') OR (`entry`=56771 /*56771*/ AND `locale`='deDE') OR (`entry`=54998 /*54998*/ AND `locale`='deDE') OR (`entry`=56815 /*56815*/ AND `locale`='deDE') OR (`entry`=62325 /*62325*/ AND `locale`='deDE') OR (`entry`=62328 /*62328*/ AND `locale`='deDE') OR (`entry`=62323 /*62323*/ AND `locale`='deDE') OR (`entry`=59112 /*59112*/ AND `locale`='deDE') OR (`entry`=65114 /*65114*/ AND `locale`='deDE') OR (`entry`=62326 /*62326*/ AND `locale`='deDE') OR (`entry`=62322 /*62322*/ AND `locale`='deDE') OR (`entry`=62321 /*62321*/ AND `locale`='deDE') OR (`entry`=65098 /*65098*/ AND `locale`='deDE') OR (`entry`=62334 /*62334*/ AND `locale`='deDE') OR (`entry`=62516 /*62516*/ AND `locale`='deDE') OR (`entry`=62327 /*62327*/ AND `locale`='deDE') OR (`entry`=59735 /*59735*/ AND `locale`='deDE') OR (`entry`=56346 /*56346*/ AND `locale`='deDE') OR (`entry`=56467 /*56467*/ AND `locale`='deDE') OR (`entry`=56345 /*56345*/ AND `locale`='deDE') OR (`entry`=65022 /*65022*/ AND `locale`='deDE') OR (`entry`=64999 /*64999*/ AND `locale`='deDE') OR (`entry`=59883 /*59883*/ AND `locale`='deDE') OR (`entry`=64994 /*64994*/ AND `locale`='deDE') OR (`entry`=64325 /*64325*/ AND `locale`='deDE') OR (`entry`=64365 /*64365*/ AND `locale`='deDE') OR (`entry`=64343 /*64343*/ AND `locale`='deDE') OR (`entry`=64342 /*64342*/ AND `locale`='deDE') OR (`entry`=64345 /*64345*/ AND `locale`='deDE') OR (`entry`=64333 /*64333*/ AND `locale`='deDE') OR (`entry`=64332 /*64332*/ AND `locale`='deDE') OR (`entry`=64331 /*64331*/ AND `locale`='deDE') OR (`entry`=59733 /*59733*/ AND `locale`='deDE') OR (`entry`=62868 /*62868*/ AND `locale`='deDE') OR (`entry`=65624 /*65624*/ AND `locale`='deDE') OR (`entry`=65628 /*65628*/ AND `locale`='deDE') OR (`entry`=65623 /*65623*/ AND `locale`='deDE') OR (`entry`=65621 /*65621*/ AND `locale`='deDE') OR (`entry`=65641 /*65641*/ AND `locale`='deDE') OR (`entry`=65614 /*65614*/ AND `locale`='deDE') OR (`entry`=65663 /*65663*/ AND `locale`='deDE') OR (`entry`=65666 /*65666*/ AND `locale`='deDE') OR (`entry`=65653 /*65653*/ AND `locale`='deDE') OR (`entry`=65622 /*65622*/ AND `locale`='deDE') OR (`entry`=51984 /*51984*/ AND `locale`='deDE') OR (`entry`=65003 /*65003*/ AND `locale`='deDE') OR (`entry`=65658 /*65658*/ AND `locale`='deDE') OR (`entry`=65634 /*65634*/ AND `locale`='deDE') OR (`entry`=65667 /*65667*/ AND `locale`='deDE') OR (`entry`=59104 /*59104*/ AND `locale`='deDE') OR (`entry`=58528 /*58528*/ AND `locale`='deDE') OR (`entry`=58527 /*58527*/ AND `locale`='deDE') OR (`entry`=58526 /*58526*/ AND `locale`='deDE') OR (`entry`=58420 /*58420*/ AND `locale`='deDE') OR (`entry`=58499 /*58499*/ AND `locale`='deDE') OR (`entry`=58519 /*58519*/ AND `locale`='deDE') OR (`entry`=58516 /*58516*/ AND `locale`='deDE') OR (`entry`=58506 /*58506*/ AND `locale`='deDE') OR (`entry`=58510 /*58510*/ AND `locale`='deDE') OR (`entry`=58520 /*58520*/ AND `locale`='deDE') OR (`entry`=58511 /*58511*/ AND `locale`='deDE') OR (`entry`=58518 /*58518*/ AND `locale`='deDE') OR (`entry`=58509 /*58509*/ AND `locale`='deDE') OR (`entry`=61407 /*61407*/ AND `locale`='deDE') OR (`entry`=58517 /*58517*/ AND `locale`='deDE') OR (`entry`=58508 /*58508*/ AND `locale`='deDE') OR (`entry`=59732 /*59732*/ AND `locale`='deDE') OR (`entry`=65733 /*65733*/ AND `locale`='deDE') OR (`entry`=65732 /*65732*/ AND `locale`='deDE') OR (`entry`=65731 /*65731*/ AND `locale`='deDE') OR (`entry`=58531 /*58531*/ AND `locale`='deDE') OR (`entry`=58414 /*58414*/ AND `locale`='deDE') OR (`entry`=58228 /*58228*/ AND `locale`='deDE') OR (`entry`=58564 /*58564*/ AND `locale`='deDE') OR (`entry`=56798 /*56798*/ AND `locale`='deDE') OR (`entry`=58413 /*58413*/ AND `locale`='deDE') OR (`entry`=67199 /*67199*/ AND `locale`='deDE') OR (`entry`=61406 /*61406*/ AND `locale`='deDE') OR (`entry`=58692 /*58692*/ AND `locale`='deDE') OR (`entry`=61410 /*61410*/ AND `locale`='deDE') OR (`entry`=56063 /*56063*/ AND `locale`='deDE') OR (`entry`=63002 /*63002*/ AND `locale`='deDE') OR (`entry`=58696 /*58696*/ AND `locale`='deDE') OR (`entry`=55681 /*55681*/ AND `locale`='deDE') OR (`entry`=56790 /*56790*/ AND `locale`='deDE') OR (`entry`=65215 /*65215*/ AND `locale`='deDE') OR (`entry`=56829 /*56829*/ AND `locale`='deDE') OR (`entry`=56201 /*56201*/ AND `locale`='deDE') OR (`entry`=56070 /*56070*/ AND `locale`='deDE') OR (`entry`=56931 /*56931*/ AND `locale`='deDE') OR (`entry`=55267 /*55267*/ AND `locale`='deDE') OR (`entry`=55236 /*55236*/ AND `locale`='deDE') OR (`entry`=60476 /*60476*/ AND `locale`='deDE') OR (`entry`=63810 /*63810*/ AND `locale`='deDE') OR (`entry`=64694 /*64694*/ AND `locale`='deDE') OR (`entry`=64697 /*64697*/ AND `locale`='deDE') OR (`entry`=64696 /*64696*/ AND `locale`='deDE') OR (`entry`=64695 /*64695*/ AND `locale`='deDE') OR (`entry`=64984 /*64984*/ AND `locale`='deDE') OR (`entry`=64398 /*64398*/ AND `locale`='deDE') OR (`entry`=64394 /*64394*/ AND `locale`='deDE') OR (`entry`=64384 /*64384*/ AND `locale`='deDE') OR (`entry`=64381 /*64381*/ AND `locale`='deDE') OR (`entry`=59781 /*59781*/ AND `locale`='deDE') OR (`entry`=59779 /*59779*/ AND `locale`='deDE') OR (`entry`=64382 /*64382*/ AND `locale`='deDE') OR (`entry`=59569 /*59569*/ AND `locale`='deDE') OR (`entry`=67097 /*67097*/ AND `locale`='deDE') OR (`entry`=67096 /*67096*/ AND `locale`='deDE') OR (`entry`=54914 /*54914*/ AND `locale`='deDE') OR (`entry`=54913 /*54913*/ AND `locale`='deDE') OR (`entry`=54925 /*54925*/ AND `locale`='deDE') OR (`entry`=54924 /*54924*/ AND `locale`='deDE') OR (`entry`=54919 /*54919*/ AND `locale`='deDE') OR (`entry`=59736 /*59736*/ AND `locale`='deDE') OR (`entry`=54922 /*54922*/ AND `locale`='deDE') OR (`entry`=55207 /*55207*/ AND `locale`='deDE') OR (`entry`=55030 /*55030*/ AND `locale`='deDE') OR (`entry`=54926 /*54926*/ AND `locale`='deDE') OR (`entry`=54944 /*54944*/ AND `locale`='deDE') OR (`entry`=55029 /*55029*/ AND `locale`='deDE') OR (`entry`=55028 /*55028*/ AND `locale`='deDE') OR (`entry`=62867 /*62867*/ AND `locale`='deDE') OR (`entry`=54982 /*54982*/ AND `locale`='deDE') OR (`entry`=54980 /*54980*/ AND `locale`='deDE') OR (`entry`=54915 /*54915*/ AND `locale`='deDE') OR (`entry`=54981 /*54981*/ AND `locale`='deDE') OR (`entry`=55094 /*55094*/ AND `locale`='deDE') OR (`entry`=54918 /*54918*/ AND `locale`='deDE') OR (`entry`=70264 /*70264*/ AND `locale`='deDE') OR (`entry`=70267 /*70267*/ AND `locale`='deDE') OR (`entry`=70266 /*70266*/ AND `locale`='deDE') OR (`entry`=70265 /*70265*/ AND `locale`='deDE') OR (`entry`=55184 /*55184*/ AND `locale`='deDE') OR (`entry`=55183 /*55183*/ AND `locale`='deDE') OR (`entry`=55199 /*55199*/ AND `locale`='deDE') OR (`entry`=55198 /*55198*/ AND `locale`='deDE') OR (`entry`=54917 /*54917*/ AND `locale`='deDE') OR (`entry`=56401 /*56401*/ AND `locale`='deDE') OR (`entry`=1860 /*1860*/ AND `locale`='deDE') OR (`entry`=54930 /*54930*/ AND `locale`='deDE') OR (`entry`=59103 /*59103*/ AND `locale`='deDE') OR (`entry`=59102 /*59102*/ AND `locale`='deDE') OR (`entry`=61586 /*61586*/ AND `locale`='deDE') OR (`entry`=56563 /*56563*/ AND `locale`='deDE') OR (`entry`=65092 /*65092*/ AND `locale`='deDE') OR (`entry`=61622 /*61622*/ AND `locale`='deDE') OR (`entry`=56464 /*56464*/ AND `locale`='deDE') OR (`entry`=56404 /*56404*/ AND `locale`='deDE') OR (`entry`=56349 /*56349*/ AND `locale`='deDE') OR (`entry`=56543 /*56543*/ AND `locale`='deDE') OR (`entry`=3127 /*3127*/ AND `locale`='deDE') OR (`entry`=56508 /*56508*/ AND `locale`='deDE') OR (`entry`=56510 /*56510*/ AND `locale`='deDE') OR (`entry`=69946 /*69946*/ AND `locale`='deDE') OR (`entry`=59772 /*59772*/ AND `locale`='deDE') OR (`entry`=61700 /*61700*/ AND `locale`='deDE') OR (`entry`=64359 /*64359*/ AND `locale`='deDE') OR (`entry`=64366 /*64366*/ AND `locale`='deDE') OR (`entry`=64326 /*64326*/ AND `locale`='deDE') OR (`entry`=64324 /*64324*/ AND `locale`='deDE') OR (`entry`=64356 /*64356*/ AND `locale`='deDE') OR (`entry`=59788 /*59788*/ AND `locale`='deDE') OR (`entry`=55438 /*55438*/ AND `locale`='deDE') OR (`entry`=55288 /*55288*/ AND `locale`='deDE') OR (`entry`=55279 /*55279*/ AND `locale`='deDE') OR (`entry`=58065 /*58065*/ AND `locale`='deDE') OR (`entry`=55290 /*55290*/ AND `locale`='deDE') OR (`entry`=55455 /*55455*/ AND `locale`='deDE') OR (`entry`=55489 /*55489*/ AND `locale`='deDE') OR (`entry`=59492 /*59492*/ AND `locale`='deDE') OR (`entry`=56014 /*56014*/ AND `locale`='deDE') OR (`entry`=65794 /*65794*/ AND `locale`='deDE') OR (`entry`=56181 /*56181*/ AND `locale`='deDE') OR (`entry`=65743 /*65743*/ AND `locale`='deDE') OR (`entry`=55291 /*55291*/ AND `locale`='deDE') OR (`entry`=61120 /*61120*/ AND `locale`='deDE') OR (`entry`=61125 /*61125*/ AND `locale`='deDE') OR (`entry`=65779 /*65779*/ AND `locale`='deDE') OR (`entry`=55086 /*55086*/ AND `locale`='deDE') OR (`entry`=55092 /*55092*/ AND `locale`='deDE') OR (`entry`=54987 /*54987*/ AND `locale`='deDE') OR (`entry`=55238 /*55238*/ AND `locale`='deDE') OR (`entry`=54988 /*54988*/ AND `locale`='deDE') OR (`entry`=66840 /*66840*/ AND `locale`='deDE') OR (`entry`=56199 /*56199*/ AND `locale`='deDE') OR (`entry`=56198 /*56198*/ AND `locale`='deDE') OR (`entry`=66742 /*66742*/ AND `locale`='deDE') OR (`entry`=64868 /*64868*/ AND `locale`='deDE') OR (`entry`=66915 /*66915*/ AND `locale`='deDE') OR (`entry`=62167 /*62167*/ AND `locale`='deDE') OR (`entry`=66554 /*66554*/ AND `locale`='deDE') OR (`entry`=66648 /*66648*/ AND `locale`='deDE') OR (`entry`=66651 /*66651*/ AND `locale`='deDE') OR (`entry`=66654 /*66654*/ AND `locale`='deDE') OR (`entry`=66647 /*66647*/ AND `locale`='deDE') OR (`entry`=66336 /*66336*/ AND `locale`='deDE') OR (`entry`=66426 /*66426*/ AND `locale`='deDE') OR (`entry`=66425 /*66425*/ AND `locale`='deDE') OR (`entry`=66650 /*66650*/ AND `locale`='deDE') OR (`entry`=66649 /*66649*/ AND `locale`='deDE') OR (`entry`=66948 /*66948*/ AND `locale`='deDE') OR (`entry`=66928 /*66928*/ AND `locale`='deDE') OR (`entry`=58811 /*58811*/ AND `locale`='deDE') OR (`entry`=66857 /*66857*/ AND `locale`='deDE') OR (`entry`=66659 /*66659*/ AND `locale`='deDE') OR (`entry`=66477 /*66477*/ AND `locale`='deDE') OR (`entry`=66217 /*66217*/ AND `locale`='deDE') OR (`entry`=66307 /*66307*/ AND `locale`='deDE') OR (`entry`=66214 /*66214*/ AND `locale`='deDE') OR (`entry`=29238 /*29238*/ AND `locale`='deDE') OR (`entry`=66218 /*66218*/ AND `locale`='deDE') OR (`entry`=66665 /*66665*/ AND `locale`='deDE') OR (`entry`=66265 /*66265*/ AND `locale`='deDE') OR (`entry`=66237 /*66237*/ AND `locale`='deDE') OR (`entry`=66235 /*66235*/ AND `locale`='deDE') OR (`entry`=66230 /*66230*/ AND `locale`='deDE') OR (`entry`=66228 /*66228*/ AND `locale`='deDE') OR (`entry`=66223 /*66223*/ AND `locale`='deDE') OR (`entry`=66220 /*66220*/ AND `locale`='deDE') OR (`entry`=66236 /*66236*/ AND `locale`='deDE') OR (`entry`=66238 /*66238*/ AND `locale`='deDE') OR (`entry`=66219 /*66219*/ AND `locale`='deDE') OR (`entry`=67150 /*67150*/ AND `locale`='deDE') OR (`entry`=66430 /*66430*/ AND `locale`='deDE') OR (`entry`=66979 /*66979*/ AND `locale`='deDE') OR (`entry`=66347 /*66347*/ AND `locale`='deDE') OR (`entry`=66234 /*66234*/ AND `locale`='deDE') OR (`entry`=66233 /*66233*/ AND `locale`='deDE') OR (`entry`=66225 /*66225*/ AND `locale`='deDE') OR (`entry`=66232 /*66232*/ AND `locale`='deDE') OR (`entry`=66346 /*66346*/ AND `locale`='deDE') OR (`entry`=66981 /*66981*/ AND `locale`='deDE') OR (`entry`=66980 /*66980*/ AND `locale`='deDE') OR (`entry`=66222 /*66222*/ AND `locale`='deDE') OR (`entry`=66239 /*66239*/ AND `locale`='deDE') OR (`entry`=66227 /*66227*/ AND `locale`='deDE') OR (`entry`=66231 /*66231*/ AND `locale`='deDE') OR (`entry`=66668 /*66668*/ AND `locale`='deDE') OR (`entry`=55509 /*55509*/ AND `locale`='deDE') OR (`entry`=62545 /*62545*/ AND `locale`='deDE') OR (`entry`=66916 /*66916*/ AND `locale`='deDE') OR (`entry`=67016 /*67016*/ AND `locale`='deDE') OR (`entry`=66917 /*66917*/ AND `locale`='deDE') OR (`entry`=66911 /*66911*/ AND `locale`='deDE') OR (`entry`=61535 /*61535*/ AND `locale`='deDE') OR (`entry`=55463 /*55463*/ AND `locale`='deDE') OR (`entry`=55464 /*55464*/ AND `locale`='deDE') OR (`entry`=55379 /*55379*/ AND `locale`='deDE') OR (`entry`=67017 /*67017*/ AND `locale`='deDE') OR (`entry`=61507 /*61507*/ AND `locale`='deDE') OR (`entry`=55403 /*55403*/ AND `locale`='deDE') OR (`entry`=55404 /*55404*/ AND `locale`='deDE') OR (`entry`=63058 /*63058*/ AND `locale`='deDE') OR (`entry`=67018 /*67018*/ AND `locale`='deDE') OR (`entry`=67019 /*67019*/ AND `locale`='deDE') OR (`entry`=55383 /*55383*/ AND `locale`='deDE') OR (`entry`=55470 /*55470*/ AND `locale`='deDE') OR (`entry`=55388 /*55388*/ AND `locale`='deDE') OR (`entry`=55392 /*55392*/ AND `locale`='deDE') OR (`entry`=29888 /*29888*/ AND `locale`='deDE') OR (`entry`=63467 /*63467*/ AND `locale`='deDE') OR (`entry`=63420 /*63420*/ AND `locale`='deDE') OR (`entry`=62930 /*62930*/ AND `locale`='deDE') OR (`entry`=58945 /*58945*/ AND `locale`='deDE') OR (`entry`=63326 /*63326*/ AND `locale`='deDE') OR (`entry`=63323 /*63323*/ AND `locale`='deDE') OR (`entry`=63321 /*63321*/ AND `locale`='deDE') OR (`entry`=63320 /*63320*/ AND `locale`='deDE') OR (`entry`=63324 /*63324*/ AND `locale`='deDE') OR (`entry`=63322 /*63322*/ AND `locale`='deDE') OR (`entry`=62536 /*62536*/ AND `locale`='deDE') OR (`entry`=62537 /*62537*/ AND `locale`='deDE') OR (`entry`=56272 /*56272*/ AND `locale`='deDE') OR (`entry`=63059 /*63059*/ AND `locale`='deDE') OR (`entry`=56183 /*56183*/ AND `locale`='deDE') OR (`entry`=56180 /*56180*/ AND `locale`='deDE') OR (`entry`=58979 /*58979*/ AND `locale`='deDE') OR (`entry`=59787 /*59787*/ AND `locale`='deDE') OR (`entry`=56242 /*56242*/ AND `locale`='deDE') OR (`entry`=56240 /*56240*/ AND `locale`='deDE') OR (`entry`=63756 /*63756*/ AND `locale`='deDE') OR (`entry`=68027 /*68027*/ AND `locale`='deDE') OR (`entry`=68026 /*68026*/ AND `locale`='deDE') OR (`entry`=55555 /*55555*/ AND `locale`='deDE') OR (`entry`=64523 /*64523*/ AND `locale`='deDE') OR (`entry`=28960 /*28960*/ AND `locale`='deDE') OR (`entry`=59796 /*59796*/ AND `locale`='deDE') OR (`entry`=63782 /*63782*/ AND `locale`='deDE') OR (`entry`=63742 /*63742*/ AND `locale`='deDE') OR (`entry`=56283 /*56283*/ AND `locale`='deDE') OR (`entry`=66717 /*66717*/ AND `locale`='deDE') OR (`entry`=56289 /*56289*/ AND `locale`='deDE') OR (`entry`=63809 /*63809*/ AND `locale`='deDE') OR (`entry`=63768 /*63768*/ AND `locale`='deDE') OR (`entry`=62975 /*62975*/ AND `locale`='deDE') OR (`entry`=25721 /*25721*/ AND `locale`='deDE') OR (`entry`=50586 /*50586*/ AND `locale`='deDE') OR (`entry`=67149 /*67149*/ AND `locale`='deDE') OR (`entry`=56286 /*56286*/ AND `locale`='deDE') OR (`entry`=59713 /*59713*/ AND `locale`='deDE') OR (`entry`=56315 /*56315*/ AND `locale`='deDE') OR (`entry`=59720 /*59720*/ AND `locale`='deDE') OR (`entry`=59730 /*59730*/ AND `locale`='deDE') OR (`entry`=56583 /*56583*/ AND `locale`='deDE') OR (`entry`=56358 /*56358*/ AND `locale`='deDE') OR (`entry`=56309 /*56309*/ AND `locale`='deDE') OR (`entry`=58996 /*58996*/ AND `locale`='deDE') OR (`entry`=56264 /*56264*/ AND `locale`='deDE') OR (`entry`=56447 /*56447*/ AND `locale`='deDE') OR (`entry`=56509 /*56509*/ AND `locale`='deDE') OR (`entry`=59015 /*59015*/ AND `locale`='deDE') OR (`entry`=56396 /*56396*/ AND `locale`='deDE') OR (`entry`=58943 /*58943*/ AND `locale`='deDE') OR (`entry`=63764 /*63764*/ AND `locale`='deDE') OR (`entry`=58895 /*58895*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(64244, 'deDE', 'Mishi', NULL, 'Lehrensucher Chos Begleiter', NULL, 23222), -- 64244
(59411, 'deDE', 'Lehrensucher Cho', NULL, NULL, NULL, 23222), -- 59411
(59756, 'deDE', 'Chibee', NULL, NULL, NULL, 23222), -- 59756
(54854, 'deDE', 'Sanfte Mutter Hanae', NULL, NULL, NULL, 23222), -- 54854
(54697, 'deDE', 'Shao der Widerspenstige', NULL, NULL, NULL, 23222), -- 54697
(55209, 'deDE', 'Traumatisierter Bauer von Nektarhauch', NULL, NULL, NULL, 23222), -- 55209
(55081, 'deDE', 'Verwundeter Bauer von Nektarhauch', 'Verwundete Bäuerin von Nektarhauch', NULL, NULL, 23222), -- 55081
(54702, 'deDE', 'Gormaliplünderer', NULL, NULL, NULL, 23222), -- 54702
(60782, 'deDE', 'Verbrennen', NULL, NULL, NULL, 23222), -- 60782
(54763, 'deDE', 'Bauer von Nektarhauch', 'Bäuerin von Nektarhauch', NULL, NULL, 23222), -- 54763
(43499, 'deDE', 'Weihe', NULL, NULL, 'Interact', 23222), -- 43499
(54703, 'deDE', 'Gormaliverbrenner', NULL, NULL, NULL, 23222), -- 54703
(64310, 'deDE', 'Himmelstänzer Ji', NULL, 'Flugmeister', NULL, 23222), -- 64310
(55009, 'deDE', 'Shao der Widerspenstige', NULL, NULL, NULL, 23222), -- 55009
(61472, 'deDE', 'Gefesselter Bauer von Nektarhauch', 'Gefesselte Bäuerin von Nektarhauch', NULL, NULL, 23222), -- 61472
(55016, 'deDE', 'Unterwerfer Gormal', NULL, NULL, NULL, 23222), -- 55016
(54990, 'deDE', 'Gefesselter Bauer von Nektarhauch', 'Gefesselte Bäuerin von Nektarhauch', NULL, 'questinteract', 23222), -- 54990
(54989, 'deDE', 'Gormalisklavenhändler', NULL, NULL, NULL, 23222), -- 54989
(59641, 'deDE', 'Monströser Ebenenfalke', NULL, NULL, NULL, 23222), -- 59641
(65216, 'deDE', 'Schreinfliege', NULL, NULL, 'wildpetcapturable', 23222), -- 65216
(59770, 'deDE', 'Schreinfliege', NULL, NULL, NULL, 23222), -- 59770
(59769, 'deDE', 'Bambuspython', NULL, NULL, NULL, 23222), -- 59769
(64311, 'deDE', 'Ausguck des Schlangenblicks', 'Ausguck des Schlangenblicks', NULL, NULL, 23222), -- 64311
(59747, 'deDE', 'Donnerfaustmatriarchin', NULL, NULL, NULL, 23222), -- 59747
(59748, 'deDE', 'Donnerfaustjungtier', NULL, NULL, NULL, 23222), -- 59748
(59768, 'deDE', 'Jadeschimmerwespe', NULL, NULL, NULL, 23222), -- 59768
(66898, 'deDE', 'Stromschreiter', NULL, NULL, NULL, 23222), -- 66898
(59899, 'deDE', 'Fei', NULL, 'Die Jadeschlange', NULL, 23222), -- 59899
(61615, 'deDE', 'Momo Appears Smoke Bunny', NULL, NULL, NULL, 23222), -- 61615
(59418, 'deDE', 'Lehrensucher Cho', NULL, NULL, NULL, 23222), -- 59418
(59463, 'deDE', 'Schüler der Perlflossen', NULL, NULL, NULL, 23222), -- 59463
(59417, 'deDE', 'Sha-Echo', NULL, NULL, NULL, 23222), -- 59417
(59462, 'deDE', 'Klatscher vom Flotschhügel', NULL, 'Temp Model and Name', NULL, 23222), -- 59462
(59434, 'deDE', 'Sha-Rückstände', NULL, NULL, NULL, 23222), -- 59434
(66730, 'deDE', 'Hyuna von den Schreinen', NULL, 'Großmeistertierzähmerin', NULL, 23222), -- 66730
(66711, 'deDE', 'Himmelsformer', NULL, NULL, NULL, 23222), -- 66711
(66710, 'deDE', 'Schlingor', NULL, NULL, NULL, 23222), -- 66710
(66709, 'deDE', 'Dor die Wand', NULL, NULL, NULL, 23222), -- 66709
(59774, 'deDE', 'Elfenbeinstar', NULL, NULL, NULL, 23222), -- 59774
(56788, 'deDE', 'Schlangenausbilder', 'Schlangenausbilderin', NULL, NULL, 23222), -- 56788
(67084, 'deDE', 'Riesiger Siebwurm', NULL, NULL, NULL, 23222), -- 67084
(58363, 'deDE', 'Honigbewahrer', NULL, NULL, NULL, 23222), -- 58363
(58362, 'deDE', 'Obstgartenbienenstock', NULL, NULL, 'LootAll', 23222), -- 58362
(65711, 'deDE', 'Siebwurm', NULL, NULL, NULL, 23222), -- 65711
(57319, 'deDE', 'Ältestenweise Soom-Sing', NULL, NULL, NULL, 23222), -- 57319
(62650, 'deDE', 'Weiße Orchidee', NULL, NULL, NULL, 23222), -- 62650
(62649, 'deDE', 'Blutrote Pfingstrose', NULL, NULL, NULL, 23222), -- 62649
(70169, 'deDE', 'Meister Nguyen', NULL, 'Die Fünf Zauberer', NULL, 23222), -- 70169
(62656, 'deDE', 'Huo der Feuerteufel', NULL, 'Feuerwerk', NULL, 23222), -- 62656
(62657, 'deDE', 'Munterer Chao', NULL, 'Speis & Trank', NULL, 23222), -- 62657
(58440, 'deDE', 'Rennwimpel', NULL, NULL, NULL, 23222), -- 58440
(58168, 'deDE', 'Übungsziel', NULL, NULL, NULL, 23222), -- 58168
(57316, 'deDE', 'Adept von Yu''lon', 'Adeptin von Yu''lon', NULL, NULL, 23222), -- 57316
(57318, 'deDE', 'Schlangenreiter', 'Schlangenreiterin', NULL, NULL, 23222), -- 57318
(57323, 'deDE', 'Ältestenweiser Sou-Ting', NULL, NULL, NULL, 23222), -- 57323
(58694, 'deDE', 'Tintenspitzenkranich', NULL, NULL, NULL, 23222), -- 58694
(57314, 'deDE', 'Weiser Ja-Ro', NULL, NULL, NULL, 23222), -- 57314
(62647, 'deDE', 'Lan Hua', NULL, NULL, NULL, 23222), -- 62647
(57242, 'deDE', 'Ältestenweise Wind-Yi', NULL, NULL, NULL, 23222), -- 57242
(57325, 'deDE', 'Xiong der Muskelprotz', NULL, NULL, NULL, 23222), -- 57325
(58186, 'deDE', 'Wächter von Yu''lon', 'Wächterin von Yu''lon', NULL, NULL, 23222), -- 58186
(62655, 'deDE', 'Zhang Ochsenfuß', NULL, NULL, NULL, 23222), -- 62655
(62653, 'deDE', 'Jade', NULL, NULL, NULL, 23222), -- 62653
(62644, 'deDE', 'Kleiner Wen', NULL, NULL, NULL, 23222), -- 62644
(62643, 'deDE', 'Frecher Liu', NULL, NULL, NULL, 23222), -- 62643
(62642, 'deDE', 'Chunhua die wirbelnde Blüte', NULL, NULL, NULL, 23222), -- 62642
(62646, 'deDE', 'Fleißige Cheng', NULL, NULL, NULL, 23222), -- 62646
(62645, 'deDE', 'Schüchterne Sheng', NULL, NULL, NULL, 23222), -- 62645
(62652, 'deDE', 'Tempelwart Weiji', NULL, NULL, NULL, 23222), -- 62652
(62651, 'deDE', 'Tanji der Fischer', NULL, NULL, NULL, 23222), -- 62651
(57324, 'deDE', 'Ältestenweiser Tai-Feng', NULL, NULL, NULL, 23222), -- 57324
(57445, 'deDE', 'Kriegsschlange', NULL, NULL, NULL, 23222), -- 57445
(57444, 'deDE', 'Schlangenreiter', 'Schlangenreiterin', NULL, NULL, 23222), -- 57444
(59767, 'deDE', 'Winkelblume', NULL, NULL, NULL, 23222), -- 59767
(59727, 'deDE', 'Ginsa Pfeilauge', NULL, 'Flugmeisterin', NULL, 23222), -- 59727
(57313, 'deDE', 'Fela Holzohr', NULL, 'Gastwirtin', NULL, 23222), -- 57313
(66243, 'deDE', 'Pan die sanfte Hand', NULL, 'Stallmeisterin', NULL, 23222), -- 66243
(62999, 'deDE', 'Tempelschlange', NULL, NULL, 'wildpetcapturable', 23222), -- 62999
(63894, 'deDE', 'Helen Ry', NULL, NULL, NULL, 23222), -- 63894
(63893, 'deDE', 'Ben Fur', NULL, NULL, NULL, 23222), -- 63893
(57312, 'deDE', 'Meister Tao Holzohr', NULL, 'Speis & Trank', NULL, 23222), -- 57312
(56812, 'deDE', 'Priester von Yu''lon', 'Priesterin von Yu''lon', NULL, NULL, 23222), -- 56812
(62663, 'deDE', 'Tintenmeisterin Trista', NULL, 'Exotische Waren', NULL, 23222), -- 62663
(62662, 'deDE', 'Rüstungsschmiedin Moki', NULL, 'Metallwaren', NULL, 23222), -- 62662
(62661, 'deDE', 'Handwerker Yang', NULL, 'Schneider- und Lederverarbeitungsbedarf', NULL, 23222), -- 62661
(62660, 'deDE', 'Apotheker Sun', NULL, 'Alchemiehändler', NULL, 23222), -- 62660
(62659, 'deDE', 'Historikerin Lili', NULL, NULL, NULL, 23222), -- 62659
(56811, 'deDE', 'Schüler von Yu''lon', 'Schülerin von Yu''lon', NULL, NULL, 23222), -- 56811
(57326, 'deDE', 'Wächter von Yu''lon', 'Wächterin von Yu''lon', NULL, NULL, 23222), -- 57326
(58698, 'deDE', 'Tempelschlange', NULL, NULL, NULL, 23222), -- 58698
(59786, 'deDE', 'Lotuskranich', NULL, NULL, NULL, 23222), -- 59786
(56032, 'deDE', 'General Rik-Rik Junior', NULL, 'Sohn des Rik-Rik', NULL, 23222), -- 56032
(68555, 'deDE', 'Ka''wi der Schlinger', NULL, NULL, 'wildpet', 23222), -- 68555
(59753, 'deDE', 'Goldener Tiger', NULL, NULL, NULL, 23222), -- 59753
(59757, 'deDE', 'Dämmerpirscher', NULL, NULL, NULL, 23222), -- 59757
(59754, 'deDE', 'Gesprenkelte Raupe', NULL, NULL, NULL, 23222), -- 59754
(59766, 'deDE', 'Winkelblume', NULL, NULL, NULL, 23222), -- 59766
(59665, 'deDE', 'Diebischer Haubenkranich', NULL, NULL, NULL, 23222), -- 59665
(59110, 'deDE', 'Fuchswelpe', NULL, NULL, NULL, 23222), -- 59110
(55565, 'deDE', 'Kranichjäger', NULL, NULL, NULL, 23222), -- 55565
(59667, 'deDE', 'Schillernder Karpfen', NULL, NULL, NULL, 23222), -- 59667
(59782, 'deDE', 'Zypressenkondor', NULL, NULL, NULL, 23222), -- 59782
(66633, 'deDE', 'Ruukti', NULL, NULL, NULL, 23222), -- 66633
(66803, 'deDE', 'Ruukti Shadowpirscher', NULL, NULL, NULL, 23222), -- 66803
(66617, 'deDE', 'Ro''shen', NULL, NULL, NULL, 23222), -- 66617
(56441, 'deDE', 'Schemen von Ling Herzfaust', NULL, NULL, NULL, 23222), -- 56441
(62991, 'deDE', 'Korallnatter', NULL, NULL, 'wildpetcapturable', 23222), -- 62991
(56444, 'deDE', 'Gepeinigter Geist', NULL, NULL, NULL, 23222), -- 56444
(59114, 'deDE', 'Generic Bunny', NULL, NULL, NULL, 23222), -- 59114
(56233, 'deDE', 'Bewohner des Perlflossendorfes', NULL, NULL, 'buy', 23222), -- 56233
(59351, 'deDE', 'Smaragdene Sumpffliege', NULL, NULL, NULL, 23222), -- 59351
(59356, 'deDE', 'Korallnatter', NULL, NULL, NULL, 23222), -- 59356
(56653, 'deDE', 'Wilder Pirscher', NULL, NULL, NULL, 23222), -- 56653
(56683, 'deDE', 'Wilder Pirscher', NULL, NULL, NULL, 23222), -- 56683
(56655, 'deDE', 'Wilde Jägerin', NULL, NULL, NULL, 23222), -- 56655
(56654, 'deDE', 'Wilder Pirscher', NULL, NULL, NULL, 23222), -- 56654
(56650, 'deDE', 'Steinhautbasilisk', NULL, NULL, NULL, 23222), -- 56650
(55196, 'deDE', 'Karasshi der Draufgänger', NULL, NULL, NULL, 23222), -- 55196
(55201, 'deDE', 'Jinyugefangener', NULL, NULL, NULL, 23222), -- 55201
(61605, 'deDE', 'Jinyufloß', NULL, NULL, NULL, 23222), -- 61605
(61547, 'deDE', 'Gukguts Schaukel', NULL, NULL, NULL, 23222), -- 61547
(61546, 'deDE', 'Häuptling Gukgut', NULL, NULL, NULL, 23222), -- 61546
(59037, 'deDE', 'Kung Din', NULL, NULL, NULL, 23222), -- 59037
(61558, 'deDE', 'Schlingschwanzschaukel', NULL, NULL, NULL, 23222), -- 61558
(61557, 'deDE', 'Kronenhüpfer der Schlingschwänze', NULL, NULL, NULL, 23222), -- 61557
(55490, 'deDE', 'Allianzgefangener', NULL, NULL, NULL, 23222), -- 55490
(55193, 'deDE', 'Schlammseher der Schlingschwänze', NULL, NULL, NULL, 23222), -- 55193
(61562, 'deDE', 'Kronenhüpfer der Schlingschwänze', NULL, NULL, NULL, 23222), -- 61562
(61602, 'deDE', 'Glasaugenschildkröte', NULL, NULL, NULL, 23222), -- 61602
(54960, 'deDE', 'Ältester Lusshan', NULL, 'Wassersprecher', NULL, 23222), -- 54960
(59159, 'deDE', 'Mystiker der Perlflossen', NULL, NULL, NULL, 23222), -- 59159
(64475, 'deDE', 'Mishi', NULL, 'Lehrensucher Chos Begleiter', NULL, 23222), -- 64475
(59620, 'deDE', 'Lehrensucher Cho', NULL, NULL, NULL, 23222), -- 59620
(56737, 'deDE', 'Ut-Nam', NULL, 'Drachenmeister', NULL, 23222), -- 56737
(59058, 'deDE', 'Perlenhüter Fujin', NULL, NULL, NULL, 23222), -- 59058
(59060, 'deDE', 'Wildtiertöter Insshu', NULL, NULL, NULL, 23222), -- 59060
(54557, 'deDE', 'Bernglaskrabbe', NULL, NULL, NULL, 23222), -- 54557
(56672, 'deDE', 'Nebelwirkeradept', NULL, NULL, NULL, 23222), -- 56672
(56222, 'deDE', 'Karasshi der Draufgänger', NULL, NULL, NULL, 23222), -- 56222
(56227, 'deDE', 'Mishka', NULL, 'SI:7', NULL, 23222), -- 56227
(55333, 'deDE', 'Rell Nachtwind', NULL, 'SI:7', NULL, 23222), -- 55333
(55283, 'deDE', 'Amber Kearnen', NULL, 'SI:7', NULL, 23222), -- 55283
(55282, 'deDE', 'Sully "Die Gurke" McLeary', NULL, 'SI:7', NULL, 23222), -- 55282
(55284, 'deDE', 'Klein-Lu', NULL, NULL, NULL, 23222), -- 55284
(61604, 'deDE', 'Winziger Goldfisch', NULL, NULL, NULL, 23222), -- 61604
(61747, 'deDE', 'Juwelenbesetzter Ara', NULL, NULL, NULL, 23222), -- 61747
(59059, 'deDE', 'Aalbeschwörer Shaoshu', NULL, 'Lebensmittelverkäufer', NULL, 23222), -- 59059
(56687, 'deDE', 'Metallarbeiter Sashi', NULL, 'Handwerkswaren', 'Buy', 23222), -- 56687
(61598, 'deDE', 'Seidenweber Rui', NULL, 'Abenteurerbedarf', 'Buy', 23222), -- 61598
(61596, 'deDE', 'Hangmeister Puoba', NULL, 'Getränke', NULL, 23222), -- 61596
(59169, 'deDE', 'Bunny: Spirits of the Water', NULL, NULL, NULL, 23222), -- 59169
(61599, 'deDE', 'Jessu der Fröhliche', NULL, 'Gastwirt', NULL, 23222), -- 61599
(56689, 'deDE', 'Perlentaucher Su-Su', NULL, 'Seltene Edelsteine', 'Buy', 23222), -- 56689
(55370, 'deDE', 'General Purpose Bunny ZTO', NULL, NULL, NULL, 23222), -- 55370
(56693, 'deDE', 'Ot-Temmdo', NULL, 'Wasserschmied', NULL, 23222), -- 56693
(56690, 'deDE', 'Ausbilder Scharffinne', NULL, NULL, NULL, 23222), -- 56690
(61614, 'deDE', 'Bläschenmacher Ashji', NULL, NULL, NULL, 23222), -- 61614
(59084, 'deDE', 'Perlmuttschildkröte', NULL, NULL, NULL, 23222), -- 59084
(61611, 'deDE', 'Winziger Goldfisch', NULL, NULL, NULL, 23222), -- 61611
(56591, 'deDE', 'Aqualyth der Perlflossen', NULL, NULL, NULL, 23222), -- 56591
(54959, 'deDE', 'Aqualyth der Perlflossen', NULL, NULL, NULL, 23222), -- 54959
(59348, 'deDE', 'Bewohner des Perlflossendorfes', NULL, NULL, NULL, 23222), -- 59348
(56592, 'deDE', 'Aqualyth der Perlflossen', NULL, NULL, NULL, 23222), -- 56592
(56585, 'deDE', 'Aqualyth der Perlflossen', NULL, NULL, NULL, 23222), -- 56585
(62998, 'deDE', 'Spiegelschreiter', NULL, NULL, 'wildpetcapturable', 23222), -- 62998
(60971, 'deDE', 'Wütender Teichbeobachter der Perlflossen', NULL, NULL, NULL, 23222), -- 60971
(55110, 'deDE', 'Langfinger der Schlingschwänze', NULL, NULL, NULL, 23222), -- 55110
(56701, 'deDE', 'Teichhüter der Perlflossen', NULL, NULL, NULL, 23222), -- 56701
(55195, 'deDE', 'Entbeiner der Schlingschwänze', NULL, NULL, NULL, 23222), -- 55195
(59085, 'deDE', 'Spiegelschreiter', NULL, NULL, NULL, 23222), -- 59085
(59115, 'deDE', 'Generic Bunny', NULL, NULL, NULL, 23222), -- 59115
(59113, 'deDE', 'Generic Bunny', NULL, NULL, NULL, 23222), -- 59113
(60798, 'deDE', 'Toxischer Nebel', NULL, NULL, NULL, 23222), -- 60798
(63919, 'deDE', 'Gefleckter Laubfrosch', NULL, NULL, 'wildpetcapturable', 23222), -- 63919
(64287, 'deDE', 'Sommer', NULL, 'Lehrensucher Chos Begleiter', NULL, 23222), -- 64287
(64261, 'deDE', 'Exotischer gezähmter Kranich', NULL, NULL, NULL, 23222), -- 64261
(63137, 'deDE', 'Tree Trimming Effects Bunny', NULL, NULL, NULL, 23222), -- 63137
(64295, 'deDE', 'Herbst', NULL, 'Lehrensucher Chos Begleiter', NULL, 23222), -- 64295
(55376, 'deDE', 'Alchemy Bunny', NULL, NULL, NULL, 23222), -- 55376
(56304, 'deDE', 'Nebelkrabbler', NULL, NULL, NULL, 23222), -- 56304
(64774, 'deDE', 'Gefleckter Laubfrosch', NULL, NULL, NULL, 23222), -- 64774
(54558, 'deDE', 'Sumpfkrokilisk', NULL, NULL, NULL, 23222), -- 54558
(59742, 'deDE', 'Donnerfaustgorilla', NULL, NULL, NULL, 23222), -- 59742
(63716, 'deDE', 'Maskenenokjunges', NULL, NULL, 'wildpetcapturable', 23222), -- 63716
(63003, 'deDE', 'Maskenenok', NULL, NULL, 'wildpetcapturable', 23222), -- 63003
(66276, 'deDE', 'Ga''trul', NULL, NULL, NULL, 23222), -- 66276
(54616, 'deDE', 'Sully "Die Gurke" McLeary', NULL, 'SI:7', NULL, 23222), -- 54616
(66274, 'deDE', 'Dalgan', NULL, NULL, NULL, 23222), -- 66274
(66275, 'deDE', 'Bellandra Teufelssturm', NULL, NULL, NULL, 23222), -- 66275
(66281, 'deDE', 'Heilet Schmachtherz', NULL, NULL, NULL, 23222), -- 66281
(66270, 'deDE', 'Hexenmeister des Teufelssturms', 'Hexenmeisterin des Teufelssturms', NULL, NULL, 23222), -- 66270
(66271, 'deDE', 'Beschwörer des Teufelssturms', 'Beschwörerin des Teufelssturms', NULL, NULL, 23222), -- 66271
(66273, 'deDE', 'Demolierexperte von Zwillingsfels', NULL, NULL, NULL, 23222), -- 66273
(12922, 'deDE', 'Wichteldiener', NULL, NULL, NULL, 23222), -- 12922
(59744, 'deDE', 'Silberhornjährling', NULL, NULL, NULL, 23222), -- 59744
(59743, 'deDE', 'Silberhornhirsch', NULL, NULL, NULL, 23222), -- 59743
(66092, 'deDE', 'Point Bunny', NULL, NULL, NULL, 23222), -- 66092
(65910, 'deDE', 'Sunke Khang', NULL, 'Bürgermeister', NULL, 23222), -- 65910
(66094, 'deDE', 'Burma Schwellblüte', NULL, NULL, NULL, 23222), -- 66094
(66093, 'deDE', 'To''ud Dämmerbrise', NULL, NULL, NULL, 23222), -- 66093
(66032, 'deDE', 'Behüterin Jo Lin', NULL, NULL, NULL, 23222), -- 66032
(66034, 'deDE', 'Kheila Fallwasser', NULL, NULL, NULL, 23222), -- 66034
(66030, 'deDE', 'Rollenbewahrer Mushu', NULL, NULL, NULL, 23222), -- 66030
(66035, 'deDE', 'Händlerin Jina', NULL, 'Gemischtwaren', NULL, 23222), -- 66035
(66031, 'deDE', 'Ältester Daelo', NULL, NULL, NULL, 23222), -- 66031
(54615, 'deDE', 'Nick Codeknack', NULL, 'SI:7', NULL, 23222), -- 54615
(54559, 'deDE', 'Schillernde Bernfliege', NULL, NULL, NULL, 23222), -- 54559
(55175, 'deDE', 'Kanone der Allianz', NULL, NULL, 'Inspect', 23222), -- 55175
(55174, 'deDE', 'Navigationskarte', NULL, NULL, 'Inspect', 23222), -- 55174
(54623, 'deDE', 'Schiffbrüchiger Fußsoldat', 'Schiffbrüchige Fußsoldatin', NULL, NULL, 23222), -- 54623
(55155, 'deDE', 'Edles ledergebundenes Tagebuch', NULL, NULL, 'Inspect', 23222), -- 55155
(55176, 'deDE', 'Allianzbefehle', NULL, NULL, 'Inspect', 23222), -- 55176
(55168, 'deDE', 'Allianzleibwächter', NULL, NULL, 'Inspect', 23222), -- 55168
(55173, 'deDE', 'Seebär der Horde', NULL, NULL, 'Inspect', 23222), -- 55173
(55167, 'deDE', 'Banner der Allianz', NULL, NULL, 'Inspect', 23222), -- 55167
(55177, 'deDE', 'Bunny: Sapphire Explosion', NULL, NULL, NULL, 23222), -- 55177
(66102, 'deDE', 'Ho-zen-Schaukel', NULL, NULL, NULL, 23222), -- 66102
(66148, 'deDE', 'Ho-zen-Taucher', NULL, NULL, NULL, 23222), -- 66148
(66106, 'deDE', 'Ho-zen-Strauchdieb', NULL, NULL, NULL, 23222), -- 66106
(66153, 'deDE', 'Ho-zen-Strauchdieb', NULL, NULL, NULL, 23222), -- 66153
(57119, 'deDE', 'Ho-zen-Verwüster', NULL, NULL, NULL, 23222), -- 57119
(63152, 'deDE', 'Painting Effects Bunny', NULL, NULL, NULL, 23222), -- 63152
(67156, 'deDE', 'Grain Gears Bunny', NULL, NULL, NULL, 23222), -- 67156
(62994, 'deDE', 'Smaragdschildkröte', NULL, NULL, 'wildpetcapturable', 23222), -- 62994
(62997, 'deDE', 'Dschungelflitzer', NULL, NULL, 'wildpetcapturable', 23222), -- 62997
(66750, 'deDE', 'Ammenhai', NULL, NULL, NULL, 23222), -- 66750
(66747, 'deDE', 'Blutige Lache', NULL, NULL, NULL, 23222), -- 66747
(61446, 'deDE', 'Moosflutkrabbe', NULL, NULL, NULL, 23222), -- 61446
(66942, 'deDE', 'Sha-Heimsuchung', NULL, NULL, NULL, 23222), -- 66942
(63001, 'deDE', 'Seidenperlenschnecke', NULL, NULL, 'wildpetcapturable', 23222), -- 63001
(55336, 'deDE', 'Seidenperlenschnecke', NULL, NULL, NULL, 23222), -- 55336
(67011, 'deDE', 'Grunzer von Garrosh''ar', 'Grunzerin von Garrosh''ar', NULL, NULL, 23222), -- 67011
(66441, 'deDE', 'Marinesoldat der Himmelsfeuer', 'Marinesoldatin der Himmelsfeuer', NULL, NULL, 23222), -- 66441
(66460, 'deDE', 'Gyrokopterpilot der Himmelsfeuer', 'Gyrokopterpilotin der Himmelsfeuer', NULL, NULL, 23222), -- 66460
(66280, 'deDE', 'Peon von Garrosh''ar', NULL, NULL, NULL, 23222), -- 66280
(66300, 'deDE', 'Marinesoldat der Himmelsfeuer', 'Marinesoldatin der Himmelsfeuer', NULL, NULL, 23222), -- 66300
(66371, 'deDE', 'Fire Stalker', NULL, NULL, NULL, 23222), -- 66371
(66688, 'deDE', 'Sha-Fühler', NULL, NULL, NULL, 23222), -- 66688
(66424, 'deDE', 'Sha-Herold', NULL, NULL, NULL, 23222), -- 66424
(66423, 'deDE', 'Sha-Heimsuchung', NULL, NULL, NULL, 23222), -- 66423
(66290, 'deDE', 'Kupplungsschmierer von Garrosh''ar', 'Kupplungsschmiererin von Garrosh''ar', NULL, NULL, 23222), -- 66290
(66457, 'deDE', 'Grunzer von Garrosh''ar', 'Grunzerin von Garrosh''ar', NULL, NULL, 23222), -- 66457
(59668, 'deDE', 'Dschungelflitzer', NULL, NULL, NULL, 23222), -- 59668
(66279, 'deDE', 'Demolierer von Zwillingsfels', NULL, NULL, NULL, 23222), -- 66279
(66272, 'deDE', 'Todeswache von Zwillingsfels', NULL, NULL, NULL, 23222), -- 66272
(66267, 'deDE', 'Fluchwirker der Gatrul''lon', 'Fluchwirkerin der Gatrul''lon', NULL, NULL, 23222), -- 66267
(66268, 'deDE', 'Flammenrufer der Gatrul''lon', 'Flammenruferin der Gatrul''lon', NULL, NULL, 23222), -- 66268
(66269, 'deDE', 'Grunzer von Zwillingsfels', 'Grunzerin von Zwillingsfels', NULL, NULL, 23222), -- 66269
(66366, 'deDE', 'Gefangenes Pandarenkind', 'Gefangenes Pandarenkind', NULL, NULL, 23222), -- 66366
(63715, 'deDE', 'Springspinne', NULL, NULL, 'wildpetcapturable', 23222),
(63358, 'deDE', 'Sifangotterjunges', NULL, NULL, 'wildpetcapturable', 23222), -- 63358
(63057, 'deDE', 'Sifangotter', NULL, NULL, 'wildpetcapturable', 23222), -- 63057
(65612, 'deDE', 'Windwärtssäbler', NULL, NULL, NULL, 23222), -- 65612
(67085, 'deDE', 'Windwärtsalphatier', NULL, NULL, NULL, 23222), -- 67085
(63538, 'deDE', 'Windwärtsjägerin', NULL, NULL, NULL, 23222), -- 63538
(63540, 'deDE', 'Junger Windwärtstiger', NULL, NULL, NULL, 23222), -- 63540
(63537, 'deDE', 'Windwärtstiger', NULL, NULL, NULL, 23222), -- 63537
(63536, 'deDE', 'Echsenlord der Schlängelschuppen', NULL, NULL, NULL, 23222), -- 63536
(58217, 'deDE', 'Windwärtsviper', NULL, NULL, NULL, 23222), -- 58217
(58212, 'deDE', 'Reißer der Schlängelschuppen', NULL, NULL, NULL, 23222), -- 58212
(63005, 'deDE', 'Turmgebundene Krabbe', NULL, NULL, 'wildpetcapturable', 23222), -- 63005
(58675, 'deDE', 'Turmgebundene Krabbe', NULL, NULL, NULL, 23222), -- 58675
(63006, 'deDE', 'Sandiger Sturmvogel', NULL, NULL, 'wildpetcapturable', 23222), -- 63006
(58230, 'deDE', 'Sandiger Sturmvogel', NULL, NULL, NULL, 23222), -- 58230
(65635, 'deDE', 'Schattenfeetrickster', NULL, NULL, NULL, 23222), -- 65635
(58275, 'deDE', 'Nest Bunny', NULL, NULL, NULL, 23222), -- 58275
(58231, 'deDE', 'Klammnetzfischadler', NULL, NULL, NULL, 23222), -- 58231
(58225, 'deDE', 'Ausbilder Tong', NULL, NULL, NULL, 23222), -- 58225
(58220, 'deDE', 'Windwärtsjungtier', NULL, NULL, NULL, 23222), -- 58220
(58213, 'deDE', 'Windwärtsmatriarchin', NULL, NULL, NULL, 23222), -- 58213
(58244, 'deDE', 'Windwärtsjungtier', NULL, NULL, NULL, 23222), -- 58244
(58416, 'deDE', 'Verletztes Junges', NULL, NULL, NULL, 23222), -- 58416
(58243, 'deDE', 'Windwärtsjungtier', NULL, NULL, NULL, 23222), -- 58243
(58218, 'deDE', 'Salzrückenschildkröte', NULL, NULL, NULL, 23222), -- 58218
(63532, 'deDE', 'Eitrinker der Schlängelschuppen', NULL, NULL, NULL, 23222), -- 63532
(58219, 'deDE', 'Salzrückenjährling', NULL, NULL, NULL, 23222), -- 58219
(58214, 'deDE', 'Windwärtsmatriarchin', NULL, NULL, NULL, 23222), -- 58214
(58236, 'deDE', 'Sandiger Sturmvogel', NULL, NULL, NULL, 23222), -- 58236
(62382, 'deDE', 'Herrenloser Drachen', NULL, NULL, 'taxi', 23222), -- 62382
(61640, 'deDE', 'Koch Kyel', NULL, 'Händler für Inspiration', NULL, 23222), -- 61640
(56065, 'deDE', 'Tintenmeister Wei', NULL, 'Inschriftenkundelehrer', NULL, 23222), -- 56065
(56064, 'deDE', 'Tintenmeister Glenzu', NULL, NULL, NULL, 23222), -- 56064
(25305, 'deDE', 'Tanzende Flammen', NULL, NULL, NULL, 23222), -- 25305
(55233, 'deDE', 'Lin Windpelz', NULL, 'Gastwirtin', NULL, 23222), -- 55233
(55809, 'deDE', 'Peiji Zapfgold', NULL, 'Braumeister', NULL, 23222), -- 55809
(56775, 'deDE', 'Li', NULL, 'Der Becherfinder', NULL, 23222), -- 56775
(56774, 'deDE', 'Bolo', NULL, 'Der Fassflicker', NULL, 23222), -- 56774
(56776, 'deDE', 'Pan', NULL, 'Die Hahnbiegerin', NULL, 23222), -- 56776
(56778, 'deDE', 'Yol', NULL, 'Nudelmatrone', NULL, 23222), -- 56778
(56707, 'deDE', 'Chin', NULL, 'Meister der Nudeln', NULL, 23222), -- 56707
(65127, 'deDE', 'Lai die Zauberpranke', NULL, 'Verzauberkunstlehrerin', NULL, 23222), -- 65127
(61861, 'deDE', 'Eyan Gerdzu', NULL, 'Steinmetzmeister', NULL, 23222), -- 61861
(56777, 'deDE', 'Ni Sanftpfote', NULL, 'Alchemiebedarf', NULL, 23222), -- 56777
(56769, 'deDE', 'Junger Morgenwächter', 'Junge Morgenwächterin', NULL, NULL, 23222), -- 56769
(56062, 'deDE', 'Tzu der Eisenbauch', NULL, 'Mönch im Ruhestand', NULL, 23222), -- 56062
(66293, 'deDE', 'Albinoreitkranich', NULL, NULL, NULL, 23222), -- 66293
(66241, 'deDE', 'Hong der Sanfte', NULL, 'Stallmeister', NULL, 23222), -- 66241
(66306, 'deDE', 'Schwarzkrallenagentin', NULL, 'Diener des Schwarzen Prinzen', NULL, 23222), -- 66306
(56705, 'deDE', 'Sengbrumm', NULL, 'Abenteurerbedarf', NULL, 23222), -- 56705
(56708, 'deDE', 'Syra Zapfgold', NULL, NULL, NULL, 23222), -- 56708
(55788, 'deDE', 'Lo Wanderbräu', NULL, 'Braulehrling', NULL, 23222), -- 55788
(59173, 'deDE', 'Kai Wanderbräu', NULL, 'Braumeister', NULL, 23222), -- 59173
(59186, 'deDE', 'Fassläufer Li', NULL, 'Flugmeister', NULL, 23222), -- 59186
(56348, 'deDE', 'Toya', NULL, NULL, NULL, 23222), -- 56348
(59160, 'deDE', 'Meister Windpelz', NULL, 'Bürgermeister von Morgenblüte', NULL, 23222), -- 59160
(56659, 'deDE', 'Shin', NULL, NULL, NULL, 23222), -- 56659
(55413, 'deDE', 'An Windpelz', NULL, NULL, NULL, 23222), -- 55413
(59383, 'deDE', 'Alter Mann Trübaug', NULL, NULL, NULL, 23222), -- 59383
(59105, 'deDE', 'Wandernder Grünrücken', NULL, NULL, NULL, 23222), -- 59105
(64522, 'deDE', 'Nik Gi', NULL, 'Dörrfleischverkäufer', NULL, 23222), -- 64522
(56768, 'deDE', 'Morgenwanderer', 'Morgenwanderin', NULL, NULL, 23222), -- 56768
(63899, 'deDE', 'Reitpanza', NULL, NULL, NULL, 23222), -- 63899
(63900, 'deDE', 'Panzareita', NULL, NULL, NULL, 23222), -- 63900
(56771, 'deDE', 'Wächter der Morgenröte', 'Wächterin der Morgenröte', NULL, NULL, 23222), -- 56771
(54998, 'deDE', 'Lehrling Yufi', NULL, NULL, NULL, 23222), -- 54998
(56815, 'deDE', 'Gartenbesucher', 'Gartenbesucherin', 'Tourist', NULL, 23222), -- 56815
(62325, 'deDE', 'Meila', NULL, NULL, NULL, 23222), -- 62325
(62328, 'deDE', 'Schnitzer Liupo', NULL, 'Holzarbeiten und Reparatur', NULL, 23222), -- 62328
(62323, 'deDE', 'La die Sanfte', NULL, NULL, NULL, 23222), -- 62323
(59112, 'deDE', 'Morgenfederflieger', NULL, NULL, NULL, 23222), -- 59112
(65114, 'deDE', 'Len der Hammer', NULL, 'Schmiedekunstlehrer', NULL, 23222), -- 65114
(62326, 'deDE', 'Stämmiger Shung', NULL, NULL, NULL, 23222), -- 62326
(62322, 'deDE', 'Anmutiger Schwan', NULL, 'Gastwirtin', NULL, 23222), -- 62322
(62321, 'deDE', 'Braumeister Tzu', NULL, 'Mönchslehrer', NULL, 23222), -- 62321
(65098, 'deDE', 'Mai die Jadeformerin', NULL, 'Juwelierskunstlehrerin', NULL, 23222), -- 65098
(62334, 'deDE', 'Mönch von Grünstein', 'Mönch von Grünstein', NULL, NULL, 23222), -- 62334
(62516, 'deDE', 'Dorfbewohner von Grünstein', 'Dorfbewohnerin von Grünstein', NULL, NULL, 23222), -- 62516
(62327, 'deDE', 'Schreiberin Rinji', NULL, 'Inschriftenkundelehrerin', NULL, 23222), -- 62327
(59735, 'deDE', 'Versorger Towsa', NULL, 'Flugmeister', NULL, 23222), -- 59735
(56346, 'deDE', 'Vorarbeiterin Mann', NULL, '"Sie fühlt sich schön mit Jade"', NULL, 23222), -- 56346
(56467, 'deDE', 'Hao Mann', NULL, NULL, NULL, 23222), -- 56467
(56345, 'deDE', 'Lehrensucher Cho', NULL, NULL, NULL, 23222), -- 56345
(65022, 'deDE', 'Verwundetes Besatzungsmitglied', 'Verwundetes Besatzungsmitglied', NULL, NULL, 23222), -- 65022
(64999, 'deDE', 'Kapitän Matok', NULL, NULL, NULL, 23222), -- 64999
(59883, 'deDE', 'Hozen Swing - Go Go', NULL, NULL, NULL, 23222), -- 59883
(64994, 'deDE', 'Go Go', NULL, NULL, NULL, 23222), -- 64994
(64325, 'deDE', 'Junger Fischer', NULL, NULL, NULL, 23222), -- 64325
(64365, 'deDE', 'Mili Wanderbräu', NULL, 'Braumeisterlehrling', NULL, 23222), -- 64365
(64343, 'deDE', 'Dip Dip', NULL, 'Glitschis & Flutschis', NULL, 23222), -- 64343
(64342, 'deDE', 'Nip Nip', NULL, 'Glitschis & Flutschis', NULL, 23222), -- 64342
(64345, 'deDE', 'Salzschuppenkarpfen', NULL, NULL, NULL, 23222), -- 64345
(64333, 'deDE', 'Frau Lang', NULL, 'Die Rosenfinderin', NULL, 23222), -- 64333
(64332, 'deDE', 'Ariel', NULL, NULL, NULL, 23222), -- 64332
(64331, 'deDE', 'Bry Lang', NULL, 'Der Fischfänger', NULL, 23222), -- 64331
(59733, 'deDE', 'Gingo Bierboden', NULL, 'Flugmeister', NULL, 23222), -- 59733
(62868, 'deDE', 'Lana die Meeresbriese', NULL, 'Gastwirt', NULL, 23222), -- 62868
(65624, 'deDE', 'Kampftrinker von Uuna', NULL, NULL, NULL, 23222), -- 65624
(65628, 'deDE', 'Fürst Uuna', NULL, 'Schrecken der See', NULL, 23222), -- 65628
(65623, 'deDE', 'Thunfischfänger von Uuna', NULL, NULL, NULL, 23222), -- 65623
(65621, 'deDE', 'Stümper von Uuna', NULL, NULL, NULL, 23222), -- 65621
(65641, 'deDE', 'Fehlgeleitete Schlange', NULL, NULL, 'lootall', 23222), -- 65641
(65614, 'deDE', 'Saat des Zweifels', NULL, NULL, NULL, 23222), -- 65614
(65663, 'deDE', 'Sha Bunny 1', NULL, NULL, NULL, 23222), -- 65663
(65666, 'deDE', 'Sha Bunny 2', NULL, NULL, NULL, 23222), -- 65666
(65653, 'deDE', 'Weinende Witwe', NULL, NULL, NULL, 23222), -- 65653
(65622, 'deDE', 'Weinende Witwe', NULL, NULL, NULL, 23222), -- 65622
(51984, 'deDE', 'Deadwind Widow Invisible Stalker', NULL, NULL, NULL, 23222), -- 51984
(65003, 'deDE', 'Martar der Nicht-sehr-Kluge', NULL, NULL, NULL, 23222), -- 65003
(65658, 'deDE', 'Witwenbrut', NULL, NULL, NULL, 23222), -- 65658
(65634, 'deDE', 'Witwennetz', NULL, NULL, NULL, 23222), -- 65634
(65667, 'deDE', 'Sha Bunny 3', NULL, NULL, NULL, 23222), -- 65667
(59104, 'deDE', 'Schreinelch', NULL, NULL, NULL, 23222), -- 59104
(58528, 'deDE', 'Goldwolkenschlange', NULL, NULL, NULL, 23222), -- 58528
(58527, 'deDE', 'Jadewolkenschlange', NULL, NULL, NULL, 23222), -- 58527
(58526, 'deDE', 'Azurwolkenschlange', NULL, NULL, NULL, 23222), -- 58526
(58420, 'deDE', 'Ausbilderin Windklinge', NULL, 'Schlangenmeisterin', NULL, 23222), -- 58420
(58499, 'deDE', 'Goldwolkenschlange', NULL, NULL, 'vehichleCursor', 23222), -- 58499
(58519, 'deDE', 'Sonnenschein', NULL, NULL, NULL, 23222), -- 58519
(58516, 'deDE', 'Agara die Rote', NULL, NULL, NULL, 23222), -- 58516
(58506, 'deDE', 'Flitze Langpranke', NULL, NULL, NULL, 23222), -- 58506
(58510, 'deDE', 'Suchi die Süße', NULL, NULL, NULL, 23222), -- 58510
(58520, 'deDE', 'Asche', NULL, NULL, NULL, 23222), -- 58520
(58511, 'deDE', 'Qua-Ro Weißstirn', NULL, NULL, NULL, 23222), -- 58511
(58518, 'deDE', 'Li Ying', NULL, NULL, NULL, 23222), -- 58518
(58509, 'deDE', 'Ningna Dunkelrad', NULL, NULL, NULL, 23222), -- 58509
(61407, 'deDE', 'Ausbilderin Sternblüte', NULL, 'Schlangenmeisterin', NULL, 23222), -- 61407
(58517, 'deDE', 'Hyakinthos', NULL, NULL, NULL, 23222), -- 58517
(58508, 'deDE', 'Bao der Große', NULL, NULL, NULL, 23222), -- 58508
(59732, 'deDE', 'Injar''i Seeblüte', NULL, 'Flugmeisterin', NULL, 23222), -- 59732
(65733, 'deDE', 'Goldwolkenschlange', NULL, NULL, NULL, 23222), -- 65733
(65732, 'deDE', 'Azurwolkenschlange', NULL, NULL, NULL, 23222), -- 65732
(65731, 'deDE', 'Jadewolkenschlange', NULL, NULL, NULL, 23222), -- 65731
(58531, 'deDE', 'Todu Tigerkralle', NULL, 'Wettrennenfunktionär', NULL, 23222), -- 58531
(58414, 'deDE', 'San Rotschuppe', NULL, 'Schlangenhüter', NULL, 23222), -- 58414
(58228, 'deDE', 'Ausbilder Himmelsdorn', NULL, 'Schlangenmeister', NULL, 23222), -- 58228
(58564, 'deDE', 'Älteste Anli', NULL, 'Schlangenmeisterin', NULL, 23222), -- 58564
(56798, 'deDE', 'Gezähmte Schlange', NULL, NULL, NULL, 23222), -- 56798
(58413, 'deDE', 'Jenova Langaug', NULL, 'Schlangenmeisterin', NULL, 23222), -- 58413
(67199, 'deDE', 'Verteidiger des Arboretums', 'Verteidigerin des Arboretums', NULL, NULL, 23222), -- 67199
(61406, 'deDE', 'Zuschauerin', NULL, NULL, NULL, 23222), -- 61406
(58692, 'deDE', 'Zuschauer', NULL, NULL, NULL, 23222), -- 58692
(61410, 'deDE', 'Junge Wolkenschlange', NULL, NULL, NULL, 23222), -- 61410
(56063, 'deDE', 'Tintenmeister Jo Po', NULL, NULL, NULL, 23222), -- 56063
(63002, 'deDE', 'Gartenfrosch', NULL, NULL, 'wildpetcapturable', 23222), -- 63002
(58696, 'deDE', 'Gartenfrosch', NULL, NULL, NULL, 23222), -- 58696
(55681, 'deDE', 'Ayor', NULL, 'Langs Begleiter', NULL, 23222), -- 55681
(56790, 'deDE', 'Honigfänger Lang', NULL, NULL, NULL, 23222), -- 56790
(65215, 'deDE', 'Gartenmotte', NULL, NULL, 'wildpetcapturable', 23222), -- 65215
(56829, 'deDE', 'Gartenmotte', NULL, NULL, NULL, 23222), -- 56829
(56201, 'deDE', 'Obstgartenwespe', NULL, NULL, NULL, 23222), -- 56201
(56070, 'deDE', 'Seidenholzpirscher', NULL, NULL, NULL, 23222), -- 56070
(56931, 'deDE', 'Bambusbaumzweig', NULL, NULL, NULL, 23222), -- 56931
(55267, 'deDE', 'Verängstigtes Pandarenkind', 'Verängstigtes Pandarenkind', NULL, NULL, 23222), -- 55267
(55236, 'deDE', 'Jadewächter', NULL, NULL, NULL, 23222), -- 55236
(60476, 'deDE', 'Ho-zen-Wegelagerer', NULL, NULL, NULL, 23222), -- 60476
(63810, 'deDE', 'Ho-zen-Magenfetzer', NULL, NULL, NULL, 23222), -- 63810
(64694, 'deDE', 'Sam der Weise', NULL, 'Der Metallbraumeister', NULL, 23222), -- 64694
(64697, 'deDE', 'Liu', NULL, NULL, NULL, 23222), -- 64697
(64696, 'deDE', 'Ty', NULL, NULL, NULL, 23222), -- 64696
(64695, 'deDE', 'Mei Li-Sa', NULL, NULL, NULL, 23222), -- 64695
(64984, 'deDE', 'Statue der Jadeschlange', NULL, NULL, NULL, 23222), -- 64984
(64398, 'deDE', 'Nebelwirkermönch', 'Nebelwirkermönch', NULL, NULL, 23222), -- 64398
(64394, 'deDE', 'Nebelwirker Chun', NULL, NULL, NULL, 23222), -- 64394
(64384, 'deDE', 'Nebelwirkerin Lian', NULL, NULL, NULL, 23222), -- 64384
(64381, 'deDE', 'Donnerpfotenwächter', 'Donnerpfotenwächterin', NULL, NULL, 23222), -- 64381
(59781, 'deDE', 'Donnerpfoteninitiand', NULL, NULL, NULL, 23222), -- 59781
(59779, 'deDE', 'Donnerpfotenwächter', 'Donnerpfotenwächterin', NULL, NULL, 23222), -- 59779
(64382, 'deDE', 'Donnerpfoteninitiand', 'Donnerpfoteninitiandin', NULL, NULL, 23222), -- 64382
(59569, 'deDE', 'Braumeisterin Blanche', NULL, NULL, NULL, 23222), -- 59569
(67097, 'deDE', 'Yang Wei der Weise', NULL, NULL, NULL, 23222), -- 67097
(67096, 'deDE', 'Flinker Shou', NULL, 'Braumeisterlehrling', NULL, 23222), -- 67096
(54914, 'deDE', 'Hoher Ältester Wolkensturz', NULL, NULL, NULL, 23222), -- 54914
(54913, 'deDE', 'Lin Zartpfote', NULL, NULL, NULL, 23222), -- 54913
(54925, 'deDE', 'Husshun', NULL, NULL, NULL, 23222), -- 54925
(54924, 'deDE', 'Zhi-Zhi', NULL, NULL, NULL, 23222), -- 54924
(54919, 'deDE', 'Geistweiser Gaoquan', NULL, NULL, NULL, 23222), -- 54919
(59736, 'deDE', 'Der gelehrsame Chu', NULL, 'Flugmeister', NULL, 23222), -- 59736
(54922, 'deDE', 'Meister Steinfaust', NULL, NULL, NULL, 23222), -- 54922
(55207, 'deDE', 'Bierbringer von Tian', NULL, NULL, NULL, 23222), -- 55207
(55030, 'deDE', 'Lehrling von Tian', NULL, NULL, NULL, 23222), -- 55030
(54926, 'deDE', 'Xiao', NULL, NULL, NULL, 23222), -- 54926
(54944, 'deDE', 'Schüler von Tian', 'Schülerin von Tian', NULL, NULL, 23222), -- 54944
(55029, 'deDE', 'Lehrling von Tian', NULL, NULL, NULL, 23222), -- 55029
(55028, 'deDE', 'Lehrling von Tian', 'Lehrling von Tian', NULL, NULL, 23222), -- 55028
(62867, 'deDE', 'Bolo der Ältere', NULL, 'Gastwirt und Schankwirt', NULL, 23222), -- 62867
(54982, 'deDE', 'Braumeister Lei Kanglei', NULL, NULL, NULL, 23222), -- 54982
(54980, 'deDE', 'Yao der Sammler', NULL, NULL, NULL, 23222), -- 54980
(54915, 'deDE', 'Klosterwart Wu', NULL, NULL, NULL, 23222), -- 54915
(54981, 'deDE', 'Juni Weißblüte', NULL, NULL, NULL, 23222), -- 54981
(55094, 'deDE', 'Wächter Shan Long', NULL, NULL, NULL, 23222), -- 55094
(54918, 'deDE', 'Ausbilderin Myang', NULL, NULL, NULL, 23222), -- 54918
(70264, 'deDE', 'Jimmy Truong', NULL, NULL, NULL, 23222), -- 70264
(70267, 'deDE', 'Emily Mei', NULL, NULL, NULL, 23222), -- 70267
(70266, 'deDE', 'Kevin Horng', NULL, NULL, NULL, 23222), -- 70266
(70265, 'deDE', 'Dan Mei', NULL, NULL, NULL, 23222), -- 70265
(55184, 'deDE', 'Sandsack', NULL, NULL, NULL, 23222), -- 55184
(55183, 'deDE', 'Sandsack', NULL, NULL, NULL, 23222), -- 55183
(55199, 'deDE', 'Ausbilder von Tian', NULL, NULL, NULL, 23222), -- 55199
(55198, 'deDE', 'Schüler von Tian', 'Schülerin von Tian', NULL, NULL, 23222), -- 55198
(54917, 'deDE', 'Ausbilder Xann', NULL, NULL, NULL, 23222), -- 54917
(56401, 'deDE', 'Grünsteinknabberer', NULL, NULL, NULL, 23222), -- 56401
(1860, 'deDE', 'Leerwandler', NULL, NULL, NULL, 23222), -- 1860
(54930, 'deDE', 'Grünholzdieb', NULL, NULL, NULL, 23222), -- 54930
(59103, 'deDE', 'Waldjungtier', NULL, NULL, NULL, 23222), -- 59103
(59102, 'deDE', 'Waldjägerin', NULL, NULL, NULL, 23222), -- 59102
(61586, 'deDE', 'Grünsteinbergarbeiter', 'Grünsteinbergarbeiterin', NULL, NULL, 23222), -- 61586
(56563, 'deDE', 'Jademinenankunftauslöser', NULL, NULL, NULL, 23222), -- 56563
(65092, 'deDE', 'Schmelzmeisterin Aschtatze', NULL, 'Bergbaulehrerin', NULL, 23222), -- 65092
(61622, 'deDE', 'Jade Mines Cave-In Bunny', NULL, NULL, NULL, 23222), -- 61622
(56464, 'deDE', 'Grünsteinbergarbeiter', 'Grünsteinbergarbeiterin', NULL, 'questinteract', 23222), -- 56464
(56404, 'deDE', 'Grünsteinverschlinger', NULL, NULL, NULL, 23222), -- 56404
(56349, 'deDE', 'Schelmischer Waldgeist', NULL, NULL, NULL, 23222), -- 56349
(56543, 'deDE', 'Brüchiger Grünsteinverschlinger', NULL, NULL, NULL, 23222), -- 56543
(3127, 'deDE', 'Siechstachelskorpid', NULL, NULL, NULL, 23222), -- 3127
(56508, 'deDE', 'Jadekarren', NULL, NULL, NULL, 23222), -- 56508
(56510, 'deDE', 'Hao Mann', NULL, NULL, NULL, 23222), -- 56510
(69946, 'deDE', 'Hutia', NULL, NULL, NULL, 23222), -- 69946
(59772, 'deDE', 'Rauchiges Stachelschwein', NULL, NULL, NULL, 23222), -- 59772
(61700, 'deDE', 'Jadekarren', NULL, NULL, NULL, 23222), -- 61700
(64359, 'deDE', 'Salzschuppenkarpfen', NULL, NULL, NULL, 23222), -- 64359
(64366, 'deDE', 'Der lächelnde Waiyu', NULL, 'Meister der Schuppenfaust', NULL, 23222), -- 64366
(64326, 'deDE', 'Anglerin', NULL, NULL, NULL, 23222), -- 64326
(64324, 'deDE', 'Fischer der Anglerexpedition', NULL, NULL, NULL, 23222), -- 64324
(64356, 'deDE', 'Blauwasserhai', NULL, NULL, NULL, 23222), -- 64356
(59788, 'deDE', 'Geisterflitzer', NULL, NULL, NULL, 23222), -- 59788
(55438, 'deDE', 'Verstoßener Waldgeist', NULL, NULL, NULL, 23222), -- 55438
(55288, 'deDE', 'Steingebundener Zerstörer', NULL, NULL, NULL, 23222), -- 55288
(55279, 'deDE', 'Shan''ze-Geisterbinder', NULL, NULL, NULL, 23222), -- 55279
(58065, 'deDE', 'General Purpose Bunny (DLA)', NULL, NULL, NULL, 23222), -- 58065
(55290, 'deDE', 'Verirrter Urahne', NULL, NULL, NULL, 23222), -- 55290
(55455, 'deDE', 'General Purpose Bunny (DLA)', NULL, NULL, NULL, 23222), -- 55455
(55489, 'deDE', 'Uralter Geist', NULL, NULL, NULL, 23222), -- 55489
(59492, 'deDE', 'Pei-Zhi', NULL, NULL, NULL, 23222), -- 59492
(56014, 'deDE', 'Tan Chao Bunny', NULL, NULL, NULL, 23222), -- 56014
(65794, 'deDE', 'Verirrter Urahne', NULL, NULL, NULL, 23222), -- 65794
(56181, 'deDE', 'Shan''ze-Geisterklaue', NULL, NULL, NULL, 23222), -- 56181
(65743, 'deDE', 'General Purpose Bunny (Gigantic AOI) (DLA)', NULL, NULL, NULL, 23222), -- 65743
(55291, 'deDE', 'Shan''ze-Geisterklaue', NULL, NULL, NULL, 23222), -- 55291
(61120, 'deDE', 'Wunschfrosch', NULL, NULL, NULL, 23222), -- 61120
(61125, 'deDE', 'Coin Bunny', NULL, NULL, NULL, 23222), -- 61125
(65779, 'deDE', 'Shan''ze-Geisterklaue', NULL, NULL, NULL, 23222), -- 65779
(55086, 'deDE', 'Kochender Kessel', NULL, NULL, NULL, 23222), -- 55086
(55092, 'deDE', 'Mutter Wu', NULL, NULL, NULL, 23222), -- 55092
(54987, 'deDE', 'Grünholztrickster', NULL, NULL, NULL, 23222), -- 54987
(55238, 'deDE', 'Wachsgehölzmatriarchin', NULL, NULL, NULL, 23222), -- 55238
(54988, 'deDE', 'Wachsgehölzjäger', NULL, NULL, NULL, 23222), -- 54988
(66840, 'deDE', 'Brew bunny', NULL, NULL, NULL, 23222), -- 66840
(56199, 'deDE', 'Leichtschwingenjäger', NULL, NULL, NULL, 23222), -- 56199
(56198, 'deDE', 'Dickichtpirscher', NULL, NULL, NULL, 23222), -- 56198
(66742, 'deDE', 'Thunder Hold Fire Effects Bunny - Scorched Ground', NULL, NULL, NULL, 23222),
(64868, 'deDE', 'Kor''kron Dubs', NULL, NULL, NULL, 23222), -- 64868
(66915, 'deDE', 'Elitesoldat der Kor''kron', NULL, NULL, NULL, 23222), -- 66915
(62167, 'deDE', 'General Purpose Bunny ZTO', NULL, NULL, NULL, 23222), -- 62167
(66554, 'deDE', 'Barrikade der Allianz', NULL, NULL, NULL, 23222), -- 66554
(66648, 'deDE', 'Leutnant der Donnerwehr', NULL, NULL, NULL, 23222), -- 66648
(66651, 'deDE', 'Arbeiter der Donnerwehr', NULL, NULL, NULL, 23222), -- 66651
(66654, 'deDE', 'Vorräte der Donnerwehr', NULL, NULL, NULL, 23222), -- 66654
(66647, 'deDE', 'Scharfschütze der Donnerwehr', 'Scharfschützin der Donnerwehr', NULL, NULL, 23222), -- 66647
(66336, 'deDE', 'Thunder Hold Fire Effects Bunny', NULL, NULL, NULL, 23222), -- 66336
(66426, 'deDE', 'Sha-Herold', NULL, NULL, NULL, 23222), -- 66426
(66425, 'deDE', 'Sha-Heimsuchung', NULL, NULL, NULL, 23222), -- 66425
(66650, 'deDE', 'Infanterist der Donnerwehr', 'Infanteristin der Donnerwehr', NULL, NULL, 23222), -- 66650
(66649, 'deDE', 'Heilerin der Donnerwehr', NULL, NULL, NULL, 23222), -- 66649
(66948, 'deDE', 'Verzerrter Leichnam', NULL, NULL, NULL, 23222), -- 66948
(66928, 'deDE', 'Sha-Speier', NULL, NULL, NULL, 23222), -- 66928
(58811, 'deDE', 'Jadeschnappschildkröte', NULL, NULL, NULL, 23222), -- 58811
(66857, 'deDE', 'Parachute Bunny', NULL, NULL, NULL, 23222), -- 66857
(66659, 'deDE', 'Sue-Ji die Pflegerin', NULL, NULL, NULL, 23222), -- 66659
(66477, 'deDE', 'Thunder Hold Cannon Fire Effects Bunny', NULL, NULL, NULL, 23222), -- 66477
(66217, 'deDE', 'Siat Honigtau', NULL, NULL, NULL, 23222), -- 66217
(66307, 'deDE', 'Flame Bunny', NULL, NULL, NULL, 23222), -- 66307
(66214, 'deDE', 'Verängstigter Feldarbeiter', 'Verängstigte Feldarbeiterin', NULL, NULL, 23222), -- 66214
(29238, 'deDE', 'Spukgestalt der Geißel', NULL, NULL, NULL, 23222), -- 29238
(66218, 'deDE', 'Lo Don', NULL, NULL, NULL, 23222), -- 66218
(66665, 'deDE', 'Taran Zhu', NULL, 'Meister der Shado-Pan', NULL, 23222), -- 66665
(66265, 'deDE', 'Lu Jon Sun', NULL, NULL, NULL, 23222), -- 66265
(66237, 'deDE', 'Kan der Geistvolle', NULL, NULL, NULL, 23222), -- 66237
(66235, 'deDE', 'Feldhüter Mao', NULL, NULL, NULL, 23222), -- 66235
(66230, 'deDE', 'Su Mi', NULL, 'Stallmeisterin', NULL, 23222), -- 66230
(66228, 'deDE', 'Tau Be', NULL, NULL, NULL, 23222), -- 66228
(66223, 'deDE', 'Älteste Honigtatze', NULL, 'Handwerkswaren', NULL, 23222), -- 66223
(66220, 'deDE', 'Bürgermeister Honigtau', NULL, NULL, NULL, 23222), -- 66220
(66236, 'deDE', 'Braumutter Kiki', NULL, 'Gastwirtin', 'innkeeper', 23222), -- 66236
(66238, 'deDE', 'Herim Woo', NULL, 'Gemischtwaren', NULL, 23222), -- 66238
(66219, 'deDE', 'Kofa der Flinke', NULL, 'Pfeilmacher', 'repairnpc', 23222), -- 66219
(67150, 'deDE', 'Dorfwache von Honigtau', NULL, NULL, NULL, 23222), -- 67150
(66430, 'deDE', 'Ethan Grabesstamm', NULL, 'Magier der Horde', NULL, 23222), -- 66430
(66979, 'deDE', 'Steinbrecher Ruian', NULL, 'Bergbaulehrer', NULL, 23222), -- 66979
(66347, 'deDE', 'Schwarzkrallenagentin', NULL, 'Diener des Schwarzen Prinzen', NULL, 23222), -- 66347
(66234, 'deDE', 'Mitsua', NULL, NULL, NULL, 23222), -- 66234
(66233, 'deDE', 'Pio', NULL, NULL, NULL, 23222), -- 66233
(66225, 'deDE', 'Koch Tsu', NULL, NULL, NULL, 23222), -- 66225
(66232, 'deDE', 'Puya', NULL, NULL, NULL, 23222), -- 66232
(66346, 'deDE', 'Schwarzkrallenwächter', NULL, 'Diener des Schwarzen Prinzen', NULL, 23222), -- 66346
(66981, 'deDE', 'Fallensteller Ri', NULL, 'Kürschnerlehrer', NULL, 23222), -- 66981
(66980, 'deDE', 'Kräutergärtnerin Miao', NULL, 'Kräuterkundelehrerin', NULL, 23222), -- 66980
(66222, 'deDE', 'Ältester Muur', NULL, 'Lehrer für Erste Hilfe', NULL, 23222), -- 66222
(66239, 'deDE', 'Aimee Morgenbrise', NULL, NULL, NULL, 23222), -- 66239
(66227, 'deDE', 'Wing Hya', NULL, 'Flugmeisterin', 'taxi', 23222), -- 66227
(66231, 'deDE', 'Sona Morgenbrise', NULL, NULL, NULL, 23222), -- 66231
(66668, 'deDE', 'Sha-verseuchter Pirscher', NULL, NULL, NULL, 23222), -- 66668
(55509, 'deDE', 'Konk', NULL, NULL, NULL, 23222), -- 55509
(62545, 'deDE', 'Großes Feuer', NULL, NULL, NULL, 23222), -- 62545
(66916, 'deDE', 'Ho-zen-Schaukler', NULL, NULL, NULL, 23222), -- 66916
(67016, 'deDE', 'Torso einer uralten Statue', NULL, NULL, NULL, 23222), -- 67016
(66917, 'deDE', 'Ho-zen-Wutschäumer', NULL, NULL, NULL, 23222), -- 66917
(66911, 'deDE', 'Lorbu Trauerspruch', NULL, NULL, NULL, 23222), -- 66911
(61535, 'deDE', 'Toter Ho-zen', NULL, NULL, NULL, 23222), -- 61535
(55463, 'deDE', 'Ho-zen-Leichenhügel', NULL, NULL, NULL, 23222), -- 55463
(55464, 'deDE', 'Toter Ho-zen', NULL, NULL, NULL, 23222), -- 55464
(55379, 'deDE', 'Torso Kill Credit Bunny', NULL, NULL, NULL, 23222), -- 55379
(67017, 'deDE', 'Arm einer uralten Statue', NULL, NULL, NULL, 23222), -- 67017
(61507, 'deDE', 'Zin''Juns linke Hand', NULL, NULL, NULL, 23222), -- 61507
(55403, 'deDE', 'Zin''Jun', NULL, NULL, NULL, 23222), -- 55403
(55404, 'deDE', 'Zin''Juns Augenhöhle', NULL, NULL, NULL, 23222), -- 55404
(63058, 'deDE', 'Quest - Jade Forest (Lay of the Land Kill Credit 01) - JSB', NULL, NULL, NULL, 23222), -- 63058
(67018, 'deDE', 'Kopf einer uralten Statue', NULL, NULL, NULL, 23222), -- 67018
(67019, 'deDE', 'Schulter einer uralten Statue', NULL, NULL, NULL, 23222), -- 67019
(55383, 'deDE', 'Arm Kill Credit Bunny', NULL, NULL, NULL, 23222), -- 55383
(55470, 'deDE', 'Ho-zen-Wutschäumer', NULL, NULL, NULL, 23222), -- 55470
(55388, 'deDE', 'Shoulder Kill Credit Bunny', NULL, NULL, NULL, 23222), -- 55388
(55392, 'deDE', 'Head Kill Credit Bunny', NULL, NULL, NULL, 23222), -- 55392
(29888, 'deDE', 'Kräuterkundeblume', NULL, NULL, NULL, 23222), -- 29888
(63467, 'deDE', 'Wütender Treant', NULL, NULL, NULL, 23222), -- 63467
(63420, 'deDE', 'SLG Generic MoP', NULL, NULL, NULL, 23222), -- 63420
(62930, 'deDE', 'Wasserwaldgeist', NULL, NULL, NULL, 23222), -- 62930
(58945, 'deDE', 'Ho-zen-Spenkspieker', NULL, NULL, NULL, 23222), -- 58945
(63326, 'deDE', 'Lehrensucher Cheech', NULL, NULL, NULL, 23222), -- 63326
(63323, 'deDE', 'Lehrensucherin Lola', NULL, NULL, NULL, 23222), -- 63323
(63321, 'deDE', 'Lehrensucher Chang', NULL, NULL, NULL, 23222), -- 63321
(63320, 'deDE', 'Nebel', NULL, NULL, NULL, 23222), -- 63320
(63324, 'deDE', 'Lehrensucher Chong', NULL, NULL, NULL, 23222), -- 63324
(63322, 'deDE', 'Lehrensucherin Chi Chi', NULL, NULL, NULL, 23222), -- 63322
(62536, 'deDE', 'Ho-zen-Schaukler', NULL, NULL, NULL, 23222), -- 62536
(62537, 'deDE', 'Ho-zen-Schaukler', NULL, NULL, NULL, 23222), -- 62537
(56272, 'deDE', 'Waldpirscher', NULL, NULL, NULL, 23222), -- 56272
(63059, 'deDE', 'Quest - Jade Forest (Lay of the Land Kill Credit 02) - JSB', NULL, NULL, NULL, 23222), -- 63059
(56183, 'deDE', 'Vollgefressener Krokilisk', NULL, NULL, NULL, 23222), -- 56183
(56180, 'deDE', 'Bachsaibling', NULL, NULL, 'questinteract', 23222), -- 56180
(58979, 'deDE', 'Quest Sparkles on Pottery', 'Quest - Jade Forest (One Hand Clapping) - JLR', NULL, NULL, 23222), -- 58979
(59787, 'deDE', 'Morgenkranich', NULL, NULL, NULL, 23222), -- 59787
(56242, 'deDE', 'Ho-zen-Zischer', NULL, NULL, NULL, 23222), -- 56242
(56240, 'deDE', 'Ho-zen-Tierhetzer', NULL, NULL, NULL, 23222), -- 56240
(63756, 'deDE', 'Eingefangener Waldpirscher', NULL, NULL, NULL, 23222), -- 63756
(68027, 'deDE', 'Finstere Nadel', NULL, NULL, NULL, 23222), -- 68027
(68026, 'deDE', 'Gipfelgeist', NULL, NULL, NULL, 23222), -- 68026
(55555, 'deDE', 'Ho-zen-Schlickschleuderer', NULL, NULL, NULL, 23222), -- 55555
(64523, 'deDE', 'Luftangriff', NULL, NULL, NULL, 23222), -- 64523
(28960, 'deDE', 'Totally Generic Bunny (JSB)', NULL, NULL, NULL, 23222), -- 28960
(59796, 'deDE', 'Blaurückenbiber', NULL, NULL, NULL, 23222), -- 59796
(63782, 'deDE', 'Kampferprobter Fallschirmjäger', NULL, NULL, NULL, 23222), -- 63782
(63742, 'deDE', 'Stampfer vom Flotschhügel', NULL, NULL, NULL, 23222), -- 63742
(56283, 'deDE', 'Tigerfliege', NULL, NULL, NULL, 23222), -- 56283
(66717, 'deDE', 'Bestienaufpasser Chi Chi', NULL, 'Stallmeister', NULL, 23222), -- 66717
(56289, 'deDE', 'Strandläufer der Jinyu', NULL, NULL, NULL, 23222), -- 56289
(63809, 'deDE', 'Jack Arrow', NULL, 'Kapitän der Allianz', NULL, 23222), -- 63809
(63768, 'deDE', 'Flare Bunny', NULL, NULL, NULL, 23222), -- 63768
(62975, 'deDE', 'Betthaber vom Flotschhügel', NULL, NULL, NULL, 23222), -- 62975
(25721, 'deDE', 'Arkane Schlange', NULL, NULL, NULL, 23222), -- 25721
(50586, 'deDE', 'Maderich', NULL, NULL, NULL, 23222), -- 50586
(67149, 'deDE', 'Kundschafter vom Flotschhügel', NULL, NULL, NULL, 23222), -- 67149
(56286, 'deDE', 'Eingefangener Waldpirscher', NULL, NULL, NULL, 23222), -- 56286
(59713, 'deDE', 'Hetzer vom Flotschhügel', NULL, NULL, NULL, 23222), -- 59713
(56315, 'deDE', 'Jinyugefangener', NULL, NULL, NULL, 23222), -- 56315
(59720, 'deDE', 'Ho-zen-Schaukel', NULL, NULL, NULL, 23222), -- 59720
(59730, 'deDE', 'Dreckschnipser vom Flotschhügel', NULL, NULL, NULL, 23222), -- 59730
(56583, 'deDE', 'Dynamite Fishing Event Bunny ZTO', NULL, NULL, NULL, 23222), -- 56583
(56358, 'deDE', 'Kritsche Kratsche', NULL, NULL, NULL, 23222), -- 56358
(56309, 'deDE', 'Wildschwanz vom Flotschhügel', NULL, NULL, NULL, 23222), -- 56309
(58996, 'deDE', 'Fliegen auf dem Flotschhügel', 'Quest - Jade Forest (Fort Grookin Flavor) - JLR', NULL, NULL, 23222), -- 58996
(56264, 'deDE', 'Klatscher vom Flotschhügel', NULL, NULL, NULL, 23222), -- 56264
(56447, 'deDE', 'Schlammschnapper', NULL, NULL, NULL, 23222), -- 56447
(56509, 'deDE', 'Allianzlager', NULL, NULL, NULL, 23222), -- 56509
(59015, 'deDE', 'Dynamite Fishing Bunny ZTO/JLR', NULL, NULL, NULL, 23222), -- 59015
(56396, 'deDE', 'Hungrige Blutkralle', NULL, NULL, NULL, 23222), -- 56396
(58943, 'deDE', 'Ho-zen-Magenfetzer', NULL, NULL, NULL, 23222), -- 58943
(63764, 'deDE', 'Kampferprobter Fallschirmjäger', NULL, NULL, NULL, 23222), -- 63764
(58895, 'deDE', 'Sonnenweidenungetüm', NULL, NULL, NULL, 23222); -- 58895


DELETE FROM `page_text_locale` WHERE (`ID`=4524 AND `locale`='deDE');
INSERT INTO `page_text_locale` (`ID`, `locale`, `Text`, `VerifiedBuild`) VALUES
(4524, 'deDE', 'In den Zeiten der Mogudynastien waren Sklaven die Lebensader des Reiches. Pandaren, Ho-zen und Jinyu arbeiteten auf den Feldern, gruben in den Bergwerken und bauten mächtige Festungen für ihre Meister.$b$bUm die Müdigkeit zu bekämpfen, die Arbeitsmoral aufrechtzuerhalten und Verletzte zurück zur Arbeit zu bringen, erlaubten die Mogu einer Kaste der Pandaren, Heilmittel zu brauen. Einfache Tees und Umschläge wurden zuerst hergestellt. Im Laufe der Jahre wurden diese Spezialisten zu Heilern, Anführern und Braumeistern.$b$bEine edle Tradition war geboren und diese frühen "Mönche" wurden zu Symbolen der Hoffnung und des Stolzes unter den Pandaren.$b$bEs waren diese Helden, die zuerst erlernten, ohne Waffen zu kämpfen. Heimlich brachten die Mönche den anderen Sklaven die Geheimnisse der Kampfkunst bei. Als die Revolution begann, zogen die Mönche als Erste in den Kampf und ermutigten einfache Bauern, Schmiede und Steinmetze, ihnen zu folgen...', 23222); -- 4524


INSERT IGNORE INTO `page_text` (`ID`, `Text`, `NextPageID`, `PlayerConditionID`, `Flags`, `VerifiedBuild`) VALUES
(4524, 'In den Zeiten der Mogudynastien waren Sklaven die Lebensader des Reiches. Pandaren, Ho-zen und Jinyu arbeiteten auf den Feldern, gruben in den Bergwerken und bauten mächtige Festungen für ihre Meister.$b$bUm die Müdigkeit zu bekämpfen, die Arbeitsmoral aufrechtzuerhalten und Verletzte zurück zur Arbeit zu bringen, erlaubten die Mogu einer Kaste der Pandaren, Heilmittel zu brauen. Einfache Tees und Umschläge wurden zuerst hergestellt. Im Laufe der Jahre wurden diese Spezialisten zu Heilern, Anführern und Braumeistern.$b$bEine edle Tradition war geboren und diese frühen "Mönche" wurden zu Symbolen der Hoffnung und des Stolzes unter den Pandaren.$b$bEs waren diese Helden, die zuerst erlernten, ohne Waffen zu kämpfen. Heimlich brachten die Mönche den anderen Sklaven die Geheimnisse der Kampfkunst bei. Als die Revolution begann, zogen die Mönche als Erste in den Kampf und ermutigten einfache Bauern, Schmiede und Steinmetze, ihnen zu folgen...', 0, 0, 0, 23222); -- 4524


INSERT IGNORE INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `OptionBroadcastTextID`, `action_poi_id`, `box_coded`, `box_money`, `box_text`, `BoxBroadcastTextID`) VALUES
(13530, 1, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13282, 0, 1, 'Ich möchte ein wenig Eure Ware betrachten.', 0, 0, 0, 0, '', 0),
(9868, 1, 1, 'Ich möchte ein wenig in Euren Waren stöbern.', 0, 0, 0, 0, '', 0),
(9868, 0, 5, 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', 0, 0, 0, 0, '', 0),
(13374, 2, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13374, 1, 5, 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', 0, 0, 0, 0, '', 0),
(9821, 2, 0, 'Ich würde gern meine Kampfhaustiere heilen und wiederbeleben.', 0, 0, 0, 1000, 'Es wird eine kleine Gebühr für die medizinische Hilfe erhoben.', 0),
(9821, 1, 1, 'Ich suche nach einem verlorenen Gefährten.', 0, 0, 0, 0, '', 0),
(14629, 0, 3, 'Unterrichtet mich in der Verzauberkunst.', 0, 0, 0, 0, '', 0),
(14625, 0, 3, 'Unterrichtet mich in der Juwelierskunst.', 0, 0, 0, 0, '', 0),
(14626, 1, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(14626, 0, 3, 'Unterrichtet mich in der Schmiedekunst.', 0, 0, 0, 0, '', 0),
(14649, 8, 25, 'Warteschlange für Grünstein.', 0, 0, 0, 0, '', 0),
(10188, 0, 1, 'Ich sehe mich nur mal um.', 0, 0, 0, 0, '', 0),
(8903, 0, 5, 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', 0, 0, 0, 0, '', 0),
(14432, 0, 1, 'Ich möchte ein wenig Eure Ware betrachten.', 0, 0, 0, 0, '', 0),
(14433, 0, 1, 'Ich möchte ein wenig Eure Ware betrachten.', 0, 0, 0, 0, '', 0),
(83, 0, 4, 'Bringt mich ins Leben zurück.', 0, 0, 0, 0, '', 0),
(13580, 1, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13580, 0, 25, 'Warteschlange für Ein Sturm braut sich zusammen.', 0, 0, 0, 0, '', 0),
(13105, 0, 0, 'Was ist das hier für ein Ort?', 0, 0, 0, 0, '', 0),
(14624, 0, 3, 'Unterrichtet mich in Bergbau.', 0, 0, 0, 0, '', 0);


-- unknown broadcasttextid
INSERT IGNORE INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `comment`) VALUES
(54989, 0, 0, '$gUnverschämter kleiner:Unverschämte kleine:r; $R!', 12, 0, 100, 0, 0, 0, 0, 'Gormalisklavenhändler to Player'),
(55470, 0, 0, 'Stampft die nappeligen Nappel von weit weg in Boden!', 12, 0, 100, 0, 0, 0, 0, 'Ho-zen-Wutschäumer to Player'),
(56349, 0, 0, 'Meiner!', 12, 0, 100, 0, 0, 0, 0, 'Schelmischer Waldgeist to Schelmischer Waldgeist'),
(56349, 1, 0, 'Gefangen!', 12, 0, 100, 0, 0, 0, 0, 'Schelmischer Waldgeist'),
(56444, 0, 0, 'Am Ende herrscht nur noch die Stille.', 12, 0, 100, 0, 0, 0, 0, 'Gepeinigter Geist to Player'),
(56444, 1, 0, 'Ein würdiges Opfer für unseren Meister.', 12, 0, 100, 0, 0, 0, 0, 'Gepeinigter Geist to Player'),
(56444, 2, 0, 'Die Lebenden sind hier nicht willkommen!', 12, 0, 100, 0, 0, 0, 0, 'Gepeinigter Geist to Player'),
(56464, 0, 0, 'Dankeschööön!', 12, 0, 100, 0, 0, 0, 0, 'Grünsteinbergarbeiterin to Player'),
(56464, 1, 0, 'Grr... Komm'' nur näher, Mistviech. Ich beiß dir die Beine ab.', 12, 0, 100, 0, 0, 0, 0, 'Grünsteinbergarbeiterin to Grünsteinknabberer'),
(57313, 0, 0, 'Willkommen in meinem Gasthaus. Wir haben warme Mahlzeiten und weiche Betten.', 12, 0, 100, 0, 0, 0, 0, 'Fela Holzohr to Player'),
(59348, 0, 0, '$R, Ihr seid hier sehr willkommen. Karasshi der Draufgänger hat schon von Euren Heldentaten berichtet.', 12, 0, 100, 6, 0, 0, 0, 'Bewohner des Perlflossendorfes to Player'),
(59348, 1, 0, 'Ihr müsst $gder:die:r; $R sein, $gder:die; Karasshi den Draufgänger und unsere Brüder befreit hat! Wir schulden Euch Dank.', 12, 0, 100, 92, 0, 0, 0, 'Bewohner des Perlflossendorfes to Player'),
(62323, 0, 0, 'Wie geht''s den Nudeln heute?', 12, 0, 100, 0, 0, 0, 0, 'La die Sanfte'),
(63742, 0, 0, 'Knatz gebrammt!', 12, 0, 100, 53, 0, 0, 0, 'Stampfer vom Flotschhügel to Kampferprobter Fallschirmjäger'),
(63742, 1, 0, 'Werd geflotscht, Knatzel!', 12, 0, 100, 53, 0, 0, 0, 'Stampfer vom Flotschhügel to Kampferprobter Fallschirmjäger'),
(65923, 0, 0, 'Ich hoffe, dass Ihr Recht habt. Ich hoffe es sehr.', 12, 0, 100, 0, 0, 0, 0, 'Ju Apfelblüte to Ju Apfelblüte'),
(66235, 0, 0, 'Meine Kinder werden den Krieg nicht kennenlernen, dafür sorge ich!', 12, 0, 100, 0, 0, 0, 0, 'Feldhüter Mao to Player');