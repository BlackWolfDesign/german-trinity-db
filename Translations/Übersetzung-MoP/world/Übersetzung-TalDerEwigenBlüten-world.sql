-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_0_23222
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/13/2017 16:27:35


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=35318 AND `locale`='deDE') OR (`ID`=43242 AND `locale`='deDE') OR (`ID`=43245 AND `locale`='deDE') OR (`ID`=43287 AND `locale`='deDE') OR (`ID`=43297 AND `locale`='deDE') OR (`ID`=43285 AND `locale`='deDE') OR (`ID`=43286 AND `locale`='deDE') OR (`ID`=43296 AND `locale`='deDE') OR (`ID`=43288 AND `locale`='deDE') OR (`ID`=43290 AND `locale`='deDE') OR (`ID`=43289 AND `locale`='deDE') OR (`ID`=43282 AND `locale`='deDE') OR (`ID`=44911 AND `locale`='deDE') OR (`ID`=44910 AND `locale`='deDE') OR (`ID`=44909 AND `locale`='deDE') OR (`ID`=44908 AND `locale`='deDE') OR (`ID`=44891 AND `locale`='deDE') OR (`ID`=43303 AND `locale`='deDE') OR (`ID`=43179 AND `locale`='deDE') OR (`ID`=42422 AND `locale`='deDE') OR (`ID`=42421 AND `locale`='deDE') OR (`ID`=42420 AND `locale`='deDE') OR (`ID`=42234 AND `locale`='deDE') OR (`ID`=42233 AND `locale`='deDE') OR (`ID`=42170 AND `locale`='deDE') OR (`ID`=41761 AND `locale`='deDE') OR (`ID`=37479 AND `locale`='deDE') OR (`ID`=31889 AND `locale`='deDE') OR (`ID`=44543 AND `locale`='deDE') OR (`ID`=43806 AND `locale`='deDE') OR (`ID`=40519 AND `locale`='deDE') OR (`ID`=38421 AND `locale`='deDE') OR (`ID`=36095 AND `locale`='deDE') OR (`ID`=36220 AND `locale`='deDE') OR (`ID`=5841 AND `locale`='deDE') OR (`ID`=6134 AND `locale`='deDE') OR (`ID`=6984 AND `locale`='deDE') OR (`ID`=6662 AND `locale`='deDE') OR (`ID`=38299 AND `locale`='deDE') OR (`ID`=31902 AND `locale`='deDE') OR (`ID`=30457 AND `locale`='deDE') OR (`ID`=30675 AND `locale`='deDE') OR (`ID`=30344 AND `locale`='deDE') OR (`ID`=30701 AND `locale`='deDE') OR (`ID`=30352 AND `locale`='deDE') OR (`ID`=31613 AND `locale`='deDE') OR (`ID`=30567 AND `locale`='deDE') OR (`ID`=39068 AND `locale`='deDE') OR (`ID`=38345 AND `locale`='deDE') OR (`ID`=37179 AND `locale`='deDE') OR (`ID`=37161 AND `locale`='deDE') OR (`ID`=37160 AND `locale`='deDE') OR (`ID`=37151 AND `locale`='deDE') OR (`ID`=37150 AND `locale`='deDE') OR (`ID`=36995 AND `locale`='deDE') OR (`ID`=36984 AND `locale`='deDE') OR (`ID`=36973 AND `locale`='deDE') OR (`ID`=34448 AND `locale`='deDE') OR (`ID`=32838 AND `locale`='deDE') OR (`ID`=31087 AND `locale`='deDE') OR (`ID`=31023 AND `locale`='deDE') OR (`ID`=30095 AND `locale`='deDE') OR (`ID`=29456 AND `locale`='deDE') OR (`ID`=28882 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(35318, 'deDE', 'Mogu''shan Palace Reward Quest', '', '', '', '', '', '', '', '', 23222),
(43242, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23222),
(43245, 'deDE', 'Invasion: Westfall', '', '', '', '', '', '', '', '', 23222),
(43287, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23222),
(43297, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23222),
(43285, 'deDE', 'Invasion: Vorgebirge des Hügellands', '', '', '', '', '', '', '', '', 23222),
(43286, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23222),
(43296, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23222),
(43288, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23222),
(43290, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23222),
(43289, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23222),
(43282, 'deDE', 'Invasion: Nördliches Brachland', '', '', '', '', '', '', '', '', 23222),
(44911, 'deDE', 'Unbenutzt', '', '', '', '', '', '', '', '', 23222),
(44910, 'deDE', 'Unbenutzt', '', '', '', '', '', '', '', '', 23222),
(44909, 'deDE', 'Wöchentliches gewertetes Schlachtfeld', '', '', '', '', '', '', '', '', 23222),
(44908, 'deDE', 'Wöchentliche 3vs3-Quest', '', '', '', '', '', '', '', '', 23222),
(44891, 'deDE', 'Wöchentliche 2vs2-Quest', '', '', '', '', '', '', '', '', 23222),
(43303, 'deDE', 'Reif für Randale!', 'Sprecht mit David Globetrotter und findet heraus, wie man an der Rabenwehrrandale teilnimmt.', 'Willkommen zur Rabenwehrrandale! Es haben schon viele Kämpfer in der Arena der Rabenwehr teilgenommen, um ihre Stärke unter Beweis zu stellen, und jetzt seid Ihr an der Reihe.', '', '', '', '', '', '', 23222),
(43179, 'deDE', 'Die Kirin Tor von Dalaran', 'Schließt 4 Weltquests der Kirin Tor ab.', 'Unterstützt die Kirin Tor von Dalaran, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Kriegsmagier Silva in Dalaran zurück.', 23222),
(42422, 'deDE', 'Die Wächterinnen', 'Schließt 4 Weltquests der Wächterinnen ab.', 'Unterstützt die Wächterinnen, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Marin Klingenflügel auf der Insel der Behüter zurück.', 23222),
(42421, 'deDE', 'Die Nachtsüchtigen', 'Schließt beliebige 4 Weltquests in Suramar ab.', 'Unterstützt die Nachtsüchtigen von Suramar, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zur Ersten Arkanistin Thalyssra in Suramar zurück.', 23222),
(42420, 'deDE', 'Farondis'' Hofstaat', 'Schließt beliebige 4 Weltquests in Azsuna ab.', 'Unterstützt Farondis'' Hofstaat in Azsuna, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Veridis Fallon in Azsuna zurück.', 23222),
(42234, 'deDE', 'Die Valarjar', 'Schließt 4 beliebige Weltquests in Sturmheim ab.', 'Unterstützt die Vrykul von Sturmheim, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Valdemar Sturmsucher in Sturmheim zurück.', 23222),
(42233, 'deDE', 'Hochbergstämme', 'Schließt beliebige 4 Weltquests am Hochberg ab.', 'Unterstützt die Tauren vom Hochberg, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Ransa Graufeder in Donnertotem am Hochberg zurück.', 23222),
(42170, 'deDE', 'Die Traumweber', 'Schließt beliebige 4 Weltquests in Val''sharah ab.', 'Unterstützt die Druiden von Val''sharah, indem Ihr 4 Weltquests abschließt.', '', '', '', '', '', 'Kehrt zu Sylvio Hirschhorn in Val''sharah zurück.', 23222),
(41761, 'deDE', 'Zusammentreiben der Winterelche', 'Rettet 4 Talbuks auf Kuuros Hof.', 'Dieser Hof gehört meinem Vater Karmaan, und unser Viehbestand besteht größtenteils aus Talbuks. Ein Schotling muss das Tor ihres Pferchs geöffnet haben, denn sie sind alle entkommen!$B$BDie kleinen Mistviecher hätten sie direkt im Gehege töten können, doch sie jagen ihre Beute gerne. Ich befürchte, dass ich ihr nächstes Opfer sein werde, wenn ich noch länger hierbleibe!$B$BKönnt Ihr die Talbuks zusammentreiben und herbringen? Ich schaffe es nicht und mein Vater darf nichts davon erfahren!', '', '', '', '', '', '', 23222),
(37479, 'deDE', 'Bonusziel: Allianzanführer töten', '', '', '', '', '', '', '', '', 23222),
(31889, 'deDE', 'Kampfhaustierzähmer: Kalimdor', 'Besiegt Gluk den Verräter, Grazzel den Großen, Kela Grimmtotem, Zoltan und Elena Flatterflug in Haustierkämpfen.', 'Jetzt, da Ihr Gelegenheit hattet, Euren Fertigkeiten den letzten Schliff zu verleihen, steht Euch die Welt der Haustierkämpfe uneingeschränkt offen.$B$BIn Kalimdor gibt es fünf Zähmer, die Ihr für mich besiegen sollt: Gluk den Verräter in Feralas, Grazzel den Großen in den Düstermarschen, Kela Grimmtotem in Tausend Nadeln, Zoltan im Teufelswald und Elena Flatterflug in der Mondlichtung.$B$BWenn Ihr sie bezwingen könnt, seid Ihr bereit, voranzuschreiten.', '', '', '', 'Wow! Ihr seid wirklich beeindruckend.', 'Marlene Trichdie', '', 23222),
(44543, 'deDE', 'Die Schlacht um die Verheerte Küste', 'Nehmt den Windreiter zur Verheerten Küste.', 'Dieser Windreiter wird Euch zur Verheerten Küste bringen, $n. Möge Euch Eure Waffe heute viele Trophäen einbringen.\n\nDer Kriegshäuptling ist schon an Land gegangen. Ich denke, Ihr müsst einfach nur der Spur Dämonenblut folgen.', '', '', '', '', '', '', 23222),
(43806, 'deDE', 'Die Schlacht um die Verheerte Küste', 'Trefft Euch mit der dritten Flotte.', 'Die dritte Flotte ist bereits vor uns ausgelaufen, $n. Dieser Greif wird Euch dorthin bringen.\n\nViel Glück da draußen. Die Legion wird es uns bestimmt nicht leicht machen. Hoffen wir nur, dass König Varian die Lage im Griff hat.', '', '', '', '', '', '', 23222),
(40519, 'deDE', 'Die Rückkehr der Legion', 'Meldet Euch bei Musterungsoffizier Lee im Hafen von Sturmwind.', 'Helden der Allianz, ich schreibe Euch in Azeroths dunkelster Stunde. Die brennenden Armeen der Legion kehren aus dem Schlund der Hölle zurück.\n\nUnsere Flotte bricht unverzüglich zu den Verheerten Inseln auf. Wie eine große Lanze werden wir die Reihen der Legion durchstoßen und sie in den Nether zurückdrängen, aus dem sie hervorgekrochen sind.\n\nEuer Schiff wartet im Hafen von Sturmwind. Verliert keine Zeit.', '', '', '', '', '', '', 23222),
(38421, 'deDE', 'Garnisonskampagne: Ansturm auf Auchindoun', 'Trefft Euch mit Yrel bei Auchindoun in Talador.', 'Die Auchenai haben mir ein dringendes Schreiben geschickt: Auchindoun ist wieder in Gefahr! Die Sargerei haben erneut angegriffen und sind in die Kellergewölbe eingedrungen. Ich spüre, dass die Hand Gul''dans dahintersteckt.$b$bPackt Eure Sachen und trefft Euch schnellstens mit mir im Hof der Seelen westlich von Auchindoun!', '', '', '', '', '', 'Sprecht mit Yrel bei Auchindoun in Talador.', 23222),
(36095, 'deDE', 'Azuka Klingenwut', 'Kehrt zu Bodrick Grau in Eurer Garnison im Schattenmondtal zurück.', 'Agent Springschnell ist tot. Ein Feuer lodert tief in einer großen Wunde, direkt dort, wo einmal sein Herz war.$b$bNach dem Durchblättern eines Berichts, den Ihr bei seinem Leichnam gefunden habt, könnt Ihr Euch einige grundlegende Informationen zusammenreimen. Azuka hat die Oger betrogen, sie hat Ugbrecht getötet und seine Macht für sich selbst beansprucht.$b$bIhr solltet Bodrick diese Informationen sofort überbringen.', '', '', '', '', '', 'Kehrt zu Bodrick Grau in Eurer Garnison im Schattenmondtal zurück.', 23222),
(36220, 'deDE', 'Wie hart seid Ihr?', 'Tötet Lug''dol und zeigt seinen Kopf Kroggol der Wand vor dem Gorianischen Übungsgelände.', 'Mogor veranstaltet einen ganz besonderen Kampf drüben im Ring des Blutes, um den neuen Blutchampion zu krönen, und ich glaube, Ihr hättet das Zeug dazu!$B$BAllerdings respektieren diese Oger einzig und allein eins: brutale Härte! Der Eingang wird von einem großen Schläger bewacht, der dafür sorgt, dass nur die härtesten Gladiatoren zum Übungsgelände gelangen.$B$BFalls Ihr es ernst meint, dann besiegt den Gladiatorveteranen Lug''dol beim Turm die Straße hoch und bringt seinen Kopf zu Kroggol der Wand. Das sollte ihnen zeigen, aus welchem Holz Ihr geschnitzt seid.', '', '', 'Lug''dol', '', '', '', 23222),
(5841, 'deDE', 'Willkommen!', 'Bringt Yori Bruchhelm in Kharanos den Geschenkgutschein von Kharanos.', 'Willkommen bei World of Warcraft!$B$BAls besonderes Dankeschön dafür, dass Ihr die World of Warcraft Collector''s Edition erworben habt, erhaltet Ihr diesen Geschenkgutschein, den Ihr bei Yori Bruchhelm in Kharanos einlösen könnt. Dort erhaltet Ihr dann ein Geschenk: einen kleinen Gefährten, der Euch bei Eurer Suche nach Abenteuer und Ruhm begleiten wird.$B$BNochmals vielen Dank und genießt Euren Aufenthalt in World of Warcraft!', '', '', '', '', '', '', 23222),
(6134, 'deDE', 'Geisterplasmasuche', 'Bringt 8 Einheiten Geisterplasma und die Kiste mit den Geistermagneten zu Hornizz Brummsumms in Desolace.', 'Im Südosten liegt ein Tal, das Tal der Knochen, das von den Geistern der Magram heimgesucht wird. Klingt gruselig, nicht wahr? Stellt Euch mal vor, wie sich die Magram erst gruseln müssen! Wenn wir Geisterenergie an jenem Ort einfangen könnten, wäre das den Zentauren sicher einiges wert.$B$BHier, nehmt diese Kiste mit Geistermagneten. Wenn Ihr einen zwischen den beiden großen Skeletten im Tal aufstellt, den toten Goliaths, dann werden sich Geister materialisieren und dorthin gehen. Nehmt Abstand, schaltet die Geister aus und holt Euch ihr Geisterplasma - das verkaufen wir den Magram.', '', '', '', '', '', '', 23222),
(6984, 'deDE', 'Ein Dankeschön von Kokelwälder!', 'Sprecht mit Altvater Winter in Orgrimmar.', 'Wir von Kokelwälder sind dankbar für die Wiederbeschaffung der gestohlenen Leckereien, $n. Dafür möchten wir Euch ein besonderes Geschenk anbieten... präsentiert von niemand Geringerem als Altvater Winter persönlich!$B$BBitte, sprecht mit Altvater Winter und er wird Euch Euer Geschenk zum Winterhauchfest von uns hier bei Kokelwälder überreichen. Frisch von unserem Hof auf Euren Tisch - beste Kokelwälder Vollwertkost... dank Euch natürlich!', '', '', '', '', '', '', 23222),
(6662, 'deDE', 'Mein Bruder Nipsy...', 'Bringt den Karton mit eigenartigem Fleisch zu Nipsy im Tiefenbahndepot von Sturmwind.', 'Mein Bruder Nipsy hat ''n Rattengeschäft auf der anderen Seite der Tiefenbahn. Ihr müsst die Kiste mit Ratten hier zu ihm bringen, bevor sie schlecht werden. Zackig!$B$BIhr seid doch wohl schon mal mit ''ner Bahn gefahren, oder? Ganz einfach. Steigt einfach ein, wenn sie kommt, und genießt die Fahrt. Und haltet Eure Arme und Beine während der Fahrt drinnen, wenn Ihr keine Holzgräten haben wollt.', '', '', '', '', '', '', 23222),
(38299, 'deDE', 'Geschöpfe von Draenor', 'Besiegt Erris die Sammlerin in einem Haustierkampf.', 'Ich bin immer auf der Suche nach neuen Haustieren für meine Sammlung und hier in Draenor habe ich schon einige ungewöhnliche Haustiere ergattert. Was haltet Ihr von einem Haustierkampf? Wenn Ihr mein bescheidenes Team besiegt, lernen wir beide vielleicht ein paar neue Tricks.', '', '', '', '', '', '', 23222),
(31902, 'deDE', 'Kampfhaustierzähmer: Östliche Königreiche', 'Besiegt David Kosse, Deiza Seuchenhorn, Kortas Düsterhammer, Everessa und Durin Düsterhammer in Haustierkämpfen.', 'Jetzt, da Ihr Gelegenheit hattet, Euren Fertigkeiten den letzten Schliff zu verleihen, steht Euch die Welt der Haustierkämpfe uneingeschränkt offen.$B$BIn den Östlichen Königreichen gibt es fünf Zähmer, die Ihr für mich besiegen sollt: David Kosse im Hinterland, Deiza Seuchenhorn in den Östlichen Pestländern, Kortas Düsterhammer in der Sengenden Schlucht, Everessa in den Sümpfen des Elends und Kortas'' Bruder Durin Düsterhammer in der Brennenden Steppe.$B$BWenn Ihr sie bezwingen könnt, seid Ihr bereit für den nächsten Schritt.', '', '', '', 'Wow! Ihr seid wirklich beeindruckend.', 'Marlene Trichdie', '', 23222),
(30457, 'deDE', 'Raus mit dem Anführer', 'Tötet 10 Angehörige des Bataaristamms am Kun-Lai-Pass, bis der Feuerkrieger der Bataari auftaucht. Und dann tötet auch ihn.', 'Eure Hilfe für uns ist wirklich heldenhaft.$B$BBitte tötet den Bataari-Stamm am Kun-Lai-Pass im Nordwesten, bis der Feuerkrieger der Bataari wütend genug ist, um aufzutauchen. Und dann erledigt auch ihn.', '', 'Tötet 10 Mitglieder des Bataari-Stamms, um ihn zu provozieren. Und dann tötet auch ihn!', 'Feuerkrieger der Bataari', '', '', '', 23222),
(30675, 'deDE', 'Vergrabener Schatz der Ho-zen', 'Sucht den vergrabenen Schatz der Ho-zen.', 'Die Karte in Euren Händen ist zerknittert, schmutzig und schlecht gezeichnet, aber Ihr könnt ein großes X am Strand sehen.$b$bGibt es irgendwo auf dieser Insel einen vergrabenen Schatz?', '', '', '', '', '', '', 23222),
(30344, 'deDE', 'Die verlorene Dynastie', 'Besorgt 6 dynastische Tafeln.', 'Ich frage mich, ob ich Euch mit noch einer Aufgabe belästigen könnte. Meine Tochter, die gute Kommandantin der Schildwachen Lyalia hier, verbietet mir, ins Feld zu ziehen.$b$bJeden Tag, den ich in dieser Oubliette gefangen war, war ich gezwungen, auf die Ruinen im Norden zu starren. Es reizt mich, sie zu erkunden.$b$bWenn wir dort sind, wo ich vermute, dass wir sind, könnten diese Ruinen der Schlüssel sein, den wir benötigen, um die sagenhaften Teiche der Jugend zu finden!$b$BBitte, $C! Geht zu diesen Ruinen und bergt alle Tafeln, die noch Schrift aufweisen.', '', '', '', '', '', '', 23222),
(30701, 'deDE', 'Zwingzangensuppe', 'Tötet Zwingzangenfischer am Strand und kehrt mit 16 Augen für eine leckere Suppe zurück.', 'Nichts schmeckt so gut wie Zwingzangensuppe. Das ist eine Krasarangspezialität!$b$bIch wollte mir gerade eine machen, als ich bemerkte, dass eine Hauptzutat fehlt: die Augen!$b$bIhr müsst die Zwingzangenfischer am Strand für mich töten und mir ihre Augen für die Suppe bringen!', '', '', '', '', '', '', 23222),
(30352, 'deDE', 'Kranich-Meisterschaft', 'Tötet 12 Karpfenjäger.', 'Die Sache bei der Jagd auf Vögel ist die, dass man Munition braucht - und ich habe keine. Die Sache bei der Jagd mit Nesingwary ist, dass man beweisen muss, dass man in Abschüssen und Prahlerei nicht nachlässt.$b$bKeine Jagd, keine Prahlerei. Und ich mag Prahlerei, also ist dies eine schlechte Situation.$b$bKann ich Euch anheuern, ein paar Karpfenjäger für mich zu töten? Ihr findet sie rauf und runter am Fluss westlich von hier. Es sind ziemlich viele, also jagt nebenher ruhig ein wenig.', '', '', '', '', '', '', 23222),
(31613, 'deDE', 'Brisantes Grünsteinbräu', 'Helft bei der Verteidigung von Grünstein.', 'Ich arbeite an einer Ladung meines berühmten brisanten Grünsteinbräus.$b$bSollte sich hervorragend dafür eignen, mit den Strolchen und den schelmischen Elementaren aufzuräumen, die wir in letzter Zeit in der Nähe der Minen gesehen haben.$b$bIch sag Euch was, wie wär''s, wenn Ihr ein paar Eurer Freunde zusammentrommelt und sie herbringt, um etwas davon auszuprobieren?$b$bDie erste Runde geht aufs Haus!', '', '', '', '', '', '', 23222),
(30567, 'deDE', 'Blanches Hammerbräu', 'Schließt das Szenario Ein Sturm braut sich zusammen ab.', 'Riecht Ihr es in der Luft? Ein Sturm braut sich zusammen. Da braue ich doch gleich mit!$B$BKommt hoch auf den Hügel, wenn der Sturm da ist, dann brauen wir gemeinsam ein Fässchen meines berühmtesten Gebräus. Kann ich auf Eure Hilfe zählen?', '', '', '', '', '', '', 23222),
(39068, 'deDE', 'Vorherrschaft auf hoher See', 'Sammelt 2.500 Garnisonsressourcen, 2.500 Gold und schließt 25 Marineschatzmissionen ab.', 'Unsere Vorherrschaft auf hoher See besteht fort!$b$bWir benötigen allerdings noch mehr Erfahrung und Ressourcen, um diese Schiffswerft weiter aufzurüsten. Wenn wir unsere Ziele erreicht haben, können wir die Kapazität der Schiffswerft noch weiter erhöhen!', '', '', '', '', '', '', 23222),
(38345, 'deDE', 'Münzkunde', 'Bringt Krom Starkarm in Eisenschmiede fehlgeprägte draenische Münzen. Ihr könnt fehlgeprägte draenische Münzen beim Abschluss Eures ersten zufälligen heroischen Draenordungeons des Tages erhalten.', 'Ich hasse es, düstere Stimmung zu verbreiten, aber ich glaube nicht, dass wir Nolan in einem Stück finden. Oder in Einzelteilen.\n\nNolan war ein begeisterter Schatzjäger. Er reiste von einem Ende der Welt zum anderen auf der Suche nach seltenen Münzen. Es sieht ganz so aus, als hätte ihn die Jagd nach einigen gut erhaltenen draenischen Münzen das Leben gekostet. Hört zu: Ich zeige Euch, wie diese Münzen aussehen und Ihr bringt mir ein paar mehr davon, dann können wir ihm zu Ehren eine Ausstellung organisieren.', 'Bringt Krom Starkarm in Eisenschmiede fehlgeprägte draenische Münzen.', '', '', '', '', '', 23222),
(37179, 'deDE', 'Ein Herz für Kinder!', 'Holt einen eisernen Miniaturstern von der Oberen Schwarzfelsspitze.', 'Halt, wartet! Greift mich nicht an! Ich habe mich geändert!$B$BIm Ernst, ich habe ein neues Kapitel aufgeschlagen, versprochen. Ich habe schon monatelang nicht mehr "UNTERGAAAAAANG!" gesagt. Mich interessiert nur noch eins: die armen kleinen Waisen von Azeroth zu retten, die ihre Eltern in diesem tragischen Krieg verloren haben.$B$BDeshalb bin ich hier! Mit einem einzigen eisernen Miniaturstern könnte ich alle Waisen von Azeroth mit sauberem Wasser und Strom versorgen! Und niemand könnte mich dabei aufhalten!$B$BWas haltet Ihr davon, Zuckerschnäuzchen? Werdet Ihr dem alten Millhaus bei der Rettung der Waisen helfen?', '', '', '', '', '', '', 23222),
(37161, 'deDE', 'Familientraditionen', 'Findet Finkles verbesserten Kürschner an der Schwarzfelsspitze.', 'Er ist damit weggerannt! Was kann ich nur tun, was soll ich TUN?$B$BMein Sohn, und mein Messer, sind zur Schwarzfelsspitze aufgebrochen. Nun, mein Messer ist wohl eher mitgenommen worden. Darum geht es nicht!$B$BIhr müsst es zurückbringen! Ja, das Messer! Ihr müsst das Messer zurückbringen!$B$BGeht! Los! Hört auf zu schwatzen!', '', '', '', '', '', '', 23222),
(37160, 'deDE', 'Spalter!', 'Findet einen eisernen Gliedmaßenspalter im Grimmgleisdepot.', 'Ich, Gamon, bin durch das Dunkle Portal gekommen, um viele Orcschädel zu spalten. Doch meine Waffe konnte meiner Wut nicht standhalten und ist stumpf und schartig.$B$BFindet mir eine neue Waffe: eine, die sich dafür eignet, Höllschreis Glieder zu entfernen.', '', '', '', '', '', '', 23222),
(37151, 'deDE', 'Kalter Stahl', 'Findet eine Eiserne Autokanone im Grimmgleisdepot.', 'Ihr wollt wissen, was ich will? Dasselbe wie jeder, der durch das Portal geschritten ist. Jeder Soldat, der einer Welt entflohen ist, die ihn nicht verstehen konnte, ihn nicht gebraucht hat. Ich will, dass diese Welt uns so sehr liebt, wie wir sie lieben.$B$BDoch wir gehören dort einfach nicht hin, nicht mehr. Der Krieg ist ein Teil von uns, und wir können nicht mehr zurückkehren. Für alle dort sind wir entbehrlich.$B$BDas Einzige, was mich jetzt noch aufmuntert, ist das Gefühl einer Kanone aus kaltem Stahl in meinen Händen – direkt auf den Feind gerichtet.$B$B<Keeshan starrt in seinen Bierkrug.>', '', '', '', '', '', '', 23222),
(37150, 'deDE', 'Gut zu Vögeln', 'Sucht ein winziges Pfauenküken im Immergrünen Flor.', 'Hallo! Habt Ihr schon mal ein frisch geschlüpftes Pfauenküken gehalten?$b$b<Mylune ergreift Eure Hände ohne Vorwarnung und legt ein kleines zwitscherndes Vögelchen hinein.>$b$bSo klein und leicht und flaumig. Als würde man eine piepende kleine WOLKE mit Schnabel halten! Es ist so niedlich, da kann man nicht anders, als es zu umarmen und zu knuddeln und vorgekautes Essen in seinen klitzekleinen Mund zu stopfen.$b$b$n, die Pflanzenwesen des Immergrünen Flors machen die Pfauenküken zu Monstern, bevor sie aufwachsen und zu wunderschönen Schwänen werden können. Oder was auch immer sie werden, wenn sie aufwachsen. Bitte helft ihnen!', '', '', '', '', '', '', 23222),
(36995, 'deDE', 'Eberausbildung: Sichelklaue', 'Besiegt Sichelklaue vom Rücken Eures Ausbildungsebers aus.', 'Der Eber hat schon einige Kämpfe in Gorgrond erlebt. Das sollte es etwas einfacher machen, ihn auszubilden. Wir starten auf bekanntem Territorium, genau wie mit den anderen Tieren.$B$BMacht Euch gemeinsam auf, um den riesigen Raptor Sichelklaue auszuschalten.', '', '', '', '', '', '', 23222),
(36984, 'deDE', 'Grollhufausbildung: Rakkiri', 'Besiegt Rakkiri vom Rücken Eures Ausbildungsgrollhufs aus.', 'Jetzt, wo Euer Grollhuf schon über etwas Kampferfahrung verfügt, wollen wir ihn mal in unwirtlicheren Gegenden testen.$B$BReitet mit Eurem Grollhuf in die eisige Bergwelt des Frostfeuergrats und stellt Euch dem Kampf mit einem prächtigen Raubtier: Rakkiri. Jetzt wird sich Euer Reittier beweisen müssen.', '', '', '', '', '', '', 23222),
(36973, 'deDE', 'Talbukausbildung: Sichelklaue', 'Besiegt Sichelklaue vom Rücken Eures Ausbildungstalbuks aus.', 'Schon viele Reittiere sind beim Anblick eines gefährlichen Raubtieres geflüchtet. Und im Gebiet der abgeschiedenen Klippen von Gorgrond leben ein paar der gefährlichsten von ihnen.$B$BWir wollen Euren Talbuk auf eine harte Probe stellen. Mal sehen, ob Ihr beide den mächtigen Raptor Sichelklaue ausschalten könnt.', '', '', '', '', '', '', 23222),
(34448, 'deDE', 'Kaelynara Sonnenjäger', 'Konfrontiert Kaelynara Sonnenjäger.', 'Der Fokussierkristall ist fertig, $n. Wenn Ihr bereit seid, werde ich ihn verwenden, um Kaelynaras Schild zu durchbrechen.$b$bEs ist allerdings nicht sicher genug, sie im Innern der Mine anzugreifen. Also werde ich uns an einen sicheren Ort außerhalb der Jorunmine teleportieren, sobald ihr Schild außer Gefecht ist. Es wird viel einfacher sein, sie zu besiegen, wenn sie weit vom Kristall entfernt ist.$b$bSprecht mich einfach an, wenn Ihr bereit seid, die Aktion zu starten.', '', '', '', '', '', '', 23222),
(32838, 'deDE', 'Eine Fabel von Liebe und Edelmut', 'Bringt "Der Bär und die Jungfrau hehr" zu einem Mitglied der Kampfgilde Eurer Fraktion.', 'Ein kleines Kinderbuch. Es ist gebunden und in ziemlich gutem Zustand.$b$bAuf dem hinteren Buchdeckel entdeckt Ihr neben dem Buchrücken in kursiver Handschrift folgende Aufschrift:$b$bZUR KAMPFGILDE ZURÜCKBRINGEN', '', '', '', '', '', '', 23222),
(31087, 'deDE', 'Unsere Reichweite ausbauen', 'Aktiviert das resonierende Signal in der Bernscheinkuhle.', 'Die Klaxxi''va hören noch einen Getreuen. Seine Schreie sind schwach und verlieren sich in der Dunkelheit.$b$bDas Signal muss verstärkt werden.$b$bEs gibt einen Turm in der Bernscheinkuhle, der schon seit vielen Jahren verstummt ist. Er muss singen, die Klaxxi verlangen es!$b$bDieses Mal begleite ich Euch. Ihr werdet mir helfen, ihn zu reparieren.$B$BNehmt den Resonanzkristall und trefft mich in der Kuhle.', '', '', '', '', '', '', 23222),
(31023, 'deDE', 'Schwarmrelikte', 'Sammelt 8 Mantisrelikte.', 'Diese merkwürdige Schnitzerei ist mit komplizierten Schriftzeichen bedeckt, die aus der Sprache der Mantis stammen könnten.$B$BEuch fallen weitere Relikte auf, die achtlos über das Lager und im See verstreut liegen.$B$BVielleicht hätten die Klaxxi Interesse daran, diese Artefakte zu untersuchen.$B$BWer weiß? Vielleicht verdient Ihr Euch dadurch einen Gefallen.', '', '', '', '', '', '', 23222),
(30095, 'deDE', 'Die Endzeit', 'Sprecht mit Nozdormu in der Endzeit.', 'Nozdormu, der bronzene Drachenaspekt, hat die Champions der Allianz um Hilfe gebeten. Auch Ihr wurdet auserwählt. Mehr kann ich Euch nicht sagen.$B$BEs ist äußerst ungewöhnlich, dass mein Meister sterbliche Wesen um Hilfe bittet, aber ich vermute, dass die Zeit alles verändert... sogar den Aspekt der Zeit selbst.$B$BEr wartet auf Euch in der Endzeit, in ferner Zukunft. Ich kann Euch helfen, dorthin zu gelangen, wenn Ihr es wünscht.', '', '', '', '', '', 'Sprecht mit Nozdormu in der Endzeit.', 23222),
(29456, 'deDE', 'Ein erbeutetes Banner', 'Bringt das Banner der Gefallenen zu Professor Thaddeus Paleo auf dem Dunkelmond-Jahrmarkt auf der Dunkelmond-Insel.', 'Auf dem zerschlissenen Banner ist ein verblasstes Gildenwappen zu sehen. Das alte Banner muss viele Schlachten mitgemacht haben.$B$BProfessor Thaddeus Paleo vom Dunkelmond-Jahrmarkt sammelt Banner und andere Kriegsrelikte. Vielleicht wäre er auch an diesem interessiert. Zeigt ihm das Banner, wenn Ihr den Dunkelmond-Jahrmarkt das nächste Mal besucht.', '', '', '', '', '', '', 23222),
(28882, 'deDE', 'Sieg in Tol Barad', 'Erringt einen Sieg in Tol Barad und kehrt zu Major Marsden im Baradinbasislager auf der Halbinsel von Tol Barad zurück.', 'Wir haben uns den Brückenkopf auf diesem hässlichen Felsbrocken hart erkämpft und wir werden uns die Horde auf gar keinen Fall in die Quere kommen lassen. Die Kontrolle über Tol Barad zu behalten ist unsere oberste Priorität und wir brauchen dafür Eure Hilfe, $n. Erringt den Sieg in Tol Barad und kehrt mit Nachricht über unseren Sieg zu mir zurück.', 'Sieg in Tol Barad', '', '', '', '', 'Kehrt zu Major Marsden auf der Halbinsel von Tol Barad zurück.', 23222);


DELETE FROM `quest_objectives_locale` WHERE (`ID`=285159 AND `locale`='deDE') OR (`ID`=285073 AND `locale`='deDE') OR (`ID`=284172 AND `locale`='deDE') OR (`ID`=284171 AND `locale`='deDE') OR (`ID`=284170 AND `locale`='deDE') OR (`ID`=283946 AND `locale`='deDE') OR (`ID`=283945 AND `locale`='deDE') OR (`ID`=283830 AND `locale`='deDE') OR (`ID`=282762 AND `locale`='deDE') OR (`ID`=276271 AND `locale`='deDE') OR (`ID`=276270 AND `locale`='deDE') OR (`ID`=276269 AND `locale`='deDE') OR (`ID`=269150 AND `locale`='deDE') OR (`ID`=269149 AND `locale`='deDE') OR (`ID`=269148 AND `locale`='deDE') OR (`ID`=269147 AND `locale`='deDE') OR (`ID`=269145 AND `locale`='deDE') OR (`ID`=286696 AND `locale`='deDE') OR (`ID`=285830 AND `locale`='deDE') OR (`ID`=274501 AND `locale`='deDE') OR (`ID`=277663 AND `locale`='deDE') OR (`ID`=269180 AND `locale`='deDE') OR (`ID`=269179 AND `locale`='deDE') OR (`ID`=269178 AND `locale`='deDE') OR (`ID`=269177 AND `locale`='deDE') OR (`ID`=269176 AND `locale`='deDE') OR (`ID`=252818 AND `locale`='deDE') OR (`ID`=268789 AND `locale`='deDE') OR (`ID`=253450 AND `locale`='deDE') OR (`ID`=275490 AND `locale`='deDE') OR (`ID`=275479 AND `locale`='deDE') OR (`ID`=275466 AND `locale`='deDE') OR (`ID`=272736 AND `locale`='deDE') OR (`ID`=268886 AND `locale`='deDE') OR (`ID`=268039 AND `locale`='deDE');
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(285159, 'deDE', 43303, 0, 'Sprecht mit David Globetrotter', 23222),
(285073, 'deDE', 43179, 0, 'Schließt 4 Weltquests der Kirin Tor ab', 23222),
(284172, 'deDE', 42422, 0, 'Schließt 4 Weltquests der Wächterinnen ab', 23222),
(284171, 'deDE', 42421, 0, 'Schließt 4 Weltquests in Suramar ab', 23222),
(284170, 'deDE', 42420, 0, 'Schließt 4 Weltquests in Azsuna ab', 23222),
(283946, 'deDE', 42234, 0, 'Schließt 4 Weltquests in Sturmheim ab', 23222),
(283945, 'deDE', 42233, 0, 'Schließt 4 Weltquests am Hochberg ab', 23222),
(283830, 'deDE', 42170, 0, 'Schließt 4 Weltquests in Val''sharah ab', 23222),
(282762, 'deDE', 41761, 0, 'Elche zusammengetrieben', 23222),
(276271, 'deDE', 37479, 1, 'Marketa getötet', 23222),
(276270, 'deDE', 37479, 0, 'Valant getötet', 23222),
(276269, 'deDE', 37479, 2, 'Anenga getötet', 23222),
(269150, 'deDE', 31889, 0, 'Besiegt Elena Flatterflug', 23222),
(269149, 'deDE', 31889, 3, 'Besiegt Zoltan', 23222),
(269148, 'deDE', 31889, 4, 'Besiegt Kela Grimmtotem', 23222),
(269147, 'deDE', 31889, 2, 'Besiegt Grazzel den Großen', 23222),
(269145, 'deDE', 31889, 5, 'Besiegt Gluk den Verräter', 23222),
(286696, 'deDE', 44543, 0, 'Die Verheerte Küste angegriffen', 23222),
(285830, 'deDE', 43806, 0, 'Die Verheerte Küste angegriffen', 23222),
(274501, 'deDE', 36220, 1, 'Kopf vorgezeigt', 23222),
(277663, 'deDE', 38299, 0, 'Besiegt Erris die Sammlerin', 23222),
(269180, 'deDE', 31902, 9, 'Besiegt Durin Düsterhammer', 23222),
(269179, 'deDE', 31902, 8, 'Besiegt Everessa', 23222),
(269178, 'deDE', 31902, 7, 'Besiegt Kortas Düsterhammer', 23222),
(269177, 'deDE', 31902, 6, 'Besiegt Deiza Seuchenhorn', 23222),
(269176, 'deDE', 31902, 5, 'Besiegt David Kosse', 23222),
(252818, 'deDE', 30457, 1, 'Mitglieder des Bataaristammes getötet', 23222),
(268789, 'deDE', 31613, 0, 'Dorfbewohner von Grünstein verteidigt', 23222),
(253450, 'deDE', 30567, 0, 'Szenario abgeschlossen', 23222),
(275490, 'deDE', 36995, 0, 'Sichelklaue getötet', 23222),
(275479, 'deDE', 36984, 0, 'Rakkiri getötet', 23222),
(275466, 'deDE', 36973, 0, 'Sichelklaue getötet', 23222),
(272736, 'deDE', 34448, 1, 'Sprecht mit Elandra', 23222),
(268886, 'deDE', 31087, 0, 'Benutzt den Resonanzkristall mit dem stillen Signal', 23222),
(268039, 'deDE', 31087, 1, 'Bernscheinkuhle gefunden', 23222);


DELETE FROM `gameobject_template_locale` WHERE (`entry`=212173 AND `locale`='deDE') OR (`entry`=211695 AND `locale`='deDE') OR (`entry`=212016 AND `locale`='deDE') OR (`entry`=215893 AND `locale`='deDE') OR (`entry`=213195 AND `locale`='deDE') OR (`entry`=214064 AND `locale`='deDE') OR (`entry`=214063 AND `locale`='deDE') OR (`entry`=212766 AND `locale`='deDE') OR (`entry`=212755 AND `locale`='deDE') OR (`entry`=212764 AND `locale`='deDE') OR (`entry`=212757 AND `locale`='deDE') OR (`entry`=212763 AND `locale`='deDE') OR (`entry`=212754 AND `locale`='deDE') OR (`entry`=212765 AND `locale`='deDE') OR (`entry`=212761 AND `locale`='deDE') OR (`entry`=212756 AND `locale`='deDE') OR (`entry`=212762 AND `locale`='deDE') OR (`entry`=212759 AND `locale`='deDE') OR (`entry`=212758 AND `locale`='deDE') OR (`entry`=212753 AND `locale`='deDE') OR (`entry`=213042 AND `locale`='deDE') OR (`entry`=213041 AND `locale`='deDE') OR (`entry`=215041 AND `locale`='deDE') OR (`entry`=213043 AND `locale`='deDE') OR (`entry`=213044 AND `locale`='deDE') OR (`entry`=215040 AND `locale`='deDE') OR (`entry`=213066 AND `locale`='deDE') OR (`entry`=213065 AND `locale`='deDE') OR (`entry`=213064 AND `locale`='deDE') OR (`entry`=213063 AND `locale`='deDE') OR (`entry`=213040 AND `locale`='deDE') OR (`entry`=213039 AND `locale`='deDE') OR (`entry`=213038 AND `locale`='deDE') OR (`entry`=213037 AND `locale`='deDE') OR (`entry`=213059 AND `locale`='deDE') OR (`entry`=213060 AND `locale`='deDE') OR (`entry`=213061 AND `locale`='deDE') OR (`entry`=213055 AND `locale`='deDE') OR (`entry`=213056 AND `locale`='deDE') OR (`entry`=213058 AND `locale`='deDE') OR (`entry`=213057 AND `locale`='deDE') OR (`entry`=215039 AND `locale`='deDE') OR (`entry`=214798 AND `locale`='deDE') OR (`entry`=214797 AND `locale`='deDE') OR (`entry`=214761 AND `locale`='deDE') OR (`entry`=214762 AND `locale`='deDE') OR (`entry`=214801 AND `locale`='deDE') OR (`entry`=215044 AND `locale`='deDE') OR (`entry`=214802 AND `locale`='deDE') OR (`entry`=214799 AND `locale`='deDE') OR (`entry`=214800 AND `locale`='deDE') OR (`entry`=214806 AND `locale`='deDE') OR (`entry`=214805 AND `locale`='deDE') OR (`entry`=214807 AND `locale`='deDE') OR (`entry`=215043 AND `locale`='deDE') OR (`entry`=214804 AND `locale`='deDE') OR (`entry`=214803 AND `locale`='deDE') OR (`entry`=214808 AND `locale`='deDE') OR (`entry`=214809 AND `locale`='deDE') OR (`entry`=210539 AND `locale`='deDE') OR (`entry`=213033 AND `locale`='deDE') OR (`entry`=213068 AND `locale`='deDE') OR (`entry`=213067 AND `locale`='deDE') OR (`entry`=213034 AND `locale`='deDE') OR (`entry`=213369 AND `locale`='deDE') OR (`entry`=210538 AND `locale`='deDE') OR (`entry`=210537 AND `locale`='deDE') OR (`entry`=215042 AND `locale`='deDE') OR (`entry`=215045 AND `locale`='deDE') OR (`entry`=213801 AND `locale`='deDE') OR (`entry`=213036 AND `locale`='deDE') OR (`entry`=213035 AND `locale`='deDE') OR (`entry`=213799 AND `locale`='deDE') OR (`entry`=221993 AND `locale`='deDE') OR (`entry`=221992 AND `locale`='deDE') OR (`entry`=210515 AND `locale`='deDE') OR (`entry`=214470 AND `locale`='deDE') OR (`entry`=214065 AND `locale`='deDE') OR (`entry`=214906 AND `locale`='deDE') OR (`entry`=214471 AND `locale`='deDE') OR (`entry`=214381 AND `locale`='deDE') OR (`entry`=214382 AND `locale`='deDE') OR (`entry`=211395 AND `locale`='deDE') OR (`entry`=214283 AND `locale`='deDE') OR (`entry`=213416 AND `locale`='deDE') OR (`entry`=213084 AND `locale`='deDE') OR (`entry`=213083 AND `locale`='deDE') OR (`entry`=213082 AND `locale`='deDE') OR (`entry`=213812 AND `locale`='deDE') OR (`entry`=213811 AND `locale`='deDE') OR (`entry`=213303 AND `locale`='deDE') OR (`entry`=213289 AND `locale`='deDE') OR (`entry`=213180 AND `locale`='deDE') OR (`entry`=214901 AND `locale`='deDE') OR (`entry`=208903 AND `locale`='deDE') OR (`entry`=214948 AND `locale`='deDE') OR (`entry`=214896 AND `locale`='deDE') OR (`entry`=213414 AND `locale`='deDE') OR (`entry`=213640 AND `locale`='deDE') OR (`entry`=213049 AND `locale`='deDE') OR (`entry`=213800 AND `locale`='deDE') OR (`entry`=213349 AND `locale`='deDE') OR (`entry`=213347 AND `locale`='deDE') OR (`entry`=212927 AND `locale`='deDE') OR (`entry`=213350 AND `locale`='deDE') OR (`entry`=212760 AND `locale`='deDE') OR (`entry`=212750 AND `locale`='deDE') OR (`entry`=221602 AND `locale`='deDE') OR (`entry`=221663 AND `locale`='deDE') OR (`entry`=221611 AND `locale`='deDE') OR (`entry`=221245 AND `locale`='deDE') OR (`entry`=214827 AND `locale`='deDE') OR (`entry`=214826 AND `locale`='deDE') OR (`entry`=214795 AND `locale`='deDE') OR (`entry`=214825 AND `locale`='deDE') OR (`entry`=214824 AND `locale`='deDE') OR (`entry`=214813 AND `locale`='deDE') OR (`entry`=214520 AND `locale`='deDE') OR (`entry`=213810 AND `locale`='deDE') OR (`entry`=213596 AND `locale`='deDE') OR (`entry`=213595 AND `locale`='deDE') OR (`entry`=214653 AND `locale`='deDE') OR (`entry`=214652 AND `locale`='deDE') OR (`entry`=214651 AND `locale`='deDE') OR (`entry`=212162 AND `locale`='deDE') OR (`entry`=214659 AND `locale`='deDE') OR (`entry`=214660 AND `locale`='deDE') OR (`entry`=214661 AND `locale`='deDE') OR (`entry`=214650 AND `locale`='deDE') OR (`entry`=214658 AND `locale`='deDE') OR (`entry`=214662 AND `locale`='deDE') OR (`entry`=214657 AND `locale`='deDE') OR (`entry`=214649 AND `locale`='deDE') OR (`entry`=214648 AND `locale`='deDE') OR (`entry`=213665 AND `locale`='deDE') OR (`entry`=214655 AND `locale`='deDE') OR (`entry`=214654 AND `locale`='deDE') OR (`entry`=213666 AND `locale`='deDE') OR (`entry`=214656 AND `locale`='deDE') OR (`entry`=214665 AND `locale`='deDE') OR (`entry`=214664 AND `locale`='deDE') OR (`entry`=214663 AND `locale`='deDE') OR (`entry`=214668 AND `locale`='deDE') OR (`entry`=214666 AND `locale`='deDE') OR (`entry`=214667 AND `locale`='deDE') OR (`entry`=214669 AND `locale`='deDE') OR (`entry`=214670 AND `locale`='deDE') OR (`entry`=214671 AND `locale`='deDE') OR (`entry`=213593 AND `locale`='deDE') OR (`entry`=213594 AND `locale`='deDE') OR (`entry`=213597 AND `locale`='deDE') OR (`entry`=248954 AND `locale`='deDE') OR (`entry`=218954 AND `locale`='deDE') OR (`entry`=218953 AND `locale`='deDE') OR (`entry`=218951 AND `locale`='deDE') OR (`entry`=214993 AND `locale`='deDE') OR (`entry`=214226 AND `locale`='deDE') OR (`entry`=214225 AND `locale`='deDE') OR (`entry`=214223 AND `locale`='deDE') OR (`entry`=214222 AND `locale`='deDE') OR (`entry`=214220 AND `locale`='deDE') OR (`entry`=214218 AND `locale`='deDE') OR (`entry`=214217 AND `locale`='deDE') OR (`entry`=212526 AND `locale`='deDE') OR (`entry`=212020 AND `locale`='deDE') OR (`entry`=214209 AND `locale`='deDE') OR (`entry`=214221 AND `locale`='deDE') OR (`entry`=219355 AND `locale`='deDE') OR (`entry`=219354 AND `locale`='deDE') OR (`entry`=219353 AND `locale`='deDE') OR (`entry`=220373 AND `locale`='deDE') OR (`entry`=220374 AND `locale`='deDE') OR (`entry`=214219 AND `locale`='deDE') OR (`entry`=214224 AND `locale`='deDE') OR (`entry`=218952 AND `locale`='deDE') OR (`entry`=214213 AND `locale`='deDE') OR (`entry`=218956 AND `locale`='deDE') OR (`entry`=218955 AND `locale`='deDE') OR (`entry`=214208 AND `locale`='deDE') OR (`entry`=214210 AND `locale`='deDE') OR (`entry`=214212 AND `locale`='deDE') OR (`entry`=221775 AND `locale`='deDE') OR (`entry`=214205 AND `locale`='deDE') OR (`entry`=214214 AND `locale`='deDE') OR (`entry`=214206 AND `locale`='deDE') OR (`entry`=214211 AND `locale`='deDE') OR (`entry`=214207 AND `locale`='deDE') OR (`entry`=218958 AND `locale`='deDE') OR (`entry`=218957 AND `locale`='deDE') OR (`entry`=223214 AND `locale`='deDE') OR (`entry`=213255 AND `locale`='deDE') OR (`entry`=215126 AND `locale`='deDE') OR (`entry`=213693 AND `locale`='deDE') OR (`entry`=215124 AND `locale`='deDE') OR (`entry`=213142 AND `locale`='deDE') OR (`entry`=213141 AND `locale`='deDE') OR (`entry`=215127 AND `locale`='deDE') OR (`entry`=215125 AND `locale`='deDE') OR (`entry`=213683 AND `locale`='deDE') OR (`entry`=213697 AND `locale`='deDE') OR (`entry`=213688 AND `locale`='deDE') OR (`entry`=215112 AND `locale`='deDE') OR (`entry`=213717 AND `locale`='deDE') OR (`entry`=213699 AND `locale`='deDE') OR (`entry`=213143 AND `locale`='deDE') OR (`entry`=213730 AND `locale`='deDE') OR (`entry`=213729 AND `locale`='deDE') OR (`entry`=215113 AND `locale`='deDE') OR (`entry`=213703 AND `locale`='deDE') OR (`entry`=213694 AND `locale`='deDE') OR (`entry`=213690 AND `locale`='deDE') OR (`entry`=213685 AND `locale`='deDE') OR (`entry`=214234 AND `locale`='deDE') OR (`entry`=213684 AND `locale`='deDE') OR (`entry`=214232 AND `locale`='deDE') OR (`entry`=213677 AND `locale`='deDE') OR (`entry`=213700 AND `locale`='deDE') OR (`entry`=213715 AND `locale`='deDE') OR (`entry`=213702 AND `locale`='deDE') OR (`entry`=213716 AND `locale`='deDE') OR (`entry`=213705 AND `locale`='deDE') OR (`entry`=213676 AND `locale`='deDE') OR (`entry`=213675 AND `locale`='deDE') OR (`entry`=213674 AND `locale`='deDE') OR (`entry`=213673 AND `locale`='deDE') OR (`entry`=213672 AND `locale`='deDE') OR (`entry`=213122 AND `locale`='deDE') OR (`entry`=213121 AND `locale`='deDE') OR (`entry`=214231 AND `locale`='deDE') OR (`entry`=214233 AND `locale`='deDE') OR (`entry`=213706 AND `locale`='deDE') OR (`entry`=213728 AND `locale`='deDE') OR (`entry`=213115 AND `locale`='deDE') OR (`entry`=213678 AND `locale`='deDE') OR (`entry`=213667 AND `locale`='deDE') OR (`entry`=213680 AND `locale`='deDE') OR (`entry`=213668 AND `locale`='deDE') OR (`entry`=213116 AND `locale`='deDE') OR (`entry`=213114 AND `locale`='deDE') OR (`entry`=213671 AND `locale`='deDE') OR (`entry`=214176 AND `locale`='deDE') OR (`entry`=213704 AND `locale`='deDE') OR (`entry`=213986 AND `locale`='deDE') OR (`entry`=213987 AND `locale`='deDE') OR (`entry`=213133 AND `locale`='deDE') OR (`entry`=214728 AND `locale`='deDE') OR (`entry`=213689 AND `locale`='deDE') OR (`entry`=213692 AND `locale`='deDE') OR (`entry`=213695 AND `locale`='deDE') OR (`entry`=213691 AND `locale`='deDE') OR (`entry`=213723 AND `locale`='deDE') OR (`entry`=213701 AND `locale`='deDE') OR (`entry`=213722 AND `locale`='deDE') OR (`entry`=213721 AND `locale`='deDE') OR (`entry`=213686 AND `locale`='deDE') OR (`entry`=213726 AND `locale`='deDE') OR (`entry`=213724 AND `locale`='deDE') OR (`entry`=213711 AND `locale`='deDE') OR (`entry`=213710 AND `locale`='deDE') OR (`entry`=213687 AND `locale`='deDE') OR (`entry`=213714 AND `locale`='deDE') OR (`entry`=213698 AND `locale`='deDE') OR (`entry`=213725 AND `locale`='deDE') OR (`entry`=213719 AND `locale`='deDE') OR (`entry`=213989 AND `locale`='deDE') OR (`entry`=213727 AND `locale`='deDE') OR (`entry`=213990 AND `locale`='deDE') OR (`entry`=213669 AND `locale`='deDE') OR (`entry`=213991 AND `locale`='deDE') OR (`entry`=213670 AND `locale`='deDE') OR (`entry`=213696 AND `locale`='deDE') OR (`entry`=213720 AND `locale`='deDE') OR (`entry`=213132 AND `locale`='deDE') OR (`entry`=213708 AND `locale`='deDE') OR (`entry`=213681 AND `locale`='deDE') OR (`entry`=213984 AND `locale`='deDE') OR (`entry`=213712 AND `locale`='deDE') OR (`entry`=213709 AND `locale`='deDE') OR (`entry`=213718 AND `locale`='deDE') OR (`entry`=213985 AND `locale`='deDE') OR (`entry`=214729 AND `locale`='deDE') OR (`entry`=213713 AND `locale`='deDE') OR (`entry`=214727 AND `locale`='deDE') OR (`entry`=213707 AND `locale`='deDE') OR (`entry`=213682 AND `locale`='deDE') OR (`entry`=222778 AND `locale`='deDE') OR (`entry`=222777 AND `locale`='deDE') OR (`entry`=214899 AND `locale`='deDE') OR (`entry`=213297 AND `locale`='deDE') OR (`entry`=213298 AND `locale`='deDE') OR (`entry`=210407 AND `locale`='deDE') OR (`entry`=213296 AND `locale`='deDE') OR (`entry`=213295 AND `locale`='deDE') OR (`entry`=213456 AND `locale`='deDE') OR (`entry`=214895 AND `locale`='deDE') OR (`entry`=214900 AND `locale`='deDE') OR (`entry`=212876 AND `locale`='deDE') OR (`entry`=210419 AND `locale`='deDE') OR (`entry`=213353 AND `locale`='deDE') OR (`entry`=213352 AND `locale`='deDE') OR (`entry`=223008 AND `locale`='deDE') OR (`entry`=223019 AND `locale`='deDE') OR (`entry`=223018 AND `locale`='deDE') OR (`entry`=223011 AND `locale`='deDE') OR (`entry`=223010 AND `locale`='deDE') OR (`entry`=223009 AND `locale`='deDE') OR (`entry`=223192 AND `locale`='deDE') OR (`entry`=223190 AND `locale`='deDE') OR (`entry`=223017 AND `locale`='deDE') OR (`entry`=223016 AND `locale`='deDE') OR (`entry`=223015 AND `locale`='deDE') OR (`entry`=223014 AND `locale`='deDE') OR (`entry`=223013 AND `locale`='deDE') OR (`entry`=223012 AND `locale`='deDE') OR (`entry`=221268 AND `locale`='deDE') OR (`entry`=222754 AND `locale`='deDE') OR (`entry`=222753 AND `locale`='deDE') OR (`entry`=214539 AND `locale`='deDE') OR (`entry`=214885 AND `locale`='deDE') OR (`entry`=213254 AND `locale`='deDE') OR (`entry`=215116 AND `locale`='deDE') OR (`entry`=213606 AND `locale`='deDE') OR (`entry`=213599 AND `locale`='deDE') OR (`entry`=212106 AND `locale`='deDE') OR (`entry`=215119 AND `locale`='deDE') OR (`entry`=215118 AND `locale`='deDE') OR (`entry`=215117 AND `locale`='deDE') OR (`entry`=213619 AND `locale`='deDE') OR (`entry`=213609 AND `locale`='deDE') OR (`entry`=212117 AND `locale`='deDE') OR (`entry`=213608 AND `locale`='deDE') OR (`entry`=212116 AND `locale`='deDE') OR (`entry`=212111 AND `locale`='deDE') OR (`entry`=212110 AND `locale`='deDE') OR (`entry`=212109 AND `locale`='deDE') OR (`entry`=212108 AND `locale`='deDE') OR (`entry`=215120 AND `locale`='deDE') OR (`entry`=213634 AND `locale`='deDE') OR (`entry`=213633 AND `locale`='deDE') OR (`entry`=213625 AND `locale`='deDE') OR (`entry`=213607 AND `locale`='deDE') OR (`entry`=213598 AND `locale`='deDE') OR (`entry`=215121 AND `locale`='deDE') OR (`entry`=213635 AND `locale`='deDE') OR (`entry`=213626 AND `locale`='deDE') OR (`entry`=213620 AND `locale`='deDE') OR (`entry`=213614 AND `locale`='deDE') OR (`entry`=212119 AND `locale`='deDE') OR (`entry`=213617 AND `locale`='deDE') OR (`entry`=216630 AND `locale`='deDE') OR (`entry`=214260 AND `locale`='deDE') OR (`entry`=215128 AND `locale`='deDE') OR (`entry`=213616 AND `locale`='deDE') OR (`entry`=213618 AND `locale`='deDE') OR (`entry`=213604 AND `locale`='deDE') OR (`entry`=214258 AND `locale`='deDE') OR (`entry`=213602 AND `locale`='deDE') OR (`entry`=213601 AND `locale`='deDE') OR (`entry`=213615 AND `locale`='deDE') OR (`entry`=213636 AND `locale`='deDE') OR (`entry`=213600 AND `locale`='deDE') OR (`entry`=212118 AND `locale`='deDE') OR (`entry`=213603 AND `locale`='deDE') OR (`entry`=214424 AND `locale`='deDE') OR (`entry`=213227 AND `locale`='deDE') OR (`entry`=214717 AND `locale`='deDE') OR (`entry`=214718 AND `locale`='deDE') OR (`entry`=214716 AND `locale`='deDE') OR (`entry`=214714 AND `locale`='deDE') OR (`entry`=214715 AND `locale`='deDE') OR (`entry`=214713 AND `locale`='deDE') OR (`entry`=213641 AND `locale`='deDE') OR (`entry`=214702 AND `locale`='deDE') OR (`entry`=214712 AND `locale`='deDE') OR (`entry`=212113 AND `locale`='deDE') OR (`entry`=214703 AND `locale`='deDE') OR (`entry`=213642 AND `locale`='deDE') OR (`entry`=213226 AND `locale`='deDE') OR (`entry`=214711 AND `locale`='deDE') OR (`entry`=214704 AND `locale`='deDE') OR (`entry`=213643 AND `locale`='deDE') OR (`entry`=214710 AND `locale`='deDE') OR (`entry`=214705 AND `locale`='deDE') OR (`entry`=213644 AND `locale`='deDE') OR (`entry`=214697 AND `locale`='deDE') OR (`entry`=214706 AND `locale`='deDE') OR (`entry`=213645 AND `locale`='deDE') OR (`entry`=214698 AND `locale`='deDE') OR (`entry`=214721 AND `locale`='deDE') OR (`entry`=214720 AND `locale`='deDE') OR (`entry`=214719 AND `locale`='deDE') OR (`entry`=213632 AND `locale`='deDE') OR (`entry`=213631 AND `locale`='deDE') OR (`entry`=213630 AND `locale`='deDE') OR (`entry`=213629 AND `locale`='deDE') OR (`entry`=213628 AND `locale`='deDE') OR (`entry`=213627 AND `locale`='deDE') OR (`entry`=212115 AND `locale`='deDE') OR (`entry`=212107 AND `locale`='deDE') OR (`entry`=213610 AND `locale`='deDE') OR (`entry`=213613 AND `locale`='deDE') OR (`entry`=144111 AND `locale`='deDE') OR (`entry`=215171 AND `locale`='deDE') OR (`entry`=214850 AND `locale`='deDE') OR (`entry`=214849 AND `locale`='deDE') OR (`entry`=214852 AND `locale`='deDE') OR (`entry`=214851 AND `locale`='deDE') OR (`entry`=214854 AND `locale`='deDE') OR (`entry`=214853 AND `locale`='deDE') OR (`entry`=214525 AND `locale`='deDE');
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(212173, 'deDE', 'Ein Schwarm Rotbauchmandarine', '', NULL, 23222),
(211695, 'deDE', 'Pfeil der Shado-Pan', '', NULL, 23222),
(212016, 'deDE', 'Drachenschleuder von Gao-Ran', '', NULL, 23222),
(215893, 'deDE', 'Klaxxi''vess', '', NULL, 23222),
(213195, 'deDE', 'Mantiskatapult', '', NULL, 23222),
(214064, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214063, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212766, 'deDE', 'Moguartefakt', '', NULL, 23222),
(212755, 'deDE', 'Mogu Statue Piece, Upper Body w/ Head', '', NULL, 23222),
(212764, 'deDE', 'Moguartefakt', '', NULL, 23222),
(212757, 'deDE', 'Mogu Statue Piece, Hand w/ Cestus', '', NULL, 23222),
(212763, 'deDE', 'Moguartefakt', '', NULL, 23222),
(212754, 'deDE', 'Mogu Statue Piece, Shoulder', '', NULL, 23222),
(212765, 'deDE', 'Moguartefakt', '', NULL, 23222),
(212761, 'deDE', 'Rubinauge', '', NULL, 23222),
(212756, 'deDE', 'Mogu Statue Piece, Arm', '', NULL, 23222),
(212762, 'deDE', 'Moguartefakt', '', NULL, 23222),
(212759, 'deDE', 'Rubinauge', '', NULL, 23222),
(212758, 'deDE', 'Mogu Statue Piece, Head Large', '', NULL, 23222),
(212753, 'deDE', 'Mogu Statue Piece, Staff', '', NULL, 23222),
(213042, 'deDE', 'Stuhl', '', NULL, 23222),
(213041, 'deDE', 'Stuhl', '', NULL, 23222),
(215041, 'deDE', 'Ofen', '', NULL, 23222),
(213043, 'deDE', 'Stuhl', '', NULL, 23222),
(213044, 'deDE', 'Stuhl', '', NULL, 23222),
(215040, 'deDE', 'Ofen', '', NULL, 23222),
(213066, 'deDE', 'Stuhl', '', NULL, 23222),
(213065, 'deDE', 'Stuhl', '', NULL, 23222),
(213064, 'deDE', 'Stuhl', '', NULL, 23222),
(213063, 'deDE', 'Stuhl', '', NULL, 23222),
(213040, 'deDE', 'Stuhl', '', NULL, 23222),
(213039, 'deDE', 'Stuhl', '', NULL, 23222),
(213038, 'deDE', 'Stuhl', '', NULL, 23222),
(213037, 'deDE', 'Stuhl', '', NULL, 23222),
(213059, 'deDE', 'Stuhl', '', NULL, 23222),
(213060, 'deDE', 'Stuhl', '', NULL, 23222),
(213061, 'deDE', 'Stuhl', '', NULL, 23222),
(213055, 'deDE', 'Stuhl', '', NULL, 23222),
(213056, 'deDE', 'Stuhl', '', NULL, 23222),
(213058, 'deDE', 'Stuhl', '', NULL, 23222),
(213057, 'deDE', 'Stuhl', '', NULL, 23222),
(215039, 'deDE', 'Ofen', '', NULL, 23222),
(214798, 'deDE', 'Hocker', '', NULL, 23222),
(214797, 'deDE', 'Hocker', '', NULL, 23222),
(214761, 'deDE', 'Hocker', '', NULL, 23222),
(214762, 'deDE', 'Hocker', '', NULL, 23222),
(214801, 'deDE', 'Hocker', '', NULL, 23222),
(215044, 'deDE', 'Ofen', '', NULL, 23222),
(214802, 'deDE', 'Hocker', '', NULL, 23222),
(214799, 'deDE', 'Hocker', '', NULL, 23222),
(214800, 'deDE', 'Hocker', '', NULL, 23222),
(214806, 'deDE', 'Hocker', '', NULL, 23222),
(214805, 'deDE', 'Hocker', '', NULL, 23222),
(214807, 'deDE', 'Hocker', '', NULL, 23222),
(215043, 'deDE', 'Ofen', '', NULL, 23222),
(214804, 'deDE', 'Hocker', '', NULL, 23222),
(214803, 'deDE', 'Hocker', '', NULL, 23222),
(214808, 'deDE', 'Hocker', '', NULL, 23222),
(214809, 'deDE', 'Hocker', '', NULL, 23222),
(210539, 'deDE', 'Immerblüte', '', NULL, 23222),
(213033, 'deDE', 'Stuhl', '', NULL, 23222),
(213068, 'deDE', 'Stuhl', '', NULL, 23222),
(213067, 'deDE', 'Stuhl', '', NULL, 23222),
(213034, 'deDE', 'Stuhl', '', NULL, 23222),
(213369, 'deDE', 'Wassereimer von Nebelhauch', '', NULL, 23222),
(210538, 'deDE', 'Immerblüte', '', NULL, 23222),
(210537, 'deDE', 'Immerblüte', '', NULL, 23222),
(215042, 'deDE', 'Amboss', '', NULL, 23222),
(215045, 'deDE', 'Schmiede', '', NULL, 23222),
(213801, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(213036, 'deDE', 'Stuhl', '', NULL, 23222),
(213035, 'deDE', 'Stuhl', '', NULL, 23222),
(213799, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(221993, 'deDE', 'Seidenfederfalkennest', '', NULL, 23222),
(221992, 'deDE', 'Seidenfederfalkenei', '', NULL, 23222),
(210515, 'deDE', 'Überlebensringpfosten', '', NULL, 23222),
(214470, 'deDE', 'Shao-Tien-Käfig', '', NULL, 23222),
(214065, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214906, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214471, 'deDE', 'Shao-Tien-Käfig', '', NULL, 23222),
(214381, 'deDE', 'Teleportierplatte', '', NULL, 23222),
(214382, 'deDE', 'Teleportierrune', '', NULL, 23222),
(211395, 'deDE', 'Kriegsspeer des Donnerkönigs', '', NULL, 23222),
(214283, 'deDE', 'Spinnenei', '', NULL, 23222),
(213416, 'deDE', 'Gemeinsam sind wir stark', '', NULL, 23222),
(213084, 'deDE', 'Stuhl', '', NULL, 23222),
(213083, 'deDE', 'Stuhl', '', NULL, 23222),
(213082, 'deDE', 'Stuhl', '', NULL, 23222),
(213812, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(213811, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(213303, 'deDE', 'Hees Giftfalle', '', NULL, 23222),
(213289, 'deDE', 'Shao-Tien-Käfig', '', NULL, 23222),
(213180, 'deDE', 'Guo-Lai-Runenstein', '', NULL, 23222),
(214901, 'deDE', 'Sturmrufer der Shao-Tien', '', NULL, 23222),
(208903, 'deDE', 'Feuer', '', NULL, 23222),
(214948, 'deDE', 'Shao-Tien-Käfig', '', NULL, 23222),
(214896, 'deDE', 'Shao-Tien-Estrade', '', NULL, 23222),
(213414, 'deDE', 'Vergesst niemals', '', NULL, 23222),
(213640, 'deDE', 'Wukaoaufklärungsbericht', '', NULL, 23222),
(213049, 'deDE', 'Bank', '', NULL, 23222),
(213800, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(213349, 'deDE', 'Weißblütenlilie', '', NULL, 23222),
(213347, 'deDE', 'Weißblütenlilie', '', NULL, 23222),
(212927, 'deDE', 'Weißblütenlilie', '', NULL, 23222),
(213350, 'deDE', 'Mjampignon', '', NULL, 23222),
(212760, 'deDE', 'Rubinauge', '', NULL, 23222),
(212750, 'deDE', 'Mogu Statue Piece, Head Small', '', NULL, 23222),
(221602, 'deDE', 'Sha-Energiemauer', '', NULL, 23222),
(221663, 'deDE', 'Massive uralte Tür', '', NULL, 23222),
(221611, 'deDE', 'Sha-Feld', '', NULL, 23222),
(221245, 'deDE', 'Immerseus'' Tor', '', NULL, 23222),
(214827, 'deDE', 'Uralte Mogutruhe', '', NULL, 23222),
(214826, 'deDE', 'Uralte Mogutruhe', '', NULL, 23222),
(214795, 'deDE', 'Uralter Moguschatz', '', NULL, 23222),
(214825, 'deDE', 'Uralte Mogutruhe', '', NULL, 23222),
(214824, 'deDE', 'Uralte Mogutruhe', '', NULL, 23222),
(214813, 'deDE', 'Moguschatz', '', NULL, 23222),
(214520, 'deDE', 'Vermächtnis der Klananführer', '', NULL, 23222),
(213810, 'deDE', 'Doodad_PA_SecretSteps002', '', NULL, 23222),
(213596, 'deDE', 'Doodad_PA_RoyalDoor_004', '', NULL, 23222),
(213595, 'deDE', 'Doodad_PA_RoyalDoor_003', '', NULL, 23222),
(214653, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214652, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214651, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(212162, 'deDE', 'Mogulift', '', NULL, 23222),
(214659, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214660, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214661, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214650, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214658, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214662, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214657, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214649, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214648, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(213665, 'deDE', 'Doodad_PA_RoyalDoor_006', '', NULL, 23222),
(214655, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214654, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(213666, 'deDE', 'Doodad_PA_RoyalDoor_007', '', NULL, 23222),
(214656, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214665, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214664, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214663, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214668, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214666, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214667, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214669, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214670, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(214671, 'deDE', 'Uralter Mogutresor', '', NULL, 23222),
(213593, 'deDE', 'Doodad_PA_RoyalDoor_001', '', NULL, 23222),
(213594, 'deDE', 'Doodad_PA_RoyalDoor_002', '', NULL, 23222),
(213597, 'deDE', 'Doodad_PA_RoyalDoor_005', '', NULL, 23222),
(248954, 'deDE', 'Kompendium uralter Waffen, Band III', '', NULL, 23222),
(218954, 'deDE', 'Gerümpel', '', NULL, 23222),
(218953, 'deDE', 'Gerümpel', '', NULL, 23222),
(218951, 'deDE', 'Gerümpel', '', NULL, 23222),
(214993, 'deDE', 'Gerümpel', '', NULL, 23222),
(214226, 'deDE', 'Gerümpel', '', NULL, 23222),
(214225, 'deDE', 'Gerümpel', '', NULL, 23222),
(214223, 'deDE', 'Gerümpel', '', NULL, 23222),
(214222, 'deDE', 'Gerümpel', '', NULL, 23222),
(214220, 'deDE', 'Gerümpel', '', NULL, 23222),
(214218, 'deDE', 'Gerümpel', '', NULL, 23222),
(214217, 'deDE', 'Gerümpel', '', NULL, 23222),
(212526, 'deDE', 'Ein leeres Bücherregal', '', NULL, 23222),
(212020, 'deDE', 'Eure höchsteigene Sammlung', '', NULL, 23222),
(214209, 'deDE', 'Gerümpel', '', NULL, 23222),
(214221, 'deDE', 'Gerümpel', '', NULL, 23222),
(219355, 'deDE', 'Traumgebräuzubehör', '', NULL, 23222),
(219354, 'deDE', 'Traumgebräuzubehör', '', NULL, 23222),
(219353, 'deDE', 'Traumgebräutisch', '', NULL, 23222),
(220373, 'deDE', 'Knisterndes Kohlenbecken', '', NULL, 23222),
(220374, 'deDE', 'Knisterndes Kohlenbecken', '', NULL, 23222),
(214219, 'deDE', 'Gerümpel', '', NULL, 23222),
(214224, 'deDE', 'Gerümpel', '', NULL, 23222),
(218952, 'deDE', 'Gerümpel', '', NULL, 23222),
(214213, 'deDE', 'Gerümpel', '', NULL, 23222),
(218956, 'deDE', 'Gerümpel', '', NULL, 23222),
(218955, 'deDE', 'Gerümpel', '', NULL, 23222),
(214208, 'deDE', 'Gerümpel', '', NULL, 23222),
(214210, 'deDE', 'Gerümpel', '', NULL, 23222),
(214212, 'deDE', 'Gerümpel', '', NULL, 23222),
(221775, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214205, 'deDE', 'Gerümpel', '', NULL, 23222),
(214214, 'deDE', 'Gerümpel', '', NULL, 23222),
(214206, 'deDE', 'Gerümpel', '', NULL, 23222),
(214211, 'deDE', 'Gerümpel', '', NULL, 23222),
(214207, 'deDE', 'Gerümpel', '', NULL, 23222),
(218958, 'deDE', 'Gerümpel', '', NULL, 23222),
(218957, 'deDE', 'Gerümpel', '', NULL, 23222),
(223214, 'deDE', 'Kissen', '', NULL, 23222),
(213255, 'deDE', 'Versammlungsstein', '', NULL, 23222),
(215126, 'deDE', 'Silbermond', '', NULL, 23222),
(213693, 'deDE', 'Bank', '', NULL, 23222),
(215124, 'deDE', 'Unterstadt', '', NULL, 23222),
(213142, 'deDE', 'Stuhl', '', NULL, 23222),
(213141, 'deDE', 'Stuhl', '', NULL, 23222),
(215127, 'deDE', 'Orgrimmar', '', NULL, 23222),
(215125, 'deDE', 'Donnerfels', '', NULL, 23222),
(213683, 'deDE', 'Briefkasten', '', NULL, 23222),
(213697, 'deDE', 'Bank', '', NULL, 23222),
(213688, 'deDE', 'Bank', '', NULL, 23222),
(215112, 'deDE', 'Dalaran', '', NULL, 23222),
(213717, 'deDE', 'Bank', '', NULL, 23222),
(213699, 'deDE', 'Stuhl', '', NULL, 23222),
(213143, 'deDE', 'Stuhl', '', NULL, 23222),
(213730, 'deDE', 'Bank', '', NULL, 23222),
(213729, 'deDE', 'Bank', '', NULL, 23222),
(215113, 'deDE', 'Shattrath', '', NULL, 23222),
(213703, 'deDE', 'Banner', '', NULL, 23222),
(213694, 'deDE', 'Bank', '', NULL, 23222),
(213690, 'deDE', 'Bank', '', NULL, 23222),
(213685, 'deDE', 'Bank', '', NULL, 23222),
(214234, 'deDE', 'Gildentresor', '', NULL, 23222),
(213684, 'deDE', 'Briefkasten', '', NULL, 23222),
(214232, 'deDE', 'Gildentresor', '', NULL, 23222),
(213677, 'deDE', 'Hammer', '', NULL, 23222),
(213700, 'deDE', 'Banner', '', NULL, 23222),
(213715, 'deDE', 'Banner', '', NULL, 23222),
(213702, 'deDE', 'Banner', '', NULL, 23222),
(213716, 'deDE', 'Banner', '', NULL, 23222),
(213705, 'deDE', 'Banner', '', NULL, 23222),
(213676, 'deDE', 'Amboss', '', NULL, 23222),
(213675, 'deDE', 'Zange', '', NULL, 23222),
(213674, 'deDE', 'Stuhl', '', NULL, 23222),
(213673, 'deDE', 'Stuhl', '', NULL, 23222),
(213672, 'deDE', 'Stuhl', '', NULL, 23222),
(213122, 'deDE', 'Hammer', '', NULL, 23222),
(213121, 'deDE', 'Schmiede', '', NULL, 23222),
(214231, 'deDE', 'Gildentresor', '', NULL, 23222),
(214233, 'deDE', 'Gildentresor', '', NULL, 23222),
(213706, 'deDE', 'Banner', '', NULL, 23222),
(213728, 'deDE', 'Briefkasten', '', NULL, 23222),
(213115, 'deDE', 'Bank', '', NULL, 23222),
(213678, 'deDE', 'Banner', '', NULL, 23222),
(213667, 'deDE', 'Bank', '', NULL, 23222),
(213680, 'deDE', 'Bank', '', NULL, 23222),
(213668, 'deDE', 'Bank', '', NULL, 23222),
(213116, 'deDE', 'Bank', '', NULL, 23222),
(213114, 'deDE', 'Bank', '', NULL, 23222),
(213671, 'deDE', 'Bank', '', NULL, 23222),
(214176, 'deDE', 'Briefkasten', '', NULL, 23222),
(213704, 'deDE', 'Banner', '', NULL, 23222),
(213986, 'deDE', 'Todesritter', '', NULL, 23222),
(213987, 'deDE', 'Druide', '', NULL, 23222),
(213133, 'deDE', 'Banner', '', NULL, 23222),
(214728, 'deDE', 'Magier', '', NULL, 23222),
(213689, 'deDE', 'Bank', '', NULL, 23222),
(213692, 'deDE', 'Bank', '', NULL, 23222),
(213695, 'deDE', 'Bank', '', NULL, 23222),
(213691, 'deDE', 'Bank', '', NULL, 23222),
(213723, 'deDE', 'Stuhl', '', NULL, 23222),
(213701, 'deDE', 'Stuhl', '', NULL, 23222),
(213722, 'deDE', 'Stuhl', '', NULL, 23222),
(213721, 'deDE', 'Stuhl', '', NULL, 23222),
(213686, 'deDE', 'Bank', '', NULL, 23222),
(213726, 'deDE', 'Stuhl', '', NULL, 23222),
(213724, 'deDE', 'Stuhl', '', NULL, 23222),
(213711, 'deDE', 'Stuhl', '', NULL, 23222),
(213710, 'deDE', 'Stuhl', '', NULL, 23222),
(213687, 'deDE', 'Bank', '', NULL, 23222),
(213714, 'deDE', 'Stuhl', '', NULL, 23222),
(213698, 'deDE', 'Briefkasten', '', NULL, 23222),
(213725, 'deDE', 'Stuhl', '', NULL, 23222),
(213719, 'deDE', 'Stuhl', '', NULL, 23222),
(213989, 'deDE', 'Mönch', '', NULL, 23222),
(213727, 'deDE', 'Briefkasten', '', NULL, 23222),
(213990, 'deDE', 'Paladin', '', NULL, 23222),
(213669, 'deDE', 'Bank', '', NULL, 23222),
(213991, 'deDE', 'Priester', '', NULL, 23222),
(213670, 'deDE', 'Bank', '', NULL, 23222),
(213696, 'deDE', 'Bank', '', NULL, 23222),
(213720, 'deDE', 'Stuhl', '', NULL, 23222),
(213132, 'deDE', 'Banner', '', NULL, 23222),
(213708, 'deDE', 'Briefkasten', '', NULL, 23222),
(213681, 'deDE', 'Bank', '', NULL, 23222),
(213984, 'deDE', 'Schurke', '', NULL, 23222),
(213712, 'deDE', 'Stuhl', '', NULL, 23222),
(213709, 'deDE', 'Bank', '', NULL, 23222),
(213718, 'deDE', 'Stuhl', '', NULL, 23222),
(213985, 'deDE', 'Schamane', '', NULL, 23222),
(214729, 'deDE', 'Hexenmeister', '', NULL, 23222),
(213713, 'deDE', 'Stuhl', '', NULL, 23222),
(214727, 'deDE', 'Krieger', '', NULL, 23222),
(213707, 'deDE', 'Bar', '', NULL, 23222),
(213682, 'deDE', 'Briefkasten', '', NULL, 23222),
(222778, 'deDE', 'Bank', '', NULL, 23222),
(222777, 'deDE', 'Bank', '', NULL, 23222),
(214899, 'deDE', 'Sturmrufer der Shao-Tien', '', NULL, 23222),
(213297, 'deDE', 'Bank', '', NULL, 23222),
(213298, 'deDE', 'Bank', '', NULL, 23222),
(210407, 'deDE', 'Dezcos Kind', '', NULL, 23222),
(213296, 'deDE', 'Bank', '', NULL, 23222),
(213295, 'deDE', 'Bank', '', NULL, 23222),
(213456, 'deDE', 'Des Kaisers Bürde - Teil 8', '', NULL, 23222),
(214895, 'deDE', 'Sturmrufer der Shao-Tien', '', NULL, 23222),
(214900, 'deDE', 'Sturmrufer der Shao-Tien', '', NULL, 23222),
(212876, 'deDE', 'Mogubehältnis', '', NULL, 23222),
(210419, 'deDE', 'Pandarengrabstein', '', NULL, 23222),
(213353, 'deDE', 'Mjampignon', '', NULL, 23222),
(213352, 'deDE', 'Mjampignon', '', NULL, 23222),
(223008, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable014', '', NULL, 23222),
(223019, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable003', '', NULL, 23222),
(223018, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable004', '', NULL, 23222),
(223011, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable011', '', NULL, 23222),
(223010, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable012', '', NULL, 23222),
(223009, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable013', '', NULL, 23222),
(223192, 'deDE', 'Doodad_ZulDrak_Fog_Blue_toggleable001', '', NULL, 23222),
(223190, 'deDE', 'Doodad_Lightray_Dusty_toggleable021', '', NULL, 23222),
(223017, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable005', '', NULL, 23222),
(223016, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable006', '', NULL, 23222),
(223015, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable007', '', NULL, 23222),
(223014, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable008', '', NULL, 23222),
(223013, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable009', '', NULL, 23222),
(223012, 'deDE', 'Doodad_Lightray_Dusty_01_toggleable010', '', NULL, 23222),
(221268, 'deDE', 'Versammlungsstein', '', NULL, 23222),
(222754, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(222753, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214539, 'deDE', 'Instance Portal (Raid 4 Difficulties)', '', NULL, 23222),
(214885, 'deDE', 'Instance Portal (Party + Heroic + Challenge)', '', NULL, 23222),
(213254, 'deDE', 'Versammlungsstein', '', NULL, 23222),
(215116, 'deDE', 'Darnassus', '', NULL, 23222),
(213606, 'deDE', 'Bank', '', NULL, 23222),
(213599, 'deDE', 'Bank', '', NULL, 23222),
(212106, 'deDE', 'Briefkasten', '', NULL, 23222),
(215119, 'deDE', 'Sturmwind', '', NULL, 23222),
(215118, 'deDE', 'Eisenschmiede', '', NULL, 23222),
(215117, 'deDE', 'Die Exodar', '', NULL, 23222),
(213619, 'deDE', 'Bank', '', NULL, 23222),
(213609, 'deDE', 'Bank', '', NULL, 23222),
(212117, 'deDE', 'Briefkasten', '', NULL, 23222),
(213608, 'deDE', 'Bank', '', NULL, 23222),
(212116, 'deDE', 'Doodad_pa_anvil_hammer_002', '', NULL, 23222),
(212111, 'deDE', 'Amboss', '', NULL, 23222),
(212110, 'deDE', 'Doodad_pa_anvil_hammer_001', '', NULL, 23222),
(212109, 'deDE', 'Amboss', '', NULL, 23222),
(212108, 'deDE', 'Schmiede', '', NULL, 23222),
(215120, 'deDE', 'Shattrath', '', NULL, 23222),
(213634, 'deDE', 'Amboss', '', NULL, 23222),
(213633, 'deDE', 'Schmiede', '', NULL, 23222),
(213625, 'deDE', 'Useless Controller', '', NULL, 23222),
(213607, 'deDE', 'Bank', '', NULL, 23222),
(213598, 'deDE', 'Bank', '', NULL, 23222),
(215121, 'deDE', 'Dalaran', '', NULL, 23222),
(213635, 'deDE', 'Useless Controller', '', NULL, 23222),
(213626, 'deDE', 'Doodad_et_crystalforgecontroller_nosound001', '', NULL, 23222),
(213620, 'deDE', 'Gildentresor', '', NULL, 23222),
(213614, 'deDE', 'Gildentresor', '', NULL, 23222),
(212119, 'deDE', 'Briefkasten', '', NULL, 23222),
(213617, 'deDE', 'Gildentresor', '', NULL, 23222),
(216630, 'deDE', 'Amboss', '', NULL, 23222),
(214260, 'deDE', 'Briefkasten', '', NULL, 23222),
(215128, 'deDE', 'Collision (1.5x Player Character Size)', '', NULL, 23222),
(213616, 'deDE', 'Gildentresor', '', NULL, 23222),
(213618, 'deDE', 'Bank', '', NULL, 23222),
(213604, 'deDE', 'Stuhl', '', NULL, 23222),
(214258, 'deDE', 'Gildentresor', '', NULL, 23222),
(213602, 'deDE', 'Stuhl', '', NULL, 23222),
(213601, 'deDE', 'Stuhl', '', NULL, 23222),
(213615, 'deDE', 'Gildentresor', '', NULL, 23222),
(213636, 'deDE', 'Tisch', '', NULL, 23222),
(213600, 'deDE', 'Stuhl', '', NULL, 23222),
(212118, 'deDE', 'Briefkasten', '', NULL, 23222),
(213603, 'deDE', 'Stuhl', '', NULL, 23222),
(214424, 'deDE', 'Der Mogu und der Trogg', '', NULL, 23222),
(213227, 'deDE', 'Doodad_pa_standard_tushui_alliance004', '', NULL, 23222),
(214717, 'deDE', 'Herausforderungsrüstungssetteil für Krieger', '', NULL, 23222),
(214718, 'deDE', 'Doodad_lshoulder_challengewarrior_doodad001', '', NULL, 23222),
(214716, 'deDE', 'Doodad_rshoulder_challengewarrior_doodad001', '', NULL, 23222),
(214714, 'deDE', 'Herausforderungsrüstungssetteil für Hexenmeister', '', NULL, 23222),
(214715, 'deDE', 'Doodad_lshoulder_challengewarlock_doodad001', '', NULL, 23222),
(214713, 'deDE', 'Doodad_rshoulder_challengewarlock_doodad001', '', NULL, 23222),
(213641, 'deDE', 'Schamanenherausforderungsrüstungsset', '', NULL, 23222),
(214702, 'deDE', 'Doodad_lshoulder_challengeshaman_doodad001', '', NULL, 23222),
(214712, 'deDE', 'Doodad_rshoulder_challengeshaman_doodad001', '', NULL, 23222),
(212113, 'deDE', 'Briefkasten', '', NULL, 23222),
(214703, 'deDE', 'Doodad_lshoulder_challengerogue_doodad001', '', NULL, 23222),
(213642, 'deDE', 'Schurkenherausforderungsrüstungsset', '', NULL, 23222),
(213226, 'deDE', 'Doodad_pa_standard_tushui_alliance005', '', NULL, 23222),
(214711, 'deDE', 'Doodad_rshoulder_challengerogue_doodad001', '', NULL, 23222),
(214704, 'deDE', 'Doodad_lshoulder_challengepriest_doodad001', '', NULL, 23222),
(213643, 'deDE', 'Priesterherausforderungsrüstungsset', '', NULL, 23222),
(214710, 'deDE', 'Doodad_rshoulder_challengepriest_doodad001', '', NULL, 23222),
(214705, 'deDE', 'Doodad_lshoulder_challengepaladin_doodad001', '', NULL, 23222),
(213644, 'deDE', 'Paladinherausforderungsrüstungsset', '', NULL, 23222),
(214697, 'deDE', 'Doodad_rshoulder_challengepaladin_doodad001', '', NULL, 23222),
(214706, 'deDE', 'Doodad_lshoulder_challengemonk_doodad001', '', NULL, 23222),
(213645, 'deDE', 'Mönchherausforderungsrüstungsset', '', NULL, 23222),
(214698, 'deDE', 'Doodad_rshoulder_challengemonk_doodad001', '', NULL, 23222),
(214721, 'deDE', 'Doodad_lshoulder_challengemage_doodad001', '', NULL, 23222),
(214720, 'deDE', 'Herausforderungsrüstungssetteil für Magier', '', NULL, 23222),
(214719, 'deDE', 'Doodad_rshoulder_challengemage_doodad001', '', NULL, 23222),
(213632, 'deDE', 'Bank', '', NULL, 23222),
(213631, 'deDE', 'Bank', '', NULL, 23222),
(213630, 'deDE', 'Bank', '', NULL, 23222),
(213629, 'deDE', 'Bank', '', NULL, 23222),
(213628, 'deDE', 'Bank', '', NULL, 23222),
(213627, 'deDE', 'Bank', '', NULL, 23222),
(212115, 'deDE', 'Briefkasten', '', NULL, 23222),
(212107, 'deDE', 'Ofen', '', NULL, 23222),
(213610, 'deDE', 'Bank', '', NULL, 23222),
(213613, 'deDE', 'Bank', '', NULL, 23222),
(144111, 'deDE', 'Peins Truhe', '', NULL, 23222),
(215171, 'deDE', 'Kleines Boot', '', NULL, 23222),
(214850, 'deDE', 'Sha of Fear Vortex', '', NULL, 23222),
(214849, 'deDE', 'Sha of Fear Vortex Wall', '', NULL, 23222),
(214852, 'deDE', 'Lei Shi Vortex', '', NULL, 23222),
(214851, 'deDE', 'Lei Shi Vortex Wall', '', NULL, 23222),
(214854, 'deDE', 'Jinyu Council Vortex Wall', '', NULL, 23222),
(214853, 'deDE', 'Jinyu Council Vortex', '', NULL, 23222),
(214525, 'deDE', 'Instance Portal (Raid 4 Difficulties)', '', NULL, 23222);


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=14575 AND `id`=0) OR (`menu_id`=14680 AND `id`=2) OR (`menu_id`=14680 AND `id`=0) OR (`menu_id`=10183 AND `id`=0) OR (`menu_id`=15152 AND `id`=11) OR (`menu_id`=15152 AND `id`=10) OR (`menu_id`=15152 AND `id`=9) OR (`menu_id`=15152 AND `id`=8) OR (`menu_id`=15152 AND `id`=7) OR (`menu_id`=15152 AND `id`=6) OR (`menu_id`=15152 AND `id`=5) OR (`menu_id`=15152 AND `id`=4) OR (`menu_id`=15152 AND `id`=3) OR (`menu_id`=15152 AND `id`=2) OR (`menu_id`=15152 AND `id`=1) OR (`menu_id`=14558 AND `id`=11) OR (`menu_id`=14558 AND `id`=10) OR (`menu_id`=14558 AND `id`=9) OR (`menu_id`=14558 AND `id`=7) OR (`menu_id`=14558 AND `id`=6) OR (`menu_id`=14558 AND `id`=5) OR (`menu_id`=14558 AND `id`=4) OR (`menu_id`=14558 AND `id`=3) OR (`menu_id`=14558 AND `id`=2) OR (`menu_id`=14558 AND `id`=1) OR (`menu_id`=14404 AND `id`=0) OR (`menu_id`=10362 AND `id`=1) OR (`menu_id`=10362 AND `id`=0) OR (`menu_id`=15952 AND `id`=0) OR (`menu_id`=14504 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(14575, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14680, 2, '', '', 'Was kann ich hier in der Stadt finden?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14680, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10183, 0, '', '', 'Ich sehe mich nur mal um.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15152, 11, '', '', 'Andere', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15152, 10, '', '', 'Verkäufer', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15152, 9, '', '', 'Stallmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15152, 8, '', '', 'Transmogrifikation und Leerenlager', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15152, 7, '', '', 'Rüstmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15152, 6, '', '', 'Berufsausbilder', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15152, 5, '', '', 'Kampfhaustiertrainer', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15152, 4, '', '', 'Gasthaus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15152, 3, '', '', 'Fluglehrer', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15152, 2, '', '', 'Flugmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15152, 1, '', '', 'Bank', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14558, 11, '', '', 'Andere', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14558, 10, '', '', 'Verkäufer', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14558, 9, '', '', 'Stallmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14558, 7, '', '', 'Rüstmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14558, 6, '', '', 'Berufsausbilder', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14558, 5, '', '', 'Kampfhaustiertrainer', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14558, 4, '', '', 'Gasthaus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14558, 3, '', '', 'Fluglehrer', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14558, 2, '', '', 'Flugmeister', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14558, 1, '', '', 'Bank', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14404, 0, '', '', 'Könnt Ihr mir etwas vorspielen?', '', '', '', '', '', '', '', 'Seid Ihr sicher, dass Ihr zahlen wollt?', '', '', '', '', ''),
(10362, 1, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10362, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15952, 0, '', '', 'Untersucht seine Waren.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14504, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', '');


DELETE FROM `locales_creature_text` WHERE (`entry`=45979 AND `groupid`=0) OR (`entry`=61243 AND `groupid`=0) OR (`entry`=61243 AND `groupid`=1) OR (`entry`=61243 AND `groupid`=2) OR (`entry`=61399 AND `groupid`=0) OR (`entry`=61442 AND `groupid`=0) OR (`entry`=61442 AND `groupid`=1) OR (`entry`=61442 AND `groupid`=2) OR (`entry`=61442 AND `groupid`=3) OR (`entry`=61444 AND `groupid`=0) OR (`entry`=61444 AND `groupid`=1) OR (`entry`=61444 AND `groupid`=2) OR (`entry`=61444 AND `groupid`=3) OR (`entry`=61445 AND `groupid`=0) OR (`entry`=61445 AND `groupid`=1) OR (`entry`=61445 AND `groupid`=2) OR (`entry`=61445 AND `groupid`=3) OR (`entry`=61884 AND `groupid`=0) OR (`entry`=61884 AND `groupid`=1) OR (`entry`=62352 AND `groupid`=0) OR (`entry`=63013 AND `groupid`=0) OR (`entry`=64149 AND `groupid`=0) OR (`entry`=64149 AND `groupid`=1) OR (`entry`=64243 AND `groupid`=0) OR (`entry`=64547 AND `groupid`=0) OR (`entry`=64548 AND `groupid`=0) OR (`entry`=73330 AND `groupid`=0) OR (`entry`=73330 AND `groupid`=1) OR (`entry`=73330 AND `groupid`=2) OR (`entry`=73330 AND `groupid`=3) OR (`entry`=73330 AND `groupid`=4) OR (`entry`=73330 AND `groupid`=5) OR (`entry`=73349 AND `groupid`=0);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(45979, 0, 0, '', '', 'Ihr hört ein Knirschen im Aufzugschacht!', '', '', '', '', ''),
(61243, 0, 0, '', '', 'Ich höre etwas...', '', '', '', '', ''),
(61243, 1, 0, '', '', 'Haltet sie auf!', '', '', '', '', ''),
(61243, 2, 0, '', '', 'Was für eine Verschwendung...', '', '', '', '', ''),
(61399, 0, 0, '', '', 'Die Rufe des Spähers der Glintrok sind eine Warnung!', '', '', '', '', ''),
(61442, 0, 0, '', '', 'Klan Gurthan wird unserem König und dem Rest von Euch machthungrigen Schwindlern zeigen, warum wir zu Recht an seiner Seite stehen!', '', '', '', '', ''),
(61442, 1, 0, '', '', 'Es wird immer unser Vorrecht sein, das Schicksal unseres Volkes zu bestimmen!', '', '', '', '', ''),
(61442, 2, 0, '', '', 'Wer hat diese Eindringlinge in unsere Hallen gelassen? Nur Klan Harthak oder Klan Kargesh würden einen derartigen Verrat begehen!', '', '', '', '', ''),
(61442, 3, 0, '', '', 'Guter Versuch, aber so einfach täuscht Ihr uns nicht. Eure Klans haben sich eindeutig verbündet. Für den Ruhm unseres Königs und unseres Reichs werden wir Euch alle bezwingen!', '', '', '', '', ''),
(61444, 0, 0, '', '', 'Klan Harthak wird allen zeigen, warum sie die wahren Mogu sind!', '', '', '', '', ''),
(61444, 1, 0, '', '', '|TInterface\\Icons\\inv_elemental_primal_air.blp:20|t Ming der Verschlagene beschwört einen |cFFFF0000|Hspell:119981|h[wirbelnden Derwisch]|h|r!', '', '', '', '', ''),
(61444, 2, 0, '', '', 'Unser Klan ist der wahre Klan! Kein Eindringling vermag daran etwas zu ändern!', '', '', '', '', ''),
(61444, 3, 0, '', '', 'Nur die Hunde vom Klan Gurthan würden andere für ihre eigenen Fehler verantwortlich machen! Euer Klan ist der verzweifeltste. Ihr wart es ganz bestimmt!', '', '', '', '', ''),
(61445, 0, 0, '', '', 'Ohne die Stärke unseres Klans werden die Mogu ihren rechtmäßigen Ruhm nicht zurückerlangen!', '', '', '', '', ''),
(61445, 1, 0, '', '', 'Klan Kargesh wird Euch zeigen, warum nur die Starken es verdient haben, an der Seite des Königs zu stehen!', '', '', '', '', ''),
(61445, 2, 0, '', '', 'Unmöglich! Wir besitzen die größte Macht im ganzen Reich!', '', '', '', '', ''),
(61445, 3, 0, '', '', 'Klan Gurthan hat Recht! Nur der ehrlose Klan Harthak würde minderwertige Wesen seine Arbeit verrichten lassen. Ihr müsst Verräter sein!', '', '', '', '', ''),
(61884, 0, 0, '', '', 'Ihr seid alle nutzlos! Die Wachen, die Ihr mir als Tribut überlasst, können nicht einmal diese minderwertigen Wesen von meinem Palast fernhalten.', '', '', '', '', ''),
(61884, 1, 0, '', '', 'Jetzt habt Ihr die Gelegenheit, Euch zu bewähren. Nehmt Euch dieser Eindringlinge an. Der Klan, der mir ihre Köpfe bringt, wird in meiner Gunst stehen.', '', '', '', '', ''),
(62352, 0, 0, '', '', 'Plündern und brandschatzen, bwahahaha!', '', '', '', '', ''),
(63013, 0, 0, '', '', 'Kommt, setzt Euch und esst! Es ist nie die falsche Zeit für eine deftige Mahlzeit!', '', '', '', '', ''),
(64149, 0, 0, '', '', 'Willkommen in der Goldenen Laterne, dem besten Gasthaus im Tal. Wenn jemand das bestreitet, bekommt er meine Fäuste zu spüren.', '', '', '', '', ''),
(64149, 1, 0, '', '', 'Willkommen. Lasst Euch hier nieder, wenn Ihr wünscht. Ihr findet die Bankfächer und die Stadttore im oberen Stockwerk. Händler aller Arten richten im ganzen Gebäude bereits ihre Läden ein. Bitte erkundet ruhig auf eigene Faust.', '', '', '', '', ''),
(64243, 0, 0, '', '', 'Ein Saurok läuft über eine versteckte Treppe mit einem Teil des Schatzes davon.', '', '', '', '', ''),
(64547, 0, 0, '', '', 'Geht wieder da raus und kämpft!', '', '', '', '', ''),
(64548, 0, 0, '', '', 'Ihr legt Euch mit dem falschen Mogu an, $R.', '', '', '', '', ''),
(73330, 0, 0, '', '', 'Ah, wir sind wieder gemeinsam auf Reisen. Nur diesmal, fürchte ich, sind die Umstände deutlich ernster.', '', '', '', '', ''),
(73330, 1, 0, '', '', 'Das Land ist aufgewühlt, die immergrünen Bäume und Pflanzen verwelken, während das Wasser aus den Teichen abfließt.', '', '', '', '', ''),
(73330, 2, 0, '', '', 'Einst haben die Titanen diese lebensspendenden Gewässer benutzt, um alles Leben in Pandaria zu formen.', '', '', '', '', ''),
(73330, 3, 0, '', '', 'Es war dieses Wasser, das das Tal am Leben gehalten hat. Das überschüssige Wasser verwandelte das Tal der Vier Winde in das fruchtbarste Land der Welt!', '', '', '', '', ''),
(73330, 4, 0, '', '', 'Und nun hat sich die Boshaftigkeit des Alten Gottes in dem Wasser manifestiert.', '', '', '', '', ''),
(73330, 5, 0, '', '', 'Was für ein widerwärtiges Wesen - zerstört es, bevor es tief in den Boden sickert und ganz Pandaria besudelt!', '', '', '', '', ''),
(73349, 0, 0, '', '', 'Wir müssen die Becken vor der Verderbnis schützen!', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=61373 /*61373*/ AND `locale`='deDE') OR (`entry`=63953 /*63953*/ AND `locale`='deDE') OR (`entry`=65191 /*65191*/ AND `locale`='deDE') OR (`entry`=61426 /*61426*/ AND `locale`='deDE') OR (`entry`=64803 /*64803*/ AND `locale`='deDE') OR (`entry`=64238 /*64238*/ AND `locale`='deDE') OR (`entry`=66188 /*66188*/ AND `locale`='deDE') OR (`entry`=64805 /*64805*/ AND `locale`='deDE') OR (`entry`=61466 /*61466*/ AND `locale`='deDE') OR (`entry`=62077 /*62077*/ AND `locale`='deDE') OR (`entry`=64275 /*64275*/ AND `locale`='deDE') OR (`entry`=64369 /*64369*/ AND `locale`='deDE') OR (`entry`=64648 /*64648*/ AND `locale`='deDE') OR (`entry`=63653 /*63653*/ AND `locale`='deDE') OR (`entry`=58947 /*58947*/ AND `locale`='deDE') OR (`entry`=63641 /*63641*/ AND `locale`='deDE') OR (`entry`=63634 /*63634*/ AND `locale`='deDE') OR (`entry`=63640 /*63640*/ AND `locale`='deDE') OR (`entry`=63939 /*63939*/ AND `locale`='deDE') OR (`entry`=65267 /*65267*/ AND `locale`='deDE') OR (`entry`=64608 /*64608*/ AND `locale`='deDE') OR (`entry`=64567 /*64567*/ AND `locale`='deDE') OR (`entry`=65132 /*65132*/ AND `locale`='deDE') OR (`entry`=65131 /*65131*/ AND `locale`='deDE') OR (`entry`=58820 /*58820*/ AND `locale`='deDE') OR (`entry`=58455 /*58455*/ AND `locale`='deDE') OR (`entry`=58453 /*58453*/ AND `locale`='deDE') OR (`entry`=66741 /*66741*/ AND `locale`='deDE') OR (`entry`=66728 /*66728*/ AND `locale`='deDE') OR (`entry`=66726 /*66726*/ AND `locale`='deDE') OR (`entry`=66725 /*66725*/ AND `locale`='deDE') OR (`entry`=58452 /*58452*/ AND `locale`='deDE') OR (`entry`=63244 /*63244*/ AND `locale`='deDE') OR (`entry`=59024 /*59024*/ AND `locale`='deDE') OR (`entry`=58456 /*58456*/ AND `locale`='deDE') OR (`entry`=58962 /*58962*/ AND `locale`='deDE') OR (`entry`=58994 /*58994*/ AND `locale`='deDE') OR (`entry`=65129 /*65129*/ AND `locale`='deDE') OR (`entry`=58919 /*58919*/ AND `locale`='deDE') OR (`entry`=58920 /*58920*/ AND `locale`='deDE') OR (`entry`=58454 /*58454*/ AND `locale`='deDE') OR (`entry`=58704 /*58704*/ AND `locale`='deDE') OR (`entry`=65136 /*65136*/ AND `locale`='deDE') OR (`entry`=58992 /*58992*/ AND `locale`='deDE') OR (`entry`=65140 /*65140*/ AND `locale`='deDE') OR (`entry`=58743 /*58743*/ AND `locale`='deDE') OR (`entry`=58953 /*58953*/ AND `locale`='deDE') OR (`entry`=67004 /*67004*/ AND `locale`='deDE') OR (`entry`=58744 /*58744*/ AND `locale`='deDE') OR (`entry`=61982 /*61982*/ AND `locale`='deDE') OR (`entry`=61968 /*61968*/ AND `locale`='deDE') OR (`entry`=62143 /*62143*/ AND `locale`='deDE') OR (`entry`=62000 /*62000*/ AND `locale`='deDE') OR (`entry`=61981 /*61981*/ AND `locale`='deDE') OR (`entry`=68559 /*68559*/ AND `locale`='deDE') OR (`entry`=58731 /*58731*/ AND `locale`='deDE') OR (`entry`=58449 /*58449*/ AND `locale`='deDE') OR (`entry`=64274 /*64274*/ AND `locale`='deDE') OR (`entry`=64646 /*64646*/ AND `locale`='deDE') OR (`entry`=64524 /*64524*/ AND `locale`='deDE') OR (`entry`=65336 /*65336*/ AND `locale`='deDE') OR (`entry`=63972 /*63972*/ AND `locale`='deDE') OR (`entry`=65335 /*65335*/ AND `locale`='deDE') OR (`entry`=64687 /*64687*/ AND `locale`='deDE') OR (`entry`=64692 /*64692*/ AND `locale`='deDE') OR (`entry`=64336 /*64336*/ AND `locale`='deDE') OR (`entry`=63976 /*63976*/ AND `locale`='deDE') OR (`entry`=63652 /*63652*/ AND `locale`='deDE') OR (`entry`=58930 /*58930*/ AND `locale`='deDE') OR (`entry`=58928 /*58928*/ AND `locale`='deDE') OR (`entry`=66391 /*66391*/ AND `locale`='deDE') OR (`entry`=63973 /*63973*/ AND `locale`='deDE') OR (`entry`=59053 /*59053*/ AND `locale`='deDE') OR (`entry`=60932 /*60932*/ AND `locale`='deDE') OR (`entry`=66368 /*66368*/ AND `locale`='deDE') OR (`entry`=58927 /*58927*/ AND `locale`='deDE') OR (`entry`=63605 /*63605*/ AND `locale`='deDE') OR (`entry`=60294 /*60294*/ AND `locale`='deDE') OR (`entry`=66881 /*66881*/ AND `locale`='deDE') OR (`entry`=65374 /*65374*/ AND `locale`='deDE') OR (`entry`=64702 /*64702*/ AND `locale`='deDE') OR (`entry`=63654 /*63654*/ AND `locale`='deDE') OR (`entry`=65795 /*65795*/ AND `locale`='deDE') OR (`entry`=65796 /*65796*/ AND `locale`='deDE') OR (`entry`=58693 /*58693*/ AND `locale`='deDE') OR (`entry`=63935 /*63935*/ AND `locale`='deDE') OR (`entry`=65789 /*65789*/ AND `locale`='deDE') OR (`entry`=63934 /*63934*/ AND `locale`='deDE') OR (`entry`=58460 /*58460*/ AND `locale`='deDE') OR (`entry`=58767 /*58767*/ AND `locale`='deDE') OR (`entry`=58457 /*58457*/ AND `locale`='deDE') OR (`entry`=63044 /*63044*/ AND `locale`='deDE') OR (`entry`=63081 /*63081*/ AND `locale`='deDE') OR (`entry`=63082 /*63082*/ AND `locale`='deDE') OR (`entry`=63065 /*63065*/ AND `locale`='deDE') OR (`entry`=59378 /*59378*/ AND `locale`='deDE') OR (`entry`=66860 /*66860*/ AND `locale`='deDE') OR (`entry`=58412 /*58412*/ AND `locale`='deDE') OR (`entry`=59158 /*59158*/ AND `locale`='deDE') OR (`entry`=59156 /*59156*/ AND `locale`='deDE') OR (`entry`=71544 /*71544*/ AND `locale`='deDE') OR (`entry`=71548 /*71548*/ AND `locale`='deDE') OR (`entry`=73226 /*73226*/ AND `locale`='deDE') OR (`entry`=73283 /*73283*/ AND `locale`='deDE') OR (`entry`=71543 /*71543*/ AND `locale`='deDE') OR (`entry`=73488 /*73488*/ AND `locale`='deDE') OR (`entry`=73222 /*73222*/ AND `locale`='deDE') OR (`entry`=71550 /*71550*/ AND `locale`='deDE') OR (`entry`=73191 /*73191*/ AND `locale`='deDE') OR (`entry`=73260 /*73260*/ AND `locale`='deDE') OR (`entry`=73197 /*73197*/ AND `locale`='deDE') OR (`entry`=73342 /*73342*/ AND `locale`='deDE') OR (`entry`=73192 /*73192*/ AND `locale`='deDE') OR (`entry`=73349 /*73349*/ AND `locale`='deDE') OR (`entry`=70483 /*70483*/ AND `locale`='deDE') OR (`entry`=73330 /*73330*/ AND `locale`='deDE') OR (`entry`=65138 /*65138*/ AND `locale`='deDE') OR (`entry`=61679 /*61679*/ AND `locale`='deDE') OR (`entry`=61451 /*61451*/ AND `locale`='deDE') OR (`entry`=61239 /*61239*/ AND `locale`='deDE') OR (`entry`=61433 /*61433*/ AND `locale`='deDE') OR (`entry`=61340 /*61340*/ AND `locale`='deDE') OR (`entry`=61337 /*61337*/ AND `locale`='deDE') OR (`entry`=61339 /*61339*/ AND `locale`='deDE') OR (`entry`=61338 /*61338*/ AND `locale`='deDE') OR (`entry`=63739 /*63739*/ AND `locale`='deDE') OR (`entry`=61216 /*61216*/ AND `locale`='deDE') OR (`entry`=61242 /*61242*/ AND `locale`='deDE') OR (`entry`=61240 /*61240*/ AND `locale`='deDE') OR (`entry`=61399 /*61399*/ AND `locale`='deDE') OR (`entry`=64250 /*64250*/ AND `locale`='deDE') OR (`entry`=64243 /*64243*/ AND `locale`='deDE') OR (`entry`=61626 /*61626*/ AND `locale`='deDE') OR (`entry`=65402 /*65402*/ AND `locale`='deDE') OR (`entry`=61243 /*61243*/ AND `locale`='deDE') OR (`entry`=61550 /*61550*/ AND `locale`='deDE') OR (`entry`=61389 /*61389*/ AND `locale`='deDE') OR (`entry`=61392 /*61392*/ AND `locale`='deDE') OR (`entry`=61449 /*61449*/ AND `locale`='deDE') OR (`entry`=61247 /*61247*/ AND `locale`='deDE') OR (`entry`=61478 /*61478*/ AND `locale`='deDE') OR (`entry`=67231 /*67231*/ AND `locale`='deDE') OR (`entry`=61450 /*61450*/ AND `locale`='deDE') OR (`entry`=61447 /*61447*/ AND `locale`='deDE') OR (`entry`=61455 /*61455*/ AND `locale`='deDE') OR (`entry`=61549 /*61549*/ AND `locale`='deDE') OR (`entry`=61444 /*61444*/ AND `locale`='deDE') OR (`entry`=64547 /*64547*/ AND `locale`='deDE') OR (`entry`=61551 /*61551*/ AND `locale`='deDE') OR (`entry`=64548 /*64548*/ AND `locale`='deDE') OR (`entry`=61884 /*61884*/ AND `locale`='deDE') OR (`entry`=61453 /*61453*/ AND `locale`='deDE') OR (`entry`=61445 /*61445*/ AND `locale`='deDE') OR (`entry`=61442 /*61442*/ AND `locale`='deDE') OR (`entry`=61432 /*61432*/ AND `locale`='deDE') OR (`entry`=61431 /*61431*/ AND `locale`='deDE') OR (`entry`=61415 /*61415*/ AND `locale`='deDE') OR (`entry`=61947 /*61947*/ AND `locale`='deDE') OR (`entry`=61946 /*61946*/ AND `locale`='deDE') OR (`entry`=64432 /*64432*/ AND `locale`='deDE') OR (`entry`=61945 /*61945*/ AND `locale`='deDE') OR (`entry`=64922 /*64922*/ AND `locale`='deDE') OR (`entry`=63984 /*63984*/ AND `locale`='deDE') OR (`entry`=63983 /*63983*/ AND `locale`='deDE') OR (`entry`=11704 /*11704*/ AND `locale`='deDE') OR (`entry`=80633 /*80633*/ AND `locale`='deDE') OR (`entry`=70976 /*70976*/ AND `locale`='deDE') OR (`entry`=64605 /*64605*/ AND `locale`='deDE') OR (`entry`=78777 /*78777*/ AND `locale`='deDE') OR (`entry`=78709 /*78709*/ AND `locale`='deDE') OR (`entry`=64691 /*64691*/ AND `locale`='deDE') OR (`entry`=70171 /*70171*/ AND `locale`='deDE') OR (`entry`=72283 /*72283*/ AND `locale`='deDE') OR (`entry`=73691 /*73691*/ AND `locale`='deDE') OR (`entry`=66972 /*66972*/ AND `locale`='deDE') OR (`entry`=66970 /*66970*/ AND `locale`='deDE') OR (`entry`=66973 /*66973*/ AND `locale`='deDE') OR (`entry`=66971 /*66971*/ AND `locale`='deDE') OR (`entry`=65851 /*65851*/ AND `locale`='deDE') OR (`entry`=50951 /*50951*/ AND `locale`='deDE') OR (`entry`=36911 /*36911*/ AND `locale`='deDE') OR (`entry`=67013 /*67013*/ AND `locale`='deDE') OR (`entry`=67014 /*67014*/ AND `locale`='deDE') OR (`entry`=68981 /*68981*/ AND `locale`='deDE') OR (`entry`=64043 /*64043*/ AND `locale`='deDE') OR (`entry`=64040 /*64040*/ AND `locale`='deDE') OR (`entry`=64585 /*64585*/ AND `locale`='deDE') OR (`entry`=64041 /*64041*/ AND `locale`='deDE') OR (`entry`=64925 /*64925*/ AND `locale`='deDE') OR (`entry`=64924 /*64924*/ AND `locale`='deDE') OR (`entry`=67130 /*67130*/ AND `locale`='deDE') OR (`entry`=64114 /*64114*/ AND `locale`='deDE') OR (`entry`=73422 /*73422*/ AND `locale`='deDE') OR (`entry`=64059 /*64059*/ AND `locale`='deDE') OR (`entry`=63966 /*63966*/ AND `locale`='deDE') OR (`entry`=64060 /*64060*/ AND `locale`='deDE') OR (`entry`=64056 /*64056*/ AND `locale`='deDE') OR (`entry`=64049 /*64049*/ AND `locale`='deDE') OR (`entry`=64050 /*64050*/ AND `locale`='deDE') OR (`entry`=64053 /*64053*/ AND `locale`='deDE') OR (`entry`=65266 /*65266*/ AND `locale`='deDE') OR (`entry`=64058 /*64058*/ AND `locale`='deDE') OR (`entry`=64057 /*64057*/ AND `locale`='deDE') OR (`entry`=64054 /*64054*/ AND `locale`='deDE') OR (`entry`=64048 /*64048*/ AND `locale`='deDE') OR (`entry`=63965 /*63965*/ AND `locale`='deDE') OR (`entry`=63964 /*63964*/ AND `locale`='deDE') OR (`entry`=65770 /*65770*/ AND `locale`='deDE') OR (`entry`=68984 /*68984*/ AND `locale`='deDE') OR (`entry`=64051 /*64051*/ AND `locale`='deDE') OR (`entry`=64126 /*64126*/ AND `locale`='deDE') OR (`entry`=64127 /*64127*/ AND `locale`='deDE') OR (`entry`=64128 /*64128*/ AND `locale`='deDE') OR (`entry`=64588 /*64588*/ AND `locale`='deDE') OR (`entry`=73674 /*73674*/ AND `locale`='deDE') OR (`entry`=62996 /*62996*/ AND `locale`='deDE') OR (`entry`=64067 /*64067*/ AND `locale`='deDE') OR (`entry`=64107 /*64107*/ AND `locale`='deDE') OR (`entry`=64106 /*64106*/ AND `locale`='deDE') OR (`entry`=63753 /*63753*/ AND `locale`='deDE') OR (`entry`=63752 /*63752*/ AND `locale`='deDE') OR (`entry`=74010 /*74010*/ AND `locale`='deDE') OR (`entry`=64119 /*64119*/ AND `locale`='deDE') OR (`entry`=74012 /*74012*/ AND `locale`='deDE') OR (`entry`=66998 /*66998*/ AND `locale`='deDE') OR (`entry`=64118 /*64118*/ AND `locale`='deDE') OR (`entry`=65862 /*65862*/ AND `locale`='deDE') OR (`entry`=64062 /*64062*/ AND `locale`='deDE') OR (`entry`=65824 /*65824*/ AND `locale`='deDE') OR (`entry`=64121 /*64121*/ AND `locale`='deDE') OR (`entry`=64065 /*64065*/ AND `locale`='deDE') OR (`entry`=74019 /*74019*/ AND `locale`='deDE') OR (`entry`=64066 /*64066*/ AND `locale`='deDE') OR (`entry`=64047 /*64047*/ AND `locale`='deDE') OR (`entry`=63008 /*63008*/ AND `locale`='deDE') OR (`entry`=64134 /*64134*/ AND `locale`='deDE') OR (`entry`=64122 /*64122*/ AND `locale`='deDE') OR (`entry`=67133 /*67133*/ AND `locale`='deDE') OR (`entry`=66731 /*66731*/ AND `locale`='deDE') OR (`entry`=67134 /*67134*/ AND `locale`='deDE') OR (`entry`=64131 /*64131*/ AND `locale`='deDE') OR (`entry`=64579 /*64579*/ AND `locale`='deDE') OR (`entry`=64578 /*64578*/ AND `locale`='deDE') OR (`entry`=64575 /*64575*/ AND `locale`='deDE') OR (`entry`=64561 /*64561*/ AND `locale`='deDE') OR (`entry`=64534 /*64534*/ AND `locale`='deDE') OR (`entry`=64587 /*64587*/ AND `locale`='deDE') OR (`entry`=103679 /*103679*/ AND `locale`='deDE') OR (`entry`=64002 /*64002*/ AND `locale`='deDE') OR (`entry`=64535 /*64535*/ AND `locale`='deDE') OR (`entry`=64019 /*64019*/ AND `locale`='deDE') OR (`entry`=63994 /*63994*/ AND `locale`='deDE') OR (`entry`=63996 /*63996*/ AND `locale`='deDE') OR (`entry`=64976 /*64976*/ AND `locale`='deDE') OR (`entry`=64972 /*64972*/ AND `locale`='deDE') OR (`entry`=66685 /*66685*/ AND `locale`='deDE') OR (`entry`=64533 /*64533*/ AND `locale`='deDE') OR (`entry`=64531 /*64531*/ AND `locale`='deDE') OR (`entry`=64007 /*64007*/ AND `locale`='deDE') OR (`entry`=61122 /*61122*/ AND `locale`='deDE') OR (`entry`=59959 /*59959*/ AND `locale`='deDE') OR (`entry`=64011 /*64011*/ AND `locale`='deDE') OR (`entry`=63986 /*63986*/ AND `locale`='deDE') OR (`entry`=63987 /*63987*/ AND `locale`='deDE') OR (`entry`=64591 /*64591*/ AND `locale`='deDE') OR (`entry`=64590 /*64590*/ AND `locale`='deDE') OR (`entry`=64589 /*64589*/ AND `locale`='deDE') OR (`entry`=64582 /*64582*/ AND `locale`='deDE') OR (`entry`=64010 /*64010*/ AND `locale`='deDE') OR (`entry`=64001 /*64001*/ AND `locale`='deDE') OR (`entry`=60167 /*60167*/ AND `locale`='deDE') OR (`entry`=71483 /*71483*/ AND `locale`='deDE') OR (`entry`=64566 /*64566*/ AND `locale`='deDE') OR (`entry`=58245 /*58245*/ AND `locale`='deDE') OR (`entry`=64584 /*64584*/ AND `locale`='deDE') OR (`entry`=65207 /*65207*/ AND `locale`='deDE') OR (`entry`=66861 /*66861*/ AND `locale`='deDE') OR (`entry`=63076 /*63076*/ AND `locale`='deDE') OR (`entry`=58411 /*58411*/ AND `locale`='deDE') OR (`entry`=58671 /*58671*/ AND `locale`='deDE') OR (`entry`=58669 /*58669*/ AND `locale`='deDE') OR (`entry`=73818 /*73818*/ AND `locale`='deDE') OR (`entry`=61650 /*61650*/ AND `locale`='deDE') OR (`entry`=66386 /*66386*/ AND `locale`='deDE') OR (`entry`=67829 /*67829*/ AND `locale`='deDE') OR (`entry`=67851 /*67851*/ AND `locale`='deDE') OR (`entry`=63847 /*63847*/ AND `locale`='deDE') OR (`entry`=67679 /*67679*/ AND `locale`='deDE') OR (`entry`=100324 /*100324*/ AND `locale`='deDE') OR (`entry`=63838 /*63838*/ AND `locale`='deDE') OR (`entry`=100409 /*100409*/ AND `locale`='deDE') OR (`entry`=35395 /*35395*/ AND `locale`='deDE') OR (`entry`=3504 /*3504*/ AND `locale`='deDE') OR (`entry`=2331 /*2331*/ AND `locale`='deDE') OR (`entry`=2330 /*2330*/ AND `locale`='deDE') OR (`entry`=29452 /*29452*/ AND `locale`='deDE') OR (`entry`=1478 /*1478*/ AND `locale`='deDE') OR (`entry`=1477 /*1477*/ AND `locale`='deDE') OR (`entry`=7382 /*7382*/ AND `locale`='deDE') OR (`entry`=30730 /*30730*/ AND `locale`='deDE') OR (`entry`=106152 /*106152*/ AND `locale`='deDE') OR (`entry`=64160 /*64160*/ AND `locale`='deDE') OR (`entry`=64164 /*64164*/ AND `locale`='deDE') OR (`entry`=64094 /*64094*/ AND `locale`='deDE') OR (`entry`=64079 /*64079*/ AND `locale`='deDE') OR (`entry`=65579 /*65579*/ AND `locale`='deDE') OR (`entry`=64161 /*64161*/ AND `locale`='deDE') OR (`entry`=64124 /*64124*/ AND `locale`='deDE') OR (`entry`=64125 /*64125*/ AND `locale`='deDE') OR (`entry`=65585 /*65585*/ AND `locale`='deDE') OR (`entry`=64097 /*64097*/ AND `locale`='deDE') OR (`entry`=64090 /*64090*/ AND `locale`='deDE') OR (`entry`=64085 /*64085*/ AND `locale`='deDE') OR (`entry`=68982 /*68982*/ AND `locale`='deDE') OR (`entry`=64573 /*64573*/ AND `locale`='deDE') OR (`entry`=64083 /*64083*/ AND `locale`='deDE') OR (`entry`=64081 /*64081*/ AND `locale`='deDE') OR (`entry`=64080 /*64080*/ AND `locale`='deDE') OR (`entry`=64052 /*64052*/ AND `locale`='deDE') OR (`entry`=64574 /*64574*/ AND `locale`='deDE') OR (`entry`=64153 /*64153*/ AND `locale`='deDE') OR (`entry`=64092 /*64092*/ AND `locale`='deDE') OR (`entry`=63969 /*63969*/ AND `locale`='deDE') OR (`entry`=65599 /*65599*/ AND `locale`='deDE') OR (`entry`=64100 /*64100*/ AND `locale`='deDE') OR (`entry`=65576 /*65576*/ AND `locale`='deDE') OR (`entry`=65596 /*65596*/ AND `locale`='deDE') OR (`entry`=65583 /*65583*/ AND `locale`='deDE') OR (`entry`=63970 /*63970*/ AND `locale`='deDE') OR (`entry`=63968 /*63968*/ AND `locale`='deDE') OR (`entry`=64024 /*64024*/ AND `locale`='deDE') OR (`entry`=63967 /*63967*/ AND `locale`='deDE') OR (`entry`=64023 /*64023*/ AND `locale`='deDE') OR (`entry`=63971 /*63971*/ AND `locale`='deDE') OR (`entry`=64147 /*64147*/ AND `locale`='deDE') OR (`entry`=64077 /*64077*/ AND `locale`='deDE') OR (`entry`=65577 /*65577*/ AND `locale`='deDE') OR (`entry`=64145 /*64145*/ AND `locale`='deDE') OR (`entry`=64144 /*64144*/ AND `locale`='deDE') OR (`entry`=64078 /*64078*/ AND `locale`='deDE') OR (`entry`=64481 /*64481*/ AND `locale`='deDE') OR (`entry`=65578 /*65578*/ AND `locale`='deDE') OR (`entry`=63013 /*63013*/ AND `locale`='deDE') OR (`entry`=64096 /*64096*/ AND `locale`='deDE') OR (`entry`=64482 /*64482*/ AND `locale`='deDE') OR (`entry`=64146 /*64146*/ AND `locale`='deDE') OR (`entry`=64076 /*64076*/ AND `locale`='deDE') OR (`entry`=64149 /*64149*/ AND `locale`='deDE') OR (`entry`=64157 /*64157*/ AND `locale`='deDE') OR (`entry`=65574 /*65574*/ AND `locale`='deDE') OR (`entry`=64154 /*64154*/ AND `locale`='deDE') OR (`entry`=64172 /*64172*/ AND `locale`='deDE') OR (`entry`=74027 /*74027*/ AND `locale`='deDE') OR (`entry`=64138 /*64138*/ AND `locale`='deDE') OR (`entry`=74021 /*74021*/ AND `locale`='deDE') OR (`entry`=64150 /*64150*/ AND `locale`='deDE') OR (`entry`=64101 /*64101*/ AND `locale`='deDE') OR (`entry`=74022 /*74022*/ AND `locale`='deDE') OR (`entry`=64483 /*64483*/ AND `locale`='deDE') OR (`entry`=64169 /*64169*/ AND `locale`='deDE') OR (`entry`=64168 /*64168*/ AND `locale`='deDE') OR (`entry`=64167 /*64167*/ AND `locale`='deDE') OR (`entry`=64166 /*64166*/ AND `locale`='deDE') OR (`entry`=64113 /*64113*/ AND `locale`='deDE') OR (`entry`=64112 /*64112*/ AND `locale`='deDE') OR (`entry`=64104 /*64104*/ AND `locale`='deDE') OR (`entry`=64084 /*64084*/ AND `locale`='deDE') OR (`entry`=64082 /*64082*/ AND `locale`='deDE') OR (`entry`=64075 /*64075*/ AND `locale`='deDE') OR (`entry`=64073 /*64073*/ AND `locale`='deDE') OR (`entry`=64477 /*64477*/ AND `locale`='deDE') OR (`entry`=64476 /*64476*/ AND `locale`='deDE') OR (`entry`=64129 /*64129*/ AND `locale`='deDE') OR (`entry`=64130 /*64130*/ AND `locale`='deDE') OR (`entry`=64352 /*64352*/ AND `locale`='deDE') OR (`entry`=115785 /*115785*/ AND `locale`='deDE') OR (`entry`=64042 /*64042*/ AND `locale`='deDE') OR (`entry`=64074 /*64074*/ AND `locale`='deDE') OR (`entry`=64111 /*64111*/ AND `locale`='deDE') OR (`entry`=65737 /*65737*/ AND `locale`='deDE') OR (`entry`=64136 /*64136*/ AND `locale`='deDE') OR (`entry`=510 /*510*/ AND `locale`='deDE') OR (`entry`=63842 /*63842*/ AND `locale`='deDE') OR (`entry`=61088 /*61088*/ AND `locale`='deDE') OR (`entry`=65736 /*65736*/ AND `locale`='deDE') OR (`entry`=60957 /*60957*/ AND `locale`='deDE') OR (`entry`=60999 /*60999*/ AND `locale`='deDE') OR (`entry`=63025 /*63025*/ AND `locale`='deDE') OR (`entry`=60585 /*60585*/ AND `locale`='deDE') OR (`entry`=60586 /*60586*/ AND `locale`='deDE') OR (`entry`=66100 /*66100*/ AND `locale`='deDE') OR (`entry`=60583 /*60583*/ AND `locale`='deDE') OR (`entry`=64368 /*64368*/ AND `locale`='deDE') OR (`entry`=64846 /*64846*/ AND `locale`='deDE') OR (`entry`=36737 /*36737*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(61373, 'deDE', 'Fährtenleger der Krik''thik', NULL, NULL, NULL, 23222), -- 61373
(63953, 'deDE', 'Kuitanmungo', NULL, NULL, 'wildpetcapturable', 23222), -- 63953
(65191, 'deDE', 'Mungo', NULL, NULL, NULL, 23222), -- 65191
(61426, 'deDE', 'Shado-Pan-Stachelfalle', NULL, NULL, 'interact', 23222), -- 61426
(64803, 'deDE', 'Trübigel', NULL, NULL, NULL, 23222), -- 64803
(64238, 'deDE', 'Unverwüstliche Schabe', NULL, NULL, 'wildpetcapturable', 23222), -- 64238
(66188, 'deDE', 'Bernschuppenbasilisk', NULL, NULL, NULL, 23222), -- 66188
(64805, 'deDE', 'Knuspriger Skorpion', NULL, NULL, NULL, 23222), -- 64805
(61466, 'deDE', 'Fährtenleger der Krik''thik', NULL, NULL, NULL, 23222), -- 61466
(62077, 'deDE', 'Schreckensspinnerei', NULL, NULL, NULL, 23222), -- 62077
(64275, 'deDE', 'Katapult der Krik''thik', NULL, NULL, NULL, 23222), -- 64275
(64369, 'deDE', 'Kessel mit heißem Öl', NULL, NULL, 'vehichlecursor', 23222), -- 64369
(64648, 'deDE', 'Shado-Pan-Wachhauptmann', NULL, NULL, NULL, 23222), -- 64648
(63653, 'deDE', 'Gefangener des Goldenen Lotus', NULL, NULL, NULL, 23222), -- 63653
(58947, 'deDE', 'Shado-Pan-Späher', 'Shado-Pan-Späherin', NULL, 'speak', 23222), -- 58947
(63641, 'deDE', 'Folterer der Shao-Tien', NULL, NULL, NULL, 23222), -- 63641
(63634, 'deDE', 'Mogu Tent Fire Bunny', NULL, NULL, NULL, 23222), -- 63634
(63640, 'deDE', 'Gefangener des Goldenen Lotus', NULL, NULL, NULL, 23222), -- 63640
(63939, 'deDE', 'Wachposten des Goldenen Lotus', NULL, NULL, NULL, 23222), -- 63939
(65267, 'deDE', 'Bierfass von Nebelhauch', NULL, NULL, NULL, 23222), -- 65267
(64608, 'deDE', 'Seelenfragment', NULL, NULL, NULL, 23222), -- 64608
(64567, 'deDE', 'Fleischfresser der Bleichklingen', NULL, NULL, NULL, 23222), -- 64567
(65132, 'deDE', 'Eroberer der Shao-Tien', NULL, NULL, NULL, 23222), -- 65132
(65131, 'deDE', 'Schmerzwirker der Shao-Tien', NULL, NULL, NULL, 23222), -- 65131
(58820, 'deDE', 'Händler Benny', NULL, NULL, NULL, 23222), -- 58820
(58455, 'deDE', 'Stillwasserkrokilisk', NULL, NULL, NULL, 23222), -- 58455
(58453, 'deDE', 'Scheckige Kuh', NULL, NULL, NULL, 23222), -- 58453
(66741, 'deDE', 'Aki die Auserwählte', NULL, 'Großmeistertierzähmerin', NULL, 23222), -- 66741
(66728, 'deDE', 'Zirpsi', NULL, NULL, NULL, 23222), -- 66728
(66726, 'deDE', 'Sturmpeitscher', NULL, NULL, NULL, 23222), -- 66726
(66725, 'deDE', 'Schnurra', NULL, NULL, NULL, 23222), -- 66725
(58452, 'deDE', 'Silbriger Karpfen', NULL, NULL, NULL, 23222), -- 58452
(63244, 'deDE', 'Spirit Channel Target Bunny', NULL, NULL, NULL, 23222), -- 63244
(59024, 'deDE', 'Schönlandgazelle', NULL, NULL, NULL, 23222), -- 59024
(58456, 'deDE', 'Donnermaul', NULL, NULL, NULL, 23222), -- 58456
(58962, 'deDE', 'Hai-Me Plumpfaust', NULL, 'Ausbildungsmeister', NULL, 23222), -- 58962
(58994, 'deDE', 'Battle Ring Controller', NULL, NULL, NULL, 23222), -- 58994
(65129, 'deDE', 'Zenmeister Lao', NULL, 'Schmiedekunstlehrer', NULL, 23222), -- 65129
(58919, 'deDE', 'Anji Herbstlicht', NULL, NULL, NULL, 23222), -- 58919
(58920, 'deDE', 'Kun Herbstlicht', NULL, NULL, NULL, 23222), -- 58920
(58454, 'deDE', 'Schönlandgazelle', NULL, NULL, NULL, 23222), -- 58454
(58704, 'deDE', 'Kelari Federfuß', NULL, 'Meister des Rollenden Donners', NULL, 23222), -- 58704
(65136, 'deDE', 'Vorhut des Goldenen Lotus', 'Vorhut des Goldenen Lotus', NULL, NULL, 23222), -- 65136
(58992, 'deDE', 'Shado-Pan-Auszubildender', NULL, NULL, NULL, 23222), -- 58992
(65140, 'deDE', 'Wegbewacher des Goldenen Lotus', 'Wegbewacherin des Goldenen Lotus', NULL, NULL, 23222), -- 65140
(58743, 'deDE', 'Yumi Goldtatze', NULL, 'Ausbildungsmeister', NULL, 23222), -- 58743
(58953, 'deDE', 'Survival Ring Flames Controller', NULL, NULL, NULL, 23222), -- 58953
(67004, 'deDE', 'Ehrgeiziger Schüler', 'Ehrgeizige Schülerin', NULL, NULL, 23222), -- 67004
(58744, 'deDE', 'Shado-Pan-Ausbilder', NULL, NULL, NULL, 23222), -- 58744
(61982, 'deDE', 'Schreckensspinnermauerkriecher', NULL, NULL, NULL, 23222), -- 61982
(61968, 'deDE', 'Shado-Pan-Bogenschütze', 'Shado-Pan-Bogenschützin', NULL, NULL, 23222), -- 61968
(62143, 'deDE', 'Schreckensspinnerjunges', NULL, NULL, NULL, 23222), -- 62143
(62000, 'deDE', 'Schreckensspinner', NULL, NULL, NULL, 23222), -- 62000
(61981, 'deDE', 'Schreckensspinnerhüter', NULL, NULL, NULL, 23222), -- 61981
(68559, 'deDE', 'No-No', NULL, NULL, 'wildpet', 23222), -- 68559
(58731, 'deDE', 'Ehrgeiziger Schüler', NULL, NULL, NULL, 23222), -- 58731
(58449, 'deDE', 'Wolliges Yak', NULL, NULL, NULL, 23222), -- 58449
(64274, 'deDE', 'Kriegswagen der Krik''thik', NULL, NULL, NULL, 23222), -- 64274
(64646, 'deDE', 'Shado-Pan-Waldläufer', 'Shado-Pan-Waldläuferin', NULL, NULL, 23222), -- 64646
(64524, 'deDE', 'Ölfass', NULL, NULL, NULL, 23222), -- 64524
(65336, 'deDE', 'Kriegsschlange', NULL, NULL, 'vehichlecursor', 23222), -- 65336
(63972, 'deDE', 'Schwarmling der Krik''thik', NULL, NULL, NULL, 23222), -- 63972
(65335, 'deDE', 'Kriegsschlange', NULL, NULL, NULL, 23222), -- 65335
(64687, 'deDE', 'Windhieb', NULL, NULL, NULL, 23222), -- 64687
(64692, 'deDE', 'Ätzendes Gas', NULL, NULL, NULL, 23222), -- 64692
(64336, 'deDE', 'Turm der Untergehenden Sonne', NULL, NULL, 'vehichlecursor', 23222), -- 64336
(63976, 'deDE', 'Nadler der Krik''thik', NULL, NULL, NULL, 23222), -- 63976
(63652, 'deDE', 'Gefangener des Goldenen Lotus', NULL, NULL, NULL, 23222), -- 63652
(58930, 'deDE', 'Shado-Pan-Späher', 'Shado-Pan-Späherin', NULL, 'speak', 23222), -- 58930
(58928, 'deDE', 'Antiquator der Shao-Tien', NULL, NULL, NULL, 23222), -- 58928
(66391, 'deDE', 'Krik''thik Scentlayer Stalker', NULL, NULL, NULL, 23222), -- 66391
(63973, 'deDE', 'Schwärmer der Krik''thik', NULL, NULL, NULL, 23222), -- 63973
(59053, 'deDE', 'Faust der Shao-Tien', NULL, NULL, NULL, 23222), -- 59053
(60932, 'deDE', 'Aschezahnhyäne', NULL, NULL, NULL, 23222), -- 60932
(66368, 'deDE', 'Aasgeier', NULL, NULL, NULL, 23222), -- 66368
(58927, 'deDE', 'Faust der Shao-Tien', NULL, NULL, NULL, 23222), -- 58927
(63605, 'deDE', 'Steingebundener Wächter', NULL, NULL, NULL, 23222), -- 63605
(60294, 'deDE', 'Gasaran', NULL, NULL, NULL, 23222), -- 60294
(66881, 'deDE', 'Talschreiter', NULL, NULL, NULL, 23222), -- 66881
(65374, 'deDE', 'Lightning Breath', NULL, NULL, NULL, 23222), -- 65374
(64702, 'deDE', 'Loh-Ki', NULL, 'Der Sturmbeobachter', 'speak', 23222), -- 64702
(63654, 'deDE', 'Gefangener des Goldenen Lotus', NULL, NULL, NULL, 23222), -- 63654
(65795, 'deDE', 'Wachposten des Goldenen Lotus', NULL, NULL, NULL, 23222), -- 65795
(65796, 'deDE', 'Wachposten des Goldenen Lotus', NULL, NULL, NULL, 23222), -- 65796
(58693, 'deDE', 'Arbeiter von Nebelhauch', NULL, NULL, NULL, 23222), -- 58693
(63935, 'deDE', 'Dorfbewohnerin von Nebelhauch', NULL, NULL, NULL, 23222), -- 63935
(65789, 'deDE', 'Corpse Pile Bunny', NULL, NULL, NULL, 23222), -- 65789
(63934, 'deDE', 'Dorfbewohner von Nebelhauch', NULL, NULL, NULL, 23222), -- 63934
(58460, 'deDE', 'Geistfetzer der Shao-Tien', NULL, NULL, NULL, 23222), -- 58460
(58767, 'deDE', 'Pomfrucht', NULL, NULL, 'Pickup', 23222), -- 58767
(58457, 'deDE', 'Seidenfederfalke', NULL, NULL, NULL, 23222), -- 58457
(63044, 'deDE', 'Glitschzunge der Bleichklingen', NULL, NULL, NULL, 23222), -- 63044
(63081, 'deDE', 'Rückeroberer der Shao-Tien', NULL, NULL, NULL, 23222), -- 63081
(63082, 'deDE', 'Fleischfresser der Bleichklingen', NULL, NULL, NULL, 23222), -- 63082
(63065, 'deDE', 'Geistbeuger der Shao-Tien', NULL, NULL, NULL, 23222), -- 63065
(59378, 'deDE', 'Fleischfresser der Bleichklingen', NULL, NULL, NULL, 23222), -- 59378
(66860, 'deDE', 'Krieger des Goldenen Lotus', 'Kriegerin des Goldenen Lotus', NULL, NULL, 23222), -- 66860
(58412, 'deDE', 'Marodeur der Shao-Tien', NULL, NULL, NULL, 23222), -- 58412
(59158, 'deDE', 'Bezwungene Schlange', NULL, NULL, NULL, 23222), -- 59158
(59156, 'deDE', 'Mogueffigie', NULL, NULL, NULL, 23222), -- 59156
(71544, 'deDE', 'Sha-Blitz', NULL, NULL, NULL, 23222), -- 71544
(71548, 'deDE', 'Wirbel', NULL, NULL, NULL, 23222), -- 71548
(73226, 'deDE', 'Niederer Sha-Tropfen', NULL, NULL, NULL, 23222), -- 73226
(73283, 'deDE', 'Niederer verseuchter Tropfen', NULL, NULL, NULL, 23222), -- 73283
(71543, 'deDE', 'Immerseus', NULL, 'Tränen des Tals', NULL, 23222), -- 71543
(73488, 'deDE', 'Invisible Stalker', NULL, NULL, NULL, 23222), -- 73488
(73222, 'deDE', 'Ooze Controller (Stalker)', NULL, NULL, NULL, 23222), -- 73222
(71550, 'deDE', 'Strudelziel', NULL, NULL, NULL, 23222), -- 71550
(73191, 'deDE', 'Aquatischer Verteidiger', NULL, NULL, NULL, 23222), -- 73191
(73260, 'deDE', 'Niederer verseuchter Tropfen', NULL, NULL, NULL, 23222), -- 73260
(73197, 'deDE', 'Niederer Sha-Tropfen', NULL, NULL, NULL, 23222), -- 73197
(73342, 'deDE', 'Gefallener Beckenhüter', NULL, NULL, NULL, 23222), -- 73342
(73192, 'deDE', 'Invisible Stalker', NULL, NULL, NULL, 23222), -- 73192
(73349, 'deDE', 'Gequälter Initiand', NULL, NULL, NULL, 23222), -- 73349
(70483, 'deDE', 'Invisible Stalker', NULL, NULL, NULL, 23222), -- 70483
(73330, 'deDE', 'Lehrensucher Cho', NULL, NULL, NULL, 23222), -- 73330
(65138, 'deDE', 'Wütender Verteidiger des Goldenen Lotus', 'Wütende Verteidigerin des Goldenen Lotus', NULL, NULL, 23222), -- 65138
(61679, 'deDE', 'Armbrust', NULL, NULL, NULL, 23222), -- 61679
(61451, 'deDE', 'Belebte Axt', NULL, NULL, NULL, 23222), -- 61451
(61239, 'deDE', 'Orakel der Glintrok', NULL, NULL, NULL, 23222), -- 61239
(61433, 'deDE', 'Belebter Stab', NULL, NULL, NULL, 23222), -- 61433
(61340, 'deDE', 'Verhexer der Glintrok', NULL, NULL, NULL, 23222), -- 61340
(61337, 'deDE', 'Eisenhaut der Glintrok', NULL, NULL, NULL, 23222), -- 61337
(61339, 'deDE', 'Orakel der Glintrok', NULL, NULL, NULL, 23222), -- 61339
(61338, 'deDE', 'Schleicher der Glintrok', NULL, NULL, NULL, 23222), -- 61338
(63739, 'deDE', 'Qilenstatue', NULL, NULL, NULL, 23222), -- 63739
(61216, 'deDE', 'Verhexer der Glintrok', NULL, NULL, NULL, 23222), -- 61216
(61242, 'deDE', 'Eisenhaut der Glintrok', NULL, NULL, NULL, 23222), -- 61242
(61240, 'deDE', 'Schleicher der Glintrok', NULL, NULL, NULL, 23222), -- 61240
(61399, 'deDE', 'Späher der Glintrok', NULL, NULL, NULL, 23222), -- 61399
(64250, 'deDE', 'Leuchtfeuer', NULL, NULL, NULL, 23222), -- 64250
(64243, 'deDE', 'Späher der Glintrok', NULL, NULL, NULL, 23222), -- 64243
(61626, 'deDE', 'Wirbelnder Derwisch', NULL, NULL, NULL, 23222), -- 61626
(65402, 'deDE', 'Gurthanschnellklinge', NULL, NULL, NULL, 23222), -- 65402
(61243, 'deDE', 'Gekkan', NULL, NULL, NULL, 23222), -- 61243
(61550, 'deDE', 'Harthakadept', NULL, NULL, NULL, 23222), -- 61550
(61389, 'deDE', 'Kargeshhochwache', NULL, NULL, NULL, 23222), -- 61389
(61392, 'deDE', 'Harthakflammensucher', NULL, NULL, NULL, 23222), -- 61392
(61449, 'deDE', 'Harthakadept', NULL, NULL, NULL, 23222), -- 61449
(61247, 'deDE', 'Grünschnabel der Glintrok', NULL, NULL, NULL, 23222), -- 61247
(61478, 'deDE', 'Schiedsrichter', NULL, NULL, NULL, 23222), -- 61478
(67231, 'deDE', 'Die Prüfung des Königs', NULL, NULL, NULL, 23222), -- 67231
(61450, 'deDE', 'Kargeshgrunzer', NULL, NULL, NULL, 23222), -- 61450
(61447, 'deDE', 'Gurthanstreiter', NULL, NULL, NULL, 23222), -- 61447
(61455, 'deDE', 'Gurthanqilen', NULL, NULL, NULL, 23222), -- 61455
(61549, 'deDE', 'Gurthanstreiter', NULL, NULL, NULL, 23222), -- 61549
(61444, 'deDE', 'Ming der Verschlagene', NULL, NULL, NULL, 23222), -- 61444
(64547, 'deDE', 'Gurthanstreiter', NULL, NULL, NULL, 23222), -- 64547
(61551, 'deDE', 'Kargeshgrunzer', NULL, NULL, NULL, 23222), -- 61551
(64548, 'deDE', 'Kargeshgrunzer', NULL, NULL, NULL, 23222), -- 64548
(61884, 'deDE', 'Xin der Waffenmeister', NULL, 'König der Klans', NULL, 23222), -- 61884
(61453, 'deDE', 'Mu''Shiba', NULL, NULL, NULL, 23222), -- 61453
(61445, 'deDE', 'Haiyan der Unaufhaltsame', NULL, NULL, NULL, 23222), -- 61445
(61442, 'deDE', 'Kuai der Grobian', NULL, NULL, NULL, 23222), -- 61442
(61432, 'deDE', 'Plünderer der Glintrok', NULL, NULL, NULL, 23222), -- 61432
(61431, 'deDE', 'Späher der Glintrok', NULL, NULL, NULL, 23222), -- 61431
(61415, 'deDE', 'Riesige Höhlenfledermaus', NULL, NULL, NULL, 23222), -- 61415
(61947, 'deDE', 'Kargeshrippenbrecher', NULL, NULL, NULL, 23222), -- 61947
(61946, 'deDE', 'Harthaksturmrufer', NULL, NULL, NULL, 23222), -- 61946
(64432, 'deDE', 'Sinan die Träumerin', NULL, 'Verwalterin', NULL, 23222), -- 64432
(61945, 'deDE', 'Gurthaneisenschlund', NULL, NULL, NULL, 23222), -- 61945
(64922, 'deDE', 'Brann Bronzebart', NULL, 'Archäologielehrer', NULL, 23222), -- 64922
(63984, 'deDE', 'Meister Liu', NULL, NULL, NULL, 23222), -- 63984
(63983, 'deDE', 'Fräulein Thai', NULL, NULL, NULL, 23222), -- 63983
(11704, 'deDE', 'Kriss Goldenlicht', NULL, NULL, NULL, 23222), -- 11704
(80633, 'deDE', 'Lehrensucher Han', NULL, 'Geschichtenerzähler des Schlachtzugsbrowsers', NULL, 23222), -- 80633
(70976, 'deDE', 'Lehrensucher Chos Traumgebräu', NULL, NULL, 'interact', 23222), -- 70976
(64605, 'deDE', 'Tan Shin Tiao', NULL, 'Rüstmeisterin der Lehrensucher', NULL, 23222), -- 64605
(78777, 'deDE', 'Lehrensucher Shin', NULL, 'Heroischer Szenarioerzähler', NULL, 23222), -- 78777
(78709, 'deDE', 'Lehrensucher Fu', NULL, 'Szenarioerzähler', NULL, 23222), -- 78709
(64691, 'deDE', 'Lehrensucherin Huynh', NULL, 'Inschriftenkundelehrerin', NULL, 23222), -- 64691
(70171, 'deDE', 'Elloric', NULL, NULL, NULL, 23222), -- 70171
(72283, 'deDE', 'Schreiber Wie Wu', NULL, NULL, NULL, 23222), -- 72283
(73691, 'deDE', 'Chromie', NULL, 'Die Zeitwanderer', NULL, 23222), -- 73691
(66972, 'deDE', 'Smaragdgrüner Pandarenphönix', NULL, NULL, NULL, 23222), -- 66972
(66970, 'deDE', 'Violetter Pandarenphönix', NULL, NULL, NULL, 23222), -- 66970
(66973, 'deDE', 'Kai Federfall', NULL, 'Händlerin für Phönixeier', NULL, 23222), -- 66973
(66971, 'deDE', 'Aschegrauer Pandarenphönix', NULL, NULL, NULL, 23222), -- 66971
(65851, 'deDE', 'Purpurroter Pandarenphönix', NULL, NULL, NULL, 23222), -- 65851
(50951, 'deDE', 'Carapin', NULL, NULL, NULL, 23222), -- 50951
(36911, 'deDE', 'Pandarenmönch', NULL, NULL, NULL, 23222), -- 36911
(67013, 'deDE', 'Kassierer Meelad', NULL, 'Leerenlager', 'voidstorage', 23222), -- 67013
(67014, 'deDE', 'Warpweber Shafiee', NULL, 'Transmogrifizierer', 'transmogrify', 23222), -- 67014
(68981, 'deDE', 'Leerenbinder Shadzor', NULL, 'Gegenstandsaufwertung', 'reforge', 23222), -- 68981
(64043, 'deDE', 'Onkel Gus', NULL, NULL, NULL, 23222), -- 64043
(64040, 'deDE', 'Tina Wang', NULL, 'Bardame', NULL, 23222), -- 64040
(64585, 'deDE', 'Garnwickler Uko', NULL, 'Angelbedarf', NULL, 23222), -- 64585
(64041, 'deDE', 'Mifan', NULL, 'Speis & Trank', NULL, 23222), -- 64041
(64925, 'deDE', 'Tivilix Knallwandler', NULL, 'Explosive Expeditionen', NULL, 23222), -- 64925
(64924, 'deDE', 'Guyo Kristallrad', NULL, 'Explosive Expeditionen', NULL, 23222), -- 64924
(67130, 'deDE', 'X.E.T.R.A.', NULL, 'Mechanischer Auktionator', NULL, 23222), -- 67130
(64114, 'deDE', 'Joan Tremblay', NULL, NULL, NULL, 23222), -- 64114
(73422, 'deDE', 'Fräulein Jadepfote', NULL, 'Juwelierskunstbedarf', NULL, 23222), -- 73422
(64059, 'deDE', 'Razzie Kohlschlüssel', NULL, 'Bergbaubedarf', NULL, 23222), -- 64059
(63966, 'deDE', 'Kassiererin Shifen', NULL, 'Bankierin', NULL, 23222), -- 63966
(64060, 'deDE', 'Tixit Leuchtlunte', NULL, 'Ingenieursbedarf', NULL, 23222), -- 64060
(64056, 'deDE', 'Hafuna Landläufer', NULL, 'Kräuterkundebedarf', NULL, 23222), -- 64056
(64049, 'deDE', 'Victor Pearce', NULL, 'Alchemiebedarf', NULL, 23222), -- 64049
(64050, 'deDE', 'Derenda Engebein', NULL, 'Verzauberkunstbedarf', NULL, 23222), -- 64050
(64053, 'deDE', 'Fang Weißrolle', NULL, 'Inschriftenkundebedarf', NULL, 23222), -- 64053
(65266, 'deDE', 'David Harrington', NULL, NULL, 'directions', 23222), -- 65266
(64058, 'deDE', 'Jorunga Steinhuf', NULL, 'Schmiedekunstbedarf', NULL, 23222), -- 64058
(64057, 'deDE', 'Moko Pulverlauf', NULL, 'Feuerwerk', NULL, 23222), -- 64057
(64054, 'deDE', 'Krogo Rußhaut', NULL, 'Lederverarbeitungs- und Kürschnereibedarf', NULL, 23222), -- 64054
(64048, 'deDE', 'Sheena Sonnenweber', NULL, 'Reagenzien', NULL, 23222), -- 64048
(63965, 'deDE', 'Kassiererin Goldgroschen', NULL, 'Bankierin', NULL, 23222), -- 63965
(63964, 'deDE', 'Kassierer Kan', NULL, 'Bankier', NULL, 23222), -- 63964
(65770, 'deDE', 'Stormcaller Bunny', NULL, NULL, NULL, 23222), -- 65770
(68984, 'deDE', 'Ruben Holen', NULL, 'Der Bücherwurm', NULL, 23222), -- 68984
(64051, 'deDE', 'Esha die Weberin', NULL, 'Schneiderbedarf', NULL, 23222), -- 64051
(64126, 'deDE', 'Stephen Wong', NULL, 'Kochbedarf', NULL, 23222), -- 64126
(64127, 'deDE', 'Frances Lin', NULL, NULL, 'directions', 23222), -- 64127
(64128, 'deDE', 'Edward Hanes', NULL, NULL, 'directions', 23222), -- 64128
(64588, 'deDE', 'Rüstmeisterin Relna', NULL, NULL, NULL, 23222), -- 64588
(73674, 'deDE', 'Blizrik Funkenklinge', NULL, 'Händler des Schlachtzugsbrowsers', NULL, 23222), -- 73674
(62996, 'deDE', 'Madame Vee Luo', NULL, 'Gastwirtin', NULL, 23222), -- 62996
(64067, 'deDE', 'Gerstenblüte', NULL, 'Kochbedarf', NULL, 23222), -- 64067
(64107, 'deDE', 'Sadi', NULL, NULL, 'directions', 23222), -- 64107
(64106, 'deDE', 'Erin', NULL, NULL, 'directions', 23222), -- 64106
(63753, 'deDE', 'Verwundeter Verteidiger', NULL, NULL, NULL, 23222), -- 63753
(63752, 'deDE', 'Verwundeter Verteidiger', NULL, NULL, NULL, 23222), -- 63752
(74010, 'deDE', 'Nadina Sternstein', NULL, 'Händlerin für Schlachtzüge', NULL, 23222), -- 74010
(64119, 'deDE', 'Dirki Tanboshi', NULL, NULL, 'directions', 23222), -- 64119
(74012, 'deDE', 'Ki''agnuu', NULL, 'Händler für heroische Schlachtzüge', NULL, 23222), -- 74012
(66998, 'deDE', 'Jinho der Windbrecher', NULL, NULL, NULL, 23222), -- 66998
(64118, 'deDE', 'Erni Tanboshi', NULL, NULL, 'directions', 23222), -- 64118
(65862, 'deDE', 'Ala''thinel', NULL, 'Erste Hilfe', NULL, 23222), -- 65862
(64062, 'deDE', 'Die sanftmütige Dari', NULL, 'Bedarf für Erste Hilfe', NULL, 23222), -- 64062
(65824, 'deDE', 'Ungetüm der Shao-Tien', NULL, NULL, NULL, 23222), -- 65824
(64121, 'deDE', 'Eli', NULL, NULL, NULL, 23222), -- 64121
(64065, 'deDE', 'Braumeister Roland', NULL, NULL, NULL, 23222), -- 64065
(74019, 'deDE', 'Tu''aho Wegschneider', NULL, 'Händler für mythische Schlachtzüge', NULL, 23222), -- 74019
(64066, 'deDE', 'Braumeisterin Vudia', NULL, 'Speis & Trank', NULL, 23222), -- 64066
(64047, 'deDE', 'Kurong Fasskopf', NULL, 'Schankkellner', NULL, 23222), -- 64047
(63008, 'deDE', 'Braumeisterin Skei', NULL, 'Gastwirtin', NULL, 23222), -- 63008
(64134, 'deDE', 'Jontan Dum''okk', NULL, NULL, NULL, 23222), -- 64134
(64122, 'deDE', 'Andrew Vestal', NULL, NULL, NULL, 23222), -- 64122
(67133, 'deDE', 'Andi', NULL, NULL, 'directions', 23222), -- 67133
(66731, 'deDE', 'Herr Knitter', NULL, NULL, 'directions', 23222), -- 66731
(67134, 'deDE', 'Kat', NULL, NULL, 'directions', 23222), -- 67134
(64131, 'deDE', 'Evangelia', NULL, NULL, 'directions', 23222), -- 64131
(64579, 'deDE', 'Jong Wik-Wung', NULL, NULL, NULL, 23222), -- 64579
(64578, 'deDE', 'Jong Jun-Keet', NULL, NULL, NULL, 23222), -- 64578
(64575, 'deDE', 'Chan Hoi-San', NULL, 'Flüchtling von Kun-Lai', NULL, 23222), -- 64575
(64561, 'deDE', 'Jong Ming-Yiu', NULL, 'Flüchtling von Kun-Lai', NULL, 23222), -- 64561
(64534, 'deDE', 'Bogenschützenmeister Ku', NULL, 'Die Shado-Pan', NULL, 23222), -- 64534
(64587, 'deDE', 'Friedensbewahrer der Horde', 'Friedensbewahrerin der Horde', NULL, NULL, 23222), -- 64587
(103679, 'deDE', 'Seelenbildnis', NULL, NULL, NULL, 23222), -- 103679
(64002, 'deDE', 'Sang-Bo', NULL, 'Die Shado-Pan', NULL, 23222), -- 64002
(64535, 'deDE', 'Wolkenschlange', NULL, NULL, NULL, 23222),
(64019, 'deDE', 'Braumeisterin Linshi', NULL, NULL, NULL, 23222), -- 64019
(63994, 'deDE', 'Herausforderer Wuli', NULL, 'Herausforderungsdungeons', NULL, 23222), -- 63994
(63996, 'deDE', 'Ältester Liao', NULL, 'Händler für Glücksbringer', NULL, 23222), -- 63996
(64976, 'deDE', 'Bärich', NULL, 'Stopfbaus Begleiter', NULL, 23222), -- 64976
(64972, 'deDE', 'Stopfbau', NULL, 'Kun-Lai-Flüchtling', NULL, 23222), -- 64972
(66685, 'deDE', 'Danky', NULL, 'Händler für Geister der Harmonie', NULL, 23222), -- 66685
(64533, 'deDE', 'Wei Seebrise', NULL, 'Orden der Wolkenschlange', NULL, 23222), -- 64533
(64531, 'deDE', 'Lena Steinfeg', NULL, 'Die Lehrensucher', NULL, 23222), -- 64531
(64007, 'deDE', 'Weng der Gnädige', NULL, 'Goldener Lotus', NULL, 23222), -- 64007
(61122, 'deDE', 'Tania Sommerbrise', NULL, 'Flugmeisterin', NULL, 23222), -- 61122
(59959, 'deDE', 'Mokimo der Starke', NULL, 'Verwalter', NULL, 23222), -- 59959
(64011, 'deDE', 'Landarbeiter Dooka', NULL, 'Die Ackerbauern', NULL, 23222), -- 64011
(63986, 'deDE', 'Fährtenleser Lang', NULL, 'Stallmeister', NULL, 23222), -- 63986
(63987, 'deDE', 'Panzi', NULL, 'Langs Begleiter', NULL, 23222), -- 63987
(64591, 'deDE', 'Seamus', NULL, 'Kampfhaustier', NULL, 23222), -- 64591
(64590, 'deDE', 'Magenta', NULL, 'Kampfhaustier', NULL, 23222), -- 64590
(64589, 'deDE', 'Tungsten', NULL, 'Kampfhaustier', NULL, 23222), -- 64589
(64582, 'deDE', 'San die Sanfte', NULL, 'Kampfhaustiertrainerin', NULL, 23222), -- 64582
(64010, 'deDE', 'Meisterangler Karu', NULL, 'Die Angler', NULL, 23222), -- 64010
(64001, 'deDE', 'Weise Lotusblüte', NULL, 'Rüstmeister der Himmlischen Erhabenen', NULL, 23222), -- 64001
(60167, 'deDE', 'Himmelstänzer Shun', NULL, 'Fluglehrer', NULL, 23222), -- 60167
(71483, 'deDE', 'Abrogar Staubhuf', NULL, NULL, NULL, 23222), -- 71483
(64566, 'deDE', 'Sonnenläufer Dezco', NULL, 'Häuptling der Morgenjäger', NULL, 23222), -- 64566
(58245, 'deDE', 'Nala', NULL, NULL, NULL, 23222), -- 58245
(64584, 'deDE', 'Kriegerheld der Morgenjäger', 'Kriegerheldin der Morgenjäger', NULL, NULL, 23222), -- 64584
(65207, 'deDE', 'Goldenes Zibetkätzchen', NULL, NULL, NULL, 23222), -- 65207
(66861, 'deDE', 'Krieger des Goldenen Lotus', 'Kriegerin des Goldenen Lotus', NULL, NULL, 23222), -- 66861
(63076, 'deDE', 'Banner Fire Bunny', NULL, NULL, NULL, 23222), -- 63076
(58411, 'deDE', 'Handzahmes Stachelschwein', NULL, NULL, NULL, 23222), -- 58411
(58671, 'deDE', 'Uralter Mogugeist', NULL, NULL, NULL, 23222), -- 58671
(58669, 'deDE', 'Echo eines Pandarenmönches', NULL, NULL, NULL, 23222), -- 58669
(73818, 'deDE', 'Steinschneider mit Trilliumblatt', NULL, NULL, NULL, 23222), -- 73818
(61650, 'deDE', 'Keech der Große', NULL, 'Seltene Antiquitäten', NULL, 23222), -- 61650
(66386, 'deDE', 'Mishi', NULL, 'Lehrensucher Chos Begleiter', NULL, 23222), -- 66386
(67829, 'deDE', 'Augenstrahlziel', NULL, NULL, NULL, 23222), -- 67829
(67851, 'deDE', 'Augenstrahlziel', NULL, NULL, NULL, 23222), -- 67851
(63847, 'deDE', 'Tanzender Wasserläufer', NULL, NULL, 'wildpetcapturable', 23222), -- 63847
(67679, 'deDE', 'Immerschreiter', NULL, NULL, NULL, 23222), -- 67679
(100324, 'deDE', 'Hati', NULL, NULL, NULL, 23222), -- 100324
(63838, 'deDE', 'Goldfalter', NULL, NULL, 'wildpetcapturable', 23222), -- 63838
(100409, 'deDE', 'Dämmerheuler', NULL, NULL, NULL, 23222), -- 100409
(35395, 'deDE', 'Deviatjungtier', NULL, NULL, NULL, 23222), -- 35395
(3504, 'deDE', 'Gil', NULL, NULL, NULL, 23222), -- 3504
(2331, 'deDE', 'Paige Chaddis', NULL, NULL, NULL, 23222), -- 2331
(2330, 'deDE', 'Karlee Chaddis', NULL, NULL, NULL, 23222), -- 2330
(29452, 'deDE', 'Pesthund der Vargul', NULL, NULL, NULL, 23222), -- 29452
(1478, 'deDE', 'Aedis Brom', NULL, NULL, NULL, 23222), -- 1478
(1477, 'deDE', 'Christoph Faral', NULL, NULL, NULL, 23222), -- 1477
(7382, 'deDE', 'Orangefarbene Tigerkatze', NULL, NULL, NULL, 23222), -- 7382
(30730, 'deDE', 'Stanly McCormick', NULL, 'Inschriftenkundebedarf', NULL, 23222), -- 30730
(106152, 'deDE', 'Urhornkalb', NULL, NULL, NULL, 23222), -- 106152
(64160, 'deDE', 'Frostblume', NULL, NULL, 'directions', 23222), -- 64160
(64164, 'deDE', 'Ying Donnerspeer', NULL, NULL, NULL, 23222), -- 64164
(64094, 'deDE', 'Gerber Pang', NULL, 'Lederverarbeitungs- und Kürschnereibedarf', NULL, 23222), -- 64094
(64079, 'deDE', 'Yanyra Mondbalg', NULL, 'Reagenzien', NULL, 23222), -- 64079
(65579, 'deDE', 'Eric Thibeau', NULL, NULL, NULL, 23222), -- 65579
(64161, 'deDE', 'Jonathan Le Karf', NULL, NULL, NULL, 23222), -- 64161
(64124, 'deDE', 'Kata Arina', NULL, NULL, 'directions', 23222), -- 64124
(64125, 'deDE', 'Zooey', NULL, NULL, NULL, 23222), -- 64125
(65585, 'deDE', 'Flame Bunny', NULL, NULL, NULL, 23222), -- 65585
(64097, 'deDE', 'Bero', NULL, 'Juwelierskunstbedarf', NULL, 23222), -- 64097
(64090, 'deDE', 'Missy Pickenkorb', NULL, 'Bergbaubedarf', NULL, 23222), -- 64090
(64085, 'deDE', 'Cullen Hammerstirn', NULL, 'Schmiedekunstbedarf', NULL, 23222), -- 64085
(68982, 'deDE', 'Leerenbinder Lunshur', NULL, 'Gegenstandsaufwertung', 'reforge', 23222), -- 68982
(64573, 'deDE', 'Warpweber Ramahesh', NULL, 'Transmogrifizierer', 'transmogrify', 23222), -- 64573
(64083, 'deDE', 'Apotheker Grünmoos', NULL, 'Alchemiebedarf', NULL, 23222), -- 64083
(64081, 'deDE', 'Veronica Faraday', NULL, 'Inschriftenkundebedarf', NULL, 23222), -- 64081
(64080, 'deDE', 'Clara Henry', NULL, 'Verzauberkunstbedarf', NULL, 23222), -- 64080
(64052, 'deDE', 'Raishen die Nadel', NULL, 'Schneiderbedarf', NULL, 23222), -- 64052
(64574, 'deDE', 'Kassierer Edouin', NULL, 'Leerenlager', 'voidstorage', 23222), -- 64574
(64153, 'deDE', 'Cammi die Münzenzählerin', NULL, NULL, NULL, 23222), -- 64153
(64092, 'deDE', 'Murphy Wildmoor', NULL, 'Ingenieursbedarf', NULL, 23222), -- 64092
(63969, 'deDE', 'Kassierer Zischdübel', NULL, 'Bankier', NULL, 23222), -- 63969
(65599, 'deDE', 'W.A.L.L.Y.', NULL, 'Mechanischer Auktionator', NULL, 23222), -- 65599
(64100, 'deDE', 'Meng Chi die Faust', NULL, 'Gebräue', NULL, 23222), -- 64100
(65576, 'deDE', 'Philip Luke', NULL, NULL, NULL, 23222), -- 65576
(65596, 'deDE', 'Blecheimerchen', NULL, NULL, NULL, 23222), -- 65596
(65583, 'deDE', 'Shock Bunny', NULL, NULL, NULL, 23222), -- 65583
(63970, 'deDE', 'Kassiererin Melka', NULL, 'Bankierin', NULL, 23222), -- 63970
(63968, 'deDE', 'Kassiererin Xifa', NULL, 'Bankierin', NULL, 23222), -- 63968
(64024, 'deDE', 'Kassierer Silberpfote', NULL, 'Bankier', NULL, 23222), -- 64024
(63967, 'deDE', 'Kassiererin Shan', NULL, 'Bankierin', NULL, 23222), -- 63967
(64023, 'deDE', 'Kassiererin Pieta', NULL, 'Bankierin', NULL, 23222), -- 64023
(63971, 'deDE', 'Kassierer Jiaku', NULL, 'Bankier', NULL, 23222), -- 63971
(64147, 'deDE', 'Duinn Stahlbräu', NULL, NULL, 'directions', 23222), -- 64147
(64077, 'deDE', 'Kergan Flinkbart', NULL, 'Handwerkswaren', NULL, 23222), -- 64077
(65577, 'deDE', 'Collin Gilbert', NULL, NULL, NULL, 23222), -- 65577
(64145, 'deDE', 'Kavanna Nimmergut', NULL, NULL, 'directions', 23222), -- 64145
(64144, 'deDE', 'Braumeister Tsu', NULL, NULL, 'directions', 23222), -- 64144
(64078, 'deDE', 'Sarya Teeblume', NULL, 'Bardame', NULL, 23222), -- 64078
(64481, 'deDE', 'Priester Weißbraue', NULL, NULL, NULL, 23222), -- 64481
(65578, 'deDE', 'Winnie Morrison', NULL, NULL, NULL, 23222), -- 65578
(63013, 'deDE', 'Dan Ischerkoch', NULL, 'Speis & Trank', NULL, 23222), -- 63013
(64096, 'deDE', 'Serenka', NULL, 'Bedarf für Erste Hilfe', NULL, 23222), -- 64096
(64482, 'deDE', 'Heilerin Nan', NULL, 'Erste Hilfe', NULL, 23222), -- 64482
(64146, 'deDE', 'Collin Nimmergut', NULL, 'Exotische Gebräue', NULL, 23222), -- 64146
(64076, 'deDE', 'Bonni Chang', NULL, 'Gemischtwaren', NULL, 23222), -- 64076
(64149, 'deDE', 'Matrone Vi Vinh', NULL, 'Gastwirtin', NULL, 23222), -- 64149
(64157, 'deDE', 'Vamuu', NULL, NULL, NULL, 23222), -- 64157
(65574, 'deDE', 'Nikos Rhodos', NULL, NULL, NULL, 23222), -- 65574
(64154, 'deDE', 'Historikerin Jenji', NULL, NULL, NULL, 23222), -- 64154
(64172, 'deDE', 'Berater Kosa', NULL, NULL, NULL, 23222), -- 64172
(74027, 'deDE', 'Lorry Warmherz', NULL, 'Händlerin für mythische Schlachtzüge', NULL, 23222), -- 74027
(64138, 'deDE', 'Donnelly Feuerfass', NULL, NULL, 'directions', 23222), -- 64138
(74021, 'deDE', 'Clarice Chapmann', NULL, 'Händlerin für heroische Schlachtzüge', NULL, 23222), -- 74021
(64150, 'deDE', 'Meister Zhang', NULL, NULL, NULL, 23222), -- 64150
(64101, 'deDE', 'Taijing der Zyklon', NULL, 'Gebräue', NULL, 23222), -- 64101
(74022, 'deDE', 'Thelett Schieferherz', NULL, 'Händler für Schlachtzüge', NULL, 23222), -- 74022
(64483, 'deDE', 'Meglette', NULL, NULL, 'directions', 23222), -- 64483
(64169, 'deDE', 'Ranna', NULL, NULL, 'directions', 23222), -- 64169
(64168, 'deDE', 'Vienh Sturmtrunk', NULL, NULL, 'directions', 23222), -- 64168
(64167, 'deDE', 'Shing Blitzpfote', NULL, NULL, 'directions', 23222), -- 64167
(64166, 'deDE', 'Yuma Eisenkessel', NULL, NULL, 'directions', 23222), -- 64166
(64113, 'deDE', 'David Guerrero', NULL, NULL, NULL, 23222), -- 64113
(64112, 'deDE', 'Keilan Herdlied', NULL, NULL, NULL, 23222), -- 64112
(64104, 'deDE', 'Wasserformer Sharu', NULL, NULL, NULL, 23222), -- 64104
(64084, 'deDE', 'Jojo', NULL, 'Kochbedarf', NULL, 23222), -- 64084
(64082, 'deDE', 'Tommy Bastelspaten', NULL, 'Kräuterkundebedarf', NULL, 23222), -- 64082
(64075, 'deDE', 'Jai Maguri', NULL, NULL, NULL, 23222), -- 64075
(64073, 'deDE', 'Andrea Toyas', NULL, NULL, NULL, 23222), -- 64073
(64477, 'deDE', 'Flusshüter Tuushuu', NULL, NULL, NULL, 23222), -- 64477
(64476, 'deDE', 'Teichwächter Gui', NULL, NULL, NULL, 23222), -- 64476
(64129, 'deDE', 'Wilhem Ken', NULL, 'Brothändler', NULL, 23222), -- 64129
(64130, 'deDE', 'Michael Bedernik', NULL, 'Fleischwaren', NULL, 23222), -- 64130
(64352, 'deDE', 'Rapanaschnecke', NULL, NULL, 'wildpetcapturable', 23222), -- 64352
(115785, 'deDE', 'Düsterschnabeljunges', NULL, NULL, NULL, 23222), -- 115785
(64042, 'deDE', 'Hara Bierbäuchlein', NULL, 'Bardame', NULL, 23222), -- 64042
(64074, 'deDE', 'Tina Nguyen', NULL, NULL, NULL, 23222), -- 64074
(64111, 'deDE', 'Die graziöse Jessi', NULL, NULL, NULL, 23222), -- 64111
(65737, 'deDE', 'Wagen', NULL, NULL, NULL, 23222), -- 65737
(64136, 'deDE', 'Raoshan die Adlerkralle', NULL, NULL, 'directions', 23222), -- 64136
(510, 'deDE', 'Wasserelementar', NULL, NULL, NULL, 23222), -- 510
(63842, 'deDE', 'Goldenes Zibetkätzchen', NULL, NULL, 'wildpetcapturable', 23222), -- 63842
(61088, 'deDE', 'Immerschreiter', NULL, NULL, 'wildpetcapturable', 23222), -- 61088
(65736, 'deDE', 'Rückkehr zur Terrasse', NULL, NULL, 'questinteract', 23222), -- 65736
(60957, 'deDE', 'Minion of Fear Controller', NULL, NULL, NULL, 23222), -- 60957
(60999, 'deDE', 'Sha der Angst', NULL, NULL, NULL, 23222), -- 60999
(63025, 'deDE', 'Tsulong', NULL, NULL, NULL, 23222), -- 63025
(60585, 'deDE', 'Ältester Regail', NULL, NULL, NULL, 23222), -- 60585
(60586, 'deDE', 'Ältester Asani', NULL, NULL, NULL, 23222), -- 60586
(66100, 'deDE', 'Erscheinung des Schreckens', NULL, NULL, NULL, 23222), -- 66100
(60583, 'deDE', 'Beschützer Kaolan', NULL, NULL, NULL, 23222), -- 60583
(64368, 'deDE', 'Erscheinung der Angst', NULL, NULL, NULL, 23222), -- 64368
(64846, 'deDE', 'Teichwärter Ashani', NULL, NULL, NULL, 23222), -- 64846
(36737, 'deDE', 'Invisible Stalker', NULL, NULL, NULL, 23222); -- 36737


DELETE FROM `page_text_locale` WHERE (`ID`=4522 /*4522*/ AND `locale`='deDE') OR  (`ID`=44521 /*4521*/ AND `locale`='deDE');
INSERT INTO `page_text_locale` (`ID`, `locale`, `Text`, `VerifiedBuild`) VALUES
(4522, 'deDE', 'Unter der Herrschaft der Mogu durften die Dienerrassen keine Waffen tragen, also veranlasste Kang, dass die Pandaren selbst zu Waffen werden würden. Die Pandaren-Mönche begannen mit dem Training der Kampfkünste und Kang wurde als die Faust der Morgendämmerung bekannt.$b$bEs ist nicht überliefert, ob sich Kang und sein Sohn jemals wiedersahen, doch die Liebe dieses Vaters entzündete die Rebellion, die Pandaria für immer verändern sollte.', 23222), -- 4522
(4521, 'deDE', 'Selbst in den Augen der Mogu war Kaiser Lao-Fe ein bestialisches Monster. Seine bevorzugte Strafe für Pandarensklaven war es, ihnen ihre Familien zu entreißen. Sklaven, die ihm missfielen, nahm er ihre Kinder und schickte sie zum Schlangenrücken, um dort unter großem Leid als Futter für die Mantis zu enden.$b$bDieses Schicksal ereilte einen jungen Pandaren-Mönch namens Kang. Der Verlust seines Kindes raubte Kang beinahe den Verstand und fortan kleidete er sich ganz in schwarz. In einem Moment der Klarheit sah er die Moguherrscher, wie sie wirklich waren: schwach. Sie verfügten über dunkle Magie und schreckliche Waffen, aber ihr Reich hing vollständig von der Sklavenarbeit ab.', 23222); -- 4521


INSERT IGNORE INTO `page_text` (`ID`, `Text`, `NextPageID`, `PlayerConditionID`, `Flags`, `VerifiedBuild`) VALUES
(4522, 'Unter der Herrschaft der Mogu durften die Dienerrassen keine Waffen tragen, also veranlasste Kang, dass die Pandaren selbst zu Waffen werden würden. Die Pandaren-Mönche begannen mit dem Training der Kampfkünste und Kang wurde als die Faust der Morgendämmerung bekannt.$b$bEs ist nicht überliefert, ob sich Kang und sein Sohn jemals wiedersahen, doch die Liebe dieses Vaters entzündete die Rebellion, die Pandaria für immer verändern sollte.', 0, 0, 0, 23222), -- 4522
(4521, 'Selbst in den Augen der Mogu war Kaiser Lao-Fe ein bestialisches Monster. Seine bevorzugte Strafe für Pandarensklaven war es, ihnen ihre Familien zu entreißen. Sklaven, die ihm missfielen, nahm er ihre Kinder und schickte sie zum Schlangenrücken, um dort unter großem Leid als Futter für die Mantis zu enden.$b$bDieses Schicksal ereilte einen jungen Pandaren-Mönch namens Kang. Der Verlust seines Kindes raubte Kang beinahe den Verstand und fortan kleidete er sich ganz in schwarz. In einem Moment der Klarheit sah er die Moguherrscher, wie sie wirklich waren: schwach. Sie verfügten über dunkle Magie und schreckliche Waffen, aber ihr Reich hing vollständig von der Sklavenarbeit ab.', 4522, 0, 0, 23222); -- 4521


INSERT IGNORE INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `OptionBroadcastTextID`, `action_poi_id`, `box_coded`, `box_money`, `box_text`, `BoxBroadcastTextID`) VALUES
(14575, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(14680, 2, 0, 'Was kann ich hier in der Stadt finden?', 0, 0, 0, 0, '', 0),
(14680, 0, 5, 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', 0, 0, 0, 0, '', 0),
(10183, 0, 1, 'Ich sehe mich nur mal um.', 0, 0, 0, 0, '', 0),
(15152, 11, 0, 'Andere', 0, 0, 0, 0, '', 0),
(15152, 10, 0, 'Verkäufer', 0, 0, 0, 0, '', 0),
(15152, 9, 0, 'Stallmeister', 0, 0, 0, 0, '', 0),
(15152, 8, 0, 'Transmogrifikation und Leerenlager', 0, 0, 0, 0, '', 0),
(15152, 7, 0, 'Rüstmeister', 0, 0, 0, 0, '', 0),
(15152, 6, 0, 'Berufsausbilder', 0, 0, 0, 0, '', 0),
(15152, 5, 0, 'Kampfhaustiertrainer', 0, 0, 0, 0, '', 0),
(15152, 4, 0, 'Gasthaus', 0, 0, 0, 0, '', 0),
(15152, 3, 0, 'Fluglehrer', 0, 0, 0, 0, '', 0),
(15152, 2, 0, 'Flugmeister', 0, 0, 0, 0, '', 0),
(15152, 1, 0, 'Bank', 0, 0, 0, 0, '', 0),
(14558, 11, 0, 'Andere', 0, 0, 0, 0, '', 0),
(14558, 10, 0, 'Verkäufer', 0, 0, 0, 0, '', 0),
(14558, 9, 0, 'Stallmeister', 0, 0, 0, 0, '', 0),
(14558, 7, 0, 'Rüstmeister', 0, 0, 0, 0, '', 0),
(14558, 6, 0, 'Berufsausbilder', 0, 0, 0, 0, '', 0),
(14558, 5, 0, 'Kampfhaustiertrainer', 0, 0, 0, 0, '', 0),
(14558, 4, 0, 'Gasthaus', 0, 0, 0, 0, '', 0),
(14558, 3, 0, 'Fluglehrer', 0, 0, 0, 0, '', 0),
(14558, 2, 0, 'Flugmeister', 0, 0, 0, 0, '', 0),
(14558, 1, 0, 'Bank', 0, 0, 0, 0, '', 0),
(14404, 0, 0, 'Könnt Ihr mir etwas vorspielen?', 0, 0, 0, 20000, 'Seid Ihr sicher, dass Ihr zahlen wollt?', 0),
(10362, 1, 1, 'Ich möchte ein wenig Eure Ware betrachten.', 0, 0, 0, 0, '', 0),
(10362, 0, 3, 'Bildet mich aus.', 0, 0, 0, 0, '', 0),
(15952, 0, 1, 'Untersucht seine Waren.', 0, 0, 0, 0, '', 0),
(14504, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0);


-- unknown broadcasttextid
INSERT IGNORE INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `comment`) VALUES
(45979, 0, 0, 'Ihr hört ein Knirschen im Aufzugschacht!', 41, 0, 100, 0, 0, 0, 0, 'General Purpose Bunny JMF to Player'),
(61243, 0, 0, 'Ich höre etwas...', 14, 0, 100, 0, 0, 32495, 0, 'Gekkan to Verhexer der Glintrok'),
(61243, 1, 0, 'Haltet sie auf!', 14, 0, 100, 0, 0, 32488, 0, 'Gekkan'),
(61243, 2, 0, 'Was für eine Verschwendung...', 14, 0, 100, 0, 0, 32494, 0, 'Gekkan to Player'),
(61399, 0, 0, 'Die Rufe des Spähers der Glintrok sind eine Warnung!', 41, 0, 100, 0, 0, 0, 0, 'Späher der Glintrok'),
(61442, 0, 0, 'Klan Gurthan wird unserem König und dem Rest von Euch machthungrigen Schwindlern zeigen, warum wir zu Recht an seiner Seite stehen!', 14, 0, 100, 15, 0, 27932, 0, 'Kuai der Grobian to Schiedsrichter'),
(61442, 1, 0, 'Es wird immer unser Vorrecht sein, das Schicksal unseres Volkes zu bestimmen!', 14, 0, 100, 16, 0, 27933, 0, 'Kuai der Grobian'),
(61442, 2, 0, 'Wer hat diese Eindringlinge in unsere Hallen gelassen? Nur Klan Harthak oder Klan Kargesh würden einen derartigen Verrat begehen!', 14, 0, 100, 25, 0, 27940, 0, 'Kuai der Grobian to Schiedsrichter'),
(61442, 3, 0, 'Guter Versuch, aber so einfach täuscht Ihr uns nicht. Eure Klans haben sich eindeutig verbündet. Für den Ruhm unseres Königs und unseres Reichs werden wir Euch alle bezwingen!', 14, 0, 100, 15, 0, 27941, 0, 'Kuai der Grobian to Schiedsrichter'),
(61444, 0, 0, 'Klan Harthak wird allen zeigen, warum sie die wahren Mogu sind!', 14, 0, 100, 15, 0, 27947, 0, 'Ming der Verschlagene to Schiedsrichter'),
(61444, 1, 0, '|TInterface\\Icons\\inv_elemental_primal_air.blp:20|t Ming der Verschlagene beschwört einen |cFFFF0000|Hspell:119981|h[wirbelnden Derwisch]|h|r!', 41, 0, 100, 15, 0, 0, 0, 'Ming der Verschlagene'),
(61444, 2, 0, 'Unser Klan ist der wahre Klan! Kein Eindringling vermag daran etwas zu ändern!', 14, 0, 100, 16, 0, 27948, 0, 'Ming der Verschlagene'),
(61444, 3, 0, 'Nur die Hunde vom Klan Gurthan würden andere für ihre eigenen Fehler verantwortlich machen! Euer Klan ist der verzweifeltste. Ihr wart es ganz bestimmt!', 14, 0, 100, 15, 0, 27955, 0, 'Ming der Verschlagene to Schiedsrichter'),
(61445, 0, 0, 'Ohne die Stärke unseres Klans werden die Mogu ihren rechtmäßigen Ruhm nicht zurückerlangen!', 14, 0, 100, 15, 0, 28248, 0, 'Haiyan der Unaufhaltsame'),
(61445, 1, 0, 'Klan Kargesh wird Euch zeigen, warum nur die Starken es verdient haben, an der Seite des Königs zu stehen!', 14, 0, 100, 15, 0, 28246, 0, 'Haiyan der Unaufhaltsame to Schiedsrichter'),
(61445, 2, 0, 'Unmöglich! Wir besitzen die größte Macht im ganzen Reich!', 14, 0, 100, 16, 0, 28247, 0, 'Haiyan der Unaufhaltsame'),
(61445, 3, 0, 'Klan Gurthan hat Recht! Nur der ehrlose Klan Harthak würde minderwertige Wesen seine Arbeit verrichten lassen. Ihr müsst Verräter sein!', 14, 0, 100, 0, 0, 28254, 0, 'Haiyan der Unaufhaltsame to Schiedsrichter'),
(61884, 0, 0, 'Ihr seid alle nutzlos! Die Wachen, die Ihr mir als Tribut überlasst, können nicht einmal diese minderwertigen Wesen von meinem Palast fernhalten.', 14, 0, 100, 15, 0, 28353, 0, 'Xin der Waffenmeister to Xin der Waffenmeister'),
(61884, 1, 0, 'Jetzt habt Ihr die Gelegenheit, Euch zu bewähren. Nehmt Euch dieser Eindringlinge an. Der Klan, der mir ihre Köpfe bringt, wird in meiner Gunst stehen.', 14, 0, 100, 14, 0, 28354, 0, 'Xin der Waffenmeister to Xin der Waffenmeister'),
(62352, 0, 0, 'Plündern und brandschatzen, bwahahaha!', 14, 0, 100, 0, 0, 32500, 0, 'Häuptling Salyis to Galleon'),
(63013, 0, 0, 'Kommt, setzt Euch und esst! Es ist nie die falsche Zeit für eine deftige Mahlzeit!', 12, 0, 100, 396, 0, 0, 0, 'Dan Ischerkoch to Player'),
(64149, 0, 0, 'Willkommen in der Goldenen Laterne, dem besten Gasthaus im Tal. Wenn jemand das bestreitet, bekommt er meine Fäuste zu spüren.', 12, 0, 100, 1, 0, 0, 0, 'Matrone Vi Vinh to Player'),
(64149, 1, 0, 'Willkommen. Lasst Euch hier nieder, wenn Ihr wünscht. Ihr findet die Bankfächer und die Stadttore im oberen Stockwerk. Händler aller Arten richten im ganzen Gebäude bereits ihre Läden ein. Bitte erkundet ruhig auf eigene Faust.', 12, 0, 100, 0, 0, 0, 0, 'Matrone Vi Vinh to Player'),
(64243, 0, 0, 'Ein Saurok läuft über eine versteckte Treppe mit einem Teil des Schatzes davon.', 41, 0, 100, 0, 0, 0, 0, 'Späher der Glintrok to Player'),
(64547, 0, 0, 'Geht wieder da raus und kämpft!', 12, 0, 100, 0, 0, 0, 0, 'Gurthanstreiter to Player'),
(64548, 0, 0, 'Ihr legt Euch mit dem falschen Mogu an, $R.', 12, 0, 100, 0, 0, 0, 0, 'Kargeshgrunzer to Player'),
(73330, 0, 0, 'Ah, wir sind wieder gemeinsam auf Reisen. Nur diesmal, fürchte ich, sind die Umstände deutlich ernster.', 14, 0, 100, 0, 0, 38120, 0, 'Lehrensucher Cho to Player'),
(73330, 1, 0, 'Das Land ist aufgewühlt, die immergrünen Bäume und Pflanzen verwelken, während das Wasser aus den Teichen abfließt.', 14, 0, 100, 0, 0, 38122, 0, 'Lehrensucher Cho to Player'),
(73330, 2, 0, 'Einst haben die Titanen diese lebensspendenden Gewässer benutzt, um alles Leben in Pandaria zu formen.', 14, 0, 100, 0, 0, 38124, 0, 'Lehrensucher Cho to Player'),
(73330, 3, 0, 'Es war dieses Wasser, das das Tal am Leben gehalten hat. Das überschüssige Wasser verwandelte das Tal der Vier Winde in das fruchtbarste Land der Welt!', 14, 0, 100, 0, 0, 38125, 0, 'Lehrensucher Cho to Player'),
(73330, 4, 0, 'Und nun hat sich die Boshaftigkeit des Alten Gottes in dem Wasser manifestiert.', 14, 0, 100, 0, 0, 38126, 0, 'Lehrensucher Cho to Player'),
(73330, 5, 0, 'Was für ein widerwärtiges Wesen - zerstört es, bevor es tief in den Boden sickert und ganz Pandaria besudelt!', 14, 0, 100, 0, 0, 38127, 0, 'Lehrensucher Cho to Player'),
(73349, 0, 0, 'Wir müssen die Becken vor der Verderbnis schützen!', 14, 0, 100, 1, 0, 38122, 0, 'Gequälter Initiand to Gequälter Initiand');