-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23360
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/13/2017 16:25:49


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=43294 AND `locale`='deDE') OR (`ID`=43244 AND `locale`='deDE') OR (`ID`=43293 AND `locale`='deDE') OR (`ID`=43243 AND `locale`='deDE') OR (`ID`=43303 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(43294, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23360),
(43244, 'deDE', 'Invasion: Tanaris', '', '', '', '', '', '', '', '', 23360),
(43293, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23360),
(43243, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23360),
(43303, 'deDE', 'Reif für Randale!', 'Sprecht mit David Globetrotter und findet heraus, wie man an der Rabenwehrrandale teilnimmt.', 'Willkommen zur Rabenwehrrandale! Es haben schon viele Kämpfer in der Arena der Rabenwehr teilgenommen, um ihre Stärke unter Beweis zu stellen, und jetzt seid Ihr an der Reihe.', '', '', '', '', '', '', 23360);


DELETE FROM `quest_objectives_locale` WHERE (`ID`=285159 AND `locale`='deDE');
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(285159, 'deDE', 43303, 0, 'Sprecht mit David Globetrotter', 23360);


DELETE FROM `gameobject_template_locale` WHERE (`entry`=213867 AND `locale`='deDE') OR (`entry`=213866 AND `locale`='deDE') OR (`entry`=213869 AND `locale`='deDE') OR (`entry`=213868 AND `locale`='deDE') OR (`entry`=214469 AND `locale`='deDE') OR (`entry`=214455 AND `locale`='deDE') OR (`entry`=212987 AND `locale`='deDE') OR (`entry`=212988 AND `locale`='deDE') OR (`entry`=214614 AND `locale`='deDE') OR (`entry`=213887 AND `locale`='deDE') OR (`entry`=213884 AND `locale`='deDE') OR (`entry`=213886 AND `locale`='deDE') OR (`entry`=214549 AND `locale`='deDE') OR (`entry`=214543 AND `locale`='deDE') OR (`entry`=214062 AND `locale`='deDE') OR (`entry`=214770 AND `locale`='deDE') OR (`entry`=213885 AND `locale`='deDE') OR (`entry`=213071 AND `locale`='deDE') OR (`entry`=213753 AND `locale`='deDE') OR (`entry`=213752 AND `locale`='deDE') OR (`entry`=213747 AND `locale`='deDE') OR (`entry`=213746 AND `locale`='deDE') OR (`entry`=213745 AND `locale`='deDE') OR (`entry`=213744 AND `locale`='deDE') OR (`entry`=213740 AND `locale`='deDE') OR (`entry`=187376 AND `locale`='deDE') OR (`entry`=215404 AND `locale`='deDE') OR (`entry`=213508 AND `locale`='deDE') OR (`entry`=213444 AND `locale`='deDE') OR (`entry`=213410 AND `locale`='deDE') OR (`entry`=213249 AND `locale`='deDE') OR (`entry`=211624 AND `locale`='deDE') OR (`entry`=211620 AND `locale`='deDE') OR (`entry`=211622 AND `locale`='deDE') OR (`entry`=211621 AND `locale`='deDE') OR (`entry`=212933 AND `locale`='deDE') OR (`entry`=212104 AND `locale`='deDE') OR (`entry`=212100 AND `locale`='deDE') OR (`entry`=212105 AND `locale`='deDE') OR (`entry`=212099 AND `locale`='deDE') OR (`entry`=215317 AND `locale`='deDE') OR (`entry`=204272 AND `locale`='deDE') OR (`entry`=216613 AND `locale`='deDE') OR (`entry`=214411 AND `locale`='deDE') OR (`entry`=213637 AND `locale`='deDE') OR (`entry`=213968 AND `locale`='deDE') OR (`entry`=213882 AND `locale`='deDE') OR (`entry`=213881 AND `locale`='deDE') OR (`entry`=213880 AND `locale`='deDE') OR (`entry`=211619 AND `locale`='deDE') OR (`entry`=212012 AND `locale`='deDE') OR (`entry`=212046 AND `locale`='deDE') OR (`entry`=216360 AND `locale`='deDE') OR (`entry`=212045 AND `locale`='deDE') OR (`entry`=212044 AND `locale`='deDE') OR (`entry`=212009 AND `locale`='deDE') OR (`entry`=212043 AND `locale`='deDE') OR (`entry`=215862 AND `locale`='deDE') OR (`entry`=213329 AND `locale`='deDE') OR (`entry`=212018 AND `locale`='deDE') OR (`entry`=213965 AND `locale`='deDE') OR (`entry`=212079 AND `locale`='deDE') OR (`entry`=212078 AND `locale`='deDE') OR (`entry`=212177 AND `locale`='deDE') OR (`entry`=212085 AND `locale`='deDE') OR (`entry`=212084 AND `locale`='deDE') OR (`entry`=213376 AND `locale`='deDE') OR (`entry`=213374 AND `locale`='deDE') OR (`entry`=213378 AND `locale`='deDE') OR (`entry`=213377 AND `locale`='deDE') OR (`entry`=213302 AND `locale`='deDE') OR (`entry`=213381 AND `locale`='deDE') OR (`entry`=212040 AND `locale`='deDE') OR (`entry`=212041 AND `locale`='deDE') OR (`entry`=212042 AND `locale`='deDE') OR (`entry`=212039 AND `locale`='deDE') OR (`entry`=213266 AND `locale`='deDE') OR (`entry`=212038 AND `locale`='deDE') OR (`entry`=212943 AND `locale`='deDE') OR (`entry`=212916 AND `locale`='deDE') OR (`entry`=213277 AND `locale`='deDE') OR (`entry`=213276 AND `locale`='deDE') OR (`entry`=214493 AND `locale`='deDE') OR (`entry`=212695 AND `locale`='deDE') OR (`entry`=214634 AND `locale`='deDE') OR (`entry`=214488 AND `locale`='deDE') OR (`entry`=214487 AND `locale`='deDE') OR (`entry`=214486 AND `locale`='deDE') OR (`entry`=214492 AND `locale`='deDE') OR (`entry`=214489 AND `locale`='deDE') OR (`entry`=214491 AND `locale`='deDE') OR (`entry`=214485 AND `locale`='deDE') OR (`entry`=214490 AND `locale`='deDE') OR (`entry`=214484 AND `locale`='deDE') OR (`entry`=214483 AND `locale`='deDE') OR (`entry`=213409 AND `locale`='deDE') OR (`entry`=214739 AND `locale`='deDE') OR (`entry`=214978 AND `locale`='deDE') OR (`entry`=215700 AND `locale`='deDE') OR (`entry`=215761 AND `locale`='deDE') OR (`entry`=214442 AND `locale`='deDE') OR (`entry`=215703 AND `locale`='deDE') OR (`entry`=213412 AND `locale`='deDE') OR (`entry`=215762 AND `locale`='deDE') OR (`entry`=214631 AND `locale`='deDE') OR (`entry`=214630 AND `locale`='deDE') OR (`entry`=212902 AND `locale`='deDE') OR (`entry`=213267 AND `locale`='deDE') OR (`entry`=214840 AND `locale`='deDE') OR (`entry`=214562 AND `locale`='deDE') OR (`entry`=213973 AND `locale`='deDE') OR (`entry`=214907 AND `locale`='deDE') OR (`entry`=212923 AND `locale`='deDE') OR (`entry`=214428 AND `locale`='deDE') OR (`entry`=213432 AND `locale`='deDE') OR (`entry`=214991 AND `locale`='deDE') OR (`entry`=215076 AND `locale`='deDE') OR (`entry`=213430 AND `locale`='deDE') OR (`entry`=213429 AND `locale`='deDE') OR (`entry`=213433 AND `locale`='deDE') OR (`entry`=213435 AND `locale`='deDE') OR (`entry`=213434 AND `locale`='deDE') OR (`entry`=213431 AND `locale`='deDE') OR (`entry`=214544 AND `locale`='deDE') OR (`entry`=212641 AND `locale`='deDE') OR (`entry`=213428 AND `locale`='deDE') OR (`entry`=213427 AND `locale`='deDE') OR (`entry`=214427 AND `locale`='deDE') OR (`entry`=214637 AND `locale`='deDE') OR (`entry`=214633 AND `locale`='deDE') OR (`entry`=211982 AND `locale`='deDE') OR (`entry`=211981 AND `locale`='deDE') OR (`entry`=211977 AND `locale`='deDE') OR (`entry`=211976 AND `locale`='deDE') OR (`entry`=209952 AND `locale`='deDE') OR (`entry`=214169 AND `locale`='deDE') OR (`entry`=212229 AND `locale`='deDE') OR (`entry`=214617 AND `locale`='deDE') OR (`entry`=215393 AND `locale`='deDE') OR (`entry`=212899 AND `locale`='deDE') OR (`entry`=214511 AND `locale`='deDE') OR (`entry`=214647 AND `locale`='deDE') OR (`entry`=214646 AND `locale`='deDE') OR (`entry`=212071 AND `locale`='deDE') OR (`entry`=220068 AND `locale`='deDE') OR (`entry`=223498 AND `locale`='deDE') OR (`entry`=215910 AND `locale`='deDE') OR (`entry`=214616 AND `locale`='deDE') OR (`entry`=214615 AND `locale`='deDE') OR (`entry`=214645 AND `locale`='deDE') OR (`entry`=213314 AND `locale`='deDE') OR (`entry`=213078 AND `locale`='deDE') OR (`entry`=214546 AND `locale`='deDE') OR (`entry`=214545 AND `locale`='deDE') OR (`entry`=212349 AND `locale`='deDE') OR (`entry`=209329 AND `locale`='deDE') OR (`entry`=213250 AND `locale`='deDE') OR (`entry`=214281 AND `locale`='deDE') OR (`entry`=214640 AND `locale`='deDE') OR (`entry`=214639 AND `locale`='deDE') OR (`entry`=214292 AND `locale`='deDE') OR (`entry`=215077 AND `locale`='deDE') OR (`entry`=214170 AND `locale`='deDE') OR (`entry`=221620 AND `locale`='deDE') OR (`entry`=212047 AND `locale`='deDE') OR (`entry`=212882 AND `locale`='deDE') OR (`entry`=212883 AND `locale`='deDE') OR (`entry`=212878 AND `locale`='deDE') OR (`entry`=212870 AND `locale`='deDE') OR (`entry`=212880 AND `locale`='deDE') OR (`entry`=210680 AND `locale`='deDE') OR (`entry`=212881 AND `locale`='deDE') OR (`entry`=212874 AND `locale`='deDE') OR (`entry`=212879 AND `locale`='deDE') OR (`entry`=214471 AND `locale`='deDE') OR (`entry`=221447 AND `locale`='deDE') OR (`entry`=221446 AND `locale`='deDE') OR (`entry`=211695 AND `locale`='deDE') OR (`entry`=221678 AND `locale`='deDE') OR (`entry`=221677 AND `locale`='deDE') OR (`entry`=221679 AND `locale`='deDE') OR (`entry`=221676 AND `locale`='deDE') OR (`entry`=209313 AND `locale`='deDE') OR (`entry`=209328 AND `locale`='deDE') OR (`entry`=214820 AND `locale`='deDE') OR (`entry`=211863 AND `locale`='deDE') OR (`entry`=211872 AND `locale`='deDE') OR (`entry`=211873 AND `locale`='deDE') OR (`entry`=211871 AND `locale`='deDE') OR (`entry`=212016 AND `locale`='deDE') OR (`entry`=211664 AND `locale`='deDE') OR (`entry`=211665 AND `locale`='deDE') OR (`entry`=211663 AND `locale`='deDE') OR (`entry`=209312 AND `locale`='deDE') OR (`entry`=213877 AND `locale`='deDE') OR (`entry`=213876 AND `locale`='deDE') OR (`entry`=212642 AND `locale`='deDE') OR (`entry`=212169 AND `locale`='deDE') OR (`entry`=212174 AND `locale`='deDE') OR (`entry`=214641 AND `locale`='deDE') OR (`entry`=212738 AND `locale`='deDE') OR (`entry`=209355 AND `locale`='deDE') OR (`entry`=215459 AND `locale`='deDE') OR (`entry`=210565 AND `locale`='deDE') OR (`entry`=215412 AND `locale`='deDE') OR (`entry`=213879 AND `locale`='deDE') OR (`entry`=213878 AND `locale`='deDE') OR (`entry`=213873 AND `locale`='deDE') OR (`entry`=213872 AND `locale`='deDE') OR (`entry`=213875 AND `locale`='deDE') OR (`entry`=213874 AND `locale`='deDE') OR (`entry`=213871 AND `locale`='deDE') OR (`entry`=213870 AND `locale`='deDE') OR (`entry`=212643 AND `locale`='deDE') OR (`entry`=212644 AND `locale`='deDE') OR (`entry`=209349 AND `locale`='deDE') OR (`entry`=209311 AND `locale`='deDE') OR (`entry`=215868 AND `locale`='deDE') OR (`entry`=213411 AND `locale`='deDE');
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(213867, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213866, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213869, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213868, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(214469, 'deDE', 'Rik''kals Klinge', '', NULL, 23360),
(214455, 'deDE', 'Resonanzkristall der Zan''thik', '', NULL, 23360),
(212987, 'deDE', 'Stacheliger Seestern', '', NULL, 23360),
(212988, 'deDE', 'Stacheliger Seestern', '', NULL, 23360),
(214614, 'deDE', 'Freudenfeuer', '', NULL, 23360),
(213887, 'deDE', 'Freudenfeuer', '', NULL, 23360),
(213884, 'deDE', 'Freudenfeuer', '', NULL, 23360),
(213886, 'deDE', 'Freudenfeuer', '', NULL, 23360),
(214549, 'deDE', 'Furchtsporenknolle', '', NULL, 23360),
(214543, 'deDE', 'Furchtsporenknolle', '', NULL, 23360),
(214062, 'deDE', 'Leuchtender Bern', '', NULL, 23360),
(214770, 'deDE', 'Freudenfeuer', '', NULL, 23360),
(213885, 'deDE', 'Freudenfeuer', '', NULL, 23360),
(213071, 'deDE', 'Leerer Krabbentopf', '', NULL, 23360),
(213753, 'deDE', 'Improvisierte Rute', '', NULL, 23360),
(213752, 'deDE', 'Klassische Rute', '', NULL, 23360),
(213747, 'deDE', 'NSC-Entchenschwimmer', '', NULL, 23360),
(213746, 'deDE', 'Bambusstange', '', NULL, 23360),
(213745, 'deDE', 'Angelständer', '', NULL, 23360),
(213744, 'deDE', 'Schlangenrute', '', NULL, 23360),
(213740, 'deDE', 'Ju Liens Angelgestell', '', NULL, 23360),
(187376, 'deDE', 'NSC-Fischköder', '', NULL, 23360),
(215404, 'deDE', 'Nebelschleier', '', NULL, 23360),
(213508, 'deDE', 'Voller Krabbentopf', '', NULL, 23360),
(213444, 'deDE', 'Unberührter Boden', '', NULL, 23360),
(213410, 'deDE', 'Mantisgesellschaft', '', NULL, 23360),
(213249, 'deDE', 'Effekt des Sha', '', NULL, 23360),
(211624, 'deDE', 'Effekt des Sha', '', NULL, 23360),
(211620, 'deDE', 'Effekt des Sha', '', NULL, 23360),
(211622, 'deDE', 'Effekt des Sha', '', NULL, 23360),
(211621, 'deDE', 'Effekt des Sha', '', NULL, 23360),
(212933, 'deDE', 'Sonarturm', '', NULL, 23360),
(212104, 'deDE', 'Krokiliskeneier', '', NULL, 23360),
(212100, 'deDE', 'Krokiliskeneier', '', NULL, 23360),
(212105, 'deDE', 'Krokiliskeneier', '', NULL, 23360),
(212099, 'deDE', 'Krokiliskeneier', '', NULL, 23360),
(215317, 'deDE', 'Riesige Banane', '', NULL, 23360),
(204272, 'deDE', 'Vermessungsgerät (grün)', '', NULL, 23360),
(216613, 'deDE', 'Briefkasten', '', NULL, 23360),
(214411, 'deDE', 'Der Angler und die Mönche', '', NULL, 23360),
(213637, 'deDE', 'PA_Fishing_Trapcrabs_01', '', NULL, 23360),
(213968, 'deDE', 'Schwärmendes Spaltbeil von Ka''roz', '', NULL, 23360),
(213882, 'deDE', 'Zierkissen', '', NULL, 23360),
(213881, 'deDE', 'Büstenständer', '', NULL, 23360),
(213880, 'deDE', 'Verschlissene Büste', '', NULL, 23360),
(211619, 'deDE', 'Effekt des Sha', '', NULL, 23360),
(212012, 'deDE', 'Bernfragment', '', NULL, 23360),
(212046, 'deDE', 'Freudenfeuer', '', NULL, 23360),
(216360, 'deDE', 'Ungezähmter Bern', '', NULL, 23360),
(212045, 'deDE', 'Freudenfeuer', '', NULL, 23360),
(212044, 'deDE', 'Freudenfeuer', '', NULL, 23360),
(212009, 'deDE', 'Bernfragment', '', NULL, 23360),
(212043, 'deDE', 'Freudenfeuer', '', NULL, 23360),
(215862, 'deDE', 'Saurokbanner', '', NULL, 23360),
(213329, 'deDE', 'Die Deserteure', '', NULL, 23360),
(212018, 'deDE', 'Frischer Kadaver', '', NULL, 23360),
(213965, 'deDE', 'Freudenfeuer', '', NULL, 23360),
(212079, 'deDE', 'Mantisrelikt', '', NULL, 23360),
(212078, 'deDE', 'Mantisrelikt', '', NULL, 23360),
(212177, 'deDE', 'Ein Schwarm Stachelfische', '', NULL, 23360),
(212085, 'deDE', 'Sternschatten', '', NULL, 23360),
(212084, 'deDE', 'Sternschatten', '', NULL, 23360),
(213376, 'deDE', 'Mantiskäfig', '', NULL, 23360),
(213374, 'deDE', 'Mantiskäfig', '', NULL, 23360),
(213378, 'deDE', 'Eierkammer', '', NULL, 23360),
(213377, 'deDE', 'Eierkammer', '', NULL, 23360),
(213302, 'deDE', 'Mazuströmung', '', NULL, 23360),
(213381, 'deDE', 'Aktives Signal', '', NULL, 23360),
(212040, 'deDE', 'Verstärkungskristall', '', NULL, 23360),
(212041, 'deDE', 'Verstärkungskristall', '', NULL, 23360),
(212042, 'deDE', 'Verstärkungskristall', '', NULL, 23360),
(212039, 'deDE', 'Verstärkungskristall', '', NULL, 23360),
(213266, 'deDE', 'Verstärkungskristall', '', NULL, 23360),
(212038, 'deDE', 'Uralter Bernbrocken', '', NULL, 23360),
(212943, 'deDE', 'Das Herz der Angst - Zor''lok - Tore zur finalen Phase', '', NULL, 23360),
(212916, 'deDE', 'Das Herz der Angst - Zor''lok - Arenawand', '', NULL, 23360),
(213277, 'deDE', 'Die Kammer der Kaiserin', '', NULL, 23360),
(213276, 'deDE', 'Decke der Mantiskönigin', '', NULL, 23360),
(214493, 'deDE', 'Doodad_Mantid_Door_033', '', NULL, 23360),
(212695, 'deDE', 'Garalontür', '', NULL, 23360),
(214634, 'deDE', 'Garalontür (oben)', '', NULL, 23360),
(214488, 'deDE', 'Doodad_Mantid_Door_025', '', NULL, 23360),
(214487, 'deDE', 'Doodad_Mantid_Door_024', '', NULL, 23360),
(214486, 'deDE', 'Doodad_Mantid_Door_022', '', NULL, 23360),
(214492, 'deDE', 'Doodad_Mantid_Door_032', '', NULL, 23360),
(214489, 'deDE', 'Doodad_Mantid_Door_026', '', NULL, 23360),
(214491, 'deDE', 'Doodad_Mantid_Door_031', '', NULL, 23360),
(214485, 'deDE', 'Doodad_Mantid_Door_021', '', NULL, 23360),
(214490, 'deDE', 'Doodad_Mantid_Door_027', '', NULL, 23360),
(214484, 'deDE', 'Doodad_Mantid_Door_020', '', NULL, 23360),
(214483, 'deDE', 'Doodad_Mantid_Door_017', '', NULL, 23360),
(213409, 'deDE', 'Zyklus der Mantis', '', NULL, 23360),
(214739, 'deDE', 'Sha Effect 09', '', NULL, 23360),
(214978, 'deDE', 'Versammlungsstein', '', NULL, 23360),
(215700, 'deDE', 'Nordöstlicher Fütterer', '', NULL, 23360),
(215761, 'deDE', 'Östlicher Fütterer', '', NULL, 23360),
(214442, 'deDE', 'Instance Portal (Raid 4 Difficulties)', '', NULL, 23360),
(215703, 'deDE', 'Nördlicher Fütterer', '', NULL, 23360),
(213412, 'deDE', 'Die Kaiserin', '', NULL, 23360),
(215762, 'deDE', 'Zentraler Fütterer', '', NULL, 23360),
(214631, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(214630, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(212902, 'deDE', 'Ausgehärteter Bern', '', NULL, 23360),
(213267, 'deDE', 'Angstschatten', '', NULL, 23360),
(214840, 'deDE', 'Sha-Bern', '', NULL, 23360),
(214562, 'deDE', 'Sha-besessener Kristall', '', NULL, 23360),
(213973, 'deDE', 'Schallrelais der Klaxxi', '', NULL, 23360),
(214907, 'deDE', 'Krabbenkessel', '', NULL, 23360),
(212923, 'deDE', 'Bernsammler', '', NULL, 23360),
(214428, 'deDE', 'Lins Tagebuch (II)', '', NULL, 23360),
(213432, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(214991, 'deDE', 'Briefkasten', '', NULL, 23360),
(215076, 'deDE', 'Amboss', '', NULL, 23360),
(213430, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213429, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213433, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213435, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213434, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213431, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(214544, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(212641, 'deDE', 'Angstverderbter Brüter', '', NULL, 23360),
(213428, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213427, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(214427, 'deDE', 'Lins Tagebuch (I)', '', NULL, 23360),
(214637, 'deDE', 'Gurthans Grabinschrift', '', NULL, 23360),
(214633, 'deDE', 'Leuchtende Urne', '', NULL, 23360),
(211982, 'deDE', 'Doodad_PA_GhostBrewery_Door_014', '', NULL, 23360),
(211981, 'deDE', 'Doodad_PA_GhostBrewery_Door_015', '', NULL, 23360),
(211977, 'deDE', 'Doodad_PA_GhostBrewery_Door_013', '', NULL, 23360),
(211976, 'deDE', 'Doodad_PA_GhostBrewery_Door_012', '', NULL, 23360),
(209952, 'deDE', 'Jademond', '', NULL, 23360),
(214169, 'deDE', 'Versammlungsstein', '', NULL, 23360),
(212229, 'deDE', 'Shado-Pan-Seil', '', NULL, 23360),
(214617, 'deDE', 'Waffenständer', '', NULL, 23360),
(215393, 'deDE', 'Shado-Pan-Seil', '', NULL, 23360),
(212899, 'deDE', 'Instance Portal (Party + Heroic + Challenge)', '', NULL, 23360),
(214511, 'deDE', 'Zerbrochener Bern', '', NULL, 23360),
(214647, 'deDE', 'Sha Effect 08', '', NULL, 23360),
(214646, 'deDE', 'Mantid Worker Busy (Sha)', '', NULL, 23360),
(212071, 'deDE', 'Lanns Bogen', '', NULL, 23360),
(220068, 'deDE', 'Briefkasten', '', NULL, 23360),
(223498, 'deDE', 'Schuldschein', '', NULL, 23360),
(215910, 'deDE', 'Sha GroundPatch Med Tendrils', '', NULL, 23360),
(214616, 'deDE', 'Waffenständer', '', NULL, 23360),
(214615, 'deDE', 'Waffenständer', '', NULL, 23360),
(214645, 'deDE', 'Low Poly Fire Anim (Sha)', '', NULL, 23360),
(213314, 'deDE', 'Gurthanitafel', '', NULL, 23360),
(213078, 'deDE', 'Oberkannte der Großen Mauer', '', NULL, 23360),
(214546, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(214545, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(212349, 'deDE', 'Waffenständer der Mantis', '', NULL, 23360),
(209329, 'deDE', 'Reiches Kyparitvorkommen', '', NULL, 23360),
(213250, 'deDE', 'Stilles Signal', '', NULL, 23360),
(214281, 'deDE', 'Signal von Kypari Ik', '', NULL, 23360),
(214640, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(214639, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(214292, 'deDE', 'Kunchongkäfig', '', NULL, 23360),
(215077, 'deDE', 'Schmiede', '', NULL, 23360),
(214170, 'deDE', 'Ei der Vor''thik', '', NULL, 23360),
(221620, 'deDE', 'Geheimnisvolle verzierte Tür', '', NULL, 23360),
(212047, 'deDE', 'Pandarenpfeile 02 Stapel', '', NULL, 23360),
(212882, 'deDE', 'Mogu Crate, Type 2', '', NULL, 23360),
(212883, 'deDE', 'Mogulagerfeuer', '', NULL, 23360),
(212878, 'deDE', 'Mogu Tent, Slightly Smaller', '', NULL, 23360),
(212870, 'deDE', 'Mogukohlenbecken', '', NULL, 23360),
(212880, 'deDE', 'Moguteppich', '', NULL, 23360),
(210680, 'deDE', 'Moguzelt', '', NULL, 23360),
(212881, 'deDE', 'Mogu Crate, Wide', '', NULL, 23360),
(212874, 'deDE', 'Mogubanner', '', NULL, 23360),
(212879, 'deDE', 'Mogu Crate, Type 1', '', NULL, 23360),
(214471, 'deDE', 'Shao-Tien-Käfig', '', NULL, 23360),
(221447, 'deDE', 'Ausgang von Norushen', '', NULL, 23360),
(221446, 'deDE', 'Eingang des Stolzes', '', NULL, 23360),
(211695, 'deDE', 'Pfeil der Shado-Pan', '', NULL, 23360),
(221678, 'deDE', 'Ostschattengefängnis', '', NULL, 23360),
(221677, 'deDE', 'Nordschattengefängnis', '', NULL, 23360),
(221679, 'deDE', 'Südschattengefängnis', '', NULL, 23360),
(221676, 'deDE', 'Westschattengefängnis', '', NULL, 23360),
(209313, 'deDE', 'Trilliumader', '', NULL, 23360),
(209328, 'deDE', 'Reiches Geistereisenvorkommen', '', NULL, 23360),
(214820, 'deDE', 'Krik''thik-Bein', '', NULL, 23360),
(211863, 'deDE', 'Krik''thik-Bein', '', NULL, 23360),
(211872, 'deDE', 'Schreckenskugel', '', NULL, 23360),
(211873, 'deDE', 'Schreckenskugel', '', NULL, 23360),
(211871, 'deDE', 'Schreckenskugel', '', NULL, 23360),
(212016, 'deDE', 'Drachenschleuder von Gao-Ran', '', NULL, 23360),
(211664, 'deDE', 'Uralte Guo-Lai-Tür', '', NULL, 23360),
(211665, 'deDE', 'Uralte Guo-Lai-Tür', '', NULL, 23360),
(211663, 'deDE', 'Uralte Guo-Lai-Tür', '', NULL, 23360),
(209312, 'deDE', 'Kyparitvorkommen', '', NULL, 23360),
(213877, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213876, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(212642, 'deDE', 'Herzwurzel von Kypari Kor', '', NULL, 23360),
(212169, 'deDE', 'Ein Schwarm riesiger Mantisgarnelen', '', NULL, 23360),
(212174, 'deDE', 'Ein Schwarm Riffkraken', '', NULL, 23360),
(214641, 'deDE', 'Tür', '', NULL, 23360),
(212738, 'deDE', 'Gewaltiger Landrutsch', '', NULL, 23360),
(209355, 'deDE', 'Narrenkappe', '', NULL, 23360),
(215459, 'deDE', 'Doodad_Mantid_Amber_Door001', '', NULL, 23360),
(210565, 'deDE', 'Dunkle Erde', '', NULL, 23360),
(215412, 'deDE', 'Sha-berührtes Kraut', '', NULL, 23360),
(213879, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213878, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213873, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213872, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213875, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213874, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213871, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(213870, 'deDE', 'Kohlenpfanne', '', NULL, 23360),
(212643, 'deDE', 'Mal der Kaiserin', '', NULL, 23360),
(212644, 'deDE', 'Agitationskristall', '', NULL, 23360),
(209349, 'deDE', 'Teepflanze', '', NULL, 23360),
(209311, 'deDE', 'Geistereisenvorkommen', '', NULL, 23360),
(215868, 'deDE', 'Mantisstatue', '', NULL, 23360),
(213411, 'deDE', 'Bern', '', NULL, 23360);


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=14673 AND `id`=1) OR (`menu_id`=14673 AND `id`=0) OR (`menu_id`=14663 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(14673, 1, '', '', 'Bringt mich in den Salzigen Schlick.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14673, 0, '', '', 'Ich muss irgendwohin reisen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14663, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', '');


DELETE FROM `locales_creature_text` WHERE (`entry`=50739 AND `groupid`=0 ) OR (`entry`=50776 AND `groupid`=0 ) OR (`entry`=50805 AND `groupid`=0 ) OR (`entry`=65365 AND `groupid`=0 ) OR (`entry`=65478 AND `groupid`=0 ) OR (`entry`=65478 AND `groupid`=1);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(50739, 0, 0, '', '', 'Die Jungen werden sich an Eurem Leichnam gütlich tun!', '', '', '', '', ''),
(50776, 0, 0, '', '', 'Die Wellen erzittern, wenn Ihr Euch nähert...', '', '', '', '', ''),
(50805, 0, 0, '', '', 'Nein! Mein Gold!', '', '', '', '', ''),
(65365, 0, 0, '', '', 'Ist er weg? Ist es vorbei?', '', '', '', '', ''),
(65478, 0, 0, '', '', 'Gebt mir den Resonanzkristall, Ihr mickriger kleiner Rindenfresser!', '', '', '', '', ''),
(65478, 1, 0, '', '', 'Es ist noch nicht vorbei...', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=64979 /*64979*/ AND `locale`='deDE') OR (`entry`=65253 /*65253*/ AND `locale`='deDE') OR (`entry`=66852 /*66852*/ AND `locale`='deDE') OR (`entry`=64995 /*64995*/ AND `locale`='deDE') OR (`entry`=65372 /*65372*/ AND `locale`='deDE') OR (`entry`=65787 /*65787*/ AND `locale`='deDE') OR (`entry`=64983 /*64983*/ AND `locale`='deDE') OR (`entry`=66935 /*66935*/ AND `locale`='deDE') OR (`entry`=64970 /*64970*/ AND `locale`='deDE') OR (`entry`=64982 /*64982*/ AND `locale`='deDE') OR (`entry`=65231 /*65231*/ AND `locale`='deDE') OR (`entry`=63348 /*63348*/ AND `locale`='deDE') OR (`entry`=63339 /*63339*/ AND `locale`='deDE') OR (`entry`=63353 /*63353*/ AND `locale`='deDE') OR (`entry`=65274 /*65274*/ AND `locale`='deDE') OR (`entry`=64485 /*64485*/ AND `locale`='deDE') OR (`entry`=65203 /*65203*/ AND `locale`='deDE') OR (`entry`=63997 /*63997*/ AND `locale`='deDE') OR (`entry`=65330 /*65330*/ AND `locale`='deDE') OR (`entry`=50805 /*50805*/ AND `locale`='deDE') OR (`entry`=64973 /*64973*/ AND `locale`='deDE') OR (`entry`=64971 /*64971*/ AND `locale`='deDE') OR (`entry`=64253 /*64253*/ AND `locale`='deDE') OR (`entry`=64555 /*64555*/ AND `locale`='deDE') OR (`entry`=63998 /*63998*/ AND `locale`='deDE') OR (`entry`=65792 /*65792*/ AND `locale`='deDE') OR (`entry`=67088 /*67088*/ AND `locale`='deDE') OR (`entry`=62853 /*62853*/ AND `locale`='deDE') OR (`entry`=65349 /*65349*/ AND `locale`='deDE') OR (`entry`=64008 /*64008*/ AND `locale`='deDE') OR (`entry`=66752 /*66752*/ AND `locale`='deDE') OR (`entry`=68462 /*68462*/ AND `locale`='deDE') OR (`entry`=63375 /*63375*/ AND `locale`='deDE') OR (`entry`=63376 /*63376*/ AND `locale`='deDE') OR (`entry`=63470 /*63470*/ AND `locale`='deDE') OR (`entry`=64259 /*64259*/ AND `locale`='deDE') OR (`entry`=63999 /*63999*/ AND `locale`='deDE') OR (`entry`=63944 /*63944*/ AND `locale`='deDE') OR (`entry`=69768 /*69768*/ AND `locale`='deDE') OR (`entry`=63369 /*63369*/ AND `locale`='deDE') OR (`entry`=63368 /*63368*/ AND `locale`='deDE') OR (`entry`=67100 /*67100*/ AND `locale`='deDE') OR (`entry`=64680 /*64680*/ AND `locale`='deDE') OR (`entry`=62008 /*62008*/ AND `locale`='deDE') OR (`entry`=66249 /*66249*/ AND `locale`='deDE') OR (`entry`=6250 /*6250*/ AND `locale`='deDE') OR (`entry`=63277 /*63277*/ AND `locale`='deDE') OR (`entry`=63498 /*63498*/ AND `locale`='deDE') OR (`entry`=63016 /*63016*/ AND `locale`='deDE') OR (`entry`=67181 /*67181*/ AND `locale`='deDE') OR (`entry`=67180 /*67180*/ AND `locale`='deDE') OR (`entry`=63349 /*63349*/ AND `locale`='deDE') OR (`entry`=63280 /*63280*/ AND `locale`='deDE') OR (`entry`=63317 /*63317*/ AND `locale`='deDE') OR (`entry`=63329 /*63329*/ AND `locale`='deDE') OR (`entry`=63827 /*63827*/ AND `locale`='deDE') OR (`entry`=50776 /*50776*/ AND `locale`='deDE') OR (`entry`=63833 /*63833*/ AND `locale`='deDE') OR (`entry`=63726 /*63726*/ AND `locale`='deDE') OR (`entry`=63257 /*63257*/ AND `locale`='deDE') OR (`entry`=63176 /*63176*/ AND `locale`='deDE') OR (`entry`=60025 /*60025*/ AND `locale`='deDE') OR (`entry`=65118 /*65118*/ AND `locale`='deDE') OR (`entry`=63728 /*63728*/ AND `locale`='deDE') OR (`entry`=63725 /*63725*/ AND `locale`='deDE') OR (`entry`=63179 /*63179*/ AND `locale`='deDE') OR (`entry`=65357 /*65357*/ AND `locale`='deDE') OR (`entry`=63729 /*63729*/ AND `locale`='deDE') OR (`entry`=66880 /*66880*/ AND `locale`='deDE') OR (`entry`=61970 /*61970*/ AND `locale`='deDE') OR (`entry`=62023 /*62023*/ AND `locale`='deDE') OR (`entry`=59113 /*59113*/ AND `locale`='deDE') OR (`entry`=64641 /*64641*/ AND `locale`='deDE') OR (`entry`=50836 /*50836*/ AND `locale`='deDE') OR (`entry`=66587 /*66587*/ AND `locale`='deDE') OR (`entry`=69806 /*69806*/ AND `locale`='deDE') OR (`entry`=65887 /*65887*/ AND `locale`='deDE') OR (`entry`=69842 /*69842*/ AND `locale`='deDE') OR (`entry`=69769 /*69769*/ AND `locale`='deDE') OR (`entry`=65365 /*65365*/ AND `locale`='deDE') OR (`entry`=65486 /*65486*/ AND `locale`='deDE') OR (`entry`=65475 /*65475*/ AND `locale`='deDE') OR (`entry`=62074 /*62074*/ AND `locale`='deDE') OR (`entry`=62090 /*62090*/ AND `locale`='deDE') OR (`entry`=62778 /*62778*/ AND `locale`='deDE') OR (`entry`=62773 /*62773*/ AND `locale`='deDE') OR (`entry`=63035 /*63035*/ AND `locale`='deDE') OR (`entry`=64357 /*64357*/ AND `locale`='deDE') OR (`entry`=62543 /*62543*/ AND `locale`='deDE') OR (`entry`=63031 /*63031*/ AND `locale`='deDE') OR (`entry`=64339 /*64339*/ AND `locale`='deDE') OR (`entry`=63030 /*63030*/ AND `locale`='deDE') OR (`entry`=66181 /*66181*/ AND `locale`='deDE') OR (`entry`=64341 /*64341*/ AND `locale`='deDE') OR (`entry`=64338 /*64338*/ AND `locale`='deDE') OR (`entry`=64340 /*64340*/ AND `locale`='deDE') OR (`entry`=63563 /*63563*/ AND `locale`='deDE') OR (`entry`=64358 /*64358*/ AND `locale`='deDE') OR (`entry`=62980 /*62980*/ AND `locale`='deDE') OR (`entry`=67177 /*67177*/ AND `locale`='deDE') OR (`entry`=64353 /*64353*/ AND `locale`='deDE') OR (`entry`=64902 /*64902*/ AND `locale`='deDE') OR (`entry`=64355 /*64355*/ AND `locale`='deDE') OR (`entry`=63853 /*63853*/ AND `locale`='deDE') OR (`entry`=63032 /*63032*/ AND `locale`='deDE') OR (`entry`=65500 /*65500*/ AND `locale`='deDE') OR (`entry`=65499 /*65499*/ AND `locale`='deDE') OR (`entry`=65498 /*65498*/ AND `locale`='deDE') OR (`entry`=65501 /*65501*/ AND `locale`='deDE') OR (`entry`=63630 /*63630*/ AND `locale`='deDE') OR (`entry`=63629 /*63629*/ AND `locale`='deDE') OR (`entry`=63424 /*63424*/ AND `locale`='deDE') OR (`entry`=63628 /*63628*/ AND `locale`='deDE') OR (`entry`=63214 /*63214*/ AND `locale`='deDE') OR (`entry`=63213 /*63213*/ AND `locale`='deDE') OR (`entry`=63212 /*63212*/ AND `locale`='deDE') OR (`entry`=63421 /*63421*/ AND `locale`='deDE') OR (`entry`=65597 /*65597*/ AND `locale`='deDE') OR (`entry`=63209 /*63209*/ AND `locale`='deDE') OR (`entry`=63633 /*63633*/ AND `locale`='deDE') OR (`entry`=63208 /*63208*/ AND `locale`='deDE') OR (`entry`=63423 /*63423*/ AND `locale`='deDE') OR (`entry`=63632 /*63632*/ AND `locale`='deDE') OR (`entry`=63207 /*63207*/ AND `locale`='deDE') OR (`entry`=65593 /*65593*/ AND `locale`='deDE') OR (`entry`=63631 /*63631*/ AND `locale`='deDE') OR (`entry`=63292 /*63292*/ AND `locale`='deDE') OR (`entry`=62762 /*62762*/ AND `locale`='deDE') OR (`entry`=64916 /*64916*/ AND `locale`='deDE') OR (`entry`=64917 /*64917*/ AND `locale`='deDE') OR (`entry`=63049 /*63049*/ AND `locale`='deDE') OR (`entry`=36737 /*36737*/ AND `locale`='deDE') OR (`entry`=65524 /*65524*/ AND `locale`='deDE') OR (`entry`=65519 /*65519*/ AND `locale`='deDE') OR (`entry`=65521 /*65521*/ AND `locale`='deDE') OR (`entry`=64826 /*64826*/ AND `locale`='deDE') OR (`entry`=63048 /*63048*/ AND `locale`='deDE') OR (`entry`=63036 /*63036*/ AND `locale`='deDE') OR (`entry`=62863 /*62863*/ AND `locale`='deDE') OR (`entry`=66893 /*66893*/ AND `locale`='deDE') OR (`entry`=64890 /*64890*/ AND `locale`='deDE') OR (`entry`=66894 /*66894*/ AND `locale`='deDE') OR (`entry`=64714 /*64714*/ AND `locale`='deDE') OR (`entry`=50739 /*50739*/ AND `locale`='deDE') OR (`entry`=56169 /*56169*/ AND `locale`='deDE') OR (`entry`=63586 /*63586*/ AND `locale`='deDE') OR (`entry`=62073 /*62073*/ AND `locale`='deDE') OR (`entry`=63588 /*63588*/ AND `locale`='deDE') OR (`entry`=63587 /*63587*/ AND `locale`='deDE') OR (`entry`=62076 /*62076*/ AND `locale`='deDE') OR (`entry`=62075 /*62075*/ AND `locale`='deDE') OR (`entry`=62160 /*62160*/ AND `locale`='deDE') OR (`entry`=62162 /*62162*/ AND `locale`='deDE') OR (`entry`=65004 /*65004*/ AND `locale`='deDE') OR (`entry`=59829 /*59829*/ AND `locale`='deDE') OR (`entry`=55626 /*55626*/ AND `locale`='deDE') OR (`entry`=65886 /*65886*/ AND `locale`='deDE') OR (`entry`=63993 /*63993*/ AND `locale`='deDE') OR (`entry`=64352 /*64352*/ AND `locale`='deDE') OR (`entry`=65790 /*65790*/ AND `locale`='deDE') OR (`entry`=64806 /*64806*/ AND `locale`='deDE') OR (`entry`=63981 /*63981*/ AND `locale`='deDE') OR (`entry`=65511 /*65511*/ AND `locale`='deDE') OR (`entry`=65432 /*65432*/ AND `locale`='deDE') OR (`entry`=63206 /*63206*/ AND `locale`='deDE') OR (`entry`=66595 /*66595*/ AND `locale`='deDE') OR (`entry`=65328 /*65328*/ AND `locale`='deDE') OR (`entry`=63871 /*63871*/ AND `locale`='deDE') OR (`entry`=77120 /*77120*/ AND `locale`='deDE') OR (`entry`=80005 /*80005*/ AND `locale`='deDE') OR (`entry`=65220 /*65220*/ AND `locale`='deDE') OR (`entry`=66739 /*66739*/ AND `locale`='deDE') OR (`entry`=66724 /*66724*/ AND `locale`='deDE') OR (`entry`=66723 /*66723*/ AND `locale`='deDE') OR (`entry`=66722 /*66722*/ AND `locale`='deDE') OR (`entry`=65186 /*65186*/ AND `locale`='deDE') OR (`entry`=63501 /*63501*/ AND `locale`='deDE') OR (`entry`=62538 /*62538*/ AND `locale`='deDE') OR (`entry`=62774 /*62774*/ AND `locale`='deDE') OR (`entry`=68536 /*68536*/ AND `locale`='deDE') OR (`entry`=64599 /*64599*/ AND `locale`='deDE') OR (`entry`=67064 /*67064*/ AND `locale`='deDE') OR (`entry`=64815 /*64815*/ AND `locale`='deDE') OR (`entry`=62518 /*62518*/ AND `locale`='deDE') OR (`entry`=66250 /*66250*/ AND `locale`='deDE') OR (`entry`=65397 /*65397*/ AND `locale`='deDE') OR (`entry`=65753 /*65753*/ AND `locale`='deDE') OR (`entry`=65396 /*65396*/ AND `locale`='deDE') OR (`entry`=65398 /*65398*/ AND `locale`='deDE') OR (`entry`=65395 /*65395*/ AND `locale`='deDE') OR (`entry`=62521 /*62521*/ AND `locale`='deDE') OR (`entry`=62519 /*62519*/ AND `locale`='deDE') OR (`entry`=62517 /*62517*/ AND `locale`='deDE') OR (`entry`=65512 /*65512*/ AND `locale`='deDE') OR (`entry`=56740 /*56740*/ AND `locale`='deDE') OR (`entry`=58892 /*58892*/ AND `locale`='deDE') OR (`entry`=64246 /*64246*/ AND `locale`='deDE') OR (`entry`=68559 /*68559*/ AND `locale`='deDE') OR (`entry`=63842 /*63842*/ AND `locale`='deDE') OR (`entry`=58065 /*58065*/ AND `locale`='deDE') OR (`entry`=63497 /*63497*/ AND `locale`='deDE') OR (`entry`=65172 /*65172*/ AND `locale`='deDE') OR (`entry`=62112 /*62112*/ AND `locale`='deDE') OR (`entry`=61968 /*61968*/ AND `locale`='deDE') OR (`entry`=61982 /*61982*/ AND `locale`='deDE') OR (`entry`=62000 /*62000*/ AND `locale`='deDE') OR (`entry`=64336 /*64336*/ AND `locale`='deDE') OR (`entry`=65269 /*65269*/ AND `locale`='deDE') OR (`entry`=62203 /*62203*/ AND `locale`='deDE') OR (`entry`=62143 /*62143*/ AND `locale`='deDE') OR (`entry`=62077 /*62077*/ AND `locale`='deDE') OR (`entry`=61981 /*61981*/ AND `locale`='deDE') OR (`entry`=75691 /*75691*/ AND `locale`='deDE') OR (`entry`=75689 /*75689*/ AND `locale`='deDE') OR (`entry`=65865 /*65865*/ AND `locale`='deDE') OR (`entry`=78456 /*78456*/ AND `locale`='deDE') OR (`entry`=75694 /*75694*/ AND `locale`='deDE') OR (`entry`=75692 /*75692*/ AND `locale`='deDE') OR (`entry`=65452 /*65452*/ AND `locale`='deDE') OR (`entry`=65450 /*65450*/ AND `locale`='deDE') OR (`entry`=63755 /*63755*/ AND `locale`='deDE') OR (`entry`=63731 /*63731*/ AND `locale`='deDE') OR (`entry`=57196 /*57196*/ AND `locale`='deDE') OR (`entry`=59832 /*59832*/ AND `locale`='deDE') OR (`entry`=57195 /*57195*/ AND `locale`='deDE') OR (`entry`=66187 /*66187*/ AND `locale`='deDE') OR (`entry`=65582 /*65582*/ AND `locale`='deDE') OR (`entry`=65646 /*65646*/ AND `locale`='deDE') OR (`entry`=65632 /*65632*/ AND `locale`='deDE') OR (`entry`=64622 /*64622*/ AND `locale`='deDE') OR (`entry`=62582 /*62582*/ AND `locale`='deDE') OR (`entry`=64559 /*64559*/ AND `locale`='deDE') OR (`entry`=64831 /*64831*/ AND `locale`='deDE') OR (`entry`=66465 /*66465*/ AND `locale`='deDE') OR (`entry`=67160 /*67160*/ AND `locale`='deDE') OR (`entry`=64849 /*64849*/ AND `locale`='deDE') OR (`entry`=62832 /*62832*/ AND `locale`='deDE') OR (`entry`=65575 /*65575*/ AND `locale`='deDE') OR (`entry`=63548 /*63548*/ AND `locale`='deDE') OR (`entry`=66599 /*66599*/ AND `locale`='deDE') OR (`entry`=66605 /*66605*/ AND `locale`='deDE') OR (`entry`=62029 /*62029*/ AND `locale`='deDE') OR (`entry`=63740 /*63740*/ AND `locale`='deDE') OR (`entry`=62386 /*62386*/ AND `locale`='deDE') OR (`entry`=65336 /*65336*/ AND `locale`='deDE') OR (`entry`=65335 /*65335*/ AND `locale`='deDE') OR (`entry`=57444 /*57444*/ AND `locale`='deDE') OR (`entry`=63786 /*63786*/ AND `locale`='deDE') OR (`entry`=64687 /*64687*/ AND `locale`='deDE') OR (`entry`=64263 /*64263*/ AND `locale`='deDE') OR (`entry`=64260 /*64260*/ AND `locale`='deDE') OR (`entry`=64369 /*64369*/ AND `locale`='deDE') OR (`entry`=61426 /*61426*/ AND `locale`='deDE') OR (`entry`=63973 /*63973*/ AND `locale`='deDE') OR (`entry`=63976 /*63976*/ AND `locale`='deDE') OR (`entry`=63974 /*63974*/ AND `locale`='deDE') OR (`entry`=61466 /*61466*/ AND `locale`='deDE') OR (`entry`=66391 /*66391*/ AND `locale`='deDE') OR (`entry`=64275 /*64275*/ AND `locale`='deDE') OR (`entry`=65685 /*65685*/ AND `locale`='deDE') OR (`entry`=64274 /*64274*/ AND `locale`='deDE') OR (`entry`=65584 /*65584*/ AND `locale`='deDE') OR (`entry`=63972 /*63972*/ AND `locale`='deDE') OR (`entry`=65573 /*65573*/ AND `locale`='deDE') OR (`entry`=65496 /*65496*/ AND `locale`='deDE') OR (`entry`=61702 /*61702*/ AND `locale`='deDE') OR (`entry`=61746 /*61746*/ AND `locale`='deDE') OR (`entry`=61019 /*61019*/ AND `locale`='deDE') OR (`entry`=61083 /*61083*/ AND `locale`='deDE') OR (`entry`=61082 /*61082*/ AND `locale`='deDE') OR (`entry`=61181 /*61181*/ AND `locale`='deDE') OR (`entry`=61881 /*61881*/ AND `locale`='deDE') OR (`entry`=61880 /*61880*/ AND `locale`='deDE') OR (`entry`=61018 /*61018*/ AND `locale`='deDE') OR (`entry`=61017 /*61017*/ AND `locale`='deDE') OR (`entry`=61754 /*61754*/ AND `locale`='deDE') OR (`entry`=66319 /*66319*/ AND `locale`='deDE') OR (`entry`=61692 /*61692*/ AND `locale`='deDE') OR (`entry`=61365 /*61365*/ AND `locale`='deDE') OR (`entry`=66194 /*66194*/ AND `locale`='deDE') OR (`entry`=66184 /*66184*/ AND `locale`='deDE') OR (`entry`=64626 /*64626*/ AND `locale`='deDE') OR (`entry`=62814 /*62814*/ AND `locale`='deDE') OR (`entry`=62813 /*62813*/ AND `locale`='deDE') OR (`entry`=65429 /*65429*/ AND `locale`='deDE') OR (`entry`=65551 /*65551*/ AND `locale`='deDE') OR (`entry`=62771 /*62771*/ AND `locale`='deDE') OR (`entry`=62772 /*62772*/ AND `locale`='deDE') OR (`entry`=62563 /*62563*/ AND `locale`='deDE') OR (`entry`=50791 /*50791*/ AND `locale`='deDE') OR (`entry`=62128 /*62128*/ AND `locale`='deDE') OR (`entry`=62765 /*62765*/ AND `locale`='deDE') OR (`entry`=62587 /*62587*/ AND `locale`='deDE') OR (`entry`=62766 /*62766*/ AND `locale`='deDE') OR (`entry`=62767 /*62767*/ AND `locale`='deDE') OR (`entry`=66549 /*66549*/ AND `locale`='deDE') OR (`entry`=62601 /*62601*/ AND `locale`='deDE') OR (`entry`=63007 /*63007*/ AND `locale`='deDE') OR (`entry`=62763 /*62763*/ AND `locale`='deDE') OR (`entry`=50821 /*50821*/ AND `locale`='deDE') OR (`entry`=65404 /*65404*/ AND `locale`='deDE') OR (`entry`=62764 /*62764*/ AND `locale`='deDE') OR (`entry`=62770 /*62770*/ AND `locale`='deDE') OR (`entry`=65995 /*65995*/ AND `locale`='deDE') OR (`entry`=64720 /*64720*/ AND `locale`='deDE') OR (`entry`=66188 /*66188*/ AND `locale`='deDE') OR (`entry`=65187 /*65187*/ AND `locale`='deDE') OR (`entry`=41200 /*41200*/ AND `locale`='deDE') OR (`entry`=64238 /*64238*/ AND `locale`='deDE') OR (`entry`=64717 /*64717*/ AND `locale`='deDE') OR (`entry`=62833 /*62833*/ AND `locale`='deDE') OR (`entry`=64805 /*64805*/ AND `locale`='deDE') OR (`entry`=55370 /*55370*/ AND `locale`='deDE') OR (`entry`=63420 /*63420*/ AND `locale`='deDE') OR (`entry`=65996 /*65996*/ AND `locale`='deDE') OR (`entry`=64807 /*64807*/ AND `locale`='deDE') OR (`entry`=62761 /*62761*/ AND `locale`='deDE') OR (`entry`=62751 /*62751*/ AND `locale`='deDE') OR (`entry`=64804 /*64804*/ AND `locale`='deDE') OR (`entry`=64242 /*64242*/ AND `locale`='deDE') OR (`entry`=62760 /*62760*/ AND `locale`='deDE') OR (`entry`=61971 /*61971*/ AND `locale`='deDE') OR (`entry`=62758 /*62758*/ AND `locale`='deDE') OR (`entry`=62756 /*62756*/ AND `locale`='deDE') OR (`entry`=61811 /*61811*/ AND `locale`='deDE') OR (`entry`=62843 /*62843*/ AND `locale`='deDE') OR (`entry`=61302 /*61302*/ AND `locale`='deDE') OR (`entry`=62755 /*62755*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(64979, 'deDE', 'Vesswache Vik''az', NULL, NULL, NULL, 23360), -- 64979
(65253, 'deDE', 'Rik''kal der Sezierer', NULL, NULL, NULL, 23360), -- 65253
(66852, 'deDE', 'Riesiger Stachelrochen', NULL, NULL, NULL, 23360), -- 66852
(64995, 'deDE', 'Gehilfe Sek''ot', NULL, 'Hand der Shek''zeer', NULL, 23360), -- 64995
(65372, 'deDE', 'Sonic Beam Bunny', NULL, NULL, NULL, 23360), -- 65372
(65787, 'deDE', 'Resonator der Zan''thik', NULL, NULL, NULL, 23360), -- 65787
(64983, 'deDE', 'Mit Bern gefüttertes Jungtier', NULL, NULL, NULL, 23360), -- 64983
(66935, 'deDE', 'Muschelstok', NULL, 'Nachkomme von Kril''mandar', NULL, 23360), -- 66935
(64970, 'deDE', 'Gedankengefesselter Minenarbeiter', NULL, NULL, NULL, 23360), -- 64970
(64982, 'deDE', 'Bernhülle der Zan''thik', NULL, NULL, NULL, 23360), -- 64982
(65231, 'deDE', 'Kyparitkriecher', NULL, NULL, NULL, 23360), -- 65231
(63348, 'deDE', 'Salzklacker', NULL, NULL, NULL, 23360), -- 63348
(63339, 'deDE', 'Hautschinder der Schlickschuppen', NULL, NULL, NULL, 23360), -- 63339
(63353, 'deDE', 'Schreckensschiffshalter', NULL, NULL, NULL, 23360), -- 63353
(65274, 'deDE', 'Manipulator der Zan''thik', NULL, NULL, NULL, 23360), -- 65274
(64485, 'deDE', 'Junger Kunchong', NULL, NULL, NULL, 23360), -- 64485
(65203, 'deDE', 'Kaiserkrabbe', NULL, NULL, 'wildpetcapturable', 23360), -- 65203
(63997, 'deDE', 'Fleischjäger der Schlickschuppen', NULL, NULL, NULL, 23360), -- 63997
(65330, 'deDE', 'Gezähmter Salzschuppenschnapper', NULL, NULL, NULL, 23360), -- 65330
(50805, 'deDE', 'Omnis Feixmaul', NULL, NULL, NULL, 23360), -- 50805
(64973, 'deDE', 'Manipulator der Zan''thik', NULL, NULL, NULL, 23360), -- 64973
(64971, 'deDE', 'Pfähler der Zan''thik', NULL, NULL, NULL, 23360), -- 64971
(64253, 'deDE', 'Finding Kovok - Effects Bunny', NULL, NULL, NULL, 23360), -- 64253
(64555, 'deDE', 'Leuchtberneffekt', NULL, NULL, NULL, 23360), -- 64555
(63998, 'deDE', 'Orakel Zish''et', NULL, NULL, NULL, 23360), -- 63998
(65792, 'deDE', 'Krabbenhaufen', NULL, NULL, NULL, 23360), -- 65792
(67088, 'deDE', 'Versteckter Getreuer', NULL, NULL, NULL, 23360), -- 67088
(62853, 'deDE', 'Paragon 05 Bunny', NULL, NULL, NULL, 23360), -- 62853
(65349, 'deDE', 'Totem der Heilungsflut', NULL, NULL, NULL, 23360), -- 65349
(64008, 'deDE', 'Schamane der Schlickschuppen', NULL, NULL, NULL, 23360), -- 64008
(66752, 'deDE', 'Haijunges', NULL, NULL, NULL, 23360), -- 66752
(68462, 'deDE', 'Fließender Pandarengeist', NULL, 'Großmeistertierzähmer', NULL, 23360), -- 68462
(63375, 'deDE', 'Schaudermagenrohrkolbenbarsch', NULL, NULL, NULL, 23360), -- 63375
(63376, 'deDE', 'Kaltbartotter', NULL, NULL, NULL, 23360), -- 63376
(63470, 'deDE', 'Seemonarch', NULL, NULL, NULL, 23360), -- 63470
(64259, 'deDE', 'Meisterangler Ju Lien', NULL, 'Besatzungsmitglied der Nebelhüpfer', NULL, 23360), -- 64259
(63999, 'deDE', 'Schlächter der Schlickschuppen', NULL, NULL, NULL, 23360), -- 63999
(63944, 'deDE', 'Langflossendrescher', NULL, NULL, NULL, 23360), -- 63944
(69768, 'deDE', 'Kriegsspäher der Zandalari', NULL, NULL, NULL, 23360), -- 69768
(63369, 'deDE', 'Felsschalenschnappklaue', NULL, NULL, NULL, 23360), -- 63369
(63368, 'deDE', 'Felsschalenklacker', NULL, NULL, NULL, 23360), -- 63368
(67100, 'deDE', 'Versteckter Getreuer', NULL, NULL, NULL, 23360), -- 67100
(64680, 'deDE', 'Duskroot Fen Bunny', NULL, NULL, NULL, 23360), -- 64680
(62008, 'deDE', 'Kaltbissmatriarchin', NULL, NULL, NULL, 23360), -- 62008
(66249, 'deDE', 'Wildreiterin Kim', NULL, 'Stallmeisterin', NULL, 23360), -- 66249
(6250, 'deDE', 'Kriecher', NULL, NULL, NULL, 23360), -- 6250
(63277, 'deDE', 'Hund', NULL, 'Besatzung der Nebelhüpfer', NULL, 23360), -- 63277
(63498, 'deDE', 'Min die Brisenreiterin', NULL, 'Flugmeisterin', NULL, 23360), -- 63498
(63016, 'deDE', 'San die Seeberuhigerin', NULL, 'Gastwirtin', NULL, 23360), -- 63016
(67181, 'deDE', 'Chao von den hundert Krabben', NULL, 'Besatzungsmitglied der Nebelhüpfer', NULL, 23360), -- 67181
(67180, 'deDE', 'Deckflicker Lu', NULL, 'Besatzungsmitglied der Nebelhüpfer', NULL, 23360), -- 67180
(63349, 'deDE', 'Deckoffizierin Arie', NULL, 'Besatzungsmitglied der Nebelhüpfer', NULL, 23360), -- 63349
(63280, 'deDE', 'Deckmatrose', NULL, 'Besatzungsmitglied der Nebelhüpfer', NULL, 23360), -- 63280
(63317, 'deDE', 'Kapitän "Tropf" Su-Dao', NULL, 'Besatzung der Nebelhüpfer', NULL, 23360), -- 63317
(63329, 'deDE', 'Kaiserkrabbe', NULL, NULL, NULL, 23360), -- 63329
(63827, 'deDE', 'Junges Blauhautmushan', NULL, NULL, NULL, 23360), -- 63827
(50776, 'deDE', 'Nalash Verdantis', NULL, NULL, NULL, 23360), -- 50776
(63833, 'deDE', 'Mutierender Skorpid', NULL, NULL, NULL, 23360), -- 63833
(63726, 'deDE', 'Mutierender Skorpid', NULL, NULL, NULL, 23360), -- 63726
(63257, 'deDE', 'Genomant der Ik''thik', NULL, NULL, NULL, 23360), -- 63257
(63176, 'deDE', 'Eierdrohne der Ik''thik', NULL, NULL, NULL, 23360), -- 63176
(60025, 'deDE', 'Generic Bunny', NULL, NULL, NULL, 23360), -- 60025
(65118, 'deDE', 'Nadelpfeilstachelschwein', NULL, NULL, NULL, 23360), -- 65118
(63728, 'deDE', 'Bernstecher der Ik''thik', NULL, NULL, NULL, 23360), -- 63728
(63725, 'deDE', 'Knochenstaubmotte', NULL, NULL, NULL, 23360), -- 63725
(63179, 'deDE', 'Schuppenlord der Nebelklingen', NULL, NULL, NULL, 23360), -- 63179
(65357, 'deDE', 'Eine geheimnisvolle Stimme', NULL, NULL, NULL, 23360), -- 65357
(63729, 'deDE', 'Blauhautmushan', NULL, NULL, NULL, 23360), -- 63729
(66880, 'deDE', 'Seeschreiter', NULL, NULL, NULL, 23360), -- 66880
(61970, 'deDE', 'Reißer der Nebelklingen', NULL, NULL, NULL, 23360), -- 61970
(62023, 'deDE', 'Kaltbisskrokilisk', NULL, NULL, NULL, 23360), -- 62023
(59113, 'deDE', 'Generic Bunny', NULL, NULL, NULL, 23360), -- 59113
(64641, 'deDE', 'Sumpfpirscher', NULL, NULL, NULL, 23360), -- 64641
(50836, 'deDE', 'Ik-Ik der Flinke', NULL, NULL, NULL, 23360), -- 50836
(66587, 'deDE', 'Pengsong', NULL, NULL, NULL, 23360), -- 66587
(69806, 'deDE', 'Rachsüchtiger Geist', NULL, NULL, NULL, 23360), -- 69806
(65887, 'deDE', 'Toter Saurok der Schlickschuppen', NULL, NULL, NULL, 23360), -- 65887
(69842, 'deDE', 'Kriegshetzer der Zandalari', NULL, NULL, NULL, 23360), -- 69842
(69769, 'deDE', 'Kriegshetzer der Zandalari', NULL, NULL, NULL, 23360), -- 69769
(65365, 'deDE', 'Kor''ik', NULL, NULL, NULL, 23360), -- 65365
(65486, 'deDE', 'Flügelklinge der Shek''zeer', NULL, NULL, NULL, 23360), -- 65486
(65475, 'deDE', 'Kor''ik', NULL, NULL, NULL, 23360), -- 65475
(62074, 'deDE', 'Gehilfe Ikkess', NULL, 'Hand der Shek''zeer', NULL, 23360), -- 62074
(62090, 'deDE', 'Mantid Bunny', NULL, NULL, NULL, 23360), -- 62090
(62778, 'deDE', 'Mama Sturmbräu', NULL, NULL, NULL, 23360), -- 62778
(62773, 'deDE', 'Iyyokuk der Wache', NULL, NULL, NULL, 23360), -- 62773
(63035, 'deDE', 'Zelot der Zar''thik', NULL, NULL, NULL, 23360), -- 63035
(64357, 'deDE', 'Schwärmer der Kor''thik', NULL, NULL, NULL, 23360), -- 64357
(62543, 'deDE', 'Klingenfürst Ta''yak', NULL, NULL, NULL, 23360), -- 62543
(63031, 'deDE', 'Fanatiker der Set''thik', NULL, NULL, NULL, 23360), -- 63031
(64339, 'deDE', 'Ausbilder Tak''thok', NULL, 'Bewahrer des überwältigenden Angriffs', NULL, 23360), -- 64339
(63030, 'deDE', 'Versklavter Knochenschmetterer', NULL, NULL, NULL, 23360), -- 63030
(66181, 'deDE', 'Bittsteller der Zar''thik', NULL, NULL, NULL, 23360), -- 66181
(64341, 'deDE', 'Ausbilder Zarik', NULL, 'Bewahrer des Sturmstoßes', NULL, 23360), -- 64341
(64338, 'deDE', 'Ausbilder Kli''thak', NULL, 'Bewahrer des Windlaufs', NULL, 23360), -- 64338
(64340, 'deDE', 'Ausbilder Maltik', NULL, 'Bewahrer des ungesehenen Schlages', NULL, 23360), -- 64340
(63563, 'deDE', 'Einatmungspunkt', NULL, NULL, NULL, 23360), -- 63563
(64358, 'deDE', 'Sturmkrieger der Set''thik', NULL, NULL, NULL, 23360), -- 64358
(62980, 'deDE', 'Kaiserlicher Wesir Zor''lok', NULL, 'Stimme der Kaiserin', NULL, 23360), -- 62980
(67177, 'deDE', 'Bittsteller der Zar''thik', NULL, NULL, NULL, 23360), -- 67177
(64353, 'deDE', 'Windschlitzer der Set''thik', NULL, NULL, NULL, 23360), -- 64353
(64902, 'deDE', 'Fleischreißer der Kor''thik', NULL, NULL, NULL, 23360), -- 64902
(64355, 'deDE', 'Flüsterflügel der Kor''thik', NULL, NULL, NULL, 23360), -- 64355
(63853, 'deDE', 'Bittsteller der Zar''thik', NULL, NULL, NULL, 23360), -- 63853
(63032, 'deDE', 'Schildmeister der Sra''thik', NULL, NULL, NULL, 23360), -- 63032
(65500, 'deDE', 'Eliteklingenmeister der Kor''thik', NULL, NULL, NULL, 23360), -- 65500
(65499, 'deDE', 'Bernfallensteller der Sra''thik', NULL, NULL, NULL, 23360), -- 65499
(65498, 'deDE', 'Schlachtheiler der Zar''thik', NULL, NULL, NULL, 23360), -- 65498
(65501, 'deDE', 'Windfürst Mel''jarak', NULL, NULL, NULL, 23360), -- 65501
(63630, 'deDE', 'Storm Unleashed East 3 Target Stalker', NULL, NULL, NULL, 23360), -- 63630
(63629, 'deDE', 'Storm Unleashed East 2 Target Stalker', NULL, NULL, NULL, 23360), -- 63629
(63424, 'deDE', 'Storm Unleashed East Loc Stalker', NULL, NULL, NULL, 23360), -- 63424
(63628, 'deDE', 'Storm Unleashed East 1 Target Stalker', NULL, NULL, NULL, 23360), -- 63628
(63214, 'deDE', 'Storm Unleashed East 3 Stalker', NULL, NULL, NULL, 23360), -- 63214
(63213, 'deDE', 'Storm Unleashed East 2 Stalker', NULL, NULL, NULL, 23360), -- 63213
(63212, 'deDE', 'Storm Unleashed East 1 Stalker', NULL, NULL, NULL, 23360), -- 63212
(63421, 'deDE', 'Storm Unleashed Stalker Start Loc', NULL, NULL, NULL, 23360), -- 63421
(65597, 'deDE', 'Lodernde Kohlenpfanne', NULL, NULL, 'interact', 23360), -- 65597
(63209, 'deDE', 'Storm Unleashed West 3 Stalker', NULL, NULL, NULL, 23360), -- 63209
(63633, 'deDE', 'Storm Unleashed West 3 Target Stalker', NULL, NULL, NULL, 23360), -- 63633
(63208, 'deDE', 'Storm Unleashed West 2 Stalker', NULL, NULL, NULL, 23360), -- 63208
(63423, 'deDE', 'Storm Unleashed West Loc Stalker', NULL, NULL, NULL, 23360), -- 63423
(63632, 'deDE', 'Storm Unleashed West 2 Target Stalker', NULL, NULL, NULL, 23360), -- 63632
(63207, 'deDE', 'Storm Unleashed West 1 Stalker', NULL, NULL, NULL, 23360), -- 63207
(65593, 'deDE', 'Kohlenpfanne', NULL, NULL, 'interact', 23360), -- 65593
(63631, 'deDE', 'Storm Unleashed West 1 Target Stalker', NULL, NULL, NULL, 23360), -- 63631
(63292, 'deDE', 'Gale Winds Stalker', NULL, NULL, NULL, 23360), -- 63292
(62762, 'deDE', 'Amber Pool Stalker', NULL, NULL, NULL, 23360), -- 62762
(64916, 'deDE', 'Schwarmwache der Kor''thik', NULL, NULL, NULL, 23360), -- 64916
(64917, 'deDE', 'Bernrufer der Sra''thik', NULL, NULL, NULL, 23360), -- 64917
(63049, 'deDE', 'Schnellklinge der Set''thik', NULL, NULL, NULL, 23360), -- 63049
(36737, 'deDE', 'Invisible Stalker', NULL, NULL, NULL, 23360), -- 36737
(65524, 'deDE', 'Aufgespießter Klaxxi', NULL, NULL, NULL, 23360), -- 65524
(65519, 'deDE', 'Mantid Spear [DNT]', NULL, NULL, NULL, 23360), -- 65519
(65521, 'deDE', 'Aufgespießter Klaxxi', NULL, NULL, NULL, 23360), -- 65521
(64826, 'deDE', 'Kaz''rik', NULL, 'Die Klaxxi', NULL, 23360), -- 64826
(63048, 'deDE', 'Schlitzer der Kor''thik', NULL, NULL, NULL, 23360), -- 63048
(63036, 'deDE', 'Extremist der Kor''thik', NULL, NULL, NULL, 23360), -- 63036
(62863, 'deDE', 'Qi''tar der Todesrufer', NULL, NULL, NULL, 23360), -- 62863
(66893, 'deDE', 'Manfred', NULL, NULL, NULL, 23360), -- 66893
(64890, 'deDE', 'Vesswache Na''kal', NULL, NULL, NULL, 23360), -- 64890
(66894, 'deDE', 'Manfred', NULL, NULL, NULL, 23360), -- 66894
(64714, 'deDE', 'Nadler der Shek''zeer', NULL, NULL, NULL, 23360), -- 64714
(50739, 'deDE', 'Gar''lok', NULL, NULL, NULL, 23360), -- 50739
(56169, 'deDE', 'Generic Bunny 10.0', NULL, NULL, NULL, 23360), -- 56169
(63586, 'deDE', 'Onyxgiftschwanz', NULL, NULL, NULL, 23360), -- 63586
(62073, 'deDE', 'Gehilfe Ek''vem', NULL, 'Hand der Shek''zeer', NULL, 23360), -- 62073
(63588, 'deDE', 'Rubingiftschwanz', NULL, NULL, NULL, 23360), -- 63588
(63587, 'deDE', 'Schreckensskarabäus', NULL, NULL, NULL, 23360), -- 63587
(62076, 'deDE', 'Gehilfe Tzikzi', NULL, 'Hand der Shek''zeer', NULL, 23360), -- 62076
(62075, 'deDE', 'Gehilfe Suruz', NULL, 'Hand der Shek''zeer', NULL, 23360), -- 62075
(62160, 'deDE', 'Kaltwasserschildkröte', NULL, NULL, NULL, 23360), -- 62160
(62162, 'deDE', 'Horrorschuppenskorpid', NULL, NULL, NULL, 23360), -- 62162
(65004, 'deDE', 'Jonathan Jacobson', NULL, NULL, NULL, 23360), -- 65004
(59829, 'deDE', 'Si Cha Hai-Feif', NULL, 'Shado-Pan', NULL, 23360), -- 59829
(55626, 'deDE', 'General Purpose Bunny (DLA)', NULL, NULL, NULL, 23360), -- 55626
(65886, 'deDE', 'Toter Kyparitpulverisierer', NULL, NULL, NULL, 23360), -- 65886
(63993, 'deDE', 'Reißer der Schlickschuppen', NULL, NULL, NULL, 23360), -- 63993
(64352, 'deDE', 'Rapanaschnecke', NULL, NULL, 'wildpetcapturable', 23360), -- 64352
(65790, 'deDE', 'Schlammwühler', NULL, NULL, NULL, 23360), -- 65790
(64806, 'deDE', 'Rapanaschnecke', NULL, NULL, NULL, 23360), -- 64806
(63981, 'deDE', 'Salzpanzerschnapper', NULL, NULL, NULL, 23360), -- 63981
(65511, 'deDE', 'Infiltrator Ik''thal', NULL, 'Flugmeister', NULL, 23360), -- 65511
(65432, 'deDE', 'Kyparitpulverisierer', NULL, NULL, NULL, 23360), -- 65432
(63206, 'deDE', 'Ernter der Ik''thik', NULL, NULL, NULL, 23360), -- 63206
(66595, 'deDE', 'Ödniskalb', NULL, NULL, NULL, 23360), -- 66595
(65328, 'deDE', 'Discover Amberglow Bunny', NULL, NULL, NULL, 23360), -- 65328
(63871, 'deDE', 'Banditenfrett', NULL, NULL, NULL, 23360), -- 63871
(77120, 'deDE', 'Kriegsfürstin Zaela', NULL, 'Eiserne Horde', NULL, 23360), -- 77120
(80005, 'deDE', 'Himmelsfürstin Tovra', NULL, NULL, NULL, 23360), -- 80005
(65220, 'deDE', 'Zit''tix', NULL, 'Gastwirt', NULL, 23360), -- 65220
(66739, 'deDE', 'Ödniswandler Shu', NULL, 'Großmeistertierzähmer', NULL, 23360), -- 66739
(66724, 'deDE', 'Knacko', NULL, NULL, NULL, 23360), -- 66724
(66723, 'deDE', 'Stampfer', NULL, NULL, NULL, 23360), -- 66723
(66722, 'deDE', 'Verstümmler', NULL, NULL, NULL, 23360), -- 66722
(65186, 'deDE', 'Giftmischer Kil''zit', NULL, 'Alchemielehrer', NULL, 23360), -- 65186
(63501, 'deDE', 'Kik''tik', NULL, 'Flugmeister', NULL, 23360), -- 63501
(62538, 'deDE', 'Kil''ruk der Windschnitter', NULL, NULL, NULL, 23360), -- 62538
(62774, 'deDE', 'Malik der Unversehrte', NULL, NULL, NULL, 23360), -- 62774
(68536, 'deDE', 'Ja''Huk', NULL, NULL, NULL, 23360), -- 68536
(64599, 'deDE', 'Bernschmied Zikk', NULL, 'Rüstmeister der Klaxxi', NULL, 23360), -- 64599
(67064, 'deDE', 'Paragon Chat Controller', NULL, NULL, NULL, 23360), -- 67064
(64815, 'deDE', 'Kor''ik', NULL, NULL, NULL, 23360), -- 64815
(62518, 'deDE', 'Klaxxi''va Set', NULL, NULL, NULL, 23360), -- 62518
(66250, 'deDE', 'Tierführer Kla''vik', NULL, 'Stallmeister', NULL, 23360), -- 66250
(65397, 'deDE', 'Klaxxi''va Krik', NULL, NULL, NULL, 23360), -- 65397
(65753, 'deDE', 'Totally Generic Bunny - GIGANTIC (JSB)', NULL, NULL, NULL, 23360), -- 65753
(65396, 'deDE', 'Klaxxi''va Kor', NULL, NULL, NULL, 23360), -- 65396
(65398, 'deDE', 'Klaxxi''va Sra', NULL, NULL, NULL, 23360), -- 65398
(65395, 'deDE', 'Klaxxi''va Ik', NULL, NULL, NULL, 23360), -- 65395
(62521, 'deDE', 'Klaxxi''va Zan', NULL, NULL, NULL, 23360), -- 62521
(62519, 'deDE', 'Klaxxi''va Vor', NULL, NULL, NULL, 23360), -- 62519
(62517, 'deDE', 'Klaxxi''va Kek', NULL, NULL, NULL, 23360), -- 62517
(65512, 'deDE', 'Warlord Gurthan Controller', NULL, NULL, NULL, 23360), -- 65512
(56740, 'deDE', 'Nebelinkarnation', NULL, NULL, NULL, 23360), -- 56740
(58892, 'deDE', 'Wildes Zottelhorn', NULL, NULL, NULL, 23360), -- 58892
(64246, 'deDE', 'Scheuer Beuteldachs', NULL, NULL, 'wildpetcapturable', 23360), -- 64246
(68559, 'deDE', 'No-No', NULL, NULL, 'wildpet', 23360), -- 68559
(63842, 'deDE', 'Goldenes Zibetkätzchen', NULL, NULL, 'wildpetcapturable', 23360), -- 63842
(58065, 'deDE', 'General Purpose Bunny (DLA)', NULL, NULL, NULL, 23360), -- 58065
(63497, 'deDE', 'Mai von der Mauer', NULL, 'Flugmeisterin', NULL, 23360), -- 63497
(65172, 'deDE', 'Len Rüstegut', NULL, 'Abenteurerbedarf', NULL, 23360), -- 65172
(62112, 'deDE', 'Bogenmeisterin Li', NULL, 'Wachoffizierin', NULL, 23360), -- 62112
(61968, 'deDE', 'Shado-Pan-Bogenschütze', 'Shado-Pan-Bogenschützin', NULL, NULL, 23360), -- 61968
(61982, 'deDE', 'Schreckensspinnermauerkriecher', NULL, NULL, NULL, 23360), -- 61982
(62000, 'deDE', 'Schreckensspinner', NULL, NULL, NULL, 23360), -- 62000
(64336, 'deDE', 'Turm der Untergehenden Sonne', NULL, NULL, 'vehichlecursor', 23360), -- 64336
(65269, 'deDE', 'Orangefarbener Schleim', NULL, NULL, NULL, 23360), -- 65269
(62203, 'deDE', 'Klaxxi''va Tik', NULL, NULL, NULL, 23360), -- 62203
(62143, 'deDE', 'Schreckensspinnerjunges', NULL, NULL, NULL, 23360), -- 62143
(62077, 'deDE', 'Schreckensspinnerei', NULL, NULL, NULL, 23360), -- 62077
(61981, 'deDE', 'Schreckensspinnerhüter', NULL, NULL, NULL, 23360), -- 61981
(75691, 'deDE', 'Hayden Christophen', NULL, 'Schreckliche Gladiatoren', NULL, 23360), -- 75691
(75689, 'deDE', 'Lucan Malory', NULL, 'Bösartige Gladiatoren', NULL, 23360), -- 75689
(65865, 'deDE', 'Tiper Windmann', NULL, 'Flugmeisterin', NULL, 23360), -- 65865
(78456, 'deDE', 'Sternenlicht Sinclair', NULL, 'Stolze Gladiatoren', NULL, 23360), -- 78456
(75694, 'deDE', 'Waffenmeister Holinka', NULL, 'Tyrannische Gladiatoren', NULL, 23360), -- 75694
(75692, 'deDE', 'Ethan Natice', NULL, 'Erbitterte Gladiatoren', NULL, 23360), -- 75692
(65452, 'deDE', 'Rachsüchtiger Gurthanigeist', NULL, NULL, NULL, 23360), -- 65452
(65450, 'deDE', 'Uralter Wächter', NULL, NULL, NULL, 23360), -- 65450
(63755, 'deDE', 'Marschschreiter', NULL, NULL, NULL, 23360), -- 63755
(63731, 'deDE', 'Morastbestie', NULL, NULL, NULL, 23360), -- 63731
(57196, 'deDE', 'Flügelklinge der Ik''thik', NULL, NULL, NULL, 23360), -- 57196
(59832, 'deDE', 'Schlitzflügel der Ik''thik', NULL, NULL, NULL, 23360), -- 59832
(57195, 'deDE', 'Schnellklaue der Ik''thik', NULL, NULL, NULL, 23360), -- 57195
(66187, 'deDE', 'Schreckensmatriarchin', NULL, NULL, NULL, 23360), -- 66187
(65582, 'deDE', 'Kunchong der Ik''thik', NULL, NULL, NULL, 23360), -- 65582
(65646, 'deDE', 'Angstformer der Ik''thik', NULL, NULL, NULL, 23360), -- 65646
(65632, 'deDE', 'Terrorklaue der Ik''thik', NULL, NULL, NULL, 23360), -- 65632
(64622, 'deDE', 'Flüsterer der Ik''thik', NULL, NULL, NULL, 23360), -- 64622
(62582, 'deDE', 'Schwarmgeborener der Shek''zeer', NULL, NULL, NULL, 23360), -- 62582
(64559, 'deDE', 'Gelegehüter der Shek''zeer', NULL, NULL, NULL, 23360), -- 64559
(64831, 'deDE', 'Schwarmgeborener der Ik''thik', NULL, NULL, NULL, 23360), -- 64831
(66465, 'deDE', 'Gelegewächter der Ik''thik', NULL, NULL, NULL, 23360), -- 66465
(67160, 'deDE', 'Vesswache der Klaxxi', NULL, NULL, NULL, 23360), -- 67160
(64849, 'deDE', 'Kunchongjungtier', NULL, NULL, NULL, 23360), -- 64849
(62832, 'deDE', 'Kz''Kzik', NULL, NULL, NULL, 23360), -- 62832
(65575, 'deDE', 'Schwarmgeborener der Vor''thik', NULL, NULL, NULL, 23360), -- 65575
(63548, 'deDE', 'Knuspriger Skorpion', NULL, NULL, 'wildpetcapturable', 23360), -- 63548
(66599, 'deDE', 'Großrückenkalb', NULL, NULL, NULL, 23360), -- 66599
(66605, 'deDE', 'Uralter Hirsch', NULL, NULL, NULL, 23360), -- 66605
(62029, 'deDE', 'Großrückenmushan', NULL, NULL, NULL, 23360), -- 62029
(63740, 'deDE', 'Bernhülle', NULL, NULL, 'interact', 23360), -- 63740
(62386, 'deDE', 'Saftfliege', NULL, NULL, NULL, 23360), -- 62386
(65336, 'deDE', 'Kriegsschlange', NULL, NULL, 'vehichlecursor', 23360), -- 65336
(65335, 'deDE', 'Kriegsschlange', NULL, NULL, NULL, 23360), -- 65335
(57444, 'deDE', 'Schlangenreiter', 'Schlangenreiterin', NULL, NULL, 23360), -- 57444
(63786, 'deDE', 'Eye of the Empress Gas Visual Bunny', NULL, NULL, NULL, 23360), -- 63786
(64687, 'deDE', 'Windhieb', NULL, NULL, NULL, 23360), -- 64687
(64263, 'deDE', 'Shado-Pan-Mauerwächter', NULL, NULL, NULL, 23360),
(64260, 'deDE', 'Shado-Pan-Bogenschütze', 'Shado-Pan-Bogenschützin', NULL, NULL, 23360), -- 64260
(64369, 'deDE', 'Kessel mit heißem Öl', NULL, NULL, 'vehichlecursor', 23360), -- 64369
(61426, 'deDE', 'Shado-Pan-Stachelfalle', NULL, NULL, 'interact', 23360), -- 61426
(63973, 'deDE', 'Schwärmer der Krik''thik', NULL, NULL, NULL, 23360), -- 63973
(63976, 'deDE', 'Nadler der Krik''thik', NULL, NULL, NULL, 23360), -- 63976
(63974, 'deDE', 'Heuschreckenwache der Krik''thik', NULL, NULL, NULL, 23360), -- 63974
(61466, 'deDE', 'Fährtenleger der Krik''thik', NULL, NULL, NULL, 23360), -- 61466
(66391, 'deDE', 'Krik''thik Scentlayer Stalker', NULL, NULL, NULL, 23360), -- 66391
(64275, 'deDE', 'Katapult der Krik''thik', NULL, NULL, NULL, 23360), -- 64275
(65685, 'deDE', 'Räuber der Ik''thik', NULL, NULL, NULL, 23360), -- 65685
(64274, 'deDE', 'Kriegswagen der Krik''thik', NULL, NULL, NULL, 23360), -- 64274
(65584, 'deDE', 'Töter der Ik''thik', NULL, NULL, NULL, 23360), -- 65584
(63972, 'deDE', 'Schwarmling der Krik''thik', NULL, NULL, NULL, 23360), -- 63972
(65573, 'deDE', 'Krieger der Ik''thik', NULL, NULL, NULL, 23360), -- 65573
(65496, 'deDE', 'Grauenhafter Schreckensbringer', NULL, NULL, NULL, 23360), -- 65496
(61702, 'deDE', 'Gao-Ran-Kanonier', 'Gao-Ran-Kanonierin', NULL, NULL, 23360), -- 61702
(61746, 'deDE', 'Drachenschleuder', NULL, NULL, NULL, 23360), -- 61746
(61019, 'deDE', 'Gao-Ran-Magus', NULL, NULL, NULL, 23360), -- 61019
(61083, 'deDE', 'Gao-Ran-Schwarzwache', NULL, NULL, NULL, 23360), -- 61083
(61082, 'deDE', 'Gao-Ran-Schwarzwache', NULL, NULL, NULL, 23360), -- 61082
(61181, 'deDE', 'Armkneifer der Krik''thik', NULL, NULL, NULL, 23360), -- 61181
(61881, 'deDE', 'Initiand Feng', NULL, NULL, NULL, 23360), -- 61881
(61880, 'deDE', 'Initiand Xao', NULL, NULL, NULL, 23360), -- 61880
(61018, 'deDE', 'Gao-Ran-Schütze', 'Gao-Ran-Schützin', NULL, NULL, 23360), -- 61018
(61017, 'deDE', 'Gao-Ran-Schwarzwache', NULL, NULL, NULL, 23360), -- 61017
(61754, 'deDE', 'Armkneifer der Krik''thik', NULL, NULL, NULL, 23360), -- 61754
(66319, 'deDE', 'Angstberührte Motte', NULL, NULL, NULL, 23360), -- 66319
(61692, 'deDE', 'Verletzte Gao-Ran-Schwarzwache', NULL, NULL, NULL, 23360), -- 61692
(61365, 'deDE', 'Heuschreckenwache der Krik''thik', NULL, NULL, NULL, 23360), -- 61365
(66194, 'deDE', 'Ödnisaasfresser', NULL, NULL, NULL, 23360), -- 66194
(66184, 'deDE', 'Schreckenspirscher', NULL, NULL, NULL, 23360), -- 66184
(64626, 'deDE', 'Sog des Schreckens', NULL, NULL, NULL, 23360), -- 64626
(62814, 'deDE', 'Angstformer der Vor''thik', NULL, NULL, NULL, 23360), -- 62814
(62813, 'deDE', 'Furchtverschworener der Vor''thik', NULL, NULL, NULL, 23360), -- 62813
(65429, 'deDE', 'Schallfeld', NULL, NULL, NULL, 23360), -- 65429
(65551, 'deDE', 'Belagerungsschmied der Kor''thik', NULL, NULL, NULL, 23360), -- 65551
(62771, 'deDE', 'Häuptling Rikkitun', NULL, NULL, NULL, 23360), -- 62771
(62772, 'deDE', 'Boggeo', NULL, 'Orakel von Rikkitun', NULL, 23360), -- 62772
(62563, 'deDE', 'Klingenverschriebener der Shek''zeer', NULL, NULL, NULL, 23360), -- 62563
(50791, 'deDE', 'Siltriss der Schärfer', NULL, NULL, NULL, 23360), -- 50791
(62128, 'deDE', 'Flinkflügel der Kor''thik', NULL, NULL, NULL, 23360), -- 62128
(62765, 'deDE', 'Ahgunoss', NULL, NULL, NULL, 23360), -- 62765
(62587, 'deDE', 'Rikkitunsänger', NULL, NULL, NULL, 23360), -- 62587
(62766, 'deDE', 'Mygoness', NULL, NULL, NULL, 23360), -- 62766
(62767, 'deDE', 'Gokk''lok', NULL, NULL, NULL, 23360), -- 62767
(66549, 'deDE', 'Ödnisyak', NULL, NULL, NULL, 23360), -- 66549
(62601, 'deDE', 'Muttersaat', NULL, NULL, 'questinteract', 23360), -- 62601
(63007, 'deDE', 'Kyparit', NULL, NULL, NULL, 23360), -- 63007
(62763, 'deDE', 'Rikkileaschreiter', NULL, NULL, NULL, 23360), -- 62763
(50821, 'deDE', 'Ai-Li Himmelsspiegel', NULL, NULL, NULL, 23360), -- 50821
(65404, 'deDE', 'Schlurfender Nebellauerer', NULL, NULL, NULL, 23360), -- 65404
(62764, 'deDE', 'Rikkileaflatterling', NULL, NULL, 'questinteract', 23360), -- 62764
(62770, 'deDE', 'Dorfbewohner von Rikkitun', NULL, NULL, NULL, 23360), -- 62770
(65995, 'deDE', 'Klingenverschriebener der Shek''zeer', NULL, NULL, NULL, 23360), -- 65995
(64720, 'deDE', 'Manipulator der Shek''zeer', NULL, NULL, NULL, 23360), -- 64720
(66188, 'deDE', 'Bernschuppenbasilisk', NULL, NULL, NULL, 23360), -- 66188
(65187, 'deDE', 'Bernmotte', NULL, NULL, 'wildpetcapturable', 23360), -- 65187
(41200, 'deDE', 'Generic Bunny - PRK', NULL, NULL, NULL, 23360), -- 41200
(64238, 'deDE', 'Unverwüstliche Schabe', NULL, NULL, 'wildpetcapturable', 23360), -- 64238
(64717, 'deDE', 'Schreckenskunchong', NULL, NULL, NULL, 23360), -- 64717
(62833, 'deDE', 'Ilikkax', NULL, NULL, NULL, 23360), -- 62833
(64805, 'deDE', 'Knuspriger Skorpion', NULL, NULL, NULL, 23360), -- 64805
(55370, 'deDE', 'General Purpose Bunny ZTO', NULL, NULL, NULL, 23360), -- 55370
(63420, 'deDE', 'SLG Generic MoP', NULL, NULL, NULL, 23360), -- 63420
(65996, 'deDE', 'Nörgelnder Schreckling', NULL, NULL, NULL, 23360), -- 65996
(64807, 'deDE', 'Unverwüstliche Schabe', NULL, NULL, NULL, 23360), -- 64807
(62761, 'deDE', 'Sha-besessenes Ungeziefer', NULL, NULL, NULL, 23360), -- 62761
(62751, 'deDE', 'Schreckenslauerer', NULL, NULL, NULL, 23360), -- 62751
(64804, 'deDE', 'Stiller Igel', NULL, NULL, 'wildpetcapturable', 23360), -- 64804
(64242, 'deDE', 'Trübigel', NULL, NULL, 'wildpetcapturable', 23360), -- 64242
(62760, 'deDE', 'Verängstigtes Mushan', NULL, NULL, NULL, 23360), -- 62760
(61971, 'deDE', 'Scharfschalenstapfer', NULL, NULL, NULL, 23360), -- 61971
(62758, 'deDE', 'Schlachtsänger der Kor''thik', NULL, NULL, NULL, 23360), -- 62758
(62756, 'deDE', 'Chitinel der Kor''thik', NULL, NULL, NULL, 23360), -- 62756
(61811, 'deDE', 'Aufgeregte Nesselhaut', NULL, NULL, NULL, 23360), -- 61811
(62843, 'deDE', 'Azzix K''tai', NULL, NULL, NULL, 23360), -- 62843
(61302, 'deDE', 'Aufgeregter Saatdieb', NULL, NULL, NULL, 23360), -- 61302
(62755, 'deDE', 'Resonator der Kor''thik', NULL, NULL, NULL, 23360); -- 62755


DELETE FROM `page_text_locale` WHERE (`ID`=4518 and `locale`='deDE');
INSERT INTO `page_text_locale` (`ID`, `locale`, `Text`, `VerifiedBuild`) VALUES
(4518, 'deDE', 'Bern ist der Grundpfeiler der Mantisgesellschaft. Die Mantis verwenden das Material für ihre Bauwerke, ihre Kunst und ihre Technologie.$b$bAls Meister des Schalls fanden die Mantis vor langer Zeit heraus, wie sie mit Bern die Reichweite ihrer akustischen Signale erhöhen können. Auf diese Weise können sie über große Entfernungen kommunizieren. Noch keiner Armee ist es gelungen, das Gebiet der Mantis unentdeckt zu betreten, und auch einzelne Reisende sollten damit rechnen, dass jeder ihrer Schritte beobachtet wird, sobald sie die Mauer hinter sich lassen.$b$bDie Kaiserin wacht zusammen mit ihren Klaxxi-Beratern über die großen Bäume der Tonlongsteppe. Sie werden "Kypari" genannt und sind die einzige Quelle des kostbaren Berns. Legenden berichten, dass die Kypari einst auch östlich der Mauer gediehen, doch die Mogu fällten sie in ihrem endlosen Kampf gegen den Mantisschwarm.', 23360); -- 4518


INSERT IGNORE INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `OptionBroadcastTextID`, `action_poi_id`, `box_coded`, `box_money`, `box_text`, `BoxBroadcastTextID`) VALUES
(14673, 1, 0, 'Bringt mich in den Salzigen Schlick.', 0, 0, 0, 0, '', 0),
(14673, 0, 2, 'Ich muss irgendwohin reisen.', 0, 0, 0, 0, '', 0),
(14663, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0);


-- unknown broadcasttextid
INSERT IGNORE INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `comment`) VALUES
(50739, 0, 0, 'Die Jungen werden sich an Eurem Leichnam gütlich tun!', 12, 0, 100, 0, 0, 0, 0, 'Gar''lok to Player'),
(50776, 0, 0, 'Die Wellen erzittern, wenn Ihr Euch nähert...', 12, 0, 100, 0, 0, 0, 0, 'Nalash Verdantis to Player'),
(50805, 0, 0, 'Nein! Mein Gold!', 12, 0, 100, 0, 0, 0, 0, 'Omnis Feixmaul to Player'),
(65365, 0, 0, 'Ist er weg? Ist es vorbei?', 12, 0, 100, 0, 0, 0, 0, 'Kor''ik to Player'),
(65478, 0, 0, 'Gebt mir den Resonanzkristall, Ihr mickriger kleiner Rindenfresser!', 14, 0, 100, 396, 0, 0, 0, 'Gehilfe Zet''uk to Player'),
(65478, 1, 0, 'Es ist noch nicht vorbei...', 12, 0, 100, 0, 0, 0, 0, 'Gehilfe Zet''uk to Player');


INSERT IGNORE INTO `page_text` (`ID`, `Text`, `NextPageID`, `PlayerConditionID`, `Flags`, `VerifiedBuild`) VALUES
(4518, 'Bern ist der Grundpfeiler der Mantisgesellschaft. Die Mantis verwenden das Material für ihre Bauwerke, ihre Kunst und ihre Technologie.$b$bAls Meister des Schalls fanden die Mantis vor langer Zeit heraus, wie sie mit Bern die Reichweite ihrer akustischen Signale erhöhen können. Auf diese Weise können sie über große Entfernungen kommunizieren. Noch keiner Armee ist es gelungen, das Gebiet der Mantis unentdeckt zu betreten, und auch einzelne Reisende sollten damit rechnen, dass jeder ihrer Schritte beobachtet wird, sobald sie die Mauer hinter sich lassen.$b$bDie Kaiserin wacht zusammen mit ihren Klaxxi-Beratern über die großen Bäume der Tonlongsteppe. Sie werden "Kypari" genannt und sind die einzige Quelle des kostbaren Berns. Legenden berichten, dass die Kypari einst auch östlich der Mauer gediehen, doch die Mogu fällten sie in ihrem endlosen Kampf gegen den Mantisschwarm.', 0, 0, 0, 23360); -- 4518