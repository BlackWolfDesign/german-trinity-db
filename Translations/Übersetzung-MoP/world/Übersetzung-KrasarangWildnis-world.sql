-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_0_23222
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/13/2017 16:19:08


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=30675 AND `locale`='deDE') OR (`ID`=43283 AND `locale`='deDE') OR (`ID`=43291 AND `locale`='deDE') OR (`ID`=43292 AND `locale`='deDE') OR (`ID`=43284 AND `locale`='deDE') OR (`ID`=31260 AND `locale`='deDE') OR (`ID`=30344 AND `locale`='deDE') OR (`ID`=30588 AND `locale`='deDE') OR (`ID`=30701 AND `locale`='deDE') OR (`ID`=30274 AND `locale`='deDE') OR (`ID`=30353 AND `locale`='deDE') OR (`ID`=6134 AND `locale`='deDE') OR (`ID`=30352 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(30675, 'deDE', 'Vergrabener Schatz der Ho-zen', 'Sucht den vergrabenen Schatz der Ho-zen.', 'Die Karte in Euren Händen ist zerknittert, schmutzig und schlecht gezeichnet, aber Ihr könnt ein großes X am Strand sehen.$b$bGibt es irgendwo auf dieser Insel einen vergrabenen Schatz?', '', '', '', '', '', '', 23222),
(43283, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23222),
(43291, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23222),
(43292, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23222),
(43284, 'deDE', 'Invasion: Dun Morogh', '', '', '', '', '', '', '', '', 23222),
(31260, 'deDE', 'Meisterprofiteur: Casheen', 'Sammelt das Blut der alten Tigerin.', 'Ich hab'' da eine geniale Idee für eine neue Gaunerei!$b$bBei meinem letzten Flug sah ich eine unglaublich alte Tigerin umherstreifen.$b$bWisst Ihr, was für einen Preis ihr Blut auf dem Schwarzmarkt erzielen würde? Und wenn ich dann noch ein paar Hexenmeistern erzähle, dass das Blut ihren Beschwörungszaubern zusätzliche Kraft verleiht...$b$bAlso wenn Ihr mir etwas von ihrem Blut bringt, beteilige ich Euch an der Gaunerei. Zuletzt habe ich die Tigerin auf der anderen Seite des Dojani gesehen.', '', 'Königin der Tiefwildnis', 'Chasheen', '', '', '', 23222),
(30344, 'deDE', 'Die verlorene Dynastie', 'Besorgt 6 dynastische Tafeln.', 'Ich frage mich, ob ich Euch mit noch einer Aufgabe belästigen könnte. Meine Tochter, die gute Kommandantin der Schildwachen Lyalia hier, verbietet mir, ins Feld zu ziehen.$b$bJeden Tag, den ich in dieser Oubliette gefangen war, war ich gezwungen, auf die Ruinen im Norden zu starren. Es reizt mich, sie zu erkunden.$b$bWenn wir dort sind, wo ich vermute, dass wir sind, könnten diese Ruinen der Schlüssel sein, den wir benötigen, um die sagenhaften Teiche der Jugend zu finden!$b$BBitte, $C! Geht zu diesen Ruinen und bergt alle Tafeln, die noch Schrift aufweisen.', '', '', '', '', '', '', 23222),
(30588, 'deDE', 'Fischen mit blauem Auge', 'Tötet 8 Strandräuber der Flussklingen bei der Anglerexpedition westlich von der Anlegestelle der Angler.', 'Der Klan der Flussklingen-Saurok greift eine unserer hiesigen Angelexpeditionen an! Sie brauchen sofort Hilfe!$b$bRäumt bei diesen Saurok auf und helft dieser Expedition, sicher zurückzukehren!', '', 'Die Saurok sind grimmige Dschungelkämpfer, mit denen nicht zu spaßen ist.', 'Räuber der Flussklingen', '', '', '', 23222),
(30701, 'deDE', 'Zwingzangensuppe', 'Tötet Zwingzangenfischer am Strand und kehrt mit 16 Augen für eine leckere Suppe zurück.', 'Nichts schmeckt so gut wie Zwingzangensuppe. Das ist eine Krasarangspezialität!$b$bIch wollte mir gerade eine machen, als ich bemerkte, dass eine Hauptzutat fehlt: die Augen!$b$bIhr müsst die Zwingzangenfischer am Strand für mich töten und mir ihre Augen für die Suppe bringen!', '', '', '', '', '', '', 23222),
(30274, 'deDE', 'Die arkane Oubliette', 'Sucht und deaktiviert 3 Fesseln der Oubliette.', 'Ein Mitglied der Allianz? Hier? Elune sei gepriesen.$b$bMein Trupp ist auf ein Hindernis gestoßen. Wir haben mit ein paar alten Texten, die wir auf die Zeit vor Azsharas Herrschaft datiert haben, ein Portal zu diesem Land geöffnet.$b$bUnglücklicherweise waren die Koordinaten auf den Tafeln geschützt, und so lieferte uns das Portal, das uns zu unserem Ziel bringen sollte, in dieser Oubliette ab.$b$bWir sind hier schon seit Tagen gefangen, und das Portal zehrt langsam an unserer Energie, um sich selbst zu versorgen.$b$bKönnt Ihr die Fesseln in der Nähe suchen, die die Oubliette binden, und sie deaktivieren?', '', 'Seine Stimme hallt durch Euren Verstand.', 'Wissenshüter Vaeldrin', '', '', '', 23222),
(30353, 'deDE', 'Meisterlicher Profit', 'Besorgt 12 mit Tigerblut gefüllte Phiolen.', 'Ein Wort: Tigerblut!$b$bDas wird sich besser verkaufen als damals Saronit auf dem Schwarzmarkt. Besonders, nachdem ich ein Gerücht über die tolle stärkende Wirkung und so verbreitet habe.$b$bUnd im Dschungel wimmelt es nur so von Tigern. Könnt Ihr ein paar mit Tigerblut gefüllte Phiolen für mich auftreiben? Ich werde Euch auch an der Gaunerei beteiligen.', '', '', '', '', '', '', 23222),
(6134, 'deDE', 'Geisterplasmasuche', 'Bringt 8 Einheiten Geisterplasma und die Kiste mit den Geistermagneten zu Hornizz Brummsumms in Desolace.', 'Im Südosten liegt ein Tal, das Tal der Knochen, das von den Geistern der Magram heimgesucht wird. Klingt gruselig, nicht wahr? Stellt Euch mal vor, wie sich die Magram erst gruseln müssen! Wenn wir Geisterenergie an jenem Ort einfangen könnten, wäre das den Zentauren sicher einiges wert.$B$BHier, nehmt diese Kiste mit Geistermagneten. Wenn Ihr einen zwischen den beiden großen Skeletten im Tal aufstellt, den toten Goliaths, dann werden sich Geister materialisieren und dorthin gehen. Nehmt Abstand, schaltet die Geister aus und holt Euch ihr Geisterplasma - das verkaufen wir den Magram.', '', '', '', '', '', '', 23222),
(30352, 'deDE', 'Kranich-Meisterschaft', 'Tötet 12 Karpfenjäger.', 'Die Sache bei der Jagd auf Vögel ist die, dass man Munition braucht - und ich habe keine. Die Sache bei der Jagd mit Nesingwary ist, dass man beweisen muss, dass man in Abschüssen und Prahlerei nicht nachlässt.$b$bKeine Jagd, keine Prahlerei. Und ich mag Prahlerei, also ist dies eine schlechte Situation.$b$bKann ich Euch anheuern, ein paar Karpfenjäger für mich zu töten? Ihr findet sie rauf und runter am Fluss westlich von hier. Es sind ziemlich viele, also jagt nebenher ruhig ein wenig.', '', '', '', '', '', '', 23222);


DELETE FROM `quest_objectives_locale` WHERE (`ID`=264176 AND `locale`='deDE') OR (`ID`=264175 AND `locale`='deDE') OR (`ID`=264174 AND `locale`='deDE');
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(264176, 'deDE', 30274, 2, 'Südwestliche Fessel der Oubliette zerstört', 23222),
(264175, 'deDE', 30274, 1, 'Nordwestliche Fessel der Oubliette zerstört', 23222),
(264174, 'deDE', 30274, 0, 'Nordöstliche Fessel der Oubliette zerstört', 23222);

DELETE FROM `gameobject_template_locale` WHERE (`entry`=216323 AND `locale`='deDE') OR (`entry`=215387 AND `locale`='deDE') OR (`entry`=215870 AND `locale`='deDE') OR (`entry`=216365 AND `locale`='deDE') OR (`entry`=216363 AND `locale`='deDE') OR (`entry`=216274 AND `locale`='deDE') OR (`entry`=216460 AND `locale`='deDE') OR (`entry`=216459 AND `locale`='deDE') OR (`entry`=210416 AND `locale`='deDE') OR (`entry`=210418 AND `locale`='deDE') OR (`entry`=216287 AND `locale`='deDE') OR (`entry`=216275 AND `locale`='deDE') OR (`entry`=215726 AND `locale`='deDE') OR (`entry`=215701 AND `locale`='deDE') OR (`entry`=216300 AND `locale`='deDE') OR (`entry`=216162 AND `locale`='deDE') OR (`entry`=214446 AND `locale`='deDE') OR (`entry`=215072 AND `locale`='deDE') OR (`entry`=215864 AND `locale`='deDE') OR (`entry`=215863 AND `locale`='deDE') OR (`entry`=215782 AND `locale`='deDE') OR (`entry`=215073 AND `locale`='deDE') OR (`entry`=210417 AND `locale`='deDE') OR (`entry`=214678 AND `locale`='deDE') OR (`entry`=211452 AND `locale`='deDE') OR (`entry`=212154 AND `locale`='deDE') OR (`entry`=214976 AND `locale`='deDE') OR (`entry`=211451 AND `locale`='deDE') OR (`entry`=211453 AND `locale`='deDE') OR (`entry`=213324 AND `locale`='deDE') OR (`entry`=213323 AND `locale`='deDE') OR (`entry`=213322 AND `locale`='deDE') OR (`entry`=213320 AND `locale`='deDE') OR (`entry`=213321 AND `locale`='deDE') OR (`entry`=214418 AND `locale`='deDE') OR (`entry`=211993 AND `locale`='deDE') OR (`entry`=211420 AND `locale`='deDE') OR (`entry`=211382 AND `locale`='deDE') OR (`entry`=214415 AND `locale`='deDE') OR (`entry`=211379 AND `locale`='deDE') OR (`entry`=211378 AND `locale`='deDE') OR (`entry`=211377 AND `locale`='deDE') OR (`entry`=211376 AND `locale`='deDE') OR (`entry`=213865 AND `locale`='deDE') OR (`entry`=214403 AND `locale`='deDE') OR (`entry`=216161 AND `locale`='deDE') OR (`entry`=216304 AND `locale`='deDE') OR (`entry`=215831 AND `locale`='deDE') OR (`entry`=213422 AND `locale`='deDE') OR (`entry`=216307 AND `locale`='deDE') OR (`entry`=214966 AND `locale`='deDE') OR (`entry`=209919 AND `locale`='deDE') OR (`entry`=209918 AND `locale`='deDE') OR (`entry`=216306 AND `locale`='deDE') OR (`entry`=214967 AND `locale`='deDE') OR (`entry`=214989 AND `locale`='deDE') OR (`entry`=213653 AND `locale`='deDE') OR (`entry`=214975 AND `locale`='deDE') OR (`entry`=213864 AND `locale`='deDE') OR (`entry`=215170 AND `locale`='deDE') OR (`entry`=212931 AND `locale`='deDE') OR (`entry`=213293 AND `locale`='deDE') OR (`entry`=213292 AND `locale`='deDE') OR (`entry`=213291 AND `locale`='deDE') OR (`entry`=216163 AND `locale`='deDE') OR (`entry`=212935 AND `locale`='deDE') OR (`entry`=213299 AND `locale`='deDE') OR (`entry`=213332 AND `locale`='deDE') OR (`entry`=212932 AND `locale`='deDE') OR (`entry`=210209 AND `locale`='deDE') OR (`entry`=215069 AND `locale`='deDE') OR (`entry`=215068 AND `locale`='deDE') OR (`entry`=214440 AND `locale`='deDE') OR (`entry`=241593 AND `locale`='deDE') OR (`entry`=212159 AND `locale`='deDE') OR (`entry`=211423 AND `locale`='deDE') OR (`entry`=211160 AND `locale`='deDE') OR (`entry`=214165 AND `locale`='deDE') OR (`entry`=212658 AND `locale`='deDE') OR (`entry`=212657 AND `locale`='deDE') OR (`entry`=212656 AND `locale`='deDE') OR (`entry`=211596 AND `locale`='deDE') OR (`entry`=213466 AND `locale`='deDE') OR (`entry`=187376 AND `locale`='deDE') OR (`entry`=213863 AND `locale`='deDE') OR (`entry`=211474 AND `locale`='deDE') OR (`entry`=211118 AND `locale`='deDE') OR (`entry`=213223 AND `locale`='deDE') OR (`entry`=210610 AND `locale`='deDE') OR (`entry`=210758 AND `locale`='deDE') OR (`entry`=210757 AND `locale`='deDE') OR (`entry`=210511 AND `locale`='deDE') OR (`entry`=215765 AND `locale`='deDE') OR (`entry`=223184 AND `locale`='deDE') OR (`entry`=207648 AND `locale`='deDE') OR (`entry`=216753 AND `locale`='deDE') OR (`entry`=216681 AND `locale`='deDE') OR (`entry`=216680 AND `locale`='deDE') OR (`entry`=194278 AND `locale`='deDE') OR (`entry`=216229 AND `locale`='deDE') OR (`entry`=216838 AND `locale`='deDE') OR (`entry`=210535 AND `locale`='deDE') OR (`entry`=210516 AND `locale`='deDE') OR (`entry`=210533 AND `locale`='deDE') OR (`entry`=210527 AND `locale`='deDE') OR (`entry`=215071 AND `locale`='deDE') OR (`entry`=215070 AND `locale`='deDE') OR (`entry`=210618 AND `locale`='deDE') OR (`entry`=210620 AND `locale`='deDE') OR (`entry`=210619 AND `locale`='deDE') OR (`entry`=210191 AND `locale`='deDE') OR (`entry`=210213 AND `locale`='deDE') OR (`entry`=210214 AND `locale`='deDE') OR (`entry`=211195 AND `locale`='deDE') OR (`entry`=204272 AND `locale`='deDE') OR (`entry`=216476 AND `locale`='deDE') OR (`entry`=216671 AND `locale`='deDE') OR (`entry`=216670 AND `locale`='deDE') OR (`entry`=216675 AND `locale`='deDE') OR (`entry`=216609 AND `locale`='deDE') OR (`entry`=216281 AND `locale`='deDE') OR (`entry`=216781 AND `locale`='deDE') OR (`entry`=213407 AND `locale`='deDE') OR (`entry`=210185 AND `locale`='deDE') OR (`entry`=210184 AND `locale`='deDE') OR (`entry`=210186 AND `locale`='deDE') OR (`entry`=210858 AND `locale`='deDE') OR (`entry`=210857 AND `locale`='deDE') OR (`entry`=210348 AND `locale`='deDE') OR (`entry`=210040 AND `locale`='deDE') OR (`entry`=210052 AND `locale`='deDE') OR (`entry`=210039 AND `locale`='deDE') OR (`entry`=210893 AND `locale`='deDE') OR (`entry`=211428 AND `locale`='deDE') OR (`entry`=213330 AND `locale`='deDE') OR (`entry`=211640 AND `locale`='deDE') OR (`entry`=213078 AND `locale`='deDE') OR (`entry`=209938 AND `locale`='deDE') OR (`entry`=210037 AND `locale`='deDE') OR (`entry`=210002 AND `locale`='deDE') OR (`entry`=210001 AND `locale`='deDE') OR (`entry`=211982 AND `locale`='deDE') OR (`entry`=211981 AND `locale`='deDE') OR (`entry`=211977 AND `locale`='deDE') OR (`entry`=210519 AND `locale`='deDE') OR (`entry`=210188 AND `locale`='deDE') OR (`entry`=211976 AND `locale`='deDE') OR (`entry`=210228 AND `locale`='deDE') OR (`entry`=212694 AND `locale`='deDE') OR (`entry`=213750 AND `locale`='deDE') OR (`entry`=212696 AND `locale`='deDE') OR (`entry`=214164 AND `locale`='deDE') OR (`entry`=212655 AND `locale`='deDE') OR (`entry`=209921 AND `locale`='deDE') OR (`entry`=214340 AND `locale`='deDE') OR (`entry`=209899 AND `locale`='deDE') OR (`entry`=209898 AND `locale`='deDE') OR (`entry`=209892 AND `locale`='deDE');
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(216323, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(215387, 'deDE', 'Kiste der Horde', '', NULL, 23222),
(215870, 'deDE', 'Die Froschinsel', '', NULL, 23222),
(216365, 'deDE', 'Kleiner Bombenhaufen', '', NULL, 23222),
(216363, 'deDE', 'Human Arrow 01', '', NULL, 23222),
(216274, 'deDE', 'Signalfeuer', '', NULL, 23222),
(216460, 'deDE', 'Vertrag des Bilgewasserkartells', '', NULL, 23222),
(216459, 'deDE', 'Kampflog von Kriegsfürst Blutheft', '', NULL, 23222),
(210416, 'deDE', 'Moguartefakt', '', NULL, 23222),
(210418, 'deDE', 'Moguartefakt', '', NULL, 23222),
(216287, 'deDE', 'Goblinmörser', '', NULL, 23222),
(216275, 'deDE', 'Signalfeuer', '', NULL, 23222),
(215726, 'deDE', 'Tang', '', NULL, 23222),
(215701, 'deDE', 'Seetang', '', NULL, 23222),
(216300, 'deDE', 'Goblinmessgerät', '', NULL, 23222),
(216162, 'deDE', 'Erdhaufen', '', NULL, 23222),
(214446, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(215072, 'deDE', 'Amboss', '', NULL, 23222),
(215864, 'deDE', 'Muschelschale der Jinyu', '', NULL, 23222),
(215863, 'deDE', 'Gemälde der Jinyu', '', NULL, 23222),
(215782, 'deDE', 'Ursprünge', '', NULL, 23222),
(215073, 'deDE', 'Schmiede', '', NULL, 23222),
(210417, 'deDE', 'Moguartefakt', '', NULL, 23222),
(214678, 'deDE', 'Kiste', '', NULL, 23222),
(211452, 'deDE', 'Bronzene Gabe des Kranichs', '', NULL, 23222),
(212154, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214976, 'deDE', 'Briefkasten', '', NULL, 23222),
(211451, 'deDE', 'Himmelblaue Gabe des Kranichs', '', NULL, 23222),
(211453, 'deDE', 'Purpurrote Gabe des Kranichs', '', NULL, 23222),
(213324, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(213323, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(213322, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(213320, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(213321, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214418, 'deDE', 'Hai-pu', '', NULL, 23222),
(211993, 'deDE', 'Ho-zen-Reife', '', NULL, 23222),
(211420, 'deDE', 'Sandklumpen', '', NULL, 23222),
(211382, 'deDE', 'Zäher Seetang', '', NULL, 23222),
(214415, 'deDE', 'Die Saurok und die Jinyu', '', NULL, 23222),
(211379, 'deDE', 'Holzbrett', '', NULL, 23222),
(211378, 'deDE', 'Holzbrett', '', NULL, 23222),
(211377, 'deDE', 'Holzbrett', '', NULL, 23222),
(211376, 'deDE', 'Holzbrett', '', NULL, 23222),
(213865, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214403, 'deDE', 'Papierstapel', '', NULL, 23222),
(216161, 'deDE', 'Erdhaufen', '', NULL, 23222),
(216304, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(215831, 'deDE', 'Shaohaos Kranichgong', '', NULL, 23222),
(213422, 'deDE', 'Des Kaisers Bürde - Teil 4', '', NULL, 23222),
(216307, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214966, 'deDE', 'Wassereimer', '', NULL, 23222),
(209919, 'deDE', 'Schmiede', '', NULL, 23222),
(209918, 'deDE', 'Amboss', '', NULL, 23222),
(216306, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214967, 'deDE', 'Backfisch', '', NULL, 23222),
(214989, 'deDE', 'Pandarischer Pichnickkorb', '', NULL, 23222),
(213653, 'deDE', 'Pandarischer Fischspeer', '', NULL, 23222),
(214975, 'deDE', 'Bauholz', '', NULL, 23222),
(213864, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(215170, 'deDE', 'Großer Schatz', '', NULL, 23222),
(212931, 'deDE', 'Dekorative Pandarenrollkiste', '', NULL, 23222),
(213293, 'deDE', 'Moguartefakt', '', NULL, 23222),
(213292, 'deDE', 'Moguartefakt', '', NULL, 23222),
(213291, 'deDE', 'Moguartefakt', '', NULL, 23222),
(216163, 'deDE', 'Erdhaufen', '', NULL, 23222),
(212935, 'deDE', 'Moguartefakt', '', NULL, 23222),
(213299, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(213332, 'deDE', 'Die verlorene Dynastie', '', NULL, 23222),
(212932, 'deDE', 'Bruchstück einer Mogustatue', '', NULL, 23222),
(210209, 'deDE', 'Kaiserlicher Lotus', '', NULL, 23222),
(215069, 'deDE', 'Amboss', '', NULL, 23222),
(215068, 'deDE', 'Schmiede', '', NULL, 23222),
(214440, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(241593, 'deDE', 'Schwimmer', '', NULL, 23222),
(212159, 'deDE', 'Briefkasten', '', NULL, 23222),
(211423, 'deDE', 'Schiffswracktrümmer', '', NULL, 23222),
(211160, 'deDE', 'Makelloses Kranichei', '', NULL, 23222),
(214165, 'deDE', 'Ofen', '', NULL, 23222),
(212658, 'deDE', 'Stuhl', '', NULL, 23222),
(212657, 'deDE', 'Stuhl', '', NULL, 23222),
(212656, 'deDE', 'Stuhl', '', NULL, 23222),
(211596, 'deDE', 'Goblinisches Angelfloß', '', NULL, 23222),
(213466, 'deDE', 'Briefkasten', '', NULL, 23222),
(187376, 'deDE', 'NSC-Fischköder', '', NULL, 23222),
(213863, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211474, 'deDE', 'Sonnenkriecher', '', NULL, 23222),
(211118, 'deDE', 'Gezacktes Seeohr', '', NULL, 23222),
(213223, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210610, 'deDE', 'Vaeldrins Tagebuch', '', NULL, 23222),
(210758, 'deDE', 'Die Kerker von Dojan', '', NULL, 23222),
(210757, 'deDE', 'Vaeldrins Forschungsarbeit', '', NULL, 23222),
(210511, 'deDE', 'Fallenversehenes Portal nach Darnassus', '', NULL, 23222),
(215765, 'deDE', 'Warten auf die Schildkröte', '', NULL, 23222),
(223184, 'deDE', 'Wanderfestankündigung', '', NULL, 23222),
(207648, 'deDE', '[DND] Große Karte', '', NULL, 23222),
(216753, 'deDE', 'Wache der Natur', '', NULL, 23222),
(216681, 'deDE', 'Kiste von Sturmwind', '', NULL, 23222),
(216680, 'deDE', 'Nachtelfenkiste', '', NULL, 23222),
(194278, 'deDE', 'Banner von Orgrimmar', '', NULL, 23222),
(216229, 'deDE', 'Fluchtartig aufgegebenes Bauholz', '', NULL, 23222),
(216838, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210535, 'deDE', 'Südwestliche Fessel der Oubliette', '', NULL, 23222),
(210516, 'deDE', 'Arkane Oubliette', '', NULL, 23222),
(210533, 'deDE', 'Nordwestliche Fessel der Oubliette', '', NULL, 23222),
(210527, 'deDE', 'Nordöstliche Fessel der Oubliette', '', NULL, 23222),
(215071, 'deDE', 'Schmiede', '', NULL, 23222),
(215070, 'deDE', 'Amboss', '', NULL, 23222),
(210618, 'deDE', 'Uralte dynastische Tafel', '', NULL, 23222),
(210620, 'deDE', 'Uralte dynastische Tafel', '', NULL, 23222),
(210619, 'deDE', 'Uralte dynastische Tafel', '', NULL, 23222),
(210191, 'deDE', 'Gedenkflamme von Zhu', '', NULL, 23222),
(210213, 'deDE', 'Gedenkflamme von Rin', '', NULL, 23222),
(210214, 'deDE', 'Gedenkflamme von Po', '', NULL, 23222),
(211195, 'deDE', 'Relikt der Mogu', '', NULL, 23222),
(204272, 'deDE', 'Vermessungsgerät (grün)', '', NULL, 23222),
(216476, 'deDE', 'Tagebuch von Hochmarschall Doppelzopf', '', NULL, 23222),
(216671, 'deDE', 'Wurzelbarrikade 2', '', NULL, 23222),
(216670, 'deDE', 'Wurzelbarrikade', '', NULL, 23222),
(216675, 'deDE', 'Nachtelfenlandungsboot', '', NULL, 23222),
(216609, 'deDE', 'Abschussgerät für Leuchtfeuer', '', NULL, 23222),
(216281, 'deDE', 'Landungsboot', '', NULL, 23222),
(216781, 'deDE', 'Wagen der Allianz 01', '', NULL, 23222),
(213407, 'deDE', 'Quan Tou Kuo der Zweifäustige', '', NULL, 23222),
(210185, 'deDE', 'Glitschiger Matschflosser', '', NULL, 23222),
(210184, 'deDE', 'Glitschiger Matschflosser', '', NULL, 23222),
(210186, 'deDE', 'Glitschiger Matschflosser', '', NULL, 23222),
(210858, 'deDE', 'Trockenes Feuerholz', '', NULL, 23222),
(210857, 'deDE', 'Trockenes Feuerholz', '', NULL, 23222),
(210348, 'deDE', 'Trockenes Feuerholz', '', NULL, 23222),
(210040, 'deDE', 'Kletterseil', '', NULL, 23222),
(210052, 'deDE', 'Helles Getreide der Klippenwächter', '', NULL, 23222),
(210039, 'deDE', 'Abseilstrick', '', NULL, 23222),
(210893, 'deDE', '*WARNUNG* Im Aufbau. Nutzt das Seil, falls Ihr es wagt.', '', NULL, 23222),
(211428, 'deDE', 'Stachelfalle der Flussklingen', '', NULL, 23222),
(213330, 'deDE', 'Das letzte Gefecht', '', NULL, 23222),
(211640, 'deDE', 'Instance Portal (Party + Heroic + Challenge)', '', NULL, 23222),
(213078, 'deDE', 'Oberkannte der Großen Mauer', '', NULL, 23222),
(209938, 'deDE', 'Tür der Pandabrauerei', '', NULL, 23222),
(210037, 'deDE', 'Vermälzte Höhlengerste', '', NULL, 23222),
(210002, 'deDE', 'Gestohlener Gerstesack', '', NULL, 23222),
(210001, 'deDE', 'Gestohlener Malzsack', '', NULL, 23222),
(211982, 'deDE', 'Doodad_PA_GhostBrewery_Door_014', '', NULL, 23222),
(211981, 'deDE', 'Doodad_PA_GhostBrewery_Door_015', '', NULL, 23222),
(211977, 'deDE', 'Doodad_PA_GhostBrewery_Door_013', '', NULL, 23222),
(210519, 'deDE', 'Kranichnest', '', NULL, 23222),
(210188, 'deDE', 'Regennasse Honigwaben', '', NULL, 23222),
(211976, 'deDE', 'Doodad_PA_GhostBrewery_Door_012', '', NULL, 23222),
(210228, 'deDE', 'Pigmentkrug', '', NULL, 23222),
(212694, 'deDE', 'PA Mortar and Pestle', '', NULL, 23222),
(213750, 'deDE', 'Steintafel der Saurok', '', NULL, 23222),
(212696, 'deDE', 'PA Debris Pile 01', '', NULL, 23222),
(214164, 'deDE', 'Kochtopf', '', NULL, 23222),
(212655, 'deDE', 'Stuhl', '', NULL, 23222),
(209921, 'deDE', 'Schlammiges Wasser', '', NULL, 23222),
(214340, 'deDE', 'Bootsbauanweisungen', '', NULL, 23222),
(209899, 'deDE', 'Gestohlene Wassermelone', '', NULL, 23222),
(209898, 'deDE', 'Wassermelonenbootsflagge', '', NULL, 23222),
(209892, 'deDE', 'Gestohlene Wassermelone', '', NULL, 23222);


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=10181 AND `id`=0) OR (`menu_id`=14039 AND `id`=1) OR (`menu_id`=14039 AND `id`=0) OR (`menu_id`=8903 AND `id`=0) OR (`menu_id`=13384 AND `id`=0) OR (`menu_id`=13499 AND `id`=0) OR (`menu_id`=13446 AND `id`=0) OR (`menu_id`=14333 AND `id`=0) OR (`menu_id`=14310 AND `id`=1) OR (`menu_id`=14310 AND `id`=0) OR (`menu_id`=13419 AND `id`=0) OR (`menu_id`=14050 AND `id`=1) OR (`menu_id`=14050 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(10181, 0, '', '', 'Ich sehe mich nur mal um.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14039, 1, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14039, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8903, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13384, 0, '', '', 'Wie seid Ihr hierher gekommen?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13499, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13446, 0, '', '', 'Was macht Ihr hier?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14333, 0, '', '', 'Was macht Ihr hier, so weit weg von zu Hause?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14310, 1, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14310, 0, '', '', 'Ich sehe mich nur mal um.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13419, 0, '', '', 'Ich habe eine andere Frage.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14050, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14050, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', '');

DELETE FROM `locales_creature_text` WHERE (`entry`=50331 AND `groupid`=0) OR (`entry`=50830 AND `groupid`=0) OR (`entry`=56114 AND `groupid`=0) OR (`entry`=56114 AND `groupid`=1) OR (`entry`=57457 AND `groupid`=0) OR (`entry`=58735 AND `groupid`=0) OR (`entry`=58745 AND `groupid`=0) OR (`entry`=60358 AND `groupid`=0) OR (`entry`=60602 AND `groupid`=0) OR (`entry`=60602 AND `groupid`=1) OR (`entry`=62352 AND `groupid`=0) OR (`entry`=63721 AND `groupid`=0) OR (`entry`=65638 AND `groupid`=0) OR (`entry`=65626 AND `groupid`=0) OR (`entry`=65746 AND `groupid`=0) OR (`entry`=65746 AND `groupid`=1);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(50331, 0, 0, '', '', 'Spürt die Stärke der Yaungol!', '', '', '', '', ''),
(50830, 0, 0, '', '', 'Her mit den Leckerli!', '', '', '', '', ''),
(56114, 0, 0, '', '', 'Vorsicht. Wenn Ihr darin stecken bleibt, wird es Euch die Lebenskraft aussaugen, um sich selbst mit Energie zu versorgen.', '', '', '', '', ''),
(56114, 1, 0, '', '', 'Ja, ich hab'' auch schon versucht, durch das Ding zu springen.', '', '', '', '', ''),
(57457, 0, 0, '', '', '...', '', '', '', '', ''),
(58735, 0, 0, '', '', 'Vater, wir müssen uns zuerst um die Späher der Horde kümmern.', '', '', '', '', ''),
(58745, 0, 0, '', '', 'Achtet auf die magischen Fesseln an den Grenzbereichen der Oubliette. Es sollte drei davon geben.', '', '', '', '', ''),
(60358, 0, 0, '', '', 'Djuuk...', '', '', '', '', ''),
(60602, 0, 0, '', '', 'Erhebt Eure Waffen, $R! Lasst uns kämpfen!', '', '', '', '', ''),
(60602, 1, 0, '', '', 'Los geht''s!', '', '', '', '', ''),
(62352, 0, 0, '', '', 'Plündern und brandschatzen, bwahahaha!', '', '', '', '', ''),
(63721, 0, 0, '', '', 'Ah, wir teilen dieselbe Leidenschaft! Ich habe viele Ausrüstungsgegenstände von hiesigen Fischern. Sagt einfach Bescheid, wenn Ihr etwas benötigt.', '', '', '', '', ''),
(65626, 0, 0, '', '', 'Für den Donnerkönig!', '', '', '', '', ''),
(65638, 0, 0, '', '', 'Der Meister kann nicht länger auf diese Artefakte warten.', '', '', '', '', ''),
(65746, 0, 0, '', '', 'Vorsicht mit dem Teil, Jun-Jun! Ein starker Windstoß und Ihr werdet ins Meer geblasen!', '', '', '', '', ''),
(65746, 1, 0, '', '', 'Liebe Güte, Duyi. Was habt Ihr diesem Ding zu fressen gegeben?', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=66069 /*66069*/ AND `locale`='deDE') OR (`entry`=67927 /*67927*/ AND `locale`='deDE') OR (`entry`=67812 /*67812*/ AND `locale`='deDE') OR (`entry`=67903 /*67903*/ AND `locale`='deDE') OR (`entry`=67926 /*67926*/ AND `locale`='deDE') OR (`entry`=67924 /*67924*/ AND `locale`='deDE') OR (`entry`=67904 /*67904*/ AND `locale`='deDE') OR (`entry`=67884 /*67884*/ AND `locale`='deDE') OR (`entry`=67902 /*67902*/ AND `locale`='deDE') OR (`entry`=67901 /*67901*/ AND `locale`='deDE') OR (`entry`=67825 /*67825*/ AND `locale`='deDE') OR (`entry`=67900 /*67900*/ AND `locale`='deDE') OR (`entry`=66752 /*66752*/ AND `locale`='deDE') OR (`entry`=58954 /*58954*/ AND `locale`='deDE') OR (`entry`=65638 /*65638*/ AND `locale`='deDE') OR (`entry`=58645 /*58645*/ AND `locale`='deDE') OR (`entry`=66070 /*66070*/ AND `locale`='deDE') OR (`entry`=66071 /*66071*/ AND `locale`='deDE') OR (`entry`=66745 /*66745*/ AND `locale`='deDE') OR (`entry`=65054 /*65054*/ AND `locale`='deDE') OR (`entry`=58882 /*58882*/ AND `locale`='deDE') OR (`entry`=66748 /*66748*/ AND `locale`='deDE') OR (`entry`=64263 /*64263*/ AND `locale`='deDE') OR (`entry`=58415 /*58415*/ AND `locale`='deDE') OR (`entry`=58368 /*58368*/ AND `locale`='deDE') OR (`entry`=58955 /*58955*/ AND `locale`='deDE') OR (`entry`=59783 /*59783*/ AND `locale`='deDE') OR (`entry`=58367 /*58367*/ AND `locale`='deDE') OR (`entry`=59784 /*59784*/ AND `locale`='deDE') OR (`entry`=59295 /*59295*/ AND `locale`='deDE') OR (`entry`=58476 /*58476*/ AND `locale`='deDE') OR (`entry`=58830 /*58830*/ AND `locale`='deDE') OR (`entry`=59079 /*59079*/ AND `locale`='deDE') OR (`entry`=59048 /*59048*/ AND `locale`='deDE') OR (`entry`=58277 /*58277*/ AND `locale`='deDE') OR (`entry`=58278 /*58278*/ AND `locale`='deDE') OR (`entry`=58276 /*58276*/ AND `locale`='deDE') OR (`entry`=58642 /*58642*/ AND `locale`='deDE') OR (`entry`=58634 /*58634*/ AND `locale`='deDE') OR (`entry`=58641 /*58641*/ AND `locale`='deDE') OR (`entry`=58639 /*58639*/ AND `locale`='deDE') OR (`entry`=58608 /*58608*/ AND `locale`='deDE') OR (`entry`=58614 /*58614*/ AND `locale`='deDE') OR (`entry`=60602 /*60602*/ AND `locale`='deDE') OR (`entry`=66729 /*66729*/ AND `locale`='deDE') OR (`entry`=65189 /*65189*/ AND `locale`='deDE') OR (`entry`=60506 /*60506*/ AND `locale`='deDE') OR (`entry`=60528 /*60528*/ AND `locale`='deDE') OR (`entry`=60529 /*60529*/ AND `locale`='deDE') OR (`entry`=60601 /*60601*/ AND `locale`='deDE') OR (`entry`=60603 /*60603*/ AND `locale`='deDE') OR (`entry`=66755 /*66755*/ AND `locale`='deDE') OR (`entry`=66934 /*66934*/ AND `locale`='deDE') OR (`entry`=60358 /*60358*/ AND `locale`='deDE') OR (`entry`=60357 /*60357*/ AND `locale`='deDE') OR (`entry`=60367 /*60367*/ AND `locale`='deDE') OR (`entry`=60355 /*60355*/ AND `locale`='deDE') OR (`entry`=60303 /*60303*/ AND `locale`='deDE') OR (`entry`=60299 /*60299*/ AND `locale`='deDE') OR (`entry`=66753 /*66753*/ AND `locale`='deDE') OR (`entry`=60289 /*60289*/ AND `locale`='deDE') OR (`entry`=65384 /*65384*/ AND `locale`='deDE') OR (`entry`=65383 /*65383*/ AND `locale`='deDE') OR (`entry`=65382 /*65382*/ AND `locale`='deDE') OR (`entry`=67184 /*67184*/ AND `locale`='deDE') OR (`entry`=62872 /*62872*/ AND `locale`='deDE') OR (`entry`=60441 /*60441*/ AND `locale`='deDE') OR (`entry`=60290 /*60290*/ AND `locale`='deDE') OR (`entry`=60293 /*60293*/ AND `locale`='deDE') OR (`entry`=60173 /*60173*/ AND `locale`='deDE') OR (`entry`=65289 /*65289*/ AND `locale`='deDE') OR (`entry`=60182 /*60182*/ AND `locale`='deDE') OR (`entry`=60422 /*60422*/ AND `locale`='deDE') OR (`entry`=60282 /*60282*/ AND `locale`='deDE') OR (`entry`=60139 /*60139*/ AND `locale`='deDE') OR (`entry`=60279 /*60279*/ AND `locale`='deDE') OR (`entry`=60201 /*60201*/ AND `locale`='deDE') OR (`entry`=60196 /*60196*/ AND `locale`='deDE') OR (`entry`=60200 /*60200*/ AND `locale`='deDE') OR (`entry`=60198 /*60198*/ AND `locale`='deDE') OR (`entry`=60202 /*60202*/ AND `locale`='deDE') OR (`entry`=69815 /*69815*/ AND `locale`='deDE') OR (`entry`=60487 /*60487*/ AND `locale`='deDE') OR (`entry`=60616 /*60616*/ AND `locale`='deDE') OR (`entry`=67182 /*67182*/ AND `locale`='deDE') OR (`entry`=62967 /*62967*/ AND `locale`='deDE') OR (`entry`=58610 /*58610*/ AND `locale`='deDE') OR (`entry`=58256 /*58256*/ AND `locale`='deDE') OR (`entry`=59047 /*59047*/ AND `locale`='deDE') OR (`entry`=58255 /*58255*/ AND `locale`='deDE') OR (`entry`=58257 /*58257*/ AND `locale`='deDE') OR (`entry`=65852 /*65852*/ AND `locale`='deDE') OR (`entry`=58273 /*58273*/ AND `locale`='deDE') OR (`entry`=68566 /*68566*/ AND `locale`='deDE') OR (`entry`=69791 /*69791*/ AND `locale`='deDE') OR (`entry`=69792 /*69792*/ AND `locale`='deDE') OR (`entry`=63508 /*63508*/ AND `locale`='deDE') OR (`entry`=59315 /*59315*/ AND `locale`='deDE') OR (`entry`=58885 /*58885*/ AND `locale`='deDE') OR (`entry`=59542 /*59542*/ AND `locale`='deDE') OR (`entry`=59653 /*59653*/ AND `locale`='deDE') OR (`entry`=59549 /*59549*/ AND `locale`='deDE') OR (`entry`=59608 /*59608*/ AND `locale`='deDE') OR (`entry`=59687 /*59687*/ AND `locale`='deDE') OR (`entry`=58938 /*58938*/ AND `locale`='deDE') OR (`entry`=65873 /*65873*/ AND `locale`='deDE') OR (`entry`=59252 /*59252*/ AND `locale`='deDE') OR (`entry`=59233 /*59233*/ AND `locale`='deDE') OR (`entry`=59232 /*59232*/ AND `locale`='deDE') OR (`entry`=59306 /*59306*/ AND `locale`='deDE') OR (`entry`=59237 /*59237*/ AND `locale`='deDE') OR (`entry`=58884 /*58884*/ AND `locale`='deDE') OR (`entry`=65185 /*65185*/ AND `locale`='deDE') OR (`entry`=58968 /*58968*/ AND `locale`='deDE') OR (`entry`=59236 /*59236*/ AND `locale`='deDE') OR (`entry`=65872 /*65872*/ AND `locale`='deDE') OR (`entry`=58969 /*58969*/ AND `locale`='deDE') OR (`entry`=59137 /*59137*/ AND `locale`='deDE') OR (`entry`=58547 /*58547*/ AND `locale`='deDE') OR (`entry`=65820 /*65820*/ AND `locale`='deDE') OR (`entry`=58609 /*58609*/ AND `locale`='deDE') OR (`entry`=67183 /*67183*/ AND `locale`='deDE') OR (`entry`=62869 /*62869*/ AND `locale`='deDE') OR (`entry`=58546 /*58546*/ AND `locale`='deDE') OR (`entry`=58611 /*58611*/ AND `locale`='deDE') OR (`entry`=58631 /*58631*/ AND `locale`='deDE') OR (`entry`=58490 /*58490*/ AND `locale`='deDE') OR (`entry`=65857 /*65857*/ AND `locale`='deDE') OR (`entry`=64367 /*64367*/ AND `locale`='deDE') OR (`entry`=65860 /*65860*/ AND `locale`='deDE') OR (`entry`=58489 /*58489*/ AND `locale`='deDE') OR (`entry`=58427 /*58427*/ AND `locale`='deDE') OR (`entry`=58424 /*58424*/ AND `locale`='deDE') OR (`entry`=58285 /*58285*/ AND `locale`='deDE') OR (`entry`=65627 /*65627*/ AND `locale`='deDE') OR (`entry`=56752 /*56752*/ AND `locale`='deDE') OR (`entry`=70948 /*70948*/ AND `locale`='deDE') OR (`entry`=70947 /*70947*/ AND `locale`='deDE') OR (`entry`=65821 /*65821*/ AND `locale`='deDE') OR (`entry`=65747 /*65747*/ AND `locale`='deDE') OR (`entry`=70949 /*70949*/ AND `locale`='deDE') OR (`entry`=70946 /*70946*/ AND `locale`='deDE') OR (`entry`=70945 /*70945*/ AND `locale`='deDE') OR (`entry`=70944 /*70944*/ AND `locale`='deDE') OR (`entry`=65894 /*65894*/ AND `locale`='deDE') OR (`entry`=65889 /*65889*/ AND `locale`='deDE') OR (`entry`=65745 /*65745*/ AND `locale`='deDE') OR (`entry`=65890 /*65890*/ AND `locale`='deDE') OR (`entry`=70951 /*70951*/ AND `locale`='deDE') OR (`entry`=70950 /*70950*/ AND `locale`='deDE') OR (`entry`=65746 /*65746*/ AND `locale`='deDE') OR (`entry`=65744 /*65744*/ AND `locale`='deDE') OR (`entry`=65748 /*65748*/ AND `locale`='deDE') OR (`entry`=65768 /*65768*/ AND `locale`='deDE') OR (`entry`=66095 /*66095*/ AND `locale`='deDE') OR (`entry`=55597 /*55597*/ AND `locale`='deDE') OR (`entry`=61091 /*61091*/ AND `locale`='deDE') OR (`entry`=65823 /*65823*/ AND `locale`='deDE') OR (`entry`=64034 /*64034*/ AND `locale`='deDE') OR (`entry`=63767 /*63767*/ AND `locale`='deDE') OR (`entry`=58165 /*58165*/ AND `locale`='deDE') OR (`entry`=63276 /*63276*/ AND `locale`='deDE') OR (`entry`=66067 /*66067*/ AND `locale`='deDE') OR (`entry`=65626 /*65626*/ AND `locale`='deDE') OR (`entry`=63282 /*63282*/ AND `locale`='deDE') OR (`entry`=58117 /*58117*/ AND `locale`='deDE') OR (`entry`=58419 /*58419*/ AND `locale`='deDE') OR (`entry`=58068 /*58068*/ AND `locale`='deDE') OR (`entry`=58185 /*58185*/ AND `locale`='deDE') OR (`entry`=58184 /*58184*/ AND `locale`='deDE') OR (`entry`=58857 /*58857*/ AND `locale`='deDE') OR (`entry`=63139 /*63139*/ AND `locale`='deDE') OR (`entry`=59715 /*59715*/ AND `locale`='deDE') OR (`entry`=66733 /*66733*/ AND `locale`='deDE') OR (`entry`=66713 /*66713*/ AND `locale`='deDE') OR (`entry`=66712 /*66712*/ AND `locale`='deDE') OR (`entry`=61027 /*61027*/ AND `locale`='deDE') OR (`entry`=66714 /*66714*/ AND `locale`='deDE') OR (`entry`=60278 /*60278*/ AND `locale`='deDE') OR (`entry`=59763 /*59763*/ AND `locale`='deDE') OR (`entry`=60774 /*60774*/ AND `locale`='deDE') OR (`entry`=59714 /*59714*/ AND `locale`='deDE') OR (`entry`=63878 /*63878*/ AND `locale`='deDE') OR (`entry`=63877 /*63877*/ AND `locale`='deDE') OR (`entry`=59936 /*59936*/ AND `locale`='deDE') OR (`entry`=63887 /*63887*/ AND `locale`='deDE') OR (`entry`=59584 /*59584*/ AND `locale`='deDE') OR (`entry`=63885 /*63885*/ AND `locale`='deDE') OR (`entry`=63884 /*63884*/ AND `locale`='deDE') OR (`entry`=63882 /*63882*/ AND `locale`='deDE') OR (`entry`=60762 /*60762*/ AND `locale`='deDE') OR (`entry`=60136 /*60136*/ AND `locale`='deDE') OR (`entry`=63917 /*63917*/ AND `locale`='deDE') OR (`entry`=60760 /*60760*/ AND `locale`='deDE') OR (`entry`=64087 /*64087*/ AND `locale`='deDE') OR (`entry`=63886 /*63886*/ AND `locale`='deDE') OR (`entry`=63883 /*63883*/ AND `locale`='deDE') OR (`entry`=63721 /*63721*/ AND `locale`='deDE') OR (`entry`=60674 /*60674*/ AND `locale`='deDE') OR (`entry`=64088 /*64088*/ AND `locale`='deDE') OR (`entry`=60693 /*60693*/ AND `locale`='deDE') OR (`entry`=67197 /*67197*/ AND `locale`='deDE') OR (`entry`=59586 /*59586*/ AND `locale`='deDE') OR (`entry`=60135 /*60135*/ AND `locale`='deDE') OR (`entry`=60673 /*60673*/ AND `locale`='deDE') OR (`entry`=60675 /*60675*/ AND `locale`='deDE') OR (`entry`=44880 /*44880*/ AND `locale`='deDE') OR (`entry`=59775 /*59775*/ AND `locale`='deDE') OR (`entry`=66251 /*66251*/ AND `locale`='deDE') OR (`entry`=60761 /*60761*/ AND `locale`='deDE') OR (`entry`=58139 /*58139*/ AND `locale`='deDE') OR (`entry`=59049 /*59049*/ AND `locale`='deDE') OR (`entry`=58942 /*58942*/ AND `locale`='deDE') OR (`entry`=58941 /*58941*/ AND `locale`='deDE') OR (`entry`=58940 /*58940*/ AND `locale`='deDE') OR (`entry`=68311 /*68311*/ AND `locale`='deDE') OR (`entry`=68821 /*68821*/ AND `locale`='deDE') OR (`entry`=68822 /*68822*/ AND `locale`='deDE') OR (`entry`=68490 /*68490*/ AND `locale`='deDE') OR (`entry`=68486 /*68486*/ AND `locale`='deDE') OR (`entry`=68485 /*68485*/ AND `locale`='deDE') OR (`entry`=68483 /*68483*/ AND `locale`='deDE') OR (`entry`=67665 /*67665*/ AND `locale`='deDE') OR (`entry`=68331 /*68331*/ AND `locale`='deDE') OR (`entry`=68380 /*68380*/ AND `locale`='deDE') OR (`entry`=68157 /*68157*/ AND `locale`='deDE') OR (`entry`=63288 /*63288*/ AND `locale`='deDE') OR (`entry`=62670 /*62670*/ AND `locale`='deDE') OR (`entry`=56114 /*56114*/ AND `locale`='deDE') OR (`entry`=58867 /*58867*/ AND `locale`='deDE') OR (`entry`=58790 /*58790*/ AND `locale`='deDE') OR (`entry`=58784 /*58784*/ AND `locale`='deDE') OR (`entry`=58745 /*58745*/ AND `locale`='deDE') OR (`entry`=58735 /*58735*/ AND `locale`='deDE') OR (`entry`=58926 /*58926*/ AND `locale`='deDE') OR (`entry`=58789 /*58789*/ AND `locale`='deDE') OR (`entry`=59151 /*59151*/ AND `locale`='deDE') OR (`entry`=58887 /*58887*/ AND `locale`='deDE') OR (`entry`=59116 /*59116*/ AND `locale`='deDE') OR (`entry`=58779 /*58779*/ AND `locale`='deDE') OR (`entry`=58070 /*58070*/ AND `locale`='deDE') OR (`entry`=58850 /*58850*/ AND `locale`='deDE') OR (`entry`=65598 /*65598*/ AND `locale`='deDE') OR (`entry`=58161 /*58161*/ AND `locale`='deDE') OR (`entry`=58173 /*58173*/ AND `locale`='deDE') OR (`entry`=58170 /*58170*/ AND `locale`='deDE') OR (`entry`=68312 /*68312*/ AND `locale`='deDE') OR (`entry`=68328 /*68328*/ AND `locale`='deDE') OR (`entry`=58881 /*58881*/ AND `locale`='deDE') OR (`entry`=68333 /*68333*/ AND `locale`='deDE') OR (`entry`=68332 /*68332*/ AND `locale`='deDE') OR (`entry`=68326 /*68326*/ AND `locale`='deDE') OR (`entry`=61090 /*61090*/ AND `locale`='deDE') OR (`entry`=68334 /*68334*/ AND `locale`='deDE') OR (`entry`=58880 /*58880*/ AND `locale`='deDE') OR (`entry`=57825 /*57825*/ AND `locale`='deDE') OR (`entry`=57649 /*57649*/ AND `locale`='deDE') OR (`entry`=58221 /*58221*/ AND `locale`='deDE') OR (`entry`=71021 /*71021*/ AND `locale`='deDE') OR (`entry`=58377 /*58377*/ AND `locale`='deDE') OR (`entry`=58937 /*58937*/ AND `locale`='deDE') OR (`entry`=59046 /*59046*/ AND `locale`='deDE') OR (`entry`=59310 /*59310*/ AND `locale`='deDE') OR (`entry`=58172 /*58172*/ AND `locale`='deDE') OR (`entry`=65814 /*65814*/ AND `locale`='deDE') OR (`entry`=65802 /*65802*/ AND `locale`='deDE') OR (`entry`=63291 /*63291*/ AND `locale`='deDE') OR (`entry`=59196 /*59196*/ AND `locale`='deDE') OR (`entry`=57285 /*57285*/ AND `locale`='deDE') OR (`entry`=59224 /*59224*/ AND `locale`='deDE') OR (`entry`=58931 /*58931*/ AND `locale`='deDE') OR (`entry`=58858 /*58858*/ AND `locale`='deDE') OR (`entry`=65799 /*65799*/ AND `locale`='deDE') OR (`entry`=65741 /*65741*/ AND `locale`='deDE') OR (`entry`=65740 /*65740*/ AND `locale`='deDE') OR (`entry`=65739 /*65739*/ AND `locale`='deDE') OR (`entry`=65738 /*65738*/ AND `locale`='deDE') OR (`entry`=66892 /*66892*/ AND `locale`='deDE') OR (`entry`=65819 /*65819*/ AND `locale`='deDE') OR (`entry`=63293 /*63293*/ AND `locale`='deDE') OR (`entry`=64798 /*64798*/ AND `locale`='deDE') OR (`entry`=58533 /*58533*/ AND `locale`='deDE') OR (`entry`=58274 /*58274*/ AND `locale`='deDE') OR (`entry`=63064 /*63064*/ AND `locale`='deDE') OR (`entry`=69947 /*69947*/ AND `locale`='deDE') OR (`entry`=57267 /*57267*/ AND `locale`='deDE') OR (`entry`=57456 /*57456*/ AND `locale`='deDE') OR (`entry`=57257 /*57257*/ AND `locale`='deDE') OR (`entry`=58227 /*58227*/ AND `locale`='deDE') OR (`entry`=59251 /*59251*/ AND `locale`='deDE') OR (`entry`=59197 /*59197*/ AND `locale`='deDE') OR (`entry`=57215 /*57215*/ AND `locale`='deDE') OR (`entry`=56524 /*56524*/ AND `locale`='deDE') OR (`entry`=57216 /*57216*/ AND `locale`='deDE') OR (`entry`=64799 /*64799*/ AND `locale`='deDE') OR (`entry`=58181 /*58181*/ AND `locale`='deDE') OR (`entry`=59235 /*59235*/ AND `locale`='deDE') OR (`entry`=65759 /*65759*/ AND `locale`='deDE') OR (`entry`=58116 /*58116*/ AND `locale`='deDE') OR (`entry`=58067 /*58067*/ AND `locale`='deDE') OR (`entry`=63361 /*63361*/ AND `locale`='deDE') OR (`entry`=63796 /*63796*/ AND `locale`='deDE') OR (`entry`=63289 /*63289*/ AND `locale`='deDE') OR (`entry`=66096 /*66096*/ AND `locale`='deDE') OR (`entry`=58216 /*58216*/ AND `locale`='deDE') OR (`entry`=58215 /*58215*/ AND `locale`='deDE') OR (`entry`=67237 /*67237*/ AND `locale`='deDE') OR (`entry`=65124 /*65124*/ AND `locale`='deDE') OR (`entry`=62879 /*62879*/ AND `locale`='deDE') OR (`entry`=57744 /*57744*/ AND `locale`='deDE') OR (`entry`=56115 /*56115*/ AND `locale`='deDE') OR (`entry`=57830 /*57830*/ AND `locale`='deDE') OR (`entry`=58312 /*58312*/ AND `locale`='deDE') OR (`entry`=55626 /*55626*/ AND `locale`='deDE') OR (`entry`=60232 /*60232*/ AND `locale`='deDE') OR (`entry`=57457 /*57457*/ AND `locale`='deDE') OR (`entry`=100820 /*100820*/ AND `locale`='deDE') OR (`entry`=57999 /*57999*/ AND `locale`='deDE') OR (`entry`=57998 /*57998*/ AND `locale`='deDE') OR (`entry`=57745 /*57745*/ AND `locale`='deDE') OR (`entry`=58376 /*58376*/ AND `locale`='deDE') OR (`entry`=62431 /*62431*/ AND `locale`='deDE') OR (`entry`=62351 /*62351*/ AND `locale`='deDE') OR (`entry`=62429 /*62429*/ AND `locale`='deDE') OR (`entry`=62350 /*62350*/ AND `locale`='deDE') OR (`entry`=62352 /*62352*/ AND `locale`='deDE') OR (`entry`=62355 /*62355*/ AND `locale`='deDE') OR (`entry`=62346 /*62346*/ AND `locale`='deDE') OR (`entry`=63304 /*63304*/ AND `locale`='deDE') OR (`entry`=57851 /*57851*/ AND `locale`='deDE') OR (`entry`=65838 /*65838*/ AND `locale`='deDE') OR (`entry`=58899 /*58899*/ AND `locale`='deDE') OR (`entry`=56357 /*56357*/ AND `locale`='deDE') OR (`entry`=58900 /*58900*/ AND `locale`='deDE') OR (`entry`=56106 /*56106*/ AND `locale`='deDE') OR (`entry`=56034 /*56034*/ AND `locale`='deDE') OR (`entry`=63095 /*63095*/ AND `locale`='deDE') OR (`entry`=63094 /*63094*/ AND `locale`='deDE') OR (`entry`=56532 /*56532*/ AND `locale`='deDE') OR (`entry`=56531 /*56531*/ AND `locale`='deDE') OR (`entry`=56526 /*56526*/ AND `locale`='deDE') OR (`entry`=58889 /*58889*/ AND `locale`='deDE') OR (`entry`=58910 /*58910*/ AND `locale`='deDE') OR (`entry`=63062 /*63062*/ AND `locale`='deDE') OR (`entry`=58891 /*58891*/ AND `locale`='deDE') OR (`entry`=58890 /*58890*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(66069, 'deDE', 'Krasarangfrosch', NULL, NULL, NULL, 23222), -- 66069
(67927, 'deDE', 'Blutwache Gro''tash', NULL, NULL, NULL, 23222), -- 67927
(67812, 'deDE', 'Späherin Rokla', NULL, NULL, NULL, 23222), -- 67812
(67903, 'deDE', 'Grunzer der Horde', 'Grunzerin von Garrosh''ar', NULL, NULL, 23222), -- 67903
(67926, 'deDE', 'Grizzle Radflutsch', NULL, NULL, NULL, 23222), -- 67926
(67924, 'deDE', 'Bilgwässer Feldmesser', 'Bilgwässer Feldmesserin', NULL, NULL, 23222), -- 67924
(67904, 'deDE', 'Priester der Horde', 'Priesterin der Horde', NULL, NULL, 23222), -- 67904
(67884, 'deDE', 'Bilgewässer Sprengenieur', 'Bilgewässer Sprengenieurin', NULL, NULL, 23222), -- 67884
(67902, 'deDE', 'Priester der Allianz', 'Priesterin der Allianz', NULL, NULL, 23222), -- 67902
(67901, 'deDE', 'Fußsoldat der Allianz', 'Fußsoldatin der Allianz', NULL, NULL, 23222), -- 67901
(67825, 'deDE', 'Späher der Kor''kron', 'Späherin der Kor''kron', NULL, NULL, 23222), -- 67825
(67900, 'deDE', 'Schildwache der Allianz', NULL, NULL, NULL, 23222), -- 67900
(66752, 'deDE', 'Haijunges', NULL, NULL, NULL, 23222), -- 66752
(58954, 'deDE', 'Botschafter Len', NULL, 'Gesandtschaft von Steinpflug', NULL, 23222), -- 58954
(65638, 'deDE', 'Korjanisklavenmeister', NULL, NULL, NULL, 23222), -- 65638
(58645, 'deDE', 'Verfallener Wächter', NULL, NULL, NULL, 23222), -- 58645
(66070, 'deDE', 'Krasarangfluthüpfer', NULL, NULL, NULL, 23222), -- 66070
(66071, 'deDE', 'Krasarangunke', NULL, NULL, NULL, 23222), -- 66071
(66745, 'deDE', 'Hippocampus Mystikus', NULL, NULL, NULL, 23222), -- 66745
(65054, 'deDE', 'Fieberbissjunges', NULL, NULL, 'wildpetcapturable', 23222), -- 65054
(58882, 'deDE', 'Fieberbissjunges', NULL, NULL, NULL, 23222), -- 58882
(66748, 'deDE', 'Altehrwürdige Zwingzange', NULL, NULL, NULL, 23222), -- 66748
(64263, 'deDE', 'Shado-Pan-Mauerwächter', NULL, NULL, NULL, 23222), -- 64263
(58415, 'deDE', 'Bergyak', NULL, NULL, NULL, 23222), -- 58415
(58368, 'deDE', 'Vorposten der Ik''thik', NULL, NULL, NULL, 23222), -- 58368
(58955, 'deDE', 'Gesandter von Steinpflug', 'Gesandte von Steinpflug', NULL, 'questinteract', 23222), -- 58955
(59783, 'deDE', 'Geist des Tiefenwaldes', NULL, NULL, NULL, 23222), -- 59783
(58367, 'deDE', 'Vorläufer der Ik''thik', NULL, NULL, NULL, 23222), -- 58367
(59784, 'deDE', 'Geisterjunges', NULL, NULL, NULL, 23222), -- 59784
(59295, 'deDE', 'Verirrtes Mushan', NULL, NULL, NULL, 23222), -- 59295
(58476, 'deDE', 'Die Schindenburg', NULL, NULL, NULL, 23222), -- 58476
(58830, 'deDE', 'Verwundete Schildwache', NULL, NULL, NULL, 23222), -- 58830
(59079, 'deDE', 'Christofen Mondfeder', NULL, 'Vorräte & Heilung', NULL, 23222), -- 59079
(59048, 'deDE', 'Maylen Mondfeder', NULL, 'Flugmeister', NULL, 23222), -- 59048
(58277, 'deDE', 'Om aus Fallsang', NULL, NULL, NULL, 23222), -- 58277
(58278, 'deDE', 'Der müde Shushen', NULL, 'Wassersprecher', NULL, 23222), -- 58278
(58276, 'deDE', 'Kai aus Fallsang', NULL, NULL, NULL, 23222), -- 58276
(58642, 'deDE', 'Krasarijunges', NULL, NULL, NULL, 23222), -- 58642
(58634, 'deDE', 'Korjanieintreiber', NULL, NULL, NULL, 23222), -- 58634
(58641, 'deDE', 'Krasaristreuner', NULL, NULL, NULL, 23222), -- 58641
(58639, 'deDE', 'Gefangene Schildwache', NULL, NULL, 'questinteract', 23222), -- 58639
(58608, 'deDE', 'Gefangener der Morgenjäger', 'Gefangene der Morgenjäger', NULL, 'questinteract', 23222), -- 58608
(58614, 'deDE', 'Korjanieroberer', NULL, NULL, NULL, 23222), -- 58614
(60602, 'deDE', 'Schüler von Chi-Ji', 'Schülerin von Chi-Ji', NULL, NULL, 23222), -- 60602
(66729, 'deDE', 'Krasaranggleiter', NULL, NULL, NULL, 23222), -- 66729
(65189, 'deDE', 'Federhüter Li', NULL, 'Flugmeister', NULL, 23222), -- 65189
(60506, 'deDE', 'Thelonius', NULL, NULL, NULL, 23222), -- 60506
(60528, 'deDE', 'Kuo-Na Stachelpfote', NULL, NULL, NULL, 23222), -- 60528
(60529, 'deDE', 'Yan Stachelpfote', NULL, NULL, NULL, 23222), -- 60529
(60601, 'deDE', 'Schüler von Chi-Ji', 'Schülerin von Chi-Ji', NULL, NULL, 23222), -- 60601
(60603, 'deDE', 'Schüler von Chi-Ji', 'Schülerin von Chi-Ji', NULL, NULL, 23222), -- 60603
(66755, 'deDE', 'Schleierwal', NULL, NULL, NULL, 23222), -- 66755
(66934, 'deDE', 'Damlak', NULL, 'Diener von Kril''mandar', NULL, 23222), -- 66934
(60358, 'deDE', 'Ungadorfbewohner', NULL, NULL, NULL, 23222), -- 60358
(60357, 'deDE', 'Ungaschatzverstecker', NULL, NULL, NULL, 23222), -- 60357
(60367, 'deDE', 'Zappelnder Karpfen', NULL, NULL, NULL, 23222), -- 60367
(60355, 'deDE', 'Magerer Karpfen', NULL, NULL, NULL, 23222), -- 60355
(60303, 'deDE', 'Magerer Karpfen', NULL, NULL, NULL, 23222), -- 60303
(60299, 'deDE', 'Ungafischholer', NULL, NULL, NULL, 23222), -- 60299
(66753, 'deDE', 'Schleierhai', NULL, NULL, NULL, 23222), -- 66753
(60289, 'deDE', 'Leni Kelpenstout', NULL, NULL, NULL, 23222), -- 60289
(65384, 'deDE', 'Grant', NULL, 'Angler-Azubi', NULL, 23222), -- 65384
(65383, 'deDE', 'Angler Abra', NULL, NULL, NULL, 23222), -- 65383
(65382, 'deDE', 'Angler Chris', NULL, NULL, NULL, 23222), -- 65382
(67184, 'deDE', 'Bogenmacher Suyin', NULL, 'Reparaturen', NULL, 23222), -- 67184
(62872, 'deDE', 'Cranpelz der Nudler', NULL, 'Gastwirt', NULL, 23222), -- 62872
(60441, 'deDE', 'Nan-Po', NULL, 'Flugmeister', NULL, 23222), -- 60441
(60290, 'deDE', 'John Shin', NULL, NULL, NULL, 23222), -- 60290
(60293, 'deDE', 'Max Narstave', NULL, NULL, NULL, 23222), -- 60293
(60173, 'deDE', 'Jay Wolkensturz', NULL, NULL, NULL, 23222), -- 60173
(65289, 'deDE', 'Braumeister Bo', NULL, NULL, NULL, 23222), -- 65289
(60182, 'deDE', 'Tony Thunfisch', NULL, NULL, NULL, 23222), -- 60182
(60422, 'deDE', 'Fischer von Marista', NULL, NULL, NULL, 23222), -- 60422
(60282, 'deDE', 'Verzweifelter Todesrachen', NULL, NULL, NULL, 23222), -- 60282
(60139, 'deDE', 'Die weise Ana Wu', NULL, NULL, NULL, 23222), -- 60139
(60279, 'deDE', 'Verstorbener Todesrachen', NULL, NULL, NULL, 23222), -- 60279
(60201, 'deDE', 'Todesrachenschnapper', NULL, NULL, NULL, 23222), -- 60201
(60196, 'deDE', 'Dunkler Lori', NULL, NULL, NULL, 23222), -- 60196
(60200, 'deDE', 'Smaragdfarbener Lori', NULL, NULL, NULL, 23222), -- 60200
(60198, 'deDE', 'Purpurroter Lori', NULL, NULL, NULL, 23222), -- 60198
(60202, 'deDE', 'Todesrachenschleicher', NULL, NULL, NULL, 23222), -- 60202
(69815, 'deDE', 'Stillstoßpterrorschwinge', NULL, NULL, NULL, 23222), -- 69815
(60487, 'deDE', 'Geist des Kranichs', NULL, NULL, NULL, 23222), -- 60487
(60616, 'deDE', 'Kind von Chi-Ji', NULL, NULL, NULL, 23222), -- 60616
(67182, 'deDE', 'Murn Starkhuf', NULL, NULL, NULL, 23222), -- 67182
(62967, 'deDE', 'Aizra Morgenjäger', NULL, 'Gastwirtin', NULL, 23222), -- 62967
(58610, 'deDE', 'Korjanihäscher', NULL, NULL, NULL, 23222), -- 58610
(58256, 'deDE', 'Fallensteller der Morgenjäger', 'Fallenstellerin der Morgenjäger', NULL, NULL, 23222), -- 58256
(59047, 'deDE', 'Mampf Windhuf', NULL, 'Flugmeister', NULL, 23222), -- 59047
(58255, 'deDE', 'Kriegerheld der Morgenjäger', 'Kriegerheldin der Morgenjäger', NULL, NULL, 23222), -- 58255
(58257, 'deDE', 'Hautschinder der Flussklingen', NULL, NULL, NULL, 23222), -- 58257
(65852, 'deDE', 'Portal zum Kun-Lai-Gipfel', NULL, NULL, NULL, 23222), -- 65852
(58273, 'deDE', 'Töter der Flussklingen', NULL, NULL, NULL, 23222), -- 58273
(68566, 'deDE', 'Huscher Xi''a', NULL, NULL, 'wildpet', 23222), -- 68566
(69791, 'deDE', 'Feuergeist', NULL, NULL, NULL, 23222), -- 69791
(69792, 'deDE', 'Erdgeist', NULL, NULL, NULL, 23222), -- 69792
(63508, 'deDE', 'Xuen', NULL, NULL, NULL, 23222), -- 63508
(59315, 'deDE', 'Schlund der Verzweiflung', NULL, NULL, NULL, 23222), -- 59315
(58885, 'deDE', 'Sha-besessener Kranich', NULL, NULL, NULL, 23222), -- 58885
(59542, 'deDE', 'Spuk der Verzweiflung', NULL, NULL, NULL, 23222), -- 59542
(59653, 'deDE', 'Chi-Ji', NULL, 'Der Rote Kranich', NULL, 23222), -- 59653
(59549, 'deDE', 'Sha-Deckenkrabbler', NULL, NULL, NULL, 23222), -- 59549
(59608, 'deDE', 'Anduin Wrynn', NULL, 'Prinz von Sturmwind', NULL, 23222), -- 59608
(59687, 'deDE', 'Echo der Verzweiflung', NULL, NULL, NULL, 23222), -- 59687
(58938, 'deDE', 'Sha-besessenes Tigerjunges', NULL, NULL, NULL, 23222), -- 58938
(65873, 'deDE', 'Quelle der Verzweiflung', NULL, NULL, NULL, 23222), -- 65873
(59252, 'deDE', 'Sha-Mauerkriecher', NULL, NULL, NULL, 23222), -- 59252
(59233, 'deDE', 'Akolyth der Kranichschwingen', 'Akolythin der Kranichschwingen', NULL, NULL, 23222), -- 59233
(59232, 'deDE', 'Mönch der Kranichschwingen', 'Mönch der Kranichschwingen', NULL, NULL, 23222), -- 59232
(59306, 'deDE', 'Welle der Verzweiflung', NULL, NULL, NULL, 23222), -- 59306
(59237, 'deDE', 'Schlund der Verzweiflung', NULL, NULL, NULL, 23222), -- 59237
(58884, 'deDE', 'Sha-besessene Tigerin', NULL, NULL, NULL, 23222), -- 58884
(65185, 'deDE', 'Mei-Li-Funkler', NULL, NULL, 'wildpetcapturable', 23222), -- 65185
(58968, 'deDE', 'Hoffnungsloser Mönch', 'Hoffnungsloser Mönch', NULL, NULL, 23222), -- 58968
(59236, 'deDE', 'Spuk der Verzweiflung', NULL, NULL, NULL, 23222), -- 59236
(65872, 'deDE', 'Inkarnation der Verzweiflung', NULL, NULL, NULL, 23222), -- 65872
(58969, 'deDE', 'Hoffnungsloser Akolyth', 'Hoffnungslose Akolythin', NULL, NULL, 23222), -- 58969
(59137, 'deDE', 'Schildwache des Übergriffs', NULL, NULL, 'lootall', 23222), -- 59137
(58547, 'deDE', 'Koro Nebelgänger', NULL, NULL, NULL, 23222), -- 58547
(65820, 'deDE', 'Gelbrückenmokassinschlange', NULL, NULL, NULL, 23222), -- 65820
(58609, 'deDE', 'Anduin Wrynn', NULL, 'Prinz von Sturmwind', NULL, 23222), -- 58609
(67183, 'deDE', 'Unterbringer An', NULL, 'Reparaturen', NULL, 23222), -- 67183
(62869, 'deDE', 'Ni die Gnädige', NULL, 'Gastwirtin', NULL, 23222), -- 62869
(58546, 'deDE', 'Schüler der Kranichschwingen', 'Schülerin der Kranichschwingen', NULL, NULL, 23222), -- 58546
(58611, 'deDE', 'Initiand der Kranichschwingen', 'Initiandin der Kranichschwingen', NULL, NULL, 23222), -- 58611
(58631, 'deDE', 'Angreifer der Düsterschuppen', NULL, NULL, NULL, 23222), -- 58631
(58490, 'deDE', 'Wildlandhirschkuh', NULL, NULL, NULL, 23222), -- 58490
(65857, 'deDE', 'Vergrabener Kopf', NULL, NULL, NULL, 23222), -- 65857
(64367, 'deDE', 'Unsichtbarer Mann', NULL, NULL, NULL, 23222), -- 64367
(65860, 'deDE', 'Mogulaternenpfahl', NULL, NULL, NULL, 23222), -- 65860
(58489, 'deDE', 'Wildlandhirsch', NULL, NULL, NULL, 23222), -- 58489
(58427, 'deDE', 'Junges Staubrückenmushan', NULL, NULL, NULL, 23222), -- 58427
(58424, 'deDE', 'Staubrückenmushan', NULL, NULL, NULL, 23222), -- 58424
(58285, 'deDE', 'Slovan', NULL, 'Räuberanführer', NULL, 23222), -- 58285
(65627, 'deDE', 'Staubrückenbeschützer', NULL, NULL, NULL, 23222), -- 65627
(56752, 'deDE', 'Weißkopfkranich', NULL, NULL, NULL, 23222), -- 56752
(70948, 'deDE', 'Jonathan Chen', NULL, NULL, NULL, 23222), -- 70948
(70947, 'deDE', 'Handwerker Haneke', NULL, NULL, NULL, 23222), -- 70947
(65821, 'deDE', 'Todesnatter der Kobaltschuppen', NULL, NULL, NULL, 23222), -- 65821
(65747, 'deDE', 'Yanlin Wasserkante', NULL, NULL, NULL, 23222), -- 65747
(70949, 'deDE', 'Jae-Sun Di Fo', NULL, NULL, NULL, 23222), -- 70949
(70946, 'deDE', 'Din Ayala', NULL, NULL, NULL, 23222), -- 70946
(70945, 'deDE', 'Jash Fu-Hsing', NULL, NULL, NULL, 23222), -- 70945
(70944, 'deDE', 'Shappu der Weise', NULL, NULL, NULL, 23222), -- 70944
(65894, 'deDE', 'Pao Pao', NULL, NULL, NULL, 23222), -- 65894
(65889, 'deDE', 'Su-Shi', NULL, NULL, NULL, 23222), -- 65889
(65745, 'deDE', 'Duyi Wasserkante', NULL, NULL, NULL, 23222), -- 65745
(65890, 'deDE', 'Maolin Wasserkante', NULL, NULL, NULL, 23222), -- 65890
(70951, 'deDE', 'Tom Wat', NULL, NULL, NULL, 23222), -- 70951
(70950, 'deDE', 'Hilda', NULL, NULL, NULL, 23222), -- 70950
(65746, 'deDE', 'Tengfei Wasserkante', NULL, NULL, NULL, 23222), -- 65746
(65744, 'deDE', 'Jun-Jun Wasserkante', NULL, NULL, NULL, 23222), -- 65744
(65748, 'deDE', 'Onkel Deming', NULL, NULL, NULL, 23222), -- 65748
(65768, 'deDE', 'Drachen', NULL, NULL, NULL, 23222), -- 65768
(66095, 'deDE', 'Cousin Brackentief', NULL, NULL, NULL, 23222), -- 66095
(55597, 'deDE', 'Na Lek', NULL, NULL, NULL, 23222), -- 55597
(61091, 'deDE', 'Alte Zwingzange', NULL, NULL, NULL, 23222), -- 61091
(65823, 'deDE', 'Fleischfressender Sandschnapper', NULL, NULL, NULL, 23222), -- 65823
(64034, 'deDE', 'Dieb der Bleichklingen', NULL, NULL, NULL, 23222), -- 64034
(63767, 'deDE', 'Chasheen', NULL, 'Königin der Tiefwildnis', NULL, 23222), -- 63767
(58165, 'deDE', 'Dojaniunterwerfer', NULL, NULL, NULL, 23222), -- 58165
(63276, 'deDE', 'Moguwaffe', NULL, NULL, NULL, 23222), -- 63276
(66067, 'deDE', 'Uralte Rune des Angriffs', NULL, NULL, NULL, 23222), -- 66067
(65626, 'deDE', 'Dojanivollstrecker', NULL, NULL, NULL, 23222), -- 65626
(63282, 'deDE', 'Vergrabener Qilen', NULL, NULL, NULL, 23222), -- 63282
(58117, 'deDE', 'Dojanieroberer', NULL, NULL, NULL, 23222), -- 58117
(58419, 'deDE', 'Schlafender Wächter', NULL, NULL, NULL, 23222), -- 58419
(58068, 'deDE', 'Dojanispäher', NULL, NULL, NULL, 23222), -- 58068
(58185, 'deDE', 'Kosta Morgenjäger', NULL, 'Händler für Kinkerlitzchen', NULL, 23222), -- 58185
(58184, 'deDE', 'Malaya Morgenjäger', NULL, 'Gastwirtin', NULL, 23222), -- 58184
(58857, 'deDE', 'Vorreiterin von Darnassus', NULL, NULL, NULL, 23222), -- 58857
(63139, 'deDE', 'Nachtsäbler', NULL, NULL, NULL, 23222), -- 63139
(59715, 'deDE', 'Häuptling der Flussklingen', NULL, 'Oberhäuptling', NULL, 23222), -- 59715
(66733, 'deDE', 'Mo''ruk', NULL, 'Großmeistertierzähmer', NULL, 23222), -- 66733
(66713, 'deDE', 'Stachelrücken', NULL, NULL, NULL, 23222), -- 66713
(66712, 'deDE', 'Schnitzer', NULL, NULL, NULL, 23222), -- 66712
(61027, 'deDE', 'Kobalthammerhai', NULL, NULL, NULL, 23222), -- 61027
(66714, 'deDE', 'Lichtjäger', NULL, NULL, NULL, 23222), -- 66714
(60278, 'deDE', 'Rochen', NULL, NULL, NULL, 23222), -- 60278
(59763, 'deDE', 'Großer Kranich', NULL, NULL, NULL, 23222), -- 59763
(60774, 'deDE', 'Verteidiger der Anglerexpedition', NULL, NULL, NULL, 23222), -- 60774
(59714, 'deDE', 'Räuber der Flussklingen', NULL, NULL, NULL, 23222), -- 59714
(63878, 'deDE', 'Schildkrötenjunges der Wandernden See', NULL, NULL, NULL, 23222), -- 63878
(63877, 'deDE', 'Schildkröte der Wandernden See', NULL, NULL, NULL, 23222), -- 63877
(59936, 'deDE', 'Gepanzerter Karpfen', NULL, NULL, NULL, 23222), -- 59936
(63887, 'deDE', 'Angler Marzai', NULL, NULL, NULL, 23222), -- 63887
(59584, 'deDE', 'Angler Haito', NULL, NULL, NULL, 23222), -- 59584
(63885, 'deDE', 'Linnshi', NULL, NULL, NULL, 23222), -- 63885
(63884, 'deDE', 'Chu', NULL, NULL, NULL, 23222), -- 63884
(63882, 'deDE', 'Betrunkener Angler', NULL, NULL, NULL, 23222), -- 63882
(60762, 'deDE', 'Ködermeister Wurmstreichler', NULL, 'Angelbedarf', NULL, 23222), -- 60762
(60136, 'deDE', 'Fiznix', NULL, 'Explosiver Fischer', NULL, 23222), -- 60136
(63917, 'deDE', 'Rai', NULL, 'Aufstrebende Anglerin', NULL, 23222), -- 63917
(60760, 'deDE', 'Hungrige Ratte', NULL, NULL, NULL, 23222), -- 60760
(64087, 'deDE', 'Marri', NULL, 'Aufstrebender Angler', NULL, 23222), -- 64087
(63886, 'deDE', 'Ryshan', NULL, NULL, NULL, 23222), -- 63886
(63883, 'deDE', 'Sully', NULL, NULL, NULL, 23222), -- 63883
(63721, 'deDE', 'Nat Pagle', NULL, 'Rüstmeister der Angler', 'lootall', 23222), -- 63721
(60674, 'deDE', 'John "Großer Haken" Marsock', NULL, 'Sportfischer', NULL, 23222), -- 60674
(64088, 'deDE', 'Hungrige Möwe', NULL, NULL, NULL, 23222), -- 64088
(60693, 'deDE', 'Fischer der Anglerexpedition', NULL, NULL, NULL, 23222), -- 60693
(67197, 'deDE', 'Verteidiger der Anlegestelle der Angler', NULL, NULL, NULL, 23222), -- 67197
(59586, 'deDE', 'Angler Shen', NULL, NULL, NULL, 23222), -- 59586
(60135, 'deDE', 'Fischdampfer Yotimo', NULL, 'Speerfischmeister', NULL, 23222), -- 60135
(60673, 'deDE', 'Anglerältester Rassan', NULL, NULL, NULL, 23222), -- 60673
(60675, 'deDE', 'Fo Fook', NULL, NULL, NULL, 23222), -- 60675
(44880, 'deDE', 'Möwe', NULL, NULL, NULL, 23222), -- 44880
(59775, 'deDE', 'Blauspitzenriffhai', NULL, NULL, NULL, 23222), -- 59775
(66251, 'deDE', 'Jägerin Vael''yrie', NULL, 'Stallmeisterin', NULL, 23222), -- 66251
(60761, 'deDE', 'Uferkrebs', NULL, NULL, NULL, 23222), -- 60761
(58139, 'deDE', 'Beobachtereule', NULL, 'Schildwachenbegleiter', NULL, 23222), -- 58139
(59049, 'deDE', 'Tylen Mondfeder', NULL, 'Flugmeister', NULL, 23222), -- 59049
(58942, 'deDE', 'Meerdrachenjungtier', NULL, NULL, NULL, 23222), -- 58942
(58941, 'deDE', 'Meerdrache', NULL, NULL, NULL, 23222), -- 58941
(58940, 'deDE', 'Alter Seedrache', NULL, NULL, NULL, 23222), -- 58940
(68311, 'deDE', 'Späherin Lynna', NULL, NULL, NULL, 23222), -- 68311
(68821, 'deDE', 'Wurzelformer der Allianz', 'Druidin der Allianz', NULL, NULL, 23222), -- 68821
(68822, 'deDE', 'Rootshaper Bunny', NULL, NULL, NULL, 23222), -- 68822
(68490, 'deDE', 'Phalanx der Allianz', NULL, NULL, NULL, 23222), -- 68490
(68486, 'deDE', 'Druide der Allianz', 'Druidin der Allianz', NULL, NULL, 23222), -- 68486
(68485, 'deDE', 'Fußsoldat der Allianz', 'Fußsoldatin der Allianz', NULL, NULL, 23222), -- 68485
(68483, 'deDE', 'Schildwache der Allianz', NULL, NULL, NULL, 23222), -- 68483
(67665, 'deDE', 'Manfred', NULL, 'Daily Spawn Controller', NULL, 23222), -- 67665
(68331, 'deDE', 'Marschall Trottmann', NULL, NULL, NULL, 23222), -- 68331
(68380, 'deDE', 'Räuber der Horde', 'Räuberin der Horde', NULL, NULL, 23222), -- 68380
(68157, 'deDE', 'Schneller grauer Wolf', NULL, NULL, NULL, 23222), -- 68157
(63288, 'deDE', 'Amethystspinnling', NULL, NULL, 'wildpetcapturable', 23222), -- 63288
(62670, 'deDE', 'Arkane Oubliette', NULL, NULL, NULL, 23222), -- 62670
(56114, 'deDE', 'Kang Dornstab', NULL, NULL, NULL, 23222), -- 56114
(58867, 'deDE', 'Rauch', NULL, NULL, NULL, 23222), -- 58867
(58790, 'deDE', 'Alynna Flüsterblatt', NULL, NULL, NULL, 23222), -- 58790
(58784, 'deDE', 'Jägerin des Übergriffs', NULL, NULL, NULL, 23222), -- 58784
(58745, 'deDE', 'Wissenshüter Vaeldrin', NULL, NULL, NULL, 23222), -- 58745
(58735, 'deDE', 'Lyalia', NULL, 'Kommandantin der Schildwachen', NULL, 23222), -- 58735
(58926, 'deDE', 'Magister Xintar', NULL, NULL, NULL, 23222), -- 58926
(58789, 'deDE', 'Elyssa Nachtköcher', NULL, 'Vorräte & Heilung', NULL, 23222), -- 58789
(59151, 'deDE', 'Kurier aus Zhus Wacht', NULL, NULL, NULL, 23222), -- 59151
(58887, 'deDE', 'Krasariraufer', NULL, NULL, NULL, 23222), -- 58887
(59116, 'deDE', 'Krasarijägerin', NULL, NULL, NULL, 23222), -- 59116
(58779, 'deDE', 'Daggle Sprengschreiter', NULL, 'Nesingwarys Safari', NULL, 23222), -- 58779
(58070, 'deDE', 'Krasarijägerin', NULL, NULL, NULL, 23222), -- 58070
(58850, 'deDE', 'Späher der Sonnenläufer', NULL, NULL, NULL, 23222), -- 58850
(65598, 'deDE', 'Krasarirunenbewahrer', NULL, NULL, NULL, 23222), -- 65598
(58161, 'deDE', 'Red Beacon Bunny', NULL, NULL, NULL, 23222), -- 58161
(58173, 'deDE', 'Green Beacon Bunny', NULL, NULL, NULL, 23222), -- 58173
(58170, 'deDE', 'Blue Beacon Bunny', NULL, NULL, NULL, 23222), -- 58170
(68312, 'deDE', 'Hilda Hornsturz', NULL, 'Forscherliga', NULL, 23222), -- 68312
(68328, 'deDE', 'Feldmesser der Forscherliga', 'Feldmesserin der Forscherliga', 'Forscherliga', NULL, 23222), -- 68328
(58881, 'deDE', 'Zwingzangenkrabbler', NULL, NULL, NULL, 23222), -- 58881
(68333, 'deDE', 'Priester der Horde', 'Priesterin der Horde', NULL, NULL, 23222), -- 68333
(68332, 'deDE', 'Räuber der Horde', 'Räuberin der Horde', NULL, NULL, 23222), -- 68332
(68326, 'deDE', 'Späher der 7. Legion', 'Späherin der 7. Legion', NULL, NULL, 23222), -- 68326
(61090, 'deDE', 'Junge Zwingzange', NULL, NULL, NULL, 23222), -- 61090
(68334, 'deDE', 'Jäger der Horde', 'Jägerin der Horde', NULL, NULL, 23222), -- 68334
(58880, 'deDE', 'Zwingzangenfischer', NULL, NULL, NULL, 23222), -- 58880
(57825, 'deDE', 'Yun', NULL, NULL, NULL, 23222), -- 57825
(57649, 'deDE', 'Weinender Schrecken', NULL, NULL, NULL, 23222), -- 57649
(58221, 'deDE', 'Krasaripirscher', NULL, NULL, NULL, 23222), -- 58221
(71021, 'deDE', 'Niederer Leerrufer', NULL, NULL, NULL, 23222), -- 71021
(58377, 'deDE', 'Krasariquäler', NULL, NULL, NULL, 23222), -- 58377
(58937, 'deDE', 'Sonnenläufer von Donnerkluft', 'Sonnenläuferin von Donnerkluft', NULL, NULL, 23222), -- 58937
(59046, 'deDE', 'Lira Himmelsspalter', NULL, 'Flugmeisterin', NULL, 23222), -- 59046
(59310, 'deDE', 'Teve Morgenjäger', NULL, 'Stallmeister', NULL, 23222), -- 59310
(58172, 'deDE', 'Versorgungskodo', NULL, NULL, NULL, 23222), -- 58172
(65814, 'deDE', 'Krasarizischwespling', NULL, NULL, NULL, 23222), -- 65814
(65802, 'deDE', 'Krasarizischwespe', NULL, NULL, NULL, 23222), -- 65802
(63291, 'deDE', 'Schmackhafter Käfer', NULL, NULL, 'wildpetcapturable', 23222), -- 63291
(59196, 'deDE', 'Krasaristreuner', NULL, NULL, NULL, 23222), -- 59196
(57285, 'deDE', 'General Purpose Pinpoint Bunny ZTO', NULL, NULL, NULL, 23222), -- 57285
(59224, 'deDE', 'Shai Klippenhüter', NULL, NULL, NULL, 23222), -- 59224
(58931, 'deDE', 'Blutvergießer der Flussklingen', NULL, NULL, NULL, 23222), -- 58931
(58858, 'deDE', 'Wegelagerer der Flussklingen', NULL, NULL, NULL, 23222), -- 58858
(65799, 'deDE', 'Flatternder Ritterfalter', NULL, NULL, NULL, 23222), -- 65799
(65741, 'deDE', 'Grünschuppenkranichfisch', NULL, NULL, NULL, 23222), -- 65741
(65740, 'deDE', 'Rotschuppenkranichfisch', NULL, NULL, NULL, 23222), -- 65740
(65739, 'deDE', 'Blauschuppenkranichfisch', NULL, NULL, NULL, 23222), -- 65739
(65738, 'deDE', 'Weißschuppenkranichfisch', NULL, NULL, NULL, 23222), -- 65738
(66892, 'deDE', 'Fallschreiter', NULL, NULL, NULL, 23222), -- 66892
(65819, 'deDE', 'Violetthügelsänger', NULL, NULL, NULL, 23222), -- 65819
(63293, 'deDE', 'Stachelige Wasserschildkröte', NULL, NULL, 'wildpetcapturable', 23222), -- 63293
(64798, 'deDE', 'Amethystspinnling', NULL, NULL, NULL, 23222), -- 64798
(58533, 'deDE', 'Staubrückenmushan', NULL, NULL, NULL, 23222), -- 58533
(58274, 'deDE', 'Fleischjäger der Flussklingen', NULL, NULL, NULL, 23222), -- 58274
(63064, 'deDE', 'Beuteldachsjunges', NULL, NULL, 'wildpetcapturable', 23222), -- 63064
(69947, 'deDE', 'Degu', NULL, NULL, NULL, 23222), -- 69947
(57267, 'deDE', 'Kerniger Korndieb', NULL, NULL, NULL, 23222), -- 57267
(57456, 'deDE', 'Felskuppe der Vier Winde', NULL, NULL, NULL, 23222), -- 57456
(57257, 'deDE', 'Höhlenschrecken', NULL, NULL, NULL, 23222), -- 57257
(58227, 'deDE', 'Dojanieroberer', NULL, NULL, NULL, 23222), -- 58227
(59251, 'deDE', 'Strauchdieb der Grabenflügel', NULL, NULL, NULL, 23222), -- 59251
(59197, 'deDE', 'Wipfelkreischer', NULL, NULL, NULL, 23222), -- 59197
(57215, 'deDE', 'Getreidejägerfalke', NULL, NULL, NULL, 23222), -- 57215
(56524, 'deDE', 'Tiefbissheuler', NULL, NULL, NULL, 23222), -- 56524
(57216, 'deDE', 'Erhabener Jäger', NULL, NULL, NULL, 23222), -- 57216
(64799, 'deDE', 'Schmackhafter Käfer', NULL, NULL, NULL, 23222), -- 64799
(58181, 'deDE', 'Kriegerheld von Donnerkluft', 'Kriegerheldin von Donnerkluft', NULL, NULL, 23222), -- 58181
(59235, 'deDE', 'Albinoblätterdachkreischer', NULL, NULL, NULL, 23222), -- 59235
(65759, 'deDE', 'Schillerschwirrling', NULL, NULL, NULL, 23222), -- 65759
(58116, 'deDE', 'Karpfenjäger', NULL, NULL, NULL, 23222), -- 58116
(58067, 'deDE', 'Dschungelhuscher', NULL, NULL, NULL, 23222), -- 58067
(63361, 'deDE', 'Mei-Li-Funkler', NULL, NULL, NULL, 23222), -- 63361
(63796, 'deDE', 'Dolchschnabel', NULL, 'Fürst des Schwarms', NULL, 23222), -- 63796
(63289, 'deDE', 'Luyumotte', NULL, NULL, NULL, 23222), -- 63289
(66096, 'deDE', 'Sonnenweidenmushankalb', NULL, NULL, NULL, 23222), -- 66096
(58216, 'deDE', 'Kräuterkundiger der Wildschuppen', NULL, NULL, NULL, 23222), -- 58216
(58215, 'deDE', 'Saurok der Wildschuppen', NULL, NULL, NULL, 23222), -- 58215
(67237, 'deDE', 'General Purpose Bunny JMF (Look 2 - Flying, Gigantic AOI)', NULL, NULL, NULL, 23222), -- 67237
(65124, 'deDE', 'Luyumotte', NULL, NULL, 'wildpetcapturable', 23222), -- 65124
(62879, 'deDE', 'Unhöflicher Sho', NULL, 'Gastwirt', NULL, 23222), -- 62879
(57744, 'deDE', 'Mei Fassgrund', NULL, NULL, NULL, 23222), -- 57744
(56115, 'deDE', 'Ken-Ken', NULL, NULL, NULL, 23222), -- 56115
(57830, 'deDE', 'Sunni', NULL, NULL, NULL, 23222), -- 57830
(58312, 'deDE', 'Manifestation der Verzweiflung', NULL, NULL, NULL, 23222), -- 58312
(55626, 'deDE', 'General Purpose Bunny (DLA)', NULL, NULL, NULL, 23222), -- 55626
(60232, 'deDE', 'Gi Hung', NULL, 'Flugmeister', NULL, 23222), -- 60232
(57457, 'deDE', 'Bedrückter Wächter von Zhu', 'Bedrückte Wächterin von Zhu', NULL, 'Speak', 23222), -- 57457
(100820, 'deDE', 'Geisterwolf', NULL, NULL, NULL, 23222), -- 100820
(57999, 'deDE', 'Yi-Mo Questgiver Axle', NULL, NULL, NULL, 23222), -- 57999
(57998, 'deDE', 'Yi-Mo Questgiver Wheel', NULL, NULL, NULL, 23222), -- 57998
(57745, 'deDE', 'Yi-Mo Langbraue', NULL, NULL, NULL, 23222), -- 57745
(58376, 'deDE', 'Yi-Mo Langbraue', NULL, NULL, NULL, 23222), -- 58376
(62431, 'deDE', 'Galleon Vehicle Bunny (Far Back)', NULL, NULL, NULL, 23222), -- 62431
(62351, 'deDE', 'Kriegstreiber von Salyis', NULL, NULL, NULL, 23222), -- 62351
(62429, 'deDE', 'Galleon Vehicle Bunny (Mid-Back)', NULL, NULL, NULL, 23222), -- 62429
(62350, 'deDE', 'Scharmützler von Salyis', NULL, NULL, NULL, 23222), -- 62350
(62352, 'deDE', 'Häuptling Salyis', NULL, NULL, NULL, 23222), -- 62352
(62355, 'deDE', 'Galleonkanone', NULL, NULL, NULL, 23222), -- 62355
(62346, 'deDE', 'Galleon', NULL, NULL, NULL, 23222), -- 62346
(63304, 'deDE', 'Dschungelraupe', NULL, NULL, 'wildpetcapturable', 23222), -- 63304
(57851, 'deDE', 'Donnervogel', NULL, NULL, NULL, 23222), -- 57851
(65838, 'deDE', 'Totes wolliges Schaf', NULL, NULL, NULL, 23222), -- 65838
(58899, 'deDE', 'Wilderer der Wildschuppen', NULL, NULL, NULL, 23222), -- 58899
(56357, 'deDE', 'Lupello', NULL, NULL, NULL, 23222), -- 56357
(58900, 'deDE', 'Jäger der Wildschuppen', NULL, NULL, NULL, 23222), -- 58900
(56106, 'deDE', 'Diebischer Wolf', NULL, NULL, NULL, 23222), -- 56106
(56034, 'deDE', 'Diebischer Ebenenfalke', NULL, NULL, NULL, 23222), -- 56034
(63095, 'deDE', 'Malayanisches Stachelrattenjunges', NULL, NULL, 'wildpetcapturable', 23222), -- 63095
(63094, 'deDE', 'Malayanische Stachelratte', NULL, NULL, 'wildpetcapturable', 23222), -- 63094
(56532, 'deDE', 'Lohfellbock', NULL, NULL, NULL, 23222), -- 56532
(56531, 'deDE', 'Lohfellricke', NULL, NULL, NULL, 23222), -- 56531
(56526, 'deDE', 'Lohfellkitz', NULL, NULL, NULL, 23222), -- 56526
(58889, 'deDE', 'Tiefbisspatriarch', NULL, NULL, NULL, 23222), -- 58889
(58910, 'deDE', 'Ebenenfalke', NULL, NULL, NULL, 23222), -- 58910
(63062, 'deDE', 'Beuteldachs', NULL, NULL, 'wildpetcapturable', 23222), -- 63062
(58891, 'deDE', 'Tiefbissjunges', NULL, NULL, NULL, 23222), -- 58891
(58890, 'deDE', 'Tiefbissbaumutter', NULL, NULL, NULL, 23222); -- 58890


DELETE FROM `page_text_locale` WHERE `ID` (`entry`=4456 /*4456*/ AND `locale`='deDE') OR (`entry`=4556 /*4556*/ AND `locale`='deDE') OR (`entry`=4502 /*4502*/ AND `locale`='deDE') OR (`entry`=4563 /*4563*/ AND `locale`='deDE') OR (`entry`=4819 /*4819*/ AND `locale`='deDE') OR (`entry`=4514 /*4514*/ AND `locale`='deDE');
INSERT INTO `page_text_locale` (`ID`, `locale`,`Text`, `VerifiedBuild`) VALUES
(4456, 'deDE', 'Die Ho-zen werden nicht alt. Ihre Ältesten werden für gewöhnlich nicht älter als 20 Jahre. Aus diesem Grund ist ihre relative Reife im Vergleich zu anderen kommunikationsfähigen Völkern eher gering.$b$bEs ist ein leidenschaftliches Volk, das es liebt zu lieben, zu hassen und jegliche Emotion zu empfinden, die möglich ist - solange sie stark ist.', 23222), -- 4456
(4556, 'deDE', 'An diesem Ort besiegte Shaohao, der letzte Kaiser von Pandaria, vor zehntausend Jahren das Sha der Verzweiflung und sperrte es ein.$b$bAus dem Buch der Bürden, Kapitel 9:$b$b"Nach seinem Erfolg im Jadewald war Kaiser Shaohao guten Mutes, doch er verzweifelte an der Ungewissheit der Zukunft. Er suchte den Rat des Roten Kranichs, Geist der Hoffnung, der tief in der Krasarangwildnis lebte."$b$b"Der Rote Kranich sagte dem Kaiser, dass die Hoffnung in uns allen ruhte, man müsse nur tief genug blicken. Daraufhin gab der Affenkönig dem Kaiser eine Maske der Verzweiflung, die eine Fratze schrecklicher Traurigkeit darstellte. Der Kaiser legte die Maske an und entlud sich somit seiner Hoffnungslosigkeit..."$b$bEine Woche und einen Tag kämpfte der Kaiser in strömendem Regen gegen das Sha, doch mit der Hilfe des Roten Kranichs und des Affenkönigs bezwang er seine Verzweiflung.$B$BVon jenem Tag an wusste der Kaiser, dass die Zukunft rosig sein würde. Er wurde ein Wesen der Hoffnung.', 23222), -- 4556
(4502, 'deDE', 'Trutzig bis zuletzt hielten die Saurok den Mogu in den Sümpfen von Krasarang stand. Hier hatten sie eine reelle Chance, indem sie die kaiserlichen Truppen tiefer auf unbekanntes Gelände lockten.$b$bDie Zahl der Todesopfer unter den Mogu stieg, als die Rebellen die Wasservorräte vergifteten und Anlagen sabotierten.$b$bIn seinem Zorn entsandte Kaiser Dojan immer mehr Soldaten, Sklaven und Waffen nach Krasarang, um die verbliebenen Saurok auszulöschen.$b$bDoch der Erfolg blieb ihnen verwehrt.', 23222), -- 4502
(4563, 'deDE', 'Von dieser Stätte aus zog der junge Pandarenforscher Liu Lang vor vielen Generationen los, um mit fast nichts außer einem Regenschirm und etwas Proviant die Welt vom Rücken einer Meeresschildkröte aus zu erkunden.$b$bZu dieser Zeit glaubten die meisten, dass der Rest der Welt von der Großen Zerschlagung zerstört worden sei. Auch nahm man an, dass Liu Lang... "wirr" im Kopf sei.$b$bAber dies erwies sich als falsch, als Liu Lang fünf Jahre später zurückkehrte und von geheimnisvollen Ländern jenseits des Meeres berichtete. Alle fünf Jahre würde er für den Rest seines Lebens nach Pandaria zurückkehren. Seine Schildkröte wuchs bei jedem Besuch, bis sie groß genug für eine ganze Kolonie war.$b$bWanderlustige Pandaren schauten oft über das Meer in Sehnsucht nach seiner Wiederkehr. Bis heute fragt man Leute, die über das Meer schauen, ob sie "auf die Schildkröte warten."', 23222), -- 4563
(4819, 'deDE', '- WANDERFEST -\n\nAlle sind eingeladen zum Wanderfest, einer Festlichkeit für alle Träumer unter uns! Besucht uns hier jeden Sonntagabend nach Sonnenuntergang am Schildkrötenstrand. Weitere Informationen sind von den wandernden Boten am Tag des Fests zu erhalten.', 23222), -- 4819
(4514, 'deDE', 'Vater des Zwiespaltes zwischen dunklem und hellem Bier und der Schule des ausgewogenen Rausches.$b$bIn dem Bestreben, die negativen Auswirkungen von Bier zu lindern, ohne jedoch seine Vorzüge zu beeinträchtigen, entwickelte Quan Tou Kuo ein Doppeltrinksystem, um einen Zustand des ausgewogenen Rausches zu bewirken. Bei separatem Genuss im richtigem Verhältnis sollte das helle Bier des Geistes und das dunkle Bier des Verstandes sich im Magen des Trinkers vermischen und zu einem Zustand der Erleuchtung und des Wohlwollens führen, ohne seine Urteilskraft und Selbstkontrolle zu beeinträchtigen, wie es für starke Trinker sonst typisch ist.', 23222); -- 4514


INSERT IGNORE INTO `page_text` (`ID`, `Text`, `NextPageID`, `PlayerConditionID`, `Flags`, `VerifiedBuild`) VALUES
(4456, 'Die Ho-zen werden nicht alt. Ihre Ältesten werden für gewöhnlich nicht älter als 20 Jahre. Aus diesem Grund ist ihre relative Reife im Vergleich zu anderen kommunikationsfähigen Völkern eher gering.$b$bEs ist ein leidenschaftliches Volk, das es liebt zu lieben, zu hassen und jegliche Emotion zu empfinden, die möglich ist - solange sie stark ist.', 0, 0, 0, 23222), -- 4456
(4556, 'An diesem Ort besiegte Shaohao, der letzte Kaiser von Pandaria, vor zehntausend Jahren das Sha der Verzweiflung und sperrte es ein.$b$bAus dem Buch der Bürden, Kapitel 9:$b$b"Nach seinem Erfolg im Jadewald war Kaiser Shaohao guten Mutes, doch er verzweifelte an der Ungewissheit der Zukunft. Er suchte den Rat des Roten Kranichs, Geist der Hoffnung, der tief in der Krasarangwildnis lebte."$b$b"Der Rote Kranich sagte dem Kaiser, dass die Hoffnung in uns allen ruhte, man müsse nur tief genug blicken. Daraufhin gab der Affenkönig dem Kaiser eine Maske der Verzweiflung, die eine Fratze schrecklicher Traurigkeit darstellte. Der Kaiser legte die Maske an und entlud sich somit seiner Hoffnungslosigkeit..."$b$bEine Woche und einen Tag kämpfte der Kaiser in strömendem Regen gegen das Sha, doch mit der Hilfe des Roten Kranichs und des Affenkönigs bezwang er seine Verzweiflung.$B$BVon jenem Tag an wusste der Kaiser, dass die Zukunft rosig sein würde. Er wurde ein Wesen der Hoffnung.', 0, 0, 0, 23222), -- 4556
(4502, 'Trutzig bis zuletzt hielten die Saurok den Mogu in den Sümpfen von Krasarang stand. Hier hatten sie eine reelle Chance, indem sie die kaiserlichen Truppen tiefer auf unbekanntes Gelände lockten.$b$bDie Zahl der Todesopfer unter den Mogu stieg, als die Rebellen die Wasservorräte vergifteten und Anlagen sabotierten.$b$bIn seinem Zorn entsandte Kaiser Dojan immer mehr Soldaten, Sklaven und Waffen nach Krasarang, um die verbliebenen Saurok auszulöschen.$b$bDoch der Erfolg blieb ihnen verwehrt.', 0, 0, 0, 23222), -- 4502
(4563, 'Von dieser Stätte aus zog der junge Pandarenforscher Liu Lang vor vielen Generationen los, um mit fast nichts außer einem Regenschirm und etwas Proviant die Welt vom Rücken einer Meeresschildkröte aus zu erkunden.$b$bZu dieser Zeit glaubten die meisten, dass der Rest der Welt von der Großen Zerschlagung zerstört worden sei. Auch nahm man an, dass Liu Lang... "wirr" im Kopf sei.$b$bAber dies erwies sich als falsch, als Liu Lang fünf Jahre später zurückkehrte und von geheimnisvollen Ländern jenseits des Meeres berichtete. Alle fünf Jahre würde er für den Rest seines Lebens nach Pandaria zurückkehren. Seine Schildkröte wuchs bei jedem Besuch, bis sie groß genug für eine ganze Kolonie war.$b$bWanderlustige Pandaren schauten oft über das Meer in Sehnsucht nach seiner Wiederkehr. Bis heute fragt man Leute, die über das Meer schauen, ob sie "auf die Schildkröte warten."', 0, 0, 0, 23222), -- 4563
(4819, '- WANDERFEST -\n\nAlle sind eingeladen zum Wanderfest, einer Festlichkeit für alle Träumer unter uns! Besucht uns hier jeden Sonntagabend nach Sonnenuntergang am Schildkrötenstrand. Weitere Informationen sind von den wandernden Boten am Tag des Fests zu erhalten.', 0, 0, 0, 23222), -- 4819
(4514, 'Vater des Zwiespaltes zwischen dunklem und hellem Bier und der Schule des ausgewogenen Rausches.$b$bIn dem Bestreben, die negativen Auswirkungen von Bier zu lindern, ohne jedoch seine Vorzüge zu beeinträchtigen, entwickelte Quan Tou Kuo ein Doppeltrinksystem, um einen Zustand des ausgewogenen Rausches zu bewirken. Bei separatem Genuss im richtigem Verhältnis sollte das helle Bier des Geistes und das dunkle Bier des Verstandes sich im Magen des Trinkers vermischen und zu einem Zustand der Erleuchtung und des Wohlwollens führen, ohne seine Urteilskraft und Selbstkontrolle zu beeinträchtigen, wie es für starke Trinker sonst typisch ist.', 0, 0, 0, 23222); -- 4514


INSERT IGNORE INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `OptionBroadcastTextID`, `action_poi_id`, `box_coded`, `box_money`, `box_text`, `BoxBroadcastTextID`) VALUES
(10181, 0, 1, 'Ich sehe mich nur mal um.', 0, 0, 0, 0, '', 0),
(14039, 1, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(14039, 0, 5, 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', 0, 0, 0, 0, '', 0),
(8903, 0, 5, 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', 0, 0, 0, 0, '', 0),
(13384, 0, 0, 'Wie seid Ihr hierher gekommen?', 0, 0, 0, 0, '', 0),
(13499, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13446, 0, 0, 'Was macht Ihr hier?', 0, 0, 0, 0, '', 0),
(14333, 0, 0, 'Was macht Ihr hier, so weit weg von zu Hause?', 0, 0, 0, 0, '', 0),
(14310, 1, 3, 'Bildet mich aus.', 0, 0, 0, 0, '', 0),
(14310, 0, 1, 'Ich sehe mich nur mal um.', 0, 0, 0, 0, '', 0),
(13419, 0, 0, 'Ich habe eine andere Frage.', 0, 0, 0, 0, '', 0),
(14050, 1, 1, 'Ich möchte ein wenig in Euren Waren stöbern.', 0, 0, 0, 0, '', 0),
(14050, 0, 5, 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', 0, 0, 0, 0, '', 0);


-- unknown broadcasttextid
INSERT IGNORE INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `comment`) VALUES
(50331, 0, 0, 'Spürt die Stärke der Yaungol!', 12, 0, 100, 0, 0, 0, 0, 'Go-Kan to Player'),
(50830, 0, 0, 'Her mit den Leckerli!', 12, 0, 100, 0, 0, 0, 0, 'Spriggin to Player'),
(56114, 0, 0, 'Vorsicht. Wenn Ihr darin stecken bleibt, wird es Euch die Lebenskraft aussaugen, um sich selbst mit Energie zu versorgen.', 12, 0, 100, 0, 0, 0, 0, 'Kang Dornstab to Player'),
(56114, 1, 0, 'Ja, ich hab'' auch schon versucht, durch das Ding zu springen.', 12, 0, 100, 0, 0, 0, 0, 'Kang Dornstab to Player'),
(57457, 0, 0, '...', 12, 0, 100, 0, 0, 0, 0, 'Bedrückte Wächterin von Zhu to Player'),
(58735, 0, 0, 'Vater, wir müssen uns zuerst um die Späher der Horde kümmern.', 12, 0, 100, 0, 0, 0, 0, 'Lyalia to Player'),
(58745, 0, 0, 'Achtet auf die magischen Fesseln an den Grenzbereichen der Oubliette. Es sollte drei davon geben.', 12, 0, 100, 0, 0, 0, 0, 'Wissenshüter Vaeldrin to Player'),
(60358, 0, 0, 'Djuuk...', 12, 0, 100, 396, 0, 0, 0, 'Ungadorfbewohner'),
(60602, 0, 0, 'Erhebt Eure Waffen, $R! Lasst uns kämpfen!', 14, 0, 100, 0, 0, 0, 0, 'Schüler von Chi-Ji to Player'),
(60602, 1, 0, 'Los geht''s!', 14, 0, 100, 0, 0, 0, 0, 'Schüler von Chi-Ji to Player'),
(62352, 0, 0, 'Plündern und brandschatzen, bwahahaha!', 14, 0, 100, 0, 0, 32500, 0, 'Häuptling Salyis to Galleon'),
(63721, 0, 0, 'Ah, wir teilen dieselbe Leidenschaft! Ich habe viele Ausrüstungsgegenstände von hiesigen Fischern. Sagt einfach Bescheid, wenn Ihr etwas benötigt.', 12, 0, 100, 0, 0, 0, 0, 'Nat Pagle to Player'),
(65626, 0, 0, 'Für den Donnerkönig!', 12, 0, 100, 0, 0, 0, 0, 'Dojanivollstrecker to Player'),
(65638, 0, 0, 'Der Meister kann nicht länger auf diese Artefakte warten.', 12, 0, 100, 5, 0, 0, 0, 'Korjanisklavenmeister'),
(65746, 0, 0, 'Vorsicht mit dem Teil, Jun-Jun! Ein starker Windstoß und Ihr werdet ins Meer geblasen!', 12, 0, 100, 1, 0, 0, 0, 'Tengfei Wasserkante'),
(65746, 1, 0, 'Liebe Güte, Duyi. Was habt Ihr diesem Ding zu fressen gegeben?', 12, 0, 100, 6, 0, 0, 0, 'Tengfei Wasserkante');
