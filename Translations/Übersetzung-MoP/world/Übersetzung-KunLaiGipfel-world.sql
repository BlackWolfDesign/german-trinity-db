-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_0_23222
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/22/2017 21:01:42


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=30457 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(30457, 'deDE', 'Raus mit dem Anführer', 'Tötet 10 Angehörige des Bataaristamms am Kun-Lai-Pass, bis der Feuerkrieger der Bataari auftaucht. Und dann tötet auch ihn.', 'Eure Hilfe für uns ist wirklich heldenhaft.$B$BBitte tötet den Bataari-Stamm am Kun-Lai-Pass im Nordwesten, bis der Feuerkrieger der Bataari wütend genug ist, um aufzutauchen. Und dann erledigt auch ihn.', '', 'Tötet 10 Mitglieder des Bataari-Stamms, um ihn zu provozieren. Und dann tötet auch ihn!', 'Feuerkrieger der Bataari', '', '', '', 23222);


DELETE FROM `quest_objectives_locale` WHERE (`ID`=252818);
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(252818, 'deDE', 30457, 1, 'Mitglieder des Bataaristammes getötet', 23222);


DELETE FROM `gameobject_template_locale` WHERE (`entry`=212501 AND `locale`='deDE') OR (`entry`=212498 AND `locale`='deDE') OR (`entry`=259031 AND `locale`='deDE') OR (`entry`=215461 AND `locale`='deDE') OR (`entry`=215460 AND `locale`='deDE') OR (`entry`=213503 AND `locale`='deDE') OR (`entry`=213774 AND `locale`='deDE') OR (`entry`=211456 AND `locale`='deDE') OR (`entry`=211454 AND `locale`='deDE') OR (`entry`=211480 AND `locale`='deDE') OR (`entry`=186459 AND `locale`='deDE') OR (`entry`=186458 AND `locale`='deDE') OR (`entry`=215594 AND `locale`='deDE') OR (`entry`=214751 AND `locale`='deDE') OR (`entry`=211504 AND `locale`='deDE') OR (`entry`=214427 AND `locale`='deDE') OR (`entry`=214748 AND `locale`='deDE') OR (`entry`=211511 AND `locale`='deDE') OR (`entry`=214757 AND `locale`='deDE') OR (`entry`=212877 AND `locale`='deDE') OR (`entry`=214756 AND `locale`='deDE') OR (`entry`=216719 AND `locale`='deDE') OR (`entry`=216679 AND `locale`='deDE') OR (`entry`=211527 AND `locale`='deDE') OR (`entry`=216706 AND `locale`='deDE') OR (`entry`=213455 AND `locale`='deDE') OR (`entry`=213803 AND `locale`='deDE') OR (`entry`=212859 AND `locale`='deDE') OR (`entry`=211541 AND `locale`='deDE') OR (`entry`=216414 AND `locale`='deDE') OR (`entry`=212504 AND `locale`='deDE') OR (`entry`=211770 AND `locale`='deDE') OR (`entry`=211793 AND `locale`='deDE') OR (`entry`=212860 AND `locale`='deDE') OR (`entry`=212505 AND `locale`='deDE') OR (`entry`=211794 AND `locale`='deDE') OR (`entry`=209984 AND `locale`='deDE') OR (`entry`=213793 AND `locale`='deDE') OR (`entry`=211901 AND `locale`='deDE') OR (`entry`=213438 AND `locale`='deDE') OR (`entry`=211602 AND `locale`='deDE') OR (`entry`=211601 AND `locale`='deDE') OR (`entry`=211598 AND `locale`='deDE') OR (`entry`=215797 AND `locale`='deDE') OR (`entry`=211597 AND `locale`='deDE') OR (`entry`=211559 AND `locale`='deDE') OR (`entry`=211900 AND `locale`='deDE') OR (`entry`=211898 AND `locale`='deDE') OR (`entry`=211528 AND `locale`='deDE') OR (`entry`=213331 AND `locale`='deDE') OR (`entry`=211967 AND `locale`='deDE') OR (`entry`=211965 AND `locale`='deDE') OR (`entry`=211712 AND `locale`='deDE') OR (`entry`=211707 AND `locale`='deDE') OR (`entry`=210503 AND `locale`='deDE') OR (`entry`=211994 AND `locale`='deDE') OR (`entry`=212488 AND `locale`='deDE') OR (`entry`=211307 AND `locale`='deDE') OR (`entry`=212506 AND `locale`='deDE') OR (`entry`=211754 AND `locale`='deDE') OR (`entry`=211758 AND `locale`='deDE') OR (`entry`=211757 AND `locale`='deDE') OR (`entry`=211756 AND `locale`='deDE') OR (`entry`=211772 AND `locale`='deDE') OR (`entry`=211523 AND `locale`='deDE') OR (`entry`=211759 AND `locale`='deDE') OR (`entry`=211760 AND `locale`='deDE') OR (`entry`=211567 AND `locale`='deDE') OR (`entry`=211762 AND `locale`='deDE') OR (`entry`=211763 AND `locale`='deDE') OR (`entry`=212903 AND `locale`='deDE') OR (`entry`=214464 AND `locale`='deDE') OR (`entry`=211761 AND `locale`='deDE') OR (`entry`=211755 AND `locale`='deDE') OR (`entry`=211688 AND `locale`='deDE') OR (`entry`=213770 AND `locale`='deDE') OR (`entry`=211269 AND `locale`='deDE') OR (`entry`=211270 AND `locale`='deDE') OR (`entry`=214540 AND `locale`='deDE') OR (`entry`=211268 AND `locale`='deDE') OR (`entry`=211274 AND `locale`='deDE') OR (`entry`=211273 AND `locale`='deDE') OR (`entry`=211266 AND `locale`='deDE') OR (`entry`=210929 AND `locale`='deDE') OR (`entry`=211686 AND `locale`='deDE') OR (`entry`=211568 AND `locale`='deDE') OR (`entry`=209351 AND `locale`='deDE') OR (`entry`=214407 AND `locale`='deDE') OR (`entry`=209354 AND `locale`='deDE') OR (`entry`=212481 AND `locale`='deDE') OR (`entry`=212652 AND `locale`='deDE') OR (`entry`=212651 AND `locale`='deDE') OR (`entry`=212479 AND `locale`='deDE') OR (`entry`=212954 AND `locale`='deDE') OR (`entry`=211566 AND `locale`='deDE') OR (`entry`=212955 AND `locale`='deDE') OR (`entry`=213162 AND `locale`='deDE') OR (`entry`=213157 AND `locale`='deDE') OR (`entry`=213155 AND `locale`='deDE') OR (`entry`=213166 AND `locale`='deDE') OR (`entry`=213156 AND `locale`='deDE') OR (`entry`=213159 AND `locale`='deDE') OR (`entry`=214380 AND `locale`='deDE') OR (`entry`=212883 AND `locale`='deDE') OR (`entry`=212874 AND `locale`='deDE') OR (`entry`=221244 AND `locale`='deDE') OR (`entry`=210680 AND `locale`='deDE') OR (`entry`=212870 AND `locale`='deDE') OR (`entry`=214396 AND `locale`='deDE') OR (`entry`=214394 AND `locale`='deDE') OR (`entry`=214398 AND `locale`='deDE') OR (`entry`=214397 AND `locale`='deDE') OR (`entry`=214395 AND `locale`='deDE') OR (`entry`=214399 AND `locale`='deDE') OR (`entry`=214393 AND `locale`='deDE') OR (`entry`=214388 AND `locale`='deDE') OR (`entry`=213242 AND `locale`='deDE') OR (`entry`=213241 AND `locale`='deDE') OR (`entry`=213243 AND `locale`='deDE') OR (`entry`=213240 AND `locale`='deDE') OR (`entry`=211838 AND `locale`='deDE') OR (`entry`=221991 AND `locale`='deDE') OR (`entry`=211839 AND `locale`='deDE') OR (`entry`=214617 AND `locale`='deDE') OR (`entry`=214616 AND `locale`='deDE') OR (`entry`=213164 AND `locale`='deDE') OR (`entry`=213165 AND `locale`='deDE') OR (`entry`=213161 AND `locale`='deDE') OR (`entry`=213167 AND `locale`='deDE') OR (`entry`=213160 AND `locale`='deDE') OR (`entry`=211416 AND `locale`='deDE') OR (`entry`=214744 AND `locale`='deDE') OR (`entry`=213814 AND `locale`='deDE') OR (`entry`=214745 AND `locale`='deDE') OR (`entry`=211327 AND `locale`='deDE') OR (`entry`=214743 AND `locale`='deDE') OR (`entry`=212482 AND `locale`='deDE') OR (`entry`=213815 AND `locale`='deDE') OR (`entry`=215867 AND `locale`='deDE') OR (`entry`=215866 AND `locale`='deDE') OR (`entry`=213417 AND `locale`='deDE') OR (`entry`=214742 AND `locale`='deDE') OR (`entry`=211657 AND `locale`='deDE') OR (`entry`=211656 AND `locale`='deDE') OR (`entry`=211312 AND `locale`='deDE') OR (`entry`=214746 AND `locale`='deDE') OR (`entry`=211328 AND `locale`='deDE') OR (`entry`=211446 AND `locale`='deDE') OR (`entry`=211143 AND `locale`='deDE') OR (`entry`=213782 AND `locale`='deDE') OR (`entry`=211147 AND `locale`='deDE') OR (`entry`=211148 AND `locale`='deDE') OR (`entry`=212486 AND `locale`='deDE') OR (`entry`=214766 AND `locale`='deDE') OR (`entry`=214765 AND `locale`='deDE') OR (`entry`=214426 AND `locale`='deDE') OR (`entry`=213382 AND `locale`='deDE') OR (`entry`=211538 AND `locale`='deDE') OR (`entry`=214764 AND `locale`='deDE') OR (`entry`=214763 AND `locale`='deDE') OR (`entry`=211537 AND `locale`='deDE') OR (`entry`=211536 AND `locale`='deDE') OR (`entry`=211175 AND `locale`='deDE') OR (`entry`=211124 AND `locale`='deDE') OR (`entry`=215966 AND `locale`='deDE') OR (`entry`=215965 AND `locale`='deDE') OR (`entry`=215964 AND `locale`='deDE') OR (`entry`=213379 AND `locale`='deDE') OR (`entry`=211171 AND `locale`='deDE') OR (`entry`=212487 AND `locale`='deDE') OR (`entry`=211172 AND `locale`='deDE') OR (`entry`=211170 AND `locale`='deDE') OR (`entry`=211338 AND `locale`='deDE') OR (`entry`=215081 AND `locale`='deDE') OR (`entry`=210943 AND `locale`='deDE') OR (`entry`=219228 AND `locale`='deDE') OR (`entry`=219227 AND `locale`='deDE') OR (`entry`=211181 AND `locale`='deDE') OR (`entry`=211180 AND `locale`='deDE') OR (`entry`=210948 AND `locale`='deDE') OR (`entry`=211539 AND `locale`='deDE') OR (`entry`=211340 AND `locale`='deDE') OR (`entry`=211264 AND `locale`='deDE') OR (`entry`=211703 AND `locale`='deDE') OR (`entry`=211963 AND `locale`='deDE') OR (`entry`=211704 AND `locale`='deDE') OR (`entry`=211702 AND `locale`='deDE') OR (`entry`=212494 AND `locale`='deDE') OR (`entry`=212493 AND `locale`='deDE') OR (`entry`=214172 AND `locale`='deDE') OR (`entry`=212491 AND `locale`='deDE') OR (`entry`=214173 AND `locale`='deDE') OR (`entry`=212502 AND `locale`='deDE') OR (`entry`=214174 AND `locale`='deDE') OR (`entry`=212490 AND `locale`='deDE') OR (`entry`=212497 AND `locale`='deDE') OR (`entry`=212495 AND `locale`='deDE') OR (`entry`=212496 AND `locale`='deDE') OR (`entry`=214104 AND `locale`='deDE') OR (`entry`=214103 AND `locale`='deDE') OR (`entry`=214102 AND `locale`='deDE') OR (`entry`=214171 AND `locale`='deDE') OR (`entry`=212492 AND `locale`='deDE') OR (`entry`=212002 AND `locale`='deDE') OR (`entry`=212001 AND `locale`='deDE') OR (`entry`=211996 AND `locale`='deDE') OR (`entry`=211997 AND `locale`='deDE') OR (`entry`=213301 AND `locale`='deDE') OR (`entry`=211998 AND `locale`='deDE') OR (`entry`=212000 AND `locale`='deDE') OR (`entry`=211999 AND `locale`='deDE') OR (`entry`=211521 AND `locale`='deDE') OR (`entry`=211510 AND `locale`='deDE') OR (`entry`=212489 AND `locale`='deDE') OR (`entry`=212500 AND `locale`='deDE') OR (`entry`=212499 AND `locale`='deDE') OR (`entry`=212503 AND `locale`='deDE') OR (`entry`=211684 AND `locale`='deDE') OR (`entry`=213281 AND `locale`='deDE') OR (`entry`=187084 AND `locale`='deDE') OR (`entry`=214968 AND `locale`='deDE') OR (`entry`=212557 AND `locale`='deDE') OR (`entry`=213282 AND `locale`='deDE') OR (`entry`=215084 AND `locale`='deDE') OR (`entry`=215083 AND `locale`='deDE') OR (`entry`=213802 AND `locale`='deDE') OR (`entry`=211526 AND `locale`='deDE') OR (`entry`=214961 AND `locale`='deDE') OR (`entry`=214441 AND `locale`='deDE') OR (`entry`=215869 AND `locale`='deDE') OR (`entry`=213511 AND `locale`='deDE') OR (`entry`=214438 AND `locale`='deDE') OR (`entry`=210944 AND `locale`='deDE') OR (`entry`=213443 AND `locale`='deDE') OR (`entry`=214628 AND `locale`='deDE') OR (`entry`=215082 AND `locale`='deDE') OR (`entry`=214769 AND `locale`='deDE') OR (`entry`=214768 AND `locale`='deDE') OR (`entry`=215798 AND `locale`='deDE') OR (`entry`=212485 AND `locale`='deDE') OR (`entry`=214767 AND `locale`='deDE') OR (`entry`=213804 AND `locale`='deDE') OR (`entry`=214423 AND `locale`='deDE') OR (`entry`=214992 AND `locale`='deDE') OR (`entry`=210915 AND `locale`='deDE') OR (`entry`=210870 AND `locale`='deDE') OR (`entry`=210869 AND `locale`='deDE') OR (`entry`=211531 AND `locale`='deDE') OR (`entry`=211530 AND `locale`='deDE') OR (`entry`=215699 AND `locale`='deDE') OR (`entry`=212650 AND `locale`='deDE') OR (`entry`=212649 AND `locale`='deDE') OR (`entry`=212480 AND `locale`='deDE') OR (`entry`=213806 AND `locale`='deDE') OR (`entry`=211615 AND `locale`='deDE') OR (`entry`=214773 AND `locale`='deDE') OR (`entry`=211614 AND `locale`='deDE') OR (`entry`=214772 AND `locale`='deDE') OR (`entry`=214771 AND `locale`='deDE') OR (`entry`=211219 AND `locale`='deDE') OR (`entry`=211201 AND `locale`='deDE') OR (`entry`=211211 AND `locale`='deDE') OR (`entry`=212691 AND `locale`='deDE') OR (`entry`=212483 AND `locale`='deDE') OR (`entry`=211202 AND `locale`='deDE') OR (`entry`=212690 AND `locale`='deDE') OR (`entry`=212881 AND `locale`='deDE') OR (`entry`=212882 AND `locale`='deDE') OR (`entry`=212880 AND `locale`='deDE') OR (`entry`=212878 AND `locale`='deDE') OR (`entry`=212879 AND `locale`='deDE') OR (`entry`=211023 AND `locale`='deDE') OR (`entry`=211019 AND `locale`='deDE') OR (`entry`=211018 AND `locale`='deDE') OR (`entry`=210933 AND `locale`='deDE') OR (`entry`=212774 AND `locale`='deDE') OR (`entry`=212773 AND `locale`='deDE') OR (`entry`=212772 AND `locale`='deDE') OR (`entry`=212777 AND `locale`='deDE') OR (`entry`=212776 AND `locale`='deDE') OR (`entry`=210759 AND `locale`='deDE') OR (`entry`=211225 AND `locale`='deDE') OR (`entry`=211238 AND `locale`='deDE') OR (`entry`=215381 AND `locale`='deDE') OR (`entry`=215382 AND `locale`='deDE') OR (`entry`=211391 AND `locale`='deDE') OR (`entry`=211389 AND `locale`='deDE') OR (`entry`=211242 AND `locale`='deDE') OR (`entry`=211390 AND `locale`='deDE') OR (`entry`=211693 AND `locale`='deDE') OR (`entry`=210868 AND `locale`='deDE') OR (`entry`=211668 AND `locale`='deDE') OR (`entry`=212177 AND `locale`='deDE') OR (`entry`=215783 AND `locale`='deDE') OR (`entry`=215865 AND `locale`='deDE') OR (`entry`=211699 AND `locale`='deDE') OR (`entry`=211643 AND `locale`='deDE') OR (`entry`=210887 AND `locale`='deDE');
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(212501, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212498, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(259031, 'deDE', 'Portal nach Sturmwind', '', NULL, 23222),
(215461, 'deDE', 'Amboss', '', NULL, 23222),
(215460, 'deDE', 'Schmiede', '', NULL, 23222),
(213503, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(213774, 'deDE', 'Besitztümer des verschollenen Abenteurers', '', NULL, 23222),
(211456, 'deDE', 'Kafa''zäge-"Vorkommen"', '', NULL, 23222),
(211454, 'deDE', 'Kafa''kota-Strauch', '', NULL, 23222),
(211480, 'deDE', 'Kafa''kota-Beeren', '', NULL, 23222),
(186459, 'deDE', 'Stratholme Fire Medium', '', NULL, 23222),
(186458, 'deDE', 'Stratholme Fire Small', '', NULL, 23222),
(215594, 'deDE', 'Yaungolleichenhaufen', '', NULL, 23222),
(214751, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211504, 'deDE', 'Amboss von Osul', '', NULL, 23222),
(214427, 'deDE', 'Lins Tagebuch (I)', '', NULL, 23222),
(214748, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211511, 'deDE', 'Trockenholzkäfig', '', NULL, 23222),
(214757, 'deDE', 'Totemfeuer', '', NULL, 23222),
(212877, 'deDE', 'Hochexplosives Fass voll Yaungolöl', '', NULL, 23222),
(214756, 'deDE', 'Totemfeuer', '', NULL, 23222),
(216719, 'deDE', 'Mogukryptatür', '', NULL, 23222),
(216679, 'deDE', 'Altar der Korun', '', NULL, 23222),
(211527, 'deDE', 'Doodad_Mogu_Tile_Lightning001', '', NULL, 23222),
(216706, 'deDE', 'Wolkenfalle', '', NULL, 23222),
(213455, 'deDE', 'Des Kaisers Bürde - Teil 7', '', NULL, 23222),
(213803, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212859, 'deDE', 'Versammlungsstein', '', NULL, 23222),
(211541, 'deDE', 'Nest eines Raubvogels', '', NULL, 23222),
(216414, 'deDE', 'Mogukryptatür', '', NULL, 23222),
(212504, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211770, 'deDE', 'Tafel aus der Shendynastie', '', NULL, 23222),
(211793, 'deDE', 'Tafel der Waidynastie', '', NULL, 23222),
(212860, 'deDE', 'Seltsamer Text', '', NULL, 23222),
(212505, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211794, 'deDE', 'Tafel der Qiangdynastie', '', NULL, 23222),
(209984, 'deDE', 'Nazgrims Angriffspläne', '', NULL, 23222),
(213793, 'deDE', 'Rikktiks Schatulle', '', NULL, 23222),
(211901, 'deDE', 'Kaputte Statue', '', NULL, 23222),
(213438, 'deDE', 'Ren Yun der Blinde', '', NULL, 23222),
(211602, 'deDE', 'Schrein des Herzens des Suchers', '', NULL, 23222),
(211601, 'deDE', 'Schrein des Odems des Suchers', '', NULL, 23222),
(211598, 'deDE', 'Chos Lagerfeuer', '', NULL, 23222),
(215797, 'deDE', 'Des Kaisers Bürde - Teil 2', '', NULL, 23222),
(211597, 'deDE', 'Schrein des Körpers des Suchers', '', NULL, 23222),
(211559, 'deDE', 'Schatzbeutel', '', NULL, 23222),
(211900, 'deDE', 'Kaputte Statue', '', NULL, 23222),
(211898, 'deDE', 'Speerfalle', '', NULL, 23222),
(211528, 'deDE', 'Doodad_Mogu_Tile_Lightning001', '', NULL, 23222),
(213331, 'deDE', 'Tal der Kaiser', '', NULL, 23222),
(211967, 'deDE', 'Truhe des Königs', '', NULL, 23222),
(211965, 'deDE', 'Rückstoßfalle', '', NULL, 23222),
(211712, 'deDE', 'Blitzfalle', '', NULL, 23222),
(211707, 'deDE', 'Feuerfalle', '', NULL, 23222),
(210503, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211994, 'deDE', 'Die Verwüstung der Ho-zen', '', NULL, 23222),
(212488, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(211307, 'deDE', 'Gummelkäfig', '', NULL, 23222),
(212506, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211754, 'deDE', 'Seltsamer Text', '', NULL, 23222),
(211758, 'deDE', 'Südmeerbeute', '', NULL, 23222),
(211757, 'deDE', 'Südmeerbeute', '', NULL, 23222),
(211756, 'deDE', 'Südmeerbeute', '', NULL, 23222),
(211772, 'deDE', 'Ritualklinge', '', NULL, 23222),
(211523, 'deDE', 'Ritualkreis', '', NULL, 23222),
(211759, 'deDE', 'Zerbrochene Begräbnisurne', '', NULL, 23222),
(211760, 'deDE', 'Südmeerbeute', '', NULL, 23222),
(211567, 'deDE', 'Südmeerbeute', '', NULL, 23222),
(211762, 'deDE', 'Kaputte Statue', '', NULL, 23222),
(211763, 'deDE', 'Kaputte Statue', '', NULL, 23222),
(212903, 'deDE', 'Verlassenes Wrack', '', NULL, 23222),
(214464, 'deDE', 'Mogu Crypt Doors (phased/closed)', '', NULL, 23222),
(211761, 'deDE', 'Kaputte Statue', '', NULL, 23222),
(211755, 'deDE', 'Südmeerbeute', '', NULL, 23222),
(211688, 'deDE', 'Kaiser Rikktik', '', NULL, 23222),
(213770, 'deDE', 'Gestohlene Berggeiststatue', '', NULL, 23222),
(211269, 'deDE', 'Gestohlene Versorgungsgüter', '', NULL, 23222),
(211270, 'deDE', 'Gestohlene Versorgungsgüter', '', NULL, 23222),
(214540, 'deDE', 'Gestohlene Versorgungsgüter', '', NULL, 23222),
(211268, 'deDE', 'Gestohlene Versorgungsgüter', '', NULL, 23222),
(211274, 'deDE', 'Gestohlene Versorgungsgüter', '', NULL, 23222),
(211273, 'deDE', 'Gestohlene Versorgungsgüter', '', NULL, 23222),
(211266, 'deDE', 'Gestohlene Versorgungsgüter', '', NULL, 23222),
(210929, 'deDE', 'Affengötze', '', NULL, 23222),
(211686, 'deDE', 'Grummelausrüstung', '', NULL, 23222),
(211568, 'deDE', 'Schneelilienräucherwerk', '', NULL, 23222),
(209351, 'deDE', 'Schneelilie', '', NULL, 23222),
(214407, 'deDE', 'Mo-Mos Schatztruhe', '', NULL, 23222),
(209354, 'deDE', 'Goldlotus', '', NULL, 23222),
(212481, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212652, 'deDE', 'Amboss', '', NULL, 23222),
(212651, 'deDE', 'Schmiede', '', NULL, 23222),
(212479, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212954, 'deDE', 'Basis einer Mogustatue', '', NULL, 23222),
(211566, 'deDE', 'Bleichwindtotem', '', NULL, 23222),
(212955, 'deDE', 'Basis einer Mogustatue, groß', '', NULL, 23222),
(213162, 'deDE', 'Spider Web Straight Rope 1', '', NULL, 23222),
(213157, 'deDE', 'Spider Web Hanging Arch 1', '', NULL, 23222),
(213155, 'deDE', 'Spider Web Angled Rope 1', '', NULL, 23222),
(213166, 'deDE', 'Spider Web Wide Angled', '', NULL, 23222),
(213156, 'deDE', 'Spider Web Angled Rope 2', '', NULL, 23222),
(213159, 'deDE', 'Spider Web Hanging Arch 2', '', NULL, 23222),
(214380, 'deDE', 'Moguschlangenstatue', '', NULL, 23222),
(212883, 'deDE', 'Mogulagerfeuer', '', NULL, 23222),
(212874, 'deDE', 'Mogubanner', '', NULL, 23222),
(221244, 'deDE', 'Basis einer Schatztruhe', '', NULL, 23222),
(210680, 'deDE', 'Moguzelt', '', NULL, 23222),
(212870, 'deDE', 'Mogukohlenbecken', '', NULL, 23222),
(214396, 'deDE', 'Guo-Lai-Trümmer', '', NULL, 23222),
(214394, 'deDE', 'Uraltes Guo-Lai-Artefakt', '', NULL, 23222),
(214398, 'deDE', 'Guo-Lai-Trümmer', '', NULL, 23222),
(214397, 'deDE', 'Guo-Lai-Trümmer', '', NULL, 23222),
(214395, 'deDE', 'Guo-Lai-Trümmer', '', NULL, 23222),
(214399, 'deDE', 'Guo-Lai-Trümmer', '', NULL, 23222),
(214393, 'deDE', 'Guo-Lai-Artefaktpodest', '', NULL, 23222),
(214388, 'deDE', 'Uralte Guo-Lai-Truhe', '', NULL, 23222),
(213242, 'deDE', 'Wolkenfalle', '', NULL, 23222),
(213241, 'deDE', 'Feuerfalle', '', NULL, 23222),
(213243, 'deDE', 'Pfeilfalle', '', NULL, 23222),
(213240, 'deDE', 'Blitzfalle', '', NULL, 23222),
(211838, 'deDE', 'Futon einer Schildwache', '', NULL, 23222),
(221991, 'deDE', 'Shao-Tien-Reis', '', NULL, 23222),
(211839, 'deDE', 'Futon einer Schildwache', '', NULL, 23222),
(214617, 'deDE', 'Waffenständer', '', NULL, 23222),
(214616, 'deDE', 'Waffenständer', '', NULL, 23222),
(213164, 'deDE', 'Spider Web Tall Twisted', '', NULL, 23222),
(213165, 'deDE', 'Spider Web Tall Angled', '', NULL, 23222),
(213161, 'deDE', 'Spider Web Hanging Arch 4', '', NULL, 23222),
(213167, 'deDE', 'Spider Web Round Ground Wrap', '', NULL, 23222),
(213160, 'deDE', 'Spider Web Hanging Arch 3', '', NULL, 23222),
(211416, 'deDE', 'Jins Fass', '', NULL, 23222),
(214744, 'deDE', 'Totemfeuer', '', NULL, 23222),
(213814, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214745, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211327, 'deDE', 'Feuerschildkollisionswand', '', NULL, 23222),
(214743, 'deDE', 'Totemfeuer', '', NULL, 23222),
(212482, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(213815, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(215867, 'deDE', 'Yaungolschädel', '', NULL, 23222),
(215866, 'deDE', 'Yaungoltotem', '', NULL, 23222),
(213417, 'deDE', 'Taktiken der Yaungol', '', NULL, 23222),
(214742, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211657, 'deDE', 'Amboss', '', NULL, 23222),
(211656, 'deDE', 'Schmiede', '', NULL, 23222),
(211312, 'deDE', 'Explosives Fass', '', NULL, 23222),
(214746, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211328, 'deDE', 'Feuerwand', '', NULL, 23222),
(211446, 'deDE', 'Käfig', '', NULL, 23222),
(211143, 'deDE', 'Relikt der Mogu', '', NULL, 23222),
(213782, 'deDE', 'Terrakottakopf', '', NULL, 23222),
(211147, 'deDE', 'Relikt der Mogu', '', NULL, 23222),
(211148, 'deDE', 'Relikt der Mogu', '', NULL, 23222),
(212486, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(214766, 'deDE', 'Totemfeuer', '', NULL, 23222),
(214765, 'deDE', 'Totemfeuer', '', NULL, 23222),
(214426, 'deDE', 'Wir sind Yaungol', '', NULL, 23222),
(213382, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211538, 'deDE', 'Gestohlene Tasche mit Glücksbringern', '', NULL, 23222),
(214764, 'deDE', 'Totemfeuer', '', NULL, 23222),
(214763, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211537, 'deDE', 'Gestohlene Tasche mit Glücksbringern', '', NULL, 23222),
(211536, 'deDE', 'Gestohlene Tasche mit Glücksbringern', '', NULL, 23222),
(211175, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211124, 'deDE', 'Staubiges Tagebuch', '', NULL, 23222),
(215966, 'deDE', 'Hocker', '', NULL, 23222),
(215965, 'deDE', 'Hocker', '', NULL, 23222),
(215964, 'deDE', 'Hocker', '', NULL, 23222),
(213379, 'deDE', 'Briefkasten', '', NULL, 23222),
(211171, 'deDE', 'Amboss', '', NULL, 23222),
(212487, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(211172, 'deDE', 'Waffenständer', '', NULL, 23222),
(211170, 'deDE', 'Schmiede', '', NULL, 23222),
(211338, 'deDE', 'Räucherwerk für die Reise', '', NULL, 23222),
(215081, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210943, 'deDE', 'Briefkasten', '', NULL, 23222),
(219228, 'deDE', 'Glücksstehler', '', NULL, 23222),
(219227, 'deDE', 'Glücksbringer', '', NULL, 23222),
(211181, 'deDE', 'Glückbringendes Juteräucherwerkbündel', '', NULL, 23222),
(211180, 'deDE', 'Jasminräucherwerkbündel', '', NULL, 23222),
(210948, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211539, 'deDE', 'Glückbringendes Juteräucherwerk', '', NULL, 23222),
(211340, 'deDE', 'Brennendes Räucherwerk', '', NULL, 23222),
(211264, 'deDE', 'Gestohlene Nimmerlaya-Versorgungsgüter', '', NULL, 23222),
(211703, 'deDE', 'Goldene Türen', '', NULL, 23222),
(211963, 'deDE', 'Goldenes Tor', '', NULL, 23222),
(211704, 'deDE', 'Goldene Türen', '', NULL, 23222),
(211702, 'deDE', 'Goldene Türen', '', NULL, 23222),
(212494, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212493, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(214172, 'deDE', 'Voodookessel der Zandalari', '', NULL, 23222),
(212491, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214173, 'deDE', 'Voodookessel der Zandalari', '', NULL, 23222),
(212502, 'deDE', 'Kessel', '', NULL, 23222),
(214174, 'deDE', 'Voodookessel der Zandalari', '', NULL, 23222),
(212490, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212497, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212495, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212496, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(214104, 'deDE', 'Carne Asada 02', '', NULL, 23222),
(214103, 'deDE', 'Carne Asada 01', '', NULL, 23222),
(214102, 'deDE', 'OM_Bones_01', '', NULL, 23222),
(214171, 'deDE', 'Voodookessel der Zandalari', '', NULL, 23222),
(212492, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212002, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212001, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211996, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211997, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(213301, 'deDE', 'Haufen Kanonenkugeln', '', NULL, 23222),
(211998, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212000, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211999, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211521, 'deDE', 'Schwarzmähnenbeutefass', '', NULL, 23222),
(211510, 'deDE', 'Besitztümer des Weisen Liao', '', NULL, 23222),
(212489, 'deDE', 'Kessel', '', NULL, 23222),
(212500, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212499, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212503, 'deDE', 'Kessel', '', NULL, 23222),
(211684, 'deDE', 'Zunderblüte', '', NULL, 23222),
(213281, 'deDE', 'Kaputter Speer', '', NULL, 23222),
(187084, 'deDE', 'Großes Feuer', '', NULL, 23222),
(214968, 'deDE', 'Mysteriöses Käfergefäß', '', NULL, 23222),
(212557, 'deDE', 'Briefkasten', '', NULL, 23222),
(213282, 'deDE', 'Reparierter Speer', '', NULL, 23222),
(215084, 'deDE', 'Schmiede', '', NULL, 23222),
(215083, 'deDE', 'Amboss', '', NULL, 23222),
(213802, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211526, 'deDE', 'Vom Wasserfall geglätteter Stein', '', NULL, 23222),
(214961, 'deDE', 'Versammlungsstein', '', NULL, 23222),
(214441, 'deDE', 'Instance Portal (Raid 4 Difficulties)', '', NULL, 23222),
(215869, 'deDE', 'Mönchsgong', '', NULL, 23222),
(213511, 'deDE', 'Sieg am Kun-Lai', '', NULL, 23222),
(214438, 'deDE', 'Uralte Mogutafel', '', NULL, 23222),
(210944, 'deDE', 'Briefkasten', '', NULL, 23222),
(213443, 'deDE', 'Des Kaisers Bürde - Teil 6', '', NULL, 23222),
(214628, 'deDE', 'Das starke Kohlenbecken', '', NULL, 23222),
(215082, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214769, 'deDE', 'Totemfeuer', '', NULL, 23222),
(214768, 'deDE', 'Totemfeuer', '', NULL, 23222),
(215798, 'deDE', 'Yaungöl', '', NULL, 23222),
(212485, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(214767, 'deDE', 'Totemfeuer', '', NULL, 23222),
(213804, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214423, 'deDE', 'Wir waren Krieger', '', NULL, 23222),
(214992, 'deDE', 'Mogustatue', '', NULL, 23222),
(210915, 'deDE', 'Eingang', '', NULL, 23222),
(210870, 'deDE', 'Tür', '', NULL, 23222),
(210869, 'deDE', 'Tür', '', NULL, 23222),
(211531, 'deDE', 'Yaungolbohrturm', '', NULL, 23222),
(211530, 'deDE', 'Yaungolbohrturm', '', NULL, 23222),
(215699, 'deDE', 'Yaungol Windmill Fire Spout', '', NULL, 23222),
(212650, 'deDE', 'Schmiede', '', NULL, 23222),
(212649, 'deDE', 'Amboss', '', NULL, 23222),
(212480, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(213806, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211615, 'deDE', 'Uralte Mogutür', '', NULL, 23222),
(214773, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211614, 'deDE', 'Uralte Mogutür', '', NULL, 23222),
(214772, 'deDE', 'Totemfeuer', '', NULL, 23222),
(214771, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211219, 'deDE', 'Faction Hub Not Taken', '', NULL, 23222),
(211201, 'deDE', 'Faction Hub Not Taken', '', NULL, 23222),
(211211, 'deDE', 'Faction Hub Not Taken', '', NULL, 23222),
(212691, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212483, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(211202, 'deDE', 'Faction Hub Not Taken', '', NULL, 23222),
(212690, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212881, 'deDE', 'Mogu Crate, Wide', '', NULL, 23222),
(212882, 'deDE', 'Mogu Crate, Type 2', '', NULL, 23222),
(212880, 'deDE', 'Moguteppich', '', NULL, 23222),
(212878, 'deDE', 'Mogu Tent, Slightly Smaller', '', NULL, 23222),
(212879, 'deDE', 'Mogu Crate, Type 1', '', NULL, 23222),
(211023, 'deDE', 'Gusseiserner Topf', '', NULL, 23222),
(211019, 'deDE', 'Wurzelgemüse', '', NULL, 23222),
(211018, 'deDE', 'Wurzelgemüse', '', NULL, 23222),
(210933, 'deDE', 'Yaungolbanner', '', NULL, 23222),
(212774, 'deDE', 'Schmiede', '', NULL, 23222),
(212773, 'deDE', 'Amboss', '', NULL, 23222),
(212772, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212777, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212776, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210759, 'deDE', 'Pfeil für Kommandantin Hsieh', '', NULL, 23222),
(211225, 'deDE', 'Faction Hub Not Taken', '', NULL, 23222),
(211238, 'deDE', 'Faction Hub Not Taken', '', NULL, 23222),
(215381, 'deDE', 'Uralte Guo-Lai-Tür', '', NULL, 23222),
(215382, 'deDE', 'Uralte Guo-Lai-Tür', '', NULL, 23222),
(211391, 'deDE', 'Stratholme Fire Small (Quest Invis 15)', '', NULL, 23222),
(211389, 'deDE', 'Stratholme Fire Large (Quest Invis 15)', '', NULL, 23222),
(211242, 'deDE', 'Faction Hub Not Taken', '', NULL, 23222),
(211390, 'deDE', 'Stratholme Fire Medium (Quest Invis 15)', '', NULL, 23222),
(211693, 'deDE', 'Innere Tore', '', NULL, 23222),
(210868, 'deDE', 'Äußere Tore', '', NULL, 23222),
(211668, 'deDE', 'Doodad_PA_ShadowpanDoor002', '', NULL, 23222),
(212177, 'deDE', 'Ein Schwarm Stachelfische', '', NULL, 23222),
(215783, 'deDE', 'Berufung', '', NULL, 23222),
(215865, 'deDE', 'Tafel der Jinyu', '', NULL, 23222),
(211699, 'deDE', 'Kun-Lai Summit - Jinyu Mere - Jinyu Weaponrack', '', NULL, 23222),
(211643, 'deDE', 'Amboss', '', NULL, 23222),
(210887, 'deDE', 'Unverdorbene Versorgungsgüter', '', NULL, 23222);


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=14992 AND `id`=1) OR (`menu_id`=14992 AND `id`=0) OR (`menu_id`=14986 AND `id`=1) OR (`menu_id`=14986 AND `id`=0) OR (`menu_id`=14993 AND `id`=1) OR (`menu_id`=14993 AND `id`=0) OR (`menu_id`=14994 AND `id`=0) OR (`menu_id`=14380 AND `id`=0) OR (`menu_id`=13830 AND `id`=0) OR (`menu_id`=83 AND `id`=0) OR (`menu_id`=13794 AND `id`=0) OR (`menu_id`=13795 AND `id`=1) OR (`menu_id`=13795 AND `id`=0) OR (`menu_id`=13644 AND `id`=0) OR (`menu_id`=13637 AND `id`=0) OR (`menu_id`=13635 AND `id`=1) OR (`menu_id`=13635 AND `id`=0) OR (`menu_id`=13636 AND `id`=0) OR (`menu_id`=13789 AND `id`=0) OR (`menu_id`=13556 AND `id`=1) OR (`menu_id`=13556 AND `id`=0) OR (`menu_id`=13555 AND `id`=0) OR (`menu_id`=13554 AND `id`=0) OR (`menu_id`=14326 AND `id`=0) OR (`menu_id`=13753 AND `id`=0) OR (`menu_id`=13810 AND `id`=0) OR (`menu_id`=10182 AND `id`=0) OR (`menu_id`=9868 AND `id`=1) OR (`menu_id`=9868 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(14992, 1, '', '', 'Unterrichtet mich im Kürschnern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14992, 0, '', '', 'Unterrichtet mich in Lederverarbeitung.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14986, 1, '', '', 'Welche Bräus habt Ihr im Angebot?', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14986, 0, '', '', 'Bildet mich aus.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14993, 1, '', '', 'Unterrichtet mich in Inschriftenkunde.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14993, 0, '', '', 'Unterrichtet mich in Kräuterkunde.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14994, 0, '', '', 'Unterrichtet mich in Erster Hilfe.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14380, 0, '', '', 'Nehmt den Gegenstand aus seinem Gepäck.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13830, 0, '', '', 'Ich würde gerne mit einem Drachen fliegen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(83, 0, '', '', 'Bringt mich ins Leben zurück.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13794, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13795, 1, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13795, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13644, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13637, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13635, 1, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13635, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13636, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13789, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13556, 1, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13556, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13555, 0, '', '', 'Zeigt mir Eure Waren.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13554, 0, '', '', 'Zeigt mir Eure Waren.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14326, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13753, 0, '', '', 'Ich möchte ein wenig Eure Ware betrachten.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13810, 0, '', '', 'Bitte, setzt Euch und macht es Euch bequem.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10182, 0, '', '', 'Ich sehe mich nur mal um.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9868, 1, '', '', 'Ich möchte ein wenig in Euren Waren stöbern.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9868, 0, '', '', 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', '', '', '', '', '', '', '', '', '', '', '', '', '');


DELETE FROM `locales_creature_text` WHERE (`entry`=50341 AND `groupid`=0) OR  (`entry`=50733 AND `groupid`=0) OR  (`entry`=50789 AND `groupid`=0) OR  (`entry`=59019 AND `groupid`=0) OR  (`entry`=59019 AND `groupid`=1) OR  (`entry`=59019 AND `groupid`=2) OR  (`entry`=59073 AND `groupid`=0) OR  (`entry`=59077 AND `groupid`=0) OR  (`entry`=59688 AND `groupid`=0) OR  (`entry`=59688 AND `groupid`=1) OR  (`entry`=59688 AND `groupid`=2) OR  (`entry`=59688 AND `groupid`=3) OR  (`entry`=59691 AND `groupid`=0) OR  (`entry`=59691 AND `groupid`=1) OR  (`entry`=59695 AND `groupid`=0) OR  (`entry`=59695 AND `groupid`=1) OR  (`entry`=59695 AND `groupid`=2) OR  (`entry`=59695 AND `groupid`=3) OR  (`entry`=59703 AND `groupid`=0) OR  (`entry`=59827 AND `groupid`=0) OR  (`entry`=59967 AND `groupid`=0) OR  (`entry`=60161 AND `groupid`=0) OR  (`entry`=60416 AND `groupid`=0) OR  (`entry`=60491 AND `groupid`=0) OR  (`entry`=60491 AND `groupid`=1) OR  (`entry`=60491 AND `groupid`=2) OR  (`entry`=60491 AND `groupid`=3) OR  (`entry`=60508 AND `groupid`=0) OR  (`entry`=60524 AND `groupid`=0) OR  (`entry`=60524 AND `groupid`=1) OR  (`entry`=61069 AND `groupid`=0) OR  (`entry`=61496 AND `groupid`=0) OR  (`entry`=61545 AND `groupid`=0) OR  (`entry`=61756 AND `groupid`=0) OR  (`entry`=62149 AND `groupid`=0) OR  (`entry`=62158 AND `groupid`=0) OR  (`entry`=64536 AND `groupid`=0) OR  (`entry`=64536 AND `groupid`=1) OR  (`entry`=64540 AND `groupid`=0) OR  (`entry`=66261 AND `groupid`=0);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(50341, 0, 0, '', '', 'Fühlt die Faust der Dunkelheit!', '', '', '', '', ''),
(50733, 0, 0, '', '', 'Ich spüre Eure Angst...', '', '', '', '', ''),
(50789, 0, 0, '', '', 'Ihr hättet nicht herkommen sollen!', '', '', '', '', ''),
(59019, 0, 0, '', '', 'Wir werden uns vielleicht in das Tal der Vier Winde zurückziehen müssen. Ich will mein Zuhause hier nicht aufgeben.', '', '', '', '', ''),
(59019, 1, 0, '', '', '$GEr:Sie; sieht ziemlich kräftig aus. Vielleicht kräftig genug, um die Yaungol zurückzuschlagen!', '', '', '', '', ''),
(59019, 2, 0, '', '', '$GEin Fremder:Eine Fremde;. $GEr:Sie; sieht sehr exotisch aus. Nicht anstarren!', '', '', '', '', ''),
(59073, 0, 0, '', '', 'Willkommen in Binan, $C. Ich bitte Euch... bitte helft uns.', '', '', '', '', ''),
(59077, 0, 0, '', '', 'Seid gegrüßt, $C. Eure Leute sind drinnen. Kann ich Euch jedoch zuerst um einen Gefallen bitten?', '', '', '', '', ''),
(59688, 0, 0, '', '', 'Kommt aus der Kälte herein und macht es Euch gemütlich!', '', '', '', '', ''),
(59688, 1, 0, '', '', 'Eine sichere Reise!', '', '', '', '', ''),
(59688, 2, 0, '', '', 'Willkommen, Gast! Bedient Euch am Bier.', '', '', '', '', ''),
(59688, 3, 0, '', '', 'Kommt bald wieder!', '', '', '', '', ''),
(59691, 0, 0, '', '', 'Vielleicht habt Ihr Interesse an einer magischen Landkarte? Ach, egal, die sind eh alle ausverkauft.', '', '', '', '', ''),
(59691, 1, 0, '', '', 'Willkommen in meinem Laden, $gReisender:Reisende;!', '', '', '', '', ''),
(59695, 0, 0, '', '', 'Seid gegrüßt, $C! Mir scheint, Ihr könntet Euch für frische Gewürze interessieren! Oder vielleicht ein paar Bücher für die Reise?', '', '', '', '', ''),
(59695, 1, 0, '', '', 'Stattet mir auf jeden Fall wieder einen Besuch ab, $C!', '', '', '', '', ''),
(59695, 2, 0, '', '', 'Seht Euch nur meine Waren an.', '', '', '', '', ''),
(59695, 3, 0, '', '', 'Kommt bald zurück!', '', '', '', '', ''),
(59703, 0, 0, '', '', 'Grummel! Versammelt Euch und atmet das Reiseräucherwerk tief ein. Viel Glück auf dem Weg zum Basislager am Kota.', '', '', '', '', ''),
(59827, 0, 0, '', '', 'Eine saubere Waffe ist ein Zeichen dafür, dass sie nie einen Kampf gesehen hat.', '', '', '', '', ''),
(59967, 0, 0, '', '', 'Ein schelmischer Schneegeist springt aus dem verdächtig aussehenden Schneehaufen hervor.', '', '', '', '', ''),
(60161, 0, 0, '', '', 'Ihr seid durchgekommen! Kommt, wir müssen reden.', '', '', '', '', ''),
(60416, 0, 0, '', '', 'Die Winde stehen heute gut.', '', '', '', '', ''),
(60491, 0, 0, '', '', 'Ihr werdet mich nicht erneut begraben!', '', '', '', '', ''),
(60491, 1, 0, '', '', 'Mein Zorn fließt ungehindert!', '', '', '', '', ''),
(60491, 2, 0, '', '', 'Eure Wut nährt mich!', '', '', '', '', ''),
(60491, 3, 0, '', '', 'Eure Wut gewährt Euch Stärke.', '', '', '', '', ''),
(60508, 0, 0, '', '', 'Ich dachte, Ihr spürt in Euren Knien nur das Wetter.', '', '', '', '', ''),
(60524, 0, 0, '', '', 'Ihr werdet dieses Land niemals bekommen!', '', '', '', '', ''),
(60524, 1, 0, '', '', 'Ihr legt Euch mit dem falschen Reich an!', '', '', '', '', ''),
(61069, 0, 0, '', '', 'EURE WUT! ICH SAUGE EURE WUT IN MICH AUF!', '', '', '', '', ''),
(61496, 0, 0, '', '', 'Ich habe mich vom Kampf zurückgezogen, um mit meiner Familie in Frieden zu leben, doch es ist mir anscheinend nicht vergönnt.', '', '', '', '', ''),
(61545, 0, 0, '', '', 'Warum haben alle solche Angst? Ich werde keinen dieser Trolle an unseren Brunnen heranlassen! Ich bin ein guter Wächter!', '', '', '', '', ''),
(61756, 0, 0, '', '', 'Wollt Ihr etwas zum Fressen? Habe ich ganz alleine gemacht!', '', '', '', '', ''),
(62149, 0, 0, '', '', 'Hm. Ich habe diesen Ort noch nie gemocht. Die Plumpsklos waren immer dreckig.', '', '', '', '', ''),
(62158, 0, 0, '', '', 'Die Ho-zen sind wieder auf Raubzug. Das spüre ich in meinen Knien.', '', '', '', '', ''),
(64536, 0, 0, '', '', 'Das halbe Tal ist verwüstet! Diese Fremdlinge haben mehr zerstört als die Mogu und Mantis zusammen!', '', '', '', '', ''),
(64536, 1, 0, '', '', 'Großer Tiger, den Fremden von jenseits des Nebels kann man nicht trauen. Die Absturzstelle im Jadewald ist Beweis genug.', '', '', '', '', ''),
(64540, 0, 0, '', '', 'Gebt uns eine Chance, es wieder in Ordnung zu bringen.', '', '', '', '', ''),
(66261, 0, 0, '', '', 'Reißt ihnen das Fleisch von den Knochen!', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=62383 /*62383*/ AND `locale`='deDE') OR (`entry`=66700 /*66700*/ AND `locale`='deDE') OR (`entry`=61061 /*61061*/ AND `locale`='deDE') OR (`entry`=60944 /*60944*/ AND `locale`='deDE') OR (`entry`=93814 /*93814*/ AND `locale`='deDE') OR (`entry`=66360 /*66360*/ AND `locale`='deDE') OR (`entry`=64975 /*64975*/ AND `locale`='deDE') OR (`entry`=64978 /*64978*/ AND `locale`='deDE') OR (`entry`=61068 /*61068*/ AND `locale`='deDE') OR (`entry`=66260 /*66260*/ AND `locale`='deDE') OR (`entry`=66258 /*66258*/ AND `locale`='deDE') OR (`entry`=66987 /*66987*/ AND `locale`='deDE') OR (`entry`=66986 /*66986*/ AND `locale`='deDE') OR (`entry`=66207 /*66207*/ AND `locale`='deDE') OR (`entry`=67009 /*67009*/ AND `locale`='deDE') OR (`entry`=64980 /*64980*/ AND `locale`='deDE') OR (`entry`=60443 /*60443*/ AND `locale`='deDE') OR (`entry`=32642 /*32642*/ AND `locale`='deDE') OR (`entry`=32641 /*32641*/ AND `locale`='deDE') OR (`entry`=66943 /*66943*/ AND `locale`='deDE') OR (`entry`=66253 /*66253*/ AND `locale`='deDE') OR (`entry`=66257 /*66257*/ AND `locale`='deDE') OR (`entry`=67095 /*67095*/ AND `locale`='deDE') OR (`entry`=67092 /*67092*/ AND `locale`='deDE') OR (`entry`=66946 /*66946*/ AND `locale`='deDE') OR (`entry`=66947 /*66947*/ AND `locale`='deDE') OR (`entry`=67066 /*67066*/ AND `locale`='deDE') OR (`entry`=67015 /*67015*/ AND `locale`='deDE') OR (`entry`=66254 /*66254*/ AND `locale`='deDE') OR (`entry`=67021 /*67021*/ AND `locale`='deDE') OR (`entry`=66255 /*66255*/ AND `locale`='deDE') OR (`entry`=66969 /*66969*/ AND `locale`='deDE') OR (`entry`=66356 /*66356*/ AND `locale`='deDE') OR (`entry`=70470 /*70470*/ AND `locale`='deDE') OR (`entry`=66359 /*66359*/ AND `locale`='deDE') OR (`entry`=64974 /*64974*/ AND `locale`='deDE') OR (`entry`=60945 /*60945*/ AND `locale`='deDE') OR (`entry`=60936 /*60936*/ AND `locale`='deDE') OR (`entry`=66358 /*66358*/ AND `locale`='deDE') OR (`entry`=66357 /*66357*/ AND `locale`='deDE') OR (`entry`=66353 /*66353*/ AND `locale`='deDE') OR (`entry`=60930 /*60930*/ AND `locale`='deDE') OR (`entry`=66355 /*66355*/ AND `locale`='deDE') OR (`entry`=66354 /*66354*/ AND `locale`='deDE') OR (`entry`=61060 /*61060*/ AND `locale`='deDE') OR (`entry`=65689 /*65689*/ AND `locale`='deDE') OR (`entry`=42720 /*42720*/ AND `locale`='deDE') OR (`entry`=64267 /*64267*/ AND `locale`='deDE') OR (`entry`=64227 /*64227*/ AND `locale`='deDE') OR (`entry`=60493 /*60493*/ AND `locale`='deDE') OR (`entry`=66738 /*66738*/ AND `locale`='deDE') OR (`entry`=66721 /*66721*/ AND `locale`='deDE') OR (`entry`=66720 /*66720*/ AND `locale`='deDE') OR (`entry`=66719 /*66719*/ AND `locale`='deDE') OR (`entry`=71942 /*71942*/ AND `locale`='deDE') OR (`entry`=63503 /*63503*/ AND `locale`='deDE') OR (`entry`=60564 /*60564*/ AND `locale`='deDE') OR (`entry`=61819 /*61819*/ AND `locale`='deDE') OR (`entry`=61820 /*61820*/ AND `locale`='deDE') OR (`entry`=61816 /*61816*/ AND `locale`='deDE') OR (`entry`=61454 /*61454*/ AND `locale`='deDE') OR (`entry`=61512 /*61512*/ AND `locale`='deDE') OR (`entry`=63603 /*63603*/ AND `locale`='deDE') OR (`entry`=61808 /*61808*/ AND `locale`='deDE') OR (`entry`=63564 /*63564*/ AND `locale`='deDE') OR (`entry`=61806 /*61806*/ AND `locale`='deDE') OR (`entry`=61810 /*61810*/ AND `locale`='deDE') OR (`entry`=60455 /*60455*/ AND `locale`='deDE') OR (`entry`=61843 /*61843*/ AND `locale`='deDE') OR (`entry`=63576 /*63576*/ AND `locale`='deDE') OR (`entry`=66289 /*66289*/ AND `locale`='deDE') OR (`entry`=60730 /*60730*/ AND `locale`='deDE') OR (`entry`=60705 /*60705*/ AND `locale`='deDE') OR (`entry`=60692 /*60692*/ AND `locale`='deDE') OR (`entry`=63549 /*63549*/ AND `locale`='deDE') OR (`entry`=62608 /*62608*/ AND `locale`='deDE') OR (`entry`=62554 /*62554*/ AND `locale`='deDE') OR (`entry`=64802 /*64802*/ AND `locale`='deDE') OR (`entry`=64801 /*64801*/ AND `locale`='deDE') OR (`entry`=62553 /*62553*/ AND `locale`='deDE') OR (`entry`=62552 /*62552*/ AND `locale`='deDE') OR (`entry`=66476 /*66476*/ AND `locale`='deDE') OR (`entry`=66471 /*66471*/ AND `locale`='deDE') OR (`entry`=64434 /*64434*/ AND `locale`='deDE') OR (`entry`=60939 /*60939*/ AND `locale`='deDE') OR (`entry`=60938 /*60938*/ AND `locale`='deDE') OR (`entry`=60946 /*60946*/ AND `locale`='deDE') OR (`entry`=60937 /*60937*/ AND `locale`='deDE') OR (`entry`=63649 /*63649*/ AND `locale`='deDE') OR (`entry`=63648 /*63648*/ AND `locale`='deDE') OR (`entry`=63647 /*63647*/ AND `locale`='deDE') OR (`entry`=63645 /*63645*/ AND `locale`='deDE') OR (`entry`=63612 /*63612*/ AND `locale`='deDE') OR (`entry`=63650 /*63650*/ AND `locale`='deDE') OR (`entry`=63646 /*63646*/ AND `locale`='deDE') OR (`entry`=63644 /*63644*/ AND `locale`='deDE') OR (`entry`=63643 /*63643*/ AND `locale`='deDE') OR (`entry`=68563 /*68563*/ AND `locale`='deDE') OR (`entry`=66464 /*66464*/ AND `locale`='deDE') OR (`entry`=68144 /*68144*/ AND `locale`='deDE') OR (`entry`=68005 /*68005*/ AND `locale`='deDE') OR (`entry`=60923 /*60923*/ AND `locale`='deDE') OR (`entry`=60910 /*60910*/ AND `locale`='deDE') OR (`entry`=61698 /*61698*/ AND `locale`='deDE') OR (`entry`=61571 /*61571*/ AND `locale`='deDE') OR (`entry`=59877 /*59877*/ AND `locale`='deDE') OR (`entry`=61600 /*61600*/ AND `locale`='deDE') OR (`entry`=61603 /*61603*/ AND `locale`='deDE') OR (`entry`=60572 /*60572*/ AND `locale`='deDE') OR (`entry`=60826 /*60826*/ AND `locale`='deDE') OR (`entry`=60524 /*60524*/ AND `locale`='deDE') OR (`entry`=60912 /*60912*/ AND `locale`='deDE') OR (`entry`=61802 /*61802*/ AND `locale`='deDE') OR (`entry`=64618 /*64618*/ AND `locale`='deDE') OR (`entry`=71529 /*71529*/ AND `locale`='deDE') OR (`entry`=60796 /*60796*/ AND `locale`='deDE') OR (`entry`=60795 /*60795*/ AND `locale`='deDE') OR (`entry`=65812 /*65812*/ AND `locale`='deDE') OR (`entry`=65811 /*65811*/ AND `locale`='deDE') OR (`entry`=68846 /*68846*/ AND `locale`='deDE') OR (`entry`=61096 /*61096*/ AND `locale`='deDE') OR (`entry`=65688 /*65688*/ AND `locale`='deDE') OR (`entry`=65808 /*65808*/ AND `locale`='deDE') OR (`entry`=61084 /*61084*/ AND `locale`='deDE') OR (`entry`=61078 /*61078*/ AND `locale`='deDE') OR (`entry`=61316 /*61316*/ AND `locale`='deDE') OR (`entry`=60079 /*60079*/ AND `locale`='deDE') OR (`entry`=60022 /*60022*/ AND `locale`='deDE') OR (`entry`=61815 /*61815*/ AND `locale`='deDE') OR (`entry`=61787 /*61787*/ AND `locale`='deDE') OR (`entry`=61799 /*61799*/ AND `locale`='deDE') OR (`entry`=61789 /*61789*/ AND `locale`='deDE') OR (`entry`=63422 /*63422*/ AND `locale`='deDE') OR (`entry`=60765 /*60765*/ AND `locale`='deDE') OR (`entry`=63177 /*63177*/ AND `locale`='deDE') OR (`entry`=63155 /*63155*/ AND `locale`='deDE') OR (`entry`=63393 /*63393*/ AND `locale`='deDE') OR (`entry`=61794 /*61794*/ AND `locale`='deDE') OR (`entry`=68774 /*68774*/ AND `locale`='deDE') OR (`entry`=68770 /*68770*/ AND `locale`='deDE') OR (`entry`=68769 /*68769*/ AND `locale`='deDE') OR (`entry`=60596 /*60596*/ AND `locale`='deDE') OR (`entry`=60503 /*60503*/ AND `locale`='deDE') OR (`entry`=60425 /*60425*/ AND `locale`='deDE') OR (`entry`=60423 /*60423*/ AND `locale`='deDE') OR (`entry`=59755 /*59755*/ AND `locale`='deDE') OR (`entry`=59413 /*59413*/ AND `locale`='deDE') OR (`entry`=62961 /*62961*/ AND `locale`='deDE') OR (`entry`=60420 /*60420*/ AND `locale`='deDE') OR (`entry`=60416 /*60416*/ AND `locale`='deDE') OR (`entry`=61848 /*61848*/ AND `locale`='deDE') OR (`entry`=60170 /*60170*/ AND `locale`='deDE') OR (`entry`=60598 /*60598*/ AND `locale`='deDE') OR (`entry`=59439 /*59439*/ AND `locale`='deDE') OR (`entry`=59438 /*59438*/ AND `locale`='deDE') OR (`entry`=60169 /*60169*/ AND `locale`='deDE') OR (`entry`=59419 /*59419*/ AND `locale`='deDE') OR (`entry`=59680 /*59680*/ AND `locale`='deDE') OR (`entry`=59282 /*59282*/ AND `locale`='deDE') OR (`entry`=60008 /*60008*/ AND `locale`='deDE') OR (`entry`=59894 /*59894*/ AND `locale`='deDE') OR (`entry`=60012 /*60012*/ AND `locale`='deDE') OR (`entry`=60027 /*60027*/ AND `locale`='deDE') OR (`entry`=60010 /*60010*/ AND `locale`='deDE') OR (`entry`=59409 /*59409*/ AND `locale`='deDE') OR (`entry`=59495 /*59495*/ AND `locale`='deDE') OR (`entry`=59412 /*59412*/ AND `locale`='deDE') OR (`entry`=59410 /*59410*/ AND `locale`='deDE') OR (`entry`=60593 /*60593*/ AND `locale`='deDE') OR (`entry`=69943 /*69943*/ AND `locale`='deDE') OR (`entry`=58500 /*58500*/ AND `locale`='deDE') OR (`entry`=63550 /*63550*/ AND `locale`='deDE') OR (`entry`=4357 /*4357*/ AND `locale`='deDE') OR (`entry`=113845 /*113845*/ AND `locale`='deDE') OR (`entry`=106321 /*106321*/ AND `locale`='deDE') OR (`entry`=106319 /*106319*/ AND `locale`='deDE') OR (`entry`=106317 /*106317*/ AND `locale`='deDE') OR (`entry`=102392 /*102392*/ AND `locale`='deDE') OR (`entry`=60921 /*60921*/ AND `locale`='deDE') OR (`entry`=55605 /*55605*/ AND `locale`='deDE') OR (`entry`=61641 /*61641*/ AND `locale`='deDE') OR (`entry`=60825 /*60825*/ AND `locale`='deDE') OR (`entry`=65629 /*65629*/ AND `locale`='deDE') OR (`entry`=59967 /*59967*/ AND `locale`='deDE') OR (`entry`=59693 /*59693*/ AND `locale`='deDE') OR (`entry`=59958 /*59958*/ AND `locale`='deDE') OR (`entry`=59408 /*59408*/ AND `locale`='deDE') OR (`entry`=59424 /*59424*/ AND `locale`='deDE') OR (`entry`=59427 /*59427*/ AND `locale`='deDE') OR (`entry`=59920 /*59920*/ AND `locale`='deDE') OR (`entry`=60498 /*60498*/ AND `locale`='deDE') OR (`entry`=59814 /*59814*/ AND `locale`='deDE') OR (`entry`=59456 /*59456*/ AND `locale`='deDE') OR (`entry`=59430 /*59430*/ AND `locale`='deDE') OR (`entry`=66197 /*66197*/ AND `locale`='deDE') OR (`entry`=66191 /*66191*/ AND `locale`='deDE') OR (`entry`=59703 /*59703*/ AND `locale`='deDE') OR (`entry`=59414 /*59414*/ AND `locale`='deDE') OR (`entry`=59802 /*59802*/ AND `locale`='deDE') OR (`entry`=66186 /*66186*/ AND `locale`='deDE') OR (`entry`=59436 /*59436*/ AND `locale`='deDE') OR (`entry`=59435 /*59435*/ AND `locale`='deDE') OR (`entry`=59803 /*59803*/ AND `locale`='deDE') OR (`entry`=59984 /*59984*/ AND `locale`='deDE') OR (`entry`=59805 /*59805*/ AND `locale`='deDE') OR (`entry`=60879 /*60879*/ AND `locale`='deDE') OR (`entry`=59817 /*59817*/ AND `locale`='deDE') OR (`entry`=59815 /*59815*/ AND `locale`='deDE') OR (`entry`=60550 /*60550*/ AND `locale`='deDE') OR (`entry`=59556 /*59556*/ AND `locale`='deDE') OR (`entry`=59897 /*59897*/ AND `locale`='deDE') OR (`entry`=59896 /*59896*/ AND `locale`='deDE') OR (`entry`=59416 /*59416*/ AND `locale`='deDE') OR (`entry`=59898 /*59898*/ AND `locale`='deDE') OR (`entry`=66380 /*66380*/ AND `locale`='deDE') OR (`entry`=63447 /*63447*/ AND `locale`='deDE') OR (`entry`=60925 /*60925*/ AND `locale`='deDE') OR (`entry`=63562 /*63562*/ AND `locale`='deDE') OR (`entry`=63556 /*63556*/ AND `locale`='deDE') OR (`entry`=64965 /*64965*/ AND `locale`='deDE') OR (`entry`=63610 /*63610*/ AND `locale`='deDE') OR (`entry`=34337 /*34337*/ AND `locale`='deDE') OR (`entry`=63674 /*63674*/ AND `locale`='deDE') OR (`entry`=63611 /*63611*/ AND `locale`='deDE') OR (`entry`=67836 /*67836*/ AND `locale`='deDE') OR (`entry`=67833 /*67833*/ AND `locale`='deDE') OR (`entry`=67835 /*67835*/ AND `locale`='deDE') OR (`entry`=65134 /*65134*/ AND `locale`='deDE') OR (`entry`=64852 /*64852*/ AND `locale`='deDE') OR (`entry`=63572 /*63572*/ AND `locale`='deDE') OR (`entry`=65170 /*65170*/ AND `locale`='deDE') OR (`entry`=63575 /*63575*/ AND `locale`='deDE') OR (`entry`=63573 /*63573*/ AND `locale`='deDE') OR (`entry`=65133 /*65133*/ AND `locale`='deDE') OR (`entry`=61519 /*61519*/ AND `locale`='deDE') OR (`entry`=61520 /*61520*/ AND `locale`='deDE') OR (`entry`=62877 /*62877*/ AND `locale`='deDE') OR (`entry`=60163 /*60163*/ AND `locale`='deDE') OR (`entry`=60161 /*60161*/ AND `locale`='deDE') OR (`entry`=65580 /*65580*/ AND `locale`='deDE') OR (`entry`=61473 /*61473*/ AND `locale`='deDE') OR (`entry`=61252 /*61252*/ AND `locale`='deDE') OR (`entry`=60164 /*60164*/ AND `locale`='deDE') OR (`entry`=60178 /*60178*/ AND `locale`='deDE') OR (`entry`=60189 /*60189*/ AND `locale`='deDE') OR (`entry`=60190 /*60190*/ AND `locale`='deDE') OR (`entry`=60187 /*60187*/ AND `locale`='deDE') OR (`entry`=61224 /*61224*/ AND `locale`='deDE') OR (`entry`=68241 /*68241*/ AND `locale`='deDE') OR (`entry`=61303 /*61303*/ AND `locale`='deDE') OR (`entry`=60288 /*60288*/ AND `locale`='deDE') OR (`entry`=60099 /*60099*/ AND `locale`='deDE') OR (`entry`=60127 /*60127*/ AND `locale`='deDE') OR (`entry`=60032 /*60032*/ AND `locale`='deDE') OR (`entry`=60098 /*60098*/ AND `locale`='deDE') OR (`entry`=65774 /*65774*/ AND `locale`='deDE') OR (`entry`=65773 /*65773*/ AND `locale`='deDE') OR (`entry`=60031 /*60031*/ AND `locale`='deDE') OR (`entry`=60030 /*60030*/ AND `locale`='deDE') OR (`entry`=60096 /*60096*/ AND `locale`='deDE') OR (`entry`=59968 /*59968*/ AND `locale`='deDE') OR (`entry`=59969 /*59969*/ AND `locale`='deDE') OR (`entry`=59972 /*59972*/ AND `locale`='deDE') OR (`entry`=59970 /*59970*/ AND `locale`='deDE') OR (`entry`=31047 /*31047*/ AND `locale`='deDE') OR (`entry`=65440 /*65440*/ AND `locale`='deDE') OR (`entry`=59797 /*59797*/ AND `locale`='deDE') OR (`entry`=59773 /*59773*/ AND `locale`='deDE') OR (`entry`=59758 /*59758*/ AND `locale`='deDE') OR (`entry`=61488 /*61488*/ AND `locale`='deDE') OR (`entry`=61489 /*61489*/ AND `locale`='deDE') OR (`entry`=61490 /*61490*/ AND `locale`='deDE') OR (`entry`=61475 /*61475*/ AND `locale`='deDE') OR (`entry`=59790 /*59790*/ AND `locale`='deDE') OR (`entry`=59826 /*59826*/ AND `locale`='deDE') OR (`entry`=59538 /*59538*/ AND `locale`='deDE') OR (`entry`=60195 /*60195*/ AND `locale`='deDE') OR (`entry`=59593 /*59593*/ AND `locale`='deDE') OR (`entry`=59527 /*59527*/ AND `locale`='deDE') OR (`entry`=59526 /*59526*/ AND `locale`='deDE') OR (`entry`=59443 /*59443*/ AND `locale`='deDE') OR (`entry`=59697 /*59697*/ AND `locale`='deDE') OR (`entry`=60093 /*60093*/ AND `locale`='deDE') OR (`entry`=61504 /*61504*/ AND `locale`='deDE') OR (`entry`=59731 /*59731*/ AND `locale`='deDE') OR (`entry`=59716 /*59716*/ AND `locale`='deDE') OR (`entry`=59698 /*59698*/ AND `locale`='deDE') OR (`entry`=59688 /*59688*/ AND `locale`='deDE') OR (`entry`=59696 /*59696*/ AND `locale`='deDE') OR (`entry`=59699 /*59699*/ AND `locale`='deDE') OR (`entry`=62108 /*62108*/ AND `locale`='deDE') OR (`entry`=61847 /*61847*/ AND `locale`='deDE') OR (`entry`=65416 /*65416*/ AND `locale`='deDE') OR (`entry`=59691 /*59691*/ AND `locale`='deDE') OR (`entry`=62158 /*62158*/ AND `locale`='deDE') OR (`entry`=59827 /*59827*/ AND `locale`='deDE') OR (`entry`=62149 /*62149*/ AND `locale`='deDE') OR (`entry`=60508 /*60508*/ AND `locale`='deDE') OR (`entry`=59701 /*59701*/ AND `locale`='deDE') OR (`entry`=59695 /*59695*/ AND `locale`='deDE') OR (`entry`=60860 /*60860*/ AND `locale`='deDE') OR (`entry`=59307 /*59307*/ AND `locale`='deDE') OR (`entry`=59859 /*59859*/ AND `locale`='deDE') OR (`entry`=59858 /*59858*/ AND `locale`='deDE') OR (`entry`=59578 /*59578*/ AND `locale`='deDE') OR (`entry`=60523 /*60523*/ AND `locale`='deDE') OR (`entry`=64516 /*64516*/ AND `locale`='deDE') OR (`entry`=64514 /*64514*/ AND `locale`='deDE') OR (`entry`=64515 /*64515*/ AND `locale`='deDE') OR (`entry`=66415 /*66415*/ AND `locale`='deDE') OR (`entry`=59405 /*59405*/ AND `locale`='deDE') OR (`entry`=59407 /*59407*/ AND `locale`='deDE') OR (`entry`=70155 /*70155*/ AND `locale`='deDE') OR (`entry`=64882 /*64882*/ AND `locale`='deDE') OR (`entry`=64518 /*64518*/ AND `locale`='deDE') OR (`entry`=59509 /*59509*/ AND `locale`='deDE') OR (`entry`=59371 /*59371*/ AND `locale`='deDE') OR (`entry`=65121 /*65121*/ AND `locale`='deDE') OR (`entry`=61494 /*61494*/ AND `locale`='deDE') OR (`entry`=61493 /*61493*/ AND `locale`='deDE') OR (`entry`=59406 /*59406*/ AND `locale`='deDE') OR (`entry`=59403 /*59403*/ AND `locale`='deDE') OR (`entry`=59402 /*59402*/ AND `locale`='deDE') OR (`entry`=71082 /*71082*/ AND `locale`='deDE') OR (`entry`=59597 /*59597*/ AND `locale`='deDE') OR (`entry`=59576 /*59576*/ AND `locale`='deDE') OR (`entry`=63372 /*63372*/ AND `locale`='deDE') OR (`entry`=103863 /*103863*/ AND `locale`='deDE') OR (`entry`=60875 /*60875*/ AND `locale`='deDE') OR (`entry`=64791 /*64791*/ AND `locale`='deDE') OR (`entry`=59540 /*59540*/ AND `locale`='deDE') OR (`entry`=64411 /*64411*/ AND `locale`='deDE') OR (`entry`=64412 /*64412*/ AND `locale`='deDE') OR (`entry`=63362 /*63362*/ AND `locale`='deDE') OR (`entry`=64726 /*64726*/ AND `locale`='deDE') OR (`entry`=64444 /*64444*/ AND `locale`='deDE') OR (`entry`=67787 /*67787*/ AND `locale`='deDE') OR (`entry`=61541 /*61541*/ AND `locale`='deDE') OR (`entry`=64642 /*64642*/ AND `locale`='deDE') OR (`entry`=64638 /*64638*/ AND `locale`='deDE') OR (`entry`=64639 /*64639*/ AND `locale`='deDE') OR (`entry`=64643 /*64643*/ AND `locale`='deDE') OR (`entry`=64631 /*64631*/ AND `locale`='deDE') OR (`entry`=64551 /*64551*/ AND `locale`='deDE') OR (`entry`=59481 /*59481*/ AND `locale`='deDE') OR (`entry`=64693 /*64693*/ AND `locale`='deDE') OR (`entry`=63673 /*63673*/ AND `locale`='deDE') OR (`entry`=60434 /*60434*/ AND `locale`='deDE') OR (`entry`=60800 /*60800*/ AND `locale`='deDE') OR (`entry`=60767 /*60767*/ AND `locale`='deDE') OR (`entry`=66570 /*66570*/ AND `locale`='deDE') OR (`entry`=60560 /*60560*/ AND `locale`='deDE') OR (`entry`=60846 /*60846*/ AND `locale`='deDE') OR (`entry`=60694 /*60694*/ AND `locale`='deDE') OR (`entry`=63723 /*63723*/ AND `locale`='deDE') OR (`entry`=60670 /*60670*/ AND `locale`='deDE') OR (`entry`=64725 /*64725*/ AND `locale`='deDE') OR (`entry`=63672 /*63672*/ AND `locale`='deDE') OR (`entry`=66472 /*66472*/ AND `locale`='deDE') OR (`entry`=64436 /*64436*/ AND `locale`='deDE') OR (`entry`=66658 /*66658*/ AND `locale`='deDE') OR (`entry`=60437 /*60437*/ AND `locale`='deDE') OR (`entry`=64203 /*64203*/ AND `locale`='deDE') OR (`entry`=66707 /*66707*/ AND `locale`='deDE') OR (`entry`=45979 /*45979*/ AND `locale`='deDE') OR (`entry`=60580 /*60580*/ AND `locale`='deDE') OR (`entry`=64202 /*64202*/ AND `locale`='deDE') OR (`entry`=60581 /*60581*/ AND `locale`='deDE') OR (`entry`=65428 /*65428*/ AND `locale`='deDE') OR (`entry`=66261 /*66261*/ AND `locale`='deDE') OR (`entry`=66169 /*66169*/ AND `locale`='deDE') OR (`entry`=61498 /*61498*/ AND `locale`='deDE') OR (`entry`=61495 /*61495*/ AND `locale`='deDE') OR (`entry`=60605 /*60605*/ AND `locale`='deDE') OR (`entry`=64215 /*64215*/ AND `locale`='deDE') OR (`entry`=64214 /*64214*/ AND `locale`='deDE') OR (`entry`=62971 /*62971*/ AND `locale`='deDE') OR (`entry`=62970 /*62970*/ AND `locale`='deDE') OR (`entry`=61371 /*61371*/ AND `locale`='deDE') OR (`entry`=61545 /*61545*/ AND `locale`='deDE') OR (`entry`=60436 /*60436*/ AND `locale`='deDE') OR (`entry`=61503 /*61503*/ AND `locale`='deDE') OR (`entry`=65884 /*65884*/ AND `locale`='deDE') OR (`entry`=64836 /*64836*/ AND `locale`='deDE') OR (`entry`=61496 /*61496*/ AND `locale`='deDE') OR (`entry`=64830 /*64830*/ AND `locale`='deDE') OR (`entry`=66182 /*66182*/ AND `locale`='deDE') OR (`entry`=64070 /*64070*/ AND `locale`='deDE') OR (`entry`=64069 /*64069*/ AND `locale`='deDE') OR (`entry`=66165 /*66165*/ AND `locale`='deDE') OR (`entry`=61511 /*61511*/ AND `locale`='deDE') OR (`entry`=61417 /*61417*/ AND `locale`='deDE') OR (`entry`=61381 /*61381*/ AND `locale`='deDE') OR (`entry`=66174 /*66174*/ AND `locale`='deDE') OR (`entry`=66349 /*66349*/ AND `locale`='deDE') OR (`entry`=60804 /*60804*/ AND `locale`='deDE') OR (`entry`=66164 /*66164*/ AND `locale`='deDE') OR (`entry`=64745 /*64745*/ AND `locale`='deDE') OR (`entry`=63363 /*63363*/ AND `locale`='deDE') OR (`entry`=64790 /*64790*/ AND `locale`='deDE') OR (`entry`=61380 /*61380*/ AND `locale`='deDE') OR (`entry`=61379 /*61379*/ AND `locale`='deDE') OR (`entry`=63355 /*63355*/ AND `locale`='deDE') OR (`entry`=65423 /*65423*/ AND `locale`='deDE') OR (`entry`=64248 /*64248*/ AND `locale`='deDE') OR (`entry`=60866 /*60866*/ AND `locale`='deDE') OR (`entry`=66891 /*66891*/ AND `locale`='deDE') OR (`entry`=61624 /*61624*/ AND `locale`='deDE') OR (`entry`=61118 /*61118*/ AND `locale`='deDE') OR (`entry`=61651 /*61651*/ AND `locale`='deDE') OR (`entry`=61119 /*61119*/ AND `locale`='deDE') OR (`entry`=64552 /*64552*/ AND `locale`='deDE') OR (`entry`=72536 /*72536*/ AND `locale`='deDE') OR (`entry`=64528 /*64528*/ AND `locale`='deDE') OR (`entry`=64542 /*64542*/ AND `locale`='deDE') OR (`entry`=64540 /*64540*/ AND `locale`='deDE') OR (`entry`=64537 /*64537*/ AND `locale`='deDE') OR (`entry`=64536 /*64536*/ AND `locale`='deDE') OR (`entry`=61212 /*61212*/ AND `locale`='deDE') OR (`entry`=61531 /*61531*/ AND `locale`='deDE') OR (`entry`=61529 /*61529*/ AND `locale`='deDE') OR (`entry`=61211 /*61211*/ AND `locale`='deDE') OR (`entry`=60168 /*60168*/ AND `locale`='deDE') OR (`entry`=60431 /*60431*/ AND `locale`='deDE') OR (`entry`=60475 /*60475*/ AND `locale`='deDE') OR (`entry`=60458 /*60458*/ AND `locale`='deDE') OR (`entry`=59420 /*59420*/ AND `locale`='deDE') OR (`entry`=60478 /*60478*/ AND `locale`='deDE') OR (`entry`=60479 /*60479*/ AND `locale`='deDE') OR (`entry`=65875 /*65875*/ AND `locale`='deDE') OR (`entry`=59534 /*59534*/ AND `locale`='deDE') OR (`entry`=60459 /*60459*/ AND `locale`='deDE') OR (`entry`=59656 /*59656*/ AND `locale`='deDE') OR (`entry`=115146 /*115146*/ AND `locale`='deDE') OR (`entry`=59385 /*59385*/ AND `locale`='deDE') OR (`entry`=59353 /*59353*/ AND `locale`='deDE') OR (`entry`=60482 /*60482*/ AND `locale`='deDE') OR (`entry`=59382 /*59382*/ AND `locale`='deDE') OR (`entry`=59367 /*59367*/ AND `locale`='deDE') OR (`entry`=59339 /*59339*/ AND `locale`='deDE') OR (`entry`=59718 /*59718*/ AND `locale`='deDE') OR (`entry`=417 /*417*/ AND `locale`='deDE') OR (`entry`=90202 /*90202*/ AND `locale`='deDE') OR (`entry`=60028 /*60028*/ AND `locale`='deDE') OR (`entry`=59821 /*59821*/ AND `locale`='deDE') OR (`entry`=59486 /*59486*/ AND `locale`='deDE') OR (`entry`=24021 /*24021*/ AND `locale`='deDE') OR (`entry`=59679 /*59679*/ AND `locale`='deDE') OR (`entry`=59573 /*59573*/ AND `locale`='deDE') OR (`entry`=59636 /*59636*/ AND `locale`='deDE') OR (`entry`=59655 /*59655*/ AND `locale`='deDE') OR (`entry`=59702 /*59702*/ AND `locale`='deDE') OR (`entry`=63850 /*63850*/ AND `locale`='deDE') OR (`entry`=63027 /*63027*/ AND `locale`='deDE') OR (`entry`=65204 /*65204*/ AND `locale`='deDE') OR (`entry`=59341 /*59341*/ AND `locale`='deDE') OR (`entry`=59157 /*59157*/ AND `locale`='deDE') OR (`entry`=58472 /*58472*/ AND `locale`='deDE') OR (`entry`=58471 /*58471*/ AND `locale`='deDE') OR (`entry`=58470 /*58470*/ AND `locale`='deDE') OR (`entry`=58467 /*58467*/ AND `locale`='deDE') OR (`entry`=58466 /*58466*/ AND `locale`='deDE') OR (`entry`=58465 /*58465*/ AND `locale`='deDE') OR (`entry`=58408 /*58408*/ AND `locale`='deDE') OR (`entry`=58469 /*58469*/ AND `locale`='deDE') OR (`entry`=58468 /*58468*/ AND `locale`='deDE') OR (`entry`=65976 /*65976*/ AND `locale`='deDE') OR (`entry`=58818 /*58818*/ AND `locale`='deDE') OR (`entry`=58695 /*58695*/ AND `locale`='deDE') OR (`entry`=65762 /*65762*/ AND `locale`='deDE') OR (`entry`=64794 /*64794*/ AND `locale`='deDE') OR (`entry`=59645 /*59645*/ AND `locale`='deDE') OR (`entry`=64848 /*64848*/ AND `locale`='deDE') OR (`entry`=65928 /*65928*/ AND `locale`='deDE') OR (`entry`=59580 /*59580*/ AND `locale`='deDE') OR (`entry`=59577 /*59577*/ AND `locale`='deDE') OR (`entry`=64796 /*64796*/ AND `locale`='deDE') OR (`entry`=63557 /*63557*/ AND `locale`='deDE') OR (`entry`=63558 /*63558*/ AND `locale`='deDE') OR (`entry`=108452 /*108452*/ AND `locale`='deDE') OR (`entry`=89 /*89*/ AND `locale`='deDE') OR (`entry`=94584 /*94584*/ AND `locale`='deDE') OR (`entry`=99887 /*99887*/ AND `locale`='deDE') OR (`entry`=61070 /*61070*/ AND `locale`='deDE') OR (`entry`=63555 /*63555*/ AND `locale`='deDE') OR (`entry`=59384 /*59384*/ AND `locale`='deDE') OR (`entry`=59148 /*59148*/ AND `locale`='deDE') OR (`entry`=64795 /*64795*/ AND `locale`='deDE') OR (`entry`=59083 /*59083*/ AND `locale`='deDE') OR (`entry`=61749 /*61749*/ AND `locale`='deDE') OR (`entry`=59076 /*59076*/ AND `locale`='deDE') OR (`entry`=58989 /*58989*/ AND `locale`='deDE') OR (`entry`=59147 /*59147*/ AND `locale`='deDE') OR (`entry`=59523 /*59523*/ AND `locale`='deDE') OR (`entry`=58961 /*58961*/ AND `locale`='deDE') OR (`entry`=58956 /*58956*/ AND `locale`='deDE') OR (`entry`=59143 /*59143*/ AND `locale`='deDE') OR (`entry`=59021 /*59021*/ AND `locale`='deDE') OR (`entry`=68564 /*68564*/ AND `locale`='deDE') OR (`entry`=63547 /*63547*/ AND `locale`='deDE') OR (`entry`=59670 /*59670*/ AND `locale`='deDE') OR (`entry`=61069 /*61069*/ AND `locale`='deDE') OR (`entry`=59394 /*59394*/ AND `locale`='deDE') OR (`entry`=65761 /*65761*/ AND `locale`='deDE') OR (`entry`=35114 /*35114*/ AND `locale`='deDE') OR (`entry`=16445 /*16445*/ AND `locale`='deDE') OR (`entry`=59671 /*59671*/ AND `locale`='deDE') OR (`entry`=59685 /*59685*/ AND `locale`='deDE') OR (`entry`=64797 /*64797*/ AND `locale`='deDE') OR (`entry`=59335 /*59335*/ AND `locale`='deDE') OR (`entry`=59672 /*59672*/ AND `locale`='deDE') OR (`entry`=65839 /*65839*/ AND `locale`='deDE') OR (`entry`=61874 /*61874*/ AND `locale`='deDE') OR (`entry`=59319 /*59319*/ AND `locale`='deDE') OR (`entry`=55593 /*55593*/ AND `locale`='deDE') OR (`entry`=59272 /*59272*/ AND `locale`='deDE') OR (`entry`=59181 /*59181*/ AND `locale`='deDE') OR (`entry`=59180 /*59180*/ AND `locale`='deDE') OR (`entry`=60995 /*60995*/ AND `locale`='deDE') OR (`entry`=60491 /*60491*/ AND `locale`='deDE') OR (`entry`=61130 /*61130*/ AND `locale`='deDE') OR (`entry`=61566 /*61566*/ AND `locale`='deDE') OR (`entry`=59165 /*59165*/ AND `locale`='deDE') OR (`entry`=59166 /*59166*/ AND `locale`='deDE') OR (`entry`=61500 /*61500*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(62383, 'deDE', 'Herrenloser Drachen', NULL, NULL, 'taxi', 23222), -- 62383
(66700, 'deDE', 'Wahrsager der Zandalari', NULL, NULL, NULL, 23222), -- 66700
(61061, 'deDE', 'Tiger Temple General Purpose Bunny', NULL, NULL, NULL, 23222), -- 61061
(60944, 'deDE', 'Ausbilder Ko', NULL, 'Tigerstil', NULL, 23222), -- 60944
(93814, 'deDE', 'Blorp', NULL, NULL, NULL, 23222), -- 93814
(66360, 'deDE', 'Meister Brandom', NULL, 'Bergbaulehrer', NULL, 23222), -- 66360
(64975, 'deDE', 'Eisenkörper Ponshu', NULL, 'Mönchslehrer - Meisterochse', 'trainer', 23222), -- 64975
(64978, 'deDE', 'Jia Nummer Neun', NULL, 'Mönchslehrerin - Meisterkranich', 'trainer', 23222), -- 64978
(61068, 'deDE', 'Kranichstilmönch', 'Kranichstilmönch', NULL, NULL, 23222), -- 61068
(66260, 'deDE', 'Meister Hight', NULL, 'Großmeister', NULL, 23222), -- 66260
(66258, 'deDE', 'Meister Cheng', NULL, NULL, NULL, 23222), -- 66258
(66987, 'deDE', 'Sandsack', NULL, NULL, NULL, 23222), -- 66987
(66986, 'deDE', 'Auszubildender Mönch', 'Auszubildender Mönch', NULL, NULL, 23222), -- 66986
(66207, 'deDE', 'Meister Hsu', NULL, NULL, NULL, 23222), -- 66207
(67009, 'deDE', 'Auszubildender Mönch', 'Auszubildender Mönch', NULL, NULL, 23222), -- 67009
(64980, 'deDE', 'Weise Lehrmeisterin Lianji', NULL, 'Mönchslehrerin - Meisterschlange', 'trainer', 23222), -- 64980
(60443, 'deDE', 'Jadeavatar', NULL, NULL, NULL, 23222), -- 60443
(32642, 'deDE', 'Mojodishu', NULL, 'Handelsreisende', NULL, 23222), -- 32642
(32641, 'deDE', 'Drix Finsterzang', NULL, 'Der Reparaturmeister', NULL, 23222), -- 32641
(66943, 'deDE', 'Schlangenstatue', NULL, NULL, NULL, 23222), -- 66943
(66253, 'deDE', 'Meisterin Kistane', NULL, NULL, NULL, 23222), -- 66253
(66257, 'deDE', 'Meister Tsang', NULL, NULL, NULL, 23222), -- 66257
(67095, 'deDE', 'Meister Yeoh', NULL, NULL, NULL, 23222), -- 67095
(67092, 'deDE', 'Meister Chow', NULL, NULL, NULL, 23222), -- 67092
(66946, 'deDE', 'Balancierpfahl', NULL, NULL, 'vehichleCursor', 23222), -- 66946
(66947, 'deDE', 'Auszubildender Mönch', 'Auszubildender Mönch', NULL, NULL, 23222), -- 66947
(67066, 'deDE', 'Auszubildender Mönch', 'Auszubildender Mönch', NULL, NULL, 23222), -- 67066
(67015, 'deDE', 'Auszubildender Mönch', 'Auszubildender Mönch', NULL, NULL, 23222), -- 67015
(66254, 'deDE', 'Meisterin Woo', NULL, NULL, NULL, 23222), -- 66254
(67021, 'deDE', 'Auszubildender Mönch', 'Auszubildender Mönch', NULL, NULL, 23222), -- 67021
(66255, 'deDE', 'Meister Yoon', NULL, NULL, NULL, 23222), -- 66255
(66969, 'deDE', 'Sandsack', NULL, NULL, NULL, 23222), -- 66969
(66356, 'deDE', 'Meister Hwang', NULL, 'Stabhändler', NULL, 23222), -- 66356
(70470, 'deDE', 'Feng Zhe', NULL, 'Der Akrobat', NULL, 23222), -- 70470
(66359, 'deDE', 'Meister Tan', NULL, 'Händler für Faustwaffen', NULL, 23222), -- 66359
(64974, 'deDE', 'Jang der Fürchterliche', NULL, 'Mönchslehrer - Meistertiger', 'trainer', 23222), -- 64974
(60945, 'deDE', 'Ausbilderin Lin', NULL, 'Tigerstil', NULL, 23222), -- 60945
(60936, 'deDE', 'Tigerstilmönch', 'Tigerstilmönch', NULL, NULL, 23222), -- 60936
(66358, 'deDE', 'Meister Lo', NULL, 'Angler', NULL, 23222), -- 66358
(66357, 'deDE', 'Meister Bahre', NULL, 'Lehrer für Erste Hilfe', NULL, 23222), -- 66357
(66353, 'deDE', 'Meisterin Chang', NULL, 'Braumeisterin', NULL, 23222), -- 66353
(60930, 'deDE', 'Aufstrebender Mönch', 'Aufstrebender Mönch', NULL, NULL, 23222), -- 60930
(66355, 'deDE', 'Meister Marschall', NULL, 'Botaniker', NULL, 23222), -- 66355
(66354, 'deDE', 'Meister Kanone', NULL, 'Gerber', NULL, 23222), -- 66354
(61060, 'deDE', 'Schlangenstilmönch', 'Schlangenstilmönch', NULL, NULL, 23222), -- 61060
(65689, 'deDE', 'Bergsteigerseil', NULL, NULL, NULL, 23222), -- 65689
(42720, 'deDE', 'Junger Weitschreiter', NULL, NULL, NULL, 23222), -- 42720
(64267, 'deDE', 'Tornado', NULL, NULL, NULL, 23222), -- 64267
(64227, 'deDE', 'Eingefrorener Packer des Jutepfades', NULL, NULL, NULL, 23222), -- 64227
(60493, 'deDE', 'Kafa-berauschte Bergziege', NULL, NULL, NULL, 23222), -- 60493
(66738, 'deDE', 'Mutiger Yon', NULL, 'Großmeistertierzähmer', NULL, 23222), -- 66738
(66721, 'deDE', 'Piqua', NULL, NULL, NULL, 23222), -- 66721
(66720, 'deDE', 'Schmaus', NULL, NULL, NULL, 23222), -- 66720
(66719, 'deDE', 'Blök', NULL, NULL, NULL, 23222), -- 66719
(71942, 'deDE', 'Xu-Fu, Nachwuchs von Xuen', NULL, NULL, NULL, 23222), -- 71942
(63503, 'deDE', 'Kafa-berauschte Bergziege', NULL, NULL, NULL, 23222), -- 63503
(60564, 'deDE', 'Kafa-berauschter Yeti', NULL, NULL, NULL, 23222), -- 60564
(61819, 'deDE', 'Ban Bärenherz', NULL, NULL, NULL, 23222), -- 61819
(61820, 'deDE', 'Lao-Chen der eiserne Bauch', NULL, NULL, NULL, 23222), -- 61820
(61816, 'deDE', 'Lin Stillstoß', NULL, NULL, NULL, 23222), -- 61816
(61454, 'deDE', 'Suna Stillstoß', NULL, NULL, NULL, 23222), -- 61454
(61512, 'deDE', 'Drachenmeister Len', NULL, 'Flugmeister', NULL, 23222), -- 61512
(63603, 'deDE', 'Bans Ballon', NULL, NULL, NULL, 23222), -- 63603
(61808, 'deDE', 'Shado-Meister Zhiyao', NULL, NULL, 'cast', 23222), -- 61808
(63564, 'deDE', 'Shado-Pan-Torwächter', 'Shado-Pan-Torwächterin', NULL, NULL, 23222), -- 63564
(61806, 'deDE', 'Liu Tausendschlag', NULL, NULL, 'cast', 23222), -- 61806
(61810, 'deDE', 'Shiya Scharfklinge', NULL, NULL, 'cast', 23222), -- 61810
(60455, 'deDE', 'Eindringling von Osul', NULL, NULL, NULL, 23222), -- 60455
(61843, 'deDE', 'Kun-Lai-Leichenhacker', NULL, NULL, NULL, 23222), -- 61843
(63576, 'deDE', 'Feuerkrieger von Osul', NULL, NULL, 'taxi', 23222), -- 63576
(66289, 'deDE', 'Feuerspucker von Osul', NULL, NULL, NULL, 23222), -- 66289
(60730, 'deDE', 'Longying-Waldläufer', NULL, NULL, NULL, 23222), -- 60730
(60705, 'deDE', 'Balliste von Osul', NULL, NULL, NULL, 23222), -- 60705
(60692, 'deDE', 'Marodeur von Osul', NULL, NULL, NULL, 23222), -- 60692
(63549, 'deDE', 'Graslandhüpfer', NULL, NULL, 'wildpetcapturable', 23222), -- 63549
(62608, 'deDE', 'Pechschleuderer von Gai-Cho', NULL, NULL, NULL, 23222), -- 62608
(62554, 'deDE', 'Cheng Bo', NULL, 'Fürst von Gai-Cho', NULL, 23222), -- 62554
(64802, 'deDE', 'Yakratte', NULL, NULL, NULL, 23222), -- 64802
(64801, 'deDE', 'Graslandhüpfer', NULL, NULL, NULL, 23222), -- 64801
(62553, 'deDE', 'Erdsprecher von Gai-Cho', NULL, NULL, NULL, 23222), -- 62553
(62552, 'deDE', 'Yaungol von Gai-Cho', NULL, NULL, NULL, 23222), -- 62552
(66476, 'deDE', 'Riffzwicker', NULL, NULL, NULL, 23222), -- 66476
(66471, 'deDE', 'Ausgeburt von G''nathus', NULL, NULL, NULL, 23222), -- 66471
(64434, 'deDE', 'Grohl Grohl', NULL, 'Obertrommler', NULL, 23222), -- 64434
(60939, 'deDE', 'Shado-Pan-Krieger', NULL, NULL, NULL, 23222), -- 60939
(60938, 'deDE', 'Shado-Pan-Hüter', NULL, NULL, NULL, 23222), -- 60938
(60946, 'deDE', 'Kaltwasserkarpfen', NULL, NULL, NULL, 23222), -- 60946
(60937, 'deDE', 'Shado-Pan-Wächter', 'Shado-Pan-Wächterin', NULL, NULL, 23222), -- 60937
(63649, 'deDE', 'Holzlöffel', NULL, NULL, NULL, 23222), -- 63649
(63648, 'deDE', 'Mushanpastete', NULL, NULL, NULL, 23222), -- 63648
(63647, 'deDE', 'Halb aufgegessener Fisch', NULL, NULL, NULL, 23222), -- 63647
(63645, 'deDE', 'Nie schmelzender Eiszapfen', NULL, NULL, NULL, 23222), -- 63645
(63612, 'deDE', 'Schneeschuh', NULL, NULL, NULL, 23222), -- 63612
(63650, 'deDE', 'Deprimierter Grummelhändler', NULL, NULL, NULL, 23222), -- 63650
(63646, 'deDE', 'Falkenschwanz', NULL, NULL, NULL, 23222), -- 63646
(63644, 'deDE', 'Goldener Schnee', NULL, NULL, NULL, 23222), -- 63644
(63643, 'deDE', 'Wächter des Shado-Pan-Klosters', NULL, NULL, NULL, 23222), -- 63643
(68563, 'deDE', 'Kafi', NULL, NULL, 'wildpet', 23222), -- 68563
(66464, 'deDE', 'Zhing', NULL, NULL, NULL, 23222), -- 66464
(68144, 'deDE', 'Der Affenkönig', NULL, NULL, NULL, 23222), -- 68144
(68005, 'deDE', 'Der Jadekriegsfürst', NULL, NULL, NULL, 23222), -- 68005
(60923, 'deDE', '"Strange Happenings" - Book Credit', NULL, NULL, NULL, 23222), -- 60923
(60910, 'deDE', 'Tombs Flavor Bunny', NULL, NULL, NULL, 23222), -- 60910
(61698, 'deDE', 'Gefrorener Grabwächter', NULL, NULL, NULL, 23222), -- 61698
(61571, 'deDE', 'Imperialer Seelenernter', NULL, NULL, NULL, 23222), -- 61571
(59877, 'deDE', 'Erfahrener Bergführer', NULL, NULL, NULL, 23222), -- 59877
(61600, 'deDE', 'Nomade der Zandalari', NULL, NULL, NULL, 23222), -- 61600
(61603, 'deDE', 'Kaiser Rikktik', NULL, NULL, NULL, 23222), -- 61603
(60572, 'deDE', 'Nakk''rakas', NULL, 'Der Geistformer', NULL, 23222), -- 60572
(60826, 'deDE', 'Kaiserliche Wache', NULL, 'Der Schild des Kaisers', NULL, 23222), -- 60826
(60524, 'deDE', 'Schattenjäger der Zandalari', NULL, NULL, NULL, 23222), -- 60524
(60912, 'deDE', 'Grabwächter', NULL, NULL, NULL, 23222), -- 60912
(61802, 'deDE', 'Jump Landing Bunny 1', NULL, NULL, NULL, 23222), -- 61802
(64618, 'deDE', 'Tier', NULL, NULL, NULL, 23222), -- 64618
(71529, 'deDE', 'Thok der Blutrünstige', NULL, NULL, NULL, 23222), -- 71529
(60796, 'deDE', 'Mishi', NULL, 'Lehrensucher Chos Begleiter', NULL, 23222), -- 60796
(60795, 'deDE', 'Lehrensucher Cho', NULL, NULL, NULL, 23222), -- 60795
(65812, 'deDE', 'Shrine of the Seeker''s Heart', NULL, NULL, NULL, 23222), -- 65812
(65811, 'deDE', 'Shrine of the Seeker''s Breath', NULL, NULL, NULL, 23222), -- 65811
(68846, 'deDE', 'Kun-Lai-Junges', NULL, NULL, 'wildpetcapturable', 23222), -- 68846
(61096, 'deDE', 'Eiskalte Winde', NULL, NULL, NULL, 23222), -- 61096
(65688, 'deDE', 'Bergsteiger', NULL, NULL, NULL, 23222), -- 65688
(65808, 'deDE', 'Shrine of the Seeker''s Body', NULL, NULL, NULL, 23222), -- 65808
(61084, 'deDE', 'Schneekugelgenerator', NULL, NULL, NULL, 23222), -- 61084
(61078, 'deDE', 'Snow Boulder Bounce Bunny', NULL, NULL, NULL, 23222), -- 61078
(61316, 'deDE', 'Tablet Kill Credit', NULL, NULL, NULL, 23222), -- 61316
(60079, 'deDE', 'General Purpose Bunny (JLR)', NULL, NULL, NULL, 23222), -- 60079
(60022, 'deDE', 'Neverrest Caravan Delivery Credit', NULL, NULL, NULL, 23222), -- 60022
(61815, 'deDE', 'Jump Landing Bunny 3', NULL, NULL, NULL, 23222), -- 61815
(61787, 'deDE', 'Arrow Shooting Bunny', NULL, NULL, NULL, 23222), -- 61787
(61799, 'deDE', 'Jump Landing Bunny', NULL, NULL, NULL, 23222), -- 61799
(61789, 'deDE', 'Arrow Receiving Bunny', NULL, NULL, NULL, 23222), -- 61789
(63422, 'deDE', 'Arrow Shooting Bunny Visual', NULL, NULL, NULL, 23222), -- 63422
(60765, 'deDE', 'Ambush Event Bunny', NULL, NULL, NULL, 23222), -- 60765
(63177, 'deDE', 'Ambush Trigger Visual', NULL, NULL, NULL, 23222), -- 63177
(63155, 'deDE', 'Steinwächter', NULL, NULL, NULL, 23222), -- 63155
(63393, 'deDE', 'Blitzeinschlag', NULL, NULL, NULL, 23222), -- 63393
(61794, 'deDE', 'Flammenfontäne', NULL, NULL, NULL, 23222), -- 61794
(68774, 'deDE', 'Jadejünger', NULL, NULL, NULL, 23222), -- 68774
(68770, 'deDE', 'In Jade gefangener Qilen', NULL, NULL, NULL, 23222), -- 68770
(68769, 'deDE', 'Königlicher Aufspieker', NULL, 'Das Gefolge', NULL, 23222), -- 68769
(60596, 'deDE', 'Cousin Zägenfell', NULL, 'Kühnbohn Kafa GmbH', NULL, 23222), -- 60596
(60503, 'deDE', 'Onkel Kühnbohn', NULL, 'Kühnbohn Kafa GmbH', NULL, 23222), -- 60503
(60425, 'deDE', 'Cousin Teeblatt', NULL, 'Grummelgrog', NULL, 23222), -- 60425
(60423, 'deDE', 'Cousin Kupferfinder', NULL, 'Grummelkuchen & Glücksbringer', NULL, 23222), -- 60423
(59755, 'deDE', 'Bruder Ölyak', NULL, 'Organisator des Jutepfads', NULL, 23222), -- 59755
(59413, 'deDE', 'Cousin Bergmoschus', NULL, 'Stallmeister', NULL, 23222), -- 59413
(62961, 'deDE', 'Grummel der Kotaspitze', NULL, NULL, NULL, 23222), -- 62961
(60420, 'deDE', 'Kleehüter', NULL, 'Gastwirt', NULL, 23222), -- 60420
(60416, 'deDE', 'Onkel Ostwind', NULL, 'Flugmeister', NULL, 23222), -- 60416
(61848, 'deDE', 'Wilde Onyxschlange', NULL, NULL, NULL, 23222), -- 61848
(60170, 'deDE', 'Großer Gladiatorhäuptling', NULL, NULL, NULL, 23222), -- 60170
(60598, 'deDE', 'Der Uuk von Knatz', NULL, 'Anführer der Ho-zen', NULL, 23222), -- 60598
(59439, 'deDE', 'Ho-zen-Schaukel', NULL, NULL, NULL, 23222), -- 59439
(59438, 'deDE', 'Faustkloppzuschauer', NULL, NULL, NULL, 23222), -- 59438
(60169, 'deDE', 'Käfigmeister', NULL, NULL, NULL, 23222), -- 60169
(59419, 'deDE', 'Mampf Mampf', NULL, NULL, NULL, 23222), -- 59419
(59680, 'deDE', 'Kampfziege', NULL, NULL, NULL, 23222), -- 59680
(59282, 'deDE', 'Faustkloppwegelagerer', NULL, NULL, NULL, 23222), -- 59282
(60008, 'deDE', 'Mok Mok', NULL, 'Legendenoberhäuptling', NULL, 23222), -- 60008
(59894, 'deDE', 'Bruder Yakschuh', NULL, NULL, NULL, 23222), -- 59894
(60012, 'deDE', 'Gefangener Grummel', NULL, NULL, NULL, 23222), -- 60012
(60027, 'deDE', 'Fesseln', NULL, NULL, NULL, 23222), -- 60027
(60010, 'deDE', 'Kampfziege', NULL, NULL, NULL, 23222), -- 60010
(59409, 'deDE', 'Ho-zen-Schaukel', NULL, NULL, NULL, 23222), -- 59409
(59495, 'deDE', 'Faustkloppzerkracher', NULL, NULL, NULL, 23222), -- 59495
(59412, 'deDE', 'Faustkloppaufspieker', NULL, NULL, NULL, 23222), -- 59412
(59410, 'deDE', 'Faustkloppzerkracher', NULL, NULL, NULL, 23222), -- 59410
(60593, 'deDE', 'Erfahrener Bergführer', NULL, NULL, NULL, 23222), -- 60593
(69943, 'deDE', 'Gumi', NULL, NULL, NULL, 23222), -- 69943
(58500, 'deDE', 'Brennendes Wrack', NULL, NULL, NULL, 23222), -- 58500
(63550, 'deDE', 'Alpiner Fuchsling', NULL, NULL, 'wildpetcapturable', 23222), -- 63550
(4357, 'deDE', 'Blutsumpfschmetterschwanz', NULL, NULL, NULL, 23222), -- 4357
(113845, 'deDE', 'Totembeherrschung', NULL, NULL, NULL, 23222), -- 113845
(106321, 'deDE', 'Totem des Rückenwinds', NULL, NULL, NULL, 23222), -- 106321
(106319, 'deDE', 'Totem der Glut', NULL, NULL, NULL, 23222), -- 106319
(106317, 'deDE', 'Totem des Sturms', NULL, NULL, NULL, 23222), -- 106317
(102392, 'deDE', 'Totem der Resonanz', NULL, NULL, NULL, 23222), -- 102392
(60921, 'deDE', '"Strange Happenings" - Ritual Credit', NULL, NULL, NULL, 23222), -- 60921
(55605, 'deDE', 'Blutlache', NULL, NULL, NULL, 23222), -- 55605
(61641, 'deDE', 'Imperialer Antiquator', NULL, NULL, NULL, 23222), -- 61641
(60825, 'deDE', 'Gequälter Geist', NULL, NULL, NULL, 23222), -- 60825
(65629, 'deDE', 'Oberlandrabe', NULL, NULL, NULL, 23222), -- 65629
(59967, 'deDE', 'Verdächtig aussehender Schneehaufen', NULL, NULL, NULL, 23222), -- 59967
(59693, 'deDE', 'Schelmischer Schneegeist', NULL, NULL, NULL, 23222), -- 59693
(59958, 'deDE', 'Tak Tak', NULL, NULL, NULL, 23222), -- 59958
(59408, 'deDE', 'Toter Packer', NULL, NULL, 'lootall', 23222), -- 59408
(59424, 'deDE', 'Dak Dak', NULL, 'Legendärer Häuptling', NULL, 23222), -- 59424
(59427, 'deDE', 'Toter Yeti', NULL, NULL, NULL, 23222), -- 59427
(59920, 'deDE', 'Monkey Idol Target', NULL, NULL, NULL, 23222), -- 59920
(60498, 'deDE', 'Grummelopfer', NULL, NULL, NULL, 23222), -- 60498
(59814, 'deDE', 'Toter Packer', NULL, NULL, NULL, 23222), -- 59814
(59456, 'deDE', 'Hozen Statue Eye (Right)', NULL, NULL, NULL, 23222), -- 59456
(59430, 'deDE', 'Der gehässige Ko Ko', NULL, NULL, NULL, 23222), -- 59430
(66197, 'deDE', 'Glücksbringer', NULL, NULL, NULL, 23222), -- 66197
(66191, 'deDE', 'Glücksbringer', NULL, NULL, NULL, 23222), -- 66191
(59703, 'deDE', 'Bruder Fährtenspürer', NULL, 'Organisator des Jutepfads', NULL, 23222), -- 59703
(59414, 'deDE', 'Bruchzahnstürmer', NULL, NULL, NULL, 23222), -- 59414
(59802, 'deDE', 'Bruchzahnverheerer', NULL, NULL, NULL, 23222), -- 59802
(66186, 'deDE', 'Glücksbringer', NULL, NULL, NULL, 23222), -- 66186
(59436, 'deDE', 'Verfiddelter Marodeur', NULL, NULL, NULL, 23222), -- 59436
(59435, 'deDE', 'Verfiddelter Schamane', NULL, NULL, NULL, 23222), -- 59435
(59803, 'deDE', 'Bruchzahnspringer', NULL, NULL, NULL, 23222), -- 59803
(59984, 'deDE', 'Gezähmter Spiekflügel', NULL, NULL, NULL, 23222), -- 59984
(59805, 'deDE', 'Mo Mo', NULL, NULL, NULL, 23222), -- 59805
(60879, 'deDE', 'Toter Ho-zen', NULL, NULL, NULL, 23222), -- 60879
(59817, 'deDE', 'Toter Packer', NULL, NULL, NULL, 23222), -- 59817
(59815, 'deDE', 'Toter Packer', NULL, NULL, NULL, 23222), -- 59815
(60550, 'deDE', 'Wilder Stachelberggeist', NULL, NULL, NULL, 23222), -- 60550
(59556, 'deDE', 'Grummelbergführer', NULL, NULL, NULL, 23222), -- 59556
(59897, 'deDE', 'Toter Packer', NULL, NULL, 'lootall', 23222), -- 59897
(59896, 'deDE', 'Toter Packer', NULL, NULL, 'lootall', 23222), -- 59896
(59416, 'deDE', 'Bruchzahnwerfer', NULL, NULL, NULL, 23222), -- 59416
(59898, 'deDE', 'Toter Packer', NULL, NULL, 'lootall', 23222), -- 59898
(66380, 'deDE', 'Swiftness Trap', NULL, NULL, NULL, 23222), -- 66380
(63447, 'deDE', 'Mogustatue', NULL, NULL, 'attack', 23222), -- 63447
(60925, 'deDE', 'Bewohner von Bleichwind', NULL, NULL, NULL, 23222), -- 60925
(63562, 'deDE', 'Mogu Statue Mount Bunny', NULL, NULL, NULL, 23222), -- 63562
(63556, 'deDE', 'Mogustatue', NULL, NULL, 'attack', 23222), -- 63556
(64965, 'deDE', 'Milau', NULL, 'Kind von Alani', NULL, 23222), -- 64965
(63610, 'deDE', 'Unterwerfer der Shao-Tien', NULL, NULL, NULL, 23222), -- 63610
(34337, 'deDE', 'Der Postmeister', NULL, NULL, NULL, 23222), -- 34337
(63674, 'deDE', 'Mogustatue', NULL, NULL, NULL, 23222), -- 63674
(63611, 'deDE', 'Seelenrufer der Shao-Tien', NULL, NULL, NULL, 23222), -- 63611
(67836, 'deDE', 'Runensucher von Silbermond', 'Runensucherin von Silbermond', 'Die Archäologische Akademie', NULL, 23222), -- 67836
(67833, 'deDE', 'Quecksilberwächter', NULL, NULL, 'inspect', 23222), -- 67833
(67835, 'deDE', 'Runensucher von Silbermond', 'Runensucherin von Silbermond', 'Die Archäologische Akademie', NULL, 23222), -- 67835
(65134, 'deDE', 'Faust der Shao-Tien', NULL, NULL, NULL, 23222), -- 65134
(64852, 'deDE', 'Jadewächter', NULL, NULL, NULL, 23222), -- 64852
(63572, 'deDE', 'Fire Tile Trap Bunny', NULL, NULL, NULL, 23222), -- 63572
(65170, 'deDE', 'Jadekrieger', NULL, NULL, NULL, 23222), -- 65170
(63575, 'deDE', 'Arrow Tile Trap Bunny', NULL, NULL, NULL, 23222), -- 63575
(63573, 'deDE', 'Lightning Tile Trap Bunny', NULL, NULL, NULL, 23222), -- 63573
(65133, 'deDE', 'Zauberer der Shao-Tien', NULL, NULL, NULL, 23222), -- 65133
(61519, 'deDE', 'Händler Ho-zen-Pranke', NULL, 'Grummelkuchen & Glücksbringer', NULL, 23222), -- 61519
(61520, 'deDE', 'Machganz Rothammer', NULL, 'Schmiedekunstbedarf', NULL, 23222), -- 61520
(62877, 'deDE', 'Schmutziger Krug', NULL, 'Gastwirt', NULL, 23222), -- 62877
(60163, 'deDE', 'Wukao Lee', NULL, NULL, NULL, 23222), -- 60163
(60161, 'deDE', 'Shado-Meister Chong', NULL, NULL, NULL, 23222), -- 60161
(65580, 'deDE', 'Bezwungenes Sha', NULL, NULL, NULL, 23222), -- 65580
(61473, 'deDE', 'Drachenmeisterin Ni', NULL, 'Flugmeisterin', NULL, 23222), -- 61473
(61252, 'deDE', 'Shado-Pan-Wache', NULL, NULL, NULL, 23222), -- 61252
(60164, 'deDE', 'Sha-verseuchter Yaungol', NULL, NULL, NULL, 23222), -- 60164
(60178, 'deDE', 'Sya Zhong', NULL, NULL, NULL, 23222), -- 60178
(60189, 'deDE', 'Ya Feuerzweig', NULL, NULL, NULL, 23222), -- 60189
(60190, 'deDE', 'Die alte Dame Fung', NULL, NULL, NULL, 23222), -- 60190
(60187, 'deDE', 'Jin Warmfass', NULL, NULL, NULL, 23222), -- 60187
(61224, 'deDE', 'Blinde Wut', NULL, NULL, NULL, 23222), -- 61224
(68241, 'deDE', 'Sha-verseuchter Yaungol', NULL, NULL, NULL, 23222), -- 68241
(61303, 'deDE', 'Kobai', NULL, NULL, NULL, 23222),
(60288, 'deDE', 'Toter Einwohner von Feuerzweigwinkel', 'Tote Einwohnerin von Feuerzweigwinkel', NULL, NULL, 23222), -- 60288
(60099, 'deDE', 'Westlicher Bohrturm', NULL, NULL, NULL, 23222), -- 60099
(60127, 'deDE', 'Kriegshetzer von Ordo', NULL, NULL, NULL, 23222), -- 60127
(60032, 'deDE', 'Akonu der Glutrufer', NULL, NULL, NULL, 23222), -- 60032
(60098, 'deDE', 'Südlicher Bohrturm', NULL, NULL, NULL, 23222), -- 60098
(65774, 'deDE', 'Yakkalb von Ordo', NULL, NULL, NULL, 23222), -- 65774
(65773, 'deDE', 'Yak von Ordo', NULL, NULL, NULL, 23222), -- 65773
(60031, 'deDE', 'Pao-kun der Pyromant', NULL, NULL, NULL, 23222), -- 60031
(60030, 'deDE', 'Harala der Feuersprecher', NULL, NULL, NULL, 23222), -- 60030
(60096, 'deDE', 'Östlicher Bohrturm', NULL, NULL, NULL, 23222), -- 60096
(59968, 'deDE', 'Marodeur von Ordo', NULL, NULL, NULL, 23222), -- 59968
(59969, 'deDE', 'Musaan der Flammenwirker', NULL, NULL, NULL, 23222), -- 59969
(59972, 'deDE', 'Krieger von Ordo', NULL, NULL, NULL, 23222), -- 59972
(59970, 'deDE', 'Kriegsyak von Ordo', NULL, NULL, NULL, 23222), -- 59970
(31047, 'deDE', 'ELM General Purpose Bunny Gigantic', NULL, NULL, NULL, 23222), -- 31047
(65440, 'deDE', 'Fire Beam Bunny', NULL, NULL, NULL, 23222), -- 65440
(59797, 'deDE', 'Seelenerwecker der Mogujia', NULL, NULL, NULL, 23222), -- 59797
(59773, 'deDE', 'Terrakottawächter', NULL, NULL, NULL, 23222), -- 59773
(59758, 'deDE', 'Terrakottakrieger', NULL, NULL, NULL, 23222), -- 59758
(61488, 'deDE', 'Hackiss', NULL, NULL, NULL, 23222), -- 61488
(61489, 'deDE', 'Heiliss', NULL, NULL, NULL, 23222), -- 61489
(61490, 'deDE', 'Tankiss', NULL, NULL, NULL, 23222), -- 61490
(61475, 'deDE', 'Infanterist von Ruqin', NULL, NULL, NULL, 23222), -- 61475
(59790, 'deDE', 'Infanterist von Ruqin', NULL, NULL, NULL, 23222), -- 59790
(59826, 'deDE', 'Ältester von Ruqin', NULL, NULL, NULL, 23222), -- 59826
(59538, 'deDE', 'Vorhut von Ruqin', NULL, NULL, NULL, 23222), -- 59538
(60195, 'deDE', 'Bruchzahnschleicher', NULL, NULL, NULL, 23222), -- 60195
(59593, 'deDE', 'Grummelbergführer', NULL, NULL, NULL, 23222), -- 59593
(59527, 'deDE', 'Packer des Jutepfades', NULL, NULL, NULL, 23222), -- 59527
(59526, 'deDE', 'Yakführer des Jutepfades', NULL, NULL, NULL, 23222), -- 59526
(59443, 'deDE', 'Bruchzahnspeerhaber', NULL, NULL, NULL, 23222), -- 59443
(59697, 'deDE', 'Neffe Krummnagel', NULL, 'Juniorbergführer', NULL, 23222), -- 59697
(60093, 'deDE', 'Ji-Lus Karren', NULL, NULL, 'vehichleCursor', 23222), -- 60093
(61504, 'deDE', 'Klein-Sauberdrache', NULL, 'Flugmeister', NULL, 23222), -- 61504
(59731, 'deDE', 'Bruder Würfelpech', NULL, NULL, NULL, 23222), -- 59731
(59716, 'deDE', 'Ji-Lu der Glückspilz', NULL, NULL, NULL, 23222), -- 59716
(59698, 'deDE', 'Bruder Pelzstutz', NULL, NULL, NULL, 23222), -- 59698
(59688, 'deDE', 'Chiyo Nebelpfote', NULL, 'Gastwirtin', 'innkeeper', 23222), -- 59688
(59696, 'deDE', 'Onkel Kleeblatt', NULL, NULL, NULL, 23222), -- 59696
(59699, 'deDE', 'Bergyak', NULL, NULL, NULL, 23222), -- 59699
(62108, 'deDE', 'Blauer Glücksfaden', NULL, NULL, NULL, 23222), -- 62108
(61847, 'deDE', 'Lehrensucher Cho', NULL, NULL, NULL, 23222), -- 61847
(65416, 'deDE', 'Alchemist Yuan Bunny', NULL, NULL, NULL, 23222), -- 65416
(59691, 'deDE', 'Alchemist Yuan', NULL, 'Lieferant für Handwerkswaren', NULL, 23222), -- 59691
(62158, 'deDE', 'Zaiyu', NULL, NULL, NULL, 23222), -- 62158
(59827, 'deDE', 'Eisenformer Shou', NULL, 'Schmied', 'repairnpc', 23222), -- 59827
(62149, 'deDE', 'Yoona', NULL, NULL, NULL, 23222), -- 62149
(60508, 'deDE', 'Zengi', NULL, NULL, NULL, 23222), -- 60508
(59701, 'deDE', 'Bruder Flusenbeutel', NULL, 'Organisator des Jutepfads', NULL, 23222), -- 59701
(59695, 'deDE', 'Der dicke Sal', NULL, 'Gemischtwaren', NULL, 23222), -- 59695
(60860, 'deDE', 'Junges Bergyak', NULL, NULL, NULL, 23222), -- 60860
(59307, 'deDE', 'Sauberes Yak', NULL, NULL, NULL, 23222), -- 59307
(59859, 'deDE', 'Yakführer des Jutepfades', NULL, NULL, NULL, 23222), -- 59859
(59858, 'deDE', 'Packer des Jutepfades', NULL, NULL, NULL, 23222), -- 59858
(59578, 'deDE', 'Grummelbergführer', NULL, NULL, NULL, 23222), -- 59578
(60523, 'deDE', 'Gipfelpirscherjunges', NULL, NULL, NULL, 23222), -- 60523
(64516, 'deDE', 'Cousin Träghand', NULL, 'Handelsreisender', NULL, 23222), -- 64516
(64514, 'deDE', 'Großes Expeditionsyak', NULL, NULL, NULL, 23222), -- 64514
(64515, 'deDE', 'Mystiker Vogelhut', NULL, 'Transmogrifizierer', NULL, 23222), -- 64515
(66415, 'deDE', 'Händler Qiu', NULL, NULL, NULL, 23222), -- 66415
(59405, 'deDE', 'Li Zapfgold', NULL, 'Gastwirtin', NULL, 23222), -- 59405
(59407, 'deDE', 'Kobeyak', NULL, NULL, NULL, 23222), -- 59407
(70155, 'deDE', 'Gaunah', NULL, 'Erstaunliche Amulette', NULL, 23222), -- 70155
(64882, 'deDE', 'Madame Lani', NULL, NULL, NULL, 23222), -- 64882
(64518, 'deDE', 'Onkel Dickehose', NULL, 'Gebrauchtyakverkäufer', NULL, 23222), -- 64518
(59509, 'deDE', 'Hirte Blütenduft', NULL, 'Stallmeister', NULL, 23222), -- 59509
(59371, 'deDE', 'Achtgroschen der Glückspilz', NULL, 'Karawanenmeister', NULL, 23222), -- 59371
(65121, 'deDE', 'Reinpelz', NULL, 'Lederverarbeitungslehrer', NULL, 23222), -- 65121
(61494, 'deDE', 'Braumeisterin Chani', NULL, NULL, NULL, 23222), -- 61494
(61493, 'deDE', 'Gabelbein', NULL, 'Grummelkuchen & Glücksbringer', NULL, 23222), -- 61493
(59406, 'deDE', 'Grummelpacker', NULL, NULL, NULL, 23222), -- 59406
(59403, 'deDE', 'Schleifstein', NULL, 'Schmiedekunstbedarf', NULL, 23222), -- 59403
(59402, 'deDE', 'Schleimi Tintenklecks', NULL, 'Reagenzienverkäufer', NULL, 23222), -- 59402
(71082, 'deDE', 'Opa Brummelflüte', NULL, NULL, 'speak', 23222), -- 71082
(59597, 'deDE', 'Rußquaste', NULL, 'Vorratshändler des Jutepfads', NULL, 23222), -- 59597
(59576, 'deDE', 'Kleefänger', NULL, 'Organisator des Jutepfads', NULL, 23222), -- 59576
(63372, 'deDE', 'Gipfelwächter', 'Gipfelwächterin', NULL, NULL, 23222), -- 63372
(103863, 'deDE', 'Graddoc', NULL, NULL, 'vehichlecursor', 23222), -- 103863
(60875, 'deDE', 'Gipfelpirscher', NULL, NULL, NULL, 23222), -- 60875
(64791, 'deDE', 'Alpiner Fuchslingwelpe', NULL, NULL, NULL, 23222), -- 64791
(59540, 'deDE', 'Hungriger Yeti', NULL, NULL, NULL, 23222), -- 59540
(64411, 'deDE', 'Pterrorschwinge der Zandalari', NULL, NULL, NULL, 23222), -- 64411
(64412, 'deDE', 'Schreckensreiter der Zandalari', 'Schreckensreiterin der Zandalari', NULL, NULL, 23222), -- 64412
(63362, 'deDE', 'Flüsterwolkes Ballon', NULL, NULL, NULL, 23222), -- 63362
(64726, 'deDE', 'Alter Küstenpanzerschnapper', NULL, NULL, NULL, 23222), -- 64726
(64444, 'deDE', 'Nordwasserjäger', NULL, NULL, NULL, 23222), -- 64444
(67787, 'deDE', 'Mei Laoshi', NULL, 'Schwimmlehrerin', NULL, 23222), -- 67787
(61541, 'deDE', 'Prophet Khar''zul', NULL, NULL, NULL, 23222), -- 61541
(64642, 'deDE', 'Jünger der Gurubashi', NULL, 'Eindringlinge der Zandalari', NULL, 23222), -- 64642
(64638, 'deDE', 'Reitraptor der Gurubashi', NULL, NULL, NULL, 23222), -- 64638
(64639, 'deDE', 'Vorhut der Amani', NULL, 'Eindringlinge der Zandalari', NULL, 23222), -- 64639
(64643, 'deDE', 'Kopfspalter der Gurubashi', 'Kopfspalterin der Gurubashi', 'Eindringlinge der Zandalari', NULL, 23222), -- 64643
(64631, 'deDE', 'Hexendoktor der Gurubashi', 'Hexendoktorin der Zandalari', 'Eindringlinge der Zandalari', NULL, 23222), -- 64631
(64551, 'deDE', 'Dämmerschwingenkrähe', NULL, NULL, NULL, 23222), -- 64551
(59481, 'deDE', 'World Trigger', NULL, NULL, NULL, 23222), -- 59481
(64693, 'deDE', 'Unsichtbarer Mann', NULL, NULL, NULL, 23222), -- 64693
(63673, 'deDE', 'Sandstürmer der Farraki', NULL, 'Eindringlinge der Zandalari', NULL, 23222), -- 63673
(60434, 'deDE', 'Schwarzmähnenfährtenleser', NULL, NULL, NULL, 23222), -- 60434
(60800, 'deDE', 'Gezähmte Hyäne', NULL, NULL, NULL, 23222), -- 60800
(60767, 'deDE', 'Hauptmann Fleischreißer', NULL, NULL, NULL, 23222), -- 60767
(66570, 'deDE', 'Assassine der Farraki', 'Assassine der Farraki', 'Eindringlinge der Zandalari', NULL, 23222), -- 66570
(60560, 'deDE', 'Schwarzmähnenbrigant', NULL, NULL, NULL, 23222), -- 60560
(60846, 'deDE', 'Schwarzmähnenplünderer', NULL, NULL, NULL, 23222), -- 60846
(60694, 'deDE', 'Weiser Liao', NULL, NULL, NULL, 23222), -- 60694
(63723, 'deDE', 'Blauer Stachelkriecher', NULL, NULL, NULL, 23222), -- 63723
(60670, 'deDE', 'Toter Dorfbewohner', 'Tote Dorfbewohnerin', NULL, NULL, 23222), -- 60670
(64725, 'deDE', 'Küstenpanzerschnapper', NULL, NULL, NULL, 23222), -- 64725
(63672, 'deDE', 'Fire Bunny', NULL, NULL, NULL, 23222), -- 63672
(66472, 'deDE', 'Königsdornzange', NULL, NULL, NULL, 23222), -- 66472
(64436, 'deDE', 'Dornzangenkrabbler', NULL, NULL, NULL, 23222), -- 64436
(66658, 'deDE', 'Ain''zama Guju', NULL, NULL, NULL, 23222), -- 66658
(60437, 'deDE', 'Blauer Stachelkriecher', NULL, NULL, NULL, 23222), -- 60437
(64203, 'deDE', 'Behemoth der Amani', NULL, 'Eindringlinge der Zandalari', NULL, 23222), -- 64203
(66707, 'deDE', 'Frostweber der Drakkari', NULL, 'Eindringlinge der Zandalari', NULL, 23222), -- 66707
(45979, 'deDE', 'General Purpose Bunny JMF', NULL, NULL, NULL, 23222), -- 45979
(60580, 'deDE', 'Schädelberster der Amani', NULL, 'Eindringlinge der Zandalari', NULL, 23222), -- 60580
(64202, 'deDE', 'Verhexer der Gurubashi', NULL, 'Eindringlinge der Zandalari', NULL, 23222), -- 64202
(60581, 'deDE', 'Raptorführer der Amani', NULL, 'Eindringlinge der Zandalari', NULL, 23222), -- 60581
(65428, 'deDE', 'Ausgebildeter Raptor der Amani', NULL, NULL, NULL, 23222), -- 65428
(66261, 'deDE', 'Champion der Zandalari', NULL, NULL, NULL, 23222), -- 66261
(66169, 'deDE', 'Derwisch der Farraki', NULL, 'Eindringlinge der Zandalari', NULL, 23222), -- 66169
(61498, 'deDE', 'Verletzter Dorfbewohner', NULL, NULL, NULL, 23222), -- 61498
(61495, 'deDE', 'Ältester Shu', NULL, 'Dorfältester', NULL, 23222), -- 61495
(60605, 'deDE', 'Liu Ze', NULL, 'Gastwirtin', 'innkeeper', 23222), -- 60605
(64215, 'deDE', 'Verängstigter Dorfbewohner', NULL, NULL, NULL, 23222), -- 64215
(64214, 'deDE', 'Verängstigter Dorfbewohner', NULL, NULL, NULL, 23222), -- 64214
(62971, 'deDE', 'Ältester Hou', NULL, NULL, NULL, 23222), -- 62971
(62970, 'deDE', 'Älteste Chi', NULL, NULL, NULL, 23222), -- 62970
(61371, 'deDE', 'Lehrensucher Cho', NULL, NULL, NULL, 23222), -- 61371
(61545, 'deDE', 'Koa', NULL, NULL, NULL, 23222), -- 61545
(60436, 'deDE', 'Li Hai', NULL, NULL, NULL, 23222), -- 60436
(61503, 'deDE', 'Shomi', NULL, 'Die Tochter des Schmieds', NULL, 23222), -- 61503
(65884, 'deDE', 'Stew Bunny', NULL, NULL, NULL, 23222), -- 65884
(64836, 'deDE', 'Zasha', NULL, 'Handwerkswaren', NULL, 23222), -- 64836
(61496, 'deDE', 'Stahlbieger Doshu', NULL, 'Schmied', 'repairnpc', 23222), -- 61496
(64830, 'deDE', 'Toshi', NULL, NULL, NULL, 23222), -- 64830
(66182, 'deDE', 'Pirscher der Gurubashi', 'Pirscherin der Gurubashi', 'Eindringlinge der Zandalari', NULL, 23222), -- 66182
(64070, 'deDE', 'Verängstigter Dorfbewohner', NULL, NULL, NULL, 23222), -- 64070
(64069, 'deDE', 'Verängstigter Dorfbewohner', NULL, NULL, NULL, 23222), -- 64069
(66165, 'deDE', 'Scharmützler der Amani', NULL, 'Eindringlinge der Zandalari', NULL, 23222), -- 66165
(61511, 'deDE', 'Bo der Windfänger', NULL, 'Flugmeister', NULL, 23222), -- 61511
(61417, 'deDE', 'Erschöpfter Verteidiger', NULL, NULL, NULL, 23222), -- 61417
(61381, 'deDE', 'Erschöpfter Verteidiger', NULL, NULL, NULL, 23222), -- 61381
(66174, 'deDE', 'Nomade der Farraki', 'Nomadin der Farraki', 'Eindringlinge der Zandalari', NULL, 23222), -- 66174
(66349, 'deDE', 'Fledermausreiter der Gurubashi', NULL, 'Eindringlinge der Zandalari', NULL, 23222), -- 66349
(60804, 'deDE', 'Leichtfüßige Schnappklaue', NULL, NULL, NULL, 23222), -- 60804
(66164, 'deDE', 'Tiger von Zouchin', NULL, NULL, NULL, 23222), -- 66164
(64745, 'deDE', 'Flachlandstachelschwein', NULL, NULL, NULL, 23222), -- 64745
(63363, 'deDE', 'Flüsterwolkes Ballon', NULL, NULL, 'vehichlecursor', 23222), -- 63363
(64790, 'deDE', 'Alpiner Fuchsling', NULL, NULL, NULL, 23222), -- 64790
(61380, 'deDE', 'Shin Flüsterwolke', NULL, 'Flugmeister', NULL, 23222), -- 61380
(61379, 'deDE', 'Lin Flüsterwolke', NULL, NULL, NULL, 23222), -- 61379
(63355, 'deDE', 'Flüsterwolkes Ballon', NULL, NULL, 'vehichlecursor', 23222), -- 63355
(65423, 'deDE', 'Verirrte Seele', NULL, NULL, NULL, 23222), -- 65423
(64248, 'deDE', 'Gipfelzicklein', NULL, NULL, 'wildpetcapturable', 23222), -- 64248
(60866, 'deDE', 'Oberlandadler', NULL, NULL, NULL, 23222), -- 60866
(66891, 'deDE', 'Gipfelschreiter', NULL, NULL, NULL, 23222), -- 66891
(61624, 'deDE', 'Schwarzer Pfeil', NULL, 'Schmiedekunstbedarf', NULL, 23222), -- 61624
(61118, 'deDE', 'Grünfeder', NULL, 'Flugmeister', NULL, 23222), -- 61118
(61651, 'deDE', 'Meister Lao', NULL, NULL, NULL, 23222), -- 61651
(61119, 'deDE', 'Schwitzfinger', NULL, 'Mampfis und Knabberlis', NULL, 23222), -- 61119
(64552, 'deDE', 'Xuen Event Chat Bunny', NULL, NULL, NULL, 23222), -- 64552
(72536, 'deDE', 'Prüfungsmeister Rotun', NULL, 'Lehrmeister der Feuerprobe', NULL, 23222), -- 72536
(64528, 'deDE', 'Xuen', NULL, NULL, 'speak', 23222), -- 64528
(64542, 'deDE', 'Sonnenläufer Dezco', NULL, 'Häuptling der Morgenjäger', NULL, 23222), -- 64542
(64540, 'deDE', 'Anduin Wrynn', NULL, 'Prinz von Sturmwind', NULL, 23222), -- 64540
(64537, 'deDE', 'Zhi der Harmonische', NULL, 'Verwalter', NULL, 23222), -- 64537
(64536, 'deDE', 'Taran Zhu', NULL, 'Meister der Shado-Pan', NULL, 23222), -- 64536
(61212, 'deDE', 'Tigertempelzuschauer', 'Tigertempelzuschauerin', NULL, NULL, 23222), -- 61212
(61531, 'deDE', 'Gefülltes Fläschchen', NULL, 'Grummelgrog und Souvenirs', NULL, 23222), -- 61531
(61529, 'deDE', 'Kätzchen von Xuen', NULL, NULL, 'questinteract', 23222), -- 61529
(61211, 'deDE', 'Tigertempelmönch', NULL, NULL, NULL, 23222), -- 61211
(60168, 'deDE', 'Toter Infanterist von Ruqin', NULL, NULL, NULL, 23222), -- 60168
(60431, 'deDE', 'Schläger von Yongqi', NULL, NULL, NULL, 23222), -- 60431
(60475, 'deDE', 'Schreckensbann', NULL, NULL, NULL, 23222), -- 60475
(60458, 'deDE', 'Stammesalchemist', NULL, NULL, NULL, 23222), -- 60458
(59420, 'deDE', 'Totes Bergyak', NULL, NULL, NULL, 23222), -- 59420
(60478, 'deDE', 'Toter Dorfbewohner', 'Tote Dorfbewohnerin', NULL, NULL, 23222), -- 60478
(60479, 'deDE', 'Plünderer von Yongqi', NULL, NULL, NULL, 23222), -- 60479
(65875, 'deDE', 'Flammenreiter von Yongqi', NULL, NULL, NULL, 23222), -- 65875
(59534, 'deDE', 'Gipfelpirscher', NULL, NULL, NULL, 23222), -- 59534
(60459, 'deDE', 'Flammenreiter', NULL, NULL, NULL, 23222), -- 60459
(59656, 'deDE', 'Der Angstmeister', NULL, NULL, NULL, 23222), -- 59656
(115146, 'deDE', 'Knochi', NULL, NULL, NULL, 23222), -- 115146
(59385, 'deDE', 'Späher von Yongqi', NULL, NULL, NULL, 23222), -- 59385
(59353, 'deDE', 'Lao Moschuspranke', NULL, 'Yaktreiber', NULL, 23222), -- 59353
(60482, 'deDE', 'Umbrastrolch', NULL, NULL, NULL, 23222), -- 60482
(59382, 'deDE', 'Räuber der Burigli', NULL, NULL, NULL, 23222), -- 59382
(59367, 'deDE', 'Entführtes Yak', NULL, NULL, NULL, 23222), -- 59367
(59339, 'deDE', 'Erschrockenes Yak', NULL, NULL, NULL, 23222), -- 59339
(59718, 'deDE', 'Räuber der Burigli', NULL, NULL, NULL, 23222), -- 59718
(417, 'deDE', 'Teufelsjäger', NULL, NULL, NULL, 23222), -- 417
(90202, 'deDE', 'Abyssius', NULL, NULL, NULL, 23222), -- 90202
(60028, 'deDE', 'ELM General Purpose Bunny Gigantic (scale x6)', NULL, NULL, NULL, 23222), -- 60028
(59821, 'deDE', 'Bao Jian', NULL, 'Antiquitätensammler', NULL, 23222), -- 59821
(59486, 'deDE', 'Bannerwache der Bataari', NULL, NULL, NULL, 23222), -- 59486
(24021, 'deDE', 'ELM General Purpose Bunny (scale x0.01)', NULL, NULL, NULL, 23222), -- 24021
(59679, 'deDE', 'Yak der Horde', NULL, NULL, NULL, 23222), -- 59679
(59573, 'deDE', 'Bauer Chow', NULL, NULL, NULL, 23222), -- 59573
(59636, 'deDE', 'Uyen Chow', NULL, NULL, NULL, 23222), -- 59636
(59655, 'deDE', 'Vermaledeiter Shed-Ling', NULL, NULL, NULL, 23222), -- 59655
(59702, 'deDE', 'Präriemaus', NULL, NULL, 'wildpetcapturable', 23222), -- 59702
(63850, 'deDE', 'Quirliges Glühwürmchen', NULL, NULL, 'wildpetcapturable', 23222), -- 63850
(63027, 'deDE', 'Steppenläufer', NULL, NULL, NULL, 23222), -- 63027
(65204, 'deDE', 'Quirliges Glühwürmchen', NULL, NULL, NULL, 23222), -- 65204
(59341, 'deDE', 'Händler Tantan', NULL, 'Vorräte', NULL, 23222), -- 59341
(59157, 'deDE', 'Granitqilen', NULL, NULL, NULL, 23222), -- 59157
(58472, 'deDE', 'Kyo', NULL, 'Che Wildmarschs Begleiter', NULL, 23222), -- 58472
(58471, 'deDE', 'Kun Herbstlicht', NULL, NULL, NULL, 23222), -- 58471
(58470, 'deDE', 'Hee Samtfuß', NULL, NULL, NULL, 23222), -- 58470
(58467, 'deDE', 'Che Wildmarsch', NULL, NULL, NULL, 23222), -- 58467
(58466, 'deDE', 'Rook Steinzeh', NULL, NULL, NULL, 23222), -- 58466
(58465, 'deDE', 'Anji Herbstlicht', NULL, NULL, NULL, 23222), -- 58465
(58408, 'deDE', 'Leven Morgenklinge', NULL, NULL, NULL, 23222), -- 58408
(58469, 'deDE', 'Ren Feuerzunge', NULL, NULL, NULL, 23222), -- 58469
(58468, 'deDE', 'Sun Zartherz', NULL, NULL, NULL, 23222), -- 58468
(65976, 'deDE', 'Schankwirt Tomro', NULL, 'Gastwirt', NULL, 23222), -- 65976
(58818, 'deDE', 'Koch Tope', NULL, NULL, NULL, 23222), -- 58818
(58695, 'deDE', 'Hauswart Chu', NULL, NULL, NULL, 23222), -- 58695
(65762, 'deDE', 'Kanone der Shao-Tien', NULL, NULL, NULL, 23222), -- 65762
(64794, 'deDE', 'Sichuanhuhn', NULL, NULL, NULL, 23222), -- 64794
(59645, 'deDE', 'Krümel McYaungol', NULL, NULL, NULL, 23222), -- 59645
(64848, 'deDE', 'Anduin Wrynn', NULL, 'Prinz von Sturmwind', NULL, 23222), -- 64848
(65928, 'deDE', 'Räuber von Ordo', NULL, NULL, NULL, 23222), -- 65928
(59580, 'deDE', 'Aufseher von Ordo', NULL, NULL, NULL, 23222), -- 59580
(59577, 'deDE', 'Bauernhofsklave', 'Bauernhofsklavin', NULL, NULL, 23222), -- 59577
(64796, 'deDE', 'Tolaihasenjunges', NULL, NULL, NULL, 23222), -- 64796
(63557, 'deDE', 'Tolaihase', NULL, NULL, 'wildpetcapturable', 23222), -- 63557
(63558, 'deDE', 'Tolaihasenjunges', NULL, NULL, 'wildpetcapturable', 23222), -- 63558
(108452, 'deDE', 'Höllenbestie', NULL, NULL, NULL, 23222), -- 108452
(89, 'deDE', 'Höllenbestie', NULL, NULL, NULL, 23222), -- 89
(94584, 'deDE', 'Instabiler Riss', NULL, NULL, NULL, 23222), -- 94584
(99887, 'deDE', 'Schattenhafter Riss', NULL, NULL, NULL, 23222), -- 99887
(61070, 'deDE', 'Explosiver Hass', NULL, NULL, NULL, 23222), -- 61070
(63555, 'deDE', 'Zooeyschlange', NULL, NULL, 'wildpetcapturable', 23222), -- 63555
(59384, 'deDE', 'Felsfalke', NULL, NULL, NULL, 23222), -- 59384
(59148, 'deDE', 'Flammenrufer der Bataari', NULL, NULL, NULL, 23222), -- 59148
(64795, 'deDE', 'Tolaihase', NULL, NULL, NULL, 23222), -- 64795
(59083, 'deDE', 'Feuerkrieger der Bataari', NULL, NULL, NULL, 23222), -- 59083
(61749, 'deDE', 'Die Metalltatze', NULL, 'Abenteurerbedarf', NULL, 23222), -- 61749
(59076, 'deDE', 'Kommandantin Hsieh', NULL, NULL, NULL, 23222), -- 59076
(58989, 'deDE', 'Krieger von Binan', 'Kriegerin von Binan', NULL, NULL, 23222), -- 58989
(59147, 'deDE', 'Yaungol der Bataari', NULL, NULL, NULL, 23222), -- 59147
(59523, 'deDE', 'Kampfyak der Bataari', NULL, NULL, NULL, 23222), -- 59523
(58961, 'deDE', 'Yaungol der Bataari', NULL, NULL, NULL, 23222), -- 58961
(58956, 'deDE', 'Krieger von Binan', 'Kriegerin von Binan', NULL, NULL, 23222), -- 58956
(59143, 'deDE', 'Verletzter Krieger aus Binan', NULL, NULL, NULL, 23222), -- 59143
(59021, 'deDE', 'Flammenrufer der Bataari', NULL, NULL, NULL, 23222), -- 59021
(68564, 'deDE', 'Dos-Ryga', NULL, NULL, 'wildpet', 23222), -- 68564
(63547, 'deDE', 'Ebenenbeobachter', NULL, NULL, 'wildpetcapturable', 23222),
(59670, 'deDE', 'Hochlandmushan', NULL, NULL, NULL, 23222), -- 59670
(61069, 'deDE', 'Shai Hu', NULL, NULL, NULL, 23222), -- 61069
(59394, 'deDE', 'General Purpose Bunny JMF (Ground)', NULL, NULL, NULL, 23222), -- 59394
(65761, 'deDE', 'Siedendes Sha', NULL, NULL, NULL, 23222), -- 65761
(35114, 'deDE', 'ELM General Purpose Bunny (All Phases)', NULL, NULL, NULL, 23222), -- 35114
(16445, 'deDE', 'Terky', NULL, NULL, NULL, 23222), -- 16445
(59671, 'deDE', 'Hochlandkalb', NULL, NULL, NULL, 23222), -- 59671
(59685, 'deDE', 'Totes Hochlandmushan', NULL, NULL, NULL, 23222), -- 59685
(64797, 'deDE', 'Zooeyschlange', NULL, NULL, 'wildpetcapturable', 23222), -- 64797
(59335, 'deDE', 'Räuber der Burigli', NULL, NULL, NULL, 23222), -- 59335
(59672, 'deDE', 'Gipfelknochenhäuter', NULL, NULL, NULL, 23222), -- 59672
(65839, 'deDE', 'Stechklingenstachelschwein', NULL, NULL, NULL, 23222), -- 65839
(61874, 'deDE', 'Tuffi', NULL, NULL, 'vehichlecursor', 23222), -- 61874
(59319, 'deDE', 'Entlaufenes Yak', NULL, NULL, 'vehichlecursor', 23222), -- 59319
(55593, 'deDE', 'Nadelgeist', NULL, NULL, NULL, 23222), -- 55593
(59272, 'deDE', 'Wu-Peng', NULL, NULL, NULL, 23222), -- 59272
(59181, 'deDE', 'Wütender Flutweber', NULL, NULL, NULL, 23222), -- 59181
(59180, 'deDE', 'Orachi', NULL, 'Hauptmann der Tintenkiemenwache', NULL, 23222), -- 59180
(60995, 'deDE', 'Wütender Vollstrecker', NULL, NULL, NULL, 23222), -- 60995
(60491, 'deDE', 'Sha des Zorns', NULL, NULL, NULL, 23222), -- 60491
(61130, 'deDE', 'Wütender Schmied', NULL, NULL, NULL, 23222), -- 61130
(61566, 'deDE', 'Dissident der Tintenkiemen', NULL, NULL, NULL, 23222), -- 61566
(59165, 'deDE', 'Wütender Speerkämpfer', NULL, NULL, NULL, 23222), -- 59165
(59166, 'deDE', 'Wütender Priester', NULL, NULL, NULL, 23222), -- 59166
(61500, 'deDE', 'Lache der Verderbnis', NULL, NULL, 'interact', 23222); -- 61500


DELETE FROM `page_text_locale` WHERE (`ID`=4559 /*4559*/ AND `locale`='deDE') OR (`ID`=4503 /*4503*/ AND `locale`='deDE') OR (`ID`=4543 /*4543*/ AND `locale`='deDE') OR (`ID`=4528 /*4528*/ AND `locale`='deDE');
INSERT INTO `page_text_locale` (`ID`, `locale`,`Text`, `VerifiedBuild`) VALUES
(4559, 'deDE', 'Der Orden der Shado-Pan wurde vor zehntausend Jahren unter dem Patronat von Shaohao, dem letzten Kaiser von Pandaria, gegründet.$b$bShaohao wusste, dass die dunkle Energie des Sha - die physische Manifestation negativer Emotionen wie Zorn, Angst, Hass und Zweifel - eine große Gefahr für die Pandaren darstellte, wenn man sie unter dem Land schwelen lassen würde. Daher beauftragte er die stärksten Krieger von Pandaria damit, das Sha einzudämmen und zu kontrollieren.$b$bAn diesem Ort, wenige Stunden nachdem Kaiser Shaohao seinen eigenen Zorn, seinen Hass und seine Gewalt überwunden hatte, kniete der erste Shado-Pan nieder und sprach vor dem letzten Kaiser einen Eid. Die gleichen Worte werden seither von jedem Schüler der Shado-Pan gesprochen, seit zehntausend Jahren.', 23222), -- 4559
(4503, 'deDE', 'Die Mogu sehen ihre Toten als eine Sammlung von Ersatzteilen an. Seelen können zur späteren Verwendung an Stein gebunden werden. Mit Fleisch und Blut lässt sich das Leben der Kaisertreuen verlängern. Unangetastet und in einem Stück beerdigt zu werden, ist ein Zeichen großer Macht und Respekts.$b$bHier liegt das Tal der Kaiser, Ruhestätte für einhundert Generationen von Kriegsfürsten, Königen und Kaisern, die einst über dieses Land herrschten.$b$bGrabraub erfolgt auf eigene Gefahr!', 23222), -- 4503
(4543, 'deDE', 'An diesem Ort suchte Shaohao, der letzte Kaiser von Pandaria, vor zehntausend Jahren den Rat der Jadeschlange.$b$bAus dem Buch der Bürden, Kapitel 3:$b$b"Verzweifelt und ohne Hoffnung erklomm der letzte Kaiser den steilen Hang des Nimmerlaya. Dolchen gleich stach die Kälte durch seine seidene Robe und der beißende Wind schlug ihm höhnend ins Gesicht."$b$b"Erst als er den Gipfel erreichte, fand er Ruhe und Frieden und dort sprach er mit der Jadeschlange, dem Geist der Weisheit."$b$bDie Jadeschlange drängte Shaohao dazu, sich seiner Bürden zu entledigen, seinen Geist zu reinigen und eins mit dem Land zu werden.$b$bDer Kaiser war von dem Rat der Jadeschlange überrascht, doch weitere Antworten blieben ihm auf dem Gipfel verwehrt. Entmutigt stieg Shaohao von dem Berg hinab, um mit seinem Gefährten, dem Affenkönig, seinen nächsten Schritt zu besprechen.', 23222), -- 4543
(4528, 'deDE', 'Da die Yaungol den widrigen Bedingungen in der Tonlongsteppe seit der Zeit des letzten Pandarenkaisers trotzen mussten, haben sie ihre Taktiken entsprechend angepasst.$b$bDas Volk zieht ständig umher und errichtet kurzlebige "Streitlager" an Orten mit reichlichen Naturschätzen (besonders Öl und Wild), bevor sie weiterziehen. Wo das Lager erstellt wird, wie lange sie bleiben und wohin sie als Nächstes ziehen, bleibt allein dem Häuptling überlassen.$b$bIm Kampf schlagen die Yaungol am liebsten hart und schnell zu. Dabei verwenden sie viel Kavallerie, um den Feind zu umflanken und ihn mit harten Infanterieangriffen an den schwächsten Gliedern in seinen Reihen zu schikanieren. Feuermagie- und brennende Belagerungswaffen folgen diesem ersten Angriff.$b$bDie Yaungol sind dafür bekannt, dass sie sich ebenso schnell zurückziehen, wie sie angreifen. Dabei schätzen sie ihren Feind perfekt ein und setzen ihre Kräfte nur für sichere Siege ein.', 23222); -- 4528


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text    xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_offer_reward` WHERE `ID` IN (31392 /*31392*/, 31254 /*31254*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(31392, 396, 5, 0, 0, 0, 500, 0, 0, '$C, ich bin froh, dass Ihr hier seid! In den Überlieferungen der Pandaren gibt es einen legendären Ort, das Tal der Ewigen Blüten. Wenn man den Geschichten der Einheimischen Glauben schenken kann, handelt es sich um einen Ort mit großen heilerischen Kräften.$b$bAber der Kriegshäuptling der Horde hat dem Tal Unsagbares angetan.$b$bWir müssen dort einfach hinein!', 23222), -- 31392
(31254, 2, 1, 0, 0, 0, 500, 0, 0, 'Oh, Ihr seid $gein Freund:eine Freundin; von Cho? Willkommen! Willkommen!$b$bIch würde Euch ein Bankett auftischen, aber wie Ihr sehen könnt, stecken wir momentan in großen Schwierigkeiten.$b$bWürdet Ihr uns vielleicht helfen?', 23222); -- 31254


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text    xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_request_items` WHERE `ID`=31254;
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(31254, 0, 0, 0, 0, 'Noch mehr Fremde? Dieser Tag bringt mich an meine Grenzen.', 23222); -- 31254


INSERT IGNORE INTO `page_text` (`ID`, `Text`, `NextPageID`, `PlayerConditionID`, `Flags`, `VerifiedBuild`) VALUES
(4559, 'Der Orden der Shado-Pan wurde vor zehntausend Jahren unter dem Patronat von Shaohao, dem letzten Kaiser von Pandaria, gegründet.$b$bShaohao wusste, dass die dunkle Energie des Sha - die physische Manifestation negativer Emotionen wie Zorn, Angst, Hass und Zweifel - eine große Gefahr für die Pandaren darstellte, wenn man sie unter dem Land schwelen lassen würde. Daher beauftragte er die stärksten Krieger von Pandaria damit, das Sha einzudämmen und zu kontrollieren.$b$bAn diesem Ort, wenige Stunden nachdem Kaiser Shaohao seinen eigenen Zorn, seinen Hass und seine Gewalt überwunden hatte, kniete der erste Shado-Pan nieder und sprach vor dem letzten Kaiser einen Eid. Die gleichen Worte werden seither von jedem Schüler der Shado-Pan gesprochen, seit zehntausend Jahren.', 0, 0, 0, 23222), -- 4559
(4503, 'Die Mogu sehen ihre Toten als eine Sammlung von Ersatzteilen an. Seelen können zur späteren Verwendung an Stein gebunden werden. Mit Fleisch und Blut lässt sich das Leben der Kaisertreuen verlängern. Unangetastet und in einem Stück beerdigt zu werden, ist ein Zeichen großer Macht und Respekts.$b$bHier liegt das Tal der Kaiser, Ruhestätte für einhundert Generationen von Kriegsfürsten, Königen und Kaisern, die einst über dieses Land herrschten.$b$bGrabraub erfolgt auf eigene Gefahr!', 0, 0, 0, 23222), -- 4503
(4543, 'An diesem Ort suchte Shaohao, der letzte Kaiser von Pandaria, vor zehntausend Jahren den Rat der Jadeschlange.$b$bAus dem Buch der Bürden, Kapitel 3:$b$b"Verzweifelt und ohne Hoffnung erklomm der letzte Kaiser den steilen Hang des Nimmerlaya. Dolchen gleich stach die Kälte durch seine seidene Robe und der beißende Wind schlug ihm höhnend ins Gesicht."$b$b"Erst als er den Gipfel erreichte, fand er Ruhe und Frieden und dort sprach er mit der Jadeschlange, dem Geist der Weisheit."$b$bDie Jadeschlange drängte Shaohao dazu, sich seiner Bürden zu entledigen, seinen Geist zu reinigen und eins mit dem Land zu werden.$b$bDer Kaiser war von dem Rat der Jadeschlange überrascht, doch weitere Antworten blieben ihm auf dem Gipfel verwehrt. Entmutigt stieg Shaohao von dem Berg hinab, um mit seinem Gefährten, dem Affenkönig, seinen nächsten Schritt zu besprechen.', 0, 0, 0, 23222), -- 4543
(4528, 'Da die Yaungol den widrigen Bedingungen in der Tonlongsteppe seit der Zeit des letzten Pandarenkaisers trotzen mussten, haben sie ihre Taktiken entsprechend angepasst.$b$bDas Volk zieht ständig umher und errichtet kurzlebige "Streitlager" an Orten mit reichlichen Naturschätzen (besonders Öl und Wild), bevor sie weiterziehen. Wo das Lager erstellt wird, wie lange sie bleiben und wohin sie als Nächstes ziehen, bleibt allein dem Häuptling überlassen.$b$bIm Kampf schlagen die Yaungol am liebsten hart und schnell zu. Dabei verwenden sie viel Kavallerie, um den Feind zu umflanken und ihn mit harten Infanterieangriffen an den schwächsten Gliedern in seinen Reihen zu schikanieren. Feuermagie- und brennende Belagerungswaffen folgen diesem ersten Angriff.$b$bDie Yaungol sind dafür bekannt, dass sie sich ebenso schnell zurückziehen, wie sie angreifen. Dabei schätzen sie ihren Feind perfekt ein und setzen ihre Kräfte nur für sichere Siege ein.', 0, 0, 0, 23222); -- 4528


INSERT IGNORE INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `OptionBroadcastTextID`, `action_poi_id`, `box_coded`, `box_money`, `box_text`, `BoxBroadcastTextID`) VALUES
(14992, 1, 3, 'Unterrichtet mich im Kürschnern.', 0, 0, 0, 0, '', 0),
(14992, 0, 3, 'Unterrichtet mich in Lederverarbeitung.', 0, 0, 0, 0, '', 0),
(14986, 1, 1, 'Welche Bräus habt Ihr im Angebot?', 0, 0, 0, 0, '', 0),
(14986, 0, 3, 'Bildet mich aus.', 0, 0, 0, 0, '', 0),
(14993, 1, 3, 'Unterrichtet mich in Inschriftenkunde.', 0, 0, 0, 0, '', 0),
(14993, 0, 3, 'Unterrichtet mich in Kräuterkunde.', 0, 0, 0, 0, '', 0),
(14994, 0, 3, 'Unterrichtet mich in Erster Hilfe.', 0, 0, 0, 0, '', 0),
(14380, 0, 0, 'Nehmt den Gegenstand aus seinem Gepäck.', 0, 0, 0, 0, '', 0),
(13830, 0, 2, 'Ich würde gerne mit einem Drachen fliegen.', 0, 0, 0, 0, '', 0),
(83, 0, 4, 'Bringt mich ins Leben zurück.', 0, 0, 0, 0, '', 0),
(13794, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13795, 1, 5, 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', 0, 0, 0, 0, '', 0),
(13795, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13644, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13637, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13635, 1, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13635, 0, 5, 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', 0, 0, 0, 0, '', 0),
(13636, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13789, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13556, 1, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13556, 0, 5, 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', 0, 0, 0, 0, '', 0),
(13555, 0, 1, 'Zeigt mir Eure Waren.', 0, 0, 0, 0, '', 0),
(13554, 0, 1, 'Zeigt mir Eure Waren.', 0, 0, 0, 0, '', 0),
(14326, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(13753, 0, 1, 'Ich möchte ein wenig Eure Ware betrachten.', 0, 0, 0, 0, '', 0),
(13810, 0, 5, 'Bitte, setzt Euch und macht es Euch bequem.', 0, 0, 0, 0, '', 0),
(10182, 0, 1, 'Ich sehe mich nur mal um.', 0, 0, 0, 0, '', 0),
(9868, 1, 1, 'Ich möchte ein wenig in Euren Waren stöbern.', 0, 0, 0, 0, '', 0),
(9868, 0, 5, 'Ich möchte dieses Gasthaus zu meinem Heimatort machen.', 0, 0, 0, 0, '', 0);


INSERT IGNORE INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `comment`) VALUES
(50341, 0, 0, 'Fühlt die Faust der Dunkelheit!', 12, 0, 100, 0, 0, 0, 0, 'Borginn Dunkelfaust to Player'),
(50733, 0, 0, 'Ich spüre Eure Angst...', 12, 0, 100, 0, 0, 0, 0, 'Ski''thik to Player'),
(50789, 0, 0, 'Ihr hättet nicht herkommen sollen!', 12, 0, 100, 0, 0, 0, 0, 'Nessos das Orakel to Player'),
(59019, 0, 0, 'Wir werden uns vielleicht in das Tal der Vier Winde zurückziehen müssen. Ich will mein Zuhause hier nicht aufgeben.', 12, 0, 100, 92, 0, 0, 0, 'Dorfbewohner von Binan to Player'),
(59019, 1, 0, '$GEr:Sie; sieht ziemlich kräftig aus. Vielleicht kräftig genug, um die Yaungol zurückzuschlagen!', 12, 0, 100, 92, 0, 0, 0, 'Dorfbewohner von Binan to Player'),
(59019, 2, 0, '$GEin Fremder:Eine Fremde;. $GEr:Sie; sieht sehr exotisch aus. Nicht anstarren!', 12, 0, 100, 92, 0, 0, 0, 'Dorfbewohner von Binan to Player'),
(59073, 0, 0, 'Willkommen in Binan, $C. Ich bitte Euch... bitte helft uns.', 12, 0, 100, 20, 0, 0, 0, 'Bürgermeister Dornstab to Player'),
(59077, 0, 0, 'Seid gegrüßt, $C. Eure Leute sind drinnen. Kann ich Euch jedoch zuerst um einen Gefallen bitten?', 12, 0, 100, 1, 0, 0, 0, 'Apothekerin Cheng to Player'),
(59688, 0, 0, 'Kommt aus der Kälte herein und macht es Euch gemütlich!', 12, 0, 100, 1, 0, 0, 0, 'Chiyo Nebelpfote to Player'),
(59688, 1, 0, 'Eine sichere Reise!', 12, 0, 100, 3, 0, 0, 0, 'Chiyo Nebelpfote to Player'),
(59688, 2, 0, 'Willkommen, Gast! Bedient Euch am Bier.', 12, 0, 100, 1, 0, 0, 0, 'Chiyo Nebelpfote to Player'),
(59688, 3, 0, 'Kommt bald wieder!', 12, 0, 100, 3, 0, 0, 0, 'Chiyo Nebelpfote to Player'),
(59691, 0, 0, 'Vielleicht habt Ihr Interesse an einer magischen Landkarte? Ach, egal, die sind eh alle ausverkauft.', 12, 0, 100, 1, 0, 0, 0, 'Alchemist Yuan to Player'),
(59691, 1, 0, 'Willkommen in meinem Laden, $gReisender:Reisende;!', 12, 0, 100, 1, 0, 0, 0, 'Alchemist Yuan to Player'),
(59695, 0, 0, 'Seid gegrüßt, $C! Mir scheint, Ihr könntet Euch für frische Gewürze interessieren! Oder vielleicht ein paar Bücher für die Reise?', 12, 0, 100, 1, 0, 0, 0, 'Der dicke Sal to Player'),
(59695, 1, 0, 'Stattet mir auf jeden Fall wieder einen Besuch ab, $C!', 12, 0, 100, 2, 0, 0, 0, 'Der dicke Sal to Player'),
(59695, 2, 0, 'Seht Euch nur meine Waren an.', 12, 0, 100, 1, 0, 0, 0, 'Der dicke Sal to Player'),
(59695, 3, 0, 'Kommt bald zurück!', 12, 0, 100, 2, 0, 0, 0, 'Der dicke Sal to Player'),
(59703, 0, 0, 'Grummel! Versammelt Euch und atmet das Reiseräucherwerk tief ein. Viel Glück auf dem Weg zum Basislager am Kota.', 14, 0, 100, 0, 0, 0, 0, 'Bruder Fährtenspürer to Packer des Jutepfades'),
(59827, 0, 0, 'Eine saubere Waffe ist ein Zeichen dafür, dass sie nie einen Kampf gesehen hat.', 12, 0, 100, 0, 0, 0, 0, 'Eisenformer Shou to Player'),
(59967, 0, 0, 'Ein schelmischer Schneegeist springt aus dem verdächtig aussehenden Schneehaufen hervor.', 16, 0, 100, 0, 0, 0, 0, 'Verdächtig aussehender Schneehaufen to Player'),
(60161, 0, 0, 'Ihr seid durchgekommen! Kommt, wir müssen reden.', 12, 0, 100, 396, 0, 0, 0, 'Shado-Meister Chong to Player'),
(60416, 0, 0, 'Die Winde stehen heute gut.', 12, 0, 100, 1, 0, 0, 0, 'Onkel Ostwind to Player'),
(60491, 0, 0, 'Ihr werdet mich nicht erneut begraben!', 14, 0, 100, 0, 0, 29008, 0, 'Sha des Zorns to General Purpose Bunny JMF (Ground)'),
(60491, 1, 0, 'Mein Zorn fließt ungehindert!', 14, 0, 100, 0, 0, 29009, 0, 'Sha des Zorns to General Purpose Bunny JMF (Ground)'),
(60491, 2, 0, 'Eure Wut nährt mich!', 14, 0, 100, 0, 0, 29007, 0, 'Sha des Zorns to General Purpose Bunny JMF (Ground)'),
(60491, 3, 0, 'Eure Wut gewährt Euch Stärke.', 14, 0, 100, 0, 0, 29006, 0, 'Sha des Zorns to General Purpose Bunny JMF (Ground)'),
(60508, 0, 0, 'Ich dachte, Ihr spürt in Euren Knien nur das Wetter.', 12, 0, 100, 0, 0, 0, 0, 'Zengi to Zaiyu'),
(60524, 0, 0, 'Ihr werdet dieses Land niemals bekommen!', 12, 0, 100, 0, 0, 0, 0, 'Schattenjäger der Zandalari to Player'),
(60524, 1, 0, 'Ihr legt Euch mit dem falschen Reich an!', 12, 0, 100, 0, 0, 0, 0, 'Schattenjäger der Zandalari to Player'),
(61069, 0, 0, 'EURE WUT! ICH SAUGE EURE WUT IN MICH AUF!', 14, 0, 100, 5, 0, 0, 0, 'Shai Hu'),
(61496, 0, 0, 'Ich habe mich vom Kampf zurückgezogen, um mit meiner Familie in Frieden zu leben, doch es ist mir anscheinend nicht vergönnt.', 12, 0, 100, 1, 0, 0, 0, 'Stahlbieger Doshu to Player'),
(61545, 0, 0, 'Warum haben alle solche Angst? Ich werde keinen dieser Trolle an unseren Brunnen heranlassen! Ich bin ein guter Wächter!', 12, 0, 100, 0, 0, 0, 0, 'Koa to Player'),
(61756, 0, 0, 'Wollt Ihr etwas zum Fressen? Habe ich ganz alleine gemacht!', 12, 0, 100, 0, 0, 0, 0, 'Klein Elsa to Player'),
(62149, 0, 0, 'Hm. Ich habe diesen Ort noch nie gemocht. Die Plumpsklos waren immer dreckig.', 12, 0, 100, 0, 0, 0, 0, 'Yoona to Zaiyu'),
(62158, 0, 0, 'Die Ho-zen sind wieder auf Raubzug. Das spüre ich in meinen Knien.', 12, 0, 100, 0, 0, 0, 0, 'Zaiyu'),
(64536, 0, 0, 'Das halbe Tal ist verwüstet! Diese Fremdlinge haben mehr zerstört als die Mogu und Mantis zusammen!', 12, 0, 100, 5, 0, 38306, 0, 'Taran Zhu to Player'),
(64536, 1, 0, 'Großer Tiger, den Fremden von jenseits des Nebels kann man nicht trauen. Die Absturzstelle im Jadewald ist Beweis genug.', 12, 0, 100, 396, 0, 28416, 0, 'Taran Zhu to Player'),
(64540, 0, 0, 'Gebt uns eine Chance, es wieder in Ordnung zu bringen.', 12, 0, 100, 396, 0, 38016, 0, 'Anduin Wrynn to Player'),
(66261, 0, 0, 'Reißt ihnen das Fleisch von den Knochen!', 14, 0, 100, 0, 0, 0, 0, 'Champion der Zandalari');