-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_0_23222
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/15/2017 21:20:40


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=35312 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(35312, 'deDE', 'Siege of Niuzao Reward Quest', '', '', '', '', '', '', '', '', 23222);


DELETE FROM `gameobject_template_locale` WHERE (`entry`=212224 AND `locale`='deDE') OR (`entry`=213579 AND `locale`='deDE') OR (`entry`=213573 AND `locale`='deDE') OR (`entry`=213572 AND `locale`='deDE') OR (`entry`=213571 AND `locale`='deDE') OR (`entry`=213581 AND `locale`='deDE') OR (`entry`=213580 AND `locale`='deDE') OR (`entry`=213574 AND `locale`='deDE') OR (`entry`=213588 AND `locale`='deDE') OR (`entry`=213582 AND `locale`='deDE') OR (`entry`=213578 AND `locale`='deDE') OR (`entry`=213577 AND `locale`='deDE') OR (`entry`=213576 AND `locale`='deDE') OR (`entry`=213575 AND `locale`='deDE') OR (`entry`=213590 AND `locale`='deDE') OR (`entry`=213589 AND `locale`='deDE') OR (`entry`=213583 AND `locale`='deDE') OR (`entry`=213587 AND `locale`='deDE') OR (`entry`=213586 AND `locale`='deDE') OR (`entry`=213591 AND `locale`='deDE') OR (`entry`=213584 AND `locale`='deDE') OR (`entry`=213585 AND `locale`='deDE') OR (`entry`=215583 AND `locale`='deDE') OR (`entry`=215581 AND `locale`='deDE') OR (`entry`=215582 AND `locale`='deDE') OR (`entry`=215580 AND `locale`='deDE') OR (`entry`=215579 AND `locale`='deDE') OR (`entry`=215578 AND `locale`='deDE') OR (`entry`=215577 AND `locale`='deDE') OR (`entry`=215576 AND `locale`='deDE') OR (`entry`=212324 AND `locale`='deDE') OR (`entry`=212318 AND `locale`='deDE') OR (`entry`=215571 AND `locale`='deDE') OR (`entry`=215570 AND `locale`='deDE') OR (`entry`=212319 AND `locale`='deDE') OR (`entry`=215584 AND `locale`='deDE') OR (`entry`=215572 AND `locale`='deDE') OR (`entry`=215569 AND `locale`='deDE') OR (`entry`=215585 AND `locale`='deDE') OR (`entry`=215575 AND `locale`='deDE') OR (`entry`=215574 AND `locale`='deDE') OR (`entry`=215573 AND `locale`='deDE') OR (`entry`=212230 AND `locale`='deDE') OR (`entry`=214480 AND `locale`='deDE') OR (`entry`=214479 AND `locale`='deDE') OR (`entry`=214481 AND `locale`='deDE') OR (`entry`=212228 AND `locale`='deDE') OR (`entry`=213309 AND `locale`='deDE') OR (`entry`=213305 AND `locale`='deDE') OR (`entry`=213311 AND `locale`='deDE') OR (`entry`=213306 AND `locale`='deDE') OR (`entry`=213310 AND `locale`='deDE') OR (`entry`=213304 AND `locale`='deDE') OR (`entry`=213502 AND `locale`='deDE') OR (`entry`=213307 AND `locale`='deDE') OR (`entry`=213308 AND `locale`='deDE') OR (`entry`=212921 AND `locale`='deDE') OR (`entry`=210097 AND `locale`='deDE') OR (`entry`=213935 AND `locale`='deDE') OR (`entry`=215697 AND `locale`='deDE') OR (`entry`=213174 AND `locale`='deDE') OR (`entry`=214513 AND `locale`='deDE') OR (`entry`=211837 AND `locale`='deDE') OR (`entry`=211836 AND `locale`='deDE') OR (`entry`=213445 AND `locale`='deDE') OR (`entry`=212133 AND `locale`='deDE') OR (`entry`=212132 AND `locale`='deDE') OR (`entry`=211766 AND `locale`='deDE') OR (`entry`=212131 AND `locale`='deDE') OR (`entry`=211765 AND `locale`='deDE') OR (`entry`=214108 AND `locale`='deDE') OR (`entry`=210362 AND `locale`='deDE') OR (`entry`=211513 AND `locale`='deDE') OR (`entry`=211518 AND `locale`='deDE') OR (`entry`=214820 AND `locale`='deDE') OR (`entry`=213849 AND `locale`='deDE') OR (`entry`=212448 AND `locale`='deDE') OR (`entry`=212447 AND `locale`='deDE') OR (`entry`=213850 AND `locale`='deDE') OR (`entry`=216611 AND `locale`='deDE') OR (`entry`=213847 AND `locale`='deDE') OR (`entry`=211863 AND `locale`='deDE') OR (`entry`=211871 AND `locale`='deDE') OR (`entry`=211873 AND `locale`='deDE') OR (`entry`=211872 AND `locale`='deDE') OR (`entry`=213844 AND `locale`='deDE') OR (`entry`=213851 AND `locale`='deDE') OR (`entry`=212136 AND `locale`='deDE') OR (`entry`=212135 AND `locale`='deDE') OR (`entry`=211680 AND `locale`='deDE') OR (`entry`=215189 AND `locale`='deDE') OR (`entry`=215194 AND `locale`='deDE') OR (`entry`=211515 AND `locale`='deDE') OR (`entry`=184955 AND `locale`='deDE') OR (`entry`=211623 AND `locale`='deDE') OR (`entry`=209329 AND `locale`='deDE') OR (`entry`=211624 AND `locale`='deDE') OR (`entry`=212213 AND `locale`='deDE') OR (`entry`=212539 AND `locale`='deDE') OR (`entry`=212538 AND `locale`='deDE') OR (`entry`=212134 AND `locale`='deDE') OR (`entry`=213846 AND `locale`='deDE') OR (`entry`=214944 AND `locale`='deDE') OR (`entry`=209312 AND `locale`='deDE') OR (`entry`=214734 AND `locale`='deDE') OR (`entry`=214818 AND `locale`='deDE') OR (`entry`=212322 AND `locale`='deDE') OR (`entry`=212205 AND `locale`='deDE') OR (`entry`=216614 AND `locale`='deDE') OR (`entry`=213817 AND `locale`='deDE') OR (`entry`=213816 AND `locale`='deDE') OR (`entry`=213959 AND `locale`='deDE') OR (`entry`=214759 AND `locale`='deDE') OR (`entry`=214760 AND `locale`='deDE') OR (`entry`=214641 AND `locale`='deDE') OR (`entry`=215459 AND `locale`='deDE') OR (`entry`=214758 AND `locale`='deDE') OR (`entry`=212779 AND `locale`='deDE') OR (`entry`=214755 AND `locale`='deDE') OR (`entry`=214754 AND `locale`='deDE') OR (`entry`=211779 AND `locale`='deDE') OR (`entry`=211517 AND `locale`='deDE') OR (`entry`=213848 AND `locale`='deDE') OR (`entry`=211719 AND `locale`='deDE') OR (`entry`=214741 AND `locale`='deDE') OR (`entry`=211721 AND `locale`='deDE') OR (`entry`=212444 AND `locale`='deDE') OR (`entry`=212442 AND `locale`='deDE') OR (`entry`=212441 AND `locale`='deDE') OR (`entry`=214753 AND `locale`='deDE') OR (`entry`=211524 AND `locale`='deDE') OR (`entry`=211512 AND `locale`='deDE') OR (`entry`=211553 AND `locale`='deDE') OR (`entry`=214749 AND `locale`='deDE') OR (`entry`=213961 AND `locale`='deDE') OR (`entry`=214750 AND `locale`='deDE') OR (`entry`=211505 AND `locale`='deDE') OR (`entry`=211506 AND `locale`='deDE') OR (`entry`=214752 AND `locale`='deDE') OR (`entry`=211507 AND `locale`='deDE') OR (`entry`=212973 AND `locale`='deDE') OR (`entry`=212445 AND `locale`='deDE') OR (`entry`=214972 AND `locale`='deDE') OR (`entry`=214973 AND `locale`='deDE') OR (`entry`=214970 AND `locale`='deDE') OR (`entry`=214971 AND `locale`='deDE') OR (`entry`=214740 AND `locale`='deDE') OR (`entry`=213852 AND `locale`='deDE') OR (`entry`=212161 AND `locale`='deDE') OR (`entry`=213420 AND `locale`='deDE') OR (`entry`=211687 AND `locale`='deDE');
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(212224, 'deDE', 'Altar der Shan''ze', '', NULL, 23222),
(213579, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213573, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213572, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213571, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213581, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213580, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213574, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213588, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213582, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213578, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213577, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213576, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213575, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213590, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213589, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213583, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213587, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213586, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213591, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213584, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(213585, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215583, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215581, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215582, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215580, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215579, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215578, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215577, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215576, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(212324, 'deDE', 'Begräbnisurne der Mogu', '', NULL, 23222),
(212318, 'deDE', 'Tafel der Shan''ze', '', NULL, 23222),
(215571, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215570, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(212319, 'deDE', 'Tafel der Shan''ze', '', NULL, 23222),
(215584, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215572, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215569, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215585, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215575, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215574, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(215573, 'deDE', 'Käfig der Shan''ze', '', NULL, 23222),
(212230, 'deDE', 'Glänzendes Ei', '', NULL, 23222),
(214480, 'deDE', 'Glänzendes Ei', '', NULL, 23222),
(214479, 'deDE', 'Glänzendes Ei', '', NULL, 23222),
(214481, 'deDE', 'Glänzendes Ei', '', NULL, 23222),
(212228, 'deDE', 'Schlangenstatue', '', NULL, 23222),
(213309, 'deDE', 'Götze der Sra''thik', '', NULL, 23222),
(213305, 'deDE', 'Götze der Sra''thik', '', NULL, 23222),
(213311, 'deDE', 'Belagerungswaffe der Sra''thik', '', NULL, 23222),
(213306, 'deDE', 'Götze der Sra''thik', '', NULL, 23222),
(213310, 'deDE', 'Belagerungswaffe der Sra''thik', '', NULL, 23222),
(213304, 'deDE', 'Götze der Sra''thik', '', NULL, 23222),
(213502, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(213307, 'deDE', 'Belagerungswaffe der Sra''thik', '', NULL, 23222),
(213308, 'deDE', 'Belagerungswaffe der Sra''thik', '', NULL, 23222),
(212921, 'deDE', 'Tür', '', NULL, 23222),
(210097, 'deDE', 'Feuerwand', '', NULL, 23222),
(213935, 'deDE', 'Mantiskäfig', '', NULL, 23222),
(215697, 'deDE', 'Herausforderungsgong', '', NULL, 23222),
(213174, 'deDE', 'Gehärtetes Harz', '', NULL, 23222),
(214513, 'deDE', 'Instance Portal (Party + Heroic + Challenge)', '', NULL, 23222),
(211837, 'deDE', 'Futon des Vaters', '', NULL, 23222),
(211836, 'deDE', 'Schild des Vaters', '', NULL, 23222),
(213445, 'deDE', 'Des Kaisers Bürde - Teil 5', '', NULL, 23222),
(212133, 'deDE', 'Lebensmittelvorrat von Niuzao', '', NULL, 23222),
(212132, 'deDE', 'Lebensmittelvorrat von Niuzao', '', NULL, 23222),
(211766, 'deDE', 'Sra''thik-Waffe', '', NULL, 23222),
(212131, 'deDE', 'Lebensmittelvorrat von Niuzao', '', NULL, 23222),
(211765, 'deDE', 'Sra''thik-Waffe', '', NULL, 23222),
(214108, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(210362, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211513, 'deDE', 'Torfklumpen', '', NULL, 23222),
(211518, 'deDE', 'Knotranke', '', NULL, 23222),
(214820, 'deDE', 'Krik''thik-Bein', '', NULL, 23222),
(213849, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212448, 'deDE', 'Schmiede', '', NULL, 23222),
(212447, 'deDE', 'Amboss', '', NULL, 23222),
(213850, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(216611, 'deDE', 'Briefkasten', '', NULL, 23222),
(213847, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(211863, 'deDE', 'Krik''thik-Bein', '', NULL, 23222),
(211871, 'deDE', 'Schreckenskugel', '', NULL, 23222),
(211873, 'deDE', 'Schreckenskugel', '', NULL, 23222),
(211872, 'deDE', 'Schreckenskugel', '', NULL, 23222),
(213844, 'deDE', 'In Bern eingeschlossene Motte', '', NULL, 23222),
(213851, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212136, 'deDE', 'Feuerpfeile der Shado-Pan', '', NULL, 23222),
(212135, 'deDE', 'Feuerpfeile der Shado-Pan', '', NULL, 23222),
(211680, 'deDE', 'Krik''thik-Vorräte', '', NULL, 23222),
(215189, 'deDE', 'PA Debris Pile', '', NULL, 23222),
(215194, 'deDE', 'PA Debris Pile', '', NULL, 23222),
(211515, 'deDE', 'Torfhügel', '', NULL, 23222),
(184955, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211623, 'deDE', 'Effekt des Sha', '', NULL, 23222),
(209329, 'deDE', 'Reiches Kyparitvorkommen', '', NULL, 23222),
(211624, 'deDE', 'Effekt des Sha', '', NULL, 23222),
(212213, 'deDE', 'Effekt des Sha', '', NULL, 23222),
(212539, 'deDE', 'Wukaofernrohrständer', '', NULL, 23222),
(212538, 'deDE', 'Wukaofernrohr', '', NULL, 23222),
(212134, 'deDE', 'Armbrustbolzenbündel der Shado-Pan', '', NULL, 23222),
(213846, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(214944, 'deDE', 'Versammlungsstein', '', NULL, 23222),
(209312, 'deDE', 'Kyparitvorkommen', '', NULL, 23222),
(214734, 'deDE', 'Käfig der Sik''thik', '', NULL, 23222),
(214818, 'deDE', 'Sha-Pfütze', '', NULL, 23222),
(212322, 'deDE', 'Tür von Sik''ness', '', NULL, 23222),
(212205, 'deDE', 'Fass', '', NULL, 23222),
(216614, 'deDE', 'Briefkasten', '', NULL, 23222),
(213817, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(213816, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(213959, 'deDE', 'Erstarrtes Harz von Kri''vess', '', NULL, 23222),
(214759, 'deDE', 'Totemfeuer', '', NULL, 23222),
(214760, 'deDE', 'Totemfeuer', '', NULL, 23222),
(214641, 'deDE', 'Tür', '', NULL, 23222),
(215459, 'deDE', 'Doodad_Mantid_Amber_Door001', '', NULL, 23222),
(214758, 'deDE', 'Totemfeuer', '', NULL, 23222),
(212779, 'deDE', 'Entweihte Yaungolüberreste', '', NULL, 23222),
(214755, 'deDE', 'Totemfeuer', '', NULL, 23222),
(214754, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211779, 'deDE', 'Mao-Weide', '', NULL, 23222),
(211517, 'deDE', 'Knotranke', '', NULL, 23222),
(213848, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(211719, 'deDE', 'Zitronenveilchen', '', NULL, 23222),
(214741, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211721, 'deDE', 'Zitronenveilchen', '', NULL, 23222),
(212444, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(212442, 'deDE', 'Schmiede', '', NULL, 23222),
(212441, 'deDE', 'Amboss', '', NULL, 23222),
(214753, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211524, 'deDE', 'Zeremonieller Haufen', '', NULL, 23222),
(211512, 'deDE', 'Fackel von Osul', '', NULL, 23222),
(211553, 'deDE', 'Trockenholzkäfig', '', NULL, 23222),
(214749, 'deDE', 'Totemfeuer', '', NULL, 23222),
(213961, 'deDE', 'Zurückgelassene Warenkiste', '', NULL, 23222),
(214750, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211505, 'deDE', 'Kochtopf von Osul', '', NULL, 23222),
(211506, 'deDE', 'Topf mit Pech', '', NULL, 23222),
(214752, 'deDE', 'Totemfeuer', '', NULL, 23222),
(211507, 'deDE', 'Topf mit Pech', '', NULL, 23222),
(212973, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212445, 'deDE', 'Briefkasten', '', NULL, 23222),
(214972, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214973, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214970, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214971, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(214740, 'deDE', 'Totemfeuer', '', NULL, 23222),
(213852, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(212161, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(213420, 'deDE', 'Gefangen in einem fremden Land', '', NULL, 23222),
(211687, 'deDE', 'Stachelfalle der Shado-Pan', '', NULL, 23222);


DELETE FROM `locales_creature_text` WHERE (`entry`=50772 AND `groupid`=0) OR (`entry`=50791 AND `groupid`=0) OR (`entry`=61567 AND `groupid`=0) OR (`entry`=63128 AND `groupid`=0) OR (`entry`=63136 AND `groupid`=0);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(50772, 0, 0, '', '', 'Die Wasser prophezeien Euren Untergang...', '', '', '', '', ''),
(50791, 0, 0, '', '', 'Ich werde Eure Haut als Kopfbedeckung tragen!', '', '', '', '', ''),
(61567, 0, 0, '', '', 'Ah-hah! Ihr werdet Euch schon bald wünschen, nicht hierhergekommen zu sein...', '', '', '', '', ''),
(63128, 0, 0, '', '', 'Greift mich an!', '', '', '', '', ''),
(63136, 0, 0, '', '', 'Ich hoffe, Eure Manöver sind nicht zu vorhersehbar.', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE (`entry`=62539 /*62539*/ AND `locale`='deDE') OR (`entry`=62792 /*62792*/ AND `locale`='deDE') OR (`entry`=62791 /*62791*/ AND `locale`='deDE') OR (`entry`=62810 /*62810*/ AND `locale`='deDE') OR (`entry`=65373 /*65373*/ AND `locale`='deDE') OR (`entry`=62790 /*62790*/ AND `locale`='deDE') OR (`entry`=62789 /*62789*/ AND `locale`='deDE') OR (`entry`=62599 /*62599*/ AND `locale`='deDE') OR (`entry`=62598 /*62598*/ AND `locale`='deDE') OR (`entry`=62267 /*62267*/ AND `locale`='deDE') OR (`entry`=62597 /*62597*/ AND `locale`='deDE') OR (`entry`=66467 /*66467*/ AND `locale`='deDE') OR (`entry`=62559 /*62559*/ AND `locale`='deDE') OR (`entry`=62530 /*62530*/ AND `locale`='deDE') OR (`entry`=62266 /*62266*/ AND `locale`='deDE') OR (`entry`=62309 /*62309*/ AND `locale`='deDE') OR (`entry`=62586 /*62586*/ AND `locale`='deDE') OR (`entry`=62585 /*62585*/ AND `locale`='deDE') OR (`entry`=62584 /*62584*/ AND `locale`='deDE') OR (`entry`=62311 /*62311*/ AND `locale`='deDE') OR (`entry`=62567 /*62567*/ AND `locale`='deDE') OR (`entry`=66593 /*66593*/ AND `locale`='deDE') OR (`entry`=62448 /*62448*/ AND `locale`='deDE') OR (`entry`=62293 /*62293*/ AND `locale`='deDE') OR (`entry`=62440 /*62440*/ AND `locale`='deDE') OR (`entry`=62522 /*62522*/ AND `locale`='deDE') OR (`entry`=62515 /*62515*/ AND `locale`='deDE') OR (`entry`=62520 /*62520*/ AND `locale`='deDE') OR (`entry`=62457 /*62457*/ AND `locale`='deDE') OR (`entry`=62268 /*62268*/ AND `locale`='deDE') OR (`entry`=66162 /*66162*/ AND `locale`='deDE') OR (`entry`=63707 /*63707*/ AND `locale`='deDE') OR (`entry`=63706 /*63706*/ AND `locale`='deDE') OR (`entry`=63714 /*63714*/ AND `locale`='deDE') OR (`entry`=63713 /*63713*/ AND `locale`='deDE') OR (`entry`=63712 /*63712*/ AND `locale`='deDE') OR (`entry`=63711 /*63711*/ AND `locale`='deDE') OR (`entry`=63710 /*63710*/ AND `locale`='deDE') OR (`entry`=63693 /*63693*/ AND `locale`='deDE') OR (`entry`=63686 /*63686*/ AND `locale`='deDE') OR (`entry`=63694 /*63694*/ AND `locale`='deDE') OR (`entry`=63709 /*63709*/ AND `locale`='deDE') OR (`entry`=63708 /*63708*/ AND `locale`='deDE') OR (`entry`=63685 /*63685*/ AND `locale`='deDE') OR (`entry`=63688 /*63688*/ AND `locale`='deDE') OR (`entry`=63696 /*63696*/ AND `locale`='deDE') OR (`entry`=63697 /*63697*/ AND `locale`='deDE') OR (`entry`=64297 /*64297*/ AND `locale`='deDE') OR (`entry`=63684 /*63684*/ AND `locale`='deDE') OR (`entry`=63681 /*63681*/ AND `locale`='deDE') OR (`entry`=63678 /*63678*/ AND `locale`='deDE') OR (`entry`=63683 /*63683*/ AND `locale`='deDE') OR (`entry`=63680 /*63680*/ AND `locale`='deDE') OR (`entry`=62684 /*62684*/ AND `locale`='deDE') OR (`entry`=61567 /*61567*/ AND `locale`='deDE') OR (`entry`=66699 /*66699*/ AND `locale`='deDE') OR (`entry`=61670 /*61670*/ AND `locale`='deDE') OR (`entry`=63106 /*63106*/ AND `locale`='deDE') OR (`entry`=57478 /*57478*/ AND `locale`='deDE') OR (`entry`=61817 /*61817*/ AND `locale`='deDE') OR (`entry`=62794 /*62794*/ AND `locale`='deDE') OR (`entry`=61812 /*61812*/ AND `locale`='deDE') OR (`entry`=62795 /*62795*/ AND `locale`='deDE') OR (`entry`=61620 /*61620*/ AND `locale`='deDE') OR (`entry`=62091 /*62091*/ AND `locale`='deDE') OR (`entry`=61613 /*61613*/ AND `locale`='deDE') OR (`entry`=64517 /*64517*/ AND `locale`='deDE') OR (`entry`=61910 /*61910*/ AND `locale`='deDE') OR (`entry`=61629 /*61629*/ AND `locale`='deDE') OR (`entry`=61929 /*61929*/ AND `locale`='deDE') OR (`entry`=61965 /*61965*/ AND `locale`='deDE') OR (`entry`=61964 /*61964*/ AND `locale`='deDE') OR (`entry`=61928 /*61928*/ AND `locale`='deDE') OR (`entry`=61967 /*61967*/ AND `locale`='deDE') OR (`entry`=66479 /*66479*/ AND `locale`='deDE') OR (`entry`=61163 /*61163*/ AND `locale`='deDE') OR (`entry`=61162 /*61162*/ AND `locale`='deDE') OR (`entry`=61161 /*61161*/ AND `locale`='deDE') OR (`entry`=61706 /*61706*/ AND `locale`='deDE') OR (`entry`=61585 /*61585*/ AND `locale`='deDE') OR (`entry`=61584 /*61584*/ AND `locale`='deDE') OR (`entry`=61583 /*61583*/ AND `locale`='deDE') OR (`entry`=61581 /*61581*/ AND `locale`='deDE') OR (`entry`=61580 /*61580*/ AND `locale`='deDE') OR (`entry`=61683 /*61683*/ AND `locale`='deDE') OR (`entry`=66926 /*66926*/ AND `locale`='deDE') OR (`entry`=66925 /*66925*/ AND `locale`='deDE') OR (`entry`=66923 /*66923*/ AND `locale`='deDE') OR (`entry`=66918 /*66918*/ AND `locale`='deDE') OR (`entry`=61685 /*61685*/ AND `locale`='deDE') OR (`entry`=61592 /*61592*/ AND `locale`='deDE') OR (`entry`=61591 /*61591*/ AND `locale`='deDE') OR (`entry`=61540 /*61540*/ AND `locale`='deDE') OR (`entry`=70346 /*70346*/ AND `locale`='deDE') OR (`entry`=64607 /*64607*/ AND `locale`='deDE') OR (`entry`=64606 /*64606*/ AND `locale`='deDE') OR (`entry`=63677 /*63677*/ AND `locale`='deDE') OR (`entry`=62277 /*62277*/ AND `locale`='deDE') OR (`entry`=62276 /*62276*/ AND `locale`='deDE') OR (`entry`=62306 /*62306*/ AND `locale`='deDE') OR (`entry`=66597 /*66597*/ AND `locale`='deDE') OR (`entry`=62307 /*62307*/ AND `locale`='deDE') OR (`entry`=62282 /*62282*/ AND `locale`='deDE') OR (`entry`=62281 /*62281*/ AND `locale`='deDE') OR (`entry`=62571 /*62571*/ AND `locale`='deDE') OR (`entry`=61635 /*61635*/ AND `locale`='deDE') OR (`entry`=61517 /*61517*/ AND `locale`='deDE') OR (`entry`=61522 /*61522*/ AND `locale`='deDE') OR (`entry`=61516 /*61516*/ AND `locale`='deDE') OR (`entry`=61518 /*61518*/ AND `locale`='deDE') OR (`entry`=61570 /*61570*/ AND `locale`='deDE') OR (`entry`=61311 /*61311*/ AND `locale`='deDE') OR (`entry`=61508 /*61508*/ AND `locale`='deDE') OR (`entry`=61502 /*61502*/ AND `locale`='deDE') OR (`entry`=61093 /*61093*/ AND `locale`='deDE') OR (`entry`=65525 /*65525*/ AND `locale`='deDE') OR (`entry`=63196 /*63196*/ AND `locale`='deDE') OR (`entry`=63136 /*63136*/ AND `locale`='deDE') OR (`entry`=99526 /*99526*/ AND `locale`='deDE') OR (`entry`=63128 /*63128*/ AND `locale`='deDE') OR (`entry`=65685 /*65685*/ AND `locale`='deDE') OR (`entry`=65584 /*65584*/ AND `locale`='deDE') OR (`entry`=66319 /*66319*/ AND `locale`='deDE') OR (`entry`=61692 /*61692*/ AND `locale`='deDE') OR (`entry`=66184 /*66184*/ AND `locale`='deDE') OR (`entry`=66194 /*66194*/ AND `locale`='deDE') OR (`entry`=61066 /*61066*/ AND `locale`='deDE') OR (`entry`=61067 /*61067*/ AND `locale`='deDE') OR (`entry`=67170 /*67170*/ AND `locale`='deDE') OR (`entry`=62875 /*62875*/ AND `locale`='deDE') OR (`entry`=61470 /*61470*/ AND `locale`='deDE') OR (`entry`=66247 /*66247*/ AND `locale`='deDE') OR (`entry`=67171 /*67171*/ AND `locale`='deDE') OR (`entry`=62903 /*62903*/ AND `locale`='deDE') OR (`entry`=61018 /*61018*/ AND `locale`='deDE') OR (`entry`=61083 /*61083*/ AND `locale`='deDE') OR (`entry`=61019 /*61019*/ AND `locale`='deDE') OR (`entry`=61754 /*61754*/ AND `locale`='deDE') OR (`entry`=61746 /*61746*/ AND `locale`='deDE') OR (`entry`=61702 /*61702*/ AND `locale`='deDE') OR (`entry`=61181 /*61181*/ AND `locale`='deDE') OR (`entry`=61881 /*61881*/ AND `locale`='deDE') OR (`entry`=61880 /*61880*/ AND `locale`='deDE') OR (`entry`=61365 /*61365*/ AND `locale`='deDE') OR (`entry`=62759 /*62759*/ AND `locale`='deDE') OR (`entry`=62876 /*62876*/ AND `locale`='deDE') OR (`entry`=62270 /*62270*/ AND `locale`='deDE') OR (`entry`=66315 /*66315*/ AND `locale`='deDE') OR (`entry`=61364 /*61364*/ AND `locale`='deDE') OR (`entry`=61376 /*61376*/ AND `locale`='deDE') OR (`entry`=61377 /*61377*/ AND `locale`='deDE') OR (`entry`=60700 /*60700*/ AND `locale`='deDE') OR (`entry`=60698 /*60698*/ AND `locale`='deDE') OR (`entry`=60683 /*60683*/ AND `locale`='deDE') OR (`entry`=68562 /*68562*/ AND `locale`='deDE') OR (`entry`=62204 /*62204*/ AND `locale`='deDE') OR (`entry`=62124 /*62124*/ AND `locale`='deDE') OR (`entry`=61299 /*61299*/ AND `locale`='deDE') OR (`entry`=61818 /*61818*/ AND `locale`='deDE') OR (`entry`=61302 /*61302*/ AND `locale`='deDE') OR (`entry`=61811 /*61811*/ AND `locale`='deDE') OR (`entry`=66309 /*66309*/ AND `locale`='deDE') OR (`entry`=62122 /*62122*/ AND `locale`='deDE') OR (`entry`=61016 /*61016*/ AND `locale`='deDE') OR (`entry`=62123 /*62123*/ AND `locale`='deDE') OR (`entry`=62579 /*62579*/ AND `locale`='deDE') OR (`entry`=65187 /*65187*/ AND `locale`='deDE') OR (`entry`=62580 /*62580*/ AND `locale`='deDE') OR (`entry`=63980 /*63980*/ AND `locale`='deDE') OR (`entry`=67173 /*67173*/ AND `locale`='deDE') OR (`entry`=62278 /*62278*/ AND `locale`='deDE') OR (`entry`=62898 /*62898*/ AND `locale`='deDE') OR (`entry`=61971 /*61971*/ AND `locale`='deDE') OR (`entry`=64242 /*64242*/ AND `locale`='deDE') OR (`entry`=64804 /*64804*/ AND `locale`='deDE') OR (`entry`=62253 /*62253*/ AND `locale`='deDE') OR (`entry`=62573 /*62573*/ AND `locale`='deDE') OR (`entry`=62248 /*62248*/ AND `locale`='deDE') OR (`entry`=61020 /*61020*/ AND `locale`='deDE') OR (`entry`=62747 /*62747*/ AND `locale`='deDE') OR (`entry`=62125 /*62125*/ AND `locale`='deDE') OR (`entry`=62420 /*62420*/ AND `locale`='deDE') OR (`entry`=65178 /*65178*/ AND `locale`='deDE') OR (`entry`=62126 /*62126*/ AND `locale`='deDE') OR (`entry`=62128 /*62128*/ AND `locale`='deDE') OR (`entry`=62767 /*62767*/ AND `locale`='deDE') OR (`entry`=62768 /*62768*/ AND `locale`='deDE') OR (`entry`=66938 /*66938*/ AND `locale`='deDE') OR (`entry`=64891 /*64891*/ AND `locale`='deDE') OR (`entry`=62581 /*62581*/ AND `locale`='deDE') OR (`entry`=61354 /*61354*/ AND `locale`='deDE') OR (`entry`=62576 /*62576*/ AND `locale`='deDE') OR (`entry`=62575 /*62575*/ AND `locale`='deDE') OR (`entry`=70358 /*70358*/ AND `locale`='deDE') OR (`entry`=67676 /*67676*/ AND `locale`='deDE') OR (`entry`=62978 /*62978*/ AND `locale`='deDE') OR (`entry`=62909 /*62909*/ AND `locale`='deDE') OR (`entry`=66409 /*66409*/ AND `locale`='deDE') OR (`entry`=66248 /*66248*/ AND `locale`='deDE') OR (`entry`=63616 /*63616*/ AND `locale`='deDE') OR (`entry`=63617 /*63617*/ AND `locale`='deDE') OR (`entry`=63618 /*63618*/ AND `locale`='deDE') OR (`entry`=64595 /*64595*/ AND `locale`='deDE') OR (`entry`=62874 /*62874*/ AND `locale`='deDE') OR (`entry`=62305 /*62305*/ AND `locale`='deDE') OR (`entry`=62546 /*62546*/ AND `locale`='deDE') OR (`entry`=62550 /*62550*/ AND `locale`='deDE') OR (`entry`=61482 /*61482*/ AND `locale`='deDE') OR (`entry`=63614 /*63614*/ AND `locale`='deDE') OR (`entry`=63009 /*63009*/ AND `locale`='deDE') OR (`entry`=61625 /*61625*/ AND `locale`='deDE') OR (`entry`=62354 /*62354*/ AND `locale`='deDE') OR (`entry`=62380 /*62380*/ AND `locale`='deDE') OR (`entry`=62303 /*62303*/ AND `locale`='deDE') OR (`entry`=62308 /*62308*/ AND `locale`='deDE') OR (`entry`=62304 /*62304*/ AND `locale`='deDE') OR (`entry`=70360 /*70360*/ AND `locale`='deDE') OR (`entry`=63888 /*63888*/ AND `locale`='deDE') OR (`entry`=63875 /*63875*/ AND `locale`='deDE') OR (`entry`=66316 /*66316*/ AND `locale`='deDE') OR (`entry`=62844 /*62844*/ AND `locale`='deDE') OR (`entry`=62679 /*62679*/ AND `locale`='deDE') OR (`entry`=62677 /*62677*/ AND `locale`='deDE') OR (`entry`=66882 /*66882*/ AND `locale`='deDE') OR (`entry`=62613 /*62613*/ AND `locale`='deDE') OR (`entry`=68463 /*68463*/ AND `locale`='deDE') OR (`entry`=62568 /*62568*/ AND `locale`='deDE') OR (`entry`=62589 /*62589*/ AND `locale`='deDE') OR (`entry`=62577 /*62577*/ AND `locale`='deDE') OR (`entry`=62602 /*62602*/ AND `locale`='deDE') OR (`entry`=62572 /*62572*/ AND `locale`='deDE') OR (`entry`=66463 /*66463*/ AND `locale`='deDE') OR (`entry`=61618 /*61618*/ AND `locale`='deDE') OR (`entry`=66462 /*66462*/ AND `locale`='deDE') OR (`entry`=66514 /*66514*/ AND `locale`='deDE') OR (`entry`=61619 /*61619*/ AND `locale`='deDE') OR (`entry`=60857 /*60857*/ AND `locale`='deDE') OR (`entry`=60686 /*60686*/ AND `locale`='deDE') OR (`entry`=60697 /*60697*/ AND `locale`='deDE') OR (`entry`=60862 /*60862*/ AND `locale`='deDE') OR (`entry`=60733 /*60733*/ AND `locale`='deDE') OR (`entry`=46464 /*46464*/ AND `locale`='deDE') OR (`entry`=62438 /*62438*/ AND `locale`='deDE') OR (`entry`=63957 /*63957*/ AND `locale`='deDE') OR (`entry`=62439 /*62439*/ AND `locale`='deDE') OR (`entry`=61467 /*61467*/ AND `locale`='deDE') OR (`entry`=61082 /*61082*/ AND `locale`='deDE') OR (`entry`=61017 /*61017*/ AND `locale`='deDE') OR (`entry`=60802 /*60802*/ AND `locale`='deDE') OR (`entry`=61374 /*61374*/ AND `locale`='deDE') OR (`entry`=61561 /*61561*/ AND `locale`='deDE') OR (`entry`=62873 /*62873*/ AND `locale`='deDE') OR (`entry`=60903 /*60903*/ AND `locale`='deDE') OR (`entry`=61173 /*61173*/ AND `locale`='deDE') OR (`entry`=60736 /*60736*/ AND `locale`='deDE') OR (`entry`=60735 /*60735*/ AND `locale`='deDE') OR (`entry`=60899 /*60899*/ AND `locale`='deDE') OR (`entry`=60726 /*60726*/ AND `locale`='deDE') OR (`entry`=61010 /*61010*/ AND `locale`='deDE') OR (`entry`=66310 /*66310*/ AND `locale`='deDE') OR (`entry`=60739 /*60739*/ AND `locale`='deDE') OR (`entry`=60669 /*60669*/ AND `locale`='deDE') OR (`entry`=60647 /*60647*/ AND `locale`='deDE') OR (`entry`=60713 /*60713*/ AND `locale`='deDE') OR (`entry`=62737 /*62737*/ AND `locale`='deDE') OR (`entry`=60864 /*60864*/ AND `locale`='deDE') OR (`entry`=60687 /*60687*/ AND `locale`='deDE') OR (`entry`=60690 /*60690*/ AND `locale`='deDE') OR (`entry`=60684 /*60684*/ AND `locale`='deDE') OR (`entry`=61021 /*61021*/ AND `locale`='deDE') OR (`entry`=60688 /*60688*/ AND `locale`='deDE') OR (`entry`=62901 /*62901*/ AND `locale`='deDE') OR (`entry`=66246 /*66246*/ AND `locale`='deDE') OR (`entry`=65171 /*65171*/ AND `locale`='deDE') OR (`entry`=62396 /*62396*/ AND `locale`='deDE') OR (`entry`=61153 /*61153*/ AND `locale`='deDE') OR (`entry`=60841 /*60841*/ AND `locale`='deDE') OR (`entry`=60689 /*60689*/ AND `locale`='deDE') OR (`entry`=78461 /*78461*/ AND `locale`='deDE') OR (`entry`=75695 /*75695*/ AND `locale`='deDE') OR (`entry`=75688 /*75688*/ AND `locale`='deDE') OR (`entry`=75693 /*75693*/ AND `locale`='deDE') OR (`entry`=65863 /*65863*/ AND `locale`='deDE') OR (`entry`=75690 /*75690*/ AND `locale`='deDE') OR (`entry`=65190 /*65190*/ AND `locale`='deDE') OR (`entry`=63954 /*63954*/ AND `locale`='deDE') OR (`entry`=66448 /*66448*/ AND `locale`='deDE') OR (`entry`=66418 /*66418*/ AND `locale`='deDE') OR (`entry`=66421 /*66421*/ AND `locale`='deDE') OR (`entry`=60951 /*60951*/ AND `locale`='deDE') OR (`entry`=60929 /*60929*/ AND `locale`='deDE') OR (`entry`=65192 /*65192*/ AND `locale`='deDE') OR (`entry`=64807 /*64807*/ AND `locale`='deDE');
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(62539, 'deDE', 'Kugel und Kette', NULL, NULL, '', 23222), -- 62539
(62792, 'deDE', 'Versorgerin Gira', NULL, NULL, NULL, 23222), -- 62792
(62791, 'deDE', 'Versorger Bao', NULL, NULL, NULL, 23222), -- 62791
(62810, 'deDE', 'Moshu der Arkane', NULL, NULL, NULL, 23222), -- 62810
(65373, 'deDE', 'Li Vera', NULL, NULL, NULL, 23222), -- 65373
(62790, 'deDE', 'Omniapriester', 'Omniapriesterin', NULL, NULL, 23222), -- 62790
(62789, 'deDE', 'Omniamagier', 'Omniamagierin', NULL, NULL, 23222), -- 62789
(62599, 'deDE', 'Wilder Wolkenreiter', NULL, NULL, NULL, 23222), -- 62599
(62598, 'deDE', 'Wilder Wolkenreiter', NULL, NULL, NULL, 23222), -- 62598
(62267, 'deDE', 'Wilder Wolkenreiter', NULL, NULL, NULL, 23222), -- 62267
(62597, 'deDE', 'Wilder Wolkenreiter', NULL, NULL, NULL, 23222), -- 62597
(66467, 'deDE', 'G''nathus', NULL, NULL, NULL, 23222), -- 66467
(62559, 'deDE', 'Todessprecher der Shan''ze', NULL, NULL, NULL, 23222), -- 62559
(62530, 'deDE', 'Bestienmeister der Shan''ze', NULL, NULL, NULL, 23222), -- 62530
(62266, 'deDE', 'Bronzeqilen', NULL, NULL, NULL, 23222), -- 62266
(62309, 'deDE', 'Hei Feng', NULL, NULL, NULL, 23222), -- 62309
(62586, 'deDE', 'Wolkenreiter der Shan''ze', NULL, NULL, NULL, 23222), -- 62586
(62585, 'deDE', 'Wolkenreiter der Shan''ze', NULL, NULL, NULL, 23222), -- 62585
(62584, 'deDE', 'Wolkenreiter der Shan''ze', NULL, NULL, NULL, 23222), -- 62584
(62311, 'deDE', 'Wolkenreiter der Shan''ze', NULL, NULL, NULL, 23222), -- 62311
(62567, 'deDE', 'Wolkenreiterjungtier', NULL, NULL, NULL, 23222), -- 62567
(66593, 'deDE', 'Uralter Leviathan', NULL, NULL, NULL, 23222), -- 66593
(62448, 'deDE', 'Onyxsturmklaue', NULL, NULL, NULL, 23222), -- 62448
(62293, 'deDE', 'Schlangenbinder der Shan''ze', NULL, NULL, NULL, 23222), -- 62293
(62440, 'deDE', 'Illusionist der Shan''ze', NULL, NULL, NULL, 23222), -- 62440
(62522, 'deDE', 'Dunkelholzfee', NULL, NULL, NULL, 23222), -- 62522
(62515, 'deDE', 'Bestienmeister der Shan''ze', NULL, NULL, NULL, 23222), -- 62515
(62520, 'deDE', 'Dunkelholzirrlicht', NULL, NULL, NULL, 23222), -- 62520
(62457, 'deDE', 'Dunkelholzirrlicht', NULL, NULL, NULL, 23222), -- 62457
(62268, 'deDE', 'Dunkelholzpixie', NULL, NULL, NULL, 23222), -- 62268
(66162, 'deDE', 'Scotty', NULL, NULL, NULL, 23222), -- 66162
(63707, 'deDE', 'Building 2 Credit', NULL, NULL, NULL, 23222), -- 63707
(63706, 'deDE', 'Building 1 Credit', NULL, NULL, NULL, 23222), -- 63706
(63714, 'deDE', 'Building 9 Credit', NULL, NULL, NULL, 23222), -- 63714
(63713, 'deDE', 'Building 8 Credit', NULL, NULL, NULL, 23222), -- 63713
(63712, 'deDE', 'Building 7 Credit', NULL, NULL, NULL, 23222), -- 63712
(63711, 'deDE', 'Building 6 Credit', NULL, NULL, NULL, 23222), -- 63711
(63710, 'deDE', 'Building 5 Credit', NULL, NULL, NULL, 23222), -- 63710
(63693, 'deDE', 'Schwarmfürst der Sra''thik', NULL, NULL, NULL, 23222), -- 63693
(63686, 'deDE', 'Vesswache der Sra''thik', NULL, NULL, NULL, 23222), -- 63686
(63694, 'deDE', 'Brutfürst der Sra''thik', NULL, NULL, NULL, 23222), -- 63694
(63709, 'deDE', 'Building 4 Credit', NULL, NULL, NULL, 23222), -- 63709
(63708, 'deDE', 'Building 3 Credit', NULL, NULL, NULL, 23222), -- 63708
(63685, 'deDE', 'Schwarmführer der Sra''thik', NULL, NULL, NULL, 23222), -- 63685
(63688, 'deDE', 'Todesmischer der Sra''thik', NULL, NULL, NULL, 23222), -- 63688
(63696, 'deDE', 'Gefräßiger Kunchong', NULL, NULL, NULL, 23222), -- 63696
(63697, 'deDE', 'Heranwachsender Kunchong', NULL, NULL, NULL, 23222), -- 63697
(64297, 'deDE', 'Townlong Steppes - Sra''vess - Amber Channel Target', NULL, NULL, NULL, 23222), -- 64297
(63684, 'deDE', 'Willensbrecher der Sra''thik', NULL, NULL, NULL, 23222), -- 63684
(63681, 'deDE', 'Erneuerer der Sra''thik', NULL, NULL, NULL, 23222), -- 63681
(63678, 'deDE', 'Verstümmler der Sra''thik', NULL, NULL, NULL, 23222), -- 63678
(63683, 'deDE', 'Flinkschwinge der Sra''thik', NULL, NULL, NULL, 23222), -- 63683
(63680, 'deDE', 'Kakophyt der Sra''thik', NULL, NULL, NULL, 23222), -- 63680
(62684, 'deDE', 'Fassziel', NULL, NULL, NULL, 23222), -- 62684
(61567, 'deDE', 'Wesir Jin''bak', NULL, NULL, NULL, 23222), -- 61567
(66699, 'deDE', 'Generic Invisible Stalker Controller NonImmune - IH', NULL, NULL, NULL, 23222), -- 66699
(61670, 'deDE', 'Verwüster der Sik''thik', NULL, NULL, NULL, 23222), -- 61670
(63106, 'deDE', 'Schwärmer der Sik''thik', NULL, NULL, NULL, 23222), -- 63106
(57478, 'deDE', 'Unsichtbarer Pirscher', NULL, NULL, NULL, 23222), -- 57478
(61817, 'deDE', 'Mantisteerfass', NULL, NULL, NULL, 23222), -- 61817
(62794, 'deDE', 'Lo Chu', NULL, NULL, NULL, 23222), -- 62794
(61812, 'deDE', 'Li Chu', NULL, NULL, NULL, 23222), -- 61812
(62795, 'deDE', 'Wärter der Sik''thik', NULL, NULL, NULL, 23222), -- 62795
(61620, 'deDE', 'Yang Eisenklaue', NULL, NULL, NULL, 23222), -- 61620
(62091, 'deDE', 'Flieger der Sik''thik', NULL, NULL, NULL, 23222), -- 62091
(61613, 'deDE', 'Saftpfütze', NULL, NULL, NULL, 23222), -- 61613
(64517, 'deDE', 'Shado-Meister Chum Kiu', NULL, NULL, NULL, 23222), -- 64517
(61910, 'deDE', 'Harzflocke', NULL, NULL, NULL, 23222), -- 61910
(61629, 'deDE', 'Sappling Summon Dest', NULL, NULL, NULL, 23222), -- 61629
(61929, 'deDE', 'Bernweber der Sik''thik', NULL, NULL, NULL, 23222), -- 61929
(61965, 'deDE', 'Saftpfütze', NULL, NULL, NULL, 23222), -- 61965
(61964, 'deDE', 'Saftspritzer', NULL, NULL, NULL, 23222), -- 61964
(61928, 'deDE', 'Wächter der Sik''thik', NULL, NULL, NULL, 23222), -- 61928
(61967, 'deDE', 'Resin Stalker', NULL, NULL, NULL, 23222), -- 61967
(66479, 'deDE', 'Riffdrescher', NULL, NULL, NULL, 23222), -- 66479
(61163, 'deDE', 'Ruthers', NULL, NULL, NULL, 23222), -- 61163
(61162, 'deDE', 'Cousin Steinchenbeutel', NULL, NULL, NULL, 23222), -- 61162
(61161, 'deDE', 'Blausattel', NULL, NULL, NULL, 23222), -- 61161
(61706, 'deDE', 'Tai Ho', NULL, NULL, NULL, 23222), -- 61706
(61585, 'deDE', 'Yakhüterin Kyana', NULL, NULL, NULL, 23222), -- 61585
(61584, 'deDE', 'Bewacherkommandant Qipan', NULL, NULL, NULL, 23222), -- 61584
(61583, 'deDE', 'Hochadept Paosha', NULL, NULL, NULL, 23222), -- 61583
(61581, 'deDE', 'Ogo der Jüngere', NULL, NULL, NULL, 23222), -- 61581
(61580, 'deDE', 'Ogo der Ältere', NULL, NULL, NULL, 23222), -- 61580
(61683, 'deDE', 'Späher Yalo', NULL, NULL, '', 23222), -- 61683
(66926, 'deDE', 'Diamant', NULL, NULL, NULL, 23222), -- 66926
(66925, 'deDE', 'Mollus', NULL, NULL, NULL, 23222), -- 66925
(66923, 'deDE', 'Gleiter', NULL, NULL, NULL, 23222), -- 66923
(66918, 'deDE', 'Sucher Zusshi', NULL, 'Großmeistertierzähmer', NULL, 23222), -- 66918
(61685, 'deDE', 'Ha-Cha', NULL, NULL, NULL, 23222), -- 61685
(61592, 'deDE', 'Mundu', NULL, NULL, NULL, 23222), -- 61592
(61591, 'deDE', 'Jumoy', NULL, NULL, NULL, 23222), -- 61591
(61540, 'deDE', 'Et''kil', NULL, NULL, NULL, 23222), -- 61540
(70346, 'deDE', 'Ao Pye', NULL, 'Rüstmeisterin des Shado-Pan-Vorstoßes', NULL, 23222), -- 70346
(64607, 'deDE', 'Kommandant Lo Ping', NULL, 'Rüstmeister für Gerechtigkeitspunkte', NULL, 23222), -- 64607
(64606, 'deDE', 'Kommandantin Ochsenherz', NULL, 'Rüstmeisterin für Tapferkeitspunkte', NULL, 23222), -- 64606
(63677, 'deDE', 'Drohne der Sra''thik', NULL, NULL, NULL, 23222), -- 63677
(62277, 'deDE', 'Angsterfüllte Schildwache', NULL, NULL, NULL, 23222), -- 62277
(62276, 'deDE', 'Angsterfüllte Schildwache', NULL, NULL, '', 23222), -- 62276
(62306, 'deDE', 'Schreckling', NULL, NULL, NULL, 23222), -- 62306
(66597, 'deDE', 'Terrorling', NULL, NULL, NULL, 23222), -- 66597
(62307, 'deDE', 'Schreckensschatten', NULL, NULL, NULL, 23222), -- 62307
(62282, 'deDE', 'Angsterfüllte Schildwache', NULL, NULL, NULL, 23222), -- 62282
(62281, 'deDE', 'Angsterfüllte Schildwache', NULL, NULL, '', 23222), -- 62281
(62571, 'deDE', 'Weitenpirscher von Osul', NULL, NULL, NULL, 23222), -- 62571
(61635, 'deDE', 'Wildes Tonlongyak', NULL, NULL, NULL, 23222), -- 61635
(61517, 'deDE', 'Wundenheiler der Sra''thik', NULL, NULL, NULL, 23222), -- 61517
(61522, 'deDE', 'Verletzer Soldat der Sra''thik', NULL, NULL, NULL, 23222), -- 61522
(61516, 'deDE', 'Drohne der Sra''thik', NULL, NULL, NULL, 23222), -- 61516
(61518, 'deDE', 'Wache der Sra''thik', NULL, NULL, NULL, 23222), -- 61518
(61570, 'deDE', 'Verwundeter Niuzaobewacher', 'Verwundete Niuzaobewacherin', NULL, NULL, 23222), -- 61570
(61311, 'deDE', 'Niuzaobewacher', 'Niuzaobewacherin', NULL, NULL, 23222), -- 61311
(61508, 'deDE', 'Flinkkralle der Sra''thik', NULL, NULL, NULL, 23222), -- 61508
(61502, 'deDE', 'Kriegsrufer der Sra''thik', NULL, NULL, NULL, 23222), -- 61502
(61093, 'deDE', 'Niuzao', NULL, 'Der Schwarze Ochse', NULL, 23222), -- 61093
(65525, 'deDE', 'Windstoßvortex', NULL, NULL, NULL, 23222), -- 65525
(63196, 'deDE', 'Schwarzwachenkampfmeister', NULL, NULL, NULL, 23222), -- 63196
(63136, 'deDE', 'Schwarzwachenbraumeister', NULL, NULL, NULL, 23222), -- 63136
(99526, 'deDE', 'Leylinienbrutling', NULL, NULL, '', 23222), -- 99526
(63128, 'deDE', 'Schwarzwachenkampfmeister', NULL, NULL, NULL, 23222), -- 63128
(65685, 'deDE', 'Räuber der Ik''thik', NULL, NULL, NULL, 23222), -- 65685
(65584, 'deDE', 'Töter der Ik''thik', NULL, NULL, NULL, 23222), -- 65584
(66319, 'deDE', 'Angstberührte Motte', NULL, NULL, NULL, 23222), -- 66319
(61692, 'deDE', 'Verletzte Gao-Ran-Schwarzwache', NULL, NULL, NULL, 23222), -- 61692
(66184, 'deDE', 'Schreckenspirscher', NULL, NULL, NULL, 23222), -- 66184
(66194, 'deDE', 'Ödnisaasfresser', NULL, NULL, NULL, 23222), -- 66194
(61066, 'deDE', 'Taran Zhu', NULL, 'Meister der Shado-Pan', NULL, 23222), -- 61066
(61067, 'deDE', 'Gao-Ran der Gemäßigte', NULL, NULL, NULL, 23222), -- 61067
(67170, 'deDE', 'Eisenformer Peng', NULL, NULL, NULL, 23222), -- 67170
(62875, 'deDE', 'Kim die Stille', NULL, 'Gastwirtin', NULL, 23222), -- 62875
(61470, 'deDE', 'Septi die Kräuterkundige', NULL, NULL, NULL, 23222), -- 61470
(66247, 'deDE', 'Tigermeister Liu-Do', NULL, 'Stallmeister', NULL, 23222), -- 66247
(67171, 'deDE', 'Lin die Mutige', NULL, 'Verbrauchsgüter', NULL, 23222), -- 67171
(62903, 'deDE', 'Drachenmeisterin Nenshi', NULL, 'Flugmeisterin', NULL, 23222), -- 62903
(61018, 'deDE', 'Gao-Ran-Schütze', 'Gao-Ran-Schützin', NULL, NULL, 23222), -- 61018
(61083, 'deDE', 'Gao-Ran-Schwarzwache', NULL, NULL, NULL, 23222), -- 61083
(61019, 'deDE', 'Gao-Ran-Magus', NULL, NULL, NULL, 23222), -- 61019
(61754, 'deDE', 'Armkneifer der Krik''thik', NULL, NULL, NULL, 23222), -- 61754
(61746, 'deDE', 'Drachenschleuder', NULL, NULL, NULL, 23222), -- 61746
(61702, 'deDE', 'Gao-Ran-Kanonier', 'Gao-Ran-Kanonierin', NULL, NULL, 23222), -- 61702
(61181, 'deDE', 'Armkneifer der Krik''thik', NULL, NULL, NULL, 23222), -- 61181
(61881, 'deDE', 'Initiand Feng', NULL, NULL, NULL, 23222), -- 61881
(61880, 'deDE', 'Initiand Xao', NULL, NULL, NULL, 23222), -- 61880
(61365, 'deDE', 'Heuschreckenwache der Krik''thik', NULL, NULL, NULL, 23222), -- 61365
(62759, 'deDE', 'Skorpid der Kor''thik', NULL, NULL, NULL, 23222), -- 62759
(62876, 'deDE', 'Siechende Hülle', NULL, NULL, 'lootall', 23222), -- 62876
(62270, 'deDE', 'Voress''thalik', NULL, NULL, NULL, 23222), -- 62270
(66315, 'deDE', 'Steinbieger der Shan''ze', NULL, NULL, NULL, 23222), -- 66315
(61364, 'deDE', 'Eggs Credit', NULL, NULL, NULL, 23222), -- 61364
(61376, 'deDE', 'Knochenschlitzer der Krik''thik', NULL, NULL, NULL, 23222), -- 61376
(61377, 'deDE', 'Schreier der Krik''thik', NULL, NULL, NULL, 23222), -- 61377
(60700, 'deDE', 'Sumpfbinsenlauerer', NULL, NULL, NULL, 23222), -- 60700
(60698, 'deDE', 'Nebelschamanenfackel', NULL, NULL, NULL, 23222), -- 60698
(60683, 'deDE', 'Dmong Naruuk', NULL, NULL, NULL, 23222), -- 60683
(68562, 'deDE', 'Ti''un der Wanderer', NULL, NULL, '', 23222), -- 68562
(62204, 'deDE', 'Dämmerlichtinitiand', 'Dämmerlichtinitiandin', NULL, NULL, 23222), -- 62204
(62124, 'deDE', 'Initiand Pao-me', NULL, NULL, NULL, 23222), -- 62124
(61299, 'deDE', 'Sengender Fleischreißer', NULL, NULL, NULL, 23222), -- 61299
(61818, 'deDE', 'Nadelrücken', NULL, NULL, NULL, 23222), -- 61818
(61302, 'deDE', 'Aufgeregter Saatdieb', NULL, NULL, NULL, 23222), -- 61302
(61811, 'deDE', 'Aufgeregte Nesselhaut', NULL, NULL, NULL, 23222), -- 61811
(66309, 'deDE', 'Ausweider der Klingenschuppen', NULL, NULL, NULL, 23222), -- 66309
(62122, 'deDE', 'Dämmerlichtscharmützler', NULL, NULL, NULL, 23222), -- 62122
(61016, 'deDE', 'Lao-Chen der eiserne Bauch', NULL, NULL, NULL, 23222), -- 61016
(62123, 'deDE', 'Dämmerlichtwaldläufer', 'Dämmerlichtwaldläuferin', NULL, NULL, 23222), -- 62123
(62579, 'deDE', 'Korvexxis', NULL, NULL, NULL, 23222), -- 62579
(65187, 'deDE', 'Bernmotte', NULL, NULL, '', 23222), -- 65187
(62580, 'deDE', 'Serevex', NULL, NULL, NULL, 23222), -- 62580
(63980, 'deDE', 'Bernmotte', NULL, NULL, NULL, 23222), -- 63980
(67173, 'deDE', 'Versorgerin Qiao', NULL, NULL, NULL, 23222), -- 67173
(62278, 'deDE', 'Rensai Eichenbalg', NULL, NULL, NULL, 23222), -- 62278
(62898, 'deDE', 'Drachenmeister Li-Sen', NULL, 'Flugmeister', NULL, 23222), -- 62898
(61971, 'deDE', 'Scharfschalenstapfer', NULL, NULL, NULL, 23222), -- 61971
(64242, 'deDE', 'Trübigel', NULL, NULL, '', 23222), -- 64242
(64804, 'deDE', 'Stiller Igel', NULL, NULL, '', 23222), -- 64804
(62253, 'deDE', 'Shado-Pan-Falke', NULL, NULL, NULL, 23222), -- 62253
(62573, 'deDE', 'Schützin Ye', NULL, NULL, NULL, 23222), -- 62573
(62248, 'deDE', 'Farwatch Landing Controller', NULL, NULL, NULL, 23222), -- 62248
(61020, 'deDE', 'Falkenmeister Nurong', NULL, NULL, NULL, 23222), -- 61020
(62747, 'deDE', 'Nurongs Kanone', NULL, NULL, '', 23222), -- 62747
(62125, 'deDE', 'Weitblickschütze', 'Weitblickschützin', NULL, NULL, 23222), -- 62125
(62420, 'deDE', 'Weißfeder', NULL, 'Falkenmeister Nurongs Begleiter', NULL, 23222), -- 62420
(65178, 'deDE', 'Adele', NULL, NULL, NULL, 23222), -- 65178
(62126, 'deDE', 'Weitblickfalkenmeister', 'Weitblickfalknerin', NULL, NULL, 23222), -- 62126
(62128, 'deDE', 'Flinkflügel der Kor''thik', NULL, NULL, NULL, 23222), -- 62128
(62767, 'deDE', 'Gokk''lok', NULL, NULL, NULL, 23222), -- 62767
(62768, 'deDE', 'Zu groß geratener Karpfen', NULL, NULL, NULL, 23222), -- 62768
(66938, 'deDE', 'Ulk''nirok', NULL, 'Seher von Kril''mandar', NULL, 23222), -- 66938
(64891, 'deDE', 'Resonating Crystal Stalker', NULL, NULL, NULL, 23222), -- 64891
(62581, 'deDE', 'Rothek', NULL, NULL, NULL, 23222), -- 62581
(61354, 'deDE', 'Vesswache der Sik''thik', NULL, NULL, NULL, 23222), -- 61354
(62576, 'deDE', 'Drohne der Sik''thik', NULL, NULL, NULL, 23222), -- 62576
(62575, 'deDE', 'Giftspucker der Sik''thik', NULL, NULL, NULL, 23222), -- 62575
(70358, 'deDE', 'Späherhauptmann Elsia', NULL, NULL, NULL, 23222), -- 70358
(67676, 'deDE', 'Drachenfalke der Sonnenhäscher', NULL, NULL, NULL, 23222), -- 67676
(62978, 'deDE', 'Lao-Chen der eiserne Bauch', NULL, NULL, NULL, 23222), -- 62978
(62909, 'deDE', 'Drachenmeisterin Yao-Li', NULL, 'Flugmeisterin', NULL, 23222), -- 62909
(66409, 'deDE', 'Lehrensucher Pao', NULL, 'Shado-Pan-Historiker', NULL, 23222), -- 66409
(66248, 'deDE', 'Tigermeisterin Min-To', NULL, 'Stallmeisterin', NULL, 23222), -- 66248
(63616, 'deDE', 'Tenwu der Rote Rauch', NULL, NULL, NULL, 23222), -- 63616
(63617, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 63617
(63618, 'deDE', 'Falkenmeister Nurong', NULL, NULL, NULL, 23222), -- 63618
(64595, 'deDE', 'Rushi der Fuchs', NULL, 'Rüstmeister der Shado-Pan', NULL, 23222), -- 64595
(62874, 'deDE', 'Kali die Nachtwächterin', NULL, 'Gastwirtin', NULL, 23222), -- 62874
(62305, 'deDE', 'Wukaoschüler', NULL, NULL, NULL, 23222), -- 62305
(62546, 'deDE', 'Beschützer Yi', NULL, NULL, NULL, 23222), -- 62546
(62550, 'deDE', 'Chao die Stimme', NULL, NULL, NULL, 23222), -- 62550
(61482, 'deDE', 'Tai Ho', NULL, NULL, NULL, 23222), -- 61482
(63614, 'deDE', 'Sechs-Pool-Ling', NULL, NULL, NULL, 23222), -- 63614
(63009, 'deDE', 'Meister Schneewehe', NULL, NULL, NULL, 23222), -- 63009
(61625, 'deDE', 'Versorger Bamfu', NULL, NULL, NULL, 23222), -- 61625
(62354, 'deDE', 'Fei Li', NULL, '"Wildfang"', NULL, 23222), -- 62354
(62380, 'deDE', 'Schneeblüte', NULL, NULL, NULL, 23222), -- 62380
(62303, 'deDE', 'Yalia Weisenwisper', NULL, NULL, NULL, 23222), -- 62303
(62308, 'deDE', 'Schwarzwachenschüler', NULL, NULL, NULL, 23222), -- 62308
(62304, 'deDE', 'Ban Bärenherz', NULL, NULL, NULL, 23222), -- 62304
(70360, 'deDE', 'Vereesa Windläufer', NULL, 'Waldläufergeneralin des Silberbunds', NULL, 23222), -- 70360
(63888, 'deDE', 'Spionagemeister der Shan''ze', NULL, NULL, NULL, 23222), -- 63888
(63875, 'deDE', 'Eindringling der Shan''ze', NULL, NULL, NULL, 23222), -- 63875
(66316, 'deDE', 'Bernfragment', NULL, NULL, NULL, 23222), -- 66316
(62844, 'deDE', 'Zermalmer des Totensprechers', NULL, NULL, NULL, 23222), -- 62844
(62679, 'deDE', 'Entweihter Geist', NULL, NULL, NULL, 23222), -- 62679
(62677, 'deDE', 'Leichenschänder des Totensprechers', NULL, NULL, NULL, 23222), -- 62677
(66882, 'deDE', 'Meerschreiter', NULL, NULL, NULL, 23222), -- 66882
(62613, 'deDE', 'Uruk', NULL, 'Der Totensprecher', NULL, 23222), -- 62613
(68463, 'deDE', 'Brennender Pandarengeist', NULL, 'Großmeistertierzähmer', NULL, 23222), -- 68463
(62568, 'deDE', 'Torwächter von Gai-Cho', NULL, NULL, NULL, 23222), -- 62568
(62589, 'deDE', 'Bolzenschütze von Gai-Cho', NULL, NULL, NULL, 23222), -- 62589
(62577, 'deDE', 'Ausbrenner von Gai-Cho', NULL, NULL, NULL, 23222), -- 62577
(62602, 'deDE', 'Kreischer der Krik''thik', NULL, NULL, NULL, 23222), -- 62602
(62572, 'deDE', 'Angreifer der Krik''thik', NULL, NULL, NULL, 23222), -- 62572
(66463, 'deDE', 'Langschattenbulle', NULL, NULL, NULL, 23222), -- 66463
(61618, 'deDE', 'Langschattenmushan', NULL, NULL, NULL, 23222), -- 61618
(66462, 'deDE', 'Ranzbissvorfahr', NULL, NULL, NULL, 23222), -- 66462
(66514, 'deDE', 'Flatternde Motte', NULL, NULL, NULL, 23222), -- 66514
(61619, 'deDE', 'Ranzbissschildkröte', NULL, NULL, NULL, 23222), -- 61619
(60857, 'deDE', 'Orbiss', NULL, NULL, NULL, 23222), -- 60857
(60686, 'deDE', 'Dmongs Fackel', NULL, NULL, NULL, 23222), -- 60686
(60697, 'deDE', 'Nebelschamane von Osul', NULL, NULL, NULL, 23222), -- 60697
(60862, 'deDE', 'Steam Bunny', NULL, NULL, NULL, 23222), -- 60862
(60733, 'deDE', 'Sumpfnagetier', NULL, NULL, NULL, 23222), -- 60733
(46464, 'deDE', 'Generic Bunny - PRK - Extra-Small', NULL, NULL, NULL, 23222), -- 46464
(62438, 'deDE', 'Verletzter Gao-Ran-Verteidiger', NULL, NULL, NULL, 23222), -- 62438
(63957, 'deDE', 'Yakratte', NULL, NULL, '', 23222), -- 63957
(62439, 'deDE', 'Verletzte Gao-Ran-Verteidigerin', NULL, NULL, NULL, 23222), -- 62439
(61467, 'deDE', 'Mao der Ausguck', NULL, NULL, NULL, 23222), -- 61467
(61082, 'deDE', 'Gao-Ran-Schwarzwache', NULL, NULL, NULL, 23222), -- 61082
(61017, 'deDE', 'Gao-Ran-Schwarzwache', NULL, NULL, NULL, 23222), -- 61017
(60802, 'deDE', 'Jahesh von Osul', NULL, NULL, NULL, 23222), -- 60802
(61374, 'deDE', 'Tiefenspäher der Krik''thik', NULL, NULL, NULL, 23222), -- 61374
(61561, 'deDE', 'Flüchtling von Bleichwind', NULL, NULL, NULL, 23222), -- 61561
(62873, 'deDE', 'Saito der schlafende Schatten', NULL, 'Gastwirt', NULL, 23222), -- 62873
(60903, 'deDE', 'Xiao Tu', NULL, NULL, NULL, 23222), -- 60903
(61173, 'deDE', 'Longying-Verteidiger', NULL, NULL, NULL, 23222), -- 61173
(60736, 'deDE', 'Chain Bunny', NULL, NULL, NULL, 23222), -- 60736
(60735, 'deDE', 'Katak der Besiegte', NULL, NULL, NULL, 23222), -- 60735
(60899, 'deDE', 'Lin Stillstoß', NULL, 'Sunas Ehemann', NULL, 23222), -- 60899
(60726, 'deDE', 'Ku-Tong', NULL, 'Champion von Osul', NULL, 23222), -- 60726
(61010, 'deDE', 'Die Klinge', NULL, NULL, NULL, 23222), -- 61010
(66310, 'deDE', 'Auslöscher der Shan''ze', NULL, NULL, NULL, 23222), -- 66310
(60739, 'deDE', 'Steppenmushan', NULL, NULL, '', 23222), -- 60739
(60669, 'deDE', 'Langhaaryak', NULL, NULL, '', 23222), -- 60669
(60647, 'deDE', 'Scharfhorn von Osul', NULL, NULL, NULL, 23222), -- 60647
(60713, 'deDE', 'Stürmer von Osul', NULL, NULL, NULL, 23222), -- 60713
(62737, 'deDE', 'Lieferant Xin', NULL, 'Rüstungsverkäufer', NULL, 23222), -- 62737
(60864, 'deDE', 'Yalia Weisenwisper', NULL, NULL, NULL, 23222), -- 60864
(60687, 'deDE', 'Ban Bärenherz', NULL, NULL, NULL, 23222), -- 60687
(60690, 'deDE', 'Longying-Bogenschütze', 'Longying-Bogenschützin', NULL, NULL, 23222), -- 60690
(60684, 'deDE', 'Suna Stillstoß', NULL, NULL, NULL, 23222), -- 60684
(61021, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 61021
(60688, 'deDE', 'Taran Zhu', NULL, 'Meister der Shado-Pan', NULL, 23222), -- 60688
(62901, 'deDE', 'Drachenmeister Wong', NULL, 'Flugmeister', NULL, 23222), -- 62901
(66246, 'deDE', 'Tigermeister Gai-Lin', NULL, 'Stallmeister', NULL, 23222), -- 66246
(65171, 'deDE', 'Alin der Finder', NULL, 'Abenteurerbedarf', NULL, 23222), -- 65171
(62396, 'deDE', 'Shado-Pan-Tiger', NULL, NULL, NULL, 23222), -- 62396
(61153, 'deDE', 'Verletzte Longying-Verteidigerin', NULL, NULL, NULL, 23222), -- 61153
(60841, 'deDE', 'Verletzter Longying-Verteidiger', NULL, NULL, NULL, 23222), -- 60841
(60689, 'deDE', 'Longying-Verteidiger', 'Longying-Verteidigerin', NULL, NULL, 23222), -- 60689
(78461, 'deDE', 'Shonn Su', NULL, 'Stolze Gladiatoren', NULL, 23222), -- 78461
(75695, 'deDE', 'Acon Gramschwinger', NULL, 'Erbitterte Gladiatoren', NULL, 23222), -- 75695
(75688, 'deDE', 'Roo Desvin', NULL, 'Tyrannische Gladiatoren', NULL, 23222), -- 75688
(75693, 'deDE', 'Doris Chiltonius', NULL, 'Bösartige Gladiatoren', NULL, 23222), -- 75693
(65863, 'deDE', 'Morla Himmelsklinge', NULL, 'Flugmeisterin', NULL, 23222), -- 65863
(75690, 'deDE', 'Lok''nor Blutfaust', NULL, 'Schreckliche Gladiatoren', NULL, 23222), -- 75690
(65190, 'deDE', 'Mungo', NULL, NULL, '', 23222), -- 65190
(63954, 'deDE', 'Mungojunges', NULL, NULL, '', 23222), -- 63954
(66448, 'deDE', 'Schleierbasilisk', NULL, NULL, NULL, 23222), -- 66448
(66418, 'deDE', 'Getüpfelte Raupe', NULL, NULL, NULL, 23222), -- 66418
(66421, 'deDE', 'Schillerhirsch', NULL, NULL, NULL, 23222), -- 66421
(60951, 'deDE', 'Shado-Pan-Waldläufer', NULL, NULL, NULL, 23222), -- 60951
(60929, 'deDE', 'Dorfoberhaupt von Bleichwind', NULL, NULL, NULL, 23222), -- 60929
(65192, 'deDE', 'Mungojunges', NULL, NULL, NULL, 23222), -- 65192
(64807, 'deDE', 'Unverwüstliche Schabe', NULL, NULL, NULL, 23222); -- 64807s


DELETE FROM `page_text_locale` WHERE (`ID`=4549 AND `locale`='deDE');
INSERT INTO `page_text_locale` (`ID`, `locale`,`Text`, `VerifiedBuild`) VALUES
(4549, 'deDE', 'An diesem Ort besiegte Shaohao, der letzte Kaiser von Pandaria, vor zehntausend Jahren das Sha der Angst und sperrte es ein.$b$bAus dem Buch der Bürden, Kapitel 14:$b$b"Obgleich Shaohao von Zweifel und Verzweiflung befreit war, überkam ihn dennoch Angst. Er suchte den Rat des Schwarzen Ochsen, Geist des Mutes und der Zuversicht, der in der Steppe jenseits der Mauer lebte."$b$b"Der Schwarze Ochse, der Rote Kranich, der Kaiser und der Affenkönig diskutierten ausführlich über die Angst, bis der Affenkönig eine Idee hatte. Eine Maske der Angst wurde gefertigt, die wahrlich schrecklich anzusehen war. Mit zitternden Händen legte der Kaiser die Maske an, die ihm seine eigene Angst entzog..."$b$bDer Kampf gegen das Sha der Angst dauerte eine Woche und einen Tag, währenddessen niemals die Sonne aufgegangen sein soll. Schließlich wurde das Sha besiegt und eingesperrt, doch der Kaiser hatte sich unwiderruflich verändert: Er konnte keine Angst mehr empfinden. Er wurde ein Wesen des Mutes.', 23222); -- 4549


--  unknown broadcasttextid
INSERT IGNORE INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `comment`) VALUES
(50772, 0, 0, 'Die Wasser prophezeien Euren Untergang...', 12, 0, 100, 0, 0, 0, 0, 'Eshelon to Player'),
(50791, 0, 0, 'Ich werde Eure Haut als Kopfbedeckung tragen!', 12, 0, 100, 0, 0, 0, 0, 'Siltriss der Schärfer to Player'),
(61567, 0, 0, 'Ah-hah! Ihr werdet Euch schon bald wünschen, nicht hierhergekommen zu sein...', 14, 0, 100, 0, 0, 29390, 0, 'Wesir Jin''bak to Player'),
(63128, 0, 0, 'Greift mich an!', 12, 0, 100, 25, 0, 30651, 0, 'Schwarzwachenkampfmeister to Player'),
(63136, 0, 0, 'Ich hoffe, Eure Manöver sind nicht zu vorhersehbar.', 12, 0, 100, 0, 0, 29801, 0, 'Schwarzwachenbraumeister to Player');


INSERT IGNORE INTO `page_text` (`ID`, `Text`, `NextPageID`, `PlayerConditionID`, `Flags`, `VerifiedBuild`) VALUES
(4549, 'An diesem Ort besiegte Shaohao, der letzte Kaiser von Pandaria, vor zehntausend Jahren das Sha der Angst und sperrte es ein.$b$bAus dem Buch der Bürden, Kapitel 14:$b$b"Obgleich Shaohao von Zweifel und Verzweiflung befreit war, überkam ihn dennoch Angst. Er suchte den Rat des Schwarzen Ochsen, Geist des Mutes und der Zuversicht, der in der Steppe jenseits der Mauer lebte."$b$b"Der Schwarze Ochse, der Rote Kranich, der Kaiser und der Affenkönig diskutierten ausführlich über die Angst, bis der Affenkönig eine Idee hatte. Eine Maske der Angst wurde gefertigt, die wahrlich schrecklich anzusehen war. Mit zitternden Händen legte der Kaiser die Maske an, die ihm seine eigene Angst entzog..."$b$bDer Kampf gegen das Sha der Angst dauerte eine Woche und einen Tag, währenddessen niemals die Sonne aufgegangen sein soll. Schließlich wurde das Sha besiegt und eingesperrt, doch der Kaiser hatte sich unwiderruflich verändert: Er konnte keine Angst mehr empfinden. Er wurde ein Wesen des Mutes.', 0, 0, 0, 23222); -- 4549