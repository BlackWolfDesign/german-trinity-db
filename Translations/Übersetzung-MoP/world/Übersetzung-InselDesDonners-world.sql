g-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_0_23222
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/13/2017 15:05:16


SET NAMES 'utf8';
DELETE FROM `quest_template_locale` WHERE (`ID`=32505 AND `locale`='deDE') OR (`ID`=32543 AND `locale`='deDE') OR (`ID`=32549 AND `locale`='deDE') OR (`ID`=32707 AND `locale`='deDE') OR (`ID`=32733 AND `locale`='deDE') OR (`ID`=32656 AND `locale`='deDE') OR (`ID`=32655 AND `locale`='deDE') OR (`ID`=32652 AND `locale`='deDE') OR (`ID`=32654 AND `locale`='deDE') OR (`ID`=32644 AND `locale`='deDE') OR (`ID`=32706 AND `locale`='deDE') OR (`ID`=32681 AND `locale`='deDE') OR (`ID`=43294 AND `locale`='deDE') OR (`ID`=43301 AND `locale`='deDE') OR (`ID`=43244 AND `locale`='deDE') OR (`ID`=43293 AND `locale`='deDE') OR (`ID`=43300 AND `locale`='deDE') OR (`ID`=43298 AND `locale`='deDE') OR (`ID`=43243 AND `locale`='deDE') OR (`ID`=43299 AND `locale`='deDE');
INSERT INTO `quest_template_locale` (`ID`, `locale`, `LogTitle`, `LogDescription`, `QuestDescription`, `AreaDescription`, `PortraitGiverText`, `PortraitGiverName`, `PortraitTurnInText`, `PortraitTurnInName`, `QuestCompletionLog`, `VerifiedBuild`) VALUES
(32505, 'deDE', 'Der zerfallene Kämmerer', 'Findet die Schulter, den Kopf, den Stab, das Haar und den Torso des Kämmerers.', 'Ihr dort! Helft mir.$b$bAls ich noch lebte, war ich der Kämmerer Seiner Majestät des Donnerkönigs. Für meine Ergebenheit wurde ich zu dieser steinernen Form verdammt, dazu bestimmt, auf ewig einen Teil seiner Schätze zu bewachen.$b$bDoch jemand hat meinen prachtvollen Körper entweiht und seine Teile über die Insel verteilt. Der Gedanke, die Ewigkeit als nichts weiter als ein Kopf zu verbringen, bedrückt mich.$b$bSucht die Insel ab, bringt meine Teile zurück und ich werde Euch einen der Schätze Seiner Majestät gewähren.', '', '', '', '', '', '', 23222),
(32543, 'deDE', 'Gefahren von Za''Tual', 'Tötet 12 Trolle in Za''Tual.', 'Die Zandalari sind ernstzunehmende Gegner – gefährlicher als viele, die wir in der Vergangenheit bekämpft haben. Aber auch Ihr seid $gein ernstzunehmender Gegner:eine ernstzunehmende Gegnerin;, $n.\n\nBetretet Za''Tual und zeigt ihnen, wie gefährlich Ihr wirklich seid.', '', '', '', '', '', '', 23222),
(32549, 'deDE', 'In der Haut eines Saurok', 'Begebt Euch zur Ihgalukklamm und tötet einen Skumklingensaurok.', 'Die Ihgalukklamm ist ein gefährlicher Ort. Glücklicherweise stärkt Euch die Magie der Kirin Tor den Rücken.$b$bIch kann Euch in einen Saurok verwandeln.$b$bIch werde jetzt die erste Hälfte der Illusion auf Euch wirken. Danach solltet Ihr sofort zur Ihgalukklamm gehen und einen Skumklingensaurok töten. Sobald das erledigt ist, werde ich Euch kontaktieren und die zweite Hälfte vollenden.', '', '', '', 'Ich werde Euch so verwandeln, dass Ihr wie ein Saurok ausseht.', 'Abbild von Erzmagier Vargoth', '', 23222),
(32707, 'deDE', 'Geheimnisse auf der Insel des Donners', 'Beschafft Euch 3 Ritualsteine der Shan''ze. Die Steine sind sehr selten, und es könnte eine Weile dauern, sie zu sammeln.', 'Da gibt es etwas, um das ich Euch bitten möchte, $C.$B$BUns ist ein mächtiger Ritualstein in die Hände gefallen, den die Mogu der Shan''ze benutzen, um ihre Verbündeten in die Schlacht zu rufen, aber wir brauchen mehr als einen, um ihre Rituale auszuführen.$B$BWenn Ihr drei dieser Steine finden könnt, bringt sie mir. Wir können unsere Truppen sammeln und diese Steine benutzen, um einige von Lei Shens mächtigsten Dienern direkt in unsere Fallen zu locken.', '', '', '', '', '', '', 23222),
(32733, 'deDE', 'Za''Tual', 'Findet Erzmagier Lan''dalock außerhalb von Za''Tual.', 'Die Zandalari haben seit unserer Ankunft stets eine lästige Bedrohung dargestellt. Ich habe Erzmagier Lan''dalock damit beauftragt, ihre Position in Za''Tual im Süden zu schwächen.$B$BMeldet Euch bei ihm, sobald Ihr die Gelegenheit bekommt – er wird etwas für Euch zu tun haben.', '', '', '', '', '', '', 23222),
(32656, 'deDE', 'Der Sturz von Shan''Bu', 'Besiegt Shan''Bu.', 'Lei Shen fand zu Beginn seines Feldzuges ein Gelege seltener Sturmdracheneier. Eine dieser Kreaturen - Nalak - war besonders brutal und tötete alle seine Pfleger, also nahm Lei Shen sich seiner an.\n\nNun arbeitet Shan''Bu, einer der treuesten Diener Lei Shens, daran, den Drachen wiederzuerwecken.\n\nEs wird höchste Zeit, gegen Shan''Bu loszuschlagen!\n\nSprecht mich wieder an, sobald Ihr bereit seid, den Angriff zu starten!', '', '', '', '', '', '', 23222),
(32655, 'deDE', 'Eine waghalsige Idee', 'Infiltriert den Sturmgepeitschten Hafen und öffnet das Tor zum Hof von innen. Sprecht mit Taoshi, um die Mission zu beginnen.', 'Kriegsschiffe der Zandalari plündern die Meere um diese Insel herum. Sie bringen Soldaten und Kriegsbestien aus Zandalar, um Lei Shens Armeen aufzustocken.$B$BWenn wir die Werft des Palastes in unseren Besitz bringen können, ist die Versorgungsroute des Donnerkönigs abgeschnitten, und Euer Standort hier ist gesichert.$B$BIch habe einen Plan, um genau das zu bewerkstelligen, und wir werden keine ganze Armee dazu brauchen.', '', '', '', '', '', '', 23222),
(32652, 'deDE', 'In die Lüfte!', 'Verschafft Euch Zugang zu den Blitzaderminen.', 'Wir haben einen Weg gefunden, die Waffenproduktion der Mogu aufzuhalten, und es hat etwas mit der Kreatur zu tun, die Ihr von den Zandalari befreit habt.\n\nSprecht mit mir, wenn Ihr bereit seid, mehr zu erfahren.', '', '', '', '', '', '', 23222),
(32654, 'deDE', 'Die Mauer muss weg!', 'Sprecht mit Lady Jaina Prachtmeer und dringt dann in die untere Stadt vor, um sie für die Allianz zu erobern.', 'Die Zeit ist gekommen. Unsere Stellung ist gut genug befestigt, sodass wir es riskieren können, in die Behausung des Donnerkönigs vorzudringen. Leider sieht es nicht so aus, als ob er dieses Tor in nächster Zeit öffnen wird.$b$bIch vermute also, dass wir uns unser eigenes Tor schaffen müssen. Lasst es mich wissen, wenn Ihr bereit seid, $n.', '', '', '', '', '', '', 23222),
(32644, 'deDE', 'Der Angriff auf Shaol''mara', 'Schließt den Angriff auf Shaol''mara ab. Sprecht mit Jaina Prachtmeer, um den Angriff zu beginnen.', 'Unsere Erkundungstätigkeiten an der Küste haben uns wertvolle Informationen über die Insel eingebracht, $C.$B$BWir werden allerdings nichts erreichen, wenn wir nicht weiter auf das Festland vordringen.$B$BIch habe einen Trupp unserer besten Lufteinheiten zusammengestellt, um die Attacke einzuleiten.$B$BIch möchte, dass Ihr Euch diesem Angriff anschließt. Ein Champion wie Ihr könnte die Schlacht im kritischen Moment zu unseren Gunsten wenden.$B$BWenn Ihr bereit seid, mit dem Angriff zu beginnen, sprecht mit mir.', '', '', '', '', '', '', 23222),
(32706, 'deDE', 'Verbündete in den Schatten', 'Sprecht in der Tristen Höhle mit Taran Zhu.', 'Die Shado-Pan sind vor uns hier angekommen und sie arbeiten bereits daran, die Armeen des Donnerkönigs zu sabotieren.$B$BIch möchte, dass Ihr Euch mit Taran Zhu, dem Anführer der Shado-Pan, trefft und ihm Eure Dienste im Namen der Kirin Tor anbietet. Ich bezweifle, dass er wirklich Hilfe braucht, aber die Geste selbst ist wichtig genug.', '', '', '', '', '', '', 23222),
(32681, 'deDE', 'Der Sturm braut sich zusammen', 'Reist mit Vereesa Windläufer zur Insel des Donners.', 'Wir haben keine Zeit zu verlieren, $n. Jeder Moment, den wir hier vertrödeln, gibt den Sonnenhäschern weitere Teile der Insel preis.$b$bLady Jaina Prachtmeer wartet auf uns. Wir sollten sie nicht noch länger warten lassen. Sprecht mit mir, wenn Ihr bereit seid, abzureisen. Ich werde Euch selbst hinbringen.', '', '', '', '', '', '', 23222),
(43294, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23222),
(43301, 'deDE', 'Invasion: Azshara', '', '', '', '', '', '', '', '', 23222),
(43244, 'deDE', 'Invasion: Tanaris', '', '', '', '', '', '', '', '', 23222),
(43293, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23222),
(43300, 'deDE', 'Zurückschlagen', '', '', '', '', '', '', '', '', 23222),
(43298, 'deDE', 'Verteidigen', '', '', '', '', '', '', '', '', 23222),
(43243, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23222),
(43299, 'deDE', 'Dämonenkommandant', '', '', '', '', '', '', '', '', 23222);


DELETE FROM `quest_objectives_locale` WHERE (`ID`=270057 AND `locale`='deDE') OR (`ID`=270065 AND `locale`='deDE') OR (`ID`=270218 AND `locale`='deDE') OR (`ID`=270217 AND `locale`='deDE') OR (`ID`=270244 AND `locale`='deDE') OR (`ID`=270215 AND `locale`='deDE') OR (`ID`=270214 AND `locale`='deDE') OR (`ID`=270239 AND `locale`='deDE') OR (`ID`=270238 AND `locale`='deDE') OR (`ID`=270204 AND `locale`='deDE') OR (`ID`=270203 AND `locale`='deDE') OR (`ID`=270245 AND `locale`='deDE') OR (`ID`=270303 AND `locale`='deDE');
INSERT INTO `quest_objectives_locale` (`ID`, `locale`, `QuestId`, `StorageIndex`, `Description`, `VerifiedBuild`) VALUES
(270057, 'deDE', 32543, 0, 'Trolle in Za''Tual getötet', 23222),
(270065, 'deDE', 32549, 0, 'Saurok der Skumklingen getötet', 23222),
(270218, 'deDE', 32656, 0, 'Nehmt den Fuß von Lei Shen ein', 23222),
(270217, 'deDE', 32656, 1, 'Sprecht mit Lady Jaina Prachtmeer', 23222),
(270244, 'deDE', 32655, 0, 'Infiltriert den Sturmgepeitschten Hafen', 23222),
(270215, 'deDE', 32652, 1, 'Greift die Blitzaderminen an', 23222),
(270214, 'deDE', 32652, 0, 'Sprecht mit Jaina Prachtmeer, um Eure Mission zu beginnen', 23222),
(270239, 'deDE', 32654, 1, 'Sichert die untere Stadt', 23222),
(270238, 'deDE', 32654, 0, 'Sprecht mit Lady Jaina Prachtmeer, um Eure Mission zu beginnen', 23222),
(270204, 'deDE', 32644, 1, 'Schließt den Angriff auf Shaol''mara ab', 23222),
(270203, 'deDE', 32644, 0, 'Sprecht mit Jaina Prachtmeer', 23222),
(270245, 'deDE', 32681, 0, 'Insel des Donners entdeckt', 23222),
(270303, 'deDE', 32681, 1, 'Sprecht mit Vereesa', 23222);


DELETE FROM `gameobject_template_locale` WHERE (`entry`=215868 AND `locale`='deDE') OR (`entry`=213411 AND `locale`='deDE') OR (`entry`=214169 AND `locale`='deDE') OR (`entry`=215393 AND `locale`='deDE') OR (`entry`=212229 AND `locale`='deDE') OR (`entry`=214511 AND `locale`='deDE') OR (`entry`=214739 AND `locale`='deDE') OR (`entry`=214170 AND `locale`='deDE') OR (`entry`=215079 AND `locale`='deDE') OR (`entry`=215078 AND `locale`='deDE') OR (`entry`=214466 AND `locale`='deDE') OR (`entry`=218432 AND `locale`='deDE') OR (`entry`=218868 AND `locale`='deDE') OR (`entry`=218435 AND `locale`='deDE') OR (`entry`=218060 AND `locale`='deDE') OR (`entry`=218081 AND `locale`='deDE') OR (`entry`=217763 AND `locale`='deDE') OR (`entry`=217764 AND `locale`='deDE') OR (`entry`=218054 AND `locale`='deDE') OR (`entry`=218077 AND `locale`='deDE') OR (`entry`=218433 AND `locale`='deDE') OR (`entry`=217768 AND `locale`='deDE') OR (`entry`=218076 AND `locale`='deDE') OR (`entry`=218742 AND `locale`='deDE') OR (`entry`=218429 AND `locale`='deDE') OR (`entry`=218427 AND `locale`='deDE') OR (`entry`=218074 AND `locale`='deDE') OR (`entry`=218814 AND `locale`='deDE') OR (`entry`=218811 AND `locale`='deDE') OR (`entry`=218809 AND `locale`='deDE') OR (`entry`=233282 AND `locale`='deDE') OR (`entry`=218977 AND `locale`='deDE') OR (`entry`=218812 AND `locale`='deDE') OR (`entry`=218813 AND `locale`='deDE') OR (`entry`=217751 AND `locale`='deDE') OR (`entry`=217750 AND `locale`='deDE') OR (`entry`=218797 AND `locale`='deDE') OR (`entry`=217724 AND `locale`='deDE') OR (`entry`=218069 AND `locale`='deDE') OR (`entry`=216991 AND `locale`='deDE') OR (`entry`=218799 AND `locale`='deDE') OR (`entry`=218801 AND `locale`='deDE') OR (`entry`=216626 AND `locale`='deDE') OR (`entry`=218100 AND `locale`='deDE') OR (`entry`=218098 AND `locale`='deDE') OR (`entry`=218106 AND `locale`='deDE') OR (`entry`=218102 AND `locale`='deDE') OR (`entry`=218101 AND `locale`='deDE') OR (`entry`=218105 AND `locale`='deDE') OR (`entry`=218073 AND `locale`='deDE') OR (`entry`=218097 AND `locale`='deDE') OR (`entry`=218434 AND `locale`='deDE') OR (`entry`=218638 AND `locale`='deDE') OR (`entry`=216734 AND `locale`='deDE') OR (`entry`=216730 AND `locale`='deDE') OR (`entry`=216729 AND `locale`='deDE') OR (`entry`=216728 AND `locale`='deDE') OR (`entry`=182352 AND `locale`='deDE') OR (`entry`=218085 AND `locale`='deDE') OR (`entry`=218836 AND `locale`='deDE') OR (`entry`=216733 AND `locale`='deDE') OR (`entry`=216732 AND `locale`='deDE') OR (`entry`=216731 AND `locale`='deDE') OR (`entry`=218086 AND `locale`='deDE') OR (`entry`=218431 AND `locale`='deDE') OR (`entry`=216987 AND `locale`='deDE') OR (`entry`=216299 AND `locale`='deDE') OR (`entry`=217758 AND `locale`='deDE') OR (`entry`=218584 AND `locale`='deDE') OR (`entry`=218551 AND `locale`='deDE') OR (`entry`=218548 AND `locale`='deDE') OR (`entry`=218552 AND `locale`='deDE') OR (`entry`=218555 AND `locale`='deDE') OR (`entry`=218554 AND `locale`='deDE') OR (`entry`=218547 AND `locale`='deDE') OR (`entry`=218550 AND `locale`='deDE') OR (`entry`=218546 AND `locale`='deDE') OR (`entry`=218549 AND `locale`='deDE') OR (`entry`=218585 AND `locale`='deDE') OR (`entry`=218545 AND `locale`='deDE') OR (`entry`=218392 AND `locale`='deDE') OR (`entry`=218388 AND `locale`='deDE') OR (`entry`=218393 AND `locale`='deDE') OR (`entry`=218723 AND `locale`='deDE') OR (`entry`=218980 AND `locale`='deDE') OR (`entry`=218869 AND `locale`='deDE') OR (`entry`=218469 AND `locale`='deDE') OR (`entry`=218657 AND `locale`='deDE') OR (`entry`=218655 AND `locale`='deDE') OR (`entry`=218656 AND `locale`='deDE') OR (`entry`=218671 AND `locale`='deDE') OR (`entry`=218674 AND `locale`='deDE') OR (`entry`=218661 AND `locale`='deDE') OR (`entry`=218670 AND `locale`='deDE') OR (`entry`=218660 AND `locale`='deDE') OR (`entry`=218673 AND `locale`='deDE') OR (`entry`=218666 AND `locale`='deDE') OR (`entry`=218668 AND `locale`='deDE') OR (`entry`=218667 AND `locale`='deDE') OR (`entry`=218659 AND `locale`='deDE') OR (`entry`=218658 AND `locale`='deDE') OR (`entry`=218672 AND `locale`='deDE') OR (`entry`=218669 AND `locale`='deDE') OR (`entry`=218677 AND `locale`='deDE') OR (`entry`=218676 AND `locale`='deDE') OR (`entry`=218662 AND `locale`='deDE') OR (`entry`=218663 AND `locale`='deDE') OR (`entry`=218675 AND `locale`='deDE') OR (`entry`=218678 AND `locale`='deDE') OR (`entry`=218664 AND `locale`='deDE') OR (`entry`=218665 AND `locale`='deDE') OR (`entry`=219216 AND `locale`='deDE') OR (`entry`=214611 AND `locale`='deDE') OR (`entry`=218438 AND `locale`='deDE') OR (`entry`=216483 AND `locale`='deDE') OR (`entry`=218850 AND `locale`='deDE') OR (`entry`=218853 AND `locale`='deDE') OR (`entry`=218075 AND `locale`='deDE') OR (`entry`=218857 AND `locale`='deDE') OR (`entry`=218856 AND `locale`='deDE') OR (`entry`=219218 AND `locale`='deDE') OR (`entry`=218428 AND `locale`='deDE') OR (`entry`=215389 AND `locale`='deDE') OR (`entry`=218890 AND `locale`='deDE') OR (`entry`=218891 AND `locale`='deDE') OR (`entry`=218084 AND `locale`='deDE') OR (`entry`=218948 AND `locale`='deDE') OR (`entry`=218755 AND `locale`='deDE') OR (`entry`=218065 AND `locale`='deDE') OR (`entry`=218760 AND `locale`='deDE') OR (`entry`=218834 AND `locale`='deDE') OR (`entry`=218187 AND `locale`='deDE') OR (`entry`=218055 AND `locale`='deDE') OR (`entry`=218852 AND `locale`='deDE') OR (`entry`=218373 AND `locale`='deDE') OR (`entry`=218372 AND `locale`='deDE') OR (`entry`=218833 AND `locale`='deDE') OR (`entry`=218827 AND `locale`='deDE') OR (`entry`=218826 AND `locale`='deDE') OR (`entry`=218052 AND `locale`='deDE') OR (`entry`=218851 AND `locale`='deDE') OR (`entry`=218593 AND `locale`='deDE') OR (`entry`=218072 AND `locale`='deDE') OR (`entry`=218053 AND `locale`='deDE') OR (`entry`=218050 AND `locale`='deDE') OR (`entry`=218056 AND `locale`='deDE') OR (`entry`=218049 AND `locale`='deDE') OR (`entry`=218064 AND `locale`='deDE') OR (`entry`=218059 AND `locale`='deDE') OR (`entry`=219215 AND `locale`='deDE') OR (`entry`=218192 AND `locale`='deDE') OR (`entry`=218094 AND `locale`='deDE') OR (`entry`=218835 AND `locale`='deDE') OR (`entry`=218111 AND `locale`='deDE') OR (`entry`=217752 AND `locale`='deDE') OR (`entry`=218108 AND `locale`='deDE') OR (`entry`=218379 AND `locale`='deDE') OR (`entry`=218381 AND `locale`='deDE') OR (`entry`=218380 AND `locale`='deDE') OR (`entry`=218107 AND `locale`='deDE') OR (`entry`=216315 AND `locale`='deDE') OR (`entry`=218642 AND `locale`='deDE') OR (`entry`=218462 AND `locale`='deDE') OR (`entry`=218626 AND `locale`='deDE') OR (`entry`=218758 AND `locale`='deDE') OR (`entry`=218816 AND `locale`='deDE') OR (`entry`=219094 AND `locale`='deDE') OR (`entry`=218989 AND `locale`='deDE') OR (`entry`=219001 AND `locale`='deDE') OR (`entry`=219004 AND `locale`='deDE') OR (`entry`=219000 AND `locale`='deDE') OR (`entry`=215839 AND `locale`='deDE') OR (`entry`=219088 AND `locale`='deDE') OR (`entry`=218387 AND `locale`='deDE') OR (`entry`=218384 AND `locale`='deDE') OR (`entry`=218383 AND `locale`='deDE') OR (`entry`=218407 AND `locale`='deDE') OR (`entry`=218386 AND `locale`='deDE') OR (`entry`=218493 AND `locale`='deDE') OR (`entry`=218491 AND `locale`='deDE') OR (`entry`=218490 AND `locale`='deDE') OR (`entry`=218489 AND `locale`='deDE') OR (`entry`=218385 AND `locale`='deDE') OR (`entry`=218988 AND `locale`='deDE') OR (`entry`=218983 AND `locale`='deDE') OR (`entry`=218544 AND `locale`='deDE') OR (`entry`=217186 AND `locale`='deDE') OR (`entry`=218198 AND `locale`='deDE') OR (`entry`=217544 AND `locale`='deDE') OR (`entry`=218855 AND `locale`='deDE') OR (`entry`=217169 AND `locale`='deDE') OR (`entry`=217165 AND `locale`='deDE') OR (`entry`=218463 AND `locale`='deDE') OR (`entry`=218586 AND `locale`='deDE') OR (`entry`=218591 AND `locale`='deDE') OR (`entry`=218197 AND `locale`='deDE') OR (`entry`=218975 AND `locale`='deDE') OR (`entry`=218976 AND `locale`='deDE') OR (`entry`=218592 AND `locale`='deDE') OR (`entry`=218110 AND `locale`='deDE') OR (`entry`=178646 AND `locale`='deDE') OR (`entry`=218521 AND `locale`='deDE') OR (`entry`=216316 AND `locale`='deDE') OR (`entry`=218583 AND `locale`='deDE') OR (`entry`=218647 AND `locale`='deDE') OR (`entry`=180779 AND `locale`='deDE') OR (`entry`=218641 AND `locale`='deDE') OR (`entry`=218582 AND `locale`='deDE') OR (`entry`=218378 AND `locale`='deDE') OR (`entry`=216056 AND `locale`='deDE') OR (`entry`=218847 AND `locale`='deDE') OR (`entry`=218764 AND `locale`='deDE') OR (`entry`=218763 AND `locale`='deDE') OR (`entry`=218533 AND `locale`='deDE') OR (`entry`=218531 AND `locale`='deDE') OR (`entry`=218526 AND `locale`='deDE') OR (`entry`=218528 AND `locale`='deDE') OR (`entry`=218527 AND `locale`='deDE') OR (`entry`=218534 AND `locale`='deDE') OR (`entry`=218532 AND `locale`='deDE') OR (`entry`=218535 AND `locale`='deDE') OR (`entry`=218529 AND `locale`='deDE') OR (`entry`=218525 AND `locale`='deDE') OR (`entry`=218524 AND `locale`='deDE') OR (`entry`=218645 AND `locale`='deDE') OR (`entry`=218752 AND `locale`='deDE') OR (`entry`=218709 AND `locale`='deDE') OR (`entry`=218754 AND `locale`='deDE') OR (`entry`=218753 AND `locale`='deDE') OR (`entry`=218751 AND `locale`='deDE') OR (`entry`=218710 AND `locale`='deDE') OR (`entry`=218708 AND `locale`='deDE') OR (`entry`=218707 AND `locale`='deDE') OR (`entry`=218727 AND `locale`='deDE') OR (`entry`=218726 AND `locale`='deDE') OR (`entry`=218822 AND `locale`='deDE') OR (`entry`=218825 AND `locale`='deDE') OR (`entry`=208438 AND `locale`='deDE') OR (`entry`=208437 AND `locale`='deDE') OR (`entry`=218538 AND `locale`='deDE') OR (`entry`=218932 AND `locale`='deDE') OR (`entry`=218639 AND `locale`='deDE') OR (`entry`=218837 AND `locale`='deDE') OR (`entry`=218416 AND `locale`='deDE') OR (`entry`=218838 AND `locale`='deDE') OR (`entry`=217754 AND `locale`='deDE') OR (`entry`=218849 AND `locale`='deDE') OR (`entry`=218730 AND `locale`='deDE') OR (`entry`=218103 AND `locale`='deDE') OR (`entry`=218744 AND `locale`='deDE') OR (`entry`=218104 AND `locale`='deDE') OR (`entry`=218728 AND `locale`='deDE') OR (`entry`=218815 AND `locale`='deDE') OR (`entry`=218698 AND `locale`='deDE') OR (`entry`=218697 AND `locale`='deDE') OR (`entry`=218696 AND `locale`='deDE') OR (`entry`=218057 AND `locale`='deDE') OR (`entry`=218628 AND `locale`='deDE') OR (`entry`=218717 AND `locale`='deDE') OR (`entry`=218729 AND `locale`='deDE') OR (`entry`=218058 AND `locale`='deDE');
INSERT INTO `gameobject_template_locale` (`entry`, `locale`, `name`, `castBarCaption`, `unk1`, `VerifiedBuild`) VALUES
(215868, 'deDE', 'Mantisstatue', '', NULL, 23222),
(213411, 'deDE', 'Bern', '', NULL, 23222),
(214169, 'deDE', 'Versammlungsstein', '', NULL, 23222),
(215393, 'deDE', 'Shado-Pan-Seil', '', NULL, 23222),
(212229, 'deDE', 'Shado-Pan-Seil', '', NULL, 23222),
(214511, 'deDE', 'Zerbrochener Bern', '', NULL, 23222),
(214739, 'deDE', 'Sha Effect 09', '', NULL, 23222),
(214170, 'deDE', 'Ei der Vor''thik', '', NULL, 23222),
(215079, 'deDE', 'Amboss', '', NULL, 23222),
(215078, 'deDE', 'Schmiede', '', NULL, 23222),
(214466, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218432, 'deDE', 'Der heilige Berg', '', NULL, 23222),
(218868, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218435, 'deDE', 'Vertreter der Ordnung', '', NULL, 23222),
(218060, 'deDE', 'Mogugrabstein', '', NULL, 23222),
(218081, 'deDE', 'Gura der Zurückgeforderte', '', NULL, 23222),
(217763, 'deDE', 'Schmiedemeister Deng', '', NULL, 23222),
(217764, 'deDE', 'Haqin der hundert Speere', '', NULL, 23222),
(218054, 'deDE', 'Mogusarg', '', NULL, 23222),
(218077, 'deDE', 'Basis des königlichen Kämmerers', '', NULL, 23222),
(218433, 'deDE', 'Einigkeit hat ihren Preis', '', NULL, 23222),
(217768, 'deDE', 'Foltererkäfig', '', NULL, 23222),
(218076, 'deDE', 'Torso des königlichen Kämmerers', '', NULL, 23222),
(218742, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(218429, 'deDE', 'Die Schatten des Loa', '', NULL, 23222),
(218427, 'deDE', 'Weg zur Mündigkeit', '', NULL, 23222),
(218074, 'deDE', 'Stab des königlichen Kämmerers', '', NULL, 23222),
(218814, 'deDE', '6288.187500 6270.696289 16.899106', '', NULL, 23222),
(218811, 'deDE', 'Glimmender Amboss', '', NULL, 23222),
(218809, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(233282, 'deDE', 'Gedeckter Tisch', '', NULL, 23222),
(218977, 'deDE', 'Knochenhaufen', '', NULL, 23222),
(218812, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218813, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(217751, 'deDE', 'Bodennebel', '', NULL, 23222),
(217750, 'deDE', 'Nebliger Eingang', '', NULL, 23222),
(218797, 'deDE', 'Cha''lats Opferaltar', '', NULL, 23222),
(217724, 'deDE', 'Trollzelt-Augenleuchten', '', NULL, 23222),
(218069, 'deDE', 'Staubwolke', '', NULL, 23222),
(216991, 'deDE', 'Zandalariopfergabe', '', NULL, 23222),
(218799, 'deDE', 'Tec''uats Opferaltar', '', NULL, 23222),
(218801, 'deDE', 'Pa''cheks Opferaltar', '', NULL, 23222),
(216626, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(218100, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218098, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218106, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218102, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218101, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218105, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218073, 'deDE', 'Schulter des königlichen Kämmerers', '', NULL, 23222),
(218097, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218434, 'deDE', 'Das Problem mit den Pandaren', '', NULL, 23222),
(218638, 'deDE', 'Portal zur Shado-Pan-Garnison', '', NULL, 23222),
(216734, 'deDE', 'Briefkasten', '', NULL, 23222),
(216730, 'deDE', 'Amboss', '', NULL, 23222),
(216729, 'deDE', 'Schmiede', '', NULL, 23222),
(216728, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(182352, 'deDE', 'Portal: Silbermond', '', NULL, 23222),
(218085, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(218836, 'deDE', 'Rommaths Buch der Zaubersprüche', '', NULL, 23222),
(216733, 'deDE', 'Kohlenpfanne', '', NULL, 23222),
(216732, 'deDE', 'Amboss', '', NULL, 23222),
(216731, 'deDE', 'Schmiede', '', NULL, 23222),
(218086, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(218431, 'deDE', 'Lei Shen', '', NULL, 23222),
(216987, 'deDE', 'Runengefängnis', '', NULL, 23222),
(216299, 'deDE', 'Manasammler der Sonnenhäscher', '', NULL, 23222),
(217758, 'deDE', 'Schutzzauber der Sonnenhäscher', '', NULL, 23222),
(218584, 'deDE', 'Primordius - Eingangstor', '', NULL, 23222),
(218551, 'deDE', 'Primordius - Gitter', '', NULL, 23222),
(218548, 'deDE', 'Primordius - Gitter', '', NULL, 23222),
(218552, 'deDE', 'Primordius - Gitter', '', NULL, 23222),
(218555, 'deDE', 'Massive Steintür', '', NULL, 23222),
(218554, 'deDE', 'Massive Steintür', '', NULL, 23222),
(218547, 'deDE', 'Primordius - Gitter', '', NULL, 23222),
(218550, 'deDE', 'Primordius - Gitter', '', NULL, 23222),
(218546, 'deDE', 'Primordius - Gitter', '', NULL, 23222),
(218549, 'deDE', 'Primordius - Gitter', '', NULL, 23222),
(218585, 'deDE', 'Primordius - Ausgangstor', '', NULL, 23222),
(218545, 'deDE', 'Primordius - Gitter', '', NULL, 23222),
(218392, 'deDE', 'Beschlagene Eisentür', '', NULL, 23222),
(218388, 'deDE', 'Kanalisationsgitter', '', NULL, 23222),
(218393, 'deDE', 'Uralte Steintür', '', NULL, 23222),
(218723, 'deDE', 'Uralte Moguglocke', '', NULL, 23222),
(218980, 'deDE', 'Doodad_Thunderking_TurtleDoor001', '', NULL, 23222),
(218869, 'deDE', 'Donnerbrücke', '', NULL, 23222),
(218469, 'deDE', 'Gusseisernes Tor', '', NULL, 23222),
(218657, 'deDE', 'Rat der Zandalari - Ausgang, innen', '', NULL, 23222),
(218655, 'deDE', 'Rat der Zandalari - Eingang, links', '', NULL, 23222),
(218656, 'deDE', 'Rat der Zandalari - Eingang, rechts', '', NULL, 23222),
(218671, 'deDE', 'Stammestor der Drakkari', '', NULL, 23222),
(218674, 'deDE', 'Großes Tor', '', NULL, 23222),
(218661, 'deDE', 'Rat der Zandalari - Trash, Tür zwei - außen', '', NULL, 23222),
(218670, 'deDE', 'Stammestor der Gurubashi', '', NULL, 23222),
(218660, 'deDE', 'Rat der Zandalari - Trash, Tür zwei - innen', '', NULL, 23222),
(218673, 'deDE', 'Stammestor der Amani', '', NULL, 23222),
(218666, 'deDE', 'Gusseisernes Tor', '', NULL, 23222),
(218668, 'deDE', 'Gusseisernes Tor', '', NULL, 23222),
(218667, 'deDE', 'Gusseisernes Tor', '', NULL, 23222),
(218659, 'deDE', 'Rat der Zandalari - Trash, Tür eins - außen', '', NULL, 23222),
(218658, 'deDE', 'Rat der Zandalari - Trash, Tür eins - innen', '', NULL, 23222),
(218672, 'deDE', 'Stammestor der Farraki', '', NULL, 23222),
(218669, 'deDE', 'Gusseisernes Tor', '', NULL, 23222),
(218677, 'deDE', 'Mogubrunnen', '', NULL, 23222),
(218676, 'deDE', 'Mogubrunnen', '', NULL, 23222),
(218662, 'deDE', 'Gusseisernes Tor', '', NULL, 23222),
(218663, 'deDE', 'Gusseisernes Tor', '', NULL, 23222),
(218675, 'deDE', 'Mogubrunnen', '', NULL, 23222),
(218678, 'deDE', 'Mogubrunnen', '', NULL, 23222),
(218664, 'deDE', 'Gusseisernes Tor', '', NULL, 23222),
(218665, 'deDE', 'Gusseisernes Tor', '', NULL, 23222),
(219216, 'deDE', 'Versammlungsstein', '', NULL, 23222),
(214611, 'deDE', 'Instance Portal (Raid 4 Difficulties)', '', NULL, 23222),
(218438, 'deDE', 'Das Zeitalter der Hundert Könige', '', NULL, 23222),
(216483, 'deDE', 'Grim Tomalesky', '', NULL, 23222),
(218850, 'deDE', 'Edikt des Donnerkönigs', '', NULL, 23222),
(218853, 'deDE', 'Sturmtruhe', '', NULL, 23222),
(218075, 'deDE', 'Haar des königlichen Kämmerers', '', NULL, 23222),
(218857, 'deDE', 'Glimmender Amboss', '', NULL, 23222),
(218856, 'deDE', 'Glimmender Amboss', '', NULL, 23222),
(219218, 'deDE', 'Zandalaribelagerungswaffe', '', NULL, 23222),
(218428, 'deDE', 'Für Rat und König', '', NULL, 23222),
(215389, 'deDE', 'Orc Refuge Board 2', '', NULL, 23222),
(218890, 'deDE', 'Zandalarikiste 03 - Geöffnet', '', NULL, 23222),
(218891, 'deDE', 'Zandalari Crate 03 Top', '', NULL, 23222),
(218084, 'deDE', 'Werkstattaufträge', '', NULL, 23222),
(218948, 'deDE', 'Zandalaribanner 01', '', NULL, 23222),
(218755, 'deDE', 'Feuergrube', '', NULL, 23222),
(218065, 'deDE', 'Zandalaritrank', '', NULL, 23222),
(218760, 'deDE', 'Mogutür', '', NULL, 23222),
(218834, 'deDE', 'Pestilenz mit Zähnen', '', NULL, 23222),
(218187, 'deDE', 'Wie man einen Teufelssaurier ruft', '', NULL, 23222),
(218055, 'deDE', 'Loa-beseelte Klinge', '', NULL, 23222),
(218852, 'deDE', 'Das Duell von Donner und Stärke', '', NULL, 23222),
(218373, 'deDE', 'Saurierfetisch', '', NULL, 23222),
(218372, 'deDE', 'Saurierfetisch', '', NULL, 23222),
(218833, 'deDE', 'Rikscha der Shado-Pan', '', NULL, 23222),
(218827, 'deDE', 'Karte der Zandalari', '', NULL, 23222),
(218826, 'deDE', 'Tisch mit strategischer Übersichtskarte', '', NULL, 23222),
(218052, 'deDE', 'Ritualartefakt', '', NULL, 23222),
(218851, 'deDE', 'Die Übereinkunft von Zandalar', '', NULL, 23222),
(218593, 'deDE', 'Schatztruhe des Donnerkönigs', '', NULL, 23222),
(218072, 'deDE', 'Kopf des königlichen Kämmerers', '', NULL, 23222),
(218053, 'deDE', 'Ritualartefakt', '', NULL, 23222),
(218050, 'deDE', 'Ritualartefakt', '', NULL, 23222),
(218056, 'deDE', 'Loa-beseelte Klinge', '', NULL, 23222),
(218049, 'deDE', 'Ritualartefakt', '', NULL, 23222),
(218064, 'deDE', 'Zandalaritrank', '', NULL, 23222),
(218059, 'deDE', 'Zandalaritrank', '', NULL, 23222),
(219215, 'deDE', 'Aufgeladenes Moganit', '', NULL, 23222),
(218192, 'deDE', 'Aufgeladenes Moganit', '', NULL, 23222),
(218094, 'deDE', 'Statue', '', NULL, 23222),
(218835, 'deDE', 'Jainas verwittertes Zauberbuch', '', NULL, 23222),
(218111, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(217752, 'deDE', 'Schutzzauber, Kun-Lai-Zaunpfahl', '', NULL, 23222),
(218108, 'deDE', 'Schmiede', '', NULL, 23222),
(218379, 'deDE', 'Vorratskiste des Silberbunds', '', NULL, 23222),
(218381, 'deDE', 'Vorratsfass des Silberbunds', '', NULL, 23222),
(218380, 'deDE', 'Vorratskiste des Silberbunds', '', NULL, 23222),
(218107, 'deDE', 'Amboss', '', NULL, 23222),
(216315, 'deDE', 'Runengefängnis', '', NULL, 23222),
(218642, 'deDE', 'Allianzwagen', '', NULL, 23222),
(218462, 'deDE', 'Portal zur Insel des Donners', '', NULL, 23222),
(218626, 'deDE', 'Geisterwall', '', NULL, 23222),
(218758, 'deDE', 'Eisstachel', '', NULL, 23222),
(218816, 'deDE', 'Portal zur Violetten Erhebung', '', NULL, 23222),
(219094, 'deDE', 'Deaktivierter Zugriffsgenerator', '', NULL, 23222),
(218989, 'deDE', 'Stratholme Fire Large', '', NULL, 23222),
(219001, 'deDE', 'Kleiner Bombenhaufen', '', NULL, 23222),
(219004, 'deDE', 'Feuerbrandfalle', '', NULL, 23222),
(219000, 'deDE', 'Taoshis Bombe', '', NULL, 23222),
(215839, 'deDE', 'Orcischer Teppich', '', NULL, 23222),
(219088, 'deDE', 'Holzwände der Zandalari', '', NULL, 23222),
(218387, 'deDE', 'Dinosauriersattel der Zandalari 01', '', NULL, 23222),
(218384, 'deDE', 'Zandalarikiste 03', '', NULL, 23222),
(218383, 'deDE', 'Zandalarikiste 03 - Geöffnet', '', NULL, 23222),
(218407, 'deDE', 'Zandalarikäfig', '', NULL, 23222),
(218386, 'deDE', 'Dinosauriersattel der Zandalari 02', '', NULL, 23222),
(218493, 'deDE', 'Worgenflasche 03', '', NULL, 23222),
(218491, 'deDE', 'Keule', '', NULL, 23222),
(218490, 'deDE', 'Gebackener Fisch', '', NULL, 23222),
(218489, 'deDE', 'Rippchen', '', NULL, 23222),
(218385, 'deDE', 'Langer Zandalaritisch', '', NULL, 23222),
(218988, 'deDE', 'Zandalaribelagerungswaffe', '', NULL, 23222),
(218983, 'deDE', 'Taoshis Bombe', '', NULL, 23222),
(218544, 'deDE', 'Einsturzstelle', '', NULL, 23222),
(217186, 'deDE', 'Anbindepfosten für Teufelssaurier', '', NULL, 23222),
(218198, 'deDE', 'Mogutür', '', NULL, 23222),
(217544, 'deDE', 'Saurierei', '', NULL, 23222),
(218855, 'deDE', 'Untersuchungstisch', '', NULL, 23222),
(217169, 'deDE', 'Schutzzauber der Kirin Tor', '', NULL, 23222),
(217165, 'deDE', 'Schutzzauber, Mogulaternenpfahl', '', NULL, 23222),
(218463, 'deDE', 'Portal zur Insel des Donners', '', NULL, 23222),
(218586, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218591, 'deDE', 'Schwerer Sprengstoff', '', NULL, 23222),
(218197, 'deDE', 'Schwerer Sprengstoff', '', NULL, 23222),
(218975, 'deDE', 'IoTTK - Progression Scenario Sunwell', '', NULL, 23222),
(218976, 'deDE', 'IoTTK - Progression Scenarios - Dalaran Tower', '', NULL, 23222),
(218592, 'deDE', 'Verlassener Wagen', '', NULL, 23222),
(218110, 'deDE', 'Schmiede', '', NULL, 23222),
(178646, 'deDE', 'Vorratskiste der Allianz', '', NULL, 23222),
(218521, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(216316, 'deDE', 'Manafokus der Kirin Tor', '', NULL, 23222),
(218583, 'deDE', 'Bank', '', NULL, 23222),
(218647, 'deDE', 'Banner des Silberbunds', '', NULL, 23222),
(180779, 'deDE', 'Fass', '', NULL, 23222),
(218641, 'deDE', 'Balliste des Silberbundes', '', NULL, 23222),
(218582, 'deDE', 'Bank', '', NULL, 23222),
(218378, 'deDE', 'Briefkasten', '', NULL, 23222),
(216056, 'deDE', 'Collision PC Size', '', NULL, 23222),
(218847, 'deDE', 'Dalaranturm der Moguinsel', '', NULL, 23222),
(218764, 'deDE', 'Portal nach Dalaran', '', NULL, 23222),
(218763, 'deDE', 'Portal der Kirin Tor', '', NULL, 23222),
(218533, 'deDE', 'Amboss', '', NULL, 23222),
(218531, 'deDE', 'Schmiede', '', NULL, 23222),
(218526, 'deDE', 'Bank', '', NULL, 23222),
(218528, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218527, 'deDE', 'Bank', '', NULL, 23222),
(218534, 'deDE', 'Amboss', '', NULL, 23222),
(218532, 'deDE', 'Schmiede', '', NULL, 23222),
(218535, 'deDE', 'Bank', '', NULL, 23222),
(218529, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218525, 'deDE', 'Bank', '', NULL, 23222),
(218524, 'deDE', 'Bank', '', NULL, 23222),
(218645, 'deDE', 'Teufelsblutelixier', '', NULL, 23222),
(218752, 'deDE', 'Doodad_PA_Anvil_011', '', NULL, 23222),
(218709, 'deDE', 'Doodad_Mogu_Crucible004', '', NULL, 23222),
(218754, 'deDE', 'Doodad_PA_Anvil_009', '', NULL, 23222),
(218753, 'deDE', 'Doodad_PA_Anvil_010', '', NULL, 23222),
(218751, 'deDE', 'Doodad_PA_Anvil_012', '', NULL, 23222),
(218710, 'deDE', 'Doodad_Mogu_Crucible003', '', NULL, 23222),
(218708, 'deDE', 'Doodad_Mogu_Crucible002', '', NULL, 23222),
(218707, 'deDE', 'Doodad_Mogu_Crucible001', '', NULL, 23222),
(218727, 'deDE', 'Donnerschmiedentor', '', NULL, 23222),
(218726, 'deDE', 'Mogutür', '', NULL, 23222),
(218822, 'deDE', 'Freudenfeuer', '', NULL, 23222),
(218825, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(208438, 'deDE', 'Trollkanu 02', '', NULL, 23222),
(208437, 'deDE', 'Trollkanu 01', '', NULL, 23222),
(218538, 'deDE', 'Scenario 5.2 - Tear Down This Wall - Destructible Wall - JSB', '', NULL, 23222),
(218932, 'deDE', 'Teleport Visual', '', NULL, 23222),
(218639, 'deDE', 'Portal zur Shado-Pan-Garnison', '', NULL, 23222),
(218837, 'deDE', 'Teleport nach Za''Tual', '', NULL, 23222),
(218416, 'deDE', 'Strickleiter', '', NULL, 23222),
(218838, 'deDE', 'Teleport zur Meereskeil', '', NULL, 23222),
(217754, 'deDE', 'Staubwolke', '', NULL, 23222),
(218849, 'deDE', 'IoTTK - Mini Sunwell', '', NULL, 23222),
(218730, 'deDE', 'Mogutür', '', NULL, 23222),
(218103, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218744, 'deDE', 'Die Prall Gefüllte Kammer', '', NULL, 23222),
(218104, 'deDE', 'Lagerfeuer', '', NULL, 23222),
(218728, 'deDE', 'Mogutür', '', NULL, 23222),
(218815, 'deDE', 'Schiff der Zandalari', '', NULL, 23222),
(218698, 'deDE', 'Mogutür', '', NULL, 23222),
(218697, 'deDE', 'Mogutür', '', NULL, 23222),
(218696, 'deDE', 'Mogutür', '', NULL, 23222),
(218057, 'deDE', 'Trollstadtmauer_Links', '', NULL, 23222),
(218628, 'deDE', 'Mogutür', '', NULL, 23222),
(218717, 'deDE', 'Mogutür', '', NULL, 23222),
(218729, 'deDE', 'Mogutür', '', NULL, 23222),
(218058, 'deDE', 'Trollstadtmauer_Rechts', '', NULL, 23222);


DELETE FROM `locales_gossip_menu_option` WHERE (`menu_id`=14640 AND `id`=0) OR (`menu_id`=83 AND `id`=0) OR (`menu_id`=15553 AND `id`=0) OR (`menu_id`=15516 AND `id`=0) OR (`menu_id`=15559 AND `id`=0) OR (`menu_id`=15523 AND `id`=0) OR (`menu_id`=15516 AND `id`=2) OR (`menu_id`=15506 AND `id`=0) OR (`menu_id`=15516 AND `id`=3) OR (`menu_id`=15516 AND `id`=1) OR (`menu_id`=15570 AND `id`=0);
INSERT INTO `locales_gossip_menu_option` (`menu_id`, `id`, `option_text_loc1`, `option_text_loc2`, `option_text_loc3`, `option_text_loc4`, `option_text_loc5`, `option_text_loc6`, `option_text_loc7`, `option_text_loc8`, `box_text_loc1`, `box_text_loc2`, `box_text_loc3`, `box_text_loc4`, `box_text_loc5`, `box_text_loc6`, `box_text_loc7`, `box_text_loc8`) VALUES
(14640, 0, '', '', 'Ich möchte etwas von Euch kaufen.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(83, 0, '', '', 'Bringt mich ins Leben zurück.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15553, 0, '', '', 'Ich bin bereit.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15516, 0, '', '', 'Ich bin bereit, den Angriff zu starten. [Für Solo-Instanz anmelden.]', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15559, 0, '', '', 'Ich bin bei Euch. Brechen wir in diese Werft ein! [Für Solo-Instanz anmelden.]', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15523, 0, '', '', 'Also gut. Los geht''s.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15516, 2, '', '', 'Ich bin bereit! [Für Solo-Instanz anmelden.]', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15506, 0, '', '', 'Hier ist der Sprengstoff, Vereesa.', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15516, 3, '', '', 'Ich bin bereit, die Mauer zu Fall zu bringen! [Für Solo-Instanz anmelden.]', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15516, 1, '', '', 'Ich bin bereit! [Für Solo-Instanz anmelden.]', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15570, 0, '', '', 'Ich bin bereit.', '', '', '', '', '', '', '', '', '', '', '', '', '');


DELETE FROM `locales_creature_text` WHERE (`entry`=67661 AND `groupid`=0) OR (`entry`=67661 AND `groupid`=1) OR (`entry`=67661 AND `groupid`=2) OR (`entry`=67661 AND `groupid`=3) OR (`entry`=67930 AND `groupid`=0) OR (`entry`=67992 AND `groupid`=0) OR (`entry`=67992 AND `groupid`=1) OR (`entry`=67992 AND `groupid`=2) OR (`entry`=67992 AND `groupid`=3) OR (`entry`=67992 AND `groupid`=4) OR (`entry`=67992 AND `groupid`=5) OR (`entry`=67992 AND `groupid`=6) OR (`entry`=67992 AND `groupid`=7) OR (`entry`=67992 AND `groupid`=8) OR (`entry`=69099 AND `groupid`=0) OR (`entry`=69099 AND `groupid`=1) OR (`entry`=69099 AND `groupid`=2) OR (`entry`=69136 AND `groupid`=0) OR (`entry`=69210 AND `groupid`=0) OR (`entry`=69210 AND `groupid`=1) OR (`entry`=69223 AND `groupid`=0) OR (`entry`=69227 AND `groupid`=0) OR (`entry`=69250 AND `groupid`=0) OR (`entry`=69265 AND `groupid`=0) OR (`entry`=69272 AND `groupid`=0) OR (`entry`=69391 AND `groupid`=0) OR (`entry`=69435 AND `groupid`=0) OR (`entry`=69435 AND `groupid`=1) OR (`entry`=69444 AND `groupid`=0) OR (`entry`=69457 AND `groupid`=0) OR (`entry`=69457 AND `groupid`=1) OR (`entry`=69457 AND `groupid`=2) OR (`entry`=69461 AND `groupid`=0) OR (`entry`=69461 AND `groupid`=1) OR (`entry`=69461 AND `groupid`=2) OR (`entry`=69465 AND `groupid`=0) OR (`entry`=69468 AND `groupid`=0) OR (`entry`=69468 AND `groupid`=1) OR (`entry`=69468 AND `groupid`=2) OR (`entry`=69468 AND `groupid`=3) OR (`entry`=69468 AND `groupid`=4) OR (`entry`=69468 AND `groupid`=5) OR (`entry`=69468 AND `groupid`=6) OR (`entry`=69468 AND `groupid`=7) OR (`entry`=69468 AND `groupid`=8) OR (`entry`=69482 AND `groupid`=0) OR (`entry`=69482 AND `groupid`=1) OR (`entry`=69482 AND `groupid`=2) OR (`entry`=69483 AND `groupid`=0) OR (`entry`=69483 AND `groupid`=1) OR (`entry`=69483 AND `groupid`=2) OR (`entry`=69483 AND `groupid`=3) OR (`entry`=69501 AND `groupid`=0) OR (`entry`=69501 AND `groupid`=1) OR (`entry`=69501 AND `groupid`=2) OR (`entry`=69501 AND `groupid`=3) OR (`entry`=69501 AND `groupid`=4) OR (`entry`=69501 AND `groupid`=5) OR (`entry`=69501 AND `groupid`=6) OR (`entry`=69501 AND `groupid`=7) OR (`entry`=69501 AND `groupid`=8) OR (`entry`=69501 AND `groupid`=9) OR (`entry`=69501 AND `groupid`=10) OR (`entry`=69501 AND `groupid`=11) OR (`entry`=69501 AND `groupid`=12) OR (`entry`=69501 AND `groupid`=13) OR (`entry`=69501 AND `groupid`=14) OR (`entry`=69501 AND `groupid`=15) OR (`entry`=69501 AND `groupid`=16) OR (`entry`=69501 AND `groupid`=17) OR (`entry`=69501 AND `groupid`=18) OR (`entry`=69501 AND `groupid`=19) OR (`entry`=69501 AND `groupid`=20) OR (`entry`=69530 AND `groupid`=0) OR (`entry`=69530 AND `groupid`=1) OR (`entry`=69530 AND `groupid`=2) OR (`entry`=69530 AND `groupid`=3) OR (`entry`=69530 AND `groupid`=4) OR (`entry`=69534 AND `groupid`=0) OR (`entry`=69534 AND `groupid`=1) OR (`entry`=69538 AND `groupid`=0) OR (`entry`=69538 AND `groupid`=1) OR (`entry`=69538 AND `groupid`=2) OR (`entry`=69538 AND `groupid`=3) OR (`entry`=69538 AND `groupid`=4) OR (`entry`=69544 AND `groupid`=0) OR (`entry`=69544 AND `groupid`=1) OR (`entry`=69544 AND `groupid`=2) OR (`entry`=69544 AND `groupid`=3) OR (`entry`=69544 AND `groupid`=4) OR (`entry`=69559 AND `groupid`=0) OR (`entry`=69559 AND `groupid`=1) OR (`entry`=69559 AND `groupid`=2) OR (`entry`=69559 AND `groupid`=3) OR (`entry`=69600 AND `groupid`=0) OR (`entry`=69600 AND `groupid`=1) OR (`entry`=69600 AND `groupid`=2) OR (`entry`=69600 AND `groupid`=3) OR (`entry`=69600 AND `groupid`=4) OR (`entry`=69600 AND `groupid`=5) OR (`entry`=69600 AND `groupid`=6) OR (`entry`=69600 AND `groupid`=7) OR (`entry`=69600 AND `groupid`=8) OR (`entry`=69600 AND `groupid`=9) OR (`entry`=69600 AND `groupid`=10) OR (`entry`=69600 AND `groupid`=11) OR (`entry`=69600 AND `groupid`=12) OR (`entry`=69600 AND `groupid`=13) OR (`entry`=69600 AND `groupid`=14) OR (`entry`=69600 AND `groupid`=15) OR (`entry`=69600 AND `groupid`=16) OR (`entry`=69600 AND `groupid`=17) OR (`entry`=69600 AND `groupid`=18) OR (`entry`=69615 AND `groupid`=0) OR (`entry`=69615 AND `groupid`=1) OR (`entry`=69615 AND `groupid`=2) OR (`entry`=69615 AND `groupid`=3) OR (`entry`=69615 AND `groupid`=4) OR (`entry`=69616 AND `groupid`=0) OR (`entry`=69616 AND `groupid`=1) OR (`entry`=69616 AND `groupid`=2) OR (`entry`=69616 AND `groupid`=3) OR (`entry`=69616 AND `groupid`=4) OR (`entry`=69616 AND `groupid`=5) OR (`entry`=69616 AND `groupid`=6) OR (`entry`=69616 AND `groupid`=7) OR (`entry`=69616 AND `groupid`=8) OR (`entry`=69616 AND `groupid`=9) OR (`entry`=69616 AND `groupid`=10) OR (`entry`=69616 AND `groupid`=11) OR (`entry`=69616 AND `groupid`=12) OR (`entry`=69616 AND `groupid`=13) OR (`entry`=69616 AND `groupid`=14) OR (`entry`=69616 AND `groupid`=15) OR (`entry`=69617 AND `groupid`=0) OR (`entry`=69617 AND `groupid`=1) OR (`entry`=69617 AND `groupid`=2) OR (`entry`=69617 AND `groupid`=3) OR (`entry`=69617 AND `groupid`=4) OR (`entry`=69617 AND `groupid`=5) OR (`entry`=69617 AND `groupid`=6) OR (`entry`=69617 AND `groupid`=7) OR (`entry`=69617 AND `groupid`=8) OR (`entry`=69617 AND `groupid`=9) OR (`entry`=69619 AND `groupid`=0) OR (`entry`=69620 AND `groupid`=0) OR (`entry`=69621 AND `groupid`=0) OR (`entry`=69621 AND `groupid`=1) OR (`entry`=69621 AND `groupid`=2) OR (`entry`=69621 AND `groupid`=3) OR (`entry`=69621 AND `groupid`=4) OR (`entry`=69621 AND `groupid`=5) OR (`entry`=69621 AND `groupid`=6) OR (`entry`=69652 AND `groupid`=0) OR (`entry`=69717 AND `groupid`=0) OR (`entry`=69741 AND `groupid`=0) OR (`entry`=69741 AND `groupid`=1) OR (`entry`=69743 AND `groupid`=0) OR (`entry`=69744 AND `groupid`=0) OR (`entry`=69755 AND `groupid`=0) OR (`entry`=69755 AND `groupid`=1) OR (`entry`=69755 AND `groupid`=2) OR (`entry`=69755 AND `groupid`=3) OR (`entry`=69755 AND `groupid`=4) OR (`entry`=69903 AND `groupid`=0) OR (`entry`=69917 AND `groupid`=0) OR (`entry`=69917 AND `groupid`=1) OR (`entry`=69917 AND `groupid`=2) OR (`entry`=69917 AND `groupid`=3) OR (`entry`=69917 AND `groupid`=4) OR (`entry`=69917 AND `groupid`=5) OR (`entry`=69923 AND `groupid`=0) OR (`entry`=69923 AND `groupid`=1) OR (`entry`=69923 AND `groupid`=2) OR (`entry`=69923 AND `groupid`=3) OR (`entry`=69923 AND `groupid`=4) OR (`entry`=69949 AND `groupid`=0) OR (`entry`=69949 AND `groupid`=1) OR (`entry`=69949 AND `groupid`=2) OR (`entry`=69949 AND `groupid`=3) OR (`entry`=69949 AND `groupid`=4) OR (`entry`=70040 AND `groupid`=0) OR (`entry`=70041 AND `groupid`=0) OR (`entry`=70041 AND `groupid`=1) OR (`entry`=70041 AND `groupid`=2) OR (`entry`=70041 AND `groupid`=3) OR (`entry`=70041 AND `groupid`=4) OR (`entry`=70041 AND `groupid`=5) OR (`entry`=70092 AND `groupid`=0) OR (`entry`=70092 AND `groupid`=1) OR (`entry`=70092 AND `groupid`=2) OR (`entry`=70092 AND `groupid`=3) OR (`entry`=70092 AND `groupid`=4) OR (`entry`=70092 AND `groupid`=5) OR (`entry`=70092 AND `groupid`=6) OR (`entry`=70105 AND `groupid`=0) OR (`entry`=70105 AND `groupid`=1) OR (`entry`=70105 AND `groupid`=2) OR (`entry`=70285 AND `groupid`=0) OR (`entry`=70297 AND `groupid`=0) OR (`entry`=70370 AND `groupid`=0) OR (`entry`=70370 AND `groupid`=1) OR (`entry`=70482 AND `groupid`=0) OR (`entry`=70482 AND `groupid`=1) OR (`entry`=70482 AND `groupid`=2);
INSERT INTO `locales_creature_text` (`entry`, `groupid`, `id`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(67661, 0, 0, '', '', 'Tod der Allianz!', '', '', '', '', ''),
(67661, 1, 0, '', '', 'Für die Horde!', '', '', '', '', ''),
(67661, 2, 0, '', '', 'Ihr werdet nicht lebend entkommen, $R!', '', '', '', '', ''),
(67661, 3, 0, '', '', 'Ich freu mich schon drauf...', '', '', '', '', ''),
(67930, 0, 0, '', '', 'Nicht autorisiertes Personal identifiziert! Bestrafungsprotokoll aktiviert.', '', '', '', '', ''),
(67992, 0, 0, '', '', 'Ich habe eine Landestelle auf der Insel ausgewählt: erhöhtes Terrain, leicht zu verteidigen.', '', '', '', '', ''),
(67992, 1, 0, '', '', 'Leider haben sich die Zandalaritrolle bereits dort eingenistet und die Stellung befestigt.', '', '', '', '', ''),
(67992, 2, 0, '', '', 'Sie haben den Ort mit einer Geisterbarriere umhüllt. Wir müssen sie zu Fall bringen, bevor wir unsere Truppen herbeiteleportieren können.', '', '', '', '', ''),
(67992, 3, 0, '', '', 'Das ist der Geist der Allianz – wir sind nicht aufzuhalten!', '', '', '', '', ''),
(67992, 4, 0, '', '', 'Vereesa, sichert die Umgebung. Modera, stellt die Schutzzauber auf. Narasi, beginnt damit, die Zelte und Versorgungsgüter herzuteleportieren.', '', '', '', '', ''),
(67992, 5, 0, '', '', 'Alles auf Position! Wir sind auf allen Seiten von Feinden umringt! Wir haben keine Zeit zu verlieren.', '', '', '', '', ''),
(67992, 6, 0, '', '', 'Champion, Ihr habt meine Erwartungen übertroffen. Unsere Stellung auf der Insel ist gesichert, dank Eures Mutes und Eurer Entschlossenheit. Nun beginnt die wahre Arbeit.', '', '', '', '', ''),
(67992, 7, 0, '', '', 'Ich glaube, ich habe einen Plan, wie wir die Waffenkammer des Donnerkönigs infiltrieren können. Mit ein wenig Hilfe von den Shado-Pan.', '', '', '', '', ''),
(67992, 8, 0, '', '', 'Sprecht mit mir, wenn Ihr bereit seid, den Angriff zu starten!', '', '', '', '', ''),
(69099, 0, 0, '', '', 'Ich bin die Verkörperung des Donners!', '', '', '', '', ''),
(69099, 1, 0, '', '', 'Könnt Ihr den kalten Hauch spüren? Ein Sturm naht...', '', '', '', '', ''),
(69099, 2, 0, '', '', 'Ich bin nur... die Dunkelheit... vor dem wahren Sturm...', '', '', '', '', ''),
(69136, 0, 0, '', '', 'Die Macht des Loa kommt.', '', '', '', '', ''),
(69210, 0, 0, '', '', 'Gah!', '', '', '', '', ''),
(69210, 1, 0, '', '', 'Eindringlinge!', '', '', '', '', ''),
(69223, 0, 0, '', '', 'Dein Tod wird Spaß mach''n.', '', '', '', '', ''),
(69227, 0, 0, '', '', 'Was ist diese Magie?', '', '', '', '', ''),
(69250, 0, 0, '', '', 'Ja, kommt näher, kleines Wesen. Meinem Skalpell dürstet nach Eurem Blut!', '', '', '', '', ''),
(69265, 0, 0, '', '', 'Ich sollte Euch zerreißen, Allianzabschaum! Dieses eine Mal werde ich mich jedoch zurückhalten.', '', '', '', '', ''),
(69272, 0, 0, '', '', 'Dieses Schiff is'' unter neuer Führung! Wer sich nich'' anstrengt, dessen Geist wird ''nem Golem zugewiesen!', '', '', '', '', ''),
(69391, 0, 0, '', '', 'Eure Fähigkeiten sind durchaus beeindruckend. Ihr werdet eines Tages einen ausgezeichneten Sklaven abgeben.', '', '', '', '', ''),
(69435, 0, 0, '', '', 'BLUT! KÖSTLICH! Ihr werdet mich nähren!', '', '', '', '', ''),
(69435, 1, 0, '', '', 'Nein! Mein Blut, es… fließt…', '', '', '', '', ''),
(69444, 0, 0, '', '', 'Wie können die Mogu nur zu solchen Untaten fähig sein? Dem Himmel sei Dank, dass Ihr mich gefunden habt.', '', '', '', '', ''),
(69457, 0, 0, '', '', 'Ich zähle sechs... nein, sieben Wachen, die am Himmel patrouillieren.', '', '', '', '', ''),
(69457, 1, 0, '', '', 'Einer von ihnen fliegt höher als die anderen... Er dreht weit draußen über dem Wasser seine Kreise – allein. Er überschätzt sich. Das ist unsere Lücke.', '', '', '', '', ''),
(69457, 2, 0, '', '', 'Wir schalten ihn aus und übernehmen seine Patrouille. Los geht''s!', '', '', '', '', ''),
(69461, 0, 0, '', '', 'Der Sturm wird Euch verschlingen!', '', '', '', '', ''),
(69461, 1, 0, '', '', 'Herr des Donners… schütze mich!', '', '', '', '', ''),
(69461, 2, 0, '', '', 'Und so… erlischt… des Lebens Funke...', '', '', '', '', ''),
(69465, 0, 0, '', '', 'Donnerkönig hat mir Macht gegeben! Kommt, ich zeig''s euch!', '', '', '', '', ''),
(69468, 0, 0, '', '', 'Nur ein bisschen näher...', '', '', '', '', ''),
(69468, 1, 0, '', '', 'HA!', '', '', '', '', ''),
(69468, 2, 0, '', '', 'Die Zandalari sind vorsichtiger, als ich dachte. Seht, dort unten wimmelt es von Patrouillen.', '', '', '', '', ''),
(69468, 3, 0, '', '', 'Und ihre Schamanen haben ein Netzwerk aus Wächtertotems aufgestellt.', '', '', '', '', ''),
(69468, 4, 0, '', '', 'Eine Rauchbombe reicht, um eines ihrer Totems zu blenden.', '', '', '', '', ''),
(69468, 5, 0, '', '', 'Bahnt uns einen Weg vom Schiff zum oberen Tor – wir werden zu Fuß dorthin gelangen müssen.', '', '', '', '', ''),
(69468, 6, 0, '', '', 'Wir können hier oben nicht ewig kreisen. Tut, was Ihr könnt, während ich unsere nächsten Schritte plane.', '', '', '', '', ''),
(69468, 7, 0, '', '', 'Die Zeit ist um. Wir müssen an Bord dieses Schiffes gelangen.', '', '', '', '', ''),
(69468, 8, 0, '', '', 'Wir sollten die Belagerungswaffen am Bug sabotieren. Dann schleichen wir uns unter Deck und suchen nach dem Kapitän.', '', '', '', '', ''),
(69482, 0, 0, '', '', '%s schnarcht vor sich hin.', '', '', '', '', ''),
(69482, 1, 0, '', '', 'Hm... wa...? Blinde Passagiere!', '', '', '', '', ''),
(69482, 2, 0, '', '', 'Ihr... kommt hier nie... lebend raus.', '', '', '', '', ''),
(69483, 0, 0, '', '', 'Wie der Donnerkönig es befiehlt, so wird es geschehen.', '', '', '', '', ''),
(69483, 1, 0, '', '', 'Schluss mit dem Versteckspiel!', '', '', '', '', ''),
(69483, 2, 0, '', '', 'Jetzt gibt es kein Entkommen. Der einzige Ausweg führt durch mich hindurch!', '', '', '', '', ''),
(69483, 3, 0, '', '', 'Niemand... darf...', '', '', '', '', ''),
(69501, 0, 0, '', '', 'IHR werdet knien, wenn Ihr mich um Gnade anfleht, Monster!', '', '', '', '', ''),
(69501, 1, 0, '', '', 'Diese Statuen gebieten über die Macht des ersten Kaisers. Zerstört sie!', '', '', '', '', ''),
(69501, 2, 0, '', '', 'Taoshi, öffnet diese Türen.', '', '', '', '', ''),
(69501, 3, 0, '', '', 'Folgt mir! Wir müssen vorwärts drängen, während uns Eure Kameraden den Rücken freihalten.', '', '', '', '', ''),
(69501, 4, 0, '', '', 'Shan''Bu! Erneut versucht Ihr, einen Gott zu erschaffen. Werdet Ihr jemals lernen?', '', '', '', '', ''),
(69501, 5, 0, '', '', 'Da kommen noch mehr Zandalari.', '', '', '', '', ''),
(69501, 6, 0, '', '', 'Yalia, wir benötigen mehr Heilung! Taoshi, gebt uns Rückendeckung!', '', '', '', '', ''),
(69501, 7, 0, '', '', 'Tretet mir gegenüber, Shan''Bu! Oder ist der Hof des Donnerkönigs ein Nest voller Feiglinge?', '', '', '', '', ''),
(69501, 8, 0, '', '', 'Arrgh – tötet ihn. Schnell!', '', '', '', '', ''),
(69501, 9, 0, '', '', 'Erledigt ihn!', '', '', '', '', ''),
(69501, 10, 0, '', '', 'Seht Ihr? Niemand kann diejenigen unterdrücken, die reinen Geistes sind.', '', '', '', '', ''),
(69501, 11, 0, '', '', 'Wie kleine Kinder.', '', '', '', '', ''),
(69501, 12, 0, '', '', 'Taoshi! Reicht mir meine Waffe.', '', '', '', '', ''),
(69501, 13, 0, '', '', 'Meine Waffe! Champion, folgt mir. Ich möchte, dass Ihr das seht.', '', '', '', '', ''),
(69501, 14, 0, '', '', 'GENUG! Heute wird es kein weiteres Blutvergießen geben!', '', '', '', '', ''),
(69501, 15, 0, '', '', 'Nun sehe ich, warum Eure Allianz und Eure Horde nicht aufhören können, sich zu bekämpfen.', '', '', '', '', ''),
(69501, 16, 0, '', '', 'Jede Vergeltungstat ist in sich ein Akt der Aggression, und jedem Akt der Aggression folgt unmittelbare Vergeltung.', '', '', '', '', ''),
(69501, 17, 0, '', '', 'SCHWEIGT! IHR müsst den Teufelskreis brechen!', '', '', '', '', ''),
(69501, 18, 0, '', '', 'Es endet hier und HEUTE. Der Teufelskreis bricht, wenn Ihr, Lordregent, und Ihr, Lady Prachtmeer, Euch voneinander abwendet und die Sache ruhen lasst.', '', '', '', '', ''),
(69501, 19, 0, '', '', 'Champion, diese Waffenruhe ist vielleicht nicht von Dauer, aber erinnert Euch daran, was Ihr heute erlebt habt.', '', '', '', '', ''),
(69501, 20, 0, '', '', 'Shan''Bus Monstrosität ist noch immer am Fuße des Palastes. Sie muss aufgehalten werden, bevor sie ganz Pandaria in Schutt und Asche legt.', '', '', '', '', ''),
(69530, 0, 0, '', '', 'Taran Zhu! Ganz gleich, wie viele Ihr gegen den Donnerkönig ins Feld führt, bald werden alle vor ihm das Knie beugen.', '', '', '', '', ''),
(69530, 1, 0, '', '', 'Wer braucht einen Gott? Wir haben den Donnerkönig!', '', '', '', '', ''),
(69530, 2, 0, '', '', 'Trolle – macht Euch nützlich. Ich bin hier fast fertig.', '', '', '', '', ''),
(69530, 3, 0, '', '', 'Erhebt Euch, Nalak! Beschützt unseren geliebten Kaiser.', '', '', '', '', ''),
(69530, 4, 0, '', '', 'Der Kaiser wird hocherfreut sein, wenn ich ihm Euren Kopf präsentiere, Shado-Pan.', '', '', '', '', ''),
(69534, 0, 0, '', '', 'Eure Anmaßung langweilt mich! Ich werde Euch das Leben ausquetschen, arroganter Sklave!', '', '', '', '', ''),
(69534, 1, 0, '', '', 'Ich... nein... Mein Kaiser! Mein Kaiser, ich habe Euch enttäuscht!', '', '', '', '', ''),
(69538, 0, 0, '', '', 'Sie kommen! Zu den Waffen, meine Brüder – schleudert sie zurück ins Meer!', '', '', '', '', ''),
(69538, 1, 0, '', '', 'Ha! Ihr habt noch nich'' mal die ganze Macht der Zandalari erlebt!', '', '', '', '', ''),
(69538, 2, 0, '', '', 'Haltet die Ruinen. Tötet sie einen nach dem anderen!', '', '', '', '', ''),
(69538, 3, 0, '', '', 'Zermalmt die Eindringlinge! Die Barriere darf nich'' fall''n.', '', '', '', '', ''),
(69538, 4, 0, '', '', 'Zandalar...', '', '', '', '', ''),
(69544, 0, 0, '', '', 'Ganz leise... Schleicht Euch an und schaltet sie aus.', '', '', '', '', ''),
(69544, 1, 0, '', '', 'Taoshi nickt in Richtung des Besatzungsmitglieds rechts. Schleicht Euch heran, um es bewusstlos zu schlagen!', '', '', '', '', ''),
(69544, 2, 0, '', '', 'Hier drüben! Versteckt Euch im Rauch!', '', '', '', '', ''),
(69544, 3, 0, '', '', 'Auf den nächsten Troll, der sich hier zu schaffen macht, wartet eine sehr unangenehme Überraschung. Verschwinden wir.', '', '', '', '', ''),
(69544, 4, 0, '', '', 'Nicht so leise, wie ich gehofft hatte, aber auch gut.', '', '', '', '', ''),
(69559, 0, 0, '', '', 'NARREN! Ich werde euch zeigen, wie es geht!', '', '', '', '', ''),
(69559, 1, 0, '', '', 'Hast du gedacht, du kannst hier einfach so vorbeiflattern?!', '', '', '', '', ''),
(69559, 2, 0, '', '', 'Na ja, du bist an deinem Ziel angekommen. Zu blöd nur, dass du den Rückweg nich'' erleb''n wirst.', '', '', '', '', ''),
(69559, 3, 0, '', '', 'Und jetz'' stirb, Eindringling!', '', '', '', '', ''),
(69600, 0, 0, '', '', 'Hier oben, Champion!', '', '', '', '', ''),
(69600, 1, 0, '', '', 'Ich glaube, wir haben eine Methode gefunden, wie wir uns vor den Blitzen schützen können. Fliegt aber zur Sicherheit trotzdem in lockerer Formation.', '', '', '', '', ''),
(69600, 2, 0, '', '', 'Unsere Angriffe auf den Friedhof und die Siedlung unter uns zahlen sich bereits aus. Weder die Mogu noch die Zandalari sollten einen Gegenangriff starten können.', '', '', '', '', ''),
(69600, 3, 0, '', '', 'Seht Ihr den zentralen Turm? Es schlagen dort unaufhörlich Blitze ein. Wir vermuten, dass diese Stürme vom Herzen des Palastes ausgehen.', '', '', '', '', ''),
(69600, 4, 0, '', '', 'Vor uns liegt der Palast des Donnerkönigs. Wir müssen erst auf der Insel Fuß fassen, bevor wir seine Festung angreifen können.', '', '', '', '', ''),
(69600, 5, 0, '', '', 'Der Treffpunkt liegt direkt vor uns. Wir haben eine Elitetruppe für einen Angriff... was zum?', '', '', '', '', ''),
(69600, 6, 0, '', '', 'Anar''alah! Der Himmel verrät uns – wir sind hier oben nicht sicher!', '', '', '', '', ''),
(69600, 7, 0, '', '', 'Landen! Alle Mann landen!', '', '', '', '', ''),
(69600, 8, 0, '', '', 'Ahh! Wartet!', '', '', '', '', ''),
(69600, 9, 0, '', '', 'Champion, geht es Euch gut?', '', '', '', '', ''),
(69600, 10, 0, '', '', 'Sie waren doch vorbereitet. Unsere Streitkräfte sind verstreut und wir sind auf uns allein gestellt.', '', '', '', '', ''),
(69600, 11, 0, '', '', 'So viel zum Überraschungsmoment.', '', '', '', '', ''),
(69600, 12, 0, '', '', 'Sie ziehen sich zurück. Wir müssen ihnen nachstellen, bevor sie sich wieder sammeln können. Folgt mir!', '', '', '', '', ''),
(69600, 13, 0, '', '', 'Eine Barrikade versperrt den Weg. Zerstört sie!', '', '', '', '', ''),
(69600, 14, 0, '', '', 'Gute Arbeit. Machen wir weiter.', '', '', '', '', ''),
(69600, 15, 0, '', '', 'Macht Euch gefasst – das ist kein gewöhnlicher Zandalari.', '', '', '', '', ''),
(69600, 16, 0, '', '', 'Euer Stil gefällt mir.', '', '', '', '', ''),
(69600, 17, 0, '', '', 'Der Schild ist durchbrochen! Geschafft!', '', '', '', '', ''),
(69600, 18, 0, '', '', 'Jainas Empfehlung hat sich bestätigt. Ihr habt diese Mission vor dem Scheitern bewahrt... und mir das Leben gerettet.', '', '', '', '', ''),
(69615, 0, 0, '', '', 'Gut, Ihr seid hier.', '', '', '', '', ''),
(69615, 1, 0, '', '', 'Lady Prachtmeer! Alle sind in Position und die Shado-Pan sind bereits im Inneren, aber...', '', '', '', '', ''),
(69615, 2, 0, '', '', 'Die Horde befindet sich auf der anderen Seite des Hofes.', '', '', '', '', ''),
(69615, 3, 0, '', '', 'Lady Prachtmeer – noch mehr Zandalari nähern sich vom Hafen.', '', '', '', '', ''),
(69615, 4, 0, '', '', 'Sie haben meinen Mann getötet!', '', '', '', '', ''),
(69616, 0, 0, '', '', 'Aber was?', '', '', '', '', ''),
(69616, 1, 0, '', '', 'Natürlich ist die Horde auch hier. Aber ein Problem nach dem anderen.', '', '', '', '', ''),
(69616, 2, 0, '', '', 'Alle Mann vorwärts!', '', '', '', '', ''),
(69616, 3, 0, '', '', '... und falls jemand Aethas Sonnenhäscher sieht, der gehört mir...', '', '', '', '', ''),
(69616, 4, 0, '', '', 'Ignoriert die Horde – konzentriert Euch auf die Zandalari am Tor.', '', '', '', '', ''),
(69616, 5, 0, '', '', 'Sie versuchen, uns zu flankieren. Haltet diese Position!', '', '', '', '', ''),
(69616, 6, 0, '', '', 'Liefert uns den Erzmagier aus und rettet so Eure eigene Haut, Lor''themar.', '', '', '', '', ''),
(69616, 7, 0, '', '', 'Eure Leute sind rechtmäßige Kriegsgefangene. Sie haben einen Angriff auf Darnassus von MEINER Stadt aus...', '', '', '', '', ''),
(69616, 8, 0, '', '', 'Sie haben SÄMTLICHE Friedensbemühungen untergraben!', '', '', '', '', ''),
(69616, 9, 0, '', '', 'Jaina atmet tief durch.', '', '', '', '', ''),
(69616, 10, 0, '', '', 'Nun gut. Wir werden nicht angreifen.', '', '', '', '', ''),
(69616, 11, 0, '', '', 'Das hier wird ihn nicht zurückbringen.', '', '', '', '', ''),
(69616, 12, 0, '', '', 'Eins lasst Euch gesagt sein, "Blut-"Elf: Es kann keinen Frieden geben, solange Höllschrei Kriegshäuptling der Horde ist.', '', '', '', '', ''),
(69616, 13, 0, '', '', 'Jainas Ausdruck wird weicher.', '', '', '', '', ''),
(69616, 14, 0, '', '', 'Lordregent.', '', '', '', '', ''),
(69616, 15, 0, '', '', 'Wir sammeln uns im Lager. Unsere Arbeit hier ist getan.', '', '', '', '', ''),
(69617, 0, 0, '', '', 'Ein Problem nach dem anderen. Konzentriert Euch auf die Zandalari!', '', '', '', '', ''),
(69617, 1, 0, '', '', 'Prachtmeer! Ihr lasst meine Leute aus der Violetten Festung frei oder ich werde Euch eigenhändig erschlagen!', '', '', '', '', ''),
(69617, 2, 0, '', '', 'Die Sonnenhäscher wussten NICHTS von Garroshs Angriff auf Darnassus!', '', '', '', '', ''),
(69617, 3, 0, '', '', 'Ich muss mein souveränes Volk beschützen.', '', '', '', '', ''),
(69617, 4, 0, '', '', 'Lor''themar verengt die Augen.', '', '', '', '', ''),
(69617, 5, 0, '', '', 'Waldläufer. Senkt die Waffen.', '', '', '', '', ''),
(69617, 6, 0, '', '', 'Tut es!', '', '', '', '', ''),
(69617, 7, 0, '', '', 'Und genau deswegen möchte ich hier heute die Stärke meines Volkes bewahren.', '', '', '', '', ''),
(69617, 8, 0, '', '', 'Lady.', '', '', '', '', ''),
(69617, 9, 0, '', '', 'Sammelt die Verwundeten ein. Zieht Euch zum Hafen zurück.', '', '', '', '', ''),
(69619, 0, 0, '', '', 'Aethas Sonnenhäscher weicht leicht zurück.', '', '', '', '', ''),
(69620, 0, 0, '', '', 'Lordregent?', '', '', '', '', ''),
(69621, 0, 0, '', '', 'Lord Zhu! Die Allianz – die Horde – dort draußen bricht gleich das reine Chaos aus.', '', '', '', '', ''),
(69621, 1, 0, '', '', 'Lord Zhu... Ihr seid schwer verwundet.', '', '', '', '', ''),
(69621, 2, 0, '', '', 'Lord Zhu!', '', '', '', '', ''),
(69621, 3, 0, '', '', 'Wir müssen Euch zum Kloster zurückbringen.', '', '', '', '', ''),
(69621, 4, 0, '', '', 'Yalia, bringt Lord Zhu in Sicherheit.', '', '', '', '', ''),
(69621, 5, 0, '', '', 'Es ist nun an Euch, den Sturmfürsten Nalak zu besiegen. Ihr werdet eine kleine Armee aufstellen müssen!', '', '', '', '', ''),
(69621, 6, 0, '', '', 'Doch nach allem, was ich heute gesehen habe, traue ich Euch das zu.', '', '', '', '', ''),
(69652, 0, 0, '', '', 'Versperrt das Tor. Die Zandalari konnten die Mauern nicht halten.', '', '', '', '', ''),
(69717, 0, 0, '', '', 'Einerlei. Taoshis Vorschlag ist zu gefährlich.', '', '', '', '', ''),
(69741, 0, 0, '', '', 'Ah! Wir haben gerade über Euch gesprochen.', '', '', '', '', ''),
(69741, 1, 0, '', '', 'Wenn wir den Donnerkönig in die Knie zwingen wollen, dann müssen wir den Hafen einnehmen. Wir brauchen gewagte Ideen.', '', '', '', '', ''),
(69743, 0, 0, '', '', 'Erwacht, Hu''seng! Ihr seid für den Hafen verantwortlich. Niemand darf die Tore seiner Majestät passieren.', '', '', '', '', ''),
(69744, 0, 0, '', '', 'Waldläufer – zerschlagt die Scharniere. Stemmt die Türen auf.', '', '', '', '', ''),
(69755, 0, 0, '', '', 'Die Geister geb''n uns Stärke!', '', '', '', '', ''),
(69755, 1, 0, '', '', 'Tretet vor! Erblickt die Gaben des Donnerkönigs!', '', '', '', '', ''),
(69755, 2, 0, '', '', 'Zu viel... die Geister können seine Macht nicht halt''n...', '', '', '', '', ''),
(69755, 3, 0, '', '', 'Ihr werdet bestraft! Euer Leben ist ein Schauspiel der Vergeblichkeit!', '', '', '', '', ''),
(69755, 4, 0, '', '', 'Für Zandalar!!', '', '', '', '', ''),
(69903, 0, 0, '', '', 'Ahhh, es gilt eine weitere Seele zu rauben.', '', '', '', '', ''),
(69917, 0, 0, '', '', 'Machen wir uns an die Arbeit.', '', '', '', '', ''),
(69917, 1, 0, '', '', 'Die Mogu schmieden Waffen aus Blut und Stahl. Ihre Klingen laben sich am Tod.', '', '', '', '', ''),
(69917, 2, 0, '', '', 'Sie werden lernen, dass sie nirgends vor den Shado-Pan sicher sind.', '', '', '', '', ''),
(69917, 3, 0, '', '', 'Seid vorsichtig! Wir haben fast das Tor erreicht.', '', '', '', '', ''),
(69917, 4, 0, '', '', 'Dieses Schloss... ist komplizierter, als ich dachte...', '', '', '', '', ''),
(69917, 5, 0, '', '', 'Geschafft! Das Tor ist offen!', '', '', '', '', ''),
(69923, 0, 0, '', '', 'Was? Noch eine Rebellion?', '', '', '', '', ''),
(69923, 1, 0, '', '', 'Ihr werdet... bestraft werden...', '', '', '', '', ''),
(69923, 2, 0, '', '', 'Verneigt Euch vor mir!', '', '', '', '', ''),
(69923, 3, 0, '', '', 'Kniet vor Eurem Meister!', '', '', '', '', ''),
(69923, 4, 0, '', '', 'Entfernt Euch von den Fesseln, um die Ketten zu sprengen.', '', '', '', '', ''),
(69949, 0, 0, '', '', 'Die Belagerung ist im Gange, aber unsere Sprengsätze haben es nicht bis zur Mauer geschafft!', '', '', '', '', ''),
(69949, 1, 0, '', '', 'Sammelt die Sprengsätze ein und bringt sie zur Mauer, während ich die Verstärkung organisiere!', '', '', '', '', ''),
(69949, 2, 0, '', '', 'Die Mauer ist verdrahtet und will gesprengt werden – Euch gebührt die Ehre!', '', '', '', '', ''),
(69949, 3, 0, '', '', 'Hier entlang – die Zandalari ziehen sich zurück!', '', '', '', '', ''),
(69949, 4, 0, '', '', 'Nur ein Kratzer. Mir geht es gut.', '', '', '', '', ''),
(70040, 0, 0, '', '', 'Vereesa wartet auf Euch.', '', '', '', '', ''),
(70041, 0, 0, '', '', 'Dieses wunderschöne Geschöpf ist immun gegen die Blitzschläge, die diese Insel beschützen.', '', '', '', '', ''),
(70041, 1, 0, '', '', 'Könnt Ihr es reiten?', '', '', '', '', ''),
(70041, 2, 0, '', '', 'Wunderbar! Sobald Ihr in den Lüften seid, werden Euch die Zandalari jagen.', '', '', '', '', ''),
(70041, 3, 0, '', '', 'Seid auf einen Luftkampf vorbereitet. Springt von Reittier zu Reittier, wenn nötig!', '', '', '', '', ''),
(70041, 4, 0, '', '', 'Eine Agentin der Shado-Pan wird Euch hinter den Minen treffen. Viel Glück!', '', '', '', '', ''),
(70041, 5, 0, '', '', 'Waldläufer! Sichert die Schmiede. Zerstört das Tor, damit es nicht verschlossen werden kann!', '', '', '', '', ''),
(70092, 0, 0, '', '', 'Setzt sie unter Druck! Sie mögen stark sein, aber wir sind schlauer!', '', '', '', '', ''),
(70092, 1, 0, '', '', 'Drängt vorwärts!', '', '', '', '', ''),
(70092, 2, 0, '', '', 'Gute Arbeit! Wir sind durch – Zähne zusammenbeißen, alle Mann. Nutzt die Gunst der Stunde!', '', '', '', '', ''),
(70092, 3, 0, '', '', 'Er ist tot! Ihr seid eine Inspiration für die Kirin Tor.', '', '', '', '', ''),
(70092, 4, 0, '', '', 'Vereesa, seid Ihr verletzt?', '', '', '', '', ''),
(70092, 5, 0, '', '', 'Ihr müsst ins Lager zurück. Ich darf Euch nicht verlieren.', '', '', '', '', ''),
(70092, 6, 0, '', '', 'Versorgt die Verwundeten! Wir haben einen wichtigen Sieg errungen – der nächste Abschnitt unseres Feldzugs beginnt.', '', '', '', '', ''),
(70105, 0, 0, '', '', 'Klingt so, als hätten sie unser kleines Geschenk gefunden.', '', '', '', '', ''),
(70105, 1, 0, '', '', 'Durch Euch hindurch, wie? Das lässt sich einrichten.', '', '', '', '', ''),
(70105, 2, 0, '', '', 'Ist Euch der Bauch oder der Brustkorb lieber?', '', '', '', '', ''),
(70285, 0, 0, '', '', 'Bestienrufer von Shaol''mara ruft Raptoren zu Hilfe!', '', '', '', '', ''),
(70297, 0, 0, '', '', 'Champion, hört zu: Wir greifen den Hafen bei Nacht an und benutzen dafür diese prächtige Bestie, die Ihr gebändigt habt.', '', '', '', '', ''),
(70370, 0, 0, '', '', 'Dieses Jahr war das schlimmste meines Lebens: die Zerstörung Theramores. Der Verrat aus den Reihen der Kirin Tor.', '', '', '', '', ''),
(70370, 1, 0, '', '', 'Was ich daraus gelernt habe? Von jetzt an ergreife ich die Initiative.', '', '', '', '', ''),
(70482, 0, 0, '', '', 'Das Mogureich wurde vor über 12.000 Jahren von Lei Shen gegründet, dem "Donnerkönig".', '', '', '', '', ''),
(70482, 1, 0, '', '', 'Er vereinigte die Mogu, unterwarf die anderen Völker Pandarias und errichtete ein Kaiserreich, das jahrtausendelang bestehen sollte.', '', '', '', '', ''),
(70482, 2, 0, '', '', 'Legenden zufolge hatte der Donnerkönig enorme Macht und baute sich eine palastartige Festung. Wir sollten sie bald sehen können...', '', '', '', '', '');


DELETE FROM `creature_template_locale` WHERE(`entry`=62843 AND `locale`='deDE') OR (`entry`=64626 AND `locale`='deDE') OR (`entry`=24133 AND `locale`='deDE') OR (`entry`=62112 AND `locale`='deDE') OR (`entry`=65172 AND `locale`='deDE') OR (`entry`=63497 AND `locale`='deDE') OR (`entry`=62203 AND `locale`='deDE') OR (`entry`=63755 AND `locale`='deDE') OR (`entry`=63731 AND `locale`='deDE') OR (`entry`=66605 AND `locale`='deDE') OR (`entry`=63548 AND `locale`='deDE') OR (`entry`=65269 AND `locale`='deDE') OR (`entry`=66599 AND `locale`='deDE') OR (`entry`=62863 AND `locale`='deDE') OR (`entry`=65575 AND `locale`='deDE') OR (`entry`=62029 AND `locale`='deDE') OR (`entry`=62813 AND `locale`='deDE') OR (`entry`=62814 AND `locale`='deDE') OR (`entry`=65551 AND `locale`='deDE') OR (`entry`=62832 AND `locale`='deDE') OR (`entry`=62755 AND `locale`='deDE') OR (`entry`=65996 AND `locale`='deDE') OR (`entry`=62761 AND `locale`='deDE') OR (`entry`=62754 AND `locale`='deDE') OR (`entry`=62751 AND `locale`='deDE') OR (`entry`=62757 AND `locale`='deDE') OR (`entry`=62760 AND `locale`='deDE') OR (`entry`=62667 AND `locale`='deDE') OR (`entry`=62668 AND `locale`='deDE') OR (`entry`=62666 AND `locale`='deDE') OR (`entry`=62845 AND `locale`='deDE') OR (`entry`=67179 AND `locale`='deDE') OR (`entry`=67178 AND `locale`='deDE') OR (`entry`=63500 AND `locale`='deDE') OR (`entry`=63218 AND `locale`='deDE') OR (`entry`=63034 AND `locale`='deDE') OR (`entry`=62859 AND `locale`='deDE') OR (`entry`=62756 AND `locale`='deDE') OR (`entry`=63033 AND `locale`='deDE') OR (`entry`=62758 AND `locale`='deDE') OR (`entry`=70176 AND `locale`='deDE') OR (`entry`=69250 AND `locale`='deDE') OR (`entry`=69342 AND `locale`='deDE') OR (`entry`=70204 AND `locale`='deDE') OR (`entry`=69315 AND `locale`='deDE') OR (`entry`=69367 AND `locale`='deDE') OR (`entry`=69369 AND `locale`='deDE') OR (`entry`=71511 AND `locale`='deDE') OR (`entry`=69592 AND `locale`='deDE') OR (`entry`=69445 AND `locale`='deDE') OR (`entry`=69444 AND `locale`='deDE') OR (`entry`=69373 AND `locale`='deDE') OR (`entry`=69284 AND `locale`='deDE') OR (`entry`=69283 AND `locale`='deDE') OR (`entry`=69430 AND `locale`='deDE') OR (`entry`=69894 AND `locale`='deDE') OR (`entry`=69331 AND `locale`='deDE') OR (`entry`=69235 AND `locale`='deDE') OR (`entry`=69270 AND `locale`='deDE') OR (`entry`=69236 AND `locale`='deDE') OR (`entry`=69265 AND `locale`='deDE') OR (`entry`=70201 AND `locale`='deDE') OR (`entry`=69575 AND `locale`='deDE') OR (`entry`=69577 AND `locale`='deDE') OR (`entry`=69573 AND `locale`='deDE') OR (`entry`=69574 AND `locale`='deDE') OR (`entry`=69578 AND `locale`='deDE') OR (`entry`=69576 AND `locale`='deDE') OR (`entry`=69267 AND `locale`='deDE') OR (`entry`=69264 AND `locale`='deDE') OR (`entry`=69900 AND `locale`='deDE') OR (`entry`=69903 AND `locale`='deDE') OR (`entry`=69238 AND `locale`='deDE') OR (`entry`=69305 AND `locale`='deDE') OR (`entry`=69449 AND `locale`='deDE') OR (`entry`=69450 AND `locale`='deDE') OR (`entry`=69420 AND `locale`='deDE') OR (`entry`=69423 AND `locale`='deDE') OR (`entry`=69421 AND `locale`='deDE') OR (`entry`=70628 AND `locale`='deDE') OR (`entry`=69240 AND `locale`='deDE') OR (`entry`=69248 AND `locale`='deDE') OR (`entry`=69237 AND `locale`='deDE') OR (`entry`=69254 AND `locale`='deDE') OR (`entry`=69281 AND `locale`='deDE') OR (`entry`=69285 AND `locale`='deDE') OR (`entry`=69087 AND `locale`='deDE') OR (`entry`=69379 AND `locale`='deDE') OR (`entry`=70141 AND `locale`='deDE') OR (`entry`=69128 AND `locale`='deDE') OR (`entry`=69666 AND `locale`='deDE') OR (`entry`=70529 AND `locale`='deDE') OR (`entry`=70522 AND `locale`='deDE') OR (`entry`=69256 AND `locale`='deDE') OR (`entry`=70523 AND `locale`='deDE') OR (`entry`=69446 AND `locale`='deDE') OR (`entry`=69136 AND `locale`='deDE') OR (`entry`=69266 AND `locale`='deDE') OR (`entry`=69075 AND `locale`='deDE') OR (`entry`=69255 AND `locale`='deDE') OR (`entry`=69263 AND `locale`='deDE') OR (`entry`=69224 AND `locale`='deDE') OR (`entry`=69065 AND `locale`='deDE') OR (`entry`=69697 AND `locale`='deDE') OR (`entry`=69665 AND `locale`='deDE') OR (`entry`=67760 AND `locale`='deDE') OR (`entry`=69405 AND `locale`='deDE') OR (`entry`=69683 AND `locale`='deDE') OR (`entry`=69690 AND `locale`='deDE') OR (`entry`=69688 AND `locale`='deDE') OR (`entry`=69402 AND `locale`='deDE') OR (`entry`=69401 AND `locale`='deDE') OR (`entry`=69400 AND `locale`='deDE') OR (`entry`=70203 AND `locale`='deDE') OR (`entry`=69399 AND `locale`='deDE') OR (`entry`=69251 AND `locale`='deDE') OR (`entry`=69247 AND `locale`='deDE') OR (`entry`=70528 AND `locale`='deDE') OR (`entry`=67753 AND `locale`='deDE') OR (`entry`=67935 AND `locale`='deDE') OR (`entry`=70344 AND `locale`='deDE') OR (`entry`=69391 AND `locale`='deDE') OR (`entry`=70097 AND `locale`='deDE') OR (`entry`=69337 AND `locale`='deDE') OR (`entry`=69252 AND `locale`='deDE') OR (`entry`=67672 AND `locale`='deDE') OR (`entry`=67660 AND `locale`='deDE') OR (`entry`=67669 AND `locale`='deDE') OR (`entry`=67670 AND `locale`='deDE') OR (`entry`=67668 AND `locale`='deDE') OR (`entry`=69259 AND `locale`='deDE') OR (`entry`=69656 AND `locale`='deDE') OR (`entry`=69840 AND `locale`='deDE') OR (`entry`=67333 AND `locale`='deDE') OR (`entry`=69838 AND `locale`='deDE') OR (`entry`=67586 AND `locale`='deDE') OR (`entry`=67990 AND `locale`='deDE') OR (`entry`=70567 AND `locale`='deDE') OR (`entry`=67673 AND `locale`='deDE') OR (`entry`=67663 AND `locale`='deDE') OR (`entry`=69447 AND `locale`='deDE') OR (`entry`=67604 AND `locale`='deDE') OR (`entry`=67674 AND `locale`='deDE') OR (`entry`=67662 AND `locale`='deDE') OR (`entry`=70253 AND `locale`='deDE') OR (`entry`=67933 AND `locale`='deDE') OR (`entry`=70220 AND `locale`='deDE') OR (`entry`=69759 AND `locale`='deDE') OR (`entry`=70555 AND `locale`='deDE') OR (`entry`=70388 AND `locale`='deDE') OR (`entry`=67675 AND `locale`='deDE') OR (`entry`=67930 AND `locale`='deDE') OR (`entry`=69772 AND `locale`='deDE') OR (`entry`=69780 AND `locale`='deDE') OR (`entry`=67991 AND `locale`='deDE') OR (`entry`=69292 AND `locale`='deDE') OR (`entry`=54020 AND `locale`='deDE') OR (`entry`=70491 AND `locale`='deDE') OR (`entry`=70441 AND `locale`='deDE') OR (`entry`=70246 AND `locale`='deDE') OR (`entry`=69459 AND `locale`='deDE') OR (`entry`=70341 AND `locale`='deDE') OR (`entry`=55091 AND `locale`='deDE') OR (`entry`=70308 AND `locale`='deDE') OR (`entry`=69465 AND `locale`='deDE') OR (`entry`=69467 AND `locale`='deDE') OR (`entry`=70236 AND `locale`='deDE') OR (`entry`=70245 AND `locale`='deDE') OR (`entry`=69388 AND `locale`='deDE') OR (`entry`=69390 AND `locale`='deDE') OR (`entry`=69455 AND `locale`='deDE') OR (`entry`=70230 AND `locale`='deDE') OR (`entry`=69274 AND `locale`='deDE') OR (`entry`=40789 AND `locale`='deDE') OR (`entry`=70334 AND `locale`='deDE') OR (`entry`=70311 AND `locale`='deDE') OR (`entry`=70761 AND `locale`='deDE') OR (`entry`=69661 AND `locale`='deDE') OR (`entry`=70758 AND `locale`='deDE') OR (`entry`=69662 AND `locale`='deDE') OR (`entry`=70760 AND `locale`='deDE') OR (`entry`=69660 AND `locale`='deDE') OR (`entry`=70759 AND `locale`='deDE') OR (`entry`=69655 AND `locale`='deDE') OR (`entry`=69462 AND `locale`='deDE') OR (`entry`=69461 AND `locale`='deDE') OR (`entry`=70588 AND `locale`='deDE') OR (`entry`=69413 AND `locale`='deDE') OR (`entry`=70498 AND `locale`='deDE') OR (`entry`=69435 AND `locale`='deDE') OR (`entry`=69300 AND `locale`='deDE') OR (`entry`=69198 AND `locale`='deDE') OR (`entry`=69195 AND `locale`='deDE') OR (`entry`=69424 AND `locale`='deDE') OR (`entry`=69403 AND `locale`='deDE') OR (`entry`=69569 AND `locale`='deDE') OR (`entry`=69570 AND `locale`='deDE') OR (`entry`=69336 AND `locale`='deDE') OR (`entry`=69636 AND `locale`='deDE') OR (`entry`=69568 AND `locale`='deDE') OR (`entry`=69567 AND `locale`='deDE') OR (`entry`=69301 AND `locale`='deDE') OR (`entry`=69868 AND `locale`='deDE') OR (`entry`=69867 AND `locale`='deDE') OR (`entry`=69866 AND `locale`='deDE') OR (`entry`=69826 AND `locale`='deDE') OR (`entry`=70298 AND `locale`='deDE') OR (`entry`=70295 AND `locale`='deDE') OR (`entry`=69183 AND `locale`='deDE') OR (`entry`=69271 AND `locale`='deDE') OR (`entry`=69311 AND `locale`='deDE') OR (`entry`=69306 AND `locale`='deDE') OR (`entry`=69302 AND `locale`='deDE') OR (`entry`=70318 AND `locale`='deDE') OR (`entry`=70316 AND `locale`='deDE') OR (`entry`=70315 AND `locale`='deDE') OR (`entry`=70312 AND `locale`='deDE') OR (`entry`=70160 AND `locale`='deDE') OR (`entry`=70535 AND `locale`='deDE') OR (`entry`=70317 AND `locale`='deDE') OR (`entry`=70284 AND `locale`='deDE') OR (`entry`=70197 AND `locale`='deDE') OR (`entry`=71297 AND `locale`='deDE') OR (`entry`=71298 AND `locale`='deDE') OR (`entry`=70314 AND `locale`='deDE') OR (`entry`=70313 AND `locale`='deDE') OR (`entry`=70293 AND `locale`='deDE') OR (`entry`=69819 AND `locale`='deDE') OR (`entry`=69431 AND `locale`='deDE') OR (`entry`=69406 AND `locale`='deDE') OR (`entry`=69648 AND `locale`='deDE') OR (`entry`=69794 AND `locale`='deDE') OR (`entry`=69549 AND `locale`='deDE') OR (`entry`=69818 AND `locale`='deDE') OR (`entry`=69608 AND `locale`='deDE') OR (`entry`=69604 AND `locale`='deDE') OR (`entry`=70394 AND `locale`='deDE') OR (`entry`=70217 AND `locale`='deDE') OR (`entry`=69803 AND `locale`='deDE') OR (`entry`=69811 AND `locale`='deDE') OR (`entry`=67661 AND `locale`='deDE') OR (`entry`=70181 AND `locale`='deDE') OR (`entry`=69580 AND `locale`='deDE') OR (`entry`=69534 AND `locale`='deDE') OR (`entry`=69506 AND `locale`='deDE') OR (`entry`=69525 AND `locale`='deDE') OR (`entry`=69562 AND `locale`='deDE') OR (`entry`=65019 AND `locale`='deDE') OR (`entry`=69513 AND `locale`='deDE') OR (`entry`=69527 AND `locale`='deDE') OR (`entry`=69625 AND `locale`='deDE') OR (`entry`=69258 AND `locale`='deDE') OR (`entry`=69619 AND `locale`='deDE') OR (`entry`=69620 AND `locale`='deDE') OR (`entry`=69617 AND `locale`='deDE') OR (`entry`=69616 AND `locale`='deDE') OR (`entry`=70128 AND `locale`='deDE') OR (`entry`=69615 AND `locale`='deDE') OR (`entry`=69624 AND `locale`='deDE') OR (`entry`=69530 AND `locale`='deDE') OR (`entry`=69618 AND `locale`='deDE') OR (`entry`=69501 AND `locale`='deDE') OR (`entry`=69621 AND `locale`='deDE') OR (`entry`=69510 AND `locale`='deDE') OR (`entry`=69744 AND `locale`='deDE') OR (`entry`=70193 AND `locale`='deDE') OR (`entry`=70615 AND `locale`='deDE') OR (`entry`=70211 AND `locale`='deDE') OR (`entry`=69212 AND `locale`='deDE') OR (`entry`=70105 AND `locale`='deDE') OR (`entry`=70078 AND `locale`='deDE') OR (`entry`=69544 AND `locale`='deDE') OR (`entry`=70566 AND `locale`='deDE') OR (`entry`=70511 AND `locale`='deDE') OR (`entry`=69622 AND `locale`='deDE') OR (`entry`=70118 AND `locale`='deDE') OR (`entry`=70123 AND `locale`='deDE') OR (`entry`=70120 AND `locale`='deDE') OR (`entry`=69552 AND `locale`='deDE') OR (`entry`=70119 AND `locale`='deDE') OR (`entry`=70263 AND `locale`='deDE') OR (`entry`=69482 AND `locale`='deDE') OR (`entry`=70114 AND `locale`='deDE') OR (`entry`=69468 AND `locale`='deDE') OR (`entry`=69472 AND `locale`='deDE') OR (`entry`=70121 AND `locale`='deDE') OR (`entry`=69743 AND `locale`='deDE') OR (`entry`=69483 AND `locale`='deDE') OR (`entry`=69505 AND `locale`='deDE') OR (`entry`=69463 AND `locale`='deDE') OR (`entry`=69464 AND `locale`='deDE') OR (`entry`=69457 AND `locale`='deDE') OR (`entry`=69458 AND `locale`='deDE') OR (`entry`=70353 AND `locale`='deDE') OR (`entry`=69741 AND `locale`='deDE') OR (`entry`=70297 AND `locale`='deDE') OR (`entry`=69717 AND `locale`='deDE') OR (`entry`=70237 AND `locale`='deDE') OR (`entry`=70574 AND `locale`='deDE') OR (`entry`=69962 AND `locale`='deDE') OR (`entry`=69154 AND `locale`='deDE') OR (`entry`=69635 AND `locale`='deDE') OR (`entry`=69192 AND `locale`='deDE') OR (`entry`=67576 AND `locale`='deDE') OR (`entry`=69917 AND `locale`='deDE') OR (`entry`=69810 AND `locale`='deDE') OR (`entry`=69602 AND `locale`='deDE') OR (`entry`=69559 AND `locale`='deDE') OR (`entry`=69207 AND `locale`='deDE') OR (`entry`=67477 AND `locale`='deDE') OR (`entry`=69286 AND `locale`='deDE') OR (`entry`=67466 AND `locale`='deDE') OR (`entry`=69140 AND `locale`='deDE') OR (`entry`=69217 AND `locale`='deDE') OR (`entry`=69216 AND `locale`='deDE') OR (`entry`=69326 AND `locale`='deDE') OR (`entry`=70547 AND `locale`='deDE') OR (`entry`=70546 AND `locale`='deDE') OR (`entry`=69523 AND `locale`='deDE') OR (`entry`=69521 AND `locale`='deDE') OR (`entry`=69511 AND `locale`='deDE') OR (`entry`=69923 AND `locale`='deDE') OR (`entry`=69493 AND `locale`='deDE') OR (`entry`=69384 AND `locale`='deDE') OR (`entry`=69598 AND `locale`='deDE') OR (`entry`=69155 AND `locale`='deDE') OR (`entry`=69142 AND `locale`='deDE') OR (`entry`=69376 AND `locale`='deDE') OR (`entry`=58071 AND `locale`='deDE') OR (`entry`=69186 AND `locale`='deDE') OR (`entry`=70208 AND `locale`='deDE') OR (`entry`=70041 AND `locale`='deDE') OR (`entry`=70040 AND `locale`='deDE') OR (`entry`=70517 AND `locale`='deDE') OR (`entry`=28682 AND `locale`='deDE') OR (`entry`=19172 AND `locale`='deDE') OR (`entry`=32685 AND `locale`='deDE') OR (`entry`=32681 AND `locale`='deDE') OR (`entry`=37776 AND `locale`='deDE') OR (`entry`=69303 AND `locale`='deDE') OR (`entry`=69451 AND `locale`='deDE') OR (`entry`=69335 AND `locale`='deDE') OR (`entry`=69952 AND `locale`='deDE') OR (`entry`=69297 AND `locale`='deDE') OR (`entry`=69295 AND `locale`='deDE') OR (`entry`=69623 AND `locale`='deDE') OR (`entry`=69429 AND `locale`='deDE') OR (`entry`=69349 AND `locale`='deDE') OR (`entry`=70092 AND `locale`='deDE') OR (`entry`=67768 AND `locale`='deDE') OR (`entry`=69296 AND `locale`='deDE') OR (`entry`=69755 AND `locale`='deDE') OR (`entry`=70591 AND `locale`='deDE') OR (`entry`=69294 AND `locale`='deDE') OR (`entry`=69995 AND `locale`='deDE') OR (`entry`=70397 AND `locale`='deDE') OR (`entry`=70396 AND `locale`='deDE') OR (`entry`=70395 AND `locale`='deDE') OR (`entry`=67763 AND `locale`='deDE') OR (`entry`=70090 AND `locale`='deDE') OR (`entry`=70089 AND `locale`='deDE') OR (`entry`=67772 AND `locale`='deDE') OR (`entry`=67770 AND `locale`='deDE') OR (`entry`=69865 AND `locale`='deDE') OR (`entry`=70091 AND `locale`='deDE') OR (`entry`=308 AND `locale`='deDE') OR (`entry`=306 AND `locale`='deDE') OR (`entry`=65753 AND `locale`='deDE') OR (`entry`=69225 AND `locale`='deDE') OR (`entry`=69171 AND `locale`='deDE') OR (`entry`=69343 AND `locale`='deDE') OR (`entry`=69558 AND `locale`='deDE') OR (`entry`=68847 AND `locale`='deDE') OR (`entry`=68844 AND `locale`='deDE') OR (`entry`=69949 AND `locale`='deDE') OR (`entry`=70146 AND `locale`='deDE') OR (`entry`=70182 AND `locale`='deDE') OR (`entry`=19171 AND `locale`='deDE') OR (`entry`=70185 AND `locale`='deDE') OR (`entry`=70262 AND `locale`='deDE') OR (`entry`=70349 AND `locale`='deDE') OR (`entry`=69895 AND `locale`='deDE') OR (`entry`=70309 AND `locale`='deDE') OR (`entry`=70337 AND `locale`='deDE') OR (`entry`=70561 AND `locale`='deDE') OR (`entry`=70343 AND `locale`='deDE') OR (`entry`=69476 AND `locale`='deDE') OR (`entry`=69531 AND `locale`='deDE') OR (`entry`=69532 AND `locale`='deDE') OR (`entry`=70287 AND `locale`='deDE') OR (`entry`=70290 AND `locale`='deDE') OR (`entry`=69689 AND `locale`='deDE') OR (`entry`=69516 AND `locale`='deDE') OR (`entry`=69515 AND `locale`='deDE') OR (`entry`=70291 AND `locale`='deDE') OR (`entry`=70289 AND `locale`='deDE') OR (`entry`=70285 AND `locale`='deDE') OR (`entry`=69538 AND `locale`='deDE') OR (`entry`=70288 AND `locale`='deDE') OR (`entry`=70286 AND `locale`='deDE') OR (`entry`=69508 AND `locale`='deDE') OR (`entry`=67704 AND `locale`='deDE') OR (`entry`=70069 AND `locale`='deDE') OR (`entry`=67703 AND `locale`='deDE') OR (`entry`=67473 AND `locale`='deDE') OR (`entry`=67771 AND `locale`='deDE') OR (`entry`=69404 AND `locale`='deDE') OR (`entry`=69244 AND `locale`='deDE') OR (`entry`=69200 AND `locale`='deDE') OR (`entry`=69269 AND `locale`='deDE') OR (`entry`=69223 AND `locale`='deDE') OR (`entry`=69170 AND `locale`='deDE') OR (`entry`=69162 AND `locale`='deDE') OR (`entry`=70233 AND `locale`='deDE') OR (`entry`=69678 AND `locale`='deDE') OR (`entry`=69571 AND `locale`='deDE') OR (`entry`=69675 AND `locale`='deDE') OR (`entry`=69668 AND `locale`='deDE') OR (`entry`=69674 AND `locale`='deDE') OR (`entry`=69600 AND `locale`='deDE') OR (`entry`=70231 AND `locale`='deDE') OR (`entry`=70213 AND `locale`='deDE') OR (`entry`=70214 AND `locale`='deDE') OR (`entry`=67996 AND `locale`='deDE') OR (`entry`=69677 AND `locale`='deDE') OR (`entry`=67997 AND `locale`='deDE') OR (`entry`=69670 AND `locale`='deDE') OR (`entry`=69673 AND `locale`='deDE') OR (`entry`=70234 AND `locale`='deDE') OR (`entry`=70370 AND `locale`='deDE') OR (`entry`=67994 AND `locale`='deDE') OR (`entry`=67999 AND `locale`='deDE') OR (`entry`=67998 AND `locale`='deDE') OR (`entry`=67992 AND `locale`='deDE') OR (`entry`=70518 AND `locale`='deDE') OR (`entry`=67995 AND `locale`='deDE') OR (`entry`=67993 AND `locale`='deDE') OR (`entry`=68001 AND `locale`='deDE') OR (`entry`=68000 AND `locale`='deDE') OR (`entry`=70515 AND `locale`='deDE') OR (`entry`=70184 AND `locale`='deDE') OR (`entry`=70183 AND `locale`='deDE') OR (`entry`=69613 AND `locale`='deDE') OR (`entry`=67970 AND `locale`='deDE') OR (`entry`=67937 AND `locale`='deDE') OR (`entry`=67934 AND `locale`='deDE') OR (`entry`=69411 AND `locale`='deDE') OR (`entry`=69412 AND `locale`='deDE') OR (`entry`=69156 AND `locale`='deDE') OR (`entry`=69397 AND `locale`='deDE') OR (`entry`=69338 AND `locale`='deDE') OR (`entry`=69229 AND `locale`='deDE') OR (`entry`=69234 AND `locale`='deDE') OR (`entry`=69695 AND `locale`='deDE') OR (`entry`=69348 AND `locale`='deDE') OR (`entry`=69358 AND `locale`='deDE') OR (`entry`=69227 AND `locale`='deDE') OR (`entry`=69228 AND `locale`='deDE') OR (`entry`=69210 AND `locale`='deDE') OR (`entry`=69226 AND `locale`='deDE') OR (`entry`=70226 AND `locale`='deDE') OR (`entry`=70501 AND `locale`='deDE') OR (`entry`=70372 AND `locale`='deDE') OR (`entry`=70503 AND `locale`='deDE') OR (`entry`=73476 AND `locale`='deDE') OR (`entry`=70366 AND `locale`='deDE') OR (`entry`=70482 AND `locale`='deDE') OR (`entry`=70369 AND `locale`='deDE') OR (`entry`=108655 AND `locale`='deDE'); 
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `NameAlt`, `Title`, `TitleAlt`, `VerifiedBuild`) VALUES
(62843, 'deDE', 'Azzix K''tai', NULL, NULL, NULL, 23222), -- 62843
(64626, 'deDE', 'Sog des Schreckens', NULL, NULL, NULL, 23222), -- 64626
(24133, 'deDE', 'Uralter Geisterführer der Grimmtotems', NULL, NULL, NULL, 23222), -- 24133
(62112, 'deDE', 'Bogenmeisterin Li', NULL, 'Wachoffizierin', NULL, 23222), -- 62112
(65172, 'deDE', 'Len Rüstegut', NULL, 'Abenteurerbedarf', NULL, 23222), -- 65172
(63497, 'deDE', 'Mai von der Mauer', NULL, 'Flugmeisterin', NULL, 23222), -- 63497
(62203, 'deDE', 'Klaxxi''va Tik', NULL, NULL, NULL, 23222), -- 62203
(63755, 'deDE', 'Marschschreiter', NULL, NULL, NULL, 23222), -- 63755
(63731, 'deDE', 'Morastbestie', NULL, NULL, NULL, 23222), -- 63731
(66605, 'deDE', 'Uralter Hirsch', NULL, NULL, NULL, 23222), -- 66605
(63548, 'deDE', 'Knuspriger Skorpion', NULL, NULL, 'wildpetcapturable', 23222), -- 63548
(65269, 'deDE', 'Orangefarbener Schleim', NULL, NULL, NULL, 23222), -- 65269
(66599, 'deDE', 'Großrückenkalb', NULL, NULL, NULL, 23222), -- 66599
(62863, 'deDE', 'Qi''tar der Todesrufer', NULL, NULL, NULL, 23222), -- 62863
(65575, 'deDE', 'Schwarmgeborener der Vor''thik', NULL, NULL, NULL, 23222), -- 65575
(62029, 'deDE', 'Großrückenmushan', NULL, NULL, NULL, 23222), -- 62029
(62813, 'deDE', 'Furchtverschworener der Vor''thik', NULL, NULL, NULL, 23222), -- 62813
(62814, 'deDE', 'Angstformer der Vor''thik', NULL, NULL, NULL, 23222), -- 62814
(65551, 'deDE', 'Belagerungsschmied der Kor''thik', NULL, NULL, NULL, 23222), -- 65551
(62832, 'deDE', 'Kz''Kzik', NULL, NULL, NULL, 23222), -- 62832
(62755, 'deDE', 'Resonator der Kor''thik', NULL, NULL, NULL, 23222), -- 62755
(65996, 'deDE', 'Nörgelnder Schreckling', NULL, NULL, NULL, 23222), -- 65996
(62761, 'deDE', 'Sha-besessenes Ungeziefer', NULL, NULL, NULL, 23222), -- 62761
(62754, 'deDE', 'Kriegsrufer der Kor''thik', NULL, NULL, NULL, 23222), -- 62754
(62751, 'deDE', 'Schreckenslauerer', NULL, NULL, NULL, 23222), -- 62751
(62757, 'deDE', 'Verwüster der Kor''thik', NULL, NULL, NULL, 23222), -- 62757
(62760, 'deDE', 'Verängstigtes Mushan', NULL, NULL, NULL, 23222), -- 62760
(62667, 'deDE', 'Lya von den Zehn Gesängen', NULL, NULL, NULL, 23222), -- 62667
(62668, 'deDE', 'Olon', NULL, 'Der Donnergurgler', NULL, 23222), -- 62668
(62666, 'deDE', 'Saftmeister Vu', NULL, NULL, NULL, 23222), -- 62666
(62845, 'deDE', 'Dicker Dan Sturmbräu', NULL, NULL, NULL, 23222), -- 62845
(67179, 'deDE', 'Koch Jun', NULL, 'Speis & Trank', NULL, 23222), -- 67179
(67178, 'deDE', 'Lien die Versorgerin', NULL, NULL, NULL, 23222), -- 67178
(63500, 'deDE', 'Jin das fliegende Fass', NULL, 'Flugmeister', NULL, 23222), -- 63500
(63218, 'deDE', 'Verteidiger Azzo', NULL, NULL, NULL, 23222), -- 63218
(63034, 'deDE', 'Braugartenbrauer', 'Braugartenbrauerin', NULL, NULL, 23222), -- 63034
(62859, 'deDE', 'Der durstige Missho', NULL, NULL, NULL, 23222), -- 62859
(62756, 'deDE', 'Chitinel der Kor''thik', NULL, NULL, NULL, 23222), -- 62756
(63033, 'deDE', 'Braugartenverteidiger', 'Braugartenverteidigerin', NULL, NULL, 23222), -- 63033
(62758, 'deDE', 'Schlachtsänger der Kor''thik', NULL, NULL, NULL, 23222), -- 62758
(70176, 'deDE', 'Horgak der Versklaver', NULL, NULL, NULL, 23222), -- 70176
(69250, 'deDE', 'Kaida der Blutvergießer', NULL, NULL, NULL, 23222), -- 69250
(69342, 'deDE', 'Auferstandener Urahne', NULL, NULL, NULL, 23222), -- 69342
(70204, 'deDE', 'Geistritualstein', NULL, NULL, NULL, 23222), -- 70204
(69315, 'deDE', 'Gefangenenklon', NULL, NULL, NULL, 23222), -- 69315
(69367, 'deDE', 'Kulans Speer', NULL, NULL, NULL, 23222), -- 69367
(69369, 'deDE', 'Lightning Ritual Bunny', NULL, NULL, NULL, 23222), -- 69369
(71511, 'deDE', 'Donnerrufer der Shan''ze', NULL, NULL, NULL, 23222), -- 71511
(69592, 'deDE', 'Warrior Summon Stalker', NULL, NULL, NULL, 23222), -- 69592
(69445, 'deDE', 'Seele eines Spähers des Silberbunds', NULL, NULL, NULL, 23222), -- 69445
(69444, 'deDE', 'Späher des Silberbunds', NULL, NULL, NULL, 23222), -- 69444
(69373, 'deDE', 'Dance Bunny', NULL, NULL, NULL, 23222), -- 69373
(69284, 'deDE', 'Mogu Corpse Bunny', NULL, NULL, NULL, 23222), -- 69284
(69283, 'deDE', 'Moguleiche', NULL, NULL, NULL, 23222), -- 69283
(69430, 'deDE', 'Dunkelaugenrabe', NULL, NULL, NULL, 23222), -- 69430
(69894, 'deDE', 'Grabwächter', NULL, NULL, NULL, 23222), -- 69894
(69331, 'deDE', 'Mumifizierte Überreste', NULL, NULL, NULL, 23222), -- 69331
(69235, 'deDE', 'Grabhüter der Shan''ze', NULL, NULL, NULL, 23222), -- 69235
(69270, 'deDE', 'Seele eines Spähers der Sonnenhäscher', NULL, NULL, NULL, 23222), -- 69270
(69236, 'deDE', 'Seelenreißer der Shan''ze', NULL, NULL, NULL, 23222), -- 69236
(69265, 'deDE', 'Späher der Sonnenhäscher', NULL, NULL, NULL, 23222), -- 69265
(70201, 'deDE', 'Geistritualstein', NULL, NULL, NULL, 23222), -- 70201
(69575, 'deDE', 'Sarkophagdeckel', NULL, NULL, NULL, 23222), -- 69575
(69577, 'deDE', 'Sarkophagdeckel', NULL, NULL, NULL, 23222), -- 69577
(69573, 'deDE', 'Mogukriegergeist', NULL, NULL, NULL, 23222), -- 69573
(69574, 'deDE', 'Kriegersarkophag', NULL, NULL, NULL, 23222), -- 69574
(69578, 'deDE', 'Sarkophagdeckel', NULL, NULL, NULL, 23222), -- 69578
(69576, 'deDE', 'Sarkophagdeckel', NULL, NULL, NULL, 23222), -- 69576
(69267, 'deDE', 'Späher des Silberbunds', 'Späherin des Silberbunds', NULL, NULL, 23222), -- 69267
(69264, 'deDE', 'Donnerrufer der Shan''ze', NULL, NULL, NULL, 23222), -- 69264
(69900, 'deDE', 'Stone Ritual Bunny', NULL, NULL, NULL, 23222), -- 69900
(69903, 'deDE', 'Seelenreißer der Shan''ze', NULL, NULL, NULL, 23222), -- 69903
(69238, 'deDE', 'Uralter Steineroberer', NULL, NULL, NULL, 23222), -- 69238
(69305, 'deDE', 'Späher der Sonnenhäscher', 'Späherin der Sonnenhäscher', NULL, NULL, 23222), -- 69305
(69449, 'deDE', 'Sarkophagdeckel', NULL, NULL, NULL, 23222), -- 69449
(69450, 'deDE', 'Sarkophagdeckel', NULL, NULL, NULL, 23222), -- 69450
(69420, 'deDE', 'Mogusarkophag', NULL, NULL, NULL, 23222), -- 69420
(69423, 'deDE', 'Sarkophagdeckel', NULL, NULL, NULL, 23222), -- 69423
(69421, 'deDE', 'Sarkophagdeckel', NULL, NULL, NULL, 23222), -- 69421
(70628, 'deDE', 'Quilen Statue Mount Bunny', NULL, NULL, NULL, 23222), -- 70628
(69240, 'deDE', 'Grabwächter', NULL, NULL, NULL, 23222), -- 69240
(69248, 'deDE', 'Ascheweber', NULL, NULL, NULL, 23222), -- 69248
(69237, 'deDE', 'Auferstandener Urahne', NULL, NULL, NULL, 23222), -- 69237
(69254, 'deDE', 'Geisterbinder Cha''lat', NULL, NULL, NULL, 23222), -- 69254
(69281, 'deDE', 'Arkanital der Zandalari', NULL, 'Vorbote der Loa', NULL, 23222), -- 69281
(69285, 'deDE', 'Geisterweber der Zandalari', NULL, 'Vorbote der Loa', NULL, 23222), -- 69285
(69087, 'deDE', 'Seelengefäß', NULL, NULL, NULL, 23222), -- 69087
(69379, 'deDE', 'Bestienrufer der Zandalari', NULL, NULL, NULL, 23222), -- 69379
(70141, 'deDE', 'Gezähmte Pterrorschwinge', NULL, NULL, NULL, 23222), -- 70141
(69128, 'deDE', 'Himmelskreischerjungtier', NULL, NULL, NULL, 23222), -- 69128
(69666, 'deDE', 'Zur''chaka der Knochenformer', NULL, NULL, NULL, 23222), -- 69666
(70529, 'deDE', 'Hinrichtungswächter der Zandalari', NULL, NULL, NULL, 23222), -- 70529
(70522, 'deDE', 'Toter Raptor', NULL, NULL, NULL, 23222), -- 70522
(69256, 'deDE', 'Geisterbinder Pa''chek', NULL, NULL, NULL, 23222), -- 69256
(70523, 'deDE', 'Unsterblicher Geist von Tharon''ja', NULL, NULL, NULL, 23222), -- 70523
(69446, 'deDE', 'Energiekugel des Arkanitals', NULL, NULL, NULL, 23222), -- 69446
(69136, 'deDE', 'Zandalarisprecher', NULL, 'Vorbote der Loa', NULL, 23222), -- 69136
(69266, 'deDE', 'Bestienrufer Aht''lutal', NULL, NULL, NULL, 23222), -- 69266
(69075, 'deDE', 'Zandalariopfer', NULL, NULL, NULL, 23222), -- 69075
(69255, 'deDE', 'Geisterbinder Tec''uat', NULL, NULL, NULL, 23222), -- 69255
(69263, 'deDE', 'Gepeinigter Himmelskreischer', NULL, NULL, NULL, 23222), -- 69263
(69224, 'deDE', 'Arkanwirker der Zandalari', NULL, NULL, NULL, 23222), -- 69224
(69065, 'deDE', 'Bestienrufer der Zandalari', NULL, NULL, NULL, 23222), -- 69065
(69697, 'deDE', 'Fehlerhaftes Konstrukt des Silberbunds', NULL, NULL, NULL, 23222), -- 69697
(69665, 'deDE', 'Arkanwirker Jor''guva', NULL, NULL, NULL, 23222), -- 69665
(67760, 'deDE', 'Wühler der Skumklingen', NULL, NULL, NULL, 23222), -- 67760
(69405, 'deDE', 'Koloss der Zandalari', NULL, NULL, NULL, 23222), -- 69405
(69683, 'deDE', 'Magister B Sparkles', NULL, NULL, NULL, 23222), -- 69683
(69690, 'deDE', 'Begrabenes Konstrukt des Silberbunds - Vehikel', NULL, NULL, NULL, 23222), -- 69690
(69688, 'deDE', 'Begrabenes Konstrukt des Silberbunds', NULL, NULL, NULL, 23222), -- 69688
(69402, 'deDE', 'Tomma', NULL, NULL, NULL, 23222), -- 69402
(69401, 'deDE', 'Akolik', NULL, NULL, NULL, 23222), -- 69401
(69400, 'deDE', 'Gokkar', NULL, NULL, NULL, 23222), -- 69400
(70203, 'deDE', 'Geistritualstein', NULL, NULL, NULL, 23222), -- 70203
(69399, 'deDE', 'Siakann', NULL, NULL, NULL, 23222), -- 69399
(69251, 'deDE', 'Wabernder Dreck', NULL, NULL, NULL, 23222), -- 69251
(69247, 'deDE', 'Fauliger Fleischhaufen', NULL, NULL, NULL, 23222), -- 69247
(70528, 'deDE', 'Sparkle Stalker (RKS)', NULL, NULL, NULL, 23222), -- 70528
(67753, 'deDE', 'Fackel', NULL, NULL, NULL, 23222), -- 67753
(67935, 'deDE', 'Arkaner Zerstörer', NULL, NULL, NULL, 23222), -- 67935
(70344, 'deDE', 'Ninjaseil', NULL, NULL, NULL, 23222), -- 70344
(69391, 'deDE', 'Der königliche Kämmerer', NULL, NULL, NULL, 23222), -- 69391
(70097, 'deDE', 'Lightning Channel Target Bunny', NULL, NULL, NULL, 23222), -- 70097
(69337, 'deDE', 'Mumifizierte Überreste', NULL, NULL, NULL, 23222), -- 69337
(69252, 'deDE', 'Waldläufer Shalan', NULL, 'Stallmeister', NULL, 23222), -- 69252
(67672, 'deDE', 'Vasarin Morgenröte', NULL, 'Rüstmeister des Sonnenhäscheransturms', NULL, 23222), -- 67672
(67660, 'deDE', 'Erzmagier Aethas Sonnenhäscher', NULL, NULL, NULL, 23222), -- 67660
(67669, 'deDE', 'Nargut', NULL, NULL, NULL, 23222), -- 67669
(67670, 'deDE', 'Rhukah', NULL, NULL, NULL, 23222), -- 67670
(67668, 'deDE', 'Uda das Biest', NULL, 'Gastwirtin', NULL, 23222), -- 67668
(69259, 'deDE', 'Lanesh der Stahlweber', NULL, 'Schmiedekunstbedarf', NULL, 23222), -- 69259
(69656, 'deDE', 'Diener der Sonnenhäscher', NULL, NULL, NULL, 23222), -- 69656
(69840, 'deDE', 'Leutnant der Sonnenhäscher', NULL, NULL, NULL, 23222), -- 69840
(67333, 'deDE', 'Blutritter der Morgenwache', NULL, NULL, NULL, 23222), -- 67333
(69838, 'deDE', 'Arcanis Mechanica', NULL, 'Wächter von Großmagister Rommath', NULL, 23222), -- 69838
(67586, 'deDE', 'Elynara', NULL, 'Archäologielehrerin', NULL, 23222), -- 67586
(67990, 'deDE', 'Lor''themar Theron', NULL, 'Lordregent von Quel''Thalas', NULL, 23222), -- 67990
(70567, 'deDE', 'Befehlskarte', NULL, NULL, 'quest', 23222), -- 70567
(67673, 'deDE', 'Girana die Blutgezeichnete', NULL, NULL, NULL, 23222), -- 67673
(67663, 'deDE', 'Magister Hathorel', NULL, NULL, NULL, 23222), -- 67663
(69447, 'deDE', 'Harlan Morgenweiß', NULL, 'Die Archäologische Akademie', NULL, 23222), -- 69447
(67604, 'deDE', 'Runensucher von Silbermond', 'Runensucherin von Silbermond', 'Die Archäologische Akademie', NULL, 23222), -- 67604
(67674, 'deDE', 'Amariel Sonnenschwur', NULL, NULL, NULL, 23222), -- 67674
(67662, 'deDE', 'Hocharkanist Savor', NULL, NULL, NULL, 23222), -- 67662
(70253, 'deDE', 'Wächter der Morgensucher', 'Wächterin der Morgensucher', NULL, NULL, 23222), -- 70253
(67933, 'deDE', 'Pyrosternvernichter', NULL, NULL, NULL, 23222), -- 67933
(70220, 'deDE', 'Gefangener Kirin Tor', 'Gefangene Kirin Tor', NULL, NULL, 23222), -- 70220
(69759, 'deDE', 'Verteidigungskristall', NULL, NULL, NULL, 23222), -- 69759
(70555, 'deDE', 'Toter Leviathan', NULL, NULL, NULL, 23222), -- 70555
(70388, 'deDE', 'Sunreaver Mana Collector Target', NULL, NULL, NULL, 23222), -- 70388
(67675, 'deDE', 'Manawyrm der Sonnenhäscher', NULL, NULL, NULL, 23222), -- 67675
(67930, 'deDE', 'Konstrukt der Sonnenhäscher', NULL, NULL, NULL, 23222), -- 67930
(69772, 'deDE', 'Himmelskreischer der Zandalari', NULL, NULL, NULL, 23222), -- 69772
(69780, 'deDE', 'Bestienrufer der Zandalari', NULL, NULL, NULL, 23222), -- 69780
(67991, 'deDE', 'Zauberbrecher von Silbermond', NULL, NULL, NULL, 23222), -- 67991
(69292, 'deDE', 'Schutzzauber der Sonnenhäscher', NULL, NULL, NULL, 23222), -- 69292
(54020, 'deDE', 'General Purpose Bunny JMF (Look 2 - Flying, Huge AOI)', NULL, NULL, NULL, 23222), -- 54020
(70491, 'deDE', 'Wildgewordener Sturmrufer', NULL, NULL, NULL, 23222), -- 70491
(70441, 'deDE', 'Verlorene Seele', NULL, NULL, NULL, 23222), -- 70441
(70246, 'deDE', 'Geistschinder', NULL, NULL, NULL, 23222), -- 70246
(69459, 'deDE', 'Gebundener Wasserelementar', NULL, NULL, NULL, 23222), -- 69459
(70341, 'deDE', 'Gequälter Geist', NULL, NULL, NULL, 23222), -- 70341
(55091, 'deDE', 'General Purpose Bunny JMF (Look 2 - Flying, Infinite AOI)', NULL, NULL, NULL, 23222), -- 55091
(70308, 'deDE', 'Seelengefüttertes Konstrukt', NULL, NULL, NULL, 23222), -- 70308
(69465, 'deDE', 'Jin''rokh der Zerstörer', NULL, NULL, NULL, 23222), -- 69465
(69467, 'deDE', 'Statue', NULL, NULL, NULL, 23222), -- 69467
(70236, 'deDE', 'Sturmrufer der Zandalari', NULL, NULL, NULL, 23222), -- 70236
(70245, 'deDE', 'Trainingsattrappe', NULL, NULL, NULL, 23222), -- 70245
(69388, 'deDE', 'Speermacher der Zandalari', NULL, NULL, NULL, 23222), -- 69388
(69390, 'deDE', 'Sturmrufer der Zandalari', NULL, NULL, NULL, 23222), -- 69390
(69455, 'deDE', 'Wasserbinder der Zandalari', NULL, NULL, NULL, 23222), -- 69455
(70230, 'deDE', 'Klingeninitiand der Zandalari', NULL, NULL, NULL, 23222), -- 70230
(69274, 'deDE', 'Lu-Shero', NULL, 'Der Hüter des Horts', NULL, 23222), -- 69274
(40789, 'deDE', 'Generic Controller Bunny (CSA)', NULL, NULL, NULL, 23222), -- 40789
(70334, 'deDE', 'Steinwächter', NULL, NULL, NULL, 23222), -- 70334
(70311, 'deDE', 'Pillar Puzzle Bunny', NULL, NULL, NULL, 23222), -- 70311
(70761, 'deDE', 'Electrified Pool Bunny', NULL, NULL, NULL, 23222), -- 70761
(69661, 'deDE', 'Aktivierte Schmiede', NULL, NULL, NULL, 23222), -- 69661
(70758, 'deDE', 'Electrified Pool Bunny', NULL, NULL, NULL, 23222), -- 70758
(69662, 'deDE', 'Aktivierte Schmiede', NULL, NULL, NULL, 23222), -- 69662
(70760, 'deDE', 'Electrified Pool Bunny', NULL, NULL, NULL, 23222), -- 70760
(69660, 'deDE', 'Aktivierte Schmiede', NULL, NULL, NULL, 23222), -- 69660
(70759, 'deDE', 'Electrified Pool Bunny', NULL, NULL, NULL, 23222), -- 70759
(69655, 'deDE', 'Aktivierte Schmiede', NULL, NULL, NULL, 23222), -- 69655
(69462, 'deDE', 'Kugelblitz', NULL, NULL, NULL, 23222), -- 69462
(69461, 'deDE', 'Itoka', NULL, 'Meister der Schmiede', NULL, 23222), -- 69461
(70588, 'deDE', 'Metzler der Shan''ze', NULL, NULL, NULL, 23222), -- 70588
(69413, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 69413
(70498, 'deDE', 'Vollstrecker der Shado-Pan', NULL, NULL, NULL, 23222), -- 70498
(69435, 'deDE', 'Fleischschmied Hoku', NULL, NULL, NULL, 23222), -- 69435
(69300, 'deDE', 'Arkanital Ra''kul', NULL, NULL, NULL, 23222), -- 69300
(69198, 'deDE', 'Rekrut der Zandalari', NULL, NULL, NULL, 23222), -- 69198
(69195, 'deDE', 'Geistverzerrer der Zandalari', NULL, NULL, NULL, 23222), -- 69195
(69424, 'deDE', 'Miura Hellweber - Glitzern', NULL, NULL, NULL, 23222), -- 69424
(69403, 'deDE', 'Gefangener der Skumklingen', NULL, NULL, NULL, 23222), -- 69403
(69569, 'deDE', 'Terrorhornpriester', NULL, NULL, NULL, 23222), -- 69569
(69570, 'deDE', 'Falkenpriester', NULL, NULL, NULL, 23222), -- 69570
(69336, 'deDE', 'Wächter Tak''u', NULL, NULL, NULL, 23222), -- 69336
(69636, 'deDE', 'Windstöße des Falken', NULL, NULL, NULL, 23222), -- 69636
(69568, 'deDE', 'Hydrapriester', NULL, NULL, NULL, 23222), -- 69568
(69567, 'deDE', 'Schlangenpriester', NULL, NULL, NULL, 23222), -- 69567
(69301, 'deDE', 'Geisterbinder Tu''chek', NULL, NULL, NULL, 23222), -- 69301
(69868, 'deDE', 'Zandalariopfer', NULL, NULL, NULL, 23222), -- 69868
(69867, 'deDE', 'Zandalariopfer', NULL, NULL, NULL, 23222), -- 69867
(69866, 'deDE', 'Zandalariopfer', NULL, NULL, NULL, 23222), -- 69866
(69826, 'deDE', 'Angreifer der Zandalari', NULL, NULL, NULL, 23222), -- 69826
(70298, 'deDE', 'Attentäter der Wu Kao', NULL, NULL, NULL, 23222), -- 70298
(70295, 'deDE', 'Attentäter der Wu Kao', NULL, NULL, NULL, 23222), -- 70295
(69183, 'deDE', 'Heranwachsender Raptor', NULL, NULL, NULL, 23222), -- 69183
(69271, 'deDE', 'Klingenkrallenjungtier', NULL, NULL, NULL, 23222), -- 69271
(69311, 'deDE', 'Loa-beseelter Compy', NULL, NULL, NULL, 23222), -- 69311
(69306, 'deDE', 'Loa-beseelte Klingenkralle', NULL, NULL, NULL, 23222), -- 69306
(69302, 'deDE', 'Loa-Sprecher', NULL, NULL, NULL, 23222), -- 69302
(70318, 'deDE', 'Versorger Bao', NULL, NULL, NULL, 23222), -- 70318
(70316, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 70316
(70315, 'deDE', 'Yalia Weisenwisper', NULL, NULL, NULL, 23222), -- 70315
(70312, 'deDE', 'Lao-Chen der eiserne Bauch', NULL, NULL, NULL, 23222), -- 70312
(70160, 'deDE', 'Taran Zhu', NULL, 'Meister der Shado-Pan', NULL, 23222), -- 70160
(70535, 'deDE', 'Teng von den fliegenden Messern', NULL, 'Rüstmeisterin des Shado-Pan-Vorstoßes', NULL, 23222), -- 70535
(70317, 'deDE', 'Omniaveteran', 'Omniaveteranin', NULL, NULL, 23222), -- 70317
(70284, 'deDE', 'Schwarzwachenveteran', NULL, NULL, NULL, 23222), -- 70284
(70197, 'deDE', 'Blitzritualstein', NULL, NULL, NULL, 23222), -- 70197
(71297, 'deDE', 'Gezähmte Klingenkralle', NULL, NULL, NULL, 23222), -- 71297
(71298, 'deDE', 'Bestienherr der Zandalari', NULL, NULL, NULL, 23222), -- 71298
(70314, 'deDE', 'Weißfeder', NULL, 'Falkenmeister Nurongs Begleiter', NULL, 23222), -- 70314
(70313, 'deDE', 'Falkenmeister Nurong', NULL, NULL, NULL, 23222), -- 70313
(70293, 'deDE', 'Veteranenschütze', 'Veteranenschützin', NULL, NULL, 23222), -- 70293
(69819, 'deDE', 'Sumpfquaker', NULL, NULL, 'wildpetcapturable', 23222), -- 69819
(69431, 'deDE', 'Blutsucher der Shan''ze', NULL, NULL, NULL, 23222), -- 69431
(69406, 'deDE', 'Mächtiger Teufelssaurier', NULL, NULL, NULL, 23222), -- 69406
(69648, 'deDE', 'Donnerschwanzplanscher', NULL, NULL, 'wildpetcapturable', 23222), -- 69648
(69794, 'deDE', 'Elektrisierter Klingenzahn', NULL, NULL, 'wildpetcapturable', 23222), -- 69794
(69549, 'deDE', 'Toter Spion', 'Tote Spionin', NULL, NULL, 23222), -- 69549
(69818, 'deDE', 'Älterer Python', NULL, NULL, 'wildpetcapturable', 23222), -- 69818
(69608, 'deDE', 'Halterung für taktische Manabomben', NULL, NULL, NULL, 23222), -- 69608
(69604, 'deDE', 'Halterung für taktische Manabomben', NULL, NULL, NULL, 23222), -- 69604
(70394, 'deDE', 'Kirin Tor Mana Focus Target', NULL, NULL, NULL, 23222), -- 70394
(70217, 'deDE', 'Gefangener Sonnenhäscher', 'Gefangene Sonnenhäscherin', NULL, NULL, 23222), -- 70217
(69803, 'deDE', 'Taran Zhu', NULL, 'Meister der Shado-Pan', NULL, 23222), -- 69803
(69811, 'deDE', 'Yalias Tiger', NULL, NULL, NULL, 23222), -- 69811
(67661, 'deDE', 'Sonnenhäschermagus', 'Sonnenhäschermagistrix', NULL, NULL, 23222), -- 67661
(70181, 'deDE', 'Tropfendes Blut', NULL, NULL, NULL, 23222), -- 70181
(69580, 'deDE', 'Onyxsturmklaue', NULL, NULL, NULL, 23222), -- 69580
(69534, 'deDE', 'Shan''Bu', NULL, NULL, NULL, 23222), -- 69534
(69506, 'deDE', 'Qilenwächter', NULL, NULL, NULL, 23222), -- 69506
(69525, 'deDE', 'Blitzrichter der Shan''ze', NULL, NULL, NULL, 23222), -- 69525
(69562, 'deDE', 'Nalak', NULL, 'Der Sturmfürst', NULL, 23222), -- 69562
(65019, 'deDE', 'Bodenstampfer', NULL, NULL, NULL, 23222), -- 65019
(69513, 'deDE', 'Palasttorwächter', NULL, NULL, NULL, 23222), -- 69513
(69527, 'deDE', 'Steinschild der Zandalari', NULL, NULL, NULL, 23222), -- 69527
(69625, 'deDE', 'Waldläufer der Sonnenhäscher', 'Waldläuferin der Sonnenhäscher', NULL, NULL, 23222), -- 69625
(69258, 'deDE', 'OLD General Purpose Stalker', NULL, NULL, NULL, 23222), -- 69258
(69619, 'deDE', 'Erzmagier Aethas Sonnenhäscher', NULL, NULL, NULL, 23222), -- 69619
(69620, 'deDE', 'Späherhauptmann Elsia', NULL, 'Angriffsleiterin der Horde', NULL, 23222), -- 69620
(69617, 'deDE', 'Lor''themar Theron', NULL, 'Lordregent von Quel''Thalas', NULL, 23222), -- 69617
(69616, 'deDE', 'Lady Jaina Prachtmeer', NULL, 'Anführerin der Kirin Tor', NULL, 23222), -- 69616
(70128, 'deDE', 'Zahmer Donnerflügel', NULL, NULL, NULL, 23222), -- 70128
(69615, 'deDE', 'Vereesa Windläufer', NULL, 'Angriffsleiterin der Allianz', NULL, 23222), -- 69615
(69624, 'deDE', 'Zauberschütze des Silberbunds', 'Zauberschützin des Silberbunds', NULL, NULL, 23222), -- 69624
(69530, 'deDE', 'Shan''Bu', NULL, NULL, NULL, 23222), -- 69530
(69618, 'deDE', 'Yalia Weisenwisper', NULL, NULL, NULL, 23222), -- 69618
(69501, 'deDE', 'Taran Zhu', NULL, 'Meister der Shado-Pan', NULL, 23222), -- 69501
(69621, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 69621
(69510, 'deDE', 'Palasttorwächter', NULL, NULL, NULL, 23222), -- 69510
(69744, 'deDE', 'Vereesa Windläufer', NULL, 'Waldläufergeneralin des Silberbunds', NULL, 23222), -- 69744
(70193, 'deDE', 'Zauberschütze des Silberbunds', 'Zauberschützin des Silberbunds', NULL, NULL, 23222), -- 70193
(70615, 'deDE', 'Deaktivierter Zugriffsgenerator', NULL, NULL, NULL, 23222), -- 70615
(70211, 'deDE', 'Gate Bunny', NULL, NULL, NULL, 23222), -- 70211
(69212, 'deDE', 'Verzerrter Geist', NULL, NULL, NULL, 23222), -- 69212
(70105, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 70105
(70078, 'deDE', 'Sentry Beam Bunny', NULL, NULL, NULL, 23222), -- 70078
(69544, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 69544
(70566, 'deDE', 'Betrunkener Schwingenreiter', NULL, NULL, NULL, 23222), -- 70566
(70511, 'deDE', 'Zandalarigolem', NULL, NULL, NULL, 23222), -- 70511
(69622, 'deDE', 'Gottkoloss der Drakkari', NULL, NULL, NULL, 23222), -- 69622
(70118, 'deDE', 'Rekrut der Zandalari', NULL, NULL, NULL, 23222), -- 70118
(70123, 'deDE', 'Raptor der Zandalari', NULL, NULL, NULL, 23222), -- 70123
(70120, 'deDE', 'Pterrorschwinge', NULL, NULL, NULL, 23222), -- 70120
(69552, 'deDE', 'Schwingenreiterveteran', NULL, NULL, NULL, 23222), -- 69552
(70119, 'deDE', 'Bestienführer der Zandalari', NULL, NULL, NULL, 23222), -- 70119
(70263, 'deDE', 'Alte Pterrorschwinge', NULL, NULL, NULL, 23222), -- 70263
(69482, 'deDE', 'Kapitän Halu''kal', NULL, NULL, NULL, 23222), -- 69482
(70114, 'deDE', 'Taoshi Line Bunny', NULL, NULL, NULL, 23222), -- 70114
(69468, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 69468
(69472, 'deDE', 'Donnerflügel', NULL, NULL, NULL, 23222), -- 69472
(70121, 'deDE', 'Geistverzerrer der Zandalari', NULL, NULL, NULL, 23222), -- 70121
(69743, 'deDE', 'Shan''Bu', NULL, NULL, NULL, 23222), -- 69743
(69483, 'deDE', 'Hu''seng der Torwächter', NULL, NULL, NULL, 23222), -- 69483
(69505, 'deDE', 'Totem des Wachens', NULL, NULL, NULL, 23222), -- 69505
(69463, 'deDE', 'Schwingenreiter der Zandalari', NULL, NULL, NULL, 23222), -- 69463
(69464, 'deDE', 'Pterrorschwinge der Zandalari', NULL, NULL, NULL, 23222), -- 69464
(69457, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 69457
(69458, 'deDE', 'Donnerflügel', NULL, NULL, NULL, 23222), -- 69458
(70353, 'deDE', 'Shipyard Scenario Alliance Breadcrumb Bunny', NULL, NULL, NULL, 23222), -- 70353
(69741, 'deDE', 'Lady Jaina Prachtmeer', NULL, 'Anführerin der Kirin Tor', NULL,  23222), -- 69741
(70297, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 70297
(69717, 'deDE', 'Taran Zhu', NULL, 'Meister der Shado-Pan', NULL, 23222), -- 69717
(70237, 'deDE', 'Waldläufer des Silberbunds', 'Waldläuferin des Silberbunds', NULL, NULL, 23222), -- 70237
(70574, 'deDE', 'Magier der Kirin Tor', NULL, NULL, NULL, 23222), -- 70574
(69962, 'deDE', 'Sklavenmeister Shiaxu', NULL, NULL, NULL, 23222), -- 69962
(69154, 'deDE', 'Saurierrufer der Zandalari', NULL, NULL, NULL, 23222), -- 69154
(69635, 'deDE', 'Funke der Macht', NULL, NULL, NULL, 23222), -- 69635
(69192, 'deDE', 'Blitzeinschlag', NULL, NULL, NULL, 23222), -- 69192
(67576, 'deDE', 'Jugendliches Terrorhorn', NULL, NULL, NULL, 23222), -- 67576
(69917, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 69917
(69810, 'deDE', 'Taoshi', NULL, NULL, NULL, 23222), -- 69810
(69602, 'deDE', 'Manfred', NULL, NULL, NULL, 23222), -- 69602
(69559, 'deDE', 'Bestienmeister Horaki', NULL, NULL, NULL, 23222), -- 69559
(69207, 'deDE', 'Junger Himmelskreischer', NULL, NULL, NULL, 23222), -- 69207
(67477, 'deDE', 'Widerspenstiger Teufelssaurier', NULL, NULL, NULL, 23222), -- 67477
(69286, 'deDE', 'Meisterrufer', NULL, NULL, NULL, 23222), -- 69286
(67466, 'deDE', 'Versklaver der Shan''ze', NULL, NULL, NULL, 23222), -- 67466
(69140, 'deDE', 'Sauroksklave', NULL, NULL, NULL, 23222), -- 69140
(69217, 'deDE', 'Manfred', NULL, NULL, NULL, 23222), -- 69217
(69216, 'deDE', 'Blitzrichter der Shan''ze', NULL, NULL, NULL, 23222), -- 69216
(69326, 'deDE', 'Metallfürst Mono-Han', NULL, NULL, NULL, 23222), -- 69326
(70547, 'deDE', 'Pterrorschwinge der Zandalari', NULL, NULL, NULL, 23222), -- 70547
(70546, 'deDE', 'Pterrorschwinge der Zandalari', NULL, NULL, NULL, 23222), -- 70546
(69523, 'deDE', 'Fette Ziege', NULL, NULL, NULL, 23222), -- 69523
(69521, 'deDE', 'Bestienrufer der Zandalari', NULL, NULL, NULL, 23222), -- 69521
(69511, 'deDE', 'Pterrorschwinge der Zandalari', NULL, NULL, NULL, 23222), -- 69511
(69923, 'deDE', 'Sklavenmeister Shiaxu', NULL, NULL, NULL, 23222), -- 69923
(69493, 'deDE', 'Mauerwurm', NULL, NULL, NULL, 23222), -- 69493
(69384, 'deDE', 'Lumineszierender Krabbler', NULL, NULL, NULL, 23222), -- 69384
(69598, 'deDE', 'Toter Compy', NULL, NULL, NULL, 23222), -- 69598
(69155, 'deDE', 'Fetischbinder der Zandalari', NULL, NULL, NULL, 23222), -- 69155
(69142, 'deDE', 'Gehörnte Schlachtbestie', NULL, NULL, NULL, 23222), -- 69142
(69376, 'deDE', 'Schüler von Jalak', NULL, NULL, NULL, 23222), -- 69376
(58071, 'deDE', 'Abgerichteter Compy', NULL, NULL, NULL, 23222), -- 58071
(69186, 'deDE', 'Schutzzauber der Kirin Tor', NULL, NULL, NULL, 23222), -- 69186
(70208, 'deDE', 'Donnerflügel', NULL, NULL, NULL, 23222), -- 70208
(70041, 'deDE', 'Vereesa Windläufer', NULL, 'Waldläufergeneralin des Silberbunds', NULL, 23222), -- 70041
(70040, 'deDE', 'Lady Jaina Prachtmeer', NULL, 'Anführerin der Kirin Tor', NULL, 23222), -- 70040
(70517, 'deDE', 'Erzmagier Lan''dalock', NULL, NULL, NULL, 23222), -- 70517
(28682, 'deDE', 'Inzi Zauberlicht', NULL, 'Bardame', NULL, 23222), -- 28682
(19172, 'deDE', 'Gnomenbürger', 'Gnomenbürgerin', NULL, NULL, 23222), -- 19172
(32685, 'deDE', 'Kitz Stolzbrise', NULL, NULL, NULL, 23222), -- 32685
(32681, 'deDE', 'Der großartige Merleaux', NULL, NULL, NULL, 23222), -- 32681
(37776, 'deDE', 'Lehrling Nelphi', NULL, 'Kirin Tor', NULL, 23222), -- 37776
(69303, 'deDE', 'Arkaner Beobachter', NULL, NULL, NULL, 23222), -- 69303
(69451, 'deDE', 'Spell Focus Stalker', NULL, NULL, NULL, 23222), -- 69451
(69335, 'deDE', 'Geist-gebundener Wachposten', NULL, NULL, NULL, 23222), -- 69335
(69952, 'deDE', 'Schwerer Sprengstoff', NULL, NULL, NULL, 23222), -- 69952
(69297, 'deDE', 'Haruspex der Zandalari', NULL, NULL, NULL, 23222), -- 69297
(69295, 'deDE', 'Akolyth der Zandalari', NULL, NULL, NULL, 23222), -- 69295
(69623, 'deDE', 'Scenario 5.2 - Tear Down This Wall - Wall Destroyer Bunny - JSB', NULL, NULL, NULL, 23222), -- 69623
(69429, 'deDE', 'Gifthuscher', NULL, NULL, NULL, 23222), -- 69429
(69349, 'deDE', 'Dunkelaugenrabe', NULL, NULL, NULL, 23222), -- 69349
(70092, 'deDE', 'Lady Jaina Prachtmeer', NULL, 'Anführerin der Kirin Tor', NULL, 23222), -- 70092
(67768, 'deDE', 'Sumpfhüpfer', NULL, NULL, NULL, 23222), -- 67768
(69296, 'deDE', 'Seelenverzerrer der Zandalari', NULL, NULL, NULL, 23222), -- 69296
(69755, 'deDE', 'Kriegsgott Al''chukla', NULL, NULL, NULL, 23222), -- 69755
(70591, 'deDE', 'Mächtiger Teufelssaurier', NULL, NULL, NULL, 23222), -- 70591
(69294, 'deDE', 'Blutwache der Zandalari', NULL, NULL, NULL, 23222), -- 69294
(69995, 'deDE', 'Zauberklinge des Silberbunds', NULL, NULL, NULL, 23222), -- 69995
(70397, 'deDE', 'Hauptmann des Silberbunds', 'Hauptmann des Silberbunds', NULL, NULL, 23222), -- 70397
(70396, 'deDE', 'Zauberklinge des Silberbunds', 'Zauberklinge des Silberbunds', NULL, NULL, 23222), -- 70396
(70395, 'deDE', 'Assassine des Silberbunds', 'Assassine des Silberbunds', NULL, NULL, 23222), -- 70395
(67763, 'deDE', 'Uralte Schieferpanzerschildkröte', NULL, NULL, NULL, 23222), -- 67763
(70090, 'deDE', 'Assassine des Silberbunds', 'Assassine des Silberbunds', NULL, NULL, 23222), -- 70090
(70089, 'deDE', 'Zauberschütze des Silberbunds', 'Zauberschützin des Silberbunds', NULL, NULL, 23222), -- 70089
(67772, 'deDE', 'Schleichende Moorbestie', NULL, NULL, NULL, 23222), -- 67772
(67770, 'deDE', 'Giftzahnglitscher', NULL, NULL, NULL, 23222), -- 67770
(69865, 'deDE', 'Steinschild der Zandalari', NULL, NULL, NULL, 23222), -- 69865
(70091, 'deDE', 'Hauptmann des Silberbunds', 'Hauptmann des Silberbunds', NULL, NULL, 23222), -- 70091
(308, 'deDE', 'Rappe', NULL, NULL, NULL, 23222), -- 308
(306, 'deDE', 'Palomino', NULL, NULL, NULL, 23222), -- 306
(65753, 'deDE', 'Totally Generic Bunny - GIGANTIC (JSB)', NULL, NULL, NULL, 23222), -- 65753
(69225, 'deDE', 'Geisterbinder der Zandalari', NULL, NULL, NULL, 23222), -- 69225
(69171, 'deDE', 'Jaguarkrieger der Zandalari', NULL, NULL, NULL, 23222), -- 69171
(69343, 'deDE', 'Lightning Field Stalker', NULL, NULL, NULL, 23222), -- 69343
(69558, 'deDE', 'Koloss der Zandalari', NULL, NULL, NULL, 23222), -- 69558
(68847, 'deDE', 'Fußsoldat der Operation Schildwall', 'Fußsoldatin der Operation Schildwall', NULL, NULL, 23222), -- 68847
(68844, 'deDE', 'Fußsoldat der Operation Schildwall', 'Fußsoldatin der Operation Schildwall', NULL, NULL, 23222), -- 68844
(69949, 'deDE', 'Vereesa Windläufer', NULL, 'Waldläufergeneralin des Silberbunds', NULL, 23222), -- 69949
(70146, 'deDE', 'Donnerflügel', NULL, NULL, NULL, 23222), -- 70146
(70182, 'deDE', 'Isirami Sanftwind', NULL, 'Gastwirtin', NULL, 23222), -- 70182
(19171, 'deDE', 'Draeneibürger', 'Draeneibürgerin', NULL, NULL, 23222), -- 19171
(70185, 'deDE', 'Kazmo', NULL, NULL, NULL, 23222), -- 70185
(70262, 'deDE', 'Waldläufer des Silberbunds', 'Waldläuferin des Silberbunds', NULL, NULL, 23222), -- 70262
(70349, 'deDE', 'Gelehrter der Kirin Tor', 'Gelehrte der Kirin Tor', NULL, NULL, 23222), -- 70349
(69895, 'deDE', 'Himmelszauberschutz der Kirin Tor', NULL, NULL, NULL, 23222), -- 69895
(70309, 'deDE', 'Wächter der Kirin Tor', 'Wächterin der Kirin Tor', NULL, NULL, 23222), -- 70309
(70337, 'deDE', 'Verteidiger der Kirin Tor', 'Verteidigerin der Kirin Tor', NULL, NULL, 23222), -- 70337
(70561, 'deDE', 'Befehlskarte', NULL, NULL, NULL, 23222), -- 70561
(70343, 'deDE', 'Arbeiter der Violetten Erhebung', NULL, NULL, NULL, 23222), -- 70343
(69476, 'deDE', 'Geistermaske', NULL, NULL, NULL, 23222), -- 69476
(69531, 'deDE', 'Anki', NULL, NULL, NULL, 23222), -- 69531
(69532, 'deDE', 'Bestienrufer Pakutesh', NULL, NULL, NULL, 23222), -- 69532
(70287, 'deDE', 'Alchemist von Shaol''mara', NULL, NULL, NULL, 23222), -- 70287
(70290, 'deDE', 'Speerkämpfer von Shaol''mara', NULL, NULL, NULL, 23222), -- 70290
(69689, 'deDE', 'Speerwerfer der Zandalari', NULL, NULL, NULL, 23222), -- 69689
(69516, 'deDE', 'Zandalaribarrikade', NULL, NULL, NULL, 23222), -- 69516
(69515, 'deDE', 'Zandalaribarrikade', NULL, NULL, NULL, 23222), -- 69515
(70291, 'deDE', 'Steinschild von Shaol''mara', NULL, NULL, NULL, 23222), -- 70291
(70289, 'deDE', 'Raptor von Shaol''mara', NULL, NULL, NULL, 23222), -- 70289
(70285, 'deDE', 'Bestienrufer von Shaol''mara', NULL, NULL, NULL, 23222), -- 70285
(69538, 'deDE', 'Arkanital Mara''kah', NULL, NULL, NULL, 23222), -- 69538
(70288, 'deDE', 'Raptor von Shaol''mara', NULL, NULL, NULL, 23222), -- 70288
(70286, 'deDE', 'Bestienrufer von Shaol''mara', NULL, NULL, NULL, 23222), -- 70286
(69508, 'deDE', 'Zeb''tula Generic Bunny', NULL, NULL, NULL, 23222), -- 69508
(67704, 'deDE', 'Dornenschwanzbiber', NULL, NULL, NULL, 23222), -- 67704
(70069, 'deDE', 'Animator der Shan''ze', NULL, NULL, NULL, 23222), -- 70069
(67703, 'deDE', 'Schieferpanzerschildkröte', NULL, NULL, NULL, 23222), -- 67703
(67473, 'deDE', 'Animierter Krieger', NULL, NULL, NULL, 23222), -- 67473
(67771, 'deDE', 'Bernglühwürmchen', NULL, NULL, NULL, 23222), -- 67771
(69404, 'deDE', 'Halbwüchsiger Himmelskreischer', NULL, NULL, NULL, 23222), -- 69404
(69244, 'deDE', 'Wilder Knöchelbeißer', NULL, NULL, NULL, 23222), -- 69244
(69200, 'deDE', 'Gottkoloss der Drakkari', NULL, NULL, NULL, 23222), -- 69200
(69269, 'deDE', 'Anwärter der Zandalari', NULL, NULL, NULL, 23222), -- 69269
(69223, 'deDE', 'Steinschild der Zandalari', NULL, NULL, NULL, 23222), -- 69223
(69170, 'deDE', 'Bürger der Zandalari', NULL, NULL, NULL, 23222), -- 69170
(69162, 'deDE', 'Halbwüchsiger Himmelskreischer', NULL, NULL, NULL, 23222), -- 69162
(70233, 'deDE', 'Hippogryph des Silberbunds', NULL, NULL, NULL, 23222), -- 70233
(69678, 'deDE', 'Abbild von Erzmagier Vargoth', NULL, NULL, NULL, 23222), -- 69678
(69571, 'deDE', 'Zeb''tula Barrier Bunny', NULL, NULL, NULL, 23222), -- 69571
(69675, 'deDE', 'Lyalia', NULL, 'Kommandantin der Schildwachen', NULL, 23222), -- 69675
(69668, 'deDE', 'Danara Silberglas', NULL, 'Kirin Tor', NULL, 23222), -- 69668
(69674, 'deDE', 'Dalvin Jaacor', NULL, NULL, NULL, 23222), -- 69674
(69600, 'deDE', 'Vereesa Windläufer', NULL, 'Waldläufergeneralin des Silberbunds', NULL, 23222), -- 69600
(70231, 'deDE', 'Hippogryph des Silberbunds', NULL, NULL, NULL, 23222), -- 70231
(70213, 'deDE', 'Bogenschütze des Silberbunds', 'Bogenschützin des Silberbunds', NULL, NULL, 23222), -- 70213
(70214, 'deDE', 'Hippogryph des Silberbunds', NULL, NULL, NULL, 23222), -- 70214
(67996, 'deDE', 'Erzmagierin Modera', NULL, 'Kirin Tor', NULL, 23222), -- 67996
(69677, 'deDE', 'Sanitärzauberer Stephen Streichholz', NULL, 'Kirin Tor', NULL, 23222), -- 69677
(67997, 'deDE', 'Narasi Schneedämmerung', NULL, 'Der Silberbund', NULL, 23222), -- 67997
(69670, 'deDE', 'Vylene Amaranth', NULL, 'Der Silberbund', NULL, 23222), -- 69670
(69673, 'deDE', 'Ako', NULL, NULL, NULL, 23222), -- 69673
(70234, 'deDE', 'Hippogryph des Silberbunds', NULL, NULL, 'vehichlecursor', 23222), -- 70234
(70370, 'deDE', 'Lady Jaina Prachtmeer', NULL, 'Anführerin der Kirin Tor', NULL, 23222), -- 70370
(67994, 'deDE', 'Kriegsmagier Yurias', NULL, NULL, NULL, 23222), -- 67994
(67999, 'deDE', 'Miura Hellweber', NULL, NULL, NULL, 23222), -- 67999
(67998, 'deDE', 'Späherhauptmann Daelin', NULL, NULL, NULL, 23222), -- 67998
(67992, 'deDE', 'Lady Jaina Prachtmeer', NULL, 'Anführerin der Kirin Tor', NULL, 23222), -- 67992
(70518, 'deDE', 'Location 1', NULL, NULL, NULL, 23222), -- 70518
(67995, 'deDE', 'Kapitänin Elleane Schaumkrone', NULL, NULL, NULL, 23222), -- 67995
(67993, 'deDE', 'Vereesa Windläufer', NULL, 'Waldläufergeneralin des Silberbunds', NULL, 23222), -- 67993
(68001, 'deDE', 'Savinia Lehrensang', NULL, 'Der Silberbund', NULL, 23222), -- 68001
(68000, 'deDE', 'Hiren Lehrensang', NULL, 'Rüstmeister der Kirin Tor', NULL, 23222), -- 68000
(70515, 'deDE', 'Isirami Sanftwind', NULL, NULL, NULL, 23222), -- 70515
(70184, 'deDE', 'Tassia Flüsterschlucht', NULL, 'Stallmeisterin', NULL, 23222), -- 70184
(70183, 'deDE', 'Valaden Silberklinge', NULL, 'Reparaturen', NULL, 23222), -- 70183
(69613, 'deDE', 'Arbeiter der Violetten Erhebung', NULL, NULL, NULL, 23222), -- 69613
(67970, 'deDE', 'Zauberklinge des Silberbunds', NULL, NULL, NULL, 23222), -- 67970
(67937, 'deDE', 'Manageist der Kirin Tor', NULL, NULL, NULL, 23222), -- 67937
(67934, 'deDE', 'Konstrukt der Kirin Tor', NULL, NULL, NULL, 23222), -- 67934
(69411, 'deDE', 'Himmelskreischer der Zandalari', NULL, NULL, NULL, 23222), -- 69411
(69412, 'deDE', 'Bestienrufer der Zandalari', NULL, NULL, NULL, 23222), -- 69412
(69156, 'deDE', 'Himmelskreischer der Zandalari', NULL, NULL, NULL, 23222), -- 69156
(69397, 'deDE', 'Bestienrufer der Zandalari', NULL, NULL, NULL, 23222), -- 69397
(69338, 'deDE', 'Schläger der Skumklingen', NULL, NULL, NULL, 23222), -- 69338
(69229, 'deDE', 'Saurierpriester der Skumklingen', NULL, NULL, NULL, 23222), -- 69229
(69234, 'deDE', 'Ihgaluk-Schabe', NULL, NULL, NULL, 23222), -- 69234
(69695, 'deDE', 'Bedrängtes Konstrukt des Silberbunds', NULL, NULL, NULL, 23222), -- 69695
(69348, 'deDE', 'Stumpfzahn der Skumklingen', NULL, NULL, NULL, 23222), -- 69348
(69358, 'deDE', 'Ihgaluk-Basilisk', NULL, NULL, NULL, 23222), -- 69358
(69227, 'deDE', 'Plünderer der Skumklingen', NULL, NULL, NULL, 23222), -- 69227
(69228, 'deDE', 'Dreckschleuderer der Skumklingen', NULL, NULL, NULL, 23222), -- 69228
(69210, 'deDE', 'Fleischreißer der Skumklingen', NULL, NULL, NULL, 23222), -- 69210
(69226, 'deDE', 'Meerdrache der Skumklingen', NULL, NULL, NULL, 23222), -- 69226
(70226, 'deDE', 'Kroshik', NULL, NULL, NULL, 23222), -- 70226
(70501, 'deDE', 'Hippogryph des Silberbunds', NULL, NULL, NULL, 23222), -- 70501
(70372, 'deDE', 'Vereesa Windläufer', NULL, 'Waldläufergeneralin des Silberbunds', NULL, 23222), -- 70372
(70503, 'deDE', 'Hippogryph des Silberbunds', NULL, NULL, NULL, 23222), -- 70503
(73476, 'deDE', 'Salzbisskrokilisk', NULL, NULL, NULL, 23222), -- 73476
(70366, 'deDE', 'Hippogryph des Silberbunds', NULL, NULL, NULL, 23222), -- 70366
(70482, 'deDE', 'Vereesa Windläufer', NULL, 'Waldläufergeneralin des Silberbunds', NULL, 23222), -- 70482
(70369, 'deDE', 'Hippogryph des Silberbunds', NULL, NULL, NULL, 23222), -- 70369
(108655, 'deDE', 'Brut von Serpentrix', NULL, NULL, NULL, 23222); -- 108655


DELETE FROM `page_text_locale` WHERE (`ID`=4776 AND `locale`='deDE') OR (`ID`=4775 AND `locale`='deDE') OR (`ID`=4774 AND `locale`='deDE') OR (`ID`=4758 AND `locale`='deDE');
INSERT INTO `page_text_locale` (`ID`, `locale`,`Text`, `VerifiedBuild`) VALUES
(4776, 'deDE', 'Lei Shen vereinheitlichte die Sprache, setzte eine gemeinsame Währung ein, standardisierte Gewichte und Maßeinheiten und gründete ein Kaiserreich.$b$BZum ersten Mal waren die Völker des Landes vereint. Der Donnerkönig betrachtete ihr Leid als einen geringen Preis... lediglich eine Schwäche des Fleisches.', 23222), -- 4776
(4775, 'deDE', 'Aber viele Mogu sahen in Lei Shen das gemeinsame Ziel, das in den Generationen seit dem Beginn des Schweigens ihrer Meister gefehlt hatte. Sie marschierten unter dem Banner des Donnerkönigs. Sie dienten ihm bereitwillig, als er die anderen Völker versklavte und teilten seinen Glauben, dass die "niederen" Völker den Mogu dienen sollten, so wie die Mogu einst ihren Herren gedient hatten.', 23222), -- 4775
(4774, 'deDE', 'Bewaffnet mit der Macht der Stürme sammelte der Donnerkönig sein Gefolge und begann mit der systematischen Unterwerfung der anderen Kriegsfürsten der Mogu. Viele beugten sich seiner Autorität nicht: Die glücklicheren unter ihnen wurden von Blitzen zerstäubt oder von seinen anwachsenden Legionen zertrampelt. Die anderen wurden in Ketten verschleppt, bis er sie als "gebrochen" betrachtete.', 23222), -- 4774
(4758, 'deDE', 'Unter den Zandalari ist kein Platz für Schwäche. Stärke, Wildheit, Ausdauer, Macht: Das sind die Merkmale, an denen man Erfolg misst. Als Jugendliche müssen die männlichen Zandalari, die nicht zu Priestern oder Gelehrten erwählt wurden, dem Rat, dem König und den Göttern selbst ihre Stärke beweisen.$b$BEin beliebiger Beweis körperlicher Stärke reicht aus. Wenn Kinder zu Erwachsenen werden, werden Turniere und Wettbewerbe abgehalten. Jugendliche bereiten sich in jahrelangem Training, Zwiesprache mit den Geistern und dem Tätowieren magischer Siegel in ihre Haut auf ihre Prüfungen vor. Ein üblicher Ritus ist es, zu einer der brutalen, bestienbeherrschten Inseln nahe der Hauptstadt zu reisen, um ein wildes Tier zu stehlen oder zu zähmen.$b$BDie niederen Trollvölker haben ihre eigenen, bescheideneren Versionen dieser Tradition. Aber das Beherrschen eines Ravasaurus oder Raptors ist nichts gegen die Macht, die das Beherrschen eines Teufelssauriers oder Terrorhorns verlangt.', 23222); -- 4758


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text    xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_offer_reward` WHERE `ID` IN (32733 /*32733*/, 32706 /*32706*/, 32656 /*32656*/, 32655 /*32655*/, 32652 /*32652*/, 32654 /*32654*/, 32644 /*32644*/, 32681 /*32681*/);
INSERT INTO `quest_offer_reward` (`ID`, `Emote1`, `Emote2`, `Emote3`, `Emote4`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `EmoteDelay4`, `RewardText`, `VerifiedBuild`) VALUES
(32733, 1, 0, 0, 0, 0, 0, 0, 0, 'Noch jemand, der sich der Zandalarijagd anschließen möchte? Gut. Sehr gut.', 23222), -- 32733
(32706, 1, 0, 0, 0, 0, 0, 0, 0, 'Willkommen, $n.', 23222), -- 32706
(32656, 1, 0, 0, 0, 0, 0, 0, 0, 'Gut gemacht, $n. Shan''Bus Sturz wird die Mogu schwächen, und man wird sich Eurer Heldentaten erinnern.', 23222), -- 32656
(32655, 1, 0, 0, 0, 0, 0, 0, 0, 'Gut gemacht. Dieses Kriegsschiff wird nicht so bald wieder Feuer auf uns niedergehen lassen.$B$BTaran Zhu hat mir mitgeteilt, dass die Shado-Pan den Sturmgepeitschten Hafen bewachen werden, sodass wir die Ressourcen der Kirin Tor verwenden können, um den Angriff andernorts fortzuführen. Taoshi wird dort sein. Ich bin sicher, dass sie gern wieder mit Euch zusammenarbeiten würde.$B$BWir sind für Euren Beitrag dankbar, $n. Danke.', 23222), -- 32655
(32652, 1, 0, 0, 0, 0, 0, 0, 0, 'Unglaubliche Arbeit. Das bietet uns eine monumentale Gelegenheit.', 23222), -- 32652
(32654, 1, 0, 0, 0, 0, 0, 0, 0, 'Mit Eurer Hilfe sind wir unserem Ziel einen riesigen Schritt näher gekommen.', 23222), -- 32654
(32644, 1, 0, 0, 0, 0, 0, 0, 0, 'Ja! Ihr habt es geschafft, Champion!$B$BWir werden sein wie eine Flut, die über diese Insel hinwegbraust und unserem Volk eine neue Bestimmung bringt. Und Ihr, $gHeld:Heldin;, seid an vorderster Front.', 23222), -- 32644
(32681, 1, 0, 0, 0, 0, 0, 0, 0, 'Ihr kommt gerade rechtzeitig, $n. Dieses Land muss von den Kirin Tor kontrolliert werden, zum Wohl aller. Das wird keine leichte Aufgabe.', 23222); -- 32681


-- xOxOxOxOxOxOxOxOxOxOxOx     overwrite english text     xOxOxOxOxOxOxOxOxOxOxOx
DELETE FROM `quest_request_items` WHERE `ID`=32707;
INSERT INTO `quest_request_items` (`ID`, `EmoteOnComplete`, `EmoteOnIncomplete`, `EmoteOnCompleteDelay`, `EmoteOnIncompleteDelay`, `CompletionText`, `VerifiedBuild`) VALUES
(32707, 1, 0, 0, 0, 'Habt Ihr die Steine gefunden, von denen ich sprach?', 23222); -- 32707


-- dont exist -> insert
INSERT IGNORE INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `OptionBroadcastTextID`, `action_poi_id`, `box_coded`, `box_money`, `box_text`, `BoxBroadcastTextID`) VALUES
(14640, 0, 1, 'Ich möchte etwas von Euch kaufen.', 0, 0, 0, 0, '', 0),
(83, 0, 4, 'Bringt mich ins Leben zurück.', 0, 0, 0, 0, '', 0),
(15553, 0, 0, 'Ich bin bereit.', 0, 0, 0, 0, '', 0),
(15516, 0, 0, 'Ich bin bereit, den Angriff zu starten. [Für Solo-Instanz anmelden.]', 0, 0, 0, 0, '', 0),
(15559, 0, 0, 'Ich bin bei Euch. Brechen wir in diese Werft ein! [Für Solo-Instanz anmelden.]', 0, 0, 0, 0, '', 0),
(15523, 0, 0, 'Also gut. Los geht''s.', 0, 0, 0, 0, '', 0),
(15516, 2, 0, 'Ich bin bereit! [Für Solo-Instanz anmelden.]', 0, 0, 0, 0, '', 0),
(15506, 0, 0, 'Hier ist der Sprengstoff, Vereesa.', 0, 0, 0, 0, '', 0),
(15516, 3, 0, 'Ich bin bereit, die Mauer zu Fall zu bringen! [Für Solo-Instanz anmelden.]', 0, 0, 0, 0, '', 0),
(15516, 1, 0, 'Ich bin bereit! [Für Solo-Instanz anmelden.]', 0, 0, 0, 0, '', 0),
(15570, 0, 0, 'Ich bin bereit.', 0, 0, 0, 0, '', 0);


-- BroadcastTestId not exist
-- DELETE FROM `creature_text` WHERE (`entry`=67661 AND `groupid`=0) OR (`entry`=67661 AND `groupid`=1) OR (`entry`=67661 AND `groupid`=2) OR (`entry`=67661 AND `groupid`=3) OR (`entry`=67930 AND `groupid`=0) OR (`entry`=67992 AND `groupid`=0) OR (`entry`=67992 AND `groupid`=1) OR (`entry`=67992 AND `groupid`=2) OR (`entry`=67992 AND `groupid`=3) OR (`entry`=67992 AND `groupid`=4) OR (`entry`=67992 AND `groupid`=5) OR (`entry`=67992 AND `groupid`=6) OR (`entry`=67992 AND `groupid`=7) OR (`entry`=67992 AND `groupid`=8) OR (`entry`=69099 AND `groupid`=0) OR (`entry`=69099 AND `groupid`=1) OR (`entry`=69099 AND `groupid`=2) OR (`entry`=69136 AND `groupid`=0) OR (`entry`=69210 AND `groupid`=0) OR (`entry`=69210 AND `groupid`=1) OR (`entry`=69223 AND `groupid`=0) OR (`entry`=69227 AND `groupid`=0) OR (`entry`=69250 AND `groupid`=0) OR (`entry`=69265 AND `groupid`=0) OR (`entry`=69272 AND `groupid`=0) OR (`entry`=69391 AND `groupid`=0) OR (`entry`=69435 AND `groupid`=0) OR (`entry`=69435 AND `groupid`=1) OR (`entry`=69444 AND `groupid`=0) OR (`entry`=69457 AND `groupid`=0) OR (`entry`=69457 AND `groupid`=1) OR (`entry`=69457 AND `groupid`=2) OR (`entry`=69461 AND `groupid`=0) OR (`entry`=69461 AND `groupid`=1) OR (`entry`=69461 AND `groupid`=2) OR (`entry`=69465 AND `groupid`=0) OR (`entry`=69468 AND `groupid`=0) OR (`entry`=69468 AND `groupid`=1) OR (`entry`=69468 AND `groupid`=2) OR (`entry`=69468 AND `groupid`=3) OR (`entry`=69468 AND `groupid`=4) OR (`entry`=69468 AND `groupid`=5) OR (`entry`=69468 AND `groupid`=6) OR (`entry`=69468 AND `groupid`=7) OR (`entry`=69468 AND `groupid`=8) OR (`entry`=69482 AND `groupid`=0) OR (`entry`=69482 AND `groupid`=1) OR (`entry`=69482 AND `groupid`=2) OR (`entry`=69483 AND `groupid`=0) OR (`entry`=69483 AND `groupid`=1) OR (`entry`=69483 AND `groupid`=2) OR (`entry`=69483 AND `groupid`=3) OR (`entry`=69501 AND `groupid`=0) OR (`entry`=69501 AND `groupid`=1) OR (`entry`=69501 AND `groupid`=2) OR (`entry`=69501 AND `groupid`=3) OR (`entry`=69501 AND `groupid`=4) OR (`entry`=69501 AND `groupid`=5) OR (`entry`=69501 AND `groupid`=6) OR (`entry`=69501 AND `groupid`=7) OR (`entry`=69501 AND `groupid`=8) OR (`entry`=69501 AND `groupid`=9) OR (`entry`=69501 AND `groupid`=10) OR (`entry`=69501 AND `groupid`=11) OR (`entry`=69501 AND `groupid`=12) OR (`entry`=69501 AND `groupid`=13) OR (`entry`=69501 AND `groupid`=14) OR (`entry`=69501 AND `groupid`=15) OR (`entry`=69501 AND `groupid`=16) OR (`entry`=69501 AND `groupid`=17) OR (`entry`=69501 AND `groupid`=18) OR (`entry`=69501 AND `groupid`=19) OR (`entry`=69501 AND `groupid`=20) OR (`entry`=69530 AND `groupid`=0) OR (`entry`=69530 AND `groupid`=1) OR (`entry`=69530 AND `groupid`=2) OR (`entry`=69530 AND `groupid`=3) OR (`entry`=69530 AND `groupid`=4) OR (`entry`=69534 AND `groupid`=0) OR (`entry`=69534 AND `groupid`=1) OR (`entry`=69538 AND `groupid`=0) OR (`entry`=69538 AND `groupid`=1) OR (`entry`=69538 AND `groupid`=2) OR (`entry`=69538 AND `groupid`=3) OR (`entry`=69538 AND `groupid`=4) OR (`entry`=69544 AND `groupid`=0) OR (`entry`=69544 AND `groupid`=1) OR (`entry`=69544 AND `groupid`=2) OR (`entry`=69544 AND `groupid`=3) OR (`entry`=69544 AND `groupid`=4) OR (`entry`=69559 AND `groupid`=0) OR (`entry`=69559 AND `groupid`=1) OR (`entry`=69559 AND `groupid`=2) OR (`entry`=69559 AND `groupid`=3) OR (`entry`=69600 AND `groupid`=0) OR (`entry`=69600 AND `groupid`=1) OR (`entry`=69600 AND `groupid`=2) OR (`entry`=69600 AND `groupid`=3) OR (`entry`=69600 AND `groupid`=4) OR (`entry`=69600 AND `groupid`=5) OR (`entry`=69600 AND `groupid`=6) OR (`entry`=69600 AND `groupid`=7) OR (`entry`=69600 AND `groupid`=8) OR (`entry`=69600 AND `groupid`=9) OR (`entry`=69600 AND `groupid`=10) OR (`entry`=69600 AND `groupid`=11) OR (`entry`=69600 AND `groupid`=12) OR (`entry`=69600 AND `groupid`=13) OR (`entry`=69600 AND `groupid`=14) OR (`entry`=69600 AND `groupid`=15) OR (`entry`=69600 AND `groupid`=16) OR (`entry`=69600 AND `groupid`=17) OR (`entry`=69600 AND `groupid`=18) OR (`entry`=69615 AND `groupid`=0) OR (`entry`=69615 AND `groupid`=1) OR (`entry`=69615 AND `groupid`=2) OR (`entry`=69615 AND `groupid`=3) OR (`entry`=69615 AND `groupid`=4) OR (`entry`=69616 AND `groupid`=0) OR (`entry`=69616 AND `groupid`=1) OR (`entry`=69616 AND `groupid`=2) OR (`entry`=69616 AND `groupid`=3) OR (`entry`=69616 AND `groupid`=4) OR (`entry`=69616 AND `groupid`=5) OR (`entry`=69616 AND `groupid`=6) OR (`entry`=69616 AND `groupid`=7) OR (`entry`=69616 AND `groupid`=8) OR (`entry`=69616 AND `groupid`=9) OR (`entry`=69616 AND `groupid`=10) OR (`entry`=69616 AND `groupid`=11) OR (`entry`=69616 AND `groupid`=12) OR (`entry`=69616 AND `groupid`=13) OR (`entry`=69616 AND `groupid`=14) OR (`entry`=69616 AND `groupid`=15) OR (`entry`=69617 AND `groupid`=0) OR (`entry`=69617 AND `groupid`=1) OR (`entry`=69617 AND `groupid`=2) OR (`entry`=69617 AND `groupid`=3) OR (`entry`=69617 AND `groupid`=4) OR (`entry`=69617 AND `groupid`=5) OR (`entry`=69617 AND `groupid`=6) OR (`entry`=69617 AND `groupid`=7) OR (`entry`=69617 AND `groupid`=8) OR (`entry`=69617 AND `groupid`=9) OR (`entry`=69619 AND `groupid`=0) OR (`entry`=69620 AND `groupid`=0) OR (`entry`=69621 AND `groupid`=0) OR (`entry`=69621 AND `groupid`=1) OR (`entry`=69621 AND `groupid`=2) OR (`entry`=69621 AND `groupid`=3) OR (`entry`=69621 AND `groupid`=4) OR (`entry`=69621 AND `groupid`=5) OR (`entry`=69621 AND `groupid`=6) OR (`entry`=69652 AND `groupid`=0) OR (`entry`=69717 AND `groupid`=0) OR (`entry`=69741 AND `groupid`=0) OR (`entry`=69741 AND `groupid`=1) OR (`entry`=69743 AND `groupid`=0) OR (`entry`=69744 AND `groupid`=0) OR (`entry`=69755 AND `groupid`=0) OR (`entry`=69755 AND `groupid`=1) OR (`entry`=69755 AND `groupid`=2) OR (`entry`=69755 AND `groupid`=3) OR (`entry`=69755 AND `groupid`=4) OR (`entry`=69903 AND `groupid`=0) OR (`entry`=69917 AND `groupid`=0) OR (`entry`=69917 AND `groupid`=1) OR (`entry`=69917 AND `groupid`=2) OR (`entry`=69917 AND `groupid`=3) OR (`entry`=69917 AND `groupid`=4) OR (`entry`=69917 AND `groupid`=5) OR (`entry`=69923 AND `groupid`=0) OR (`entry`=69923 AND `groupid`=1) OR (`entry`=69923 AND `groupid`=2) OR (`entry`=69923 AND `groupid`=3) OR (`entry`=69923 AND `groupid`=4) OR (`entry`=69949 AND `groupid`=0) OR (`entry`=69949 AND `groupid`=1) OR (`entry`=69949 AND `groupid`=2) OR (`entry`=69949 AND `groupid`=3) OR (`entry`=69949 AND `groupid`=4) OR (`entry`=70040 AND `groupid`=0) OR (`entry`=70041 AND `groupid`=0) OR (`entry`=70041 AND `groupid`=1) OR (`entry`=70041 AND `groupid`=2) OR (`entry`=70041 AND `groupid`=3) OR (`entry`=70041 AND `groupid`=4) OR (`entry`=70041 AND `groupid`=5) OR (`entry`=70092 AND `groupid`=0) OR (`entry`=70092 AND `groupid`=1) OR (`entry`=70092 AND `groupid`=2) OR (`entry`=70092 AND `groupid`=3) OR (`entry`=70092 AND `groupid`=4) OR (`entry`=70092 AND `groupid`=5) OR (`entry`=70092 AND `groupid`=6) OR (`entry`=70105 AND `groupid`=0) OR (`entry`=70105 AND `groupid`=1) OR (`entry`=70105 AND `groupid`=2) OR (`entry`=70285 AND `groupid`=0) OR (`entry`=70297 AND `groupid`=0) OR (`entry`=70370 AND `groupid`=0) OR (`entry`=70370 AND `groupid`=1) OR (`entry`=70482 AND `groupid`=0) OR (`entry`=70482 AND `groupid`=1) OR (`entry`=70482 AND `groupid`=2);
INSERT IGNORE INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(67661, 0, 0, 'Tod der Allianz!', 12, 0, 100, 6, 0, 0, 0, 0, 'Sonnenhäschermagistrix to Player'),
(67661, 1, 0, 'Für die Horde!', 12, 0, 100, 6, 0, 0, 0, 0, 'Sonnenhäschermagistrix to Player'),
(67661, 2, 0, 'Ihr werdet nicht lebend entkommen, $R!', 12, 0, 100, 0, 0, 0, 0, 0, 'Sonnenhäschermagistrix to Player'),
(67661, 3, 0, 'Ich freu mich schon drauf...', 12, 0, 100, 0, 0, 0, 0, 0, 'Sonnenhäschermagistrix to Player'),
(67930, 0, 0, 'Nicht autorisiertes Personal identifiziert! Bestrafungsprotokoll aktiviert.', 12, 0, 100, 0, 0, 0, 0, 0, 'Konstrukt der Sonnenhäscher to Player'),
(67992, 0, 0, 'Ich habe eine Landestelle auf der Insel ausgewählt: erhöhtes Terrain, leicht zu verteidigen.', 12, 0, 100, 0, 0, 35526, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(67992, 1, 0, 'Leider haben sich die Zandalaritrolle bereits dort eingenistet und die Stellung befestigt.', 12, 0, 100, 0, 0, 35527, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(67992, 2, 0, 'Sie haben den Ort mit einer Geisterbarriere umhüllt. Wir müssen sie zu Fall bringen, bevor wir unsere Truppen herbeiteleportieren können.', 12, 0, 100, 0, 0, 35528, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(67992, 3, 0, 'Das ist der Geist der Allianz – wir sind nicht aufzuhalten!', 12, 0, 100, 273, 0, 35531, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(67992, 4, 0, 'Vereesa, sichert die Umgebung. Modera, stellt die Schutzzauber auf. Narasi, beginnt damit, die Zelte und Versorgungsgüter herzuteleportieren.', 12, 0, 100, 25, 0, 35532, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(67992, 5, 0, 'Alles auf Position! Wir sind auf allen Seiten von Feinden umringt! Wir haben keine Zeit zu verlieren.', 12, 0, 100, 1, 0, 35533, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(67992, 6, 0, 'Champion, Ihr habt meine Erwartungen übertroffen. Unsere Stellung auf der Insel ist gesichert, dank Eures Mutes und Eurer Entschlossenheit. Nun beginnt die wahre Arbeit.', 12, 0, 100, 1, 0, 35534, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(67992, 7, 0, 'Ich glaube, ich habe einen Plan, wie wir die Waffenkammer des Donnerkönigs infiltrieren können. Mit ein wenig Hilfe von den Shado-Pan.', 12, 0, 100, 0, 0, 35536, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(67992, 8, 0, 'Sprecht mit mir, wenn Ihr bereit seid, den Angriff zu starten!', 12, 0, 100, 0, 0, 35498, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69099, 0, 0, 'Ich bin die Verkörperung des Donners!', 14, 0, 100, 0, 0, 35732, 0, 0, 'Nalak'),
(69099, 1, 0, 'Könnt Ihr den kalten Hauch spüren? Ein Sturm naht...', 14, 0, 100, 0, 0, 35730, 0, 0, 'Nalak'),
(69099, 2, 0, 'Ich bin nur... die Dunkelheit... vor dem wahren Sturm...', 14, 0, 100, 0, 0, 35731, 0, 0, 'Nalak to Player'),
(69136, 0, 0, 'Die Macht des Loa kommt.', 12, 0, 100, 0, 0, 0, 0, 0, 'Zandalarisprecher to Player'),
(69210, 0, 0, 'Gah!', 12, 0, 100, 53, 0, 0, 0, 0, 'Fleischreißer der Skumklingen to Player'),
(69210, 1, 0, 'Eindringlinge!', 12, 0, 100, 0, 0, 0, 0, 0, 'Fleischreißer der Skumklingen to Player'),
(69223, 0, 0, 'Dein Tod wird Spaß mach''n.', 12, 0, 100, 0, 0, 0, 0, 0, 'Steinschild der Zandalari to Player'),
(69227, 0, 0, 'Was ist diese Magie?', 12, 0, 100, 15, 0, 0, 0, 0, 'Plünderer der Skumklingen to Player'),
(69250, 0, 0, 'Ja, kommt näher, kleines Wesen. Meinem Skalpell dürstet nach Eurem Blut!', 12, 0, 100, 0, 0, 0, 0, 0, 'Kaida der Blutvergießer to Player'),
(69265, 0, 0, 'Ich sollte Euch zerreißen, Allianzabschaum! Dieses eine Mal werde ich mich jedoch zurückhalten.', 12, 0, 100, 2, 0, 0, 0, 0, 'Späher der Sonnenhäscher to Seelenreißer der Shan''ze'),
(69272, 0, 0, 'Dieses Schiff is'' unter neuer Führung! Wer sich nich'' anstrengt, dessen Geist wird ''nem Golem zugewiesen!', 14, 0, 100, 0, 0, 0, 0, 0, 'Arkanwirker Uzan'),
(69391, 0, 0, 'Eure Fähigkeiten sind durchaus beeindruckend. Ihr werdet eines Tages einen ausgezeichneten Sklaven abgeben.', 15, 0, 100, 0, 0, 0, 0, 0, 'Der königliche Kämmerer to Player'),
(69435, 0, 0, 'BLUT! KÖSTLICH! Ihr werdet mich nähren!', 14, 0, 100, 0, 0, 0, 0, 0, 'Fleischschmied Hoku'),
(69435, 1, 0, 'Nein! Mein Blut, es… fließt…', 12, 0, 100, 0, 0, 0, 0, 0, 'Fleischschmied Hoku to Player'),
(69444, 0, 0, 'Wie können die Mogu nur zu solchen Untaten fähig sein? Dem Himmel sei Dank, dass Ihr mich gefunden habt.', 12, 0, 100, 2, 0, 0, 0, 0, 'Späher des Silberbunds to Seelenreißer der Shan''ze'),
(69457, 0, 0, 'Ich zähle sechs... nein, sieben Wachen, die am Himmel patrouillieren.', 12, 0, 100, 0, 0, 35852, 0, 0, 'Taoshi to Player'),
(69457, 1, 0, 'Einer von ihnen fliegt höher als die anderen... Er dreht weit draußen über dem Wasser seine Kreise – allein. Er überschätzt sich. Das ist unsere Lücke.', 12, 0, 100, 0, 0, 35853, 0, 0, 'Taoshi to Player'),
(69457, 2, 0, 'Wir schalten ihn aus und übernehmen seine Patrouille. Los geht''s!', 12, 0, 100, 0, 0, 35854, 0, 0, 'Taoshi to Player'),
(69461, 0, 0, 'Der Sturm wird Euch verschlingen!', 14, 0, 100, 0, 0, 0, 0, 0, 'Itoka'),
(69461, 1, 0, 'Herr des Donners… schütze mich!', 14, 0, 100, 0, 0, 0, 0, 0, 'Itoka'),
(69461, 2, 0, 'Und so… erlischt… des Lebens Funke...', 12, 0, 100, 0, 0, 0, 0, 0, 'Itoka to Player'),
(69465, 0, 0, 'Donnerkönig hat mir Macht gegeben! Kommt, ich zeig''s euch!', 14, 0, 100, 0, 0, 35550, 0, 0, 'Jin''rokh der Zerstörer to Player'),
(69468, 0, 0, 'Nur ein bisschen näher...', 12, 0, 100, 0, 0, 35855, 0, 0, 'Taoshi to Pterrorschwinge der Zandalari'),
(69468, 1, 0, 'HA!', 12, 0, 100, 0, 0, 35856, 0, 0, 'Taoshi to Pterrorschwinge der Zandalari'),
(69468, 2, 0, 'Die Zandalari sind vorsichtiger, als ich dachte. Seht, dort unten wimmelt es von Patrouillen.', 12, 0, 100, 0, 0, 35857, 0, 0, 'Taoshi to Taoshi Line Bunny'),
(69468, 3, 0, 'Und ihre Schamanen haben ein Netzwerk aus Wächtertotems aufgestellt.', 12, 0, 100, 0, 0, 35859, 0, 0, 'Taoshi to Taoshi Line Bunny'),
(69468, 4, 0, 'Eine Rauchbombe reicht, um eines ihrer Totems zu blenden.', 12, 0, 100, 0, 0, 35861, 0, 0, 'Taoshi to Taoshi Line Bunny'),
(69468, 5, 0, 'Bahnt uns einen Weg vom Schiff zum oberen Tor – wir werden zu Fuß dorthin gelangen müssen.', 12, 0, 100, 0, 0, 35862, 0, 0, 'Taoshi to Taoshi Line Bunny'),
(69468, 6, 0, 'Wir können hier oben nicht ewig kreisen. Tut, was Ihr könnt, während ich unsere nächsten Schritte plane.', 12, 0, 100, 0, 0, 35863, 0, 0, 'Taoshi to Taoshi Line Bunny'),
(69468, 7, 0, 'Die Zeit ist um. Wir müssen an Bord dieses Schiffes gelangen.', 12, 0, 100, 0, 0, 35864, 0, 0, 'Taoshi to Taoshi Line Bunny'),
(69468, 8, 0, 'Wir sollten die Belagerungswaffen am Bug sabotieren. Dann schleichen wir uns unter Deck und suchen nach dem Kapitän.', 12, 0, 100, 0, 0, 35868, 0, 0, 'Taoshi to Taoshi Line Bunny'),
(69482, 0, 0, '%s schnarcht vor sich hin.', 16, 0, 100, 0, 0, 35320, 0, 0, 'Kapitän Halu''kal to Player'),
(69482, 1, 0, 'Hm... wa...? Blinde Passagiere!', 12, 0, 100, 0, 0, 35321, 0, 0, 'Kapitän Halu''kal to Player'),
(69482, 2, 0, 'Ihr... kommt hier nie... lebend raus.', 12, 0, 100, 432, 0, 35325, 0, 0, 'Kapitän Halu''kal to Player'),
(69483, 0, 0, 'Wie der Donnerkönig es befiehlt, so wird es geschehen.', 12, 0, 100, 0, 0, 35446, 0, 0, 'Hu''seng der Torwächter to Player'),
(69483, 1, 0, 'Schluss mit dem Versteckspiel!', 14, 0, 100, 0, 0, 35447, 0, 0, 'Hu''seng der Torwächter to Player'),
(69483, 2, 0, 'Jetzt gibt es kein Entkommen. Der einzige Ausweg führt durch mich hindurch!', 12, 0, 100, 397, 0, 35448, 0, 0, 'Hu''seng der Torwächter to Player'),
(69483, 3, 0, 'Niemand... darf...', 12, 0, 100, 0, 0, 35456, 0, 0, 'Hu''seng der Torwächter to Player'),
(69501, 0, 0, 'IHR werdet knien, wenn Ihr mich um Gnade anfleht, Monster!', 14, 0, 100, 25, 0, 35888, 0, 0, 'Taran Zhu to Player'),
(69501, 1, 0, 'Diese Statuen gebieten über die Macht des ersten Kaisers. Zerstört sie!', 14, 0, 100, 22, 0, 35889, 0, 0, 'Taran Zhu to 208'),
(69501, 2, 0, 'Taoshi, öffnet diese Türen.', 14, 0, 100, 1, 0, 35890, 0, 0, 'Taran Zhu to 208'),
(69501, 3, 0, 'Folgt mir! Wir müssen vorwärts drängen, während uns Eure Kameraden den Rücken freihalten.', 14, 0, 100, 14, 0, 35891, 0, 0, 'Taran Zhu to 208'),
(69501, 4, 0, 'Shan''Bu! Erneut versucht Ihr, einen Gott zu erschaffen. Werdet Ihr jemals lernen?', 14, 0, 100, 5, 0, 35892, 0, 0, 'Taran Zhu to Player'),
(69501, 5, 0, 'Da kommen noch mehr Zandalari.', 12, 0, 100, 25, 0, 35893, 0, 0, 'Taran Zhu to Player'),
(69501, 6, 0, 'Yalia, wir benötigen mehr Heilung! Taoshi, gebt uns Rückendeckung!', 12, 0, 100, 1, 0, 35894, 0, 0, 'Taran Zhu to Player'),
(69501, 7, 0, 'Tretet mir gegenüber, Shan''Bu! Oder ist der Hof des Donnerkönigs ein Nest voller Feiglinge?', 14, 0, 100, 397, 0, 35895, 0, 0, 'Taran Zhu to 208'),
(69501, 8, 0, 'Arrgh – tötet ihn. Schnell!', 14, 0, 100, 0, 0, 35903, 0, 0, 'Taran Zhu to Shan''Bu'),
(69501, 9, 0, 'Erledigt ihn!', 14, 0, 100, 0, 0, 35904, 0, 0, 'Taran Zhu to Shan''Bu'),
(69501, 10, 0, 'Seht Ihr? Niemand kann diejenigen unterdrücken, die reinen Geistes sind.', 12, 0, 100, 0, 0, 35905, 0, 0, 'Taran Zhu to Player'),
(69501, 11, 0, 'Wie kleine Kinder.', 12, 0, 100, 0, 0, 36158, 0, 0, 'Taran Zhu to Player'),
(69501, 12, 0, 'Taoshi! Reicht mir meine Waffe.', 12, 0, 100, 0, 0, 36159, 0, 0, 'Taran Zhu to Player'),
(69501, 13, 0, 'Meine Waffe! Champion, folgt mir. Ich möchte, dass Ihr das seht.', 14, 0, 100, 0, 0, 35907, 0, 0, 'Taran Zhu to Player'),
(69501, 14, 0, 'GENUG! Heute wird es kein weiteres Blutvergießen geben!', 14, 0, 100, 15, 0, 35908, 0, 0, 'Taran Zhu to Player'),
(69501, 15, 0, 'Nun sehe ich, warum Eure Allianz und Eure Horde nicht aufhören können, sich zu bekämpfen.', 14, 0, 100, 5, 0, 35909, 0, 0, 'Taran Zhu to Player'),
(69501, 16, 0, 'Jede Vergeltungstat ist in sich ein Akt der Aggression, und jedem Akt der Aggression folgt unmittelbare Vergeltung.', 14, 0, 100, 0, 0, 35910, 0, 0, 'Taran Zhu to Player'),
(69501, 17, 0, 'SCHWEIGT! IHR müsst den Teufelskreis brechen!', 14, 0, 100, 22, 0, 35911, 0, 0, 'Taran Zhu to Player'),
(69501, 18, 0, 'Es endet hier und HEUTE. Der Teufelskreis bricht, wenn Ihr, Lordregent, und Ihr, Lady Prachtmeer, Euch voneinander abwendet und die Sache ruhen lasst.', 14, 0, 100, 274, 0, 35912, 0, 0, 'Taran Zhu to Player'),
(69501, 19, 0, 'Champion, diese Waffenruhe ist vielleicht nicht von Dauer, aber erinnert Euch daran, was Ihr heute erlebt habt.', 12, 0, 100, 273, 0, 35913, 0, 0, 'Taran Zhu to Player'),
(69501, 20, 0, 'Shan''Bus Monstrosität ist noch immer am Fuße des Palastes. Sie muss aufgehalten werden, bevor sie ganz Pandaria in Schutt und Asche legt.', 12, 0, 100, 0, 0, 35914, 0, 0, 'Taran Zhu to Player'),
(69530, 0, 0, 'Taran Zhu! Ganz gleich, wie viele Ihr gegen den Donnerkönig ins Feld führt, bald werden alle vor ihm das Knie beugen.', 14, 0, 100, 25, 0, 35769, 0, 0, 'Shan''Bu to Player'),
(69530, 1, 0, 'Wer braucht einen Gott? Wir haben den Donnerkönig!', 14, 0, 100, 0, 0, 35770, 0, 0, 'Shan''Bu to Player'),
(69530, 2, 0, 'Trolle – macht Euch nützlich. Ich bin hier fast fertig.', 14, 0, 100, 0, 0, 35771, 0, 0, 'Shan''Bu to Player'),
(69530, 3, 0, 'Erhebt Euch, Nalak! Beschützt unseren geliebten Kaiser.', 14, 0, 100, 0, 0, 35772, 0, 0, 'Shan''Bu to 208'),
(69530, 4, 0, 'Der Kaiser wird hocherfreut sein, wenn ich ihm Euren Kopf präsentiere, Shado-Pan.', 14, 0, 100, 0, 0, 35773, 0, 0, 'Shan''Bu to 208'),
(69534, 0, 0, 'Eure Anmaßung langweilt mich! Ich werde Euch das Leben ausquetschen, arroganter Sklave!', 14, 0, 100, 0, 0, 35779, 0, 0, 'Shan''Bu'),
(69534, 1, 0, 'Ich... nein... Mein Kaiser! Mein Kaiser, ich habe Euch enttäuscht!', 14, 0, 100, 0, 0, 36934, 0, 0, 'Shan''Bu to Player'),
(69538, 0, 0, 'Sie kommen! Zu den Waffen, meine Brüder – schleudert sie zurück ins Meer!', 12, 0, 100, 0, 0, 35415, 0, 0, 'Arkanital Mara''kah to Player'),
(69538, 1, 0, 'Ha! Ihr habt noch nich'' mal die ganze Macht der Zandalari erlebt!', 12, 0, 100, 0, 0, 35416, 0, 0, 'Arkanital Mara''kah to Raptor von Shaol''mara'),
(69538, 2, 0, 'Haltet die Ruinen. Tötet sie einen nach dem anderen!', 12, 0, 100, 0, 0, 35417, 0, 0, 'Arkanital Mara''kah to Player'),
(69538, 3, 0, 'Zermalmt die Eindringlinge! Die Barriere darf nich'' fall''n.', 12, 0, 100, 0, 0, 35418, 0, 0, 'Arkanital Mara''kah to Player'),
(69538, 4, 0, 'Zandalar...', 12, 0, 100, 0, 0, 35423, 0, 0, 'Arkanital Mara''kah to Player'),
(69544, 0, 0, 'Ganz leise... Schleicht Euch an und schaltet sie aus.', 12, 0, 100, 0, 0, 35866, 0, 0, 'Taoshi to Donnerflügel'),
(69544, 1, 0, 'Taoshi nickt in Richtung des Besatzungsmitglieds rechts. Schleicht Euch heran, um es bewusstlos zu schlagen!', 41, 0, 100, 0, 0, 0, 0, 0, 'Taoshi to 69472'),
(69544, 2, 0, 'Hier drüben! Versteckt Euch im Rauch!', 12, 0, 100, 0, 0, 35876, 0, 0, 'Taoshi'),
(69544, 3, 0, 'Auf den nächsten Troll, der sich hier zu schaffen macht, wartet eine sehr unangenehme Überraschung. Verschwinden wir.', 12, 0, 100, 0, 0, 35867, 0, 0, 'Taoshi to Player'),
(69544, 4, 0, 'Nicht so leise, wie ich gehofft hatte, aber auch gut.', 12, 0, 100, 0, 0, 35870, 0, 0, 'Taoshi to 205'),
(69559, 0, 0, 'NARREN! Ich werde euch zeigen, wie es geht!', 14, 0, 100, 25, 0, 0, 0, 0, 'Bestienmeister Horaki to Player'),
(69559, 1, 0, 'Hast du gedacht, du kannst hier einfach so vorbeiflattern?!', 14, 0, 100, 25, 0, 0, 0, 0, 'Bestienmeister Horaki to Player'),
(69559, 2, 0, 'Na ja, du bist an deinem Ziel angekommen. Zu blöd nur, dass du den Rückweg nich'' erleb''n wirst.', 14, 0, 100, 5, 0, 0, 0, 0, 'Bestienmeister Horaki to Player'),
(69559, 3, 0, 'Und jetz'' stirb, Eindringling!', 14, 0, 100, 53, 0, 0, 0, 0, 'Bestienmeister Horaki to Player'),
(69600, 0, 0, 'Hier oben, Champion!', 12, 0, 100, 0, 0, 35943, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds'),
(69600, 1, 0, 'Ich glaube, wir haben eine Methode gefunden, wie wir uns vor den Blitzen schützen können. Fliegt aber zur Sicherheit trotzdem in lockerer Formation.', 12, 0, 100, 0, 0, 35944, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds'),
(69600, 2, 0, 'Unsere Angriffe auf den Friedhof und die Siedlung unter uns zahlen sich bereits aus. Weder die Mogu noch die Zandalari sollten einen Gegenangriff starten können.', 12, 0, 100, 0, 0, 35945, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds'),
(69600, 3, 0, 'Seht Ihr den zentralen Turm? Es schlagen dort unaufhörlich Blitze ein. Wir vermuten, dass diese Stürme vom Herzen des Palastes ausgehen.', 12, 0, 100, 0, 0, 35946, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds'),
(69600, 4, 0, 'Vor uns liegt der Palast des Donnerkönigs. Wir müssen erst auf der Insel Fuß fassen, bevor wir seine Festung angreifen können.', 12, 0, 100, 0, 0, 35978, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds'),
(69600, 5, 0, 'Der Treffpunkt liegt direkt vor uns. Wir haben eine Elitetruppe für einen Angriff... was zum?', 12, 0, 100, 0, 0, 35947, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds'),
(69600, 6, 0, 'Anar''alah! Der Himmel verrät uns – wir sind hier oben nicht sicher!', 12, 0, 100, 0, 0, 35948, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds'),
(69600, 7, 0, 'Landen! Alle Mann landen!', 12, 0, 100, 0, 0, 35949, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds'),
(69600, 8, 0, 'Ahh! Wartet!', 12, 0, 100, 0, 0, 35977, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds'),
(69600, 9, 0, 'Champion, geht es Euch gut?', 12, 0, 100, 0, 0, 35950, 0, 0, 'Vereesa Windläufer to Player'),
(69600, 10, 0, 'Sie waren doch vorbereitet. Unsere Streitkräfte sind verstreut und wir sind auf uns allein gestellt.', 12, 0, 100, 0, 0, 35951, 0, 0, 'Vereesa Windläufer to Player'),
(69600, 11, 0, 'So viel zum Überraschungsmoment.', 12, 0, 100, 0, 0, 35952, 0, 0, 'Vereesa Windläufer to Player'),
(69600, 12, 0, 'Sie ziehen sich zurück. Wir müssen ihnen nachstellen, bevor sie sich wieder sammeln können. Folgt mir!', 12, 0, 100, 0, 0, 35953, 0, 0, 'Vereesa Windläufer to Raptor von Shaol''mara'),
(69600, 13, 0, 'Eine Barrikade versperrt den Weg. Zerstört sie!', 12, 0, 100, 0, 0, 35955, 0, 0, 'Vereesa Windläufer to Player'),
(69600, 14, 0, 'Gute Arbeit. Machen wir weiter.', 12, 0, 100, 0, 0, 35963, 0, 0, 'Vereesa Windläufer to 212'),
(69600, 15, 0, 'Macht Euch gefasst – das ist kein gewöhnlicher Zandalari.', 12, 0, 100, 0, 0, 35956, 0, 0, 'Vereesa Windläufer to Player'),
(69600, 16, 0, 'Euer Stil gefällt mir.', 12, 0, 100, 0, 0, 35966, 0, 0, 'Vereesa Windläufer to 212'),
(69600, 17, 0, 'Der Schild ist durchbrochen! Geschafft!', 12, 0, 100, 0, 0, 35961, 0, 0, 'Vereesa Windläufer to Player'),
(69600, 18, 0, 'Jainas Empfehlung hat sich bestätigt. Ihr habt diese Mission vor dem Scheitern bewahrt... und mir das Leben gerettet.', 12, 0, 100, 0, 0, 35962, 0, 0, 'Vereesa Windläufer to Player'),
(69615, 0, 0, 'Gut, Ihr seid hier.', 12, 0, 100, 66, 0, 36156, 0, 0, 'Vereesa Windläufer'),
(69615, 1, 0, 'Lady Prachtmeer! Alle sind in Position und die Shado-Pan sind bereits im Inneren, aber...', 12, 0, 100, 1, 0, 36157, 0, 0, 'Vereesa Windläufer to Player'),
(69615, 2, 0, 'Die Horde befindet sich auf der anderen Seite des Hofes.', 12, 0, 100, 5, 0, 35930, 0, 0, 'Vereesa Windläufer to Player'),
(69615, 3, 0, 'Lady Prachtmeer – noch mehr Zandalari nähern sich vom Hafen.', 14, 0, 100, 22, 0, 35931, 0, 0, 'Vereesa Windläufer to 208'),
(69615, 4, 0, 'Sie haben meinen Mann getötet!', 14, 0, 100, 5, 0, 35932, 0, 0, 'Vereesa Windläufer to Player'),
(69616, 0, 0, 'Aber was?', 12, 0, 100, 547, 0, 35499, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 1, 0, 'Natürlich ist die Horde auch hier. Aber ein Problem nach dem anderen.', 12, 0, 100, 1, 0, 35500, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 2, 0, 'Alle Mann vorwärts!', 14, 0, 100, 25, 0, 35501, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 3, 0, '... und falls jemand Aethas Sonnenhäscher sieht, der gehört mir...', 12, 0, 100, 45, 0, 35502, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 4, 0, 'Ignoriert die Horde – konzentriert Euch auf die Zandalari am Tor.', 14, 0, 100, 0, 0, 35643, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 5, 0, 'Sie versuchen, uns zu flankieren. Haltet diese Position!', 14, 0, 100, 274, 0, 35504, 0, 0, 'Lady Jaina Prachtmeer to 208'),
(69616, 6, 0, 'Liefert uns den Erzmagier aus und rettet so Eure eigene Haut, Lor''themar.', 14, 0, 100, 22, 0, 35505, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 7, 0, 'Eure Leute sind rechtmäßige Kriegsgefangene. Sie haben einen Angriff auf Darnassus von MEINER Stadt aus...', 14, 0, 100, 5, 0, 35506, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 8, 0, 'Sie haben SÄMTLICHE Friedensbemühungen untergraben!', 14, 0, 100, 15, 0, 35507, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 9, 0, 'Jaina atmet tief durch.', 16, 0, 100, 0, 0, 0, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 10, 0, 'Nun gut. Wir werden nicht angreifen.', 12, 0, 100, 547, 0, 35509, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 11, 0, 'Das hier wird ihn nicht zurückbringen.', 12, 0, 100, 1, 0, 35508, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 12, 0, 'Eins lasst Euch gesagt sein, "Blut-"Elf: Es kann keinen Frieden geben, solange Höllschrei Kriegshäuptling der Horde ist.', 12, 0, 100, 25, 0, 35510, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 13, 0, 'Jainas Ausdruck wird weicher.', 16, 0, 100, 0, 0, 0, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 14, 0, 'Lordregent.', 12, 0, 100, 2, 0, 35511, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69616, 15, 0, 'Wir sammeln uns im Lager. Unsere Arbeit hier ist getan.', 14, 0, 100, 0, 0, 35512, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69617, 0, 0, 'Ein Problem nach dem anderen. Konzentriert Euch auf die Zandalari!', 14, 0, 100, 0, 0, 35643, 0, 0, 'Lor''themar Theron to Player'),
(69617, 1, 0, 'Prachtmeer! Ihr lasst meine Leute aus der Violetten Festung frei oder ich werde Euch eigenhändig erschlagen!', 14, 0, 100, 15, 0, 35608, 0, 0, 'Lor''themar Theron to Player'),
(69617, 2, 0, 'Die Sonnenhäscher wussten NICHTS von Garroshs Angriff auf Darnassus!', 14, 0, 100, 5, 0, 35609, 0, 0, 'Lor''themar Theron to Player'),
(69617, 3, 0, 'Ich muss mein souveränes Volk beschützen.', 14, 0, 100, 14, 0, 35507, 0, 0, 'Lor''themar Theron to Player'),
(69617, 4, 0, 'Lor''themar verengt die Augen.', 16, 0, 100, 0, 0, 0, 0, 0, 'Lor''themar Theron to Player'),
(69617, 5, 0, 'Waldläufer. Senkt die Waffen.', 14, 0, 100, 3, 0, 35611, 0, 0, 'Lor''themar Theron to Player'),
(69617, 6, 0, 'Tut es!', 12, 0, 100, 0, 0, 35612, 0, 0, 'Lor''themar Theron to Player'),
(69617, 7, 0, 'Und genau deswegen möchte ich hier heute die Stärke meines Volkes bewahren.', 12, 0, 100, 1, 0, 35613, 0, 0, 'Lor''themar Theron to Player'),
(69617, 8, 0, 'Lady.', 12, 0, 100, 2, 0, 35614, 0, 0, 'Lor''themar Theron to Player'),
(69617, 9, 0, 'Sammelt die Verwundeten ein. Zieht Euch zum Hafen zurück.', 14, 0, 100, 0, 0, 35615, 0, 0, 'Lor''themar Theron to Player'),
(69619, 0, 0, 'Aethas Sonnenhäscher weicht leicht zurück.', 16, 0, 100, 432, 0, 0, 0, 0, 'Erzmagier Aethas Sonnenhäscher to Player'),
(69620, 0, 0, 'Lordregent?', 14, 0, 100, 547, 0, 36387, 0, 0, 'Späherhauptmann Elsia to Player'),
(69621, 0, 0, 'Lord Zhu! Die Allianz – die Horde – dort draußen bricht gleich das reine Chaos aus.', 14, 0, 100, 0, 0, 35820, 0, 0, 'Taoshi to Player'),
(69621, 1, 0, 'Lord Zhu... Ihr seid schwer verwundet.', 12, 0, 100, 5, 0, 35821, 0, 0, 'Taoshi to Player'),
(69621, 2, 0, 'Lord Zhu!', 14, 0, 100, 0, 0, 35822, 0, 0, 'Taoshi to Player'),
(69621, 3, 0, 'Wir müssen Euch zum Kloster zurückbringen.', 12, 0, 100, 1, 0, 35823, 0, 0, 'Taoshi to Player'),
(69621, 4, 0, 'Yalia, bringt Lord Zhu in Sicherheit.', 12, 0, 100, 25, 0, 35824, 0, 0, 'Taoshi to Player'),
(69621, 5, 0, 'Es ist nun an Euch, den Sturmfürsten Nalak zu besiegen. Ihr werdet eine kleine Armee aufstellen müssen!', 12, 0, 100, 547, 0, 35825, 0, 0, 'Taoshi to Player'),
(69621, 6, 0, 'Doch nach allem, was ich heute gesehen habe, traue ich Euch das zu.', 12, 0, 100, 273, 0, 35826, 0, 0, 'Taoshi to Player'),
(69652, 0, 0, 'Versperrt das Tor. Die Zandalari konnten die Mauern nicht halten.', 14, 0, 100, 0, 0, 35768, 0, 0, 'Shan''Bu to 69755'),
(69717, 0, 0, 'Einerlei. Taoshis Vorschlag ist zu gefährlich.', 12, 0, 100, 274, 0, 35915, 0, 0, 'Taran Zhu to Player'),
(69741, 0, 0, 'Ah! Wir haben gerade über Euch gesprochen.', 12, 0, 100, 396, 0, 35544, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69741, 1, 0, 'Wenn wir den Donnerkönig in die Knie zwingen wollen, dann müssen wir den Hafen einnehmen. Wir brauchen gewagte Ideen.', 12, 0, 100, 396, 0, 35545, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(69743, 0, 0, 'Erwacht, Hu''seng! Ihr seid für den Hafen verantwortlich. Niemand darf die Tore seiner Majestät passieren.', 14, 0, 100, 463, 0, 35781, 0, 0, 'Shan''Bu to Player'),
(69744, 0, 0, 'Waldläufer – zerschlagt die Scharniere. Stemmt die Türen auf.', 14, 0, 100, 0, 0, 35940, 0, 0, 'Vereesa Windläufer to Player'),
(69755, 0, 0, 'Die Geister geb''n uns Stärke!', 14, 0, 100, 0, 0, 35993, 0, 0, 'Kriegsgott Al''chukla to Lady Jaina Prachtmeer'),
(69755, 1, 0, 'Tretet vor! Erblickt die Gaben des Donnerkönigs!', 14, 0, 100, 1, 0, 35997, 0, 0, 'Kriegsgott Al''chukla to Player'),
(69755, 2, 0, 'Zu viel... die Geister können seine Macht nicht halt''n...', 14, 0, 100, 1, 0, 0, 35999, 0, 'Kriegsgott Al''chukla'),
(69755, 3, 0, 'Ihr werdet bestraft! Euer Leben ist ein Schauspiel der Vergeblichkeit!', 14, 0, 100, 1, 0, 35999, 0, 0, 'Kriegsgott Al''chukla'),
(69755, 4, 0, 'Für Zandalar!!', 14, 0, 100, 1, 0, 36001, 0, 0, 'Kriegsgott Al''chukla'),
(69903, 0, 0, 'Ahhh, es gilt eine weitere Seele zu rauben.', 12, 0, 100, 2, 0, 0, 0, 0, 'Seelenreißer der Shan''ze to Player'),
(69917, 0, 0, 'Machen wir uns an die Arbeit.', 12, 0, 100, 396, 0, 31349, 0, 0, 'Taoshi to Player'),
(69917, 1, 0, 'Die Mogu schmieden Waffen aus Blut und Stahl. Ihre Klingen laben sich am Tod.', 12, 0, 100, 0, 0, 35831, 0, 0, 'Taoshi'),
(69917, 2, 0, 'Sie werden lernen, dass sie nirgends vor den Shado-Pan sicher sind.', 12, 0, 100, 0, 0, 35837, 0, 0, 'Taoshi'),
(69917, 3, 0, 'Seid vorsichtig! Wir haben fast das Tor erreicht.', 12, 0, 100, 0, 0, 35841, 0, 0, 'Taoshi'),
(69917, 4, 0, 'Dieses Schloss... ist komplizierter, als ich dachte...', 14, 0, 100, 0, 0, 35843, 0, 0, 'Taoshi to Sklavenmeister Shiaxu'),
(69917, 5, 0, 'Geschafft! Das Tor ist offen!', 14, 0, 100, 0, 0, 35845, 0, 0, 'Taoshi to Sklavenmeister Shiaxu'),
(69923, 0, 0, 'Was? Noch eine Rebellion?', 14, 0, 100, 0, 0, 35787, 0, 0, 'Sklavenmeister Shiaxu'),
(69923, 1, 0, 'Ihr werdet... bestraft werden...', 14, 0, 100, 0, 0, 35788, 0, 0, 'Sklavenmeister Shiaxu to Player'),
(69923, 2, 0, 'Verneigt Euch vor mir!', 14, 0, 100, 0, 0, 35791, 0, 0, 'Sklavenmeister Shiaxu'),
(69923, 3, 0, 'Kniet vor Eurem Meister!', 14, 0, 100, 0, 0, 35792, 0, 0, 'Sklavenmeister Shiaxu'),
(69923, 4, 0, 'Entfernt Euch von den Fesseln, um die Ketten zu sprengen.', 41, 0, 100, 0, 0, 35792, 0, 0, 'Sklavenmeister Shiaxu'),
(69949, 0, 0, 'Die Belagerung ist im Gange, aber unsere Sprengsätze haben es nicht bis zur Mauer geschafft!', 12, 0, 100, 5, 0, 35920, 0, 0, 'Vereesa Windläufer to Player'),
(69949, 1, 0, 'Sammelt die Sprengsätze ein und bringt sie zur Mauer, während ich die Verstärkung organisiere!', 12, 0, 100, 1, 0, 35921, 0,  0,'Vereesa Windläufer to Player'),
(69949, 2, 0, 'Die Mauer ist verdrahtet und will gesprengt werden – Euch gebührt die Ehre!', 12, 0, 100, 1, 0, 35922, 0, 0, 'Vereesa Windläufer to Player'),
(69949, 3, 0, 'Hier entlang – die Zandalari ziehen sich zurück!', 14, 0, 100, 1, 0, 35923, 0, 0, 'Vereesa Windläufer to Player'),
(69949, 4, 0, 'Nur ein Kratzer. Mir geht es gut.', 12, 0, 100, 5, 0, 35928, 0, 0, 'Vereesa Windläufer to 69755'),
(70040, 0, 0, 'Vereesa wartet auf Euch.', 12, 0, 100, 0, 0, 35538, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(70041, 0, 0, 'Dieses wunderschöne Geschöpf ist immun gegen die Blitzschläge, die diese Insel beschützen.', 12, 0, 100, 0, 0, 35933, 0, 0, 'Vereesa Windläufer to Player'),
(70041, 1, 0, 'Könnt Ihr es reiten?', 12, 0, 100, 0, 0, 35934, 0, 0, 'Vereesa Windläufer to Player'),
(70041, 2, 0, 'Wunderbar! Sobald Ihr in den Lüften seid, werden Euch die Zandalari jagen.', 12, 0, 100, 0, 0, 35935, 0, 0, 'Vereesa Windläufer to Player'),
(70041, 3, 0, 'Seid auf einen Luftkampf vorbereitet. Springt von Reittier zu Reittier, wenn nötig!', 12, 0, 100, 0, 0, 35936, 0, 0, 'Vereesa Windläufer to Player'),
(70041, 4, 0, 'Eine Agentin der Shado-Pan wird Euch hinter den Minen treffen. Viel Glück!', 14, 0, 100, 0, 0, 35937, 0, 0, 'Vereesa Windläufer to Player'),
(70041, 5, 0, 'Waldläufer! Sichert die Schmiede. Zerstört das Tor, damit es nicht verschlossen werden kann!', 14, 0, 100, 0, 0, 35938, 0, 0, 'Vereesa Windläufer to Sklavenmeister Shiaxu'),
(70092, 0, 0, 'Setzt sie unter Druck! Sie mögen stark sein, aber wir sind schlauer!', 14, 0, 100, 0, 0, 35488, 0, 0, 'Lady Jaina Prachtmeer'),
(70092, 1, 0, 'Drängt vorwärts!', 14, 0, 100, 0, 0, 35489, 0, 0, 'Lady Jaina Prachtmeer'),
(70092, 2, 0, 'Gute Arbeit! Wir sind durch – Zähne zusammenbeißen, alle Mann. Nutzt die Gunst der Stunde!', 14, 0, 100, 0, 0, 35492, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(70092, 3, 0, 'Er ist tot! Ihr seid eine Inspiration für die Kirin Tor.', 12, 0, 100, 0, 0, 35493, 0, 0, 'Lady Jaina Prachtmeer to 69755'),
(70092, 4, 0, 'Vereesa, seid Ihr verletzt?', 12, 0, 100, 0, 0, 35494, 0, 0, 'Lady Jaina Prachtmeer to 69755'),
(70092, 5, 0, 'Ihr müsst ins Lager zurück. Ich darf Euch nicht verlieren.', 12, 0, 100, 0, 0, 35495, 0, 0, 'Lady Jaina Prachtmeer to 69755'),
(70092, 6, 0, 'Versorgt die Verwundeten! Wir haben einen wichtigen Sieg errungen – der nächste Abschnitt unseres Feldzugs beginnt.', 12, 0, 100, 0, 0, 35496, 0, 0, 'Lady Jaina Prachtmeer to 69755'),
(70105, 0, 0, 'Klingt so, als hätten sie unser kleines Geschenk gefunden.', 12, 0, 100, 0, 0, 35877, 0, 0, 'Taoshi to Player'),
(70105, 1, 0, 'Durch Euch hindurch, wie? Das lässt sich einrichten.', 12, 0, 100, 397, 0, 35878, 0, 0, 'Taoshi to Player'),
(70105, 2, 0, 'Ist Euch der Bauch oder der Brustkorb lieber?', 12, 0, 100, 45, 0, 35879, 0, 0, 'Taoshi to Player'),
(70285, 0, 0, 'Bestienrufer von Shaol''mara ruft Raptoren zu Hilfe!', 41, 0, 100, 0, 0, 28625, 0, 0, 'Bestienrufer von Shaol''mara to Player'),
(70297, 0, 0, 'Champion, hört zu: Wir greifen den Hafen bei Nacht an und benutzen dafür diese prächtige Bestie, die Ihr gebändigt habt.', 12, 0, 100, 1, 0, 35849, 0, 0, 'Taoshi to Player'),
(70370, 0, 0, 'Dieses Jahr war das schlimmste meines Lebens: die Zerstörung Theramores. Der Verrat aus den Reihen der Kirin Tor.', 12, 0, 100, 1, 0, 35519, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(70370, 1, 0, 'Was ich daraus gelernt habe? Von jetzt an ergreife ich die Initiative.', 12, 0, 100, 1, 0, 35520, 0, 0, 'Lady Jaina Prachtmeer to Player'),
(70482, 0, 0, 'Das Mogureich wurde vor über 12.000 Jahren von Lei Shen gegründet, dem "Donnerkönig".', 12, 0, 100, 0, 0, 35970, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds'),
(70482, 1, 0, 'Er vereinigte die Mogu, unterwarf die anderen Völker Pandarias und errichtete ein Kaiserreich, das jahrtausendelang bestehen sollte.', 12, 0, 100, 0, 0, 35971, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds'),
(70482, 2, 0, 'Legenden zufolge hatte der Donnerkönig enorme Macht und baute sich eine palastartige Festung. Wir sollten sie bald sehen können...', 12, 0, 100, 0, 0, 35972, 0, 0, 'Vereesa Windläufer to Hippogryph des Silberbunds');


INSERT IGNORE INTO `page_text` (`ID`, `Text`, `NextPageID`, `PlayerConditionID`, `Flags`, `VerifiedBuild`) VALUES
(4776, 'Lei Shen vereinheitlichte die Sprache, setzte eine gemeinsame Währung ein, standardisierte Gewichte und Maßeinheiten und gründete ein Kaiserreich.$b$BZum ersten Mal waren die Völker des Landes vereint. Der Donnerkönig betrachtete ihr Leid als einen geringen Preis... lediglich eine Schwäche des Fleisches.', 0, 0, 0, 23222), -- 4776
(4775, 'Aber viele Mogu sahen in Lei Shen das gemeinsame Ziel, das in den Generationen seit dem Beginn des Schweigens ihrer Meister gefehlt hatte. Sie marschierten unter dem Banner des Donnerkönigs. Sie dienten ihm bereitwillig, als er die anderen Völker versklavte und teilten seinen Glauben, dass die "niederen" Völker den Mogu dienen sollten, so wie die Mogu einst ihren Herren gedient hatten.', 4776, 0, 0, 23222), -- 4775
(4774, 'Bewaffnet mit der Macht der Stürme sammelte der Donnerkönig sein Gefolge und begann mit der systematischen Unterwerfung der anderen Kriegsfürsten der Mogu. Viele beugten sich seiner Autorität nicht: Die glücklicheren unter ihnen wurden von Blitzen zerstäubt oder von seinen anwachsenden Legionen zertrampelt. Die anderen wurden in Ketten verschleppt, bis er sie als "gebrochen" betrachtete.', 4775, 0, 0, 23222), -- 4774
(4758, 'Unter den Zandalari ist kein Platz für Schwäche. Stärke, Wildheit, Ausdauer, Macht: Das sind die Merkmale, an denen man Erfolg misst. Als Jugendliche müssen die männlichen Zandalari, die nicht zu Priestern oder Gelehrten erwählt wurden, dem Rat, dem König und den Göttern selbst ihre Stärke beweisen.$b$BEin beliebiger Beweis körperlicher Stärke reicht aus. Wenn Kinder zu Erwachsenen werden, werden Turniere und Wettbewerbe abgehalten. Jugendliche bereiten sich in jahrelangem Training, Zwiesprache mit den Geistern und dem Tätowieren magischer Siegel in ihre Haut auf ihre Prüfungen vor. Ein üblicher Ritus ist es, zu einer der brutalen, bestienbeherrschten Inseln nahe der Hauptstadt zu reisen, um ein wildes Tier zu stehlen oder zu zähmen.$b$BDie niederen Trollvölker haben ihre eigenen, bescheideneren Versionen dieser Tradition. Aber das Beherrschen eines Ravasaurus oder Raptors ist nichts gegen die Macht, die das Beherrschen eines Teufelssauriers oder Terrorhorns verlangt.', 0, 0, 0, 23222); -- 4758