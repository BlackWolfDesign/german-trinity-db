-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/25/2017 16:35:33


SET NAMES 'utf8';
INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(100005, 'Sargeras'' Schl�sselstein ist das Kernst�ck meines Plans gegen die Brennende Legion.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55306, 0, 0, 23420),
(100571, 'Er ist auch Euer einziger Weg zur�ck. Findet ihn und kehrt zum Schwarzen Tempel zur�ck.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55309, 0, 0, 23420),
(100022, 'Ihr habt F�rst Illidan geh�rt. Sucht den Schl�sselstein.', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 55248, 0, 0, 23420),
(103461, 'Damit k�nnen wir jede Welt der Legion angreifen, sogar Argus.', '', 0, 0, 0, 0, 0, 0, 5, 0, 0, 55246, 0, 0, 23420),
(96305, '', 'T�tet sie alle!', 0, 0, 0, 0, 0, 0, 15, 0, 0, 0, 55284, 0, 23420),
(101310, 'Wir sind zu weit verstreut. Sammeln!', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55278, 0, 0, 23420),
(100024, 'Wir m�ssen die Bedrohung durch die Legion an diesem Grat eliminieren, um dem Rest unserer Truppen vom Schwarzen Tempel einen Weg zu ebnen.', '', 396, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(98960, 'Vor unvorstellbar langer Zeit erschuf Sargeras Mardum als Gef�ngnis f�r D�monen sowie den Sargeritschl�sselstein, um sie einzusperren. Doch als der Titan beschloss, alle Sch�pfung zu vernichten, zerschmetterte er Mardum und die Bruchst�cke dieser Welt wurden �berall im Wirbelnden Nether verstreut. Die Brennende Legion war geboren. Genau auf diesem Bruchst�ck verbarg Sargeras den Schl�sselstein. Es ist eine Art Generalschl�ssel, der Zugang zu jeder einzelnen Welt der Legion erlaubt. Er ist au�erdem der Schl�ssel zu F�rst Illidans Plan zur Vernichtung der Legion.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(98843, 'Wurde da gerade eines der Portale aktiviert?', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55136, 0, 0, 23420),
(98844, 'Ich will einen Lagebericht! Sofort!', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55137, 0, 0, 23420),
(98855, 'Diese Maschinen besudeln alles mit Teufelsenergie, was sie ber�hren.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55251, 0, 0, 23420),
(100342, '', 'Ich verh�re diese D�monen mit Hilfe der Seelensense, doch sie wissen nicht, wo sich der Sargeritschl�sselstein befindet. Sie reden nur unaufh�rlich von ihrer Treue zu irgendeiner K�nigin. Cyana und Jace sind bereits aufgebrochen. Es scheint jedoch so, als w�re Cyana bei ihrem Versuch, einem anderen D�monenj�ger zu helfen, gefangen genommen worden. Irgendetwas stimmt nicht mit ihr...', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(98846, 'D�monenj�ger benutzen die Portale!', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55138, 0, 0, 23420),
(98847, 'Inquisitor Seelenschmerz? Wer spricht da?', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55139, 0, 0, 23420),
(101647, '<Der schwer verwundete Mystiker h�lt sich nur noch mit letzter Kraft am Leben. Ich bin so gut wie tot. Tut... was getan... werden muss.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(101661, 'Er hat sich als tapfer und w�rdig erwiesen. Wir sollten uns alle gl�cklich sch�tzen, das ultimative Opfer zu Ehren von F�rst Illidan zu erbringen.', '', 273, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(96144, 'Ich bin froh, dass Ihr da seid, $n. Dann lasst uns beginnen.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(103080, 'Der Tiegel? Damit k�nnen wir unsere Feinde aussp�hen oder �ber weite Entfernungen hinweg kommunizieren. Wir k�nnen ihn sogar nutzen, um mit anderen Dimensionen Kontakt aufnehmen. Der Tiegel basiert auf Teufelsenergie. Je weiter das Ziel entfernt ist, desto mehr Energie wird f�r das Ritual ben�tigt.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(103324, 'Die Shivarra und viele andere D�monen schworen F�rst Illlidan die Treue als er ihren vorherigen Meister, den Grubenlord Magtheridon, besiegte. Wir haben au�erdem Naga, Zerschlagene und viele weitere Kreaturen unter unseren Verb�ndeten. Wenn der F�rst der Scherbenwelt ihnen vertraut, dann ist das ausreichend f�r mich.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(96689, 'Beliash wird durch diese Pfeiler des Kummers gesch�tzt. Deaktiviert sie.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55230, 0, 0, 23420),
(101307, 'Viel Gl�ck, $n. Wir sehen uns oben im Vulkan.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55232, 0, 0, 23420),
(99507, 'Wenn Euch die Pfeiler Probleme bereiten, schaltet ihre Stabilisatoren aus.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55231, 0, 0, 23420),
(101650, 'Die Brutk�nigin muss etwas mit dem Tor gemacht haben. Nicht einmal die Seelen mehrerer Mo''arg haben ausgereicht, um es zu reaktivieren. Die Seele eines D�monenj�gers k�nnte jedoch helfen. Ihr m�sst eine Entscheidung treffen, $n... ... Einer von uns beiden muss sterben.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(103337, 'Nat�rlich. Ich opfere mich bereitwillig f�r unsere Sache. Welcher D�monenj�ger w�rde das nicht tun?', '', 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 23420),
(101779, 'Aufsitzen, $n! Die Legion meint es ernst. Sie setzen ihre Verw�ster ein.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55256, 0, 0, 23420),
(101780, 'Wir haben es gerade so geschafft. Die meisten jedenfalls. Wir stehen selbst hier oben unter schwerem Feuer.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55255, 0, 0, 23420),
(101781, '', 'Seid Ihr blind? Feuer!', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55067, 0, 23420),
(94951, '$n, wir m�ssen die Truppen der Legion hier im Vulkan angreifen. Es ist wichtig, sie auszuschalten, bevor wir das Kommandoschiff der Brutk�nigin, die Teufelshammer, angreifen. Unser Plan lautet, die Brutk�nigin zu t�ten und den Sargeritschl�sselstein an uns zu nehmen. Dann k�nnen wir zum Schwarzen Tempel zur�ckkehren und die Invasoren zur�ckschlagen. Keine leichte Aufgabe, doch ich bin mir sicher, dass wir es schaffen k�nnen.', '', 396, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(99020, '', '$n, ich habe einige Teufelsflederm�use gefangen. Ihr k�nnt auf ihnen hoch zum Kommandoschiff der Legion fliegen, wenn Ihr soweit seid.', 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(96307, '', 'Nun? Wie kann ich Euch dienlich sein, $n?', 396, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(92162, 'Ich erwarte Eure Befehle, $G Herr:Herrin;.', '', 66, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(95397, '', 'Seid gegr��t, $n. Wie lautet der Plan?', 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(98273, 'Wir k�nnen erst angreifen, wenn unsere Flanken gesichert sind.', '', 0, 0, 0, 0, 0, 0, 274, 0, 0, 55264, 0, 0, 23420),
(98276, '', 'Ja. Darum m�ssen wir uns unbedingt k�mmern.', 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 55091, 0, 23420),
(108864, 'Ich danke Euch erneut, dass Ihr mir die Ehre gew�hrt, mein Leben f�r die Sache opfern zu d�rfen. Jetzt wisst Ihr, dass ich bereitwillig alles geben w�rde, um F�rst Illidans Plan dienlich zu sein. Die Brennende Legion muss um jeden Preis zerst�rt werden. Ich stehe tief in Eurer Schuld, $n. Jetzt beendet, was wir angefangen haben. Bringt den Sargeritschl�sselstein an Euch und kehrt siegreich zum Schwarzen Tempel zur�ck. Lord Illidan wartet bereits.', '', 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(103959, '$n, ich vermute etwas �beraus M�chtiges in dieser H�hle. Aber die �berw�ltigende Teufelsenergie, die Mardum erf�llt, tr�bt meine Sicht. Ich bin mir nicht sicher. Wenn da drin wirklich etwas lauert, k�nnen wir es uns nicht leisten, es zu ignorieren. Bis auf F�rst Illidan meistert Ihr die F�higkeit der Geistersicht besser als jeder andere. K�nntet Ihr Eure Geistersicht auf die H�hle richten, damit wir herausfinden, ob mein Verdacht berechtigt ist?', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(99065, '', 'Ein vielversprechender Anfang. Doch der Mut scheint Eure Truppen schon zu verlassen.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55056, 0, 23420),
(99066, '', 'Ich w�rde mich ja auf Eure Ankunft freuen, wenn Ihr nur eine Chance h�ttet, zu �berleben.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55057, 0, 23420),
(99067, '', 'Ihr seid es, die nicht davonkommt, D�monenhexe!', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55093, 0, 23420),
(99068, '', 'Wie am�sant. Selbst von hier oben f�hle ich Eure Angst.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55058, 0, 23420),
(99069, '', 'Sagt, habt Ihr je daran gedacht, Euch uns anzuschlie�en?', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55059, 0, 23420),
(99070, '', '...', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(99665, 'Anf�hrer, meldet Euch!', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55253, 0, 0, 23420),
(99666, '', 'Wir sind bei der Seelenmaschine auf heftige Gegenwehr gesto�en.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55297, 0, 23420),
(99667, '', 'Unser Angriff l�uft. Ich sto�e hinzu, wenn Ihr den Aufseher angreift.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55322, 0, 23420),
(99669, '... Schlachtenf�rst, meldet Euch!', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55266, 0, 0, 23420),
(102029, 'Gaardoun?', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55267, 0, 0, 23420),
(99400, '', 'Ich WERDE Euch r�chen, Geliebter!', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55062, 0, 23420),
(99075, 'Das Ger�t, das Ihr soeben zerst�rt habt, stabilisiert die Geb�ude der Legion.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55235, 0, 0, 23420),
(99076, 'Schaut, ob es hier noch mehr davon gibt.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 55236, 0, 0, 23420),
(100842, '', 'Bringt mir den Folianten zur�ck, meine Diener!', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55066, 0, 23420),
(99094, '', '$n, meine Naga werden von einem Grubenlord aufgehalten.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55298, 0, 23420),
(99072, '', 'Erf�llt unseren Pakt und eilt ihnen zu Hilfe.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55296, 0, 23420),
(100737, '', 'Ich wei� nicht, wie Ihr dar�ber denkt, aber ich bin bereit, alles f�r unser �berleben zu tun. Wir ben�tigen mehr Energie!', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(100733, '', 'Es ist unser oberstes Ziel, den Sargeritschl�sselstein zu bergen. Sobald wir hier unten fertig sind, k�nnen wir uns um Tyrannas schwebendes Kommandozentrum k�mmern.', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(100749, '', 'Wenn der Schwarze Tempel nicht angegriffen w�rde, h�tte ich nichts dagegen l�nger hier zu bleiben. Es gibt noch so viele D�monen zu t�ten.', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(99834, 'Vielleicht ist es besser, wenn ich vorerst hier bleibe. Ich habe mich immer noch nicht von den Wunden erholt, die ich als Gefangener dort unten erlitten habe. Diese Welt ist voller d�monischer Energie. Das macht es schwieriger f�r mich, meine eigene Teufelskraft unter Kontrolle zu halten.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(100418, '', 'Ich werde das Fluchtportal zum Schwarzen Tempel sichern, sobald die anderen hier eintreffen.', 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 55096, 0, 23420),
(99829, '', 'Schnell! Kayn hat bereits mit dem Angriff auf Tyranna begonnen.', 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 55095, 0, 23420),
(99826, '', '$n! Ihr und Eure D�monenj�ger seid nichts.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55064, 0, 23420),
(99827, '', 'Der Schl�sselstein wurde mir von Sargeras selbst anvertraut. Er wird niemals Euch geh�ren!', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55065, 0, 23420);


INSERT IGNORE INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(100005, 'deDE', 'Sargeras'' Schl�sselstein ist das Kernst�ck meines Plans gegen die Brennende Legion.', '', 23420),
(100571, 'deDE', 'Er ist auch Euer einziger Weg zur�ck. Findet ihn und kehrt zum Schwarzen Tempel zur�ck.', '', 23420),
(100022, 'deDE', 'Ihr habt F�rst Illidan geh�rt. Sucht den Schl�sselstein.', '', 23420),
(103461, 'deDE', 'Damit k�nnen wir jede Welt der Legion angreifen, sogar Argus.', '', 23420),
(96305, 'deDE', '', 'T�tet sie alle!', 23420),
(101310, 'deDE', 'Wir sind zu weit verstreut. Sammeln!', '', 23420),
(100024, 'deDE', 'Wir m�ssen die Bedrohung durch die Legion an diesem Grat eliminieren, um dem Rest unserer Truppen vom Schwarzen Tempel einen Weg zu ebnen.', '', 23420),
(98960, 'deDE', 'Vor unvorstellbar langer Zeit erschuf Sargeras Mardum als Gef�ngnis f�r D�monen sowie den Sargeritschl�sselstein, um sie einzusperren. Doch als der Titan beschloss, alle Sch�pfung zu vernichten, zerschmetterte er Mardum und die Bruchst�cke dieser Welt wurden �berall im Wirbelnden Nether verstreut. Die Brennende Legion war geboren. Genau auf diesem Bruchst�ck verbarg Sargeras den Schl�sselstein. Es ist eine Art Generalschl�ssel, der Zugang zu jeder einzelnen Welt der Legion erlaubt. Er ist au�erdem der Schl�ssel zu F�rst Illidans Plan zur Vernichtung der Legion.', '', 23420),
(98843, 'deDE', 'Wurde da gerade eines der Portale aktiviert?', '', 23420),
(98844, 'deDE', 'Ich will einen Lagebericht! Sofort!', '', 23420),
(98855, 'deDE', 'Diese Maschinen besudeln alles mit Teufelsenergie, was sie ber�hren.', '', 23420),
(100342, 'deDE', '', 'Ich verh�re diese D�monen mit Hilfe der Seelensense, doch sie wissen nicht, wo sich der Sargeritschl�sselstein befindet. Sie reden nur unaufh�rlich von ihrer Treue zu irgendeiner K�nigin. Cyana und Jace sind bereits aufgebrochen. Es scheint jedoch so, als w�re Cyana bei ihrem Versuch, einem anderen D�monenj�ger zu helfen, gefangen genommen worden. Irgendetwas stimmt nicht mit ihr...', 23420),
(98846, 'deDE', 'D�monenj�ger benutzen die Portale!', '', 23420),
(98847, 'deDE', 'Inquisitor Seelenschmerz? Wer spricht da?', '', 23420),
(101647, 'deDE', '<Der schwer verwundete Mystiker h�lt sich nur noch mit letzter Kraft am Leben. Ich bin so gut wie tot. Tut... was getan... werden muss.', '', 23420),
(101661, 'deDE', 'Er hat sich als tapfer und w�rdig erwiesen. Wir sollten uns alle gl�cklich sch�tzen, das ultimative Opfer zu Ehren von F�rst Illidan zu erbringen.', '', 23420),
(96144, 'deDE', 'Ich bin froh, dass Ihr da seid, $n. Dann lasst uns beginnen.', '', 23420),
(103080, 'deDE', 'Der Tiegel? Damit k�nnen wir unsere Feinde aussp�hen oder �ber weite Entfernungen hinweg kommunizieren. Wir k�nnen ihn sogar nutzen, um mit anderen Dimensionen Kontakt aufnehmen. Der Tiegel basiert auf Teufelsenergie. Je weiter das Ziel entfernt ist, desto mehr Energie wird f�r das Ritual ben�tigt.', '', 23420),
(103324, 'deDE', 'Die Shivarra und viele andere D�monen schworen F�rst Illlidan die Treue als er ihren vorherigen Meister, den Grubenlord Magtheridon, besiegte. Wir haben au�erdem Naga, Zerschlagene und viele weitere Kreaturen unter unseren Verb�ndeten. Wenn der F�rst der Scherbenwelt ihnen vertraut, dann ist das ausreichend f�r mich.', '', 23420),
(96689, 'deDE', 'Beliash wird durch diese Pfeiler des Kummers gesch�tzt. Deaktiviert sie.', '', 23420),
(101307, 'deDE', 'Viel Gl�ck, $n. Wir sehen uns oben im Vulkan.', '', 23420),
(99507, 'deDE', 'Wenn Euch die Pfeiler Probleme bereiten, schaltet ihre Stabilisatoren aus.', '', 23420),
(101650, 'deDE', 'Die Brutk�nigin muss etwas mit dem Tor gemacht haben. Nicht einmal die Seelen mehrerer Mo''arg haben ausgereicht, um es zu reaktivieren. Die Seele eines D�monenj�gers k�nnte jedoch helfen. Ihr m�sst eine Entscheidung treffen, $n... ... Einer von uns beiden muss sterben.', '', 23420),
(103337, 'deDE', 'Nat�rlich. Ich opfere mich bereitwillig f�r unsere Sache. Welcher D�monenj�ger w�rde das nicht tun?', '', 23420),
(101779, 'deDE', 'Aufsitzen, $n! Die Legion meint es ernst. Sie setzen ihre Verw�ster ein.', '', 23420),
(101780, 'deDE', 'Wir haben es gerade so geschafft. Die meisten jedenfalls. Wir stehen selbst hier oben unter schwerem Feuer.', '', 23420),
(101781, 'deDE', '', 'Seid Ihr blind? Feuer!', 23420),
(94951, 'deDE', '$n, wir m�ssen die Truppen der Legion hier im Vulkan angreifen. Es ist wichtig, sie auszuschalten, bevor wir das Kommandoschiff der Brutk�nigin, die Teufelshammer, angreifen. Unser Plan lautet, die Brutk�nigin zu t�ten und den Sargeritschl�sselstein an uns zu nehmen. Dann k�nnen wir zum Schwarzen Tempel zur�ckkehren und die Invasoren zur�ckschlagen. Keine leichte Aufgabe, doch ich bin mir sicher, dass wir es schaffen k�nnen.', '', 23420),
(99020, 'deDE', '', '$n, ich habe einige Teufelsflederm�use gefangen. Ihr k�nnt auf ihnen hoch zum Kommandoschiff der Legion fliegen, wenn Ihr soweit seid.', 23420),
(96307, 'deDE', '', 'Nun? Wie kann ich Euch dienlich sein, $n?', 23420),
(92162, 'deDE', 'Ich erwarte Eure Befehle, $G Herr:Herrin;.', '', 23420),
(95397, 'deDE', '', 'Seid gegr��t, $n. Wie lautet der Plan?', 23420),
(98273, 'deDE', 'Wir k�nnen erst angreifen, wenn unsere Flanken gesichert sind.', '', 23420),
(98276, 'deDE', '', 'Ja. Darum m�ssen wir uns unbedingt k�mmern.', 23420),
(108864, 'deDE', 'Ich danke Euch erneut, dass Ihr mir die Ehre gew�hrt, mein Leben f�r die Sache opfern zu d�rfen. Jetzt wisst Ihr, dass ich bereitwillig alles geben w�rde, um F�rst Illidans Plan dienlich zu sein. Die Brennende Legion muss um jeden Preis zerst�rt werden. Ich stehe tief in Eurer Schuld, $n. Jetzt beendet, was wir angefangen haben. Bringt den Sargeritschl�sselstein an Euch und kehrt siegreich zum Schwarzen Tempel zur�ck. Lord Illidan wartet bereits.', '', 23420),
(103959, 'deDE', '$n, ich vermute etwas �beraus M�chtiges in dieser H�hle. Aber die �berw�ltigende Teufelsenergie, die Mardum erf�llt, tr�bt meine Sicht. Ich bin mir nicht sicher. Wenn da drin wirklich etwas lauert, k�nnen wir es uns nicht leisten, es zu ignorieren. Bis auf F�rst Illidan meistert Ihr die F�higkeit der Geistersicht besser als jeder andere. K�nntet Ihr Eure Geistersicht auf die H�hle richten, damit wir herausfinden, ob mein Verdacht berechtigt ist?', '', 23420),
(99065, 'deDE', '', 'Ein vielversprechender Anfang. Doch der Mut scheint Eure Truppen schon zu verlassen.', 23420),
(99066, 'deDE', '', 'Ich w�rde mich ja auf Eure Ankunft freuen, wenn Ihr nur eine Chance h�ttet, zu �berleben.', 23420),
(99067, 'deDE', '', 'Ihr seid es, die nicht davonkommt, D�monenhexe!', 23420),
(99068, 'deDE', '', 'Wie am�sant. Selbst von hier oben f�hle ich Eure Angst.', 23420),
(99069, 'deDE', '', 'Sagt, habt Ihr je daran gedacht, Euch uns anzuschlie�en?', 23420),
(99070, 'deDE', '', '...', 23420),
(99665, 'deDE', 'Anf�hrer, meldet Euch!', '', 23420),
(99666, 'deDE', '', 'Wir sind bei der Seelenmaschine auf heftige Gegenwehr gesto�en.', 23420),
(99667, 'deDE', '', 'Unser Angriff l�uft. Ich sto�e hinzu, wenn Ihr den Aufseher angreift.', 23420),
(99669, 'deDE', '... Schlachtenf�rst, meldet Euch!', '', 23420),
(102029, 'deDE', 'Gaardoun?', '', 23420),
(99400, 'deDE', '', 'Ich WERDE Euch r�chen, Geliebter!', 23420),
(99075, 'deDE', 'Das Ger�t, das Ihr soeben zerst�rt habt, stabilisiert die Geb�ude der Legion.', '', 23420),
(99076, 'deDE', 'Schaut, ob es hier noch mehr davon gibt.', '', 23420),
(100842, 'deDE', '', 'Bringt mir den Folianten zur�ck, meine Diener!', 23420),
(99094, 'deDE', '', '$n, meine Naga werden von einem Grubenlord aufgehalten.', 23420),
(99072, 'deDE', '', 'Erf�llt unseren Pakt und eilt ihnen zu Hilfe.', 23420),
(100737, 'deDE', '', 'Ich wei� nicht, wie Ihr dar�ber denkt, aber ich bin bereit, alles f�r unser �berleben zu tun. Wir ben�tigen mehr Energie!', 23420),
(100733, 'deDE', '', 'Es ist unser oberstes Ziel, den Sargeritschl�sselstein zu bergen. Sobald wir hier unten fertig sind, k�nnen wir uns um Tyrannas schwebendes Kommandozentrum k�mmern.', 23420),
(100749, 'deDE', '', 'Wenn der Schwarze Tempel nicht angegriffen w�rde, h�tte ich nichts dagegen l�nger hier zu bleiben. Es gibt noch so viele D�monen zu t�ten.', 23420),
(99834, 'deDE', 'Vielleicht ist es besser, wenn ich vorerst hier bleibe. Ich habe mich immer noch nicht von den Wunden erholt, die ich als Gefangener dort unten erlitten habe. Diese Welt ist voller d�monischer Energie. Das macht es schwieriger f�r mich, meine eigene Teufelskraft unter Kontrolle zu halten.', '', 23420),
(100418, 'deDE', '', 'Ich werde das Fluchtportal zum Schwarzen Tempel sichern, sobald die anderen hier eintreffen.', 23420),
(99829, 'deDE', '', 'Schnell! Kayn hat bereits mit dem Angriff auf Tyranna begonnen.', 23420),
(99826, 'deDE', '', '$n! Ihr und Eure D�monenj�ger seid nichts.', 23420),
(99827, 'deDE', '', 'Der Schl�sselstein wurde mir von Sargeras selbst anvertraut. Er wird niemals Euch geh�ren!', 23420);-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_0_23222
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/26/2017 00:03:39


SET NAMES 'utf8';
INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(91453, 'Wonach sucht ihr?', 'Wonach sucht ihr?', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23222);

DELETE FROM `broadcast_text_locale` WHERE (`ID`=91453 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(91453, 'deDE', 'Wonach sucht ihr?', 'Wonach sucht ihr?', 23222);-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 03/16/2017 22:46:21


SET NAMES 'utf8';
INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(69980, "Also, wer ist dabei?", "", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 46036, 23420),
(69981, "Das w�r's! Der Kampf hat begonnen � keine Wetten mehr!", "", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 46036, 23420),
(69966, "Schlie�t jetzt Eure Wetten ab und setzt auf den Sieger des Kampfes.", "", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 47751, 23420),
(69967, "Das w�r's! Keine Wetten mehr! Dann wollen wir mal sehen, was passiert!", "", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 47751, 23420),
(34473, "", "W�hrend die Elementare ihr Bestes tun, um Dunkelk�ste in St�cke zu rei�en, habe ich f�r den Erhalt der verbleibenden Wildtiere gek�mpft.$B$BKann ich auf Eure Hilfe z�hlen, $C?", 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(34512, "", "Ich sch�tze Eure Hilfe, die hiesigen Wildtiere zu sch�tzen. Hoffentlich kommen wir nicht zu sp�t.", 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(34515, "", "Ich w�nschte, ich k�nnte Euch mehr erz�hlen, aber bis vor Kurzem wusste niemand, dass solche Kreaturen existieren. Lange, bevor die Titanen das heutige Azeroth erschufen, vor dem nat�rlichen Kreislauf der Natur, herrschten die Urwesen �ber die Elemente und korrumpierten die Sch�pfung.$B$BWie Onu mir erz�hlte, besiegten die Titanen schlie�lich die Urwesen. Oder sperrten sie zumindest unter der Erde ein. Kann so etwas denn �berhaupt jemals get�tet werden? Ich w�nschte, ich w�sste mehr, aber das ist alles pr�historisch.$B$BIch wei�, dass es solche gibt, die die Urwesen anbeten, so unglaublich dies auch sein mag. Das ist, als w�rde man f�r den Weltuntergang beten. Diese Alten G�tter machen die Leute wahnsinnig!", 274, 1, 6, 0, 800, 800, 0, 0, 1, 0, 0, 0, 23420);


DELETE FROM `broadcast_text_locale` WHERE (`ID`=69980 AND `locale`='deDE') OR (`ID`=69981 AND `locale`='deDE') OR (`ID`=69966 AND `locale`='deDE') OR (`ID`=69967 AND `locale`='deDE') OR (`ID`=34473 AND `locale`='deDE') OR (`ID`=34512 AND `locale`='deDE') OR (`ID`=34515 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(69980, 'deDE', "Also, wer ist dabei?", "", 23420),
(69981, 'deDE', "Das w�r's! Der Kampf hat begonnen � keine Wetten mehr!", "", 23420),
(69966, 'deDE', "Schlie�t jetzt Eure Wetten ab und setzt auf den Sieger des Kampfes.", "", 23420),
(69967, 'deDE', "Das w�r's! Keine Wetten mehr! Dann wollen wir mal sehen, was passiert!", "", 23420),
(34473, 'deDE', "", "W�hrend die Elementare ihr Bestes tun, um Dunkelk�ste in St�cke zu rei�en, habe ich f�r den Erhalt der verbleibenden Wildtiere gek�mpft.$B$BKann ich auf Eure Hilfe z�hlen, $C?", 23420),
(34512, 'deDE', "", "Ich sch�tze Eure Hilfe, die hiesigen Wildtiere zu sch�tzen. Hoffentlich kommen wir nicht zu sp�t.", 23420),
(34515, 'deDE', "", "Ich w�nschte, ich k�nnte Euch mehr erz�hlen, aber bis vor Kurzem wusste niemand, dass solche Kreaturen existieren. Lange, bevor die Titanen das heutige Azeroth erschufen, vor dem nat�rlichen Kreislauf der Natur, herrschten die Urwesen �ber die Elemente und korrumpierten die Sch�pfung.$B$BWie Onu mir erz�hlte, besiegten die Titanen schlie�lich die Urwesen. Oder sperrten sie zumindest unter der Erde ein. Kann so etwas denn �berhaupt jemals get�tet werden? Ich w�nschte, ich w�sste mehr, aber das ist alles pr�historisch.$B$BIch wei�, dass es solche gibt, die die Urwesen anbeten, so unglaublich dies auch sein mag. Das ist, als w�rde man f�r den Weltuntergang beten. Diese Alten G�tter machen die Leute wahnsinnig!", 23420);

-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23360
-- Detected locale: 'deDE'
-- Targeted database: Legion
-- Parsing date: 02/23/2017 14:45:05


SET NAMES 'utf8';
DELETE FROM `broadcast_text_locale` WHERE(`ID`=44441 AND `locale`='deDE') OR (`ID`=44426 AND `locale`='deDE') OR (`ID`=43564 AND `locale`='deDE') OR (`ID`=5820 AND `locale`='deDE') OR (`ID`=43567 AND `locale`='deDE') OR (`ID`=4554 AND `locale`='deDE') OR (`ID`=5627 AND `locale`='deDE') OR (`ID`=8152 AND `locale`='deDE') OR (`ID`=98304 AND `locale`='deDE') OR (`ID`=7738 AND `locale`='deDE') OR (`ID`=49720 AND `locale`='deDE') OR (`ID`=8104 AND `locale`='deDE') OR (`ID`=43444 AND `locale`='deDE') OR (`ID`=2828 AND `locale`='deDE') OR (`ID`=8717 AND `locale`='deDE') OR (`ID`=43587 AND `locale`='deDE') OR (`ID`=11154 AND `locale`='deDE') OR (`ID`=50429 AND `locale`='deDE') OR (`ID`=51896 AND `locale`='deDE') OR (`ID`=13654 AND `locale`='deDE') OR (`ID`=25077 AND `locale`='deDE') OR (`ID`=25072 AND `locale`='deDE') OR (`ID`=43575 AND `locale`='deDE') OR (`ID`=8325 AND `locale`='deDE') OR (`ID`=4960 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(44441, 'deDE', 'Oh, hallo, $n.', '', 23360),
(44426, 'deDE', '', 'Hallo $C. Beim Einkauf?', 23360),
(43564, 'deDE', 'Yarr, und was seid Ihr? $gEin bilgewasserschluckender:Eine bilgewasserschluckende:r; $R mit komischen Hosen wie $gein:eine:c; $C und einer Visage wie $g ein Meeresriese: eine Harpyie;. Ich w�rde eher einen Happen aus Kielhols Stiefel schlucken, als einen Schw�chling wie Euch in die Reihen der Blutsegel aufnehmen.$B$BKommt wieder, wenn Ihr mehr Wasser unter dem Kiel habt.', '', 23360),
(5820, 'deDE', 'Was ist? Nat�rlich bin ich ein Schmied. Glaubt Ihr etwa, Zwerge w�ren die Einzigen, die mit einem Amboss umgehen k�nnen? Wenn Ihr das so seht, bitte sehr. Sucht einen Zwerg, der Euch hilft!', '', 23360),
(43567, 'deDE', '<Bossi versucht herauszufinden, wie sie zur�ck nach unten gelangen kann.>', '', 23360),
(4554, 'deDE', 'Guten Tag auch, $gBursche:M�dchen;, mein Name ist Glotz und diese schier unglaublichen Gnomenger�te in Eurem Besitz gehen vielleicht auf mein Konto! Wenn sie in die Luft geflogen sind, waren es nicht meine. Wenn sie Euch reich gemacht haben, k�nnen wir hingegen gern �ber Tantiemen reden!$B$BOh, und falls Ihr aus Gnomeregan kommt, dann k�nnt Ihr Euch ebensogut zwei Neunziggradwinkel schnappen, sie aneinanderkleben und den Heimweg antreten. Ich habe Euch nichts zu sagen.', '', 23360),
(5627, 'deDE', 'Willkommen in meinem Laden, $C. Was k�nnen die Gebr�der Gutstich f�r Euch tun?', '', 23360),
(8152, 'deDE', 'Man sagt, ein Stich zur rechten Zeit spart... �hm, elf...? Ah, ich wei� es nicht mehr. Sowieso ein dummes Sprichwort.', '', 23360),
(98304, 'deDE', 'Ich wei� ja nicht, was Ihr so geh�rt habt, aber meine Zahlen sind akkurat. AKKURAT, sage ich Euch!', '', 23360),
(7738, 'deDE', 'Glyx und ich haben eine ungeschriebene �bereinkunft: Er beschafft mir meine Vorr�te, und ich mache ihn reich. Doch, ernsthaft! Was dieser Halsabschneider mir f�r so einen j�mmerlichen Beutel zerkr�meltes K�nigsblut abkn�pft - das treibt selbst einem erwachsenen Goblin das Wasser in die Augen.', '', 23360),
(49720, 'deDE', 'Ich wurde von meinen Vorgesetzten hergeschickt, um seltsame Vorg�nge hier in der Beutebucht zu untersuchen. Diese Goblinstadt hat eine Flutwelle erstaunlich gut �berstanden, insbesondere, wenn man den Ruf der Goblinhandwerkskunst in Betracht zieht. Ich bin �berrascht, dass der Ort nicht bei dem Aufprall explodiert ist.$b$bIch vermute, da sind magische Kr�fte im Spiel. Es k�nnte mehr hinter der Beutebucht stecken, als Ihr vermutet, $C...', '', 23360),
(8104, 'deDE', 'Die schnellsten Greife diesseits vom Nistgipfel. Wollt Ihr''s mal versuchen?', '', 23360),
(43444, 'deDE', 'Auf Euer Wohl! Auf das meine! M�ge Zwietracht herrschen keine. $B$BAber wenn doch... LANDET IHR IM H�LLENLOCH! Auf mein Wohl.', '', 23360),
(2828, 'deDE', 'Kommt herein, kommt nur herein. Setzt Euch nur nicht zu nahe an den Ofen. Er ist heute zwar noch nicht explodiert, aber nur um sicherzugehen solltet Ihr Euch woanders hinsetzen.', 'Kommt herein, kommt nur herein. Setzt Euch nur nicht zu nahe an den Ofen. Er ist heute zwar noch nicht explodiert, aber nur um sicherzugehen solltet Ihr Euch woanders hinsetzen.', 23360),
(8717, 'deDE', 'Ihr seht aus wie jemand, in dessen Gegenwart ich auf meinen Geldbeutel achten sollte. Kann ich Euch mit etwas behilflich sein?', 'Ihr seht aus wie jemand, in dessen Gegenwart ich auf meinen Geldbeutel achten sollte. Kann ich Euch mit etwas behilflich sein?', 23360),
(43587, 'deDE', 'Willkommen in Beutebucht, $R.', '', 23360),
(11154, 'deDE', 'Willkommen in Beutebucht, Partner. Ich hoffe, Euer Aufenthalt hier wird angenehm und ereignislos verlaufen - und ich betone dabei ''ereignislos''. Wir haben hier mehr als genug zu Trinken, solltet Ihr durstig sein, und mehr als genug Rausschmei�er, solltet Ihr auf �rger aus sein.$B$BJetzt wo wir das gekl�rt haben, was kann dieser bescheidene Diener des Dampfdruckkartells f�r Euch tun? Oder was noch viel interessanter ist, was k�nnt Ihr denn f�r mich tun, h�?', '', 23360),
(50429, 'deDE', 'Seid gegr��t, $n.', 'Seid gegr��t, $n.', 23360),
(51896, 'deDE', 'Ich kann die Kunst des Kochens lehren. Habt Ihr vor, sie zu erlernen?', 'Ich kann die Kunst des Kochens lehren. Habt Ihr vor, sie zu erlernen?', 23360),
(13654, 'deDE', 'Willkommen, $n. Ich bin Landro Fernblick, Gesch�ftsf�hrer der Schwarzen Flamme. M�glicherweise habt Ihr noch nie von der Schwarzen Flamme geh�rt. Das �berrascht mich nicht; Wir arbeiten hart daran, dass das so bleibt. Wir w�rden gern... Verwicklungen mit Fraktionen vermeiden.$B$BSeien es seltene Artefakte, Gladiatorenk�mpfe oder lukratives Gl�cksspiel, die Schwarze Flamme hat sich darauf spezialisiert, nur die besten Dienstleistungen und Unterhaltungsm�glichkeiten anzubieten, die f�r Geld zu haben sind.$B$BSucht Ihr etwas Bestimmtes?', '', 23360),
(25077, 'deDE', 'Ah... Kuriosit�ten und Seltenheiten. Wir spezialisieren uns auf Azeroths einzigartigere Gegenst�nde f�r den anspruchsvollen Abenteurer.$B$BNun, da Ihr mit mir sprecht, m�sst Ihr ja schon eine Vorstellung davon haben, wonach Ihr sucht.', '', 23360),
(25072, 'deDE', 'So, $n, an welchem Gegenstand seid Ihr interessiert?', '', 23360),
(43575, 'deDE', 'Wie w�re es mit etwas Piratenausr�stung?', '', 23360),
(8325, 'deDE', 'Ich sehe hier am Hafen einige interessante Leute kommen und gehen. Passt auf Euch auf, $n. Man wei� nie genau, wem man vertrauen kann, bei den ganzen zwielichtigen Aktivit�ten, die hier ablaufen...', '', 23360),
(4960, 'deDE', 'Ich muss sagen, in Beutebucht gibt es die beste Muschelsuppe der ganzen S�dlichen Meere. Oh, Ihr interessiert Euch nicht f�r Cuisine, eh? Dann lasst mich Euch als Ersten an der Anlegestelle der LAUNISCHEN MINNA begr��en, des besten Passagierschiffs, das zwischen den �stlichen K�nigreichen und Kalimdor verkehrt. Es sollte bald f�r eine weitere Reise nach Ratschet hier einlaufen, also macht es Euch bis zu seiner Ankunft bequem.', 'Ich muss sagen, in Beutebucht gibt es die beste Muschelsuppe der ganzen S�dlichen Meere. Oh, Ihr interessiert Euch nicht f�r Cuisine, eh? Dann lasst mich Euch als Ersten an der Anlegestelle der LAUNISCHEN MINNA begr��en, des besten Passagierschiffs, das zwischen den �stlichen K�nigreichen und Kalimdor verkehrt. Es sollte bald f�r eine weitere Reise nach Ratschet hier einlaufen, also macht es Euch bis zu seiner Ankunft bequem.', 23360);


INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(44441, 'Oh, hallo, $n.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(44426, '', 'Hallo $C. Beim Einkauf?', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(43564, 'Yarr, und was seid Ihr? $gEin bilgewasserschluckender:Eine bilgewasserschluckende:r; $R mit komischen Hosen wie $gein:eine:c; $C und einer Visage wie $g ein Meeresriese: eine Harpyie;. Ich w�rde eher einen Happen aus Kielhols Stiefel schlucken, als einen Schw�chling wie Euch in die Reihen der Blutsegel aufnehmen.$B$BKommt wieder, wenn Ihr mehr Wasser unter dem Kiel habt.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(5820, 'Was ist? Nat�rlich bin ich ein Schmied. Glaubt Ihr etwa, Zwerge w�ren die Einzigen, die mit einem Amboss umgehen k�nnen? Wenn Ihr das so seht, bitte sehr. Sucht einen Zwerg, der Euch hilft!', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(43567, '<Bossi versucht herauszufinden, wie sie zur�ck nach unten gelangen kann.>', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(4554, 'Guten Tag auch, $gBursche:M�dchen;, mein Name ist Glotz und diese schier unglaublichen Gnomenger�te in Eurem Besitz gehen vielleicht auf mein Konto! Wenn sie in die Luft geflogen sind, waren es nicht meine. Wenn sie Euch reich gemacht haben, k�nnen wir hingegen gern �ber Tantiemen reden!$B$BOh, und falls Ihr aus Gnomeregan kommt, dann k�nnt Ihr Euch ebensogut zwei Neunziggradwinkel schnappen, sie aneinanderkleben und den Heimweg antreten. Ich habe Euch nichts zu sagen.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(5627, 'Willkommen in meinem Laden, $C. Was k�nnen die Gebr�der Gutstich f�r Euch tun?', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(8152, 'Man sagt, ein Stich zur rechten Zeit spart... �hm, elf...? Ah, ich wei� es nicht mehr. Sowieso ein dummes Sprichwort.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(98304, 'Ich wei� ja nicht, was Ihr so geh�rt habt, aber meine Zahlen sind akkurat. AKKURAT, sage ich Euch!', '', 603, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(7738, 'Glyx und ich haben eine ungeschriebene �bereinkunft: Er beschafft mir meine Vorr�te, und ich mache ihn reich. Doch, ernsthaft! Was dieser Halsabschneider mir f�r so einen j�mmerlichen Beutel zerkr�meltes K�nigsblut abkn�pft - das treibt selbst einem erwachsenen Goblin das Wasser in die Augen.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(49720, 'Ich wurde von meinen Vorgesetzten hergeschickt, um seltsame Vorg�nge hier in der Beutebucht zu untersuchen. Diese Goblinstadt hat eine Flutwelle erstaunlich gut �berstanden, insbesondere, wenn man den Ruf der Goblinhandwerkskunst in Betracht zieht. Ich bin �berrascht, dass der Ort nicht bei dem Aufprall explodiert ist.$b$bIch vermute, da sind magische Kr�fte im Spiel. Es k�nnte mehr hinter der Beutebucht stecken, als Ihr vermutet, $C...', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(8104, 'Die schnellsten Greife diesseits vom Nistgipfel. Wollt Ihr''s mal versuchen?', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(43444, 'Auf Euer Wohl! Auf das meine! M�ge Zwietracht herrschen keine. $B$BAber wenn doch... LANDET IHR IM H�LLENLOCH! Auf mein Wohl.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(2828, 'Kommt herein, kommt nur herein. Setzt Euch nur nicht zu nahe an den Ofen. Er ist heute zwar noch nicht explodiert, aber nur um sicherzugehen solltet Ihr Euch woanders hinsetzen.', 'Kommt herein, kommt nur herein. Setzt Euch nur nicht zu nahe an den Ofen. Er ist heute zwar noch nicht explodiert, aber nur um sicherzugehen solltet Ihr Euch woanders hinsetzen.', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(8717, 'Ihr seht aus wie jemand, in dessen Gegenwart ich auf meinen Geldbeutel achten sollte. Kann ich Euch mit etwas behilflich sein?', 'Ihr seht aus wie jemand, in dessen Gegenwart ich auf meinen Geldbeutel achten sollte. Kann ich Euch mit etwas behilflich sein?', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(43587, 'Willkommen in Beutebucht, $R.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(11154, 'Willkommen in Beutebucht, Partner. Ich hoffe, Euer Aufenthalt hier wird angenehm und ereignislos verlaufen - und ich betone dabei ''ereignislos''. Wir haben hier mehr als genug zu Trinken, solltet Ihr durstig sein, und mehr als genug Rausschmei�er, solltet Ihr auf �rger aus sein.$B$BJetzt wo wir das gekl�rt haben, was kann dieser bescheidene Diener des Dampfdruckkartells f�r Euch tun? Oder was noch viel interessanter ist, was k�nnt Ihr denn f�r mich tun, h�?', '', 3, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(50429, 'Seid gegr��t, $n.', 'Seid gegr��t, $n.', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(51896, 'Ich kann die Kunst des Kochens lehren. Habt Ihr vor, sie zu erlernen?', 'Ich kann die Kunst des Kochens lehren. Habt Ihr vor, sie zu erlernen?', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(13654, 'Willkommen, $n. Ich bin Landro Fernblick, Gesch�ftsf�hrer der Schwarzen Flamme. M�glicherweise habt Ihr noch nie von der Schwarzen Flamme geh�rt. Das �berrascht mich nicht; Wir arbeiten hart daran, dass das so bleibt. Wir w�rden gern... Verwicklungen mit Fraktionen vermeiden.$B$BSeien es seltene Artefakte, Gladiatorenk�mpfe oder lukratives Gl�cksspiel, die Schwarze Flamme hat sich darauf spezialisiert, nur die besten Dienstleistungen und Unterhaltungsm�glichkeiten anzubieten, die f�r Geld zu haben sind.$B$BSucht Ihr etwas Bestimmtes?', '', 1, 1, 6, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(25077, 'Ah... Kuriosit�ten und Seltenheiten. Wir spezialisieren uns auf Azeroths einzigartigere Gegenst�nde f�r den anspruchsvollen Abenteurer.$B$BNun, da Ihr mit mir sprecht, m�sst Ihr ja schon eine Vorstellung davon haben, wonach Ihr sucht.', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(25072, 'So, $n, an welchem Gegenstand seid Ihr interessiert?', '', 6, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(43575, 'Wie w�re es mit etwas Piratenausr�stung?', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(8325, 'Ich sehe hier am Hafen einige interessante Leute kommen und gehen. Passt auf Euch auf, $n. Man wei� nie genau, wem man vertrauen kann, bei den ganzen zwielichtigen Aktivit�ten, die hier ablaufen...', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(4960, 'Ich muss sagen, in Beutebucht gibt es die beste Muschelsuppe der ganzen S�dlichen Meere. Oh, Ihr interessiert Euch nicht f�r Cuisine, eh? Dann lasst mich Euch als Ersten an der Anlegestelle der LAUNISCHEN MINNA begr��en, des besten Passagierschiffs, das zwischen den �stlichen K�nigreichen und Kalimdor verkehrt. Es sollte bald f�r eine weitere Reise nach Ratschet hier einlaufen, also macht es Euch bis zu seiner Ankunft bequem.', 'Ich muss sagen, in Beutebucht gibt es die beste Muschelsuppe der ganzen S�dlichen Meere. Oh, Ihr interessiert Euch nicht f�r Cuisine, eh? Dann lasst mich Euch als Ersten an der Anlegestelle der LAUNISCHEN MINNA begr��en, des besten Passagierschiffs, das zwischen den �stlichen K�nigreichen und Kalimdor verkehrt. Es sollte bald f�r eine weitere Reise nach Ratschet hier einlaufen, also macht es Euch bis zu seiner Ankunft bequem.', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360);
-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/24/2017 17:01:08


SET NAMES 'utf8';
INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(67893, "Wenn sich irgendjemand mit Steinen und Felsen auskennt, dann ein Zwerg!", "", 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(49842, "", "Tiefenheim hat sich in den letzten Jahren so sehr ver�ndert, es ist kaum wiederzuerkennen.$B$BUnd doch bin ich hier... frei von Verderbnis. Frei von dem verderbenden Einfluss der Uralten G�tter. Es scheint, als m�sste einem manchmal die Freiheit genommen werden, damit man ihren wahren Wert wieder zu sch�tzen wei�.", 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 23420),
(43562, "", "Dieser Steintrogg steckt hinter dem Auswuchs auf der Rubinspanne.$B$BIch sp�re eine seltsame Macht an diesem Ort... Seid vorsichtig.", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(109217, "Das sind beunruhigende Worte, Champion.", "", 0, 0, 0, 0, 0, 0, 1, 0, 0, 67030, 0, 0, 23420),
(109218, "Wir m�ssen den Schattenhammer besiegen und das Gleichgewicht in Tiefenheim wiederherstellen.", "", 0, 0, 0, 0, 0, 0, 1, 0, 0, 67031, 0, 0, 23420);


DELETE FROM `broadcast_text_locale` WHERE (`ID`=67893 AND `locale`='deDE') OR (`ID`=49842 AND `locale`='deDE') OR (`ID`=43562 AND `locale`='deDE') OR (`ID`=109217 AND `locale`='deDE') OR (`ID`=109218 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(67893, 'deDE', "Wenn sich irgendjemand mit Steinen und Felsen auskennt, dann ein Zwerg!", "", 23420),
(49842, 'deDE', "", "Tiefenheim hat sich in den letzten Jahren so sehr ver�ndert, es ist kaum wiederzuerkennen.$B$BUnd doch bin ich hier... frei von Verderbnis. Frei von dem verderbenden Einfluss der Uralten G�tter. Es scheint, als m�sste einem manchmal die Freiheit genommen werden, damit man ihren wahren Wert wieder zu sch�tzen wei�.", 23420),
(43562, 'deDE', "", "Dieser Steintrogg steckt hinter dem Auswuchs auf der Rubinspanne.$B$BIch sp�re eine seltsame Macht an diesem Ort... Seid vorsichtig.", 23420),
(109217, 'deDE', "Das sind beunruhigende Worte, Champion.", "", 23420),
(109218, 'deDE', "Wir m�ssen den Schattenhammer besiegen und das Gleichgewicht in Tiefenheim wiederherstellen.", "", 23420);
-- TrinityCore - WowPacketParser
-- File name: multi
-- Toaster's Sniffs
-- Detected build: V7_1_5_23420
-- Detected locale: 'deDE'
-- Targeted database: Legion
-- Parsing date: 02/23/2017 14:28:29


SET NAMES 'utf8';
INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(27931, 'Dieser Posten ist sicher, $R.$b$bDiese Gegend ist seit einiger Zeit frei von Feinden.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(27606, 'Gr��e, $R.$b$bIch f�rchte, Ihr m�sst mich entschuldigen, da ich im Moment furchtbar besch�ftigt bin.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(30433, 'Seht Euch um und lasst die Zerst�rung auf Euch wirken, $C. Auch der letzte Troll, der hier gelebt hat, ist nun entweder tot oder zu den h�heren Ebenen geflohen.$b$bUnd es kommt noch schlimmer.$b$bIn einem Akt der Verzweiflung haben die �berlebenden Trolle der Drakkari ihre eigenen Tierg�tter geopfert, sodass sie vom Blut der G�tter trinken und dadurch ungeahnte Kr�fte entwickeln konnten. W�hrend sie zwar erfolgreich die Gei�el dank ihrer neuen Kr�fte aufhalten konnten, war das Resultat dennoch katastrophal. Das uralte Imperium von Zul''Drak liegt nun als Beweis daf�r in Tr�mmern.', '', 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(30482, 'Bitte seid leise! Welpen schlafen hier!', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(29866, '', 'Beeilt Euch, $R.$b$bHier drau�en geht''s um Leben und Tod!', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(28437, 'Niemand kennt die Gei�el besser als wir Todesritter.$b$bGlaubt mir, $R, es gibt nichts Effektiveres, als Feuer mit Feuer zu bek�mpfen...', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(28525, '', 'Geht weiter. Hier gibt es nichts zu sehen.', 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420);


DELETE FROM `broadcast_text_locale` WHERE (`ID`=27931 AND `locale`='deDE') OR (`ID`=27606 AND `locale`='deDE') OR (`ID`=30433 AND `locale`='deDE') OR (`ID`=30482 AND `locale`='deDE') OR (`ID`=29866 AND `locale`='deDE') OR (`ID`=28437 AND `locale`='deDE') OR (`ID`=28525 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(27931, 'deDE', 'Dieser Posten ist sicher, $R.$b$bDiese Gegend ist seit einiger Zeit frei von Feinden.', '', 23420),
(27606, 'deDE', 'Gr��e, $R.$b$bIch f�rchte, Ihr m�sst mich entschuldigen, da ich im Moment furchtbar besch�ftigt bin.', '', 23420),
(30433, 'deDE', 'Seht Euch um und lasst die Zerst�rung auf Euch wirken, $C. Auch der letzte Troll, der hier gelebt hat, ist nun entweder tot oder zu den h�heren Ebenen geflohen.$b$bUnd es kommt noch schlimmer.$b$bIn einem Akt der Verzweiflung haben die �berlebenden Trolle der Drakkari ihre eigenen Tierg�tter geopfert, sodass sie vom Blut der G�tter trinken und dadurch ungeahnte Kr�fte entwickeln konnten. W�hrend sie zwar erfolgreich die Gei�el dank ihrer neuen Kr�fte aufhalten konnten, war das Resultat dennoch katastrophal. Das uralte Imperium von Zul''Drak liegt nun als Beweis daf�r in Tr�mmern.', '', 23420),
(30482, 'deDE', 'Bitte seid leise! Welpen schlafen hier!', '', 23420),
(29866, 'deDE', '', 'Beeilt Euch, $R.$b$bHier drau�en geht''s um Leben und Tod!', 23420),
(28437, 'deDE', 'Niemand kennt die Gei�el besser als wir Todesritter.$b$bGlaubt mir, $R, es gibt nichts Effektiveres, als Feuer mit Feuer zu bek�mpfen...', '', 23420),
(28525, 'deDE', '', 'Geht weiter. Hier gibt es nichts zu sehen.', 23420);

-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23420
-- Detected locale: deDE
-- Targeted database: Legion
-- Parsing date: 02/24/2017 16:36:17


SET NAMES 'utf8';
INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(50429, "Seid gegr��t, $n.", "Seid gegr��t, $n.", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(49789, "Unser Meister Malfurion ist aus dem Traum wiedergekehrt, $C. Er erstrahlt wie ein Leuchtfeuer, das uns den Weg weist, und wir m�ssen alles in unserer Macht Stehende tun, um dieses Feuer zu bewahren.", "Unser Meister Malfurion ist aus dem Traum wiedergekehrt, $C. Er erstrahlt wie ein Leuchtfeuer, das uns den Weg weist, und wir m�ssen alles in unserer Macht Stehende tun, um dieses Feuer zu bewahren.", 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420),
(50711, "", "Nach allem, was wir durchgestanden haben, haben wir Gilneer nicht die Hoffnung aufgegeben.", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23420),
(24118, "", "Wir haben geschworen, stets unser Bestes zu geben, um die Fahrg�ste der Bravado zu besch�tzen. Die Seestra�e zwischen Rut'theran und dem Hafen von Sturmwind muss sicher bleiben.", 1, 0, 0, 0, 0, 0, 0, 7, 1, 0, 0, 0, 23420),
(8099, "Um unsere Verbindung zum Festland aufrechtzuerhalten, haben wir Hippogryphen, die regelm��ig zwischen Rut'theran und Dunkelk�ste hin und her fliegen.", "", 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23420);

SET NAMES 'latin1';
DELETE FROM `broadcast_text_locale` WHERE (`ID`=50429 AND `locale`='deDE') OR (`ID`=49789 AND `locale`='deDE') OR (`ID`=50711 AND `locale`='deDE') OR (`ID`=24118 AND `locale`='deDE') OR (`ID`=8099 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(50429, 'deDE', "Seid gegr��t, $n.", "Seid gegr��t, $n.", 23420),
(49789, 'deDE', "Unser Meister Malfurion ist aus dem Traum wiedergekehrt, $C. Er erstrahlt wie ein Leuchtfeuer, das uns den Weg weist, und wir m�ssen alles in unserer Macht Stehende tun, um dieses Feuer zu bewahren.", "Unser Meister Malfurion ist aus dem Traum wiedergekehrt, $C. Er erstrahlt wie ein Leuchtfeuer, das uns den Weg weist, und wir m�ssen alles in unserer Macht Stehende tun, um dieses Feuer zu bewahren.", 23420),
(50711, 'deDE', "", "Nach allem, was wir durchgestanden haben, haben wir Gilneer nicht die Hoffnung aufgegeben.", 23420),
(24118, 'deDE', "", "Wir haben geschworen, stets unser Bestes zu geben, um die Fahrg�ste der Bravado zu besch�tzen. Die Seestra�e zwischen Rut'theran und dem Hafen von Sturmwind muss sicher bleiben.", 23420),
(8099, 'deDE', "Um unsere Verbindung zum Festland aufrechtzuerhalten, haben wir Hippogryphen, die regelm��ig zwischen Rut'theran und Dunkelk�ste hin und her fliegen.", "", 23420);

-- TrinityCore - WowPacketParser
-- File name: multi
-- Detected build: V7_1_5_23360
-- Detected locale: 'deDE'
-- Targeted database: Legion
-- Parsing date: 02/23/2017 13:03:48

SET NAMES 'utf8';
DELETE FROM `broadcast_text_locale` WHERE(`ID`=75955 AND `locale`='deDE') OR (`ID`=75457 AND `locale`='deDE') OR (`ID`=75449 AND `locale`='deDE') OR (`ID`=75445 AND `locale`='deDE') OR (`ID`=75440 AND `locale`='deDE') OR (`ID`=75438 AND `locale`='deDE') OR (`ID`=75439 AND `locale`='deDE') OR (`ID`=75437 AND `locale`='deDE') OR (`ID`=75432 AND `locale`='deDE') OR (`ID`=75431 AND `locale`='deDE') OR (`ID`=75451 AND `locale`='deDE') OR (`ID`=74578 AND `locale`='deDE') OR (`ID`=75796 AND `locale`='deDE') OR (`ID`=75802 AND `locale`='deDE') OR (`ID`=76561 AND `locale`='deDE') OR (`ID`=76553 AND `locale`='deDE') OR (`ID`=75572 AND `locale`='deDE') OR (`ID`=76739 AND `locale`='deDE') OR (`ID`=74577 AND `locale`='deDE') OR (`ID`=74540 AND `locale`='deDE') OR (`ID`=75570 AND `locale`='deDE') OR (`ID`=75568 AND `locale`='deDE') OR (`ID`=75569 AND `locale`='deDE') OR (`ID`=75627 AND `locale`='deDE') OR (`ID`=62617 AND `locale`='deDE') OR (`ID`=75628 AND `locale`='deDE') OR (`ID`=76792 AND `locale`='deDE');
INSERT INTO `broadcast_text_locale` (`ID`, `locale`, `MaleText_lang`, `FemaleText_lang`, `VerifiedBuild`) VALUES
(75955, 'deDE', 'Seid bitte vorsichtig, $n. Die Himmlischen Erhabenen werden hier keine Aggressionen zwischen Allianz und Horde tolerieren.', '', 23360),
(75457, 'deDE', 'Ihr solltet etwas trinken. Trinkt! Habt Spaß!', '', 23360),
(75449, 'deDE', 'Nach dem Kampf gegen die Mogu frage ich mich, wie stark andere Gegner überhaupt noch sein können.', '', 23360),
(75445, 'deDE', 'Ihr kämpft mit viel Talent und Hingabe. Ich hoffe, eines Tages Eurem Weg folgen zu können.', '', 23360),
(75440, 'deDE', '', 'Es ist wahrlich eine Ehre, in der Gegenwart von Xuen und den anderen Erhabenen verweilen zu dürfen.', 23360),
(75438, 'deDE', 'Bleibt wachsam, aber vergesst nicht, auch die schönen Dinge des Lebens zu genießen: gutes Essen, gute Freunde und feines Gebräu!', '', 23360),
(75439, 'deDE', 'Wir können viel lernen, wenn wir andere beim Kämpfen beobachten.', '', 23360),
(75437, 'deDE', 'Entspannt Euch. Ihr seid unter Freunden.', '', 23360),
(75432, 'deDE', '', 'Bleibt Euch immer selbst treu, dann seid Ihr standhaft wie ein mächtiger Gletscher.', 23360),
(75431, 'deDE', '', 'Dieser Ort strahlt Ruhe aus. Er wurde nicht von den Kriegen heimgesucht, die das Festland verheeren.', 23360),
(75451, 'deDE', 'Das Turnier der Erhabenen stellt Eure Qualitäten als Anführer auf die Probe, indem Ihr gegen andere Teilnehmer Haustierkämpfe austragt. Wenn Ihr gut genug seid, tretet Ihr sogar gegen die Kinder der Erhabenen an.', '', 23360),
(74578, 'deDE', '', 'Seid gegrüßt, $n! Ich freue mich, Euch wiederzusehen. Alle reden immer noch davon, wie Ihr letztes Mal das Turnier gewonnen habt – ich habe nicht alles mitbekommen, aber ich musste eine ganze Menge Schnitte und Verbrennungen heilen. Aber keine Sorge: Sie sind alle wieder auf den Beinen!', 23360),
(75796, 'deDE', 'Jene, die sich wegen ihrer eigenen, selbstsüchtigen Ziele in die Zeitlinien einmischen, sind meist gefährlich. Aus diesem Grund schloss ich mich den Wächtern, den standhaften Verteidigern der Zeitwanderer, an. Wir schützen die Behüter und Weber bei ihren Aufgaben.', '', 23360),
(75802, 'deDE', 'Die Weber sind Arkanisten, die sich auf die Chronomantie, also die Kunst des Zeitwebens, spezialisiert haben. Unter den Zeitwanderern sind es allein wir, die Schäden an den Zeitlinien reparieren, temporale Portale öffnen und ähnliche Dinge tun können. Einst waren diese Dinge für den bronzenen Drachenschwarm einfach, aber jetzt leider nicht mehr.', '', 23360),
(76561, 'deDE', '', 'Oh, hallo!$b$bBitte, kommt doch rein! Ruht Euch ein Weilchen aus.$b$bMöchtet Ihr etwas essen? Oder trinken?', 23360),
(76553, 'deDE', 'Ihr wollt ein Paket abholen? Einen Brief senden?$b$bDas geht hier alles ruckzuck!', '', 23360),
(75572, 'deDE', 'Eure Geschichte setzt sich fort.$B$BUnd sie ist es wert, gelesen zu werden.', '', 23360),
(76739, 'deDE', 'Nun gut, ich brauche Hilfe. Ich suche meinen Körper. Er ist irgendwo hier in dieser Höhle, aber Ihr könnt ihn nur sehen, wenn Ihr Euch in der Geisterwelt befindet. Wenn Ihr mir helfen möchtet, ihn zu finden, lasst es mich wissen. Doch seid gewarnt: Die zeitlosen Geister können uns aus der Geisterwelt werfen, und wenn sie das tun sollten, glaube ich, dass ich eine Weile lang nicht stark genug bin, um Euch wieder dorthin zu bringen.', '', 23360),
(74577, 'deDE', 'Willkommen, $GReisender:Reisende;! Ich biete Schätze aus aller Welt feil.$b$bWollt Ihr Euer Glück auf die Probe stellen? Nur ein paar zeitlose Münzen und Ihr erhaltet einen Schlüssel.', '', 23360),
(74540, 'deDE', 'Die Statue brummt sanft, als Ihr Euch nähert.', 'Die Statue brummt sanft, als Ihr Euch nähert.', 23360),
(75570, 'deDE', 'Nur ein arbeitsamer Grummel ist ein guter Grummel!', '', 23360),
(75568, 'deDE', 'Heute ist uns das Glück hold. Es gibt dort viele Leute, denen man Dinge liefern kann.', '', 23360),
(75569, 'deDE', 'Ihr bewegt Euch wie ein Champion, $R.', '', 23360),
(75627, 'deDE', 'Es gibt nichts Schöneres als ein feines Gebräu!', '', 23360),
(62617, 'deDE', 'Hmm... Wenn ich das damit mische... Oh, ja. Hallo!', '', 23360),
(75628, 'deDE', 'Wie wäre es mit einer neuen Runde?', '', 23360),
(76792, 'deDE', 'Es freut mich so, diesen Augenblick mit meinem Sohn teilen zu können.', '', 23360);


INSERT IGNORE INTO `broadcast_text` (`ID`, `MaleText`, `FemaleText`, `EmoteID1`, `EmoteID2`, `EmoteID3`, `EmoteDelay1`, `EmoteDelay2`, `EmoteDelay3`, `UnkEmoteID`, `Language`, `Type`, `SoundID1`, `SoundID2`, `PlayerConditionID`, `VerifiedBuild`) VALUES
(75955, 'Seid bitte vorsichtig, $n. Die Himmlischen Erhabenen werden hier keine Aggressionen zwischen Allianz und Horde tolerieren.', '', 396, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75457, 'Ihr solltet etwas trinken. Trinkt! Habt Spaß!', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75449, 'Nach dem Kampf gegen die Mogu frage ich mich, wie stark andere Gegner überhaupt noch sein können.', '', 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75445, 'Ihr kämpft mit viel Talent und Hingabe. Ich hoffe, eines Tages Eurem Weg folgen zu können.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75440, '', 'Es ist wahrlich eine Ehre, in der Gegenwart von Xuen und den anderen Erhabenen verweilen zu dürfen.', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75438, 'Bleibt wachsam, aber vergesst nicht, auch die schönen Dinge des Lebens zu genießen: gutes Essen, gute Freunde und feines Gebräu!', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75439, 'Wir können viel lernen, wenn wir andere beim Kämpfen beobachten.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75437, 'Entspannt Euch. Ihr seid unter Freunden.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75432, '', 'Bleibt Euch immer selbst treu, dann seid Ihr standhaft wie ein mächtiger Gletscher.', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75431, '', 'Dieser Ort strahlt Ruhe aus. Er wurde nicht von den Kriegen heimgesucht, die das Festland verheeren.', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75451, 'Das Turnier der Erhabenen stellt Eure Qualitäten als Anführer auf die Probe, indem Ihr gegen andere Teilnehmer Haustierkämpfe austragt. Wenn Ihr gut genug seid, tretet Ihr sogar gegen die Kinder der Erhabenen an.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(74578, '', 'Seid gegrüßt, $n! Ich freue mich, Euch wiederzusehen. Alle reden immer noch davon, wie Ihr letztes Mal das Turnier gewonnen habt – ich habe nicht alles mitbekommen, aber ich musste eine ganze Menge Schnitte und Verbrennungen heilen. Aber keine Sorge: Sie sind alle wieder auf den Beinen!', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(75796, 'Jene, die sich wegen ihrer eigenen, selbstsüchtigen Ziele in die Zeitlinien einmischen, sind meist gefährlich. Aus diesem Grund schloss ich mich den Wächtern, den standhaften Verteidigern der Zeitwanderer, an. Wir schützen die Behüter und Weber bei ihren Aufgaben.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75802, 'Die Weber sind Arkanisten, die sich auf die Chronomantie, also die Kunst des Zeitwebens, spezialisiert haben. Unter den Zeitwanderern sind es allein wir, die Schäden an den Zeitlinien reparieren, temporale Portale öffnen und ähnliche Dinge tun können. Einst waren diese Dinge für den bronzenen Drachenschwarm einfach, aber jetzt leider nicht mehr.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(76561, '', 'Oh, hallo!$b$bBitte, kommt doch rein! Ruht Euch ein Weilchen aus.$b$bMöchtet Ihr etwas essen? Oder trinken?', 3, 1, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 23360),
(76553, 'Ihr wollt ein Paket abholen? Einen Brief senden?$b$bDas geht hier alles ruckzuck!', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75572, 'Eure Geschichte setzt sich fort.$B$BUnd sie ist es wert, gelesen zu werden.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(76739, 'Nun gut, ich brauche Hilfe. Ich suche meinen Körper. Er ist irgendwo hier in dieser Höhle, aber Ihr könnt ihn nur sehen, wenn Ihr Euch in der Geisterwelt befindet. Wenn Ihr mir helfen möchtet, ihn zu finden, lasst es mich wissen. Doch seid gewarnt: Die zeitlosen Geister können uns aus der Geisterwelt werfen, und wenn sie das tun sollten, glaube ich, dass ich eine Weile lang nicht stark genug bin, um Euch wieder dorthin zu bringen.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(74577, 'Willkommen, $GReisender:Reisende;! Ich biete Schätze aus aller Welt feil.$b$bWollt Ihr Euer Glück auf die Probe stellen? Nur ein paar zeitlose Münzen und Ihr erhaltet einen Schlüssel.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(74540, 'Die Statue brummt sanft, als Ihr Euch nähert.', 'Die Statue brummt sanft, als Ihr Euch nähert.', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(75570, 'Nur ein arbeitsamer Grummel ist ein guter Grummel!', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75568, 'Heute ist uns das Glück hold. Es gibt dort viele Leute, denen man Dinge liefern kann.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75569, 'Ihr bewegt Euch wie ein Champion, $R.', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(75627, 'Es gibt nichts Schöneres als ein feines Gebräu!', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(62617, 'Hmm... Wenn ich das damit mische... Oh, ja. Hallo!', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360),
(75628, 'Wie wäre es mit einer neuen Runde?', '', 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23360),
(76792, 'Es freut mich so, diesen Augenblick mit meinem Sohn teilen zu können.', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 23360);
